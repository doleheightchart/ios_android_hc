//
//  ToastControllerViewController.m
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 2. 21..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "ToastController.h"
#import "UIView+ImageUtil.h"
#import "UIView+Animation.h"
#import "UIFont+DoleHeightChart.h"
#import "UIColor+ColorUtil.h"
#import "NSString+EtcUtil.h"


@interface ToastView : UIView{
    UIImageView     *_backgroundImageView;
    UILabel         *_messageLabel1;
//    UILabel         *_messageLabel2;
}

@end

@implementation ToastView

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self){
        [self addBackgroundImage];
        [self addMessageLabel];
    }
    
    return self;
}

-(void)addBackgroundImage
{
    UIImage *backImage = [UIImage resizableImageWithName:@"popup_toast" CapInsets:UIEdgeInsetsMake(20/2, 22/2, 20/2, 22/2)];
    _backgroundImageView = [[UIImageView alloc]initWithFrame:self.bounds];
    _backgroundImageView.image = backImage;
    [self addSubview:_backgroundImageView];
}

-(void)addMessageLabel
{
    CGFloat lg = 22/2;
    CGFloat tg = 20/2;
    CGRect f = CGRectMake(lg, tg, self.bounds.size.width - lg*2, self.bounds.size.height - tg*2);
    
    _messageLabel1 = [[UILabel alloc]initWithFrame:f];
    _messageLabel1.backgroundColor = [UIColor clearColor];
    _messageLabel1.textAlignment = NSTextAlignmentCenter;
    _messageLabel1.font = [UIFont defaultKoreanFontWithSize:28/2];
    _messageLabel1.textColor = [UIColor colorWithRGB:0xffffff];
    _messageLabel1.numberOfLines = 3;
    _messageLabel1.lineBreakMode = NSLineBreakByWordWrapping;
    _messageLabel1.minimumScaleFactor = 14/2;
//    _messageLabel1.adjustsFontSizeToFitWidth = YES;
    
    
    
    [self addSubview:_messageLabel1];
}

-(void)setToastMessage:(NSString*)toastMessage
{
    _messageLabel1.text = toastMessage;
}


@end

@implementation ToastController



+(void)showToastWithMessage:(NSString*)message duration:(NSTimeInterval)duration
{
//    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
//    CGSize size = window.bounds.size;
//
//    CGFloat y = size.height - (72+70)/2;
//    
//    UIFont *font = [UIFont defaultRegularFontWithSize:28/2];
//    CGSize messageSize = [message sizeWithFont:font];
//    CGFloat viewWidth = messageSize.width + (22/2)*2;
//    
//    CGFloat x = (size.width - viewWidth)/2;
//    
//    CGRect frameToast = CGRectMake(x, y, viewWidth, (20+32+20)/2);
//    
//    ToastView *tv = [[ToastView alloc]initWithFrame:frameToast];
//    [tv setToastMessage:message];
//    
//    [window addSubview:tv];
//
//    [UIView animateWithDuration:duration
//                     animations:^{
//                         tv.layer.opacity = 0;
//                     }
//                     completion:^(BOOL finished){
//                         [tv removeFromSuperview];
//                     }];
    
    [ToastController showToastWithMessage:message duration:duration completion:nil];
}

+(NSTimeInterval)toastDurationWithInputDuration:(NSTimeInterval)duration Message:(NSString*)message
{
    if (duration == -1){
        NSInteger count = [message countNewLineCharater];
        
        if (count > 1){
            return 4;
        }
        else{
            if (message.length > 10)
                return 3;
            else
                return 2;
        }
    }
    return duration;
}

+(void)showToastWithMessage:(NSString*)message duration:(NSTimeInterval)duration completion:(void(^)())completion;
{
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    CGSize size = window.bounds.size;
    
    UIFont *font = [UIFont defaultKoreanFontWithSize:28/2];
    CGSize messageSize = [message sizeWithFont:font
                        constrainedToSize:CGSizeMake(250, 100000)];

    CGFloat viewWidth = messageSize.width + (22/2)*2;
    CGFloat viewHeight = messageSize.height + (20/2)*2;

    
    CGFloat x = (size.width - viewWidth)/2;
    CGFloat y = size.height - viewHeight - 70/2;

    CGRect frameToast = CGRectMake(x, y, viewWidth, viewHeight);
    
    ToastView *tv = [[ToastView alloc]initWithFrame:frameToast];
    [tv setToastMessage:message];

    [window addSubview:tv];
    
    NSTimeInterval toastDuration = [ToastController toastDurationWithInputDuration:duration Message:message];
    
    [UIView animateWithDuration:toastDuration
                     animations:^{
                         tv.layer.opacity = 0;
                     }
                     completion:^(BOOL finished){
                         [tv removeFromSuperview];
                         
                         if (completion){
                             completion();
                         }
                     }];
}

+(void)showToastWithMessage:(NSString *)message
                   duration:(NSTimeInterval)duration
           onViewController:(UIViewController*)controller
{
    UIView *view = controller.view;
    CGSize size = view.bounds.size;
    
    CGFloat y = size.height - (72+70)/2;
    
    UIFont *font = [UIFont defaultRegularFontWithSize:28/2];
    CGSize messageSize = [message sizeWithFont:font];
    CGFloat viewWidth = messageSize.width + (22/2)*2;
    
    CGFloat x = (size.width - viewWidth)/2;
    
    CGRect frameToast = CGRectMake(x, y, viewWidth, (20+32+20)/2);
    
    ToastView *tv = [[ToastView alloc]initWithFrame:frameToast];
    [tv setToastMessage:message];
    
    //    UIView *sv = window.rootViewController.view;
    
    [view addSubview:tv];
    //    [sv addSubview:tv];
    NSTimeInterval toastDuration = [ToastController toastDurationWithInputDuration:duration Message:message];

    
    [UIView animateWithDuration:toastDuration
                     animations:^{
                         tv.layer.opacity = 0;
                     }
                     completion:^(BOOL finished){
                         [tv removeFromSuperview];
                     }];

    
}


@end
