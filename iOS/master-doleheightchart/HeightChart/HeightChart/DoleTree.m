//
//  DoleTree.m
//  HeightChart
//
//  Created by ne on 2014. 3. 18..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "DoleInfo.h"
#import "DoleTree.h"

@implementation DoleTree


- (id)initWithFrame:(CGRect)frame doleMonkeyType:(enum DoleMonkeyType)type
{
    self = [super initWithFrame:frame];
    {
        self.monkeyType = type; 
    }
    return self; 
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
- (void)showTopTagWithAnimation
{
    assert(0);
}

@end
