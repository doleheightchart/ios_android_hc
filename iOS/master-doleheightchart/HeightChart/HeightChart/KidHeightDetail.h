//
//  KidHeightDetail.h
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 4..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ChildHeight.h"


@interface HeightDetailInfo : NSObject
@property (nonatomic) CGFloat kidHeight;
@property (nonatomic, retain) NSDate *takenDate;
@property (nonatomic, retain) UIImage *image;
@property (nonatomic) BOOL isChecked;
@property (nonatomic, retain) UIImage *imageFullSize;
@property (nonatomic, weak) ChildHeight *childHeight;

//- (id)initWithHeight:(CGFloat)kidHeight
//                date:(NSDate*)takenDate
//               image:(UIImage*)takenImage;

-(id)initWithChildHeight:(ChildHeight*)childHeight;
@end

@interface KidHeightDetail : NSObject

@property (nonatomic) CGFloat kidHeight;
@property (nonatomic, strong) NSMutableArray *takenHeightHistory;
@property (nonatomic) BOOL expanded;
@property (nonatomic, readonly) HeightDetailInfo *representDetail;

@end
