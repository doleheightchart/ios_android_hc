//
//  HeightDetailCell.h
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 5..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HeightDetailTreeProtocol.h"
#import "HeightDetailSticker.h"

@protocol HeightDetailCellDelegate <NSObject>
-(NSInteger)requestAge;
@end

@protocol HeightDetailAddCellDelegate<NSObject>

-(void)addHeight;

@end

@interface HeightDetailCell : UICollectionViewCell

@property (nonatomic) StickerDirection stickerdirection;
@property (nonatomic, strong) TopArrowSticker *topArrowSticker;
@property (nonatomic, weak) id<HeightDetailCellDelegate> detailCellDelegate;
@property (nonatomic) BOOL isAverageLine;

@property (nonatomic) BOOL isDeleteMode;

-(void)updateWithHeightDetail:(KidHeightDetail*)heightDetail kidHeight:(CGFloat)height;

-(void)setAllDelegate:(id)delegate;

-(BOOL)doAddNewAnimationWithDetailInfo:(HeightDetailInfo*)detailInfo;
-(void)doDeleteAnimationWithDetailInfoArray:(NSArray*)details;

@end

@interface LeftDetailCell : HeightDetailCell

@end

@interface RightDetailCell : HeightDetailCell

@end

@interface AddDetailCell : UICollectionViewCell
@property (nonatomic, weak) id<HeightDetailAddCellDelegate> addCellDelegate;
-(void)updateDeleteMode:(BOOL)isDeleteMode Animated:(BOOL)animated;
@end

@interface HeightZoomDetailCell : UICollectionViewCell
@property (nonatomic) CGFloat kidHeight;
@property (nonatomic, weak) id allDelegate;

-(void)updateHeightDetails:(NSArray*)heightDetails  kidHeight:(CGFloat)height;
-(void)setAllDelegate:(id)allDelegate;
-(void)checkAverageLineWithHeight:(CGFloat)kidHeight;
-(BOOL)doAddNewAnimationWithDetailInfo:(HeightDetailInfo*)detailInfo;
-(void)debug_setIndex:(NSUInteger)index;

@end

@interface AddZoomDetailCell : UICollectionViewCell
@property (nonatomic, weak) id<HeightDetailAddCellDelegate> addCellDelegate;
-(void)updateDeleteMode:(BOOL)isDeleteMode Animated:(BOOL)animated;
@end

#pragma mark CellAttributes

@interface FadeOutInAttributes : UICollectionViewLayoutAttributes

@property (nonatomic) CGFloat offSetScroll;
@end

