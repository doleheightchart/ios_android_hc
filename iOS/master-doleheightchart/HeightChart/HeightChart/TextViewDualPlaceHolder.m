//
//  TextViewDualPlaceHolder.m
//  HeightChart
//
//  Created by ne on 2014. 2. 24..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "TextViewDualPlaceHolder.h"
#import "UIView+ImageUtil.h"
#import "UIFont+DoleHeightChart.h"
#import "UIColor+ColorUtil.h"
#import "WarningCoverView.h"

@implementation TextViewDualPlaceHolder

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}


-(void)setDualPlaceHolderTextView:(enum DoleDualPlaceHolderTextType)textType
{
    self.backgroundColor = [UIColor clearColor];
    
//    UIEdgeInsets buttonEdgeInsets = {0, (43.0/2.0), 0, (43.0/2.0)};
    UIEdgeInsets buttonEdgeInsets = {0, (40.0/2.0), 0, (40.0/2.0)};
    
    UIImage *resizableImage = [UIImage resizableImageWithName:@"input" CapInsets:buttonEdgeInsets];
    
    NSString *imageName;
    NSString *firstPlaceHolderText = @"";
    NSString *secondPlaceHolderText = @"";
    
    
    
    BOOL useFrontImage = YES;
    
    self.holderType = textType;
    
    switch (textType) {
        case DoleDualPlaceHolderTextTypeNormal:
            firstPlaceHolderText = self.firstText;
            secondPlaceHolderText = self.secondText;
            useFrontImage = NO;
            break;
        case DoleDualPlaceHolderTextTypeBirth:
            imageName = @"input_birthday_icon";
            firstPlaceHolderText = NSLocalizedString(@"Birthday",@"");
            break;
        case DoleDualPlaceHolderTextTypeDisableEmail:
            imageName = @"input_mail_disabled_icon";
            firstPlaceHolderText = NSLocalizedString(@"Example@dole.com",@"");
            break;
        case DoleDualPlaceHolderTextTypeEmail:
            imageName = @"input_mail_icon";
            firstPlaceHolderText = NSLocalizedString(@"Example@dole.com",@"");
            break;
        case DoleDualPlaceHolderTextTypeNickName:
            imageName = @"input_nickname_icon";
            firstPlaceHolderText = NSLocalizedString(@"Nickname",@"");
            secondPlaceHolderText = NSLocalizedString(@"1~15charactor",@"");

            break;
        case DoleDualPlaceHolderTextTypePassword:
            imageName = @"input_password_icon";
            firstPlaceHolderText = NSLocalizedString(@"Password",@"");
            secondPlaceHolderText = NSLocalizedString(@"(at least 6 characters)",@"");
            break;
        case DoleDualPlaceHolderTextTypeAddress:
            imageName = @"input_adress_icon";
            firstPlaceHolderText = NSLocalizedString(@"StreetAdress",@"");
            secondPlaceHolderText = NSLocalizedString(@"including mail box number",@"");
            break;
        case DoleDualPlaceHolderTextTypeTopic:
            imageName = @"input_topic_icon";
            firstPlaceHolderText = NSLocalizedString(@"Title of inquiry",@"");
            secondPlaceHolderText = @"";
            break;
        case DoleDualPlaceHolderTextTypeCity:
            imageName = @"input_city_icon";
            firstPlaceHolderText = NSLocalizedString(@"City",@"");
            secondPlaceHolderText = @"";
            break;
        case DoleDualPlaceHolderTextTypeRecipientName:
            imageName = @"input_recipient_icon";
            firstPlaceHolderText = NSLocalizedString(@"Recipient name",@"");
            secondPlaceHolderText = @"";
            break;
        case DoleDualPlaceHolderTextTypePhone:
            imageName = @"input_phone_icon";
            firstPlaceHolderText = NSLocalizedString(@"Phone",@"");
            secondPlaceHolderText = NSLocalizedString(@"optional",@"");
            break;
            
    }
    
    self.backgroundImageView = [[UIImageView alloc]initWithImage:resizableImage];
    self.backgroundImageView.frame = CGRectMake(0, 0, self.frame.size.width , self.frame.size.height);

    if (useFrontImage){
        self.frontImageView = [[UIImageView alloc]initWithImage:[UIImage  imageNamed:imageName]];
        [self.frontImageView setContentMode:UIViewContentModeCenter];
        self.frontImageView.frame = CGRectMake(10, 5, 25, 25);
        
        self.textField = [[DualPlaceHolderTextField alloc]init];
//        self.textField.frame = CGRectMake(45, 3, 250, 30);
        self.textField.frame = CGRectMake(45, 3, 210, 30);
    }
    else{
        self.textField = [[DualPlaceHolderTextField alloc]init];
//        self.textField.frame = CGRectMake(13, 19/2, 213, 17);
        self.textField.frame = CGRectMake(13, 19/2, 218, 17);
    }
    
    
    [self addSubview:self.backgroundImageView];
    [self addSubview:self.frontImageView];
    [self addSubview:self.textField];
    
    
    [self.textField setReDrawPlaceHolder:firstPlaceHolderText SecondText:secondPlaceHolderText];
    [self.textField addTarget:self action:@selector(checkInput:) forControlEvents:UIControlEventEditingDidEnd];
//    [self.textField addTarget:self action:@selector(resignFirstResponder) forControlEvents:UIControlEventEditingDidEndOnExit];
//    self.textField addTarget:self action:nil forControlEvents:UIControlEven
    self.textField.font = [UIFont defaultBoldFontWithSize:30/2];
    self.textField.textColor = [UIColor colorWithRGB:0x606060];
    
    self.textField.delegate = self;
}

-(void)checkInput:(id)sender
{
    [self checkInputByHolderType];
}

-(BOOL)checkInputByHolderType
{
    switch (self.holderType) {
        case DoleDualPlaceHolderTextTypeNormal:
            break;
        case DoleDualPlaceHolderTextTypeBirth:
            break;
        case DoleDualPlaceHolderTextTypeDisableEmail:
            break;
        case DoleDualPlaceHolderTextTypeEmail: return [self checkIsValidEmail];
        case DoleDualPlaceHolderTextTypeNickName:return [self checkIsValidNickname];
        case DoleDualPlaceHolderTextTypePassword:return [self checkIsValidPassword];
        case DoleDualPlaceHolderTextTypeAddress:
            break;
        case DoleDualPlaceHolderTextTypeTopic:
            break;
        case DoleDualPlaceHolderTextTypeCity:
            break;
        case DoleDualPlaceHolderTextTypeRecipientName:
            break;
        case DoleDualPlaceHolderTextTypePhone:
            break;
    }
    
    return [self checkIsEmptyInput];
}


-(void)setFirstText:(NSString *)firstText
{
    _firstText = firstText;
    [self.textField setReDrawPlaceHolder:_firstText SecondText:_secondText];
}

-(void)setSecondText:(NSString *)secondText
{
    _secondText = secondText;
    [self.textField setReDrawPlaceHolder:_firstText SecondText:_secondText];
}

-(void)setText:(NSString*)text
{
    self.textField.text = text;
    
    [self.textField sendActionsForControlEvents:UIControlEventEditingDidEnd];
}

#pragma mark TextDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *changedText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (self.maxLength == 0) return TRUE;
    
    if (self.enableSpecialCharacter == NO){
        
        NSMutableCharacterSet *set = [[NSMutableCharacterSet alloc]init];
        [set formUnionWithCharacterSet:[[NSCharacterSet alphanumericCharacterSet] invertedSet]];
        [set removeCharactersInString:@"()"]; //닉네임머지에서 추가될수있다.
        
        
//        if ([changedText rangeOfCharacterFromSet:[[NSCharacterSet alphanumericCharacterSet] invertedSet]].location != NSNotFound) {
        if ([changedText rangeOfCharacterFromSet:set].location != NSNotFound) {
            // There are non-alphanumeric characters in the replacement string
            return NO;
        }
    }
    
    return (self.maxLength >= changedText.length);
    
    
    
}



@end
