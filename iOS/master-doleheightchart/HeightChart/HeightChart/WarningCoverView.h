//
//  WarningCoverView.h
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 14..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TextViewDualPlaceHolder.h"

typedef enum : NSUInteger {
    kInputFieldErrorTypeWrongEmail,
    kInputFieldErrorTypeWrongPassword,
    kInputFieldErrorTypeWrongEmailFormat,
    kInputFieldErrorTypeWrongAlreadyExistEmail,
    kInputFieldErrorTypeWrongPasswordFormat,
    kInputFieldErrorTypeWrongPasswordConfirm,
    kInputFieldErrorTypeWrongAlreadyExistNickname,
    kInputFieldErrorTypeWrongQRCodeFormat,
    kInputFieldErrorTypeWrongNickname,
    kInputFieldErrorTypeWrongContent,
} InputFieldErrorType;

@interface WarningCoverView : UIView

+(instancetype)addWarningToTargetView:(UIView*)target warningmessage:(NSString*)message;

+(instancetype)addWarningToTargetView:(UIView*)target warningtype:(InputFieldErrorType)waringType;

//+(instancetype)addWarningToTargetView:(UIView *)target NetworkErrorCode:(NSInteger)errorCode;
@end


@interface TextViewDualPlaceHolder (WarningCoverView)


//Check And AutoCover
-(BOOL)checkIsValidNickname;
-(BOOL)checkIsValidEmail;
-(BOOL)checkIsValidPassword;
-(BOOL)checkIsValidConfirmPasswordWithPassword:(NSString*)password;
//-(BOOL)checkIsValidDirectQRCodeWithMinimumLength:(NSUInteger)minLength;
-(BOOL)checkIsValidDirectQRCodeWithFixLength:(NSUInteger)length;
-(BOOL)checkIsEmptyInput;
-(BOOL)hasWarningCover;

@end

@interface UIViewController(NetworkError)

-(BOOL)queryErrorCode:(NSInteger)errorCode EmailView:(UIView*)emailView PasswordView:(UIView*)passwordView;

@end
