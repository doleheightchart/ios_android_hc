//
//  DoleTransition.m
//  TestPolyCodeServer
//
//  Created by Kim Sehyun on 2014. 3. 24..
//  Copyright (c) 2014년 Kim Sehyun. All rights reserved.
//

#define DEFAULT_SHADOW_OPACITY 0.25

#import "DoleTransition.h"
#import "MPAnimation.h"
#import <QuartzCore/QuartzCore.h>
#include <math.h>

static inline double mp_radians (double degrees) {return degrees * M_PI/180;}

@interface DoleTransition()

@end

@implementation DoleTransition

#pragma mark - Properties

@synthesize style = _style;
@synthesize foldShadowOpacity = _foldShadowOpacity;
@synthesize foldShadowColor = _foldShadowColor;

#pragma mark - init

- (id)initWithSourceView:(UIView *)sourceView destinationView:(UIView *)destinationView duration:(NSTimeInterval)duration style:(MPFoldStyle)style completionAction:(MPTransitionAction)action {
	self = [super initWithSourceView:sourceView destinationView:destinationView duration:duration timingCurve:(((style & MPFoldStyleUnfold) == MPFoldStyleUnfold)? UIViewAnimationCurveEaseIn : UIViewAnimationCurveEaseOut) completionAction:action];
	if (self)
	{
		_style = style;
		_foldShadowOpacity = DEFAULT_SHADOW_OPACITY;
		_foldShadowColor = [UIColor blackColor];
	}
	
	return self;
}

#pragma mark - Instance methods

- (void)perform:(void (^)(BOOL finished))completion
{
	CGRect bounds = [self calculateRect];
	CGFloat scale = [[UIScreen mainScreen] scale];
	
	if (![self isDimissing])
	{
		if ([self presentedControllerIncludesStatusBarInFrame])
			self.destinationView.bounds = CGRectMake(0, 0, bounds.size.width + bounds.origin.x, bounds.size.height + bounds.origin.y);
		else
			self.destinationView.bounds = (CGRect){CGPointZero, bounds.size};
	}
	
	if ([self isDimissing])
	{
		CGFloat x = self.destinationView.bounds.size.width - bounds.size.width;
		CGFloat y = self.destinationView.bounds.size.height - bounds.size.height;
		
		if (![self presentedControllerIncludesStatusBarInFrame])
			[self setRect:CGRectOffset([self rect], x, y)];
	}
	else if ([self presentedControllerIncludesStatusBarInFrame])
	{
	}
	
    UIEdgeInsets insets = UIEdgeInsetsMake(0, 0, 0, 0);
    
    CGRect sr = self.sourceView.bounds;
    CGRect dr = self.destinationView.bounds;
    
    
	// Create 4 images to represent 2 halves of the 2 views
	UIImage * srcCapture = [MPAnimation renderImageFromView:self.sourceView withRect:self.sourceView.bounds transparentInsets:insets];
	UIImage * dstCapture = [MPAnimation renderImageFromView:self.destinationView withRect:self.destinationView.bounds transparentInsets:insets];
	
	UIView *actingSource = [self sourceView]; // the view that is already part of the view hierarchy
	UIView *containerView = [actingSource superview];
	if (!containerView)
	{
		// in case of dismissal, it is actually the destination view since we had to add it
		// in order to get it to render correctly
		actingSource = [self destinationView];
		containerView = [actingSource superview];
	}
	[actingSource setHidden:YES];
	
	CATransform3D transform = CATransform3DIdentity;
	CALayer *srcLayer;
	CALayer *dstLayer;
	UIView *mainView;
//	CGFloat width = vertical? bounds.size.width : bounds.size.height;
//	CGFloat height = vertical? bounds.size.height/2 : bounds.size.width/2;
//	CGFloat upperHeight = roundf(height * scale) / scale; // round heights to integer for odd height
//	CGFloat lowerHeight = (height * 2) - upperHeight;
//	CALayer *firstJointLayer;
//	CALayer *secondJointLayer;
//	CALayer *perspectiveLayer;
	
	// view to hold all our sublayers
	CGRect mainRect = [containerView convertRect:self.rect fromView:actingSource];
	CGPoint center = (CGPoint){CGRectGetMidX(mainRect), CGRectGetMidY(mainRect)};
	if ([containerView isKindOfClass:[UIWindow class]])
		mainRect = [actingSource convertRect:mainRect fromView:nil];
	mainView = [[UIView alloc] initWithFrame:mainRect];
	mainView.backgroundColor = [UIColor clearColor];
	mainView.transform = actingSource.transform;
	[containerView insertSubview:mainView atIndex:0];
	if ([containerView isKindOfClass:[UIWindow class]])
	{
		[mainView.layer setPosition:center];
	}
	
	
	// This remains flat, and is the upper half of the destination view when moving forwards
	// It slides down to meet the bottom sleeve in the center
    
    int sssss = (int)_style;
    

    
    
    if (sssss == 7){
        

        
        
        dstLayer = [CALayer layer];
        //        dstLayer.frame = mainRect;
        dstLayer.frame = self.destinationView.bounds;
        [dstLayer setContents:(id)[dstCapture CGImage]];
        [mainView.layer addSublayer:dstLayer];
        
        srcLayer = [CALayer layer];
        //        srcLayer.frame = mainRect;
        srcLayer.frame = self.sourceView.bounds;
        [srcLayer setContents:(id)[srcCapture CGImage]];
        [mainView.layer addSublayer:srcLayer];
        
        
        ////
//        CGRect f = srcLayer.frame;
//        srcLayer.anchorPoint = CGPointMake(0, 1);
//        srcLayer.position = CGPointMake(0, 320);
//        f.origin.y += f.size.height/2;
//        f.origin.x -= f.size.width/2;
//        
////        srcLayer.frame = f;
//        srcLayer.transform = CATransform3DMakeRotation(mp_radians(90), 0, 0, 1);
        
    }
    else{
        srcLayer = [CALayer layer];
//        srcLayer.frame = mainRect;
        srcLayer.frame = self.sourceView.bounds;
        [srcLayer setContents:(id)[srcCapture CGImage]];
        [mainView.layer addSublayer:srcLayer];
        
        
        dstLayer = [CALayer layer];
//        dstLayer.frame = mainRect;
        dstLayer.frame = self.destinationView.bounds;
        [dstLayer setContents:(id)[dstCapture CGImage]];
        [mainView.layer addSublayer:dstLayer];
        
        dstLayer.opacity = 0.25;
        dstLayer.transform = CATransform3DMakeScale(0.75, 0.75, 1);
    }


//    CGRect rr = mainRect;
//    rr.origin.y += 200;
    

	
	
	
	
//	NSUInteger frameCount = ceilf(self.duration * 60); // we want 60 FPS
//	
//	
//    [UIView animateWithDuration:5 animations:^{
//            //mainView.transform = CGAffineTransformMakeScale(5, 5);
//        //srcLayer.transform = CATransform3DMakeScale(4, 3, 3);
//        
//
//        CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"opacity"];
//
//        [animation setToValue:[NSNumber numberWithDouble:0]];
//        [animation setFillMode:kCAFillModeForwards];
//        [animation setRemovedOnCompletion:NO];
//        [srcLayer addAnimation:animation forKey:nil];
//
//        
//    } completion:^(BOOL finished) {
//        [mainView removeFromSuperview];
//		[self transitionDidComplete];
//		if (completion)
//			completion(YES); // execute the completion block that was passed in
//
//    }];
    
    
    
    
	// Create a transaction (to group our animations with a single callback when done)
	[CATransaction begin];
	[CATransaction setValue:[NSNumber numberWithFloat:0.3] forKey:kCATransactionAnimationDuration];
	[CATransaction setValue:[CAMediaTimingFunction functionWithName:[self timingCurveFunctionName]] forKey:kCATransactionAnimationTimingFunction];
	[CATransaction setCompletionBlock:^{
		[mainView removeFromSuperview];
		[self transitionDidComplete];
		if (completion)
			completion(YES); // execute the completion block that was passed in
	}];
    
    
    if (sssss != 7){
    
        CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"opacity"];

        [animation setToValue:[NSNumber numberWithDouble:1]];
        [animation setFillMode:kCAFillModeForwards];
        [animation setRemovedOnCompletion:NO];
        [dstLayer addAnimation:animation forKey:nil];
        
        CABasicAnimation *animation2 = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
        
        [animation2 setToValue:[NSNumber numberWithDouble:1]];
        [animation2 setFillMode:kCAFillModeForwards];
        [animation2 setRemovedOnCompletion:NO];
        [dstLayer addAnimation:animation2 forKey:nil];
    
	}
    else{
        
        CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"opacity"];
        
        [animation setToValue:[NSNumber numberWithDouble:0]];
        [animation setFillMode:kCAFillModeForwards];
        [animation setRemovedOnCompletion:NO];
        [srcLayer addAnimation:animation forKey:nil];
        
        CABasicAnimation *animation2 = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
        
        [animation2 setToValue:[NSNumber numberWithDouble:0.75]];
        [animation2 setFillMode:kCAFillModeForwards];
        [animation2 setRemovedOnCompletion:NO];
        [srcLayer addAnimation:animation2 forKey:nil];

        
    }
	
//	// resize height of the 2 folding panels along a cosine curve.  This is necessary to maintain the 2nd joint in the center
//	// Since there's no built-in sine timing curve, we'll use CAKeyframeAnimation to achieve it
//	CAKeyframeAnimation *keyAnimation = [CAKeyframeAnimation animationWithKeyPath:vertical? @"bounds.size.height" : @"bounds.size.width"];
//	[keyAnimation setValues:[NSArray arrayWithArray:arrayHeight]];
//	[keyAnimation setFillMode:kCAFillModeForwards];
//	[keyAnimation setRemovedOnCompletion:NO];
//	[perspectiveLayer addAnimation:keyAnimation forKey:nil];
	
	// Dim the 2 folding panels as they fold away from us
	// The gradients create a crease effect between adjacent panels
//	CAKeyframeAnimation *keyAnimation = [CAKeyframeAnimation animationWithKeyPath:@"opacity"];
//	[keyAnimation setValues:[NSArray arrayWithArray:arrayShadow]];
//	[keyAnimation setFillMode:kCAFillModeForwards];
//	[keyAnimation setRemovedOnCompletion:NO];
//	[srcLayer addAnimation:keyAnimation forKey:nil];
	
//	keyAnimation = [CAKeyframeAnimation animationWithKeyPath:@"opacity"];
//	[keyAnimation setValues:[NSArray arrayWithArray:arrayShadow]];
//	[keyAnimation setFillMode:kCAFillModeForwards];
//	[keyAnimation setRemovedOnCompletion:NO];
//	[lowerFoldShadow addAnimation:keyAnimation forKey:nil];
//	
	

	
	// Commit the transaction
	[CATransaction commit];
}

#pragma mark - Class methods

+ (void)transitionFromViewController:(UIViewController *)fromController toViewController:(UIViewController *)toController duration:(NSTimeInterval)duration style:(MPFoldStyle)style completion:(void (^)(BOOL finished))completion
{
	DoleTransition *foldTransition = [[DoleTransition alloc] initWithSourceView:fromController.view destinationView:toController.view duration:duration style:style completionAction:MPTransitionActionNone];
	[foldTransition perform:completion];
}

+ (void)transitionFromView:(UIView *)fromView toView:(UIView *)toView duration:(NSTimeInterval)duration style:(MPFoldStyle)style transitionAction:(MPTransitionAction)action completion:(void (^)(BOOL finished))completion
{
	DoleTransition *foldTransition = [[DoleTransition alloc] initWithSourceView:fromView destinationView:toView duration:duration style:style completionAction:action];
	[foldTransition perform:completion];
}

+ (void)presentViewController:(UIViewController *)viewControllerToPresent from:(UIViewController *)presentingController duration:(NSTimeInterval)duration style:(MPFoldStyle)style completion:(void (^)(BOOL finished))completion
{
	DoleTransition *foldTransition = [[DoleTransition alloc] initWithSourceView:presentingController.view destinationView:viewControllerToPresent.view duration:duration style:style completionAction:MPTransitionActionNone];
	
    if ([viewControllerToPresent supportedInterfaceOrientations] != [presentingController supportedInterfaceOrientations]){
        NSLog(@"xxxxxxx----->>>>>");
    }
    
    foldTransition.srcOrientation = [viewControllerToPresent supportedInterfaceOrientations];
    foldTransition.dstOrientation = [presentingController supportedInterfaceOrientations];
    
	[foldTransition setPresentingController:presentingController];
	[foldTransition setPresentedController:viewControllerToPresent];
	
	[foldTransition perform:^(BOOL finished) {
		// under iPad for our fold transition, we need to be full screen modal (iPhone is always full screen modal)
		UIModalPresentationStyle oldStyle = [presentingController modalPresentationStyle];
		if (oldStyle != UIModalPresentationFullScreen && [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
			[presentingController setModalPresentationStyle:UIModalPresentationFullScreen];
		
		[presentingController presentViewController:viewControllerToPresent animated:NO completion:^{
			// restore previous modal presentation style
			if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
				[presentingController setModalPresentationStyle:oldStyle];
            
            
//            UIViewController *ppp = viewControllerToPresent.presentedViewController;
//            
//            assert(viewControllerToPresent.presentedViewController == presentingController);

		}];
        
		
		if (completion)
			completion(YES);
	}];
}

+ (void)dismissViewControllerFromPresentingController:(UIViewController *)presentingController duration:(NSTimeInterval)duration style:(MPFoldStyle)style completion:(void (^)(BOOL finished))completion
{
	UIViewController *src = [presentingController presentedViewController];
//    UIViewController *src = presentingController;
	if (!src)
		[NSException raise:@"Invalid Operation" format:@"dismissViewControllerFromPresentingController:direction:completion: can only be performed on a view controller with a presentedViewController."];
	
    UIViewController *dest = (UIViewController *)presentingController;
	
	// find out the presentation context for the presenting view controller
	while (YES)// (![src definesPresentationContext])
	{
		if (![dest parentViewController])
			break;
		
		dest = [dest parentViewController];
	}
	
	DoleTransition *foldTransition = [[DoleTransition alloc] initWithSourceView:src.view destinationView:dest.view duration:duration style:style completionAction:MPTransitionActionNone];
	[foldTransition setDismissing:YES];
	[foldTransition setPresentedController:src];
	[presentingController dismissViewControllerAnimated:NO completion:nil];
	[foldTransition perform:^(BOOL finished) {
		[dest.view setHidden:NO];
		if (completion)
			completion(YES);
	}];
}

@end

#pragma mark - UIViewController(DoleTransition)

@implementation UIViewController(DoleTransition)

- (void)presentViewController:(UIViewController *)viewControllerToPresent foldStyle:(MPFoldStyle)style completion:(void (^)(BOOL finished))completion
{
	[DoleTransition presentViewController:viewControllerToPresent from:self duration:[DoleTransition defaultDuration] style:style completion:completion];
}

- (void)dismissViewControllerWithFoldStyle:(MPFoldStyle)style completion:(void (^)(BOOL finished))completion
{
	[DoleTransition dismissViewControllerFromPresentingController:self duration:[DoleTransition defaultDuration] style:7 completion:completion];
}

@end

#pragma mark - UINavigationController(DoleTransition)

@implementation UINavigationController(DoleTransition)

//- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated
- (void)pushViewController:(UIViewController *)viewController foldStyle:(MPFoldStyle)style
{
	[DoleTransition transitionFromViewController:[self visibleViewController]
								  toViewController:viewController
										  duration:[DoleTransition defaultDuration]
											 style:style
										completion:^(BOOL finished) {
											[self pushViewController:viewController animated:NO];
										}
	 ];
}

- (UIViewController *)popViewControllerWithFoldStyle:(MPFoldStyle)style
{
	UIViewController *toController = [[self viewControllers] objectAtIndex:[[self viewControllers] count] - 2];
    
	[DoleTransition transitionFromViewController:[self visibleViewController]
								  toViewController:toController
										  duration:[DoleTransition defaultDuration]
											 style:7
										completion:^(BOOL finished) {
											[self popViewControllerAnimated:NO];
										}
	 ];
	
	return toController;
}

- (NSArray*)popToRootViewControllerWithFoldStyle:(MPFoldStyle)style
{
	UIViewController* toController = [[self viewControllers] objectAtIndex:0];	//to rootViewController
	
	NSMutableArray* popped = [NSMutableArray array];
	for(int i = 1; i < [[self viewControllers] count]; i++)
		[popped addObject:[[self viewControllers] objectAtIndex:i]];
	
	[DoleTransition transitionFromViewController:[self visibleViewController]
								  toViewController:toController
										  duration:[DoleTransition defaultDuration]
											 style:7
										completion:^(BOOL finished) {
											[self popToRootViewControllerAnimated:NO];
										}
	 ];
	
	return popped;
}

@end
