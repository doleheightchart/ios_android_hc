//
//  DoleSegue.h
//  TestPolyCodeServer
//
//  Created by Kim Sehyun on 2014. 3. 24..
//  Copyright (c) 2014년 Kim Sehyun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MPFoldEnumerations.h"

@interface DoleSegue : UIStoryboardSegue

@property (assign, nonatomic) MPFoldStyle style;

@end