//
//  DoleSegue.m
//  TestPolyCodeServer
//
//  Created by Kim Sehyun on 2014. 3. 24..
//  Copyright (c) 2014년 Kim Sehyun. All rights reserved.
//

#import "DoleSegue.h"

@implementation DoleSegue

@synthesize style = _style;

- (id)init
{
	self = [super init];
	if (self)
	{
		[self doInit];
	}
	
	return self;
}

- (id)initWithIdentifier:(NSString *)identifier source:(UIViewController *)source destination:(UIViewController *)destination
{
	self = [super initWithIdentifier:identifier source:source destination:destination];
	if (self)
	{
		[self doInit];
	}
	
	return self;
}

- (void)doInit
{
	_style = [self defaultStyle];
}

- (MPFoldStyle)defaultStyle
{
	return MPFoldStyleDefault;
}

@end
