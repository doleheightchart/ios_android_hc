//
//  NSDate+DoleHeightChart.m
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 8..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "NSDate+DoleHeightChart.h"

@implementation NSDate (DoleHeightChart)
-(NSString*)stringDefaultFormat
{
    NSDateFormatter *formmatter = [[NSDateFormatter alloc] init];
    formmatter.dateFormat = @"yy.MM.dd";
    return [formmatter stringFromDate:self];
}

-(NSString*)stringForHeightViewer
{
    NSDateFormatter *formmatter = [[NSDateFormatter alloc] init];
    //formmatter.dateFormat = @"yyyy.MM.dd";
    formmatter.dateFormat = @"yyyy-MM-dd";
    return [formmatter stringFromDate:self];
    
}

-(NSInteger)getYear
{
    NSCalendar *calendar = [[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *comp = [calendar components:NSCalendarUnitYear fromDate:self];
    NSInteger year = [comp year];

    return year;
}

-(NSInteger)getHours
{
    NSCalendar *calendar = [[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *comp = [calendar components:NSCalendarUnitYear|kCFCalendarUnitHour fromDate:self];
    NSInteger hours = [comp hour];
    
    return hours;

}

-(NSInteger)getMins{

    NSCalendar *calendar = [[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *comp = [calendar components:kCFCalendarUnitMinute|kCFCalendarUnitHour fromDate:self];
    NSInteger minute = [comp minute];

    return minute;
}

-(NSInteger)getHoursFromNow
{
    NSTimeInterval ti = [self timeIntervalSinceNow];
    
    return ti / (60.0 * 60.0);
}

-(NSInteger)getMinsFromNow
{
    NSTimeInterval ti = [self timeIntervalSinceNow];
    
    return ti / (60.0);
}

-(NSInteger)getGlobalAge
{
    return [self getGlobalAgeToDate:[NSDate date]];
}

-(NSInteger)getGlobalAgeToDate:(NSDate*)toDate
{
    NSDateComponents *comp = [[NSCalendar currentCalendar] components:NSYearCalendarUnit
                                                             fromDate:self
                                                               toDate:toDate
                                                              options:0];
    
    NSInteger age = MAX(0, comp.year);
    return age;
}



@end
