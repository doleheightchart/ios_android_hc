//
//  UIView+ImageUtil.m
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 2. 11..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "UIView+ImageUtil.h"
#import "UIFont+DoleHeightChart.h"
#import "UIColor+ColorUtil.h"
#import "UIViewController+DoleHeightChart.h"


//enum DoleButtonImageType {

#define kButtonImageName_Disable        @"button_disable"

//    DoleButtonImageTypeOrange,
#define kOrangeButtonImageName_Normal   @"button_normal"
#define kOrangeButtonImageName_Press    @"button_press"

//    DoleButtonImageTypeLightBlue,
#define kBlueButtonImageName_Normal     @"blue_button_normal"
#define kBlueButtonImageName_Press      @"blue_button_press"

//    DoleButtonImageTypeRed,
#define kRedButtonImageName_Normal      @"red_button_normal"
#define kRedButtonImageName_Press       @"red_button_press"


//    DoleButtonImageTypePopupOrange,
#define kRedPopupButtonImage_Normal  @"popup_ok_normal"
#define kRedPopupButtonImage_Press   @"popup_ok_press"

//    DoleButtonImageTypePopupRed,
#define kOrangePopupButtonImage_Normal     @"popup_cancel_normal"
#define kOrangePopupButtonImage_Press      @"popup_cancel_press"

#define kPopupDisableButtonImage        @"popup_btn_disable"

#define kFacebookButtonIamgeName_Normal @"invite_facebook_btn_normal"
#define kFacebookButtonIamgeName_press @"invite_facebook_btn_press"


//};


UIEdgeInsets ButtonImageEdgeInsetsArr[] = {
    // top, left, bottom, right point coordinate
    {0, (43.0/2.0), 0, (43.0/2.0)},
    {0, (43.0/2.0), 0, (43.0/2.0)},
    {0, (43.0/2.0), 0, (43.0/2.0)},
    {0, (10.0/2.0), 0, (10.0/2.0)},
    {0, (10.0/2.0), 0, (10.0/2.0)},
    {0, (74.0/2.0), 0, (126-74/2.0)}
};

//enum DoleCheckBoxImageType {
//    DoleCheckBoxImageTypeNormal,
//    DoleCheckBoxImageTypeMale,
//    DoleCheckBoxImageTypeFemale,
//    DoleCheckBoxImageTypeBoy,
//    DoleCheckBoxImageTypeGirl,
//};

UIImage* getResizableImage(NSString* imageNamed_, UIEdgeInsets insets_)
{
    UIImage *image = [UIImage imageNamed:imageNamed_];
    UIImage *resizableImage = [image resizableImageWithCapInsets:insets_ resizingMode:UIImageResizingModeTile];
    
    assert(resizableImage);
    return resizableImage;
}

@implementation UIImage (ImageUtil)

+(instancetype)resizableImageWithName:(NSString*)imageNamed CapInsets:(UIEdgeInsets)insets
{
    return getResizableImage(imageNamed, insets);
}


@end

@implementation UIImage (HeightDetail)

-(CGRect)frameCrop
{
    CGFloat width = 162;
    CGFloat height = 162;
    return CGRectMake( (640 - width) / 2.0,
                      98.0,
                      width,
                      height);
}

-(CGRect)frameCropFourInch
{
    CGFloat width = 196;
    CGFloat height = 196;
    
    return CGRectMake( (640 - width) / 2.0,
                      126.0,
                      width,
                      height);
}

-(instancetype)facecropImage
{
    //pixels
//    CGRect faceCropRect = CGRectMake((640 - 196)/2,
//                                     126,
//                                     196,
//                                     196);

    //points
    CGSize imageSize = self.size;
    CGFloat imageScale = self.scale;
    
    CGRect faceCropRectPixel;
    
    if ( (imageSize.height * imageScale) > 960){
        faceCropRectPixel = [self frameCropFourInch];
    }
    else{
        faceCropRectPixel = [self frameCrop];
    }
    
//    CGRect faceCropRect = CGRectMake(faceCropRectPixel.origin.x / imageScale,
//                                     faceCropRectPixel.origin.y / imageScale,
//                                     faceCropRectPixel.size.width / imageScale,
//                                     faceCropRectPixel.size.height / imageScale);
//    
//    CGRect faceCropRect2 = CGRectMake((640 - 196)/2/2*self.scale,
//                                     126/2*self.scale,
//                                     196/2*self.scale,
//                                     196/2*self.scale);

    
    CGImageRef imageRef = CGImageCreateWithImageInRect(self.CGImage, faceCropRectPixel);
    UIImage *cropImage = [UIImage imageWithCGImage:imageRef];
//    UIImage *cropImage = [UIImage imageWithCGImage:imageRef scale:2.0 orientation:UIImageOrientationUp];
    
    CGImageRelease(imageRef);
    
    return cropImage;
}

+(instancetype)maskedImage:(UIImage*)image make:(UIImage*)maskImage
{
    CGImageRef maskRef = maskImage.CGImage;
    
    CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(maskRef),
//    CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(image.CGImage),
                                        CGImageGetHeight(maskRef),
                                        CGImageGetBitsPerComponent(maskRef),
                                        CGImageGetBitsPerPixel(maskRef),
                                        CGImageGetBytesPerRow(maskRef),
                                        CGImageGetDataProvider(maskRef), NULL, false);
    
    CGImageRef maskedImageRef = CGImageCreateWithMask(image.CGImage, mask);
    UIImage *maskedImage = [UIImage imageWithCGImage:maskedImageRef];
//    UIImage *maskedImage = [UIImage imageWithCGImage:maskedImageRef scale:2.0 orientation:UIImageOrientationUp];

    CGImageRelease(mask);
    CGImageRelease(maskedImageRef);
    
    return maskedImage;
}

-(instancetype)facemaskImageWithDirection:(BOOL)isLeft
{
    NSString *maskImageName = isLeft ? @"detail_left_arrow_mask" : @"detail_right_arrow_mask";
    
    CGFloat xOffSet = isLeft ? -10.0 : 10.0;
    xOffSet /= self.scale;
    
    // capture image context ref
//    UIGraphicsBeginImageContextWithOptions(self.size, YES, self.scale);
    UIGraphicsBeginImageContextWithOptions(self.size, YES, 2);

    //Draw images onto the context
    
    [self drawInRect:CGRectMake(xOffSet, 0, self.size.width, self.size.height)];
    
    // assign context to new UIImage
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    //
    
    UIImage *maskImage = [UIImage imageNamed:maskImageName];
    
    // end context
    UIGraphicsEndImageContext();
    
    return [UIImage maskedImage:newImage make:maskImage];
}

+(instancetype)stickerImageFromPhoto:(UIImage*)photoImage Direction:(BOOL)isLeft;
{
//    UIImage *facecropImage = [photoImage facecropImage];
//    
//    return [facecropImage facemaskImageWithDirection:isLeft];
    

    
    return [photoImage facemaskImageWithDirection:isLeft];
}

+(instancetype)stickerExtendImageFromPhoto:(UIImage*)photoImage Direction:(BOOL)isLeft
{
    /*
    NSString *maskImageName = isLeft ? @"detail_left_arrow_over_mask" : @"detail_right_arrow_over_mask";
    
    UIImage *maskImage = [UIImage imageNamed:maskImageName];
    
    return [UIImage maskedImage:photoImage make:maskImage];
    */
    
    NSString *maskImageName = isLeft ? @"detail_left_arrow_over_mask" : @"detail_right_arrow_over_mask";
    
    UIImage *maskImage = [UIImage imageNamed:maskImageName];
    CGSize size = maskImage.size;
    UIGraphicsBeginImageContextWithOptions(size, YES, 2);
    
    //Draw images onto the context
    
    [photoImage drawInRect:CGRectMake(0, 0, size.width, size.height)];
    
    // assign context to new UIImage
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    // end context
    UIGraphicsEndImageContext();
    
    return [UIImage maskedImage:newImage make:maskImage];
}

+(instancetype)summaryFaceImage:(UIImage*)photoImage
{
    /*
    UIImage *maskImage = [UIImage imageNamed:@"summary_sticker_mask"];
    return [UIImage maskedImage:photoImage make:maskImage];
    */
    
    UIImage *maskImage = [UIImage imageNamed:@"summary_sticker_mask"];
    CGSize size = maskImage.size;
    UIGraphicsBeginImageContextWithOptions(size, YES, 2);
    
    //Draw images onto the context
    
    [photoImage drawInRect:CGRectMake(0, 0, size.width, size.height)];
    
    // assign context to new UIImage
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    //
    
    //UIImage *maskImage = [UIImage imageNamed:maskImageName];
    
    // end context
    UIGraphicsEndImageContext();

    return [UIImage maskedImage:newImage make:maskImage];
}

+(instancetype)imageName:(NSString*)stickerImageName
               kidHeight:(CGFloat)kidHeight
                 leftTop:(CGPoint)leftTop
{
    UIImage *stickerImage = [UIImage imageNamed:stickerImageName];
    
    if (leftTop.x == -1 && leftTop.y == -1) {
        
        return stickerImage;
    }
    
    // Create a new context of the desired size to render the image
//	UIGraphicsBeginImageContextWithOptions(stickerImage.size, NO, stickerImage.scale);
    UIGraphicsBeginImageContextWithOptions(stickerImage.size, NO, 2);

//    UIGraphicsBeginImageContextWithOptions(CGSizeMake(400, 400), NO, 1);
    
	CGContextRef context = UIGraphicsGetCurrentContext();
	
	// Translate it, to the desired position
	CGContextTranslateCTM(context, 0, 0);
    CGContextClearRect(context, CGRectMake(0, 0, stickerImage.size.width, stickerImage.size.height));
//    CGContextClearRect(context, CGRectMake(0, 0, stickerImage.size.width, stickerImage.size.height));
//    CGContextBeginTransparencyLayer(context, NULL);
    CGContextSetRGBStrokeColor(context, 1, 0, 0, 1);
    CGContextSetRGBFillColor(context, 1, 1, 1, 1);
    // Render the view as image
//    CGContextFillRect(context, CGRectMake(0, 0, stickerImage.size.width, stickerImage.size.height));
    [stickerImage drawAtPoint:CGPointZero];
    NSString *heightText = [NSString stringWithFormat:@"%.1f", kidHeight, nil];
    
    CGFloat margin2Length = heightText.length < 5 ? 5 : 0;
    
    //[heightText drawAtPoint:leftTop withFont:[UIFont defaultBoldFontWithSize:48/2]];
    CGRect r = CGRectMake(leftTop.x + margin2Length, leftTop.y - 2 , 128/2, 50/2);
    
//    CGContextFillRect(context, r);
//    CGContextSetRGBFillColor(context, 1, 0, 0, 1);

    [heightText drawInRect:r withFont:[UIFont defaultMediumFontWithSize:48/2]];
    
    // Fetch the image
    UIImage *renderedImage = UIGraphicsGetImageFromCurrentImageContext();
    
    // Cleanup
//    CGContextEndTransparencyLayer(context);
    UIGraphicsEndImageContext();
    
//    CGContextBeginTransparencyLayer (myContext, NULL);// 4
//    // Your drawing code here// 5
//    CGContextSetRGBFillColor (myContext, 0, 1, 0, 1);
//    CGContextFillRect (myContext, CGRectMake (wd/3+ 50,ht/2 ,wd/4,ht/4));
//    CGContextSetRGBFillColor (myContext, 0, 0, 1, 1);
//    CGContextFillRect (myContext, CGRectMake (wd/3-50,ht/2-100,wd/4,ht/4));
//    CGContextSetRGBFillColor (myContext, 1, 0, 0, 1);
//    CGContextFillRect (myContext, CGRectMake (wd/3,ht/2-50,wd/4,ht/4));
//    CGContextEndTransparencyLayer (myContext);
    
    return renderedImage;
}


@end


@implementation UIButton (ImageUtil)

-(void)setResizableImageWithType:(enum DoleButtonImageType)buttonType
{
    NSString *normalImageName;
    NSString *pressImageName;
    NSString *disableImageName;
    
    switch (buttonType) {
        case DoleButtonImageTypeOrange:
            normalImageName = kOrangeButtonImageName_Normal;
            pressImageName = kOrangeButtonImageName_Press;
            disableImageName = kButtonImageName_Disable;
            break;
        case DoleButtonImageTypeLightBlue:
            normalImageName = kBlueButtonImageName_Normal;
            pressImageName = kBlueButtonImageName_Press;
            disableImageName = kButtonImageName_Disable;
            break;
        case DoleButtonImageTypeRed:
            normalImageName = kRedButtonImageName_Normal;
            pressImageName = kRedButtonImageName_Press;
            disableImageName = kButtonImageName_Disable;
            break;
        case DoleButtonImageTypePopupOrange:
            normalImageName = kOrangePopupButtonImage_Normal;
            pressImageName = kOrangePopupButtonImage_Press;
            disableImageName = kPopupDisableButtonImage;
            break;
        case DoleButtonImageTypePopupRed:
            normalImageName = kRedPopupButtonImage_Normal;
            pressImageName = kRedPopupButtonImage_Press;
            disableImageName = kPopupDisableButtonImage;
            break;
        case DoleButtonImageTypeFacebook:
            normalImageName = kFacebookButtonIamgeName_Normal;
            pressImageName = kFacebookButtonIamgeName_press;
            disableImageName = kButtonImageName_Disable;
            break;
        default:
            break;
    }
    
    UIEdgeInsets buttonEdgeInsets = ButtonImageEdgeInsetsArr[buttonType];
    UIImage *normalImage = getResizableImage(normalImageName, buttonEdgeInsets);
    UIImage *pressImage = getResizableImage(pressImageName, buttonEdgeInsets);
    UIImage *disableImage = getResizableImage(disableImageName, buttonEdgeInsets);
    
    [self setBackgroundImage:normalImage forState:UIControlStateNormal];
    [self setBackgroundImage:pressImage forState:UIControlStateHighlighted];
    [self setBackgroundImage:disableImage forState:UIControlStateDisabled];
    
    self.adjustsImageWhenHighlighted = NO;
    self.backgroundColor = [UIColor clearColor];
    
    if ([self.currentLanguageCode isEqualToString:@"ja"]) {
        self.titleLabel.font = [UIFont defaultBoldFontWithSize:30/2];
    }
    else{
        self.titleLabel.font = [UIFont defaultBoldFontWithSize:34/2];
    }
    

    
}

-(void)setResizableCheckBoxImageWithType:(enum DoleCheckBoxImageType)checkboxType
{
    self.adjustsImageWhenHighlighted = NO;
    
    UIImage *img1 = [UIImage imageNamed:@"check_box"];
    UIImage *img2 = [UIImage imageNamed:@"check_box_off"];
    UIImage *img3 = [UIImage imageNamed:@"check_box_on"];
    
    [self setBackgroundImage:img2 forState:UIControlStateNormal];
    [self setBackgroundImage:img3 forState:UIControlStateHighlighted];
    [self setBackgroundImage:img3 forState:UIControlStateSelected];
    [self setBackgroundImage:img2 forState:UIControlStateDisabled];
    [self setBackgroundImage:img1 forState:UIControlStateApplication];
    [self setBackgroundImage:img1 forState:UIControlStateReserved];

    

}

-(void)makeDefaultUnderlineStyle
{
    UIColor *normalColor = [UIColor colorWithRGB:0x11a6b4];
    UIColor *selectColor= [UIColor colorWithRGB:0x9453e4];
    [self makeUnderlineStyleWithSelectedColor:selectColor normalColor:normalColor];
}


-(void)makeUnderlineStyleWithSelectedColor:(UIColor*)selectedColor normalColor:(UIColor*)normalColor
{
    self.backgroundColor = [UIColor clearColor];
    self.adjustsImageWhenHighlighted = NO;

    NSNumber *strokeWidth = @-2.0;

    NSDictionary *normalAttrDic = @{NSForegroundColorAttributeName: normalColor,
                                    NSUnderlineStyleAttributeName:@1,
                                    NSStrokeWidthAttributeName:strokeWidth};

    NSAttributedString *normalTitle = [[NSAttributedString alloc] initWithString:self.titleLabel.text attributes:normalAttrDic];

    NSDictionary *selectedAttrDic = @{NSForegroundColorAttributeName: selectedColor,
                                      NSUnderlineStyleAttributeName:@1,
                                      NSStrokeWidthAttributeName:strokeWidth};

    NSAttributedString *selectedTitle = [[NSAttributedString alloc] initWithString:self.titleLabel.text attributes:selectedAttrDic];
    
    NSDictionary *disableAttrDic = @{NSForegroundColorAttributeName: [UIColor grayColor],
                                      NSUnderlineStyleAttributeName:@1,
                                      NSStrokeWidthAttributeName:strokeWidth};
    
    NSAttributedString *disableTitle = [[NSAttributedString alloc] initWithString:self.titleLabel.text attributes:disableAttrDic];

    [self setAttributedTitle:normalTitle forState:UIControlStateNormal];
    [self setAttributedTitle:selectedTitle forState:UIControlStateSelected];
    [self setAttributedTitle:selectedTitle forState:UIControlStateHighlighted];
    [self setAttributedTitle:disableTitle forState:UIControlStateDisabled];
}

+(UIButton*) commonCancelButtonWithTarget:(id)target action:(SEL)selector
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];

    UIImage *noraml = [UIImage imageNamed:@"cancel_btn_normal"];
    UIImage *press = [UIImage imageNamed:@"cancel_btn_press"];

    [button setBackgroundImage:noraml forState:UIControlStateNormal];
    [button setBackgroundImage:press forState:UIControlStateSelected];
    [button setBackgroundImage:press forState:UIControlStateHighlighted];

    [button addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];

    return button;
}

+(UIButton*) cameraCancelButtonWithTarget:(id)target action:(SEL)selector
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    
    UIImage *noraml = [UIImage imageNamed:@"camera_btn_cancel_normal"];
    UIImage *press = [UIImage imageNamed:@"camera_btn_cancel_press"];
    
    [button setBackgroundImage:noraml forState:UIControlStateNormal];
    [button setBackgroundImage:press forState:UIControlStateSelected];
    [button setBackgroundImage:press forState:UIControlStateHighlighted];
    
    [button addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    
    return button;
}

@end

@implementation UIImageView (ImageUtil)

-(void)setResizableImageWithName:(NSString*)imageNamed
{
    
}

@end

@implementation UIViewController (ImageUtil)

//-(void)closeMe:(id)sender
//{
//    if (self.navigationController)
//        [self.navigationController popViewControllerAnimated:YES];
//    else
//        [self dismissViewControllerAnimated:YES completion:nil];
//}
//
//-(UIButton*)addCommonCloseButton
//{
//    UIButton *button = [UIButton commonCancelButtonWithTarget:self action:@selector(closeMe:)];
////    button.frame = CGRectMake(269, 4, 44, 44);
////    self.view.bounds;
//    button.frame = CGRectMake(self.view.bounds.size.width - (88+14)/2,
//                              (8/2),
//                              44,
//                              44);
//    
//    [self.view addSubview:button];
//
//    return button;
//}
//
////- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField;        // return NO to disallow editing.
//- (void)textFieldDidBeginEditing:(UITextField *)textField           // became first responder
//{
//    [UIView animateWithDuration:1 animations:^{
//        
//        CGRect frameThis = self.view.frame;
//        frameThis.origin.y -= 200;
//        self.view.frame = frameThis;
//    }];
//}
////- (BOOL)textFieldShouldEndEditing:(UITextField *)textField;          // return YES to allow editing to stop and to resign first responder status. NO to disallow the editing session to end
//- (void)textFieldDidEndEditing:(UITextField *)textField             // may be called if forced even if shouldEndEditing returns NO (e.g. view removed from window) or endEditing:YES called
//{
//    [UIView animateWithDuration:1 animations:^{
//        CGRect frameThis = self.view.frame;
//        frameThis.origin.y += 200;
//        self.view.frame = frameThis;
//    }];
//}


@end

@implementation UILabel (ImageUtil)

+(UILabel*) commonDefalutLabel
{
    UILabel *label = [[UILabel alloc]init];
    label.frame = CGRectMake(0, 0, 528/2, 34/2);
    label.textColor = [UIColor colorWithRGB:0x606060];
    label.textAlignment = NSTextAlignmentLeft;
    label.font = [UIFont defaultFondByLanguageWithSize:26/2];
    return label;
}

+(UILabel*) commonBoldLabel
{
    UILabel *label = [[UILabel alloc]init];
    label.frame = CGRectMake(0, 0, 528/2, 40/2);
    label.textColor = [UIColor colorWithRGB:0x606060];
    label.textAlignment = NSTextAlignmentLeft;
    label.font = [UIFont defaultBoldFontWithSize:32/2];
    return label;
}

@end




