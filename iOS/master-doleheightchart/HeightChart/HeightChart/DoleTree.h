//
//  DoleTree.h
//  HeightChart
//
//  Created by ne on 2014. 3. 18..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DoleTreeInfo : NSObject

@property (nonatomic, strong) NSString *nickName;
@property (nonatomic, assign) NSInteger index;
@property (nonatomic, assign) CGFloat height;
@property (nonatomic, assign) enum DoleMonkeyType type;


@end

@interface DoleTree : UIView 

@property (nonatomic) enum DoleMonkeyType monkeyType;


- (id)initWithFrame:(CGRect)frame doleMonkeyType:(enum DoleMonkeyType)type;
- (void)showTopTagWithAnimation;

@end
