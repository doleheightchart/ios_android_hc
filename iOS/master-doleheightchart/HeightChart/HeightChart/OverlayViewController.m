//
//  OverlayViewController.m
//  OverlayTest
//
//  Created by mytwoface on 2014. 2. 6..
//  Copyright (c) 2014년 mytwoface. All rights reserved.
//

#import "OverlayViewController.h"

@interface OverlayViewController ()

@property (nonatomic, weak) UIWindow *oldKeyWindow;
@property (nonatomic, strong) UIWindow *modalWindow;
#ifdef __IPHONE_7_0
@property (nonatomic, assign) UIViewTintAdjustmentMode oldTintAdjustmentMode;
#endif



@end

@implementation OverlayViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareModal
{
    self.oldKeyWindow = [[UIApplication sharedApplication] keyWindow];
#ifdef __IPHONE_7_0
    if ([self.oldKeyWindow respondsToSelector:@selector(setTintAdjustmentMode:)]) { // for iOS 7
        self.oldTintAdjustmentMode = self.oldKeyWindow.tintAdjustmentMode;
        self.oldKeyWindow.tintAdjustmentMode = UIViewTintAdjustmentModeDimmed;
    }
#endif

//    if (!self.alertWindow) {
//        UIWindow *window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
//        window.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
//        window.opaque = NO;
//        window.windowLevel = UIWindowLevelSIAlert;
//        window.rootViewController = viewController;
//        self.alertWindow = window;
//    }
//    [self.alertWindow makeKeyAndVisible];

        UIWindow *window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
        window.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        window.opaque = NO;
        self.modalWindow = window;

}

-(void)releaseModal
{
//    [self.containerView removeFromSuperview];
//    self.containerView = nil;
//    self.titleLabel = nil;
//    self.messageLabel = nil;
//    [self.buttons removeAllObjects];
//    [self.alertWindow removeFromSuperview];
//    self.alertWindow = nil;
//    self.layoutDirty = NO;

//    [self.modalWindow removeFromSuperview];
//    self.modalWindow = nil;

    UIWindow *window = self.oldKeyWindow;
#ifdef __IPHONE_7_0
    if ([window respondsToSelector:@selector(setTintAdjustmentMode:)]) {
        window.tintAdjustmentMode = self.oldTintAdjustmentMode;
    }
#endif
    if (!window) {
        window = [UIApplication sharedApplication].windows[0];
    }
    [window makeKeyWindow];
    window.hidden = NO;
    
    [self.modalWindow removeFromSuperview];
    self.modalWindow = nil;
}

-(void)showModalOnTheViewController:(id)parentViewController
{
    [self prepareModal];

//    _parentViewController = (UIViewController*)parentViewController;

    self.modalWindow.rootViewController = self;
    [self.modalWindow makeKeyAndVisible];
}



@end
