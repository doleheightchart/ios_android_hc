//
//  SignInViewController.h
//  HeightChart
//
//  Created by ne on 2014. 2. 11..
//  Copyright (c) 2014년 ne. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignInViewController : UIViewController<UITextFieldDelegate>

@property (nonatomic) BOOL appFirstTimeLogIn;

@property (nonatomic, copy) void(^requireLoginCompletion)(BOOL isLoggedIn);

@end
