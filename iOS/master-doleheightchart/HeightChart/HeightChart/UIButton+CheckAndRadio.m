//
//  UIButton+CheckAndRadio.m
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 10..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "UIButton+CheckAndRadio.h"

@implementation UIButton (CheckAndRadio)
-(void)makeUICheckBox
{
    self.adjustsImageWhenHighlighted = NO;
    self.backgroundColor = [UIColor clearColor];
    
//    [self setBackgroundImage:[UIImage imageNamed:@"check_box_off"] forState:UIControlStateNormal];
    [self setImage:[UIImage imageNamed:@"check_box_off"] forState:UIControlStateNormal];
    [self setImage:[UIImage imageNamed:@"check_box_on"] forState:UIControlStateSelected];
    
//    self.bounds = CGRectMake(0, 0, 40, 40);
    
    [self addTarget:self action:@selector(clickSelf:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)makeUICheckBoxWhite
{
    self.adjustsImageWhenHighlighted = NO;
    self.backgroundColor = [UIColor clearColor];

    [self setBackgroundImage:[UIImage imageNamed:@"login_check_off"] forState:UIControlStateNormal];
    [self setBackgroundImage:[UIImage imageNamed:@"login_check_on"] forState:UIControlStateSelected];

    [self addTarget:self action:@selector(clickSelf:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)clickSelf:(id)sender
{
    self.selected = !self.selected;
    
    if (self.selected) {
        NSLog(@"Yes Selected");
    }
    else{
        NSLog(@"No Selected");
    }
}

-(void)makeUIRadioBox
{ 
    self.adjustsImageWhenHighlighted = NO;
    self.backgroundColor = [UIColor clearColor];
//    [self setBackgroundImage:[UIImage imageNamed:@"radio"] forState:UIControlStateNormal];
    [self setImage:[UIImage imageNamed:@"radio_off"] forState:UIControlStateNormal];
    [self setImage:[UIImage imageNamed:@"radio_on"] forState:UIControlStateSelected];

//    self.bounds = CGRectMake(0, 0, 50, 50);

    [self addTarget:self action:@selector(clickRadioSelf:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)bindinRadioBox:(UIButton*)otherButton
{
    [otherButton addTarget:self action:@selector(clickedOtherRadio:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)clickRadioSelf:(id)sender
{
    self.selected = YES;
}

-(void)clickedOtherRadio:(id)sender
{
    self.selected = NO;
}
+(void)makeUIRadioButtons:(UIButton*)firstButton secondButton:(UIButton*)secondButton
{
    [firstButton makeUIRadioBox];
    [secondButton makeUIRadioBox];
    
    [firstButton bindinRadioBox:secondButton];
    [secondButton bindinRadioBox:firstButton];
    
}

-(void)setupSizeWhenPressed
{
    [self addTarget:self action:@selector(resizeSmall:) forControlEvents:UIControlEventTouchDown];
    [self addTarget:self action:@selector(resizeOrgSize:) forControlEvents:UIControlEventTouchUpInside];
    [self addTarget:self action:@selector(resizeOrgSize:) forControlEvents:UIControlEventTouchUpOutside];
//    [self addTarget:self action:@selector(resizeOrgSize:) forControlEvents:UIControlEventTouchCancel];
}

-(void)resizeSmall:(UIButton*)button
{
    button.transform = CGAffineTransformMakeScale(0.9, 0.9);
    
    return;

////    CGSize size = button.bounds.size;
//    
//    
    [UIView animateWithDuration:0.3 animations:^{
//        button.bounds = CGRectMake(0, 0, size.width * 0.5, size.height * 0.5);
//        button.tintColor = [UIColor blackColor];
        
        button.transform = CGAffineTransformMakeScale(0.9, 0.9);
        
    } completion:^(BOOL finished) {
//        button.transform = CGAffineTransformMakeScale(1, 1);
        //button.bounds = CGRectMake(0, 0, size.width, size.height);
        
        button.transform = CGAffineTransformMakeScale(0.9, 0.9);

    }];
    
//    [UIView animateKeyframesWithDuration:0.5 delay:0 options:(UIViewKeyframeAnimationOptions) animations:^{
//        
//    } completion:^(BOOL finished) {
//        
//    }];
}

-(void)resizeOrgSize:(UIButton*)button
{
    button.transform = CGAffineTransformScale(button.transform, 1/0.9, 1/0.9);
    
//    CGSize size = button.bounds.size;
//    button.bounds = CGRectMake(0, 0, size.width*2, size.height*2);
}

-(void)inflateBounds
{
    CGSize size = self.bounds.size;
    self.bounds = CGRectMake(0, 0, size.width*1.5, size.height*1.5);
}

@end
