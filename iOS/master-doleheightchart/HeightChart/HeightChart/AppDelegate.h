//
//  AppDelegate.h
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 21..
//  Copyright (c) 2014년 Kim Sehyun. All rights reserved.
//

#import <FacebookSDK/FacebookSDK.h>
#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "UserConfig.h"
#import "FacebookManager.h"
#import <AVFoundation/AVFoundation.h>
//#import <CAR/CARController.h>


#define APP_HANDLED_URL @"APP_HANDLED_URL"
//#define APP_iCLOUD_APPID @"FGUL65Q4E4.com.dole.HeightChart"
//#define APP_iCLOUD_APPID @"74B55D7B22.com.dole.HeightChart"
#define APP_iCLOUD_APPID @"HeightChartContainer"//@"com.dole.HeightChart"


@interface AppDelegate : UIResponder <UIApplicationDelegate , AVAudioPlayerDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (nonatomic, retain, readonly) NSManagedObjectModel *managedObjectModel;
//@property (nonatomic, retain, readonly) NSManagedObjectContext *managedObjectContext;
//@property (nonatomic, retain, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (nonatomic, retain, readonly) NSManagedObjectContext *localManagedObjectContext;
@property (nonatomic, retain, readonly) NSPersistentStoreCoordinator *localPersistentStoreCoordinator;

@property (nonatomic, retain, readonly) NSManagedObjectContext *cloudManagedObjectContext;
@property (nonatomic, retain, readonly) NSPersistentStoreCoordinator *cloudPersistentStoreCoordinator;

@property (nonatomic, readonly) NSManagedObjectContext *currentObjectContext;

@property (nonatomic, retain, readonly) UserConfig *userConfig;
@property (nonatomic, retain) NSURL *openedURL;

@property (nonatomic, retain) FacebookManager* facebookManager;
@property (nonatomic, retain) AVAudioPlayer *player;

@property (nonatomic, strong) NSPersistentStore *iCloudStore;
@property (nonatomic) BOOL deleteAccountMode;

-(void) playBackMusic;
-(void) stopBackMusic; 
-(void) pauseBackMusic;

-(void)saveContext;
-(NSURL *)applicationDocumentsDirectory;


-(void)removeCloudProperties;
//@property (strong, nonatomic) FBSession *session;

@end
