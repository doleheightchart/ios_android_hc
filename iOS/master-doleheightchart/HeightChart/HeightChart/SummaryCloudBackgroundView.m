//
//  SummaryCloudBackgroundView.m
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 13..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "SummaryCloudBackgroundView.h"
#import "MyAnimationTime.h"

@interface SummaryCloudBackgroundView (){
    UIImageView     *_cloud1_1;
    UIImageView     *_cloud1_2;
    UIImageView     *_cloud2_1;
    UIImageView     *_cloud2_2;
    UIImageView     *_cloud3_1;
    UIImageView     *_cloud3_2;
    UIImageView     *_cloud4_1;
    UIImageView     *_cloud4_2;
    
//    CGRect          _frameOffScreen_Cloud1;
//    CGRect          _frameOffScreen_Cloud2;
//    CGRect          _frameOffScreen_Cloud3;
}

@end

@implementation SummaryCloudBackgroundView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

-(void)awakeFromNib
{
    [self initialize];
}

-(void)initialize
{
    self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"sky_bg"]];
    
    [self addClouds];
}

//-(void)addClouds
//{
//    CGFloat w = self.bounds.size.width;
//    
//    _cloud1_1 = [[UIImageView alloc]initWithFrame:CGRectMake(74/2, (164/2), 162/2, 88/2)];
//    _cloud1_1.image = [UIImage imageNamed:@"cloud_land_01"];
//    [self addSubview:_cloud1_1];
//    
//    _frameOffScreen_Cloud1 = CGRectMake(-(w-(74/2)), (164/2), 162/2, 88/2);
//    _cloud1_2 = [[UIImageView alloc]initWithFrame:_frameOffScreen_Cloud1];
//    _cloud1_2.image = [UIImage imageNamed:@"cloud_land_01"];
//    [self addSubview:_cloud1_2];
//    
//    
//    _cloud2_1 = [[UIImageView alloc]initWithFrame:CGRectMake((74+162+132)/2, 94/2, 112/2, 62/2)];
//    _cloud2_1.image = [UIImage imageNamed:@"cloud_land_02"];
//    [self addSubview:_cloud2_1];
//    
//    _frameOffScreen_Cloud2 = CGRectMake(-(w-(74+162+132)/2), 94/2, 112/2, 62/2);
//    _cloud2_2 = [[UIImageView alloc]initWithFrame:_frameOffScreen_Cloud2];
//    _cloud2_2.image = [UIImage imageNamed:@"cloud_land_02"];
//    [self addSubview:_cloud2_2];
//    
//    _cloud3_1 = [[UIImageView alloc]initWithFrame:CGRectMake(454, 143, 178/2, 86/2)];
//    _cloud3_1.image = [UIImage imageNamed:@"cloud_land_03"];
//    [self addSubview:_cloud3_1];
//    
//    _frameOffScreen_Cloud3 = CGRectMake(-114, 143, 178/2, 86/2);
//    _cloud3_2 = [[UIImageView alloc]initWithFrame:_frameOffScreen_Cloud3];
//    _cloud3_2.image = [UIImage imageNamed:@"cloud_land_03"];
//    [self addSubview:_cloud3_2];
//}

-(void)addClouds
{
    CGFloat w = self.bounds.size.width;
    
    _cloud1_1 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"cloud_land_01"]];
    _cloud1_2 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"cloud_land_01"]];
    [self addSubview:_cloud1_1];
    [self addSubview:_cloud1_2];

    _cloud2_1 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"cloud_land_02"]];
    _cloud2_2 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"cloud_land_02"]];
    [self addSubview:_cloud2_1];
    [self addSubview:_cloud2_2];

    _cloud3_1 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"cloud_land_03"]];
    _cloud3_2 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"cloud_land_03"]];
    [self addSubview:_cloud3_1];
    [self addSubview:_cloud3_2];

    _cloud4_1 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"cloud_land_04"]];
    _cloud4_2 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"cloud_land_04"]];
    [self addSubview:_cloud4_1];
    [self addSubview:_cloud4_2];

    NSArray *cloudArray = @[_cloud1_1, _cloud2_1, _cloud3_1, _cloud4_1, _cloud1_2, _cloud2_2, _cloud3_2, _cloud4_2];
    
    CGFloat posX = 270/2/2;
    CGFloat posY = 98/2;
    
    for (int i = 0; i < cloudArray.count; i++) {
        UIView *cloud = cloudArray[i];
        CGRect bound = cloud.bounds;
        posX += bound.size.width;
        CGFloat left = w - posX;
        
        if ( (i % 2) ){
            posY = (98+76+116)/2;
        }
        else{
            posY = 98/2;
        }
        
        CGFloat top = posY;
        
        CGRect frame = CGRectMake(left, top, bound.size.width, bound.size.height);
        cloud.frame = frame;
        

        
        posX += (270/2);
    }
  
}

-(void)startScrolling
{
//    CGFloat currentDeviceWidth = self.bounds.size.width;
//    CGFloat currentDeviceWidth = 568; //points
    CGFloat currentDeviceWidth = 1658/2; //points
    
    
    [UIView animateWithDuration:kDuration_Cloud delay:0 options:UIViewAnimationOptionCurveLinear|UIViewAnimationOptionRepeat animations:^{
        
        _cloud1_1.center = CGPointMake(_cloud1_1.center.x + currentDeviceWidth, _cloud1_1.center.y);
        _cloud1_2.center = CGPointMake(_cloud1_2.center.x + currentDeviceWidth, _cloud1_2.center.y);
        
        _cloud2_1.center = CGPointMake(_cloud2_1.center.x + currentDeviceWidth, _cloud2_1.center.y);
        _cloud2_2.center = CGPointMake(_cloud2_2.center.x + currentDeviceWidth, _cloud2_2.center.y);
        
        _cloud3_1.center = CGPointMake(_cloud3_1.center.x + currentDeviceWidth, _cloud3_1.center.y);
        _cloud3_2.center = CGPointMake(_cloud3_2.center.x + currentDeviceWidth, _cloud3_2.center.y);
        
        _cloud4_1.center = CGPointMake(_cloud4_1.center.x + currentDeviceWidth, _cloud4_1.center.y);
        _cloud4_2.center = CGPointMake(_cloud4_2.center.x + currentDeviceWidth, _cloud4_2.center.y);

        
    } completion:^(BOOL finished){
        
        if (finished){
//            [self switchCloudPositionToOffscreenPosition];
        }
        
    }];
    
//    [UIView animateWithDuration:1.0
//                          delay: 0.0
//                        options: UIViewAnimationOptionCurveEaseIn
//                     animations:^{
//                         thirdView.alpha = 0.0;
//                     }
//                     completion:^(BOOL finished){
//                         // Wait one second and then fade in the view
//                         [UIView animateWithDuration:1.0
//                                               delay: 1.0
//                                             options:UIViewAnimationOptionCurveEaseOut
//                                          animations:^{
//                                              thirdView.alpha = 1.0;
//                                          }
//                                          completion:nil];
//                     }];
}

//-(void)switchCloudPositionToOffscreenPosition
//{
//    CGFloat currentDeviceWidth = self.bounds.size.width;
//    
//    if (_cloud1_1.center.x > currentDeviceWidth) _cloud1_1.frame = _frameOffScreen_Cloud1;
//    if (_cloud1_2.center.x > currentDeviceWidth) _cloud1_2.frame = _frameOffScreen_Cloud1;
//    
//    if (_cloud2_1.center.x > currentDeviceWidth) _cloud2_1.frame = _frameOffScreen_Cloud2;
//    if (_cloud2_2.center.x > currentDeviceWidth) _cloud2_2.frame = _frameOffScreen_Cloud2;
//    
//    if (_cloud3_1.center.x > currentDeviceWidth) _cloud3_1.frame = _frameOffScreen_Cloud3;
//    if (_cloud3_2.center.x > currentDeviceWidth) _cloud3_2.frame = _frameOffScreen_Cloud3;
//    
//    [self startScrolling];
//}

@end
