//
//  UIColor+ColorUtil.m
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 2. 24..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "UIColor+ColorUtil.h"

//RGB color macro
#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

//RGB color macro with alpha
#define UIColorFromRGBWithAlpha(rgbValue,a) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:a]

@implementation UIColor (ColorUtil)

+(instancetype)colorWithRGB:(int32_t)rgbValue
{
    return [UIColor colorWithRGB:rgbValue alpha:1.0];
}

+(instancetype)colorWithRGB:(int32_t)rgbaValue alpha:(CGFloat)alpha
{
    return UIColorFromRGBWithAlpha(rgbaValue, alpha);
}

@end

@implementation UIColor (Sticker)
+(instancetype)colorWithMonkeyNumber:(NSUInteger)monkeyNumber
{
    //    ▣ Monkey_1 : Color #0a99c8 ▣ Monkey_2 : Color # ff5411 ▣ Monkey_3 : Color #1d973a ▣ Monkey_4 : Color #9453e4 ▣ Monkey_5 : Color #ffb100
    
    switch (monkeyNumber) {
        case 1: return [UIColor colorWithRGB:0x0a99c8];
        case 2: return [UIColor colorWithRGB:0xff5411];
        case 3: return [UIColor colorWithRGB:0x1d973a];
        case 4: return [UIColor colorWithRGB:0x9453e4];
        case 5: return [UIColor colorWithRGB:0xffb100];
    }
    
    return nil;
}

@end


@implementation UIColor (NameTag)
+(instancetype)colorWithMonkeyNumber:(NSUInteger)monkeyNumber normalState:(BOOL)isNormal
{
//    Normal
//    10 Roboto 33 bold Align : center
//    ▣ Monkey_1 : Color #0a99c8
//    ▣ Monkey_2 : Color #ff5411
//    ▣ Monkey_3 : Color #1d973a
//    ▣ Monkey_4 : Color #9453e4
//    ▣ Monkey_5 : Color #ffb100
//    Press
//    Roboto 33 bold
//    Align : center
//    ▣ Monkey_1 : Color #056e91
//    ▣ Monkey_2 : Color #e3451c
//    ▣ Monkey_3 : Color #127529
//    ▣ Monkey_4 : Color #65369e
//    ▣ Monkey_5 : Color #cb8f05
    if (isNormal) {
        switch (monkeyNumber) {
            case 1: return [UIColor colorWithRGB:0x0a99c8];
            case 2: return [UIColor colorWithRGB:0xff5411];
            case 3: return [UIColor colorWithRGB:0x1d973a];
            case 4: return [UIColor colorWithRGB:0x9453e4];
            case 5: return [UIColor colorWithRGB:0xffb100];
        }
    }
    else
    {
        switch (monkeyNumber) {
            case 1: return [UIColor colorWithRGB:0x056e91];
            case 2: return [UIColor colorWithRGB:0xe3451c];
            case 3: return [UIColor colorWithRGB:0x127529];
            case 4: return [UIColor colorWithRGB:0x65369e];
            case 5: return [UIColor colorWithRGB:0xcb8f05];
        }
    }

    
    return nil;
}

@end
