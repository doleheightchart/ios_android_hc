//
//  UserConfig.m
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 2. 17..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "UserConfig.h"
#import "NSObject+HeightDatabase.h"
#import "NSDate+DoleHeightChart.h"

#define kKEY_UserLogInStatus        @"UserLogInStatus"
#define kKEY_Email                  @"Email"
#define kKEY_FaceBookID             @"FacebookID"
#define kKEY_Password               @"Password"
#define kKEY_ACCOUNT_TYPE           @"AccountType"
#define kKEY_BackgroundMusicOnOff   @"BackgroundMusic"
#define kKEY_UseCloude              @"UseCloud"
#define kKEY_BackgroundMusicOn      @"BackgroundMusicOn"

#define kKEY_GuideAddNickname       @"GuideAddNickname"
//#define kKEY_GuideAddNicknamePaper  @"GuideAddNicknamePaper"
#define kKEY_GuideDetail            @"GuideDetail"
#define kKEY_GuideInputHeight       @"GuideInputHeight"
#define kKEY_GuideCameraFrame       @"GuideCameraFrame"
#define kKEY_GuideImportPhoto       @"GuideImportPhoto"
#define kKEY_GuideFirstTimeLogIn    @"GuideFirstTimeLogIn"
#define kKEY_HeightChartRecieved    @"HeightChartRecieved"
#define kKEY_LastInputHeight        @"LastInputHeight"
#define kKey_FirstAppLaunch         @"FirstAppLaunch"
#define kKey_LastSyncDataTime       @"LastSyncDataTime"
#define kKEY_GuideAddHeight         @"GuideAddHeight"

#define kKEY_NotShownEventBanner    @"NotShownEventBanner"

#define kKEY_DoleCoinLocal          @"DoleCoinLocal"

#define kKEY_FREEQRCODEUSE          @"FreeQRCodeUsed"

static UserConfig* s_UserConfigInstance = nil;

@interface UserConfig ()

-(void)loadConfig;

@end

@implementation UserConfig

+(UserConfig*)sharedConfig
{
    if (s_UserConfigInstance == nil){
        s_UserConfigInstance = [[UserConfig alloc] init];
        [s_UserConfigInstance loadConfig];
    }
    
    return s_UserConfigInstance;
}

-(void)loadConfig
{
    NSUserDefaults *userdefault = [NSUserDefaults standardUserDefaults];
    
    NSString *email = [userdefault valueForKey:kKEY_Email];
    NSString *pwd = [userdefault valueForKey:kKEY_Password];
//    NSNumber *ls = [userdefault valueForKey:kKEY_UserLogInStatus];
    NSNumber *acctype = [userdefault valueForKey:kKEY_ACCOUNT_TYPE];
    
    
    //self.loginStatus = (enum UserLogInStatus)[userdefault integerForKey:kKEY_UserLogInStatus];
    self.loginStatus = kUserLogInStatusOffLine;
    _authKey = nil;
    
//    self.accountStatus = kSignUpByEmail;
    self.accountStatus = kNotSingUp;

//    self.email = @"lonlydriver@naver.com";
//    self.password = @"test1234";
    
    self.email = email;
    self.password = pwd;
    
//    if (ls){
//        enum UserLogInStatus loginstatus = (enum UserLogInStatus)[ls integerValue];
//        self.loginStatus = loginstatus;
//    }
//    else{
//        self.loginStatus = kUserLogInStatusOffLine;
//    }
    
    if (acctype){
        enum AccountStatus as = (enum AccountStatus)[acctype integerValue];
        self.accountStatus = as;
    }
    else
        self.accountStatus = kNotSingUp;
    
    
    NSNumber *isGuideAddnickname    = [userdefault valueForKey:kKEY_GuideAddNickname];
//    NSNumber *isGuideAddnicknamePaper    = [userdefault valueForKey:kKEY_GuideAddNicknamePaper];
    NSNumber *isGuideCameraFrame    = [userdefault valueForKey:kKEY_GuideCameraFrame];
    NSNumber *isGuideDetail         = [userdefault valueForKey:kKEY_GuideDetail];
    NSNumber *isGuideFirstTimeLogIn = [userdefault valueForKey:kKEY_GuideFirstTimeLogIn];
    NSNumber *isGuideImportPhoto    = [userdefault valueForKey:kKEY_GuideImportPhoto];
    NSNumber *isGuideInputHeight    = [userdefault valueForKey:kKEY_GuideInputHeight];
    NSNumber *isGuideAddHeight      = [userdefault valueForKey:kKEY_GuideAddHeight];

    _isNeedGuideAddNickname = [isGuideAddnickname boolValue];
//    _isNeedGuideAddnicknamePaper = [isGuideAddnicknamePaper boolValue];
    _isNeedGuideCameraFrame = [isGuideCameraFrame boolValue];
    _isNeedGuideDetail = [isGuideDetail boolValue];
    _isNeedGuideFirstTimeLogIn = [isGuideFirstTimeLogIn boolValue];
    _isNeedGuideImportPhoto = [isGuideImportPhoto boolValue];
    _isNeedGuideInputHeight = [isGuideInputHeight boolValue];
    _isNeedGuideAddHeight = [isGuideAddHeight boolValue];
    
    //_useiCloud = NO;
    
    NSNumber *useCloude = [userdefault valueForKey:kKEY_UseCloude];
    _useiCloud = [useCloude boolValue];
//    _useiCloud = YES;
    
    _backgroundMusicOn = YES;
    NSNumber *useBackMusic = [userdefault valueForKey:kKEY_BackgroundMusicOn];
    if (useBackMusic)
        _backgroundMusicOn = [useBackMusic boolValue];
    else
        _backgroundMusicOn = YES;
    
    NSNumber *isHeightChartRecied = [userdefault valueForKey:kKEY_HeightChartRecieved];
    _isHeightChartPaparRecieved = [isHeightChartRecied boolValue];
    
    _isHeightChartPaparRecievedShowBanner = _isHeightChartPaparRecieved;
    
    if (self.accountStatus != kUserLogInStatusOffLine){
        self.needQueryDoleCoin = YES;
    }

    self.needQueryDoleCoin = NO;

    NSNumber *firstLaunch = [userdefault valueForKey:kKey_FirstAppLaunch];
    self.isFirstTimeAppLaunch = (firstLaunch == nil);
    
//    NSNumber *lastInputHeight = [userdefault valueForKey:kKEY_LastInputHeight];
//    self.lastInputHeight = lastInputHeight;
    
    self.lastSyncDataTime = [userdefault valueForKey:kKey_LastSyncDataTime];
    
    self.dateEventNotShown = [userdefault valueForKey:kKEY_NotShownEventBanner];
    
    self.doleCoin = [[userdefault valueForKey:kKEY_DoleCoinLocal] integerValue];
    
    NSNumber *freeQRUsed = [userdefault valueForKey:kKEY_FREEQRCODEUSE];
    if (freeQRUsed){
        self.usedFirstFreeSticker = [freeQRUsed boolValue];
    }
    
    self.deviceToken = @"";
}

-(void)setAuthKey:(NSString *)authKey;
{
    _authKey = authKey;
    
    _dateGetAuthkey = [NSDate date];
}

-(void)setUserNo:(NSInteger)userNo
{
    _userNo = userNo;
}

-(void)signUpByEmail:(NSString*)email password:(NSString*)password
{
    self.email = email;
    self.password = password;
    self.loginStatus = kUserLogInStatusDoleLogOn;
    self.accountStatus = kSignUpByEmail;
    
    NSUserDefaults *userdefault = [NSUserDefaults standardUserDefaults];
    [userdefault setObject:email forKey:kKEY_Email];
    [userdefault setObject:password forKey:kKEY_Password];
//    [userdefault setObject:[NSNumber numberWithInteger:self.loginStatus] forKey:kKEY_UserLogInStatus];
    [userdefault setObject:[NSNumber numberWithInteger:self.accountStatus] forKey:kKEY_ACCOUNT_TYPE];
    
    [userdefault  synchronize];
    
    self.needQueryDoleCoin = YES;
}

-(void)signUpByFacebookEmail:(NSString*)facebookEmail  FacebookID:(NSString*)facebookID
{
    self.email = facebookEmail;
    self.facebookID = facebookID;
    self.loginStatus = kUserLogInStatusFacebook;
    self.accountStatus = kSignUpByFacebook;
    
    NSUserDefaults *userdefault = [NSUserDefaults standardUserDefaults];
    [userdefault setObject:facebookEmail forKey:kKEY_Email];
    [userdefault setObject:[NSNumber numberWithInteger:self.accountStatus] forKey:kKEY_ACCOUNT_TYPE];
    [userdefault setObject:facebookID forKey:kKEY_FaceBookID];
    [userdefault  synchronize];
    
    self.needQueryDoleCoin = YES;
}

-(void)modifyEmailUserPassword:(NSString*)password
{
    if (![self.password isEqualToString:password]){
        NSUserDefaults *userdefault = [NSUserDefaults standardUserDefaults];
        [userdefault setObject:password forKey:kKEY_Password];
        [userdefault  synchronize];
    }
    
    self.password = password;
}

-(void)setUseiCloud:(BOOL)useiCloud
{
    if (_useiCloud != useiCloud){
        _useiCloud = useiCloud;
        
//        if (_useiCloud){
//            
//        }
//        else{
//            [self moveCloudDBToLocalDB];
//        }

        NSUserDefaults *userdefault = [NSUserDefaults standardUserDefaults];
        [userdefault setObject:[NSNumber numberWithBool:_useiCloud] forKey:kKEY_UseCloude];
        [userdefault  synchronize];
        
        //[self didChangeDBStore];
        [self sendDBStoreChangeNotification];
    }
}

-(void)canNotUseiCloud
{
    _useiCloud = NO;
    NSUserDefaults *userdefault = [NSUserDefaults standardUserDefaults];
    [userdefault setObject:[NSNumber numberWithBool:_useiCloud] forKey:kKEY_UseCloude];
    [userdefault  synchronize];
}

-(void)setBackgroundMusicOn:(BOOL)backgroundMusicOn
{
    if (_backgroundMusicOn != backgroundMusicOn){
        _backgroundMusicOn = backgroundMusicOn;
        
        NSUserDefaults *userdefault = [NSUserDefaults standardUserDefaults];
        [userdefault setObject:[NSNumber numberWithBool:_backgroundMusicOn] forKey:kKEY_BackgroundMusicOn];
        [userdefault  synchronize];

    }
}

#pragma mark GUIDE 
-(void)setIsNeedGuideAddNickname:(BOOL)isNeedGuideAddNickname
{
    if (_isNeedGuideAddNickname != isNeedGuideAddNickname){
        _isNeedGuideAddNickname = isNeedGuideAddNickname;
        
        NSUserDefaults *userdefault = [NSUserDefaults standardUserDefaults];
        [userdefault setObject:[NSNumber numberWithBool:_isNeedGuideAddNickname] forKey:kKEY_GuideAddNickname];
        [userdefault  synchronize];
    }
}


-(void)setIsNeedGuideCameraFrame:(BOOL)isNeedGuideCameraFrame
{
    if (_isNeedGuideCameraFrame != isNeedGuideCameraFrame){
        _isNeedGuideCameraFrame = isNeedGuideCameraFrame;
        
        NSUserDefaults *userdefault = [NSUserDefaults standardUserDefaults];
        [userdefault setObject:[NSNumber numberWithBool:_isNeedGuideCameraFrame] forKey:kKEY_GuideCameraFrame];
        [userdefault  synchronize];
    }
}

-(void)setIsNeedGuideDetail:(BOOL)isNeedGuideDetail
{
    if (_isNeedGuideDetail != isNeedGuideDetail){
        _isNeedGuideDetail = isNeedGuideDetail;
        
        NSUserDefaults *userdefault = [NSUserDefaults standardUserDefaults];
        [userdefault setObject:[NSNumber numberWithBool:_isNeedGuideDetail] forKey:kKEY_GuideDetail];
        [userdefault  synchronize];
    }
}

-(void)setIsNeedGuideImportPhoto:(BOOL)isNeedGuideImportPhoto
{
    if (_isNeedGuideImportPhoto != isNeedGuideImportPhoto){
        _isNeedGuideImportPhoto = isNeedGuideImportPhoto;
        
        NSUserDefaults *userdefault = [NSUserDefaults standardUserDefaults];
        [userdefault setObject:[NSNumber numberWithBool:_isNeedGuideImportPhoto] forKey:kKEY_GuideImportPhoto];
        [userdefault  synchronize];
    }
}

-(void)setIsNeedGuideInputHeight:(BOOL)isNeedGuideInputHeight
{
    if (_isNeedGuideInputHeight != isNeedGuideInputHeight){
        _isNeedGuideInputHeight = isNeedGuideInputHeight;
        
        NSUserDefaults *userdefault = [NSUserDefaults standardUserDefaults];
        [userdefault setObject:[NSNumber numberWithBool:_isNeedGuideInputHeight] forKey:kKEY_GuideInputHeight];
        [userdefault  synchronize];
    }
}

-(void)setIsNeedGuideFirstTimeLogIn:(BOOL)isNeedGuideFirstTimeLogIn
{
    if (_isNeedGuideFirstTimeLogIn != isNeedGuideFirstTimeLogIn){
        _isNeedGuideFirstTimeLogIn = isNeedGuideFirstTimeLogIn;
        
        NSUserDefaults *userdefault = [NSUserDefaults standardUserDefaults];
        [userdefault setObject:[NSNumber numberWithBool:_isNeedGuideFirstTimeLogIn] forKey:kKEY_GuideFirstTimeLogIn];
        [userdefault  synchronize];
    }
}

-(void)setIsNeedGuideAddHeight:(BOOL)isNeedGuideAddHeight
{
    if (_isNeedGuideAddHeight != isNeedGuideAddHeight){
        _isNeedGuideAddHeight = isNeedGuideAddHeight;
        
        NSUserDefaults *userdefault = [NSUserDefaults standardUserDefaults];
        [userdefault setObject:[NSNumber numberWithBool:_isNeedGuideAddHeight] forKey:kKEY_GuideAddHeight];
        [userdefault  synchronize];
    }
}

-(void)setIsHeightChartPaparRecieved:(BOOL)isHeightChartPaparRecieved
{
    if (_isHeightChartPaparRecieved != isHeightChartPaparRecieved){
        _isHeightChartPaparRecieved = isHeightChartPaparRecieved;
        
        NSUserDefaults *userdefault = [NSUserDefaults standardUserDefaults];
        [userdefault setObject:[NSNumber numberWithBool:_isHeightChartPaparRecieved] forKey:kKEY_HeightChartRecieved];
        [userdefault  synchronize];
    }
}

//-(void)recordLastInputHeight:(CGFloat)inputHeight;
//{
//    NSNumber *lastInputHeight = [NSNumber numberWithFloat:inputHeight];
//    self.lastInputHeight = lastInputHeight;
//    
//    NSUserDefaults *userdefault = [NSUserDefaults standardUserDefaults];
//    [userdefault setObject:self.lastInputHeight forKey:kKEY_LastInputHeight];
//    [userdefault  synchronize];
//    
//}

-(void)markFirstLaunch
{
    self.isFirstTimeAppLaunch = NO;
    NSNumber *firstLaunch = [NSNumber numberWithBool:self.isFirstTimeAppLaunch];
    NSUserDefaults *userdefault = [NSUserDefaults standardUserDefaults];
    [userdefault setObject:firstLaunch forKey:kKey_FirstAppLaunch];
    [userdefault  synchronize];
}

-(void)syncCloudData
{
    NSDate *now = [NSDate date];
    self.lastSyncDataTime = now;
    NSUserDefaults *userdefault = [NSUserDefaults standardUserDefaults];
    [userdefault setObject:self.lastSyncDataTime forKey:kKey_LastSyncDataTime];
    [userdefault  synchronize];
}

-(void)withDraw
{
    self.email = @"";
    self.password = @"";
    self.accountStatus = kNotSingUp;
    self.loginStatus = kUserLogInStatusOffLine;
    self.doleCoin = 0;
    
    //self.useiCloud = NO;
    _useiCloud = NO;
    
    self.authKey = nil;
    self.userNo = 0;
    
    NSUserDefaults *userdefault = [NSUserDefaults standardUserDefaults];
//    [userdefault setObject:self.email forKey:kKEY_Email];
//    [userdefault setObject:self.password forKey:kKEY_Password];
//    [userdefault setObject:[NSNumber numberWithInteger:self.accountStatus] forKey:kKEY_ACCOUNT_TYPE];
//    [userdefault setObject:[NSNumber numberWithInteger:self.doleCoin] forKey:kKEY_DoleCoinLocal];
    
    [userdefault setObject:nil forKey:kKEY_Email];
    [userdefault setObject:nil forKey:kKEY_Password];
    [userdefault setObject:nil forKey:kKEY_ACCOUNT_TYPE];
    [userdefault setObject:nil forKey:kKEY_DoleCoinLocal];
    [userdefault setObject:nil forKey:kKEY_UseCloude];
    [userdefault synchronize];
}

-(void)notShownEventOneDays
{
    self.dateEventNotShown = [NSDate date];
    
    NSUserDefaults *userdefault = [NSUserDefaults standardUserDefaults];
    [userdefault setObject:self.dateEventNotShown forKey:kKEY_NotShownEventBanner];
    
    [userdefault  synchronize];
}

-(BOOL)shownEventBanner
{
    if (nil == self.dateEventNotShown) return TRUE;
    
    NSInteger hours = [self.dateEventNotShown getHoursFromNow];
    
    return hours < -24; // 1days
}

-(void)saveDoleCoinToLocal
{
    NSUserDefaults *userdefault = [NSUserDefaults standardUserDefaults];
    [userdefault setObject:[NSNumber numberWithInteger:self.doleCoin] forKey:kKEY_DoleCoinLocal];
    
    [userdefault  synchronize];
}

-(void)takenFreeQRCode
{
    self.usedFirstFreeSticker = YES;
    
    NSUserDefaults *userdefault = [NSUserDefaults standardUserDefaults];
    [userdefault setObject:[NSNumber numberWithBool:self.usedFirstFreeSticker] forKey:kKEY_FREEQRCODEUSE];
    
    [userdefault  synchronize];
}

@end
