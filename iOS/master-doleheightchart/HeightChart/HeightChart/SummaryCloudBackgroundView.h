//
//  SummaryCloudBackgroundView.h
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 13..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SummaryCloudBackgroundView : UIView
-(void)startScrolling;
@end
