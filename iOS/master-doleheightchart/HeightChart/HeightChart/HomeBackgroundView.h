//
//  HomeBackgroundView.h
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 21..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeBackgroundView : UIView

@property (nonatomic) BOOL isDeleteMode;

-(void)startScrolling;

@end
