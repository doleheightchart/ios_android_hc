//
//  FacebookManager.h
//  HeightChart
//
//  Created by ne on 2014. 3. 27..
//  Copyright (c) 2014년 Kim Sehyun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FacebookSDK/FacebookSDK.h>

enum FacebookInviteReturn
{
    FacebookInviteReturn_Error = 0,
    FacebookInviteReturn_OK = 1,
    FacebookInviteReturn_Cancel = 2
};

@protocol FacebookManagerDelegate <NSObject>

@optional
- (void) loginResult:(BOOL)isSucess withAcessToken:(NSString*)accessToken;
- (void) didReceiveUserID:(NSString*)userID Email:(NSString*)email Error:(id)error;
//- (void) faceBookFriendList:(NSArray*) friendList;

@end

@protocol FacebookManagerInviteDelegate <NSObject>

@optional
- (void) inviteResult:(NSInteger)result;

@end


@interface FacebookManager : NSObject

@property (nonatomic, retain) NSString *facebookAcessToken;
@property (nonatomic, weak) id<FacebookManagerDelegate> delegate;
@property (nonatomic, weak) id<FacebookManagerInviteDelegate> invitedelegate;
@property (nonatomic, strong) NSString *facebookID;
@property (nonatomic, strong) NSString *emailAddress;
@property (nonatomic, strong) NSString *gender;
@property (nonatomic, strong) NSString *birthday;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *facebookName;


- (void) login;
- (void) logout;
- (void) postingPhotoWithImage:(UIImage *)image;
- (NSArray *) getInviteFriendList;
//- (void) inviteFriendWithFriendIDList:(NSArray *)friendList;


- (void)postingPhoto:(UIImage*)photo Message:(NSString*)message completion:(void(^)(BOOL))completion;
- (void)friendListForInviteWithCompletion:(void(^)(NSArray*))completion;
- (void)checkFacebookIDWithCompletion:(void(^)(NSString*))completion;
- (void)inviteFriendWithFriendIDList:(NSArray *)friendList;

@end

@interface FacebookFriendInfo : NSObject

@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *email;
@property (nonatomic, retain) NSString *facebookID;
@property (nonatomic, retain) NSString *installed;
@property (nonatomic, retain) UIImage *icon;
@property (nonatomic) BOOL checked;

@end
