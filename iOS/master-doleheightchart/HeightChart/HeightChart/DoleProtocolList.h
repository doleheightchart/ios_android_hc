//
//  DoleProtocolList.h
//  HeightChart
//
//  Created by ne on 2014. 3. 21..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DoleTree.h"
#import "MyChild.h"
#import "PhotoButton.h"
@interface DoleProtocolList : NSObject


@end


@protocol clickMonkeyDelegate <NSObject>

-(void)clickMonkeyWithData:(DoleTree*) monkeyTree;
-(void)clickLongPress:(DoleTree*)monkeyTree;
-(void)didCheckedMonkeyWithData:(DoleTree*)monkeyTree Checked:(BOOL)checked;

@end

@protocol clickOwlDelegate <NSObject>

-(void)clickAddNickName;


@end

@protocol MonkeyTreeDataSource <NSObject>

-(NSUInteger)monkeyCount;
-(MyChild *)monkeyTreeInfoAtIndex:(NSUInteger)index;

@end


@protocol MonkeyHomeListDelegate<NSObject>

-(void)clickMonkeyAtIndex:(NSUInteger)index;
-(void)didCheckedMonkeyWithIndex:(NSUInteger)index Checked:(BOOL)checked;
-(void)longPressedAtIndex:(NSUInteger)index;
-(void)clickAddNewNickname;
-(void)didFinishedAnimation:(BOOL)isFirstTime;

@end
