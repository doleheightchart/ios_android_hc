//
//  HeightSummaryViewController.m
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 9..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "HeightSummaryViewController.h"
#import "UIView+ImageUtil.h"
#import "HeightSummary.h"
#import "UIViewController+DoleHeightChart.h"
#import "SummaryCloudBackgroundView.h"
#import "ToastController.h"
#import "SelectShareAppPopupController.h"
#import "MPFoldTransition/DoleTransition.h"
#import "UIViewController+PolyServer.h"
#import "DoleCoinPopupViewController.h"
#import "MessageBoxController.h"
#import "Mixpanel.h"

@interface HeightSummaryViewController (){
    UIButton    *_closeButton;
    NSString    *_lastOrderID;
    UIView      *_titleView;
    
    //Server
    NSMutableData   *_recevedBufferData;
}

@property (weak, nonatomic) IBOutlet UIView *testView;
@property (weak, nonatomic) IBOutlet HeightSummary *heightSummary;
@property (weak, nonatomic) IBOutlet UIImageView *bottomGrassImage;

@property (weak, nonatomic) IBOutlet UIButton *standardHeightBtn;
@property (weak, nonatomic) IBOutlet UIButton *shareHeightBtn;
@property (weak, nonatomic) IBOutlet UIImageView *grassBottomImage;
@property (weak, nonatomic) IBOutlet SummaryCloudBackgroundView *cloudBackgroundView;

- (IBAction)clickShareHeight:(id)sender;
- (IBAction)clickStandardheight:(id)sender;

@end

@implementation HeightSummaryViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[Mixpanel sharedInstance] track:@"Viewed Height Progression"];
    
    [self decorateUI];
    
    self.heightSummary.myChild = self.myChild;
    
    self.heightSummary.heightDetailDataSource = self.heightDetailDataSource;
    
    [self.heightSummary reloadData];
    
    //    self.facebookManager.delegate = self;
}

-(void)decorateUI
{
    _titleView = [self addTitleMiniToLeftTop];
    
    //Standard height button
    self.standardHeightBtn.adjustsImageWhenDisabled = NO;
    self.standardHeightBtn.backgroundColor = [UIColor clearColor];
    self.standardHeightBtn.titleLabel.text = @"";
    [self.standardHeightBtn setBackgroundImage:[UIImage imageNamed:@"st_height_btn_on"]
                                      forState:UIControlStateSelected];
    [self.standardHeightBtn setBackgroundImage:[UIImage imageNamed:@"st_height_btn_off"]
                                      forState:UIControlStateNormal];
    
    
    
    //Share height button
    self.shareHeightBtn.adjustsImageWhenDisabled = NO;
    self.shareHeightBtn.backgroundColor = [UIColor clearColor];
    self.shareHeightBtn.titleLabel.text = @"";
    [self.shareHeightBtn setBackgroundImage:[UIImage imageNamed:@"share_btn_normal"]
                                   forState:UIControlStateNormal];
    [self.shareHeightBtn setBackgroundImage:[UIImage imageNamed:@"share_btn_press"]
                                   forState:UIControlStateHighlighted];
    
    
    //    CGFloat w = self.view.bounds.size.width;
    
    
    
    //grass
    if (NO == [UIScreen isFourInchiScreen]){
        self.grassBottomImage.image = [UIImage imageNamed:@"grass_land_02_960"];
    }
    
    CGSize viewSize = self.view.bounds.size;
    CGFloat heightOfLandscape = viewSize.width;
    CGFloat widthOfLandscape = viewSize.height;
    
    // minHeightLine Display
    UIView *minHeightLineView = [[UIView alloc]initWithFrame:CGRectMake(3,
                                                                        heightOfLandscape - (138/2),
                                                                        (116/2),
                                                                        (4/2))];
    minHeightLineView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"graph_line_02_horizontal"]];
    [self.view addSubview:minHeightLineView];
    
    UIImageView *minHeightLabelImageView = [[UIImageView alloc]initWithFrame:CGRectMake(6,
                                                                                        heightOfLandscape - (134/2),
                                                                                        (62/2),
                                                                                        (20/2))];
    
    minHeightLabelImageView.image = [UIImage imageNamed:@"summary_line_50"];
    [self.view addSubview:minHeightLabelImageView];
    
    // maxHeightLine Display
    
    UIView *maxHeightLineView = [[UIView alloc]initWithFrame:CGRectMake(widthOfLandscape - (116/2),
                                                                        (170/2),
                                                                        (116/2),
                                                                        (4/2))];
    maxHeightLineView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"graph_line_02_horizontal"]];
    [self.view addSubview:maxHeightLineView];
    
    UIImageView *maxHeightLabelImageView = [[UIImageView alloc]initWithFrame:CGRectMake(widthOfLandscape - (86/2),
                                                                                        (174/2),
                                                                                        (74/2),
                                                                                        (20/2))];
    
    maxHeightLabelImageView.image = [UIImage imageNamed:@"summary_line_170"];
    [self.view addSubview:maxHeightLabelImageView];
    
    
    
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [self.heightSummary initControl];
    
    [self.heightSummary shownCellWithIndex:self.shownIndex];
    
    [self.cloudBackgroundView startScrolling];
    
    CGSize viewSize = self.view.bounds.size;
    CGFloat widthOfLandscape = viewSize.width;
    self.standardHeightBtn.frame = CGRectMake(widthOfLandscape - (88*3+18*2+14)/2, 8/2, 88/2, 88/2);
    self.shareHeightBtn.frame = CGRectMake(widthOfLandscape - (88*2+18+14)/2, 8/2, 88/2, 88/2);
    
    _closeButton = [self addCommonCloseButton];
    
    
    
    [self.heightSummary updateGraph];
}

-(void)viewWillAppear:(BOOL)animated
{
    //    [self.cloudBackgroundView startScrolling];
}

-(void)viewDidDisappear:(BOOL)animated
{
    //    self.facebookManager.delegate = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// Override to allow orientations other than the default portrait orientation.
//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
//{
////    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft) ||
////    (interfaceOrientation == UIInterfaceOrientationLandscapeRight);
//
//    return interfaceOrientation == UIInterfaceOrientationLandscapeRight;
//}

//- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
//{
//    return UIInterfaceOrientationLandscapeRight;
//}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}

//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)orientation
//{
//    if ((orientation == UIInterfaceOrientationLandscapeRight) ||
//        (orientation == UIInterfaceOrientationLandscapeLeft))
//        return YES;
//
//    return NO;
//}

-(BOOL)shouldAutorotate
{
    return NO;
}

- (UIImage*)createImageForShare
{
    CGRect dstRect = self.view.bounds; // 960 or 1136 ?
    
    _closeButton.hidden = YES;
    self.standardHeightBtn.hidden = YES;
    self.shareHeightBtn.hidden = YES;
    _titleView.hidden = YES;
    
    
    UIImageView *titleMiniView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"title_height_dole_logo"]];
    titleMiniView.frame = CGRectMake(0, 0, 116/2, 72/2);
    [self.view addSubview:titleMiniView];
    UIImageView *miniHeightTitleView= [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"height_viewer_height_chart_logo"]];
    miniHeightTitleView.frame = CGRectMake(dstRect.size.width - (108/2+22/2), 18/2, 108/2, 62/2);
    [self.view addSubview:miniHeightTitleView];
    
    [titleMiniView setNeedsDisplay];
    [miniHeightTitleView setNeedsDisplay];
    
    //    _closeButton.layer.opacity = 0;
    
    //    UIGraphicsBeginImageContextWithOptions(dstRect.size, NO, [UIScreen isFourInchiScreen] ? 2.0 : 1.0);
    UIGraphicsBeginImageContextWithOptions(dstRect.size, NO, 2.0);
    
    //    [self.cloudBackgroundView drawRect:dstRect];
    //    [self.heightSummary drawRect:dstRect];
    CGContextRef context = UIGraphicsGetCurrentContext();
    //    CGContextTranslateCTM(context, 0, 0);
    
    
    
    
    //		CGContextGetCTM(context);
    //		CGContextScaleCTM(context, 1, 1);
    //		CGContextTranslateCTM(context, 0,
    //                              0);
    
    //    [self.cloudBackgroundView drawViewHierarchyInRect:self.cloudBackgroundView.frame afterScreenUpdates:NO];
    //[self.bottomGrassImage drawRect:CGRectMake(100, 100, 100, 100)];
    //    CGRect frame = self.bottomGrassImage.frame;
    
    // Translate it, to the desired position
    //	CGContextTranslateCTM(context, -frame.origin.x, -frame.origin.y);
    //    CGRect r = CGRectMake(dstRect.size.height - self.bottomGrassImage.frame.origin.y, 0, self.bottomGrassImage.frame.size.width, self.bottomGrassImage.frame.size.height);
    //    [self.bottomGrassImage drawRect:r];
    // Render the view as image
    //[self.bottomGrassImage.layer drawInContext:context];
    
    //    [self.heightSummary drawViewHierarchyInRect:self.heightSummary.frame afterScreenUpdates:NO];
    //    [self.grassBottomImage drawRect:self.grassBottomImage.frame];
    
    //    [self.view drawViewHierarchyInRect:dstRect afterScreenUpdates:NO];
    [self.view.layer renderInContext:context];
    
    //    UIImage *logo = [UIImage imageNamed:@"title_height_dole_logo"];
    //    [logo drawInRect:CGRectMake(0, 0, 116/2, 72/2)];
    
    UIImage *resultImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    _closeButton.hidden = NO;
    self.standardHeightBtn.hidden = NO;
    self.shareHeightBtn.hidden = NO;
    _titleView.hidden = NO;
    
    [titleMiniView removeFromSuperview];
    [miniHeightTitleView removeFromSuperview];
    //
    return resultImage;
}

- (void)sharePhoto
{
    UIImage *sharePhoto = [self createImageForShare];
    [self addActiveIndicator];
    
    NSString *nickname = self.myChild.nickname;
    NSString *uploadMessage = [NSString stringWithFormat:@""];
                               // FB Prefilling not allowed: NSLocalizedString(@"%@'s growth graph. I used the #DoleHeightChart App!", @""), nickname];
    
    [self.facebookManager postingPhoto:sharePhoto  Message:uploadMessage completion:^(BOOL isSuccess) {
        if (isSuccess){
            [ToastController showToastWithMessage: NSLocalizedString(@"Successful! Your photo has been shared.", @"")  duration:kToastMessageDurationType_Auto onViewController:self];
            
            [self removeActiveIndicator];
            
            [self countUpDolePoint];
            
            [[Mixpanel sharedInstance] track:@"Shared Height Progression via Facebook"];
            
        }
        else {
            //            [ToastController showToastWithMessage:@"Facebook에 업로드 실패 하였습니다" duration:5 onViewController:self];
            
            //            MessageBoxController showOKConfirmMessage:(NSString *) onView:self completion:<#^(void)completion#>
            [self showOKConfirmMessage:NSLocalizedString(@"Failed to share on Facebook.\nPlease check network connection\nor Facebook status and try again", @"") completion:nil];
            
            [self removeActiveIndicator];
        }
    }];
}


- (IBAction)clickShareHeight:(id)sender
{
    if([self enableActionWithCurrentNetwork] == NO) return;
    
    UIStoryboard *popupStory = [UIStoryboard storyboardWithName:@"Popup" bundle:nil];
    SelectShareAppPopupController *vc = [popupStory instantiateViewControllerWithIdentifier:@"ShareFacebook"];
    vc.CompletionBlock = ^(BOOL result){
        if (result){
            
            //            if (self.facebookManager.facebookID)
            //                [self sharePhoto];
            //            else
            //                [self.facebookManager login];
            
            [self sharePhoto];
        }
    };
    [vc showModalOnTheViewController:nil];
}

- (IBAction)clickStandardheight:(id)sender
{
    self.standardHeightBtn.selected = !self.standardHeightBtn.selected;
    
    self.heightSummary.visibleStandardHeightLine = self.standardHeightBtn.selected;
    
    NSString *message = self.standardHeightBtn.selected ? NSLocalizedString(@"The Average Height Graph is displayed", @"")  : NSLocalizedString(@"The Average Height Graph is not displayed", @"");
    [ToastController showToastWithMessage:message duration:kToastMessageDurationType_Auto onViewController:self];
}

-(void)closeViewControllerAnimated:(BOOL)animated
{
    //    [self.presentingViewController dismissViewControllerWithFoldStyle:0 completion:nil];
    
    //    UIViewController *src = self;
    //    UINavigationController *presentingController = (UINavigationController*)self.presentingViewController;
    //    UIViewController *dest = presentingController.topViewController;
    //
    //    DoleTransition *foldTransition = [[DoleTransition alloc] initWithSourceView:src.view destinationView:dest.view duration:0.5 style:7 completionAction:MPTransitionActionNone];
    //	[foldTransition setDismissing:YES];
    //	[foldTransition setPresentedController:src];
    //	[presentingController dismissViewControllerAnimated:NO completion:nil];
    //	[foldTransition perform:^(BOOL finished) {
    //		[dest.view setHidden:NO];
    //
    //	}];
    
    //    [self.presentingViewController dismissViewControllerWithFlipStyle:0 completion:nil];
    
    [self.presentingViewController dismissViewControllerAnimated:NO completion:nil];
}

#pragma mark Server

-(void)countUpDolePoint
{
    if (self.userConfig.loginStatus != kUserLogInStatusOffLine){
        
        _lastOrderID = [self UUID];
        [self requestDoleCoinChargeFreeCash:100 UserNo:self.userConfig.userNo AuthKey:self.userConfig.authKey OrderID:_lastOrderID];
    }
}

- (void)didConnectionFail:(NSError *)error
{
    [self toastNetworkError];
}

- (void)didCompleteRecevedWithResultType:(DoleServerResultType)resultType ParsedData:(NSDictionary *)recevedData ParseError:(NSError *)error
{
    if (!error){
        if ([recevedData[@"Return"] boolValue]){
            
            if (resultType == kDoleServerResultTypeChargeFreeCash){
                
                self.userConfig.doleCoin += 100;
                [self.userConfig saveDoleCoinToLocal];
                
                UIStoryboard *story = [UIStoryboard storyboardWithName:@"Popup" bundle:nil];
                DoleCoinPopupViewController *dvc = [story instantiateViewControllerWithIdentifier:@"dolecoinPopup"];
                [dvc dolcoinCount:100];
                dvc.completion = ^(DoleCoinPopupViewController *controller){
                    
                };
                [dvc showModalOnTheViewController:nil];
            }
        }
        else{
            NSInteger errorCode = [recevedData[@"ReturnCode"] integerValue];
            [self toastNetworkErrorWithErrorCode:errorCode];
        }
    }
    else{
        
        [self toastNetworkError];
    }
}

- (NSMutableData*)receivedBufferData
{
    if (nil == _recevedBufferData){
        _recevedBufferData = [NSMutableData dataWithCapacity:0];
    }
    
    return _recevedBufferData;
}


- (void) loginResult:(BOOL)isSucess withAcessToken:(NSString*)accessToken
{
    
}

- (void) didReceiveUserID:(NSString*)userID Email:(NSString*)email Error:(id)error
{
    if (nil == error && userID)
        [self sharePhoto];
    else {
        
    }
}

@end
