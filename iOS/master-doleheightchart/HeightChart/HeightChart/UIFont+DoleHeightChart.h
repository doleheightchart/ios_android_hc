//
//  UIFont+DoleHeightChart.h
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 6..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (DoleHeightChart)
+(instancetype)defaultBoldFontWithSize:(CGFloat)size;
+(instancetype)defaultRegularFontWithSize:(CGFloat)size;
+(instancetype)defaultMediumFontWithSize:(CGFloat)size;
+(instancetype)defaultGuideFondWithSize:(CGFloat)size;

//TODO::한글폰트 테스트 나중에는 로컬라이즈에 따라 선택가능
+(instancetype)defaultKoreanFontWithSize:(CGFloat)size;
+(instancetype)defaultFondByLanguageWithSize:(CGFloat)size;
@end
