//
//  NSObject+HeightDatabase.h
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 4. 5..
//  Copyright (c) 2014년 Kim Sehyun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface NSObject (HeightDatabase)

@property (nonatomic, readonly) NSManagedObjectModel *usedObjectModel;
@property (nonatomic, readonly) NSManagedObjectContext *usedObjectContext;

//-(NSFetchedResultsController*)requestChildren;

-(void)saveContext;
//-(void)initailizeDB;


// use iCloud Sync

-(NSFetchedResultsController*)requestLocalFetchedResultController;
-(NSFetchedResultsController*)requestCloudFetchedResultController;
-(NSFetchedResultsController*)requestFetchedResultController;

-(BOOL)moveLocalDBToCloudDBWithLocalFetchedResultController:(NSFetchedResultsController*)localFetchedResultController; //처음시작시 여러번 불리기에 캐시된 콘트롤러를 받아야 한다.

-(void)moveCloudDBToLocalDB;

-(void)didChangeDBStore:(NSNotification *)notification;
-(void)iCloudReady:(NSNotification*)notification;


- (void)addObserverSelfChangeDBStore;
- (void)sendDBStoreChangeNotification;
- (void)registerForiCloudNotifications;
- (void)removeiCloudNotifications;
- (void)storesWillChange:(NSNotification *)notification;
- (void)storesDidChange:(NSNotification *)notification;
- (void)persistentStoreDidImportUbiquitousContentChanges:(NSNotification *)notification;

-(void)migrationCloudDB;

-(void)mergeCloudDB;

-(void)removeCloudDB;

-(BOOL)canUseiCloud;

@end
