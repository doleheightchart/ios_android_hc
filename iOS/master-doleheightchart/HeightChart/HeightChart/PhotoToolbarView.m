//
//  PhotoToolbarView.m
//  HeightChart
//
//  Created by ne on 2014. 3. 3..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "PhotoToolbarView.h"
#import "PhotoButton.h"

@interface PhotoToolbarView()
{
    UIButton *_reTakeBtn;
    enum DolePhotoButtonType _selectFrameType;
}
@property (nonatomic, retain) NSArray *toolBarButton;
@property (nonatomic) NSInteger buttonCount;
@property (nonatomic, retain) CameraButton *btnCpatureCamera;

@end
@implementation PhotoToolbarView

NSInteger lockStartPageIndex = 3;

- (id)initWithPhotoToolbar
{
    self = [super initWithFrame:CGRectMake(0, 0, 320, 73 + 42)];
    if (self) {
        [self initWithToolbarButton];
    }
    return self;
}

-(void)initWithToolbarButton
{
    self.buttonCount = 8 ;
    
    CGFloat centerButtonWidth = 64.0;
    
    for (int i = 0; i  < 4; i ++) {
        PhotoButtonControl *btn = [[PhotoButtonControl alloc]initWithPhotoButtonType:i];
        btn.frame = CGRectMake(3 + 30 * i , 14 + 42, btn.frame.size.width, btn.frame.size.height);
        
        if (i == 0) {
            btn.isSelected = YES;
            _selectFrameType = btn.type;
        }
        else if(i == lockStartPageIndex){
            btn.isLockFrame = YES;

        }
        [btn addTarget:self action:@selector(onSelectFrame:) forControlEvents:UIControlEventTouchUpInside];
        [self insertSubview:btn atIndex:i];
    }
    
    for (int i = 4 ; i < self.buttonCount; i++) {
        PhotoButtonControl *btn = [[PhotoButtonControl alloc]initWithPhotoButtonType:i];
        btn.frame = CGRectMake(centerButtonWidth + 3 + 30 * i , 14 + 42, btn.frame.size.width, btn.frame.size.height);
        btn.isLockFrame = YES;
        
        [btn addTarget:self action:@selector(onSelectFrame:) forControlEvents:UIControlEventTouchUpInside];
        [self insertSubview:btn atIndex:i];
    }
    
    self.btnCpatureCamera = [CameraButton camerabutton];
    self.btnCpatureCamera.frame = CGRectMake(3 + 30 * 4, 0 + 42, [CameraButton size].width, [CameraButton size].height);
    [self.btnCpatureCamera initCameraButtonImage];
    [self.btnCpatureCamera addTarget:self action:@selector(onCapture:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.btnCpatureCamera];
    

}

/////////////////////////////////////////////////
- (void)stateChangeWithLogInOption:(BOOL)isLogin
{
    if (isLogin == YES) {
        
        for (int i = 0 ; i < self.subviews.count; i++) {
            PhotoButtonControl * subButton = [self.subviews objectAtIndex:i];
            
            if([[self.subviews objectAtIndex:i] isKindOfClass:[PhotoButtonControl class]])
            {
                
                if (subButton.isLockFrame) {
                    subButton.isLockFrame = NO;
                }
                
            }
            
        }

    }
    else{
        // 로그인이 아닌경우 뒤에 5개는 락을 시킨다.
        for (int i = 0 ; i < self.subviews.count; i++) {
            PhotoButtonControl * subButton = [self.subviews objectAtIndex:i];
            
            if([[self.subviews objectAtIndex:i] isKindOfClass:[PhotoButtonControl class]])
            {
                NSInteger lockframeStartIndex = lockStartPageIndex;
                if (i >= lockframeStartIndex) {
                    subButton.isLockFrame = YES;
                }
            }
            
        }
    }
}

// 넘어온 타입이 있는 곳으로 셀렉트
-(void)selectFrameToolBarFrameByType:(enum DolePhotoButtonType)type
{
    _selectFrameType = type;
    
    for (int i = 0 ; i < self.subviews.count; i++) {
        PhotoButtonControl * subButton = [self.subviews objectAtIndex:i];
        
        if([[self.subviews objectAtIndex:i] isKindOfClass:[PhotoButtonControl class]])
        {
            if (subButton.type == type) {
                
                subButton.isSelected = YES;

                
                self.onClickShowLogin(subButton.isLockFrame);


            }
            else{
                subButton.isSelected = NO;
            }
        }
    }
}


//프레임버튼을 터치 했을경우 화면의 프레임을 변경해준다.
- (void)onSelectFrame:(id)sender {
    
    PhotoButtonControl *btn = (PhotoButtonControl*)sender;
    if (btn) {
        
        //CCLOG(@"Button Type %d" ,  btn.type);
        for (int i = 0 ; i < self.subviews.count; i++) {
            
            
            PhotoButtonControl * subButton = [self.subviews objectAtIndex:i];
            
            if([[self.subviews objectAtIndex:i] isKindOfClass:[PhotoButtonControl class]])
            {
                if (subButton == btn) {
                    subButton.isSelected = YES;
                }
                else{
                    subButton.isSelected = NO;
                }
            }
        }
        self.onClickSelectFrameButton(btn);
    }
}

- (void)onCapture:(id)sender {
    self.onClickCenterCameraButton();
}

- (void)setCaptureButtonState:(enum DoleCaptureButtonState)state
{

   if (state == DoleCaptureButtonStateDone) {
        for (int i = 0 ; i < self.subviews.count; i++) {
            PhotoButtonControl * subButton = [self.subviews objectAtIndex:i];
            if([[self.subviews objectAtIndex:i] isKindOfClass:[PhotoButtonControl class]])
            {
                [subButton setHidden:YES];
            }
        }
    }
    else{
        for (int i = 0 ; i < self.subviews.count; i++) {
            PhotoButtonControl * subButton = [self.subviews objectAtIndex:i];
            if([[self.subviews objectAtIndex:i] isKindOfClass:[PhotoButtonControl class]])
            {
                [subButton setHidden:NO];
            }
        }
    }
    

    [_btnCpatureCamera setCameraButtonState:state];
}

@end
