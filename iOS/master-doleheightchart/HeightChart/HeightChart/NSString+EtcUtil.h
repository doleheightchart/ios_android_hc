//
//  NSString+EtcUtil.h
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 4. 21..
//  Copyright (c) 2014년 Kim Sehyun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (EtcUtil)
+(instancetype)stringWithKideHeightWithOutCmUnit:(CGFloat)kidHeight;
+(instancetype)stringWithKideHeightWithCmUnit:(CGFloat)kidHeight;
@end

@interface NSString (NewLineCount)
-(NSInteger)countNewLineCharater;
@end
