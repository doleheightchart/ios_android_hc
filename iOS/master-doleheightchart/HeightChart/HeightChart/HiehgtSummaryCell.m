//
//  HiehgtSummaryCell.m
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 9..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "HiehgtSummaryCell.h"
#import "UIColor+ColorUtil.h"
#import "UIFont+DoleHeightChart.h"
#import "UIView+ImageUtil.h"
#import "NSDate+DoleHeightChart.h"

//#define kSummaryCellHeight ((640-94)/2)
//#define kSummaryCellWidth (108/2)
#define kSummaryLabelMaxHeight 160

@interface SummaryHeightTagView : UIView {
    UIImageView     *_ballonImageView;
    UILabel         *_heightLabel;
}

-(void)updateWithHeight:(CGFloat)kidHeight;

@end

@implementation SummaryHeightTagView
-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self){
        [self initialize];
    }
    return self;
}

-(CGRect)frameBallonWhenOneDigitHeight
{
    return CGRectMake(self.bounds.size.width/2 - (60/2/2) + 1, 0, 60/2, 52/2);
}
-(CGRect)frameBallonWhenTwoDigitHeight
{
    return CGRectMake(self.bounds.size.width/2 - (68/2/2) + 1, 0, 68/2, 52/2);
}
-(CGRect)frameBallonWhenFullDigitHeight
{
    return CGRectMake(self.bounds.size.width/2 - (74/2/2) + 1, 0, 74/2, 52/2);
}
-(CGRect)frameBallonWhenExtraDigitHeight
{
    return CGRectMake(self.bounds.size.width/2 - (104/2/2) + 1, 0, 104/2, 52/2);
}

-(void)initialize
{
//    UIImage *backBalloonImage = [UIImage resizableImageWithName:@"summary_ballon"
//                                                      CapInsets:UIEdgeInsetsMake(0, 3, 0, 3)];
    
//    UIImage *image = [UIImage imageNamed:@"summary_ballon"];
//    UIImage *resizableImage = [image resizableImageWithCapInsets:UIEdgeInsetsMake(0, 20/2, 0, 20/2) resizingMode:UIImageResizingModeStretch];

    _ballonImageView = [[UIImageView alloc]initWithFrame:[self frameBallonWhenExtraDigitHeight]];
//    _ballonImageView.image = backBalloonImage;
//    _ballonImageView.image = resizableImage;
    _ballonImageView.layer.anchorPoint = CGPointMake(0.5, 0.5);
    
    [self addSubview:_ballonImageView];
    
    _heightLabel = [[UILabel alloc]initWithFrame:CGRectMake(_ballonImageView.frame.origin.x, 2, _ballonImageView.bounds.size.width, 10)];
    _heightLabel.backgroundColor = [UIColor clearColor];
    _heightLabel.font = [UIFont defaultBoldFontWithSize:10];
    _heightLabel.textAlignment = NSTextAlignmentCenter;
    _heightLabel.textColor = [UIColor colorWithRGB:0x0a99c8];
    
    [self addSubview:_heightLabel];
}

-(UIImage*)ballonImageWithHeightTextLength:(NSInteger)heightTextLength overHeightMax:(BOOL)overHeight
{
    if (overHeight == FALSE){
        if (heightTextLength < 2){
            return [UIImage imageNamed:@"summary_balloon_01"];
        }
        else if (heightTextLength < 3){
            return [UIImage imageNamed:@"summary_balloon_02"];
        }
        else if (heightTextLength < 4){
            return [UIImage imageNamed:@"summary_balloon_03"];
        }
        else {
            return [UIImage imageNamed:@"summary_balloon_04"];
        }
    }
    else{
        if (heightTextLength < 2){
            return [UIImage imageNamed:@"summary_balloon_down_01"];
        }
        else if (heightTextLength < 3){
            return [UIImage imageNamed:@"summary_balloon_down_02"];
        }
        else if (heightTextLength < 4){
            return [UIImage imageNamed:@"summary_balloon_down_03"];
        }
        else {
            return [UIImage imageNamed:@"summary_balloon_down_04"];
        }
    }
}

-(void)updateWithHeight:(CGFloat)kidHeight
{
    NSInteger intHeightValue = kidHeight;
    CGFloat fm = fmodf(kidHeight, 1);
    BOOL overHeight = kidHeight >= kSummaryLabelMaxHeight;
    NSString *stringText = [NSString stringWithFormat:@"%.1f", kidHeight];
    
    if (fm == 0){
        stringText = [NSString stringWithFormat:@"%ld", (long)intHeightValue, nil];
    }
    else{
        stringText = [NSString stringWithFormat:@"%.1f", kidHeight, nil];
    }
    
    if (stringText.length < 2){
        _ballonImageView.frame = [self frameBallonWhenOneDigitHeight];
        _ballonImageView.image = [self ballonImageWithHeightTextLength:1 overHeightMax:overHeight];
    }
    else if (stringText.length < 3){
        _ballonImageView.frame = [self frameBallonWhenTwoDigitHeight];
        _ballonImageView.image = [self ballonImageWithHeightTextLength:2 overHeightMax:overHeight];
    }
    else if (stringText.length < 4){
        _ballonImageView.frame = [self frameBallonWhenFullDigitHeight];
        _ballonImageView.image = [self ballonImageWithHeightTextLength:3 overHeightMax:overHeight];
        
    }
    else{
        _ballonImageView.frame = [self frameBallonWhenExtraDigitHeight];
        _ballonImageView.image = [self ballonImageWithHeightTextLength:4 overHeightMax:overHeight];
    }

    
    _heightLabel.frame = CGRectMake(_ballonImageView.frame.origin.x,
                                    overHeight ? 12 : 4,
                                    _ballonImageView.bounds.size.width,
                                    10);
    
    if (fm == 0){
        stringText = [NSString stringWithFormat:@"%ldcm", (long)intHeightValue, nil];
    }
    else{
        stringText = [NSString stringWithFormat:@"%.1fcm", kidHeight, nil];
    }
    _heightLabel.text = stringText;
    
//    if (fm == 0){
//        _heightLabel.text = [NSString stringWithFormat:@"%ldcm", (long)intHeightValue, nil];
//    }
//    else{
//        _heightLabel.text = [NSString stringWithFormat:@"%.1fcm", kidHeight, nil];
//    }
}
@end

@interface HiehgtSummaryCell (){
    UIImageView     *_topPhotoImageView;
    UIImageView     *_shadowImageView;
    UILabel         *_takenDateLabel;
    UILabel         *_heightLabel;
    UIView          *_verticalLineView;
    SummaryHeightTagView *_heightTagView;
    UIImageView     *_standardMarkImageView;
}

@end

@implementation HiehgtSummaryCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initSummaryCell];
    }
    return self;
}

-(CGFloat)heightWithKidHeight:(CGFloat)kidHeight
{
    CGFloat minHeight = (138/2)-1;
    CGFloat maxHeight = (640-170)/2-1;
    
    CGFloat rangeHeight = maxHeight - minHeight;
    
    CGFloat minH = self.bounds.size.height - minHeight;
    CGFloat maxH = self.bounds.size.height - maxHeight;
    
    if (kidHeight < 50) return minH;
    if (kidHeight > 170) return maxH;
    
    CGFloat h = minHeight + rangeHeight * ( (kidHeight-50.0) / 120.0);
    
    h = MAX(minHeight, h);
    h = MIN(maxHeight, h);
    
    CGFloat heightOfView = self.bounds.size.height - h;
    
    return heightOfView;
}

-(CGRect)frameFaceWithKidHeight:(CGFloat)kidHeight
{
    CGFloat h = [self heightWithKidHeight:kidHeight];
    
    CGRect frameFace = CGRectMake(0,
                                  h - (110/2/2),
                                  108/2,
                                  110/2);
    
    return frameFace;
}

-(CGRect)frameVerticalLineWithKidHeight:(CGFloat)kidHeight
{
    CGFloat h = [self heightWithKidHeight:kidHeight];
    
    CGRect frameVertical = CGRectMake(self.bounds.size.width/2,
                                      h,
                                      (4/2),
                                      (self.bounds.size.height-h) - (52/2));
    return frameVertical;
}

-(CGRect)frameHeightTag:(CGFloat)kidHeight
{
    CGRect frameFace = [self frameFaceWithKidHeight:kidHeight];

    if (kidHeight < kSummaryLabelMaxHeight){
    
        frameFace.origin.y -= (48/2);
        frameFace.origin.x += (21/2);
        frameFace.size.height = (48/2);
        frameFace.size.width = (66/2);
        
        return frameFace;
    }
    else {
        frameFace.origin.y += frameFace.size.height;
        frameFace.origin.x += (21/2);
        frameFace.size.height = (48/2);
        frameFace.size.width = (66/2);
        
        return frameFace;

    }
}


-(void)initSummaryCell
{
    self.backgroundColor = [UIColor clearColor];
    
    CGRect frameVertical = [self frameVerticalLineWithKidHeight:0];
    _verticalLineView = [[UIView alloc]initWithFrame:frameVertical];
    _verticalLineView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"graph_line_vertical"]];
    [self addSubview:_verticalLineView];
    
    CGRect frameFace = [self frameFaceWithKidHeight:0];
    _topPhotoImageView = [[UIImageView alloc]initWithFrame:frameFace];
    
//    _topPhotoImageView.layer.shadowRadius = 3;
//    _topPhotoImageView.layer.borderWidth = 0.5;
//    _topPhotoImageView.layer.borderColor = [UIColor whiteColor].CGColor;
//    _topPhotoImageView.layer.shadowColor = [UIColor blackColor].CGColor;
//    _topPhotoImageView.layer.shadowOffset = CGSizeMake(1, 1);
//    _topPhotoImageView.layer.shadowOpacity = 0.5;
//    _topPhotoImageView.layer.cornerRadius = frameFace.size.width/2;
    
    [self addSubview:_topPhotoImageView];
    _shadowImageView = [[UIImageView alloc]initWithFrame:frameFace];
    _shadowImageView.image = [UIImage imageNamed:@"summary_sticker_shadow"];
    [self addSubview:_shadowImageView];
    
    _heightTagView = [[SummaryHeightTagView alloc] initWithFrame:[self frameHeightTag:0]];
    [self addSubview:_heightTagView];
    
    CGRect frameTakenDate = CGRectMake(0,
                                       self.bounds.size.height - (38/2),
                                       self.bounds.size.width,
                                       20/2);
    
    _takenDateLabel = [[UILabel alloc]initWithFrame:frameTakenDate];
    _takenDateLabel.backgroundColor = [UIColor clearColor];
    _takenDateLabel.font = [UIFont defaultBoldFontWithSize:16/2];
    _takenDateLabel.textAlignment = NSTextAlignmentCenter;
    _takenDateLabel.textColor = [UIColor colorWithRGB:0x9453e4];
    [self addSubview:_takenDateLabel];

}

-(void)setKidHeightDetail:(KidHeightDetail *)kidHeightDetail
{
    _kidHeightDetail = kidHeightDetail;
    
    CGFloat kidHeight = _kidHeightDetail.representDetail.kidHeight;
    
    _verticalLineView.frame = [self frameVerticalLineWithKidHeight:kidHeight];
    
    CGRect frameFacePhoto = [self frameFaceWithKidHeight:kidHeight];
    
    _topPhotoImageView.frame = frameFacePhoto;
    _shadowImageView.frame = frameFacePhoto;
    
    _heightTagView.frame = [self frameHeightTag:kidHeight];
    [_heightTagView updateWithHeight:kidHeight];
    
//    UIImage *maskedPhoto = [UIImage summaryFaceImage:_kidHeightDetail.representDetail.image];
//    _topPhotoImageView.image = maskedPhoto;
    
    _topPhotoImageView.image = _kidHeightDetail.representDetail.childHeight.thumbphoto;
//    _topPhotoImageView.image = _kidHeightDetail.representDetail.childHeight.faceCropImage;
    
    _takenDateLabel.text = [_kidHeightDetail.representDetail.takenDate stringDefaultFormat];
    
    self.heightValue = [self heightWithKidHeight:kidHeight];

}
-(void)updateWithHeightDetail:(HeightDetailInfo*)detail
{
    CGFloat kidHeight = detail.kidHeight;
    
    _verticalLineView.frame = [self frameVerticalLineWithKidHeight:kidHeight];
    
    CGRect frameFacePhoto = [self frameFaceWithKidHeight:kidHeight];
    
    _topPhotoImageView.frame = frameFacePhoto;
    _shadowImageView.frame = frameFacePhoto;
    
    _heightTagView.frame = [self frameHeightTag:kidHeight];
    [_heightTagView updateWithHeight:kidHeight];
    
    UIImage *maskedPhoto = [UIImage summaryFaceImage:detail.image];
    _topPhotoImageView.image = maskedPhoto;
    
    _takenDateLabel.text = [detail.takenDate stringDefaultFormat];
    
    self.heightValue = [self heightWithKidHeight:kidHeight];
}

-(void)updateStandardHeightMark:(BOOL)isVisible standardHeight:(CGFloat)kidHeight
{
    if (NO == isVisible){
        [_standardMarkImageView removeFromSuperview];
        _standardMarkImageView = nil;
    }
    else {
        if (nil == _standardMarkImageView){
            _standardMarkImageView = [[UIImageView alloc]init];
            _standardMarkImageView.image = [UIImage imageNamed:@"standard_dot"];
            [self insertSubview:_standardMarkImageView atIndex:0];
        }
        
        CGFloat h = [self heightWithKidHeight:kidHeight];
        CGRect f = CGRectMake(self.bounds.size.width/2 - 3 + 1, //보정
                              h-3,
                              6,
                              6);
        
        _standardMarkImageView.frame = f;
    }
}

@end
