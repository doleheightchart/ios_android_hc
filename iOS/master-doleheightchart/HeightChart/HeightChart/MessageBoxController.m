//
//  MessageBoxController.m
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 4. 8..
//  Copyright (c) 2014년 Kim Sehyun. All rights reserved.
//

#import "MessageBoxController.h"
#import "UIFont+DoleHeightChart.h"
#import "UIColor+ColorUtil.h"
#import "UIViewController+DoleHeightChart.h"
#import "UIView+ImageUtil.h"
#import "UIView+Animation.h"
#import "NSString+EtcUtil.h"


@interface MessageBoxController (){
    //Body
    UIView *_containerPopupView;

    //Decorate ImageView
    UIImageView *_iconView;
    UIImageView *_bodyView;
    UIImageView *_bottomView;
    
    //Message
    UILabel     *_messageLabel;
    
    //Buttons
    UIButton    *_cancelButton;
    UIButton    *_okButton;
}

@property (nonatomic, readonly) UIButton *leftButton;
@property (nonatomic, readonly) UIButton *rightButton;

@end

@implementation MessageBoxController

-(void)setOverlayStyle
{
    UIColor *overlayColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6];
    self.backgroundColor = overlayColor;
}

- (void)createPopup
{
    [self setOverlayStyle];
    
//    [self createUI];
    [self createUIWithMessage:self.message];
    
    _messageLabel.text = self.message;
    
    [_containerPopupView animateScaleEffectWithDuration:0.3];
}

-(void)createUI
{
    CGSize size = self.bounds.size;
    CGFloat w = size.width;
    CGFloat h = size.height;
    
    CGFloat formWidth = 544/2;
    CGFloat formHeight = (280+90)/2;
    
//    UIEdgeInsets topBarInsets = UIEdgeInsetsMake(0, (22.0/2.0), 0, (22.0/2.0));
    UIEdgeInsets bottomBarInsets = UIEdgeInsetsMake(0, (22.0/2.0), 0, (22.0/2.0));
    UIEdgeInsets notitleBodyView = UIEdgeInsetsMake((18.0/2.0), 0, 0, 0);
    
    _containerPopupView = [[UIView alloc]initWithFrame:CGRectMake((w-formWidth)/2,
                                                                  (h-formHeight)/2,
                                                                  formWidth,
                                                                  formHeight)];
    [self addSubview:_containerPopupView];
    
    _bodyView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, formWidth, 280/2)];
    _bodyView.image = [UIImage resizableImageWithName:@"popup_no_title"
                                                       CapInsets:notitleBodyView];
    [_containerPopupView addSubview:_bodyView];
    
    _bottomView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 280/2, formWidth, 90/2 - 1)];
    _bottomView.image = [UIImage resizableImageWithName:@"poup_bottom"
                                            CapInsets:bottomBarInsets];
    [_containerPopupView addSubview:_bottomView];
    
    CGFloat firstLineY = 28/2;
    CGFloat messageHeight = (130+16+38+8+38)/2;
    
    if (NO == self.hiddenIcon){
        _iconView = [[UIImageView alloc]initWithFrame:CGRectMake((formWidth-(130/2))/2, 28/2, 130/2, 130/2)];
        _iconView.image = [UIImage imageNamed:@"popup_warning"];
        [_containerPopupView addSubview:_iconView];
        
        firstLineY = (28+130+16)/2;
        messageHeight = (38+8+38)/2;
    }
    
    _messageLabel = [[UILabel alloc]initWithFrame:CGRectMake(18/2, firstLineY, formWidth-18, messageHeight)];
    _messageLabel.backgroundColor = [UIColor clearColor];
    _messageLabel.textAlignment = NSTextAlignmentCenter;
    _messageLabel.textColor = [UIColor colorWithRGB:0x787878];
    [self setuplabelFont:_messageLabel];
    _messageLabel.numberOfLines = 5;
    _messageLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [_containerPopupView addSubview:_messageLabel];
    
    CGRect frameOneButton = CGRectMake(10/2, formHeight - (80/2) , formWidth-10, (90-20)/2);
    CGRect frameLeftButton = CGRectMake(10/2, formHeight - (80/2) , (258/2), (90-20)/2);
    CGRect frameRightButton = CGRectMake((10+258+8)/2, formHeight - (80/2) , (258/2), (90-20)/2);
    
    
    if (self.buttonType != kMessageBoxButtonTypeCancel){
        _okButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _okButton.frame = CGRectMake(0, 0, 0, 0);
        [_okButton setAttributedTitle:nil forState:UIControlStateNormal];
        [_okButton setResizableImageWithType:DoleButtonImageTypePopupRed];
        [_containerPopupView addSubview:_okButton];
        [_okButton addTarget:self action:@selector(clickOKButton:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    if (self.buttonType != kMessageBoxButtonTypeOK){
        _cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _cancelButton.frame = CGRectMake(0, 0, 0, 0);
        [_cancelButton setResizableImageWithType:DoleButtonImageTypePopupOrange];
        [_containerPopupView addSubview:_cancelButton];
        [_cancelButton addTarget:self action:@selector(clickCancelButton:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    switch (self.buttonType) {
        case kMessageBoxButtonTypeOK: _okButton.frame = frameOneButton;
            [_okButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [_okButton setTitle:NSLocalizedString(@"OK", @"") forState:UIControlStateNormal];
            break;
        case kMessageBoxButtonTypeCancel: _cancelButton.frame = frameOneButton;
            [_cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [_cancelButton setTitle:NSLocalizedString(@"Cancel", @"") forState:UIControlStateNormal];
            break;
        case kMessageBoxButtonTypeOKCancel:
        case kMessageBoxButtonTypeYesNo:
            [_okButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [_okButton setTitle:NSLocalizedString(@"OK", @"") forState:UIControlStateNormal];
            [_cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [_cancelButton setTitle:NSLocalizedString(@"Cancel", @"") forState:UIControlStateNormal];
            _cancelButton.frame = frameLeftButton;
            _okButton.frame = frameRightButton;
            break;
    }
}

-(void)createContainerViewWithBounds:(CGRect)bounds
{
    CGSize size = self.bounds.size;
    CGFloat w = size.width;
    CGFloat h = size.height;
    
    CGFloat formWidth = bounds.size.width; //544/2;
    CGFloat formHeight = bounds.size.height;  //(280+90)/2;
    
    UIEdgeInsets bottomBarInsets = UIEdgeInsetsMake(0, (22.0/2.0), 0, (22.0/2.0));
    UIEdgeInsets notitleBodyView = UIEdgeInsetsMake((18.0/2.0), 0, 0, 0);
    
    _containerPopupView = [[UIView alloc]initWithFrame:CGRectMake((w-formWidth)/2,
                                                                  (h-formHeight)/2,
                                                                  formWidth,
                                                                  formHeight)];
    [self addSubview:_containerPopupView];
    
    CGFloat bodyHeight = formHeight - 45.0;
    _bodyView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, formWidth, bodyHeight)];
    _bodyView.image = [UIImage resizableImageWithName:@"popup_no_title"
                                            CapInsets:notitleBodyView];
    [_containerPopupView addSubview:_bodyView];
    
    
    _bottomView = [[UIImageView alloc]initWithFrame:CGRectMake(0, bodyHeight, formWidth, 45.0 - 1.0)];
    _bottomView.image = [UIImage resizableImageWithName:@"poup_bottom"
                                              CapInsets:bottomBarInsets];
    [_containerPopupView addSubview:_bottomView];
}

-(void)createIcon
{
    CGRect formFrame = _containerPopupView.bounds;
    _iconView = [[UIImageView alloc]initWithFrame:CGRectMake((formFrame.size.width-(130/2))/2,
                                                             28/2,
                                                             130/2,
                                                             130/2)];
    _iconView.image = [UIImage imageNamed:@"popup_warning"];
    [_containerPopupView addSubview:_iconView];
}

-(void)createMessageLabel
{
    CGRect formRect = _containerPopupView.bounds;
    
    CGFloat posY = 28/2;
    CGFloat messageHeight = formRect.size.height - posY - 45 - 12;
    
    if (NO == self.hiddenIcon){
        posY = (28+130+16)/2;
        messageHeight -= (130+16)/2;
    }
    
    _messageLabel = [[UILabel alloc]initWithFrame:CGRectMake(18/2, posY, formRect.size.width-18, messageHeight)];
    _messageLabel.backgroundColor = [UIColor clearColor];
    _messageLabel.textAlignment = NSTextAlignmentCenter;
    _messageLabel.textColor = [UIColor colorWithRGB:0x787878];
    [self setuplabelFont:_messageLabel];
    _messageLabel.numberOfLines = 5;
    _messageLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [_containerPopupView addSubview:_messageLabel];
}

-(void)createButtons
{
    CGRect formRect = _containerPopupView.bounds;
    CGFloat formHeight = formRect.size.height;
    CGFloat formWidth = formRect.size.width;
    
    CGRect frameOneButton = CGRectMake(10/2, formHeight - (80/2) , formWidth-10, (90-20)/2);
    CGRect frameLeftButton = CGRectMake(10/2, formHeight - (80/2) , (258/2), (90-20)/2);
    CGRect frameRightButton = CGRectMake((10+258+8)/2, formHeight - (80/2) , (258/2), (90-20)/2);
    
    
    if (self.buttonType != kMessageBoxButtonTypeCancel){
        _okButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _okButton.frame = CGRectMake(0, 0, 0, 0);
        [_okButton setAttributedTitle:nil forState:UIControlStateNormal];
        [_okButton setResizableImageWithType:DoleButtonImageTypePopupRed];
        [_containerPopupView addSubview:_okButton];
        [_okButton addTarget:self action:@selector(clickOKButton:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    if (self.buttonType != kMessageBoxButtonTypeOK){
        _cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _cancelButton.frame = CGRectMake(0, 0, 0, 0);
        [_cancelButton setResizableImageWithType:DoleButtonImageTypePopupOrange];
        [_containerPopupView addSubview:_cancelButton];
        [_cancelButton addTarget:self action:@selector(clickCancelButton:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    switch (self.buttonType) {
        case kMessageBoxButtonTypeOK: _okButton.frame = frameOneButton;
            [_okButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [_okButton setTitle:NSLocalizedString(@"OK", @"") forState:UIControlStateNormal];
            break;
        case kMessageBoxButtonTypeCancel: _cancelButton.frame = frameOneButton;
            [_cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [_cancelButton setTitle:NSLocalizedString(@"Cancel", @"") forState:UIControlStateNormal];
            break;
        case kMessageBoxButtonTypeOKCancel:
            [_okButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [_okButton setTitle:NSLocalizedString(@"OK", @"") forState:UIControlStateNormal];
            [_cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [_cancelButton setTitle:NSLocalizedString(@"Cancel", @"") forState:UIControlStateNormal];
            _cancelButton.frame = frameLeftButton;
            _okButton.frame = frameRightButton;
            break;
        case kMessageBoxButtonTypeYesNo:
            [_okButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [_okButton setTitle:NSLocalizedString(@"Yes", @"") forState:UIControlStateNormal];
            [_cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [_cancelButton setTitle:NSLocalizedString(@"No", @"") forState:UIControlStateNormal];
            _cancelButton.frame = frameLeftButton;
            _okButton.frame = frameRightButton;
            break;
    }
}

-(void)createUIWithMessage:(NSString*)message
{
    NSInteger lineCount = [message countNewLineCharater];
    
    lineCount = MAX(2, lineCount);
    
    CGFloat w = (544/2);
    CGFloat h = 0;
    if (self.hiddenIcon){
        h = 28 + (38*lineCount) + (8*(lineCount-1)) + 24;
        h += 90; //bottom
        h /= 2.0; //point
    }
    else{
        h = 28 + 130 + 16; //icon
        h += ( (38*lineCount) + (8*(lineCount-1)) + 24 );
        h += 90; //bottom
        h /= 2.0; //point
    }
    
    [self createContainerViewWithBounds:CGRectMake(0, 0, w, h)];
    if (NO == self.hiddenIcon) [self createIcon];
    [self createMessageLabel];
    [self createButtons];
}

//-(void)createUIWithContentHeight:(CGFloat)contentHeight
//{
//    CGSize size = self.bounds.size;
//    CGFloat w = size.width;
//    CGFloat h = size.height;
//    
//    self createContainerViewWithFrame:
//}

-(void)setuplabelFont:(UILabel*)label
{
    label.font = [UIFont defaultKoreanFontWithSize:30/2];
}


-(void)clickOKButton:(UIButton*)button
{
    
    
    if (self.okCancelCompletion){
        self.okCancelCompletion(YES);

    }
    if (self.confirmCompletion){
        self.confirmCompletion();

    }
    
    [self closePopup];
}

-(void)clickCancelButton:(UIButton*)button
{
    if (self.okCancelCompletion){
        self.okCancelCompletion(NO);
    }
    if (self.confirmCompletion){
        self.confirmCompletion();
    }
    
    [self closePopup];
}

-(void)closePopup
{
    self.lockedView.userInteractionEnabled = YES;
    
    [self removeFromSuperview];
}

-(UIButton*)leftButton
{
    return _cancelButton;
}

-(UIButton*)rightButton
{
    return _okButton;
}

+(void)showOKConfirmMessage:(NSString*)message onView:(UIView*)view completion:(void(^)())completion Icon:(BOOL)useIcon
{
    MessageBoxController *vc = [[MessageBoxController alloc]initWithFrame:view.bounds];
    vc.message = message;
    vc.hiddenIcon = !useIcon;
    vc.buttonType = kMessageBoxButtonTypeOK;
    vc.confirmCompletion = completion;
    vc.lockedView = view;
    [vc createPopup];
    [view.superview addSubview:vc];
}

+(void)showCancelConfirmMessage:(NSString*)message onView:(UIView*)view completion:(void(^)())completion Icon:(BOOL)useIcon
{
    MessageBoxController *vc = [[MessageBoxController alloc]initWithFrame:view.bounds];
    vc.message = message;
    vc.hiddenIcon = !useIcon;
    vc.buttonType = kMessageBoxButtonTypeCancel;
    vc.confirmCompletion = completion;
    vc.lockedView = view;
    [vc createPopup];
    [view.superview addSubview:vc];
}

+(void)showOkCancelConfirmMessage:(NSString*)message onView:(UIView*)view completion:(void(^)(BOOL))completion Icon:(BOOL)useIcon
{
    MessageBoxController *vc = [[MessageBoxController alloc]initWithFrame:view.bounds];
    vc.message = message;
    vc.hiddenIcon = !useIcon;
    vc.buttonType = kMessageBoxButtonTypeOKCancel;
    vc.okCancelCompletion = completion;
    vc.lockedView = view;
    [vc createPopup];
    [view.superview addSubview:vc];
}

+(void)showYesNoConfirmMessage:(NSString*)message onView:(UIView*)view completion:(void(^)(BOOL))completion Icon:(BOOL)useIcon
{
    MessageBoxController *vc = [[MessageBoxController alloc]initWithFrame:view.bounds];
    vc.message = message;
    vc.hiddenIcon = !useIcon;
    vc.buttonType = kMessageBoxButtonTypeYesNo;
    vc.okCancelCompletion = completion;
    vc.lockedView = view;
    [vc createPopup];
    [view.superview addSubview:vc];
}


+(void)showLogInCancelConfirmMessage:(NSString*)message onView:(UIView*)view completion:(void(^)(BOOL))completion Icon:(BOOL)useIcon
{
    MessageBoxController *vc = [[MessageBoxController alloc]initWithFrame:view.bounds];
    vc.message = message;
    vc.hiddenIcon = !useIcon;
    vc.buttonType = kMessageBoxButtonTypeOKCancel;
    vc.okCancelCompletion = completion;
    vc.lockedView = view;
    [vc createPopup];
    [vc.rightButton setTitle:NSLocalizedString(@"LogIn", @"") forState:UIControlStateNormal];
    [view.superview addSubview:vc];
}

@end

@implementation UIViewController(MessageBoxController)

-(void)showOKConfirmMessage:(NSString*)message completion:(void(^)())completion
{
    [self showOKConfirmMessage:message completion:completion Icon:YES];
}

-(void)showCancelConfirmMessage:(NSString*)message completion:(void(^)())completion
{
    [self showCancelConfirmMessage:message completion:completion Icon:YES];
}
-(void)showOkCancelConfirmMessage:(NSString*)message completion:(void(^)(BOOL))completion
{
    [self showOkCancelConfirmMessage:message completion:completion Icon:YES];
}
-(void)showYesNoConfirmMessage:(NSString*)message completion:(void(^)(BOOL))completion
{
    [self showYesNoConfirmMessage:message completion:completion Icon:YES];
}
-(void)showLogInCancelConfirmMessage:(NSString*)message completion:(void(^)(BOOL))completion
{
    [self showLogInCancelConfirmMessage:message completion:completion Icon:YES];
}


-(void)showOKConfirmMessage:(NSString*)message completion:(void(^)())completion Icon:(BOOL)useIcon
{
    [MessageBoxController showOKConfirmMessage:message onView:self.view completion:completion Icon:useIcon];

}
-(void)showCancelConfirmMessage:(NSString*)message completion:(void(^)())completion Icon:(BOOL)useIcon
{
    [MessageBoxController showCancelConfirmMessage:message onView:self.view completion:completion Icon:useIcon];

}
-(void)showOkCancelConfirmMessage:(NSString*)message completion:(void(^)(BOOL))completion Icon:(BOOL)useIcon
{
    [MessageBoxController showOkCancelConfirmMessage:message onView:self.view completion:completion Icon:useIcon];

}
-(void)showYesNoConfirmMessage:(NSString*)message completion:(void(^)(BOOL))completion Icon:(BOOL)useIcon
{
    [MessageBoxController showYesNoConfirmMessage:message onView:self.view completion:completion Icon:useIcon];
}
-(void)showLogInCancelConfirmMessage:(NSString*)message completion:(void(^)(BOOL))completion Icon:(BOOL)useIcon
{
    [MessageBoxController showLogInCancelConfirmMessage:message onView:self.view completion:completion Icon:useIcon];
}

@end
