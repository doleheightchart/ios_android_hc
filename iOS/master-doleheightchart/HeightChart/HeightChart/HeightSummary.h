//
//  HeightSummary.h
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 9..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HeightDetailTreeProtocol.h"
#import "MyChild.h"

@interface HeightSummary : UIControl<UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, weak) id<HeightDetailDataSource> heightDetailDataSource;

@property (nonatomic) BOOL visibleStandardHeightLine;

@property (nonatomic, weak) MyChild* myChild;

-(void)reloadData;
-(void)updateGraph;
-(void)initControl;
-(void)shownCellWithIndex:(NSUInteger)index;

@end
