//
//  InputHeightBackground.h
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 11..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InputHeightBackground : UIView
-(void)startScrolling;
-(void)startMovingAnimation;
-(void)loadAnimals;
@end
