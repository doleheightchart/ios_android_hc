//
//  DoleMonkeyTree.m
//  HeightChart
//
//  Created by ne on 2014. 3. 18..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "DoleMonkeyTree.h"
#import "DoleNameTag.h"
#import "UIView+Animation.h"
#import "DoleInfo.h"

@interface DoleMonkeyTree()
{
    UIImageView *_monkeyTreeBaseView;
    UIImageView *_monkeyView;
    UIImageView *_fruitView;
    UIImage *_monkeyDeleteImage;
    UIImage *_monkeySeletctImage;
    UIImage *_monkeyNormalImage;
    DoleNameTag *_nameTag;
}
@end

@implementation DoleMonkeyTree

CGFloat kMonkeyTreeViewHeight = 650 + 101 + 64; // tree Height + Monkey Margin  + Max NameTag Margin
CGFloat kMonkeyTreeViewWidth = 230;

CGFloat kMonkeyWidth = 185.0;
CGFloat kMonkeyHeight = 254.0;

CGFloat kNameTagDefalutMarginPosY = 32;
CGFloat kNameTagMaxHeightMarginPosY = 11;

NSString *kMonkeyNameList[] = {@"owl",@"dolekey", @"rilla", @"smong", @"faka", @"panzee"};
NSString *kMonkeyAnimationNameFormat = @"%02d_%@_%02d";
NSString *kDeleteMonkeyImageFormat = @"delete_monkey_%02d";
NSString *kSelectMonkeyImageFormat = @"delete_select_monkey_%02d";



- (id)initWithFrame:(CGRect)frame doleMonkeyType:(enum DoleMonkeyType)type
{
    self = [super initWithFrame:frame doleMonkeyType:type];
    {
        [self createImageView];
    }
    return self;
}

- (void)createImageView{

    NSString* monkeyTreeName = [NSString stringWithFormat:@"main_monkey_tree_%02d", self.monkeyType];
    NSString* monkeyFruitName = [NSString stringWithFormat:@"main_monkey_fruit_%02d", self.monkeyType];
    NSString* monkeyDefualtName = [NSString stringWithFormat:kMonkeyAnimationNameFormat, self.monkeyType,
                                   kMonkeyNameList[self.monkeyType], 00, nil];
    
    
    _monkeyNormalImage = [UIImage imageNamed:monkeyDefualtName];
    _monkeySeletctImage = [UIImage imageNamed:[NSString stringWithFormat:kSelectMonkeyImageFormat, self.monkeyType]];
    _monkeyDeleteImage = [UIImage imageNamed:[NSString stringWithFormat:kDeleteMonkeyImageFormat, self.monkeyType]];
    
    _monkeyTreeBaseView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:monkeyTreeName]];
    _fruitView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:monkeyFruitName]];
    _monkeyView = [[UIImageView alloc]initWithImage:_monkeyNormalImage];

    _monkeyTreeBaseView.frame = CGRectMake(0, (101 + 64)/2, kMonkeyTreeViewWidth/2, 650/2);
    _monkeyView.frame = CGRectMake(23/2, _monkeyTreeBaseView.frame.origin.y - 101/2, kMonkeyWidth/2, kMonkeyHeight/2);
    _fruitView.frame = CGRectMake(70/2, _monkeyTreeBaseView.frame.origin.y + 59/2, 106/2, 88/2);
    
    [self addSubview:_monkeyTreeBaseView];
    [self addSubview:_monkeyView];
    [self addSubview:_fruitView];
    
    
    
    NSMutableString * aniFormat = [NSMutableString stringWithFormat:@"%02d_%@_",
                                   self.monkeyType, kMonkeyNameList[self.monkeyType]];
    [aniFormat appendString:@"%02d"];
    
    [_monkeyView addFrameImagesWithNameFormat:aniFormat frameCount:16 duration:2.0 repeat:1 animationStart:NO];
    
    UILongPressGestureRecognizer *longpressRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self
                                                                                                      action:@selector(longPressedMonkey:)];
    [_monkeyView addGestureRecognizer:longpressRecognizer];
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                    action:@selector(tapMonkey:)];
    [_monkeyView addGestureRecognizer:tapRecognizer];


    CGFloat nameTagMaxWidth = 191/2;
    _nameTag = [[DoleNameTag alloc]initWithFrame:CGRectMake(_monkeyTreeBaseView.frame.size.width/2 - nameTagMaxWidth/2,
                                                            _monkeyView.frame.origin.y - 32/2
                                                            , nameTagMaxWidth, 72/2)];

    [_nameTag addNickName:@"noname" withMonkeyType:self.monkeyType];
    [self addSubview:_nameTag];
    
    self.userInteractionEnabled = YES ;
    _monkeyView.userInteractionEnabled = YES ;
    
    _nameTag.transform = CGAffineTransformMakeScale(0.0, 0.0);
    
}


- (void)playMonkeyAnimation
{
    [_monkeyView startAnimating];
}

-(void)longPressedMonkey:(UILongPressGestureRecognizer*)recognizer
{
    if (recognizer.state == UIGestureRecognizerStateEnded) {
        
        NSLog(@"UIGestureRecognizerStateEnded");
        if (self.isDeleteMode) {
          self.isChecked = YES;
        }

        
    }
    else if (recognizer.state == UIGestureRecognizerStateBegan){
        NSLog(@"UIGestureRecognizerStateBegan.");
        
        if (self.isDeleteMode == NO) {
            [self.clickMonkeyDelegate clickLongPress:self];
           
        }
    }
}

- (void)tapMonkey:(UITapGestureRecognizer*)recognizer
{
    if (recognizer.state == UIGestureRecognizerStateEnded) {
        
    
        if (self.isDeleteMode ) {
            self.isChecked = !self.isChecked;
            [self.clickMonkeyDelegate didCheckedMonkeyWithData:self Checked:self.isChecked ];

        }
        else{
            // 삭제모드가 아닌경우는 다른 화면으로 이동한다.
            [self.clickMonkeyDelegate clickMonkeyWithData:self];
        }
        
        CGPoint pt = [recognizer locationInView:self];
        
        if (CGRectContainsPoint(_monkeyView.frame, pt)){
            NSLog(@"MonkeyRange Touch");
        }
    }
    else if (recognizer.state == UIGestureRecognizerStateBegan){
        
    }
}


- (void)changeMonkeytreeType
{
    [_monkeyView stopAnimating];

    [UIView transitionWithView:_monkeyView duration:0.3
                       options:(UIViewAnimationOptionTransitionCrossDissolve)
                    animations:^{
                        
                        _monkeyNormalImage = [UIImage imageNamed:[NSString stringWithFormat:kMonkeyAnimationNameFormat, self.monkeyType,
                                                                  kMonkeyNameList[self.monkeyType], 00, nil]];
                        
                        
                        _monkeySeletctImage = [UIImage imageNamed:[NSString stringWithFormat:kSelectMonkeyImageFormat, self.monkeyType]];
                        _monkeyDeleteImage = [UIImage imageNamed:[NSString stringWithFormat:kDeleteMonkeyImageFormat, self.monkeyType]];
                        
                        _monkeyView.image = _monkeyNormalImage;
                        
                        _monkeyTreeBaseView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"main_monkey_tree_%02d", self.monkeyType]]];
                        _fruitView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"main_monkey_fruit_%02d", self.monkeyType]]];
                        
                        
                        NSMutableString * aniFormat = [NSMutableString stringWithFormat:@"%02d_%@_",
                                                       self.monkeyType, kMonkeyNameList[self.monkeyType]];
                        [aniFormat appendString:@"%02d"];
                        
                        [_monkeyView addFrameImagesWithNameFormat:aniFormat frameCount:16 duration:2.0 repeat:1 animationStart:NO];
                        
                        [self playMonkeyAnimation];
                        
                        
                    } completion:^(BOOL finished) {
                        
                                                self.nickname = self.nickname;

                    }];
}



- (void)setIsDeleteMode:(BOOL)isDeleteMode
{
    _isDeleteMode = isDeleteMode;
    
    [_monkeyView stopAnimating];
    assert(_monkeyDeleteImage != nil);
    assert(_monkeyNormalImage != nil);
    assert(_monkeySeletctImage != nil);
    
    if (_isDeleteMode){
        
        // change images
        _monkeyView.image = _monkeyDeleteImage;
//        _nameTag.isDeleteMode = YES;
        NSLog(@"Monkey Tree isDelteMode YES");
    }
    else{
        self.isChecked = NO;
        
        // change images
        
        _monkeyView.image = _monkeyNormalImage;
        _nameTag.isDeleteMode = NO;
        NSLog(@"Monkey Tree isDelteMode NO");
    }
}

- (void)setIsChecked:(BOOL)isChecked
{
    _isChecked = isChecked;
    
    if(!self.isDeleteMode)return;
    
    if (_isChecked) {
         _monkeyView.image = _monkeySeletctImage;
        _nameTag.isDeleteMode = YES;
    }
    else{
         _monkeyView.image = _monkeyDeleteImage;
        _nameTag.isDeleteMode = NO;

    }
    
}



- (void)setHeight:(CGFloat)height
{
    
    if (_height == height) {
        return;
    }
    _nameTag.height = _height;
    _height = height;
    
    if (_nameTag.transform.a != 0) {
        [self moveNameTagPosY];
    }
    
}

- (void)setNickname:(NSString *)nickname
{
    _nickname = nickname;
    if (_nameTag) {
       
       [_nameTag addNickName:nickname withMonkeyType:self.monkeyType];

        
        
    }
    
}


- (void)showTopTagWithAnimation
{
    [UIView animateWithDuration:0 animations:^{
        {
            //NSLog(@"a =  %.1f  , b = %.1f, c = %.1f, d = %.1f, tx=  %.1f, ty %.1f ", _nameTag.transform.a , _nameTag.transform.b ,
             //     _nameTag.transform.c,
             //     _nameTag.transform.d, _nameTag.transform.tx , _nameTag.transform.ty);
            
            if (_nameTag.transform.a == 0) {
                _nameTag.transform = CGAffineTransformMakeScale(1.0, 1.0);
                
                [self moveNameTagPosY];
                
                 _nameTag.transform = CGAffineTransformMakeScale(0, 0);
            }
        }

        
    }completion:^(BOOL finished) {
        
        [UIView animateWithDuration:0.5 delay:2.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            
            _nameTag.transform = CGAffineTransformMakeScale(1, 1);
            
        
        } completion:nil];
    
    
    }];
    
}

-(void)moveNameTagPosY
{
    BOOL isMax = false;
    if (self.height >= 170) {
        isMax = true;
    }
    CGRect frame = _nameTag.frame;
    CGFloat originY = _nameTag.frame.origin.y;
    
    switch (self.monkeyType) {
        case MonkeyTreeType_MonkeyA:
            originY = isMax ?  (_monkeyView.frame.origin.y - 11/2) :(_monkeyView.frame.origin.y - 32/2) ;
            break;
        case MonkeyTreeType_MonKeyB:
            originY = isMax ?  (_monkeyView.frame.origin.y - 11/2) : (_monkeyView.frame.origin.y - 64/2) ;
            break;
        case MonkeyTreeType_MonkeyC:
            originY = isMax ?  (_monkeyView.frame.origin.y - 11/2) : (_monkeyView.frame.origin.y - 30/2);
            break;
        case MonkeyTreeType_MonkeyD:
            originY = isMax ?  (_monkeyView.frame.origin.y - 11/2) : (_monkeyView.frame.origin.y - 23/2);
            break;
        case MonkeyTreeType_MonkeyE:
            originY = isMax ?  (_monkeyView.frame.origin.y - 11/2) : (_monkeyView.frame.origin.y - 30/2);
            break;
        default:
            break;
    }
    
    CGFloat widhth = _nameTag.frame.size.width;
    frame.origin.x = _monkeyTreeBaseView.frame.size.width/2 - widhth/2;
    frame.origin.y = originY;
    _nameTag.frame = frame;
}

@end
