//
//  DoleNameTag.h
//  HeightChart
//
//  Created by ne on 2014. 3. 21..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DoleTree.h"

enum DoleNameTagState{
    DoleNameTagStateNormal,
    DoleNameTagStatePress,
    DoleNameTagStateDelete
};

@interface DoleNameTag : UIView

@property (nonatomic)enum DoleNameTagState nameTagState;
@property (nonatomic, assign) NSString *nameTag;
@property (nonatomic) BOOL isDeleteMode;
@property (nonatomic) CGFloat nameTagWidth;
@property (nonatomic) CGFloat height; 


-(void)addNickName:(NSString*)name withMonkeyType:(enum DoleMonkeyType)type;
-(void)changeNickName:(NSString*)name;


@end
