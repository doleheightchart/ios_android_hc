//
//  BirthPopupController.m
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 2. 11..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "BirthPopupController.h"
#import "TwoLinePickerView.h"
#import "UIColor+ColorUtil.h"

@interface BirthPopupController (){
    TwoLinePickerView   *_yearView;
    NSRange             _yearRange;
}

-(IBAction)clickSet:(id)sender;

@end

@implementation BirthPopupController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    CGRect frame = CGRectMake(19, 42, 233, 137);
    
    _yearView = [TwoLinePickerView pickerviewWithFrame:frame];
    _yearView.backgroundColor = [UIColor whiteColor];
    _yearView.dataSource = self;
    _yearView.delegate = self;
    
    [self.popupContainerView addSubview:_yearView];
    UIColor *lineColor = [UIColor colorWithRGB:0x6fd4dd];
    [_yearView addTwoLineLayerWithColor:lineColor];

    NSCalendar *calendar = [[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *comp = [calendar components:NSCalendarUnitYear fromDate:[NSDate date]];
    NSInteger year = [comp year];
    
    _yearRange = NSMakeRange(year-99, 100);
    
    
//    if (!self.birthYear){
//        self.birthYear = _yearRange.location + 98;
//        [_yearView selectItemAtIndexPath:[NSIndexPath indexPathForRow:99 inSection:0] animated:NO scrollPosition:UICollectionViewScrollPositionBottom];
//    }
//    else{
//        NSInteger index = self.birthYear - _yearRange.location;
//        [_yearView selectItemAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0] animated:NO scrollPosition:UICollectionViewScrollPositionBottom];
//    }
    
    self.popupTitleLabel.text = NSLocalizedString(@"Enter the year of birth", @"");
//    self.okButton.titleLabel.text = NSLocalizedString(@"Set", @"");
//    self.cancelButton.titleLabel.text = NSLocalizedString(@"Cancel", @"");
}

-(void)viewDidAppear:(BOOL)animated
{
    if (!self.birthYear){
        self.birthYear = _yearRange.location + 98;
        [_yearView selectItemAtIndexPath:[NSIndexPath indexPathForRow:99 inSection:0] animated:NO scrollPosition:UICollectionViewScrollPositionBottom];
    }
    else{
        NSInteger index = self.birthYear - _yearRange.location;
        [_yearView selectItemAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0] animated:NO scrollPosition:UICollectionViewScrollPositionCenteredVertically];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _yearRange.length;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    TwoLinePickerView *picker = (TwoLinePickerView*)collectionView;
    
    PickerViewCell *cell = [picker dequeueReusableCellForIndexPath:indexPath];
    
    cell.textLabel.text = [NSString stringWithFormat:@"%02ld",  _yearRange.location + (long)indexPath.row, nil];
    
    return cell;
}

-(void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
{
    self.okButton.enabled = NO;
}
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    self.okButton.enabled = NO;
}

- (void) scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (decelerate == YES) return;

    CGFloat heightOfCell = scrollView.bounds.size.height / 3;
    
    NSInteger offSetIndex = roundf((scrollView.contentOffset.y) / heightOfCell);
    CGFloat moveToY = (offSetIndex * heightOfCell);
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         CGPoint point = CGPointMake(0, moveToY);
                         scrollView.contentOffset = point;
                     }
                     completion:^(BOOL finished) {
                         [self updateSelectedYear:scrollView];
                     }
     ];
}

- (void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGFloat heightOfCell = scrollView.bounds.size.height / 3;
    
    NSInteger offSetIndex = roundf((scrollView.contentOffset.y) / heightOfCell);
    CGFloat moveToY = (offSetIndex * heightOfCell);
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         CGPoint point = CGPointMake(0, moveToY);
                         scrollView.contentOffset = point;
                     }
                     completion:^(BOOL finished) {
                         [self updateSelectedYear:scrollView];
                     }
     ];
}

- (void)updateSelectedYear:(UIScrollView*)scrollView
{
    CGFloat heightOfCell = scrollView.bounds.size.height / 3;
    
    NSInteger offSetIndex = roundf((scrollView.contentOffset.y) / heightOfCell);
    
    self.birthYear = _yearRange.location + offSetIndex;
    
    self.okButton.enabled = YES;
}

-(IBAction)clickSet:(id)sender
{
    self.completion(self);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [collectionView selectItemAtIndexPath:indexPath animated:NO scrollPosition:UICollectionViewScrollPositionCenteredVertically];
    [self updateSelectedYear:collectionView];
}
@end
