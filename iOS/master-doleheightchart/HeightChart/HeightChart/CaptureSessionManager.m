#import "CaptureSessionManager.h"
#import <ImageIO/ImageIO.h>

@interface CaptureSessionManager()
{
    
}


@end

@implementation CaptureSessionManager

//@synthesize captureSession;
//@synthesize previewLayer;
//@synthesize stillImageOutput;
//@synthesize stillImage;

#pragma mark Capture Session Configuration

- (id)init {
	if ((self = [super init])) {
		[self setCaptureSession:[[AVCaptureSession alloc] init]];
	}
	return self;
}

- (void)addVideoPreviewLayer {
    self.previewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:self.captureSession];
    [self.previewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
}

- (void)addVideoInputFrontCamera:(BOOL)front {
    NSArray *devices = [AVCaptureDevice devices];
    AVCaptureDevice *frontCamera;
    AVCaptureDevice *backCamera;
    
    for (AVCaptureDevice *device in devices) {
        
        NSLog(@"Device name: %@", [device localizedName]);
        
        if ([device hasMediaType:AVMediaTypeVideo]) {
            
            
            if ([device position] == AVCaptureDevicePositionBack) {
                NSLog(@"Device position : back");
                backCamera = device;
                
                //NE.
//                backCamera.focusMode = AVCaptureFocusModeAutoFocus;
//                backCamera.subjectAreaChangeMonitoringEnabled = YES;
                // AVCaptureDeviceSubjectAreaDidChangeNotification
            }
            else {
                NSLog(@"Device position : front");
                frontCamera = device;
            }
        }
    }

    NSError *error = nil;
    if (front) {
        AVCaptureDeviceInput *frontFacingCameraDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:frontCamera error:&error];
        if (!error) {
            if ([[self captureSession] canAddInput:frontFacingCameraDeviceInput]) {
                [[self captureSession] addInput:frontFacingCameraDeviceInput];
            } else {
                NSLog(@"Couldn't add front facing video input");
            }
        }
    } else {
        AVCaptureDeviceInput *backFacingCameraDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:backCamera error:&error];
        if (!error) {
            if ([[self captureSession] canAddInput:backFacingCameraDeviceInput]) {
                [[self captureSession] addInput:backFacingCameraDeviceInput];
                
                [backCamera lockForConfiguration:nil];

                
                if ([backCamera isFocusModeSupported:AVCaptureFocusModeContinuousAutoFocus]) {
                    
                    backCamera.focusMode = AVCaptureFocusModeContinuousAutoFocus;
                    backCamera.subjectAreaChangeMonitoringEnabled = YES;
                    self.backCamera.focusPointOfInterest = CGPointMake(0, 0);
                }
                
                [backCamera unlockForConfiguration];
                
                
                
                
//                [[NSNotificationCenter defaultCenter] addObserver:self
//                                                         selector:@selector(areWeFocused:)
//                                                             name:AVCaptureDeviceSubjectAreaDidChangeNotification
//                                                           object:nil];
                
//                [NSNotificationCenter defaultCenter] addObserver:self.backCamera forKeyPath:@"adjustingFocusing" options:<#(NSKeyValueObservingOptions)#> context:<#(void *)#>
                
                self.backCamera = backCamera;
                
                
                [self.backCamera addObserver:self forKeyPath:@"adjustingFocus" options:NSKeyValueObservingOptionNew context:nil];
                
            } else {
                NSLog(@"Couldn't add back facing video input");
            }
        }
    }
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (self.backCamera.adjustingFocus){
        NSLog(@"Changed avdevice properties");
        self.onFocus(NO);
    }
    else{
        NSLog(@"AdjustingFocus = NO");
        self.onFocus(YES);
    }
}

- (void)addStillImageOutput 
{

    [self setStillImageOutput:[[AVCaptureStillImageOutput alloc] init]];
    NSDictionary *outputSettings = [[NSDictionary alloc] initWithObjectsAndKeys:AVVideoCodecJPEG,AVVideoCodecKey,nil];
    [[self stillImageOutput] setOutputSettings:outputSettings];
  
    AVCaptureConnection *videoConnection = nil;
    for (AVCaptureConnection *connection in [[self stillImageOutput] connections]) {
        for (AVCaptureInputPort *port in [connection inputPorts]) {
            if ([[port mediaType] isEqual:AVMediaTypeVideo] ) {
                videoConnection = connection;
                break;
            }
        }
        if (videoConnection) {
            break;
        }
    }
  
    [[self captureSession] addOutput:[self stillImageOutput]];
}

- (void)captureStillImage
{  
	AVCaptureConnection *videoConnection = nil;
	for (AVCaptureConnection *connection in [[self stillImageOutput] connections]) {
		for (AVCaptureInputPort *port in [connection inputPorts]) {
			if ([[port mediaType] isEqual:AVMediaTypeVideo]) {
				videoConnection = connection;
				break;
			}
		}
		if (videoConnection) { 
      break; 
    }
	}
    
    if (!videoConnection){
        //CCLOG(@"Device Connection is nil~~~");
        return;
    }
    
    
	NSLog(@"about to request a capture from: %@", [self stillImageOutput]);
	[[self stillImageOutput] captureStillImageAsynchronouslyFromConnection:videoConnection 
                                                       completionHandler:^(CMSampleBufferRef imageSampleBuffer, NSError *error) { 
                                                         CFDictionaryRef exifAttachments = CMGetAttachment(imageSampleBuffer, kCGImagePropertyExifDictionary, NULL);
                                                         if (exifAttachments) {
                                                           NSLog(@"attachements: %@", exifAttachments);
                                                         } else { 
                                                           NSLog(@"no attachments");
                                                         }
                                                         NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageSampleBuffer];    
                                                         UIImage *image = [[UIImage alloc] initWithData:imageData];
                                                         [self setStillImage:image];
//                                                         [image release];
                                                         [[NSNotificationCenter defaultCenter] postNotificationName:kImageCapturedSuccessfully object:nil];
                                                       }];
}

- (void)startCaptureRunning
{
    [[self captureSession]startRunning];
}
- (void)stopCaptureRunning
{
    [[self captureSession] stopRunning]; 
}

//- (void)dealloc {
//
//
//    [[self captureSession] stopRunning];
//
////    [previewLayer release], previewLayer = nil;
////    [captureSession release], captureSession = nil;
////    [stillImageOutput release], stillImageOutput = nil;
////    [stillImage release], stillImage = nil;
////	[super dealloc];
//}

- (void)closeSession
{
    
//    [self.backCamera addObserver:self forKeyPath:@"adjustingFocus" options:NSKeyValueObservingOptionNew context:nil];
    [self.backCamera removeObserver:self forKeyPath:@"adjustingFocus"];
    
    [self.captureSession stopRunning];
    [self.previewLayer removeFromSuperlayer];
    self.previewLayer = nil;
    self.captureSession = nil;
}


-(void)areWeFocused:(NSNotification*)notification
{
    if (self.backCamera.adjustingFocus){
        NSLog(@"Changed avdevice properties");
        self.onFocus(NO);
    }
    else{
        NSLog(@"AdjustingFocus = NO");
        
        self.onFocus(YES);
    }
    
    
}

@end
