//
//  PhotoButton.m
//  HeightChart
//
//  Created by ne on 2014. 3. 3..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "PhotoButton.h"
#import "UIFont+DoleHeightChart.h"

@implementation CameraButton
-(id)initWithCameraButton
{
    self = [super initWithFrame:CGRectMake(0, 0, 64, 68)];
    if (self) {
        [self initCameraButtonImage];
    }
    return self;
}

+(instancetype)camerabutton
{
    CameraButton *button = [CameraButton buttonWithType:UIButtonTypeCustom];
    //button.backgroundColor = [UIColor clearColor];
//    button.frame = CGRectMake(0, 0, 64, 68);
    
    return button;
}

+(CGSize)size
{
    return CGSizeMake(71 , 71);
}

-(void)initCameraButtonImage
{
    UIImage *normal = [UIImage imageNamed:@"camera_btn_center_normal"];
    UIImage *press = [UIImage imageNamed:@"camera_btn_center_press"];
    
    [self setBackgroundImage:normal forState:UIControlStateNormal];
    [self setBackgroundImage:press forState:UIControlStateHighlighted];
    
    self.contentMode = UIViewContentModeCenter;
    self.titleLabel.font = [UIFont defaultBoldFontWithSize:32/2];

    
    
    [self setCameraButtonState:DoleCaptureButtonStateCamera];
}

-(void)setCameraButtonState:(enum DoleCaptureButtonState) state
{
    switch (state) {
        case DoleCaptureButtonStateCamera:
            [self setBackgroundImage:[UIImage imageNamed:@"camera_btn_center_normal"] forState:UIControlStateNormal];
            [self setBackgroundImage:[UIImage imageNamed:@"camera_btn_center_press"] forState:UIControlStateHighlighted];
            [self setImage:[UIImage imageNamed:@"camera_btn_camera"] forState:UIControlStateNormal];
            break;
        case DoleCaptureButtonStateDone:
            [self setImage:nil forState:UIControlStateNormal];
            [self setTitle:NSLocalizedString(@"Done", @"")  forState:UIControlStateNormal];
            break;
        case DoleCaptureButtonStateLogin:
            [self setBackgroundImage:[UIImage imageNamed:@"camera_login_btn_center_normal"] forState:UIControlStateNormal];
            [self setBackgroundImage:[UIImage imageNamed:@"camera_login_btn_center_press"] forState:UIControlStateHighlighted];
            [self setImage:nil forState:UIControlStateNormal];
            [self setTitle:NSLocalizedString(@"LogIn", @"") forState:UIControlStateNormal];
            break;
        case DoleCaptureButtonStateSave:
            [self setImage:nil forState:UIControlStateNormal];
            [self setBackgroundImage:[UIImage imageNamed:@"camera_save_btn_center_normal"] forState:UIControlStateNormal];
            [self setBackgroundImage:[UIImage imageNamed:@"camera_save_btn_center_press"] forState:UIControlStateHighlighted];
            [self setTitle:NSLocalizedString(@"Save", @"") forState:UIControlStateNormal];
            break;
        default:
            break;
    }
    
        self.cameraState = state;
}

@end



@interface PhotoButtonControl()
{
    NSArray * _typeImageNameArray;
    UIImageView *_defalutBackgroundView;
    UIImageView *_selectView;
    UIImageView *_normalImageView;
    UIImageView *_lockImageView;
    UIControlState _cState;
}

@end

@implementation PhotoButtonControl

//- (id)initWithPhotoButtonType:(enum DolePhotoButtonType)type withState:(enum DolePhotoButtonState)state
//{
//
//    if ((self = [super initWithFrame:CGRectMake(0, 0, 32, 45)])) {
//        self.type = type;
//        self.buttonState = state;
//        [self initWithButton];
//    }
//    
//    return self;
//}

-(id)initWithPhotoButtonType:(enum DolePhotoButtonType)type
{
    
    if ((self = [super initWithFrame:CGRectMake(0, 0, 32, 45)])) {
        self.type = type;
        [self initWithButton];
    }
    
    return self;
}

- (void)initWithButton{
    //camera_frame_banana_farm
//    _typeImageNameArray = [NSArray arrayWithObjects:@"camera_frame_jungle",
//                                                @"camera_frame_space",
//                                                @"camera_frame_sea",
//                                                @"camera_frame_seafloor",
//                                                @"camera_frame_park",
//                                                @"camera_frame_fruit",
//                                                @"camera_frame_city_of_robot",
//                                                @"camera_frame_normal_castle"
//                                                , nil];

    
    _typeImageNameArray = [NSArray arrayWithObjects:
                           @"camera_frame_jungle",
                           @"camera_frame_banana_farm",
                           @"camera_frame_fruit",
                           @"camera_frame_normal_castle",
                           @"camera_frame_sea",
                           @"camera_frame_seafloor",
                           @"camera_frame_city_of_robot",
                           @"camera_frame_space"
                           , nil];
    
    _defalutBackgroundView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"camera_frame_normal"]];
    _selectView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"camera_frame_select"]];
    [_selectView setHidden:YES];
    
    NSString *imageName = [_typeImageNameArray objectAtIndex:self.type];
    _normalImageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:imageName]];
    
    [self addSubview:_defalutBackgroundView];
    [self addSubview:_normalImageView];
    
    
    
    UIImage *lockFrame = [UIImage imageNamed:@"camera_frame_lock"];
    UIImage *lockcover = [UIImage imageNamed:@"camera_frame_lock_cover"];
    UIImage *lockIco = [UIImage imageNamed:@"camera_frame_icon_lock"];
    
    UIImageView *lockcoverView = [[UIImageView alloc]initWithImage:lockcover];
    UIImageView *lockIcoView = [[UIImageView alloc]initWithImage:lockIco];
    
    _lockImageView = [[UIImageView alloc]init];
    _lockImageView.image = lockFrame;
    
    [_lockImageView addSubview:lockcoverView];
    [_lockImageView addSubview:lockIcoView];
    
    
    
    _lockImageView.frame = CGRectMake(0, 0, lockcoverView.frame.size.width, lockcoverView.frame.size.height);
    
    [_lockImageView.layer setOpacity:0.8];
    
    [self addSubview:_lockImageView];
    [self addSubview:_selectView];
    
    self.isSelected = NO;
    self.isLockFrame = NO;

    
}

-(void)setIsLockFrame:(BOOL)isLockFrame{
    
    _isLockFrame = isLockFrame;
    
    if(_isLockFrame == false){
//        _lockImageView.layer.opacity = 1;
        [_lockImageView setHidden:YES];
    }
    else{
        [_lockImageView setHidden:NO];
//        _lockImageView.layer.opacity = 0;
    }
    
}

-(void)setIsSelected:(BOOL)isSelected{
    
    _isSelected = isSelected;
    
    if(_isSelected == false){
        [_selectView setHidden:YES];
    }
    else{
        [_selectView setHidden:NO];
    }
    
}

- (void)sendAction:(SEL)action to:(id)target forEvent:(UIEvent *)event
{
    [super sendAction:action to:target forEvent:event];
    
    //CCLOG(@"[super sendAction:action to:target forEvent:event];");
}

- (void)sendActionsForControlEvents:(UIControlEvents)controlEvents
{
    [super sendActionsForControlEvents:controlEvents];
    
    //CCLOG(@"Control State %d", self.state);
    //CCLOG(@"Control Event %d", controlEvents);
    
}

- (BOOL)beginTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
{
//    [self setImageWithState];
    _cState = self.state;
        //CCLOG(@"beginTrackingWithTouch Control State %d", self.state);
    return [super beginTrackingWithTouch:touch withEvent:event];
}
- (BOOL)continueTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
{
//    [self setImageWithState];
    _cState = self.state;
        //CCLOG(@"continueTrackingWithTouch Control State %d", self.state);
    return [super continueTrackingWithTouch:touch withEvent:event];
}
- (void)endTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
{
    [self setImageWithState];
    _cState = self.state;
    [super endTrackingWithTouch:touch withEvent:event];
        //CCLOG(@"endTrackingWithTouch Control State %d", self.state);
    
    
    [self sendActionsForControlEvents:UIControlEventTouchUpInside];
}
- (void)cancelTrackingWithEvent:(UIEvent *)event
{
//    [self setImageWithState];
    _cState = self.state;
    [super cancelTrackingWithEvent:event];
        //CCLOG(@"cancelTrackingWithEvent Control State %d", self.state);
    
}// event may be nil if cancelled for non-event reasons, e.g. removed from window

-(void)setImageWithState
{
    
//    if (self.buttonState == DolePhotoButtonStateLock) {
//        self.buttonState = DolePhotoButtonStateLockSelect;
//    }
//    else{
//        self.buttonState = DolePhotoButtonStateNormalSelect; 
//    }
    
    self.isSelected = YES;
//    
//    [self changePhotoButtonState:self.buttonState];
    
}



@end

//@implementation PhotoButton
//- (id)initWithFrame:(CGRect)frame
//{
//    self = [super initWithFrame:frame];
//    if (self) {
//        // Initialization code
//    }
//    return self;
//}
//
///*
// // Only override drawRect: if you perform custom drawing.
// // An empty implementation adversely affects performance during animation.
// - (void)drawRect:(CGRect)rect
// {
// // Drawing code
// }
// */
//
//-(void)setPhotoButtonWithType:(enum DolePhotoButtonType)buttonType
//{
//    self.type = buttonType;
//    
//    NSString *backgroundImageName;
//    UIImage *backgroundImage;
//    
//    switch (self.type) {
//        case DolePhotoButtonTypeJungle:
//            backgroundImageName = @"camera_frame_normal_jungle";
//            break;
//        case DolePhotoButtonTypeSpace:
//            backgroundImageName = @"camera_frame_normal_space";
//            break;
//        case DolePhotoButtonTypeSea:
//            backgroundImageName = @"camera_frame_normal_sea";
//            break;
//        case DolePhotoButtonTypeSeafloor:
//            backgroundImageName = @"camera_frame_normal_seafloor";
//            break;
//        default:
//            break;
//    }
//    
//    UIImage *buttonImage = nil;
//    
//    backgroundImage = [UIImage imageNamed:backgroundImageName];
//    
//    if (self.type == DolePhotoButtonTypeLock) {
//        
//        UIImage * lockImage = [UIImage imageNamed:@"camera_frame_lock"];
//        UIImage * lockCoverImage = [UIImage imageNamed:@"camera_frame_lock_cover1"];
//        
//        backgroundImage = lockImage;
//        buttonImage = lockCoverImage;
//        
//    }
//    
//    [self setImage:buttonImage forState:UIControlStateNormal];
//    //    [self setImage:buttonImage forState:UIControlStateHighlighted];
//    //    [self setImage:buttonImage forState:UIControlStateDisabled];
//    
//    [self setBackgroundImage:backgroundImage forState:UIControlStateNormal];
//    //    [self setBackgroundImage:backgroundImage forState:UIControlStateHighlighted];
//    //    [self setBackgroundImage:backgroundImage forState:UIControlStateDisabled];
//    
//    self.adjustsImageWhenHighlighted = NO;
//}
//
//-(bool)setSelectButton
//{
//    
//    if (self.type == DolePhotoButtonTypeLock) {
//        return false;
//    }
//    
//    NSString *normalImageName = @"camera_frame_select";
//    UIImage *buttonImage = [UIImage imageNamed:normalImageName];
//    
//    [self setImage:buttonImage forState:UIControlStateNormal];
//    [self setImage:buttonImage forState:UIControlStateHighlighted];
//    [self setImage:buttonImage forState:UIControlStateDisabled];
//    
//    
//    CCLOG(@"state = %d", self.state);
//    
//    return true;
//}
//
//@end
