//
//  MessageBoxController.h
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 4. 8..
//  Copyright (c) 2014년 Kim Sehyun. All rights reserved.
//

#import "OverlayViewController.h"

typedef enum : NSUInteger {
    kMessageBoxButtonTypeOK,
    kMessageBoxButtonTypeCancel,
    kMessageBoxButtonTypeOKCancel,
    kMessageBoxButtonTypeYesNo,
} MessageBoxButtonType;

@interface MessageBoxController : UIControl

@property (nonatomic, retain) NSString *message;
@property (nonatomic) BOOL hiddenIcon;
@property (nonatomic) MessageBoxButtonType buttonType;
@property (nonatomic, retain) UIView *lockedView;


@property (nonatomic, copy) void (^confirmCompletion)();
@property (nonatomic, copy) void (^okCancelCompletion)(BOOL isOK);


+(void)showOKConfirmMessage:(NSString*)message onView:(UIView*)view completion:(void(^)())completion Icon:(BOOL)useIcon;
+(void)showCancelConfirmMessage:(NSString*)message onView:(UIView*)view completion:(void(^)())completion Icon:(BOOL)useIcon;
+(void)showOkCancelConfirmMessage:(NSString*)message onView:(UIView*)view completion:(void(^)(BOOL))completion Icon:(BOOL)useIcon;
+(void)showYesNoConfirmMessage:(NSString*)message onView:(UIView*)view completion:(void(^)(BOOL))completion Icon:(BOOL)useIcon;
+(void)showLogInCancelConfirmMessage:(NSString*)message onView:(UIView*)view completion:(void(^)(BOOL))completion Icon:(BOOL)useIcon;

@end

@interface UIViewController (MessageBoxController)

-(void)showOKConfirmMessage:(NSString*)message completion:(void(^)())completion;
-(void)showCancelConfirmMessage:(NSString*)message completion:(void(^)())completion;
-(void)showOkCancelConfirmMessage:(NSString*)message completion:(void(^)(BOOL))completion;
-(void)showYesNoConfirmMessage:(NSString*)message completion:(void(^)(BOOL))completion;
-(void)showLogInCancelConfirmMessage:(NSString*)message completion:(void(^)(BOOL))completion;

-(void)showOKConfirmMessage:(NSString*)message completion:(void(^)())completion Icon:(BOOL)useIcon;
-(void)showCancelConfirmMessage:(NSString*)message completion:(void(^)())completion Icon:(BOOL)useIcon;
-(void)showOkCancelConfirmMessage:(NSString*)message completion:(void(^)(BOOL))completion Icon:(BOOL)useIcon;
-(void)showYesNoConfirmMessage:(NSString*)message completion:(void(^)(BOOL))completion Icon:(BOOL)useIcon;
-(void)showLogInCancelConfirmMessage:(NSString*)message completion:(void(^)(BOOL))completion Icon:(BOOL)useIcon;

@end
