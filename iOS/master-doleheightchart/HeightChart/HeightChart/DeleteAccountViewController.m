//
//  DeleteAccountViewController.m
//  HeightChart
//
//  Created by ne on 2014. 4. 11..
//  Copyright (c) 2014년 Kim Sehyun. All rights reserved.
//

#import "DeleteAccountViewController.h"
#import "UIViewController+DoleHeightChart.h"
#import "UIView+ImageUtil.h"
#import "UIFont+DoleHeightChart.h"
#import "UIColor+ColorUtil.h"
#import "InputChecker.h"
#import "TextViewDualPlaceHolder.h"
#import "UIButton+CheckAndRadio.h"
#import "InputChecker.h"
#import "UIViewController+PolyServer.h"
#import "ToastController.h"
#import "WarningCoverView.h"
#import "NSObject+HeightDatabase.h"
#import "DoleTransition.h"
#import "AppDelegate.h"

#import "Mixpanel.h"


@interface AccountCell : UITableViewCell
@property (nonatomic, retain) UILabel *programName;
@property (nonatomic, retain) UILabel *programDesc;
@property (nonatomic, retain) UIImageView *iconView;
@end
@implementation AccountCell
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor clearColor];
        
        self.iconView = [[UIImageView alloc]initWithFrame:CGRectMake(18/2, (120 - 94)/2/2, 94/2, 94/2)];
        [self addSubview:self.iconView];
        
        
        CGFloat topMarginPixel = (120 - (38+5+28))/2;
        
        self.programName = [[UILabel alloc]initWithFrame:CGRectMake((18+94+24)/2, topMarginPixel/2, 362/2, 38/2)];
        self.programName.backgroundColor = [UIColor clearColor];
        self.programName.textAlignment = NSTextAlignmentLeft;
        self.programName.textColor = [UIColor colorWithRGB:0x007f8b];
        self.programName.font = [UIFont defaultRegularFontWithSize:34/2];

        [self addSubview:self.programName];

        
        self.programDesc = [[UILabel alloc]initWithFrame:CGRectMake((18+94+24)/2, (topMarginPixel+38+5)/2, 362/2, 28/2)];
        self.programDesc.backgroundColor = [UIColor clearColor];
        self.programDesc.textAlignment = NSTextAlignmentLeft;
        self.programDesc.textColor = [UIColor colorWithRGB:0x0ca6b5];
        self.programDesc.font = [UIFont defaultRegularFontWithSize:24/2];
        
        [self addSubview:self.programDesc];

        UIImageView *lineView = [[UIImageView alloc]initWithFrame:CGRectMake(0, (120-1)/2, 528/2, 1)];
        lineView.backgroundColor = [UIColor colorWithRGB:0x02c2c9];
        [self addSubview:lineView];
    }
    return self;
}
@end



@interface DeleteAccountViewController ()
{
    InputChecker *_inputChecker;
    
    UIButton *_checkbtn;
    TextViewDualPlaceHolder *_password;
    UIButton * _deleteAccount;
    UITableView *_tableView;
    
    NSMutableArray *_tabledatasource;
    
    //Server
    NSMutableData   *_receivedData;
    
    
    UIImageView * _title;
    UIImageView * _bottom;
    UIImageView * _talkbox;
    UIImageView * _monkey;
    UILabel * _accoutLabel;
    
    
}

@end

@implementation DeleteAccountViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self addBackgrounBase];
    [self addTableView];
    [self addImageAndButton]; 
    [self addCommonCloseButton];
    
    
    [self setupControlFlexiblePosition];
    
    [self initDatasource];
    
    _inputChecker = [[InputChecker alloc]init];
    [_inputChecker addHostViewRecursive:self.view submitButton:_deleteAccount checkBoxButtons:@[_checkbtn]];
    
    if (self.userConfig.loginStatus == kUserLogInStatusFacebook){
        [_inputChecker removeDTextView:_password];
        _password.hidden = YES;
        
    }
    else{
        [self addKeyboardObserver];
    }
    
    
}

-(void)willAppearKeyboard:(NSNotification *)notification
{
    [self keyboardWillAnimate:notification];
}

-(void)willDisAppearKeyboard:(NSNotification *)notification
{
    [self keyboardWillAnimate:notification];
}

- (void)keyboardWillAnimate:(NSNotification *)notification
{
    CGRect keyboardBounds;
    [[notification.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardBounds];
    
    [UIView animateWithDuration:0.3 animations:^{
        
//        CGFloat moveY = 0;
//        
//        if([notification name] == UIKeyboardWillShowNotification){
//            moveY = -(96/2);
//        }
//        else{
//            moveY = (96/2);
//        }
//        NSArray *array = @[self.view]; //그냥 뷰만 올리기만 하기로함.
//        
//        for (UIView *v  in array) {
//            CGPoint ct = v.center;
//            v.center = CGPointMake(ct.x, ct.y + moveY);
//        }
        CGFloat moveY = 0;
        if (self.view.frame.origin.y == 0){
            moveY = self.view.bounds.size.height - (_password.frame.origin.y + _password.frame.size.height);
        }
        else{
            moveY = self.view.frame.origin.y;
        }
        
        CGPoint ct = self.view.center;
        self.view.center = CGPointMake(ct.x, ct.y - moveY);
    }];
}

-(void)initDatasource
{
    _tabledatasource = [[NSMutableArray alloc]init];
    
    DoleFamilyAccountData *data1 = [[DoleFamilyAccountData alloc]init];
    data1.AccountName = @"Dole Coins";
    data1.AccountDesc = NSLocalizedString(@"Earn and usage record", @"");// @"Earn and usage record";
    data1.imageName = @"delete_account_image_dole_coin";
    
    DoleFamilyAccountData *data2 = [[DoleFamilyAccountData alloc]init];
    data2.AccountName = @"Height Chart";
    data2.AccountDesc = NSLocalizedString(@"User infomation, Synced data", @"");// @"User infomation, Synced data";
    data2.imageName = @"delete_account_image_height_chart";
    
    
    [_tabledatasource addObject:data1];
    [_tabledatasource addObject:data2];
    
    
    
    
}

- (void)addImageAndButton
{
    
    UIEdgeInsets talkboxInsets = UIEdgeInsetsMake(20/2, 20/2, 30/2, 70/2);
    _talkbox = [[UIImageView alloc]initWithImage: [UIImage resizableImageWithName:@"invite_talk_box"
                                                                                     CapInsets:talkboxInsets]];
    CGRect recttalkbox = CGRectMake(56/2, 164/2, 404/2, 162/2);
    _talkbox.frame = recttalkbox;
    
    [self.view addSubview:_talkbox];
    
    UILabel *talkLabel = [[UILabel alloc]init];
    talkLabel.font = [UIFont defaultRegularFontWithSize:26/2];
//    talkLabel.font = [UIFont defaultRegularFontWithSize:28/2];

    talkLabel.textColor = [UIColor colorWithRGB:0x606060];
    talkLabel.text =NSLocalizedString(@"Are you sure you want to delete\nyour account? The information\nbelow will be deleted.", @"");
//    talkLabel.text = @"탈퇴하시겠습니까?\n아래 정보가 모두 삭제됩니다.";
    talkLabel.textAlignment = NSTextAlignmentLeft;
    talkLabel.numberOfLines = 3;
//    talkLabel.frame = CGRectMake(18/2, 22/2, (36+36+30)/2, 404/2);
    talkLabel.frame = CGRectMake(0 + (18/2), 0, (404-18-18)/2, (162-22-22)/2);
    
    
    if ([self.currentLanguageCode isEqualToString:@"ko"]) {
        talkLabel.font = [UIFont defaultRegularFontWithSize:28/2];
        talkLabel.frame = CGRectMake(0 + (18/2), 0, (404-36-36)/2, (162-22-22)/2);
    }
    talkLabel.center = CGPointMake((404)/4, (162 - 38 + 22)/4);
    [_talkbox addSubview:talkLabel];
    
    
    _monkey = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"delete_account_monkey"]];
    CGRect rectmonkey = CGRectMake(410/2, 256/2, 174/2, 122/2);;
    _monkey.frame = rectmonkey;
    
    [self.view addSubview:_monkey];
    
    //체크 박스 버튼
    _checkbtn = [UIButton buttonWithType:UIButtonTypeCustom];
    CGRect rectcheck = CGRectMake( 56/2, (164+204+330+20)/2,  51/2, 51/2);;
    _checkbtn.frame = rectcheck;
    [self.view addSubview:_checkbtn];
    [_checkbtn makeUICheckBox];
    
    [_checkbtn addTarget:self action:@selector(clickCheck:) forControlEvents:UIControlEventTouchUpInside];
    
    // 옆에 라벨
    _accoutLabel = [[UILabel alloc]init];
    _accoutLabel.backgroundColor = [UIColor clearColor];
    _accoutLabel.font = [UIFont defaultRegularFontWithSize:26/2];
    _accoutLabel.textColor = [UIColor colorWithRGB:0x606060];
    _accoutLabel.text =  NSLocalizedString(@"Yes, I want to delete account." , @"");
    
    
    _accoutLabel.textAlignment = NSTextAlignmentLeft;
        
    _accoutLabel.frame = CGRectMake((56+51+18)/2, (164+204+330+31)/2, (459)/2, 28/2);
    
    
    [self.view addSubview:_accoutLabel];
    
    _password = [[TextViewDualPlaceHolder alloc]init];
    CGRect rectpassword = CGRectMake( 56/2, (164+204+330+20+51+41)/2,  528/2, 72/2);;
    _password.frame = rectpassword;
    [_password setDualPlaceHolderTextView:DoleDualPlaceHolderTextTypePassword];
    [_password.textField setReDrawPlaceHolder:NSLocalizedString(@"Confirm password", @"") SecondText:@""];
    _password.textField.secureTextEntry = YES;
    [self.view addSubview:_password];
    _password.textField.enabled = NO;
    
    
//    _deleteAccount = [[UIButton alloc]init];
    _deleteAccount = [UIButton buttonWithType:UIButtonTypeCustom];
    CGRect rectbtn = CGRectMake( 56/2, (1136 - 68 - 74 )/2,  528/2, 74/2);;
    _deleteAccount.frame = rectbtn;
    //_deleteAccount.titleLabel.text = @"Delete account";
    [_deleteAccount setTitle:NSLocalizedString(@"Delete account", @"") forState:UIControlStateNormal];
    [_deleteAccount setResizableImageWithType:DoleButtonImageTypeOrange];
    [self.view addSubview:_deleteAccount];
    
    [_deleteAccount addTarget:self action:@selector(clickDeleteAccount:) forControlEvents:UIControlEventTouchUpInside];
    
}
- (void)addTableView
{
   
    
        CGRect rectTable = CGRectMake(56/2, (164 + 204)/2, 528/2, 330/2);
    _tableView = [[UITableView alloc]initWithFrame:rectTable style:UITableViewStylePlain];
   
    [self.view addSubview:_tableView];

    
    
    [_tableView registerClass:[AccountCell class] forCellReuseIdentifier:@"AccountCell"];
    _tableView.rowHeight = 120/2;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.bounces = YES;
    _tableView.showsHorizontalScrollIndicator = NO;
    _tableView.showsVerticalScrollIndicator = YES;
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.backgroundColor = [UIColor clearColor];
    
    UIEdgeInsets tableBackInsets = UIEdgeInsetsMake(10/2, 10/2, 10/2, 10/2);
    UIImageView * tableBackgrounImage = [[UIImageView alloc]initWithImage: [UIImage resizableImageWithName:@"delete_account_list"
                                                                                                 CapInsets:tableBackInsets]];
    
    tableBackgrounImage.frame = _tableView.bounds;
    _tableView.backgroundView = tableBackgrounImage;
}



- (void)addBackgrounBase{
    
    //배경
    UIImageView * bg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"sky_bg"]];
    CGRect rectbg = self.view.bounds;
    bg.contentMode = UIViewContentModeScaleToFill;
    bg.frame = rectbg;
    
    [self.view addSubview:bg];
    
    //타이틀
    _title = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"title_mini"]];
    CGRect rect = CGRectMake(0, 0, 158/2, 94/2);
    _title.frame = rect;
    [self.view addSubview:_title];
    
    //배경
    _bottom = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"add_nickname_bg_bottom"]];
    CGRect rectbottom = CGRectMake(0, self.view.bounds.size.height - 295/2, self.view.bounds.size.width, 295/2);
    _bottom.frame = rectbottom;
    [self.view addSubview:_bottom];
    
    
    
}



-(void)setupControlFlexiblePosition
{
    if ([UIScreen isFourInchiScreen]) {
        
    }
    else{
        
        [_title setHidden:YES];
        [_bottom setHidden:YES];
        [self changeToHeightPixel:330  Control:_tableView];
        
        [self moveToPositionYPixel:53 Control:_talkbox];
        [self moveToPositionYPixel:53 + 204 Control:_tableView];
        [self moveToPositionYPixel:146 Control:_monkey];
        
        [self moveToPositionYPixel: 53 + 204 + 330 + 20 Control:_checkbtn];
        [self moveToPositionYPixel: 53 + 204 + 330 + 20 + 11 Control:_accoutLabel];
        [self moveToPositionYPixel: 53 + 204 + 330 + 20 + 51 + 41 Control:_password];
        [self moveToPositionYPixel: 53 + 204 + 330 + 20 + 51 + 41 +72 + 47 Control:_deleteAccount];

        

    }
    
    
    
    
}


-(void)clickCheck:(id)sender
{
    _password.textField.enabled = _checkbtn.selected;
}

-(void)clickDeleteAccount:(id)sender
{
    if ([self enableActionWithCurrentNetwork] == NO)return;
    
    if (self.userConfig.loginStatus == kUserLogInStatusDoleLogOn){
        
        NSString *authKey = self.userConfig.authKey;
        NSString *email = self.userConfig.email;
        NSInteger userNo = self.userConfig.userNo;
        NSString *password = _password.textField.text;
        
        [self requestWithdrawWithEmail:email Password:password UserNo:userNo AuthKey:authKey];
        
        //[self removeCloudDB];
        
    }
    else if (self.userConfig.loginStatus == kUserLogInStatusFacebook){
        NSString *facebookID = self.facebookManager.facebookID;
        NSString *authKey = self.userConfig.authKey;
        NSInteger userNo = self.userConfig.userNo;
        
        [self requestDissconnectWithSnsID:facebookID AuthKey:authKey UserNo:userNo];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark TableView

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  _tabledatasource.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AccountCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AccountCell" forIndexPath:indexPath];
    DoleFamilyAccountData *accountData = _tabledatasource[indexPath.row];

    UIImage *programIcon = [UIImage imageNamed:accountData.imageName];
    cell.iconView.image = programIcon;
    cell.programName.text = accountData.AccountName;
    cell.programDesc.text = accountData.AccountDesc;
    
    return cell;
}


- (NSMutableData*)receivedBufferData
{
    if (_receivedData == nil){
        _receivedData = [NSMutableData dataWithCapacity:0];
    }
    
    return _receivedData;
}


- (void)didConnectionFail:(NSError *)error
{
    //[ToastController showToastWithMessage:error.description duration:5];
    
    
    
}

- (void)didCompleteRecevedWithResultType:(DoleServerResultType)resultType ParsedData:(NSDictionary *)recevedData ParseError:(NSError *)error
{
    if (recevedData && error==nil && [recevedData[@"Return"] boolValue] ){
        
//        if (resultType == kDoleServerResultTypeWithdraw){
//            [self.userConfig withDraw];
//            exit(0);
//        }
//        else if (resultType == kDoleServerResultTypeDissconnectSns){
//            [self.facebookManager logout];
//            [self.userConfig withDraw];
//            exit(0);
//        }
        
        [self processRemoveCloudDB];
        
        //longzhe cui added
        [[Mixpanel sharedInstance] track:@"Delete Account"];
        [[Mixpanel sharedInstance].people set:@{@"Deleted":@"true"}];
        
        // JP added Anonymous tracking
        NSString *identifier = [[NSProcessInfo processInfo] globallyUniqueString];
        [[Mixpanel sharedInstance] identify:identifier];
        [[Mixpanel sharedInstance] registerSuperProperties:@{@"User Type": @"Anonymous"}];
        [[Mixpanel sharedInstance].people addPushDeviceToken:((AppDelegate*)([UIApplication sharedApplication].delegate)).userConfig.deviceTokenData];
        
    }
    else{
        
        
        NSInteger errorCode = [recevedData[@"ReturnCode"] integerValue];
        if (self.userConfig.loginStatus == kUserLogInStatusDoleLogOn){
            if ([self queryErrorCode:errorCode EmailView:nil PasswordView:_password]) return;
        }
        
        [self toastNetworkErrorWithErrorCode:errorCode];

    }
}

-(void)processDeleteAccountLocalData
{

    
    if (self.userConfig.accountStatus == kSignUpByFacebook){
        [self.userConfig withDraw];
        
    }
    else if (self.userConfig.accountStatus == kSignUpByEmail){
        [self.facebookManager logout];
        [self.userConfig withDraw];
        
    }
 
    [self.navigationController.presentingViewController dismissViewControllerWithFoldStyle:0 completion:^(BOOL finished) {
        [ToastController showToastWithMessage:NSLocalizedString(@"Thank you for using the Height Chart!", @"") duration:kToastMessageDurationType_Auto];
    }];
}

-(void)processRemoveCloudDB
{
    if (self.userConfig.useiCloud){
        [self addActiveIndicator];
        self.userConfig.useiCloud = NO;
        [self removeCloudDB];
        [self removeActiveIndicator];
        [self processDeleteAccountLocalData];
    }
    else{
        [self addActiveIndicator];
        [self addObserverSelfChangeDBStore];
        [self removeCloudDB];
        [self removeActiveIndicator];
        [self processDeleteAccountLocalData];
    }
}



@end



@implementation DoleFamilyAccountData


@end
