//
//  HeightSummary.m
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 9..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "HeightSummary.h"
#import "HiehgtSummaryCell.h"
#import "UIColor+ColorUtil.h"
#import "NSDate+DoleHeightChart.h"
#import "AverageHeightInfo.h"

//#define kSummaryViewHeight ((640-94)/2)
//#define kSummaryCellItemWidth (208)
#define kSummaryCellID  @"SummaryCellID"
#define kSummaryPaddingWidth  (121/2)

@interface HeightSummary(){
    UICollectionView            *_summaryCollectionView;
    UICollectionViewFlowLayout  *_collectionViewLayout;
    
    NSMutableDictionary         *_dataDic;
    NSMutableArray              *_dataArray;
    
    CAShapeLayer                *_heightDetailShapeLayer;
    CAShapeLayer                *_standardHeightShapeLayer;
    
    CGFloat                     _itemWidthSpacing;
    CGFloat                     _leftInset;
    CGFloat                     _rightInset;
}

@end

@implementation HeightSummary

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
//        [self initControl];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self){
//        [self initControl];
    }
    return self;
}

-(void)initControl
{
    self.backgroundColor = [UIColor clearColor];

    CGFloat width = self.bounds.size.width;
    width = width - (121);
    _itemWidthSpacing = (width - (4*54)) / 3;
    _leftInset = _rightInset = kSummaryPaddingWidth;
    
    CGRect frameCollectionView = CGRectMake(0, // Horizontal Gap
                                            0,
                                            self.bounds.size.width,
                                            self.bounds.size.height);
    
//    _itemWidthSpacing = (frameCollectionView.size.width - (kSummaryHeightCellSizeWidth*4)) / 3;
    
    
    NSInteger countOfData = [self.heightDetailDataSource countOfHeightDetail];
    
    if (countOfData < 4){
        CGFloat paddingWidth = self.bounds.size.width;
        paddingWidth -= (countOfData*54);
        paddingWidth -= _itemWidthSpacing*( MAX(0, countOfData-1) );
        paddingWidth /= 2;
        
        _leftInset = _rightInset = paddingWidth;
    }
    
    _collectionViewLayout = [[UICollectionViewFlowLayout alloc] init];
    _collectionViewLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    _collectionViewLayout.minimumInteritemSpacing = _itemWidthSpacing;
    
    _collectionViewLayout.minimumLineSpacing = _itemWidthSpacing;
    _collectionViewLayout.itemSize = CGSizeMake(kSummaryHeightCellSizeWidth, self.bounds.size.height);
//    _collectionViewLayout.sectionInset = UIEdgeInsetsMake(0, (122/2), 0, (122)+(122/4));
    _collectionViewLayout.sectionInset = UIEdgeInsetsMake(0, _leftInset, 0, _rightInset);
    
    
    _summaryCollectionView = [[UICollectionView alloc]initWithFrame:frameCollectionView
                                               collectionViewLayout:_collectionViewLayout];
    _summaryCollectionView.bounces = YES;
    _summaryCollectionView.showsHorizontalScrollIndicator = NO;
    _summaryCollectionView.showsVerticalScrollIndicator = NO;
    _summaryCollectionView.delegate = self;
    _summaryCollectionView.dataSource = self;
    _summaryCollectionView.backgroundColor = [UIColor clearColor];
    
    _standardHeightShapeLayer = [CAShapeLayer layer];
    _standardHeightShapeLayer.frame = self.bounds;
    _standardHeightShapeLayer.strokeColor = [UIColor  colorWithRGB:0x64d9e2 alpha:0.5].CGColor;
    _standardHeightShapeLayer.fillColor = [UIColor colorWithRGB:0x64d9e2 alpha:0.7].CGColor;
    _standardHeightShapeLayer.opacity = 0;
    _standardHeightShapeLayer.lineWidth = 0.0;
    [self.layer addSublayer:_standardHeightShapeLayer];
    
    _heightDetailShapeLayer = [CAShapeLayer layer];
    _heightDetailShapeLayer.frame = self.bounds;
    _heightDetailShapeLayer.lineWidth = 0.0;
    _heightDetailShapeLayer.strokeColor = [UIColor  colorWithRGB:0x9453e4 alpha:0.5].CGColor;
    _heightDetailShapeLayer.fillColor = [UIColor colorWithRGB:0x9453e4 alpha:0.3].CGColor;
    [self.layer addSublayer:_heightDetailShapeLayer];
    
    [_summaryCollectionView registerClass:[HiehgtSummaryCell class] forCellWithReuseIdentifier:kSummaryCellID];
    
    [self addSubview:_summaryCollectionView];
    

    
}

-(void)reloadData
{
    [_summaryCollectionView reloadData];
    
    [self updateGraph];
}

#pragma mark UICollectionView DataSource And Delegate

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    //return _dataArray.count;
    return [self.heightDetailDataSource countOfHeightDetail];
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellID;
    
    cellID = kSummaryCellID;
    
    HiehgtSummaryCell *cell = [_summaryCollectionView dequeueReusableCellWithReuseIdentifier:cellID forIndexPath:indexPath];
    
    HeightDetailInfo *detailInfo = [self.heightDetailDataSource heightDetailAtIndex:indexPath.row];
    
    //cell.kidHeightDetail = _dataArray[indexPath.row];
    
    [cell updateWithHeightDetail:detailInfo];
    
//    한국식나이계산을 수정함
//    NSInteger age = [detailInfo.takenDate getYear] -  [self.myChild.birthday getYear];
    NSInteger age = [self.myChild.birthday getGlobalAgeToDate:detailInfo.takenDate];
    
    CGFloat avY = [AverageHeightInfo averageHeightByAge:age IsGril:[self.myChild.isGirl boolValue]];
    [cell updateStandardHeightMark:self.visibleStandardHeightLine standardHeight:avY];
    
    return cell;
}

-(CGFloat)heightWithKidHeight:(CGFloat)kidHeight
{
    CGFloat minHeight = (138/2)-1;
    CGFloat maxHeight = (640-170)/2-1;
    
    CGFloat rangeHeight = maxHeight - minHeight;
    
    CGFloat minH = self.bounds.size.height - minHeight;
    CGFloat maxH = self.bounds.size.height - maxHeight;
    
    if (kidHeight < 50) return minH;
    if (kidHeight > 170) return maxH;
    
    CGFloat h = minHeight + rangeHeight * ( (kidHeight-50.0) / 120.0);
    
    h = MAX(minHeight, h);
    h = MIN(maxHeight, h);
    
    CGFloat heightOfView = self.bounds.size.height - h;
    
    return heightOfView;
}

-(void)updateGraph
{
    if ([self.heightDetailDataSource countOfHeightDetail] < 2) {
//        _heightDetailShapeLayer.opacity = 0;
//        _standardHeightShapeLayer.opacity = 0;
        return;
    }
    else{
//        _heightDetailShapeLayer.opacity = 1;
//        _standardHeightShapeLayer.opacity = 1;
    }
    
    CGMutablePathRef path = CGPathCreateMutable();
    CGMutablePathRef path2 = CGPathCreateMutable(); //standard heights
    
    CGFloat firstX = 0;
    CGFloat lastX = 0;
    
//    NSMutableOrderedSet *set = [NSMutableOrderedSet orderedSet];
//    for (HiehgtSummaryCell *cell in _summaryCollectionView.visibleCells) {
//        NSIndexPath *indexPath = [_summaryCollectionView indexPathForCell:cell];
//        [set addObject:[NSNumber numberWithInteger:indexPath.row]];
//    }
//    
//    NSArray *sortArray = [set sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
//        return [obj1 compare:obj2];
//    }];
//    
//    NSInteger firstIndex = [sortArray.firstObject integerValue];
//    NSInteger lastIndex = [sortArray.lastObject integerValue];
    
    NSInteger firstIndex = (_summaryCollectionView.contentOffset.x - _leftInset - (kSummaryHeightCellSizeWidth/2))/ (kSummaryHeightCellSizeWidth + _itemWidthSpacing);
    NSInteger lastIndex = firstIndex + 6;
    
    NSInteger count = [self.heightDetailDataSource countOfHeightDetail];
    
    firstIndex = MAX(firstIndex-1, 0);
    lastIndex = MIN(lastIndex+1, count-1);
    
//    NSLog(@"Start Shap Graph Line");
//    for (NSNumber *indexNumber in sortArray.objectEnumerator) {
//        NSInteger index = indexNumber.integerValue;
    for (int index = firstIndex; index <= lastIndex; index++) {
//        NSLog(@"Visible Cell Index is %d", index);
        //KidHeightDetail *k = _dataArray[index];
        HeightDetailInfo *detail = [self.heightDetailDataSource heightDetailAtIndex:index];
        
        CGFloat x = (kSummaryHeightCellSizeWidth + _itemWidthSpacing) * index;
        x += (kSummaryHeightCellSizeWidth/2);
        x -= (_summaryCollectionView.contentOffset.x);
        
        // Use Margine Width
        x += (_leftInset);
//        x = MAX((122/2), x);
//        x = MIN(self.bounds.size.width - (122/2), x);
        x += 1;
        
        CGFloat y = [self heightWithKidHeight:detail.kidHeight];
        
//        한국식나이계산 수정
//        NSInteger age = [detail.takenDate getYear] -  [self.myChild.birthday getYear];
        NSInteger age = [self.myChild.birthday getGlobalAgeToDate:detail.takenDate];
        CGFloat aveheight = [AverageHeightInfo averageHeightByAge:age IsGril:[self.myChild.isGirl boolValue]];
        CGFloat avY = [self heightWithKidHeight:aveheight];
//        CGFloat avY = [self heightWithKidHeight:detail.kidHeight + 50];
        
//        if (indexNumber == sortArray.firstObject){
        if (index == firstIndex){
            CGPathMoveToPoint(path, NULL, x, y);
            CGPathMoveToPoint(path2, NULL, x, avY);
            firstX = x;
        }
        
        CGPathAddLineToPoint(path, NULL, x, y);
        CGPathAddLineToPoint(path2, NULL, x, avY);
        lastX = x;
    }
    
    CGFloat grassHeight = 52/2;
    
    CGPathAddLineToPoint(path, NULL, lastX, self.bounds.size.height-grassHeight);
    CGPathAddLineToPoint(path, NULL, firstX,self.bounds.size.height-grassHeight);
    
    CGPathAddLineToPoint(path2, NULL, lastX, self.bounds.size.height-grassHeight);
    CGPathAddLineToPoint(path2, NULL, firstX,self.bounds.size.height-grassHeight);

//    NSLog(@"End Shap Graph Line");
    
    CGPathCloseSubpath(path);
    CGPathCloseSubpath(path2);
    
    if ((lastIndex-firstIndex) > 0){
        _heightDetailShapeLayer.path = path;
        _standardHeightShapeLayer.path = path2;
    }
    
    CGPathRelease(path);
    CGPathRelease(path2);
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self updateGraph];
}

- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    //[self updateGraph];
}

-(void)setVisibleStandardHeightLine:(BOOL)visibleStandardHeightLine
{
    _visibleStandardHeightLine = visibleStandardHeightLine;
    
    [_summaryCollectionView reloadData];
    [UIView animateWithDuration:0.5 animations:^{
        _standardHeightShapeLayer.opacity = (_visibleStandardHeightLine) ? 1 : 0;
    }];
}

-(void)shownCellWithIndex:(NSUInteger)index
{
    if ([self.heightDetailDataSource countOfHeightDetail] <= 0) return;
    
    NSLog(@"shownCellWithIndex:%ld", (unsigned long)index);
    [_summaryCollectionView selectItemAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0] animated:YES scrollPosition:UICollectionViewScrollPositionCenteredHorizontally];
}


@end
