//
//  EditFacebookViewController.m
//  HeightChart
//
//  Created by ne on 2014. 3. 24..
//  Copyright (c) 2014년 Kim Sehyun. All rights reserved.
//

#import "EditFacebookViewController.h"
#import "UIView+ImageUtil.h"
#import "TextViewDualPlaceHolder.h"
#import "UIButton+CheckAndRadio.h"
#import "UIViewController+DoleHeightChart.h"
#import "UIFont+DoleHeightChart.h"
#import "UIColor+ColorUtil.h"
#import "ToastController.h"
#import "BirthPopupController.h"
#import "WarningCoverView.h"
#import "UIViewController+PolyServer.h"
#import "InputChecker.h"

#import "BirthPopupController.h"
#import "CityPopupController.h"
#import "MessageBoxController.h"

#import "AppDelegate.h"

#import "Mixpanel.h"

@interface UIView (KeyboardHandler)

//-(BOOL)becomeFirstResponder;

@end

@implementation UIView (KeyboardHandler)

//-(BOOL)becomeFirstResponder{
//    
//
//    
//    return FALSE;
//}

@end

@interface EditFacebookViewController ()
{
    NSMutableData   *_receivedData;
    
    InputChecker *_inputChecker;
    
    
    NSInteger   _birthYearOnServer;
    NSString    *_cityOnServer;
    enum GenderType _genderOnServer;
    
}


@property (weak, nonatomic) IBOutlet UILabel *lblEmail;
@property (weak, nonatomic) IBOutlet TextViewDualPlaceHolder *txtViewBirth;
@property (weak, nonatomic) IBOutlet TextViewDualPlaceHolder *txtViewCity;

@property (weak, nonatomic) IBOutlet UIButton *btnCheckMale;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckFemale;

@property (weak, nonatomic) IBOutlet UILabel *lblMale;
@property (weak, nonatomic) IBOutlet UILabel *lblFemale;

@property (nonatomic, weak) IBOutlet UIButton *btnSave;


- (IBAction)clickSave:(id)sender;
@end

@implementation EditFacebookViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.lblEmail.text = self.userConfig.email;
    self.lblEmail.font = [UIFont defaultBoldFontWithSize:32/2];
    self.lblEmail.textColor = [UIColor colorWithRGB:0x606060];
    

    [self.txtViewBirth setDualPlaceHolderTextView:DoleDualPlaceHolderTextTypeBirth];
    self.txtViewBirth.textField.delegate = self;
    [self.txtViewCity setDualPlaceHolderTextView:DoleDualPlaceHolderTextTypeCity];
    
    [self addCommonCloseButton];
    
    [UIButton makeUIRadioButtons:self.btnCheckMale secondButton:self.btnCheckFemale];
    
    self.lblMale.font = [UIFont defaultRegularFontWithSize:34/2];
    self.lblMale.textColor = [UIColor colorWithRGB:0x606060];
    
    self.lblFemale.font = [UIFont defaultRegularFontWithSize:34/2];
    self.lblFemale.textColor = [UIColor colorWithRGB:0x606060];
    
    [self.btnSave setResizableImageWithType:DoleButtonImageTypeOrange];
    [self setupControlFlexiblePosition];
    [self setEnableInputControls:NO];
    
    self.txtViewCity.textField.delegate = self;
    self.txtViewBirth.textField.delegate = self;
    
    _inputChecker = [[InputChecker alloc]init];
    [_inputChecker addHostViewRecursive:self.view submitButton:self.btnSave checkBoxButtons:nil];
    
    self.btnSave.enabled = NO;
}



- (void)setEnableInputControls:(BOOL)enable
{
//    self.btnSave.enabled = enable;
    self.txtViewBirth.textField.enabled = enable;
    self.btnCheckMale.enabled = enable;
    self.btnCheckFemale.enabled = enable;
}

-(void)viewDidAppear:(BOOL)animated
{
    [self checkNetwork];
}

-(void)setupControlFlexiblePosition
{
    CGFloat DefalutMargin = 236;
    
    if ([UIScreen isFourInchiScreen]) {
        [self moveToPositionYPixel:(DefalutMargin) Control:self.lblEmail];
        [self moveToPositionYPixel:(DefalutMargin + 36 + 30) Control:self.txtViewBirth];
        [self moveToPositionYPixel:(DefalutMargin + 36 + 30 + 72 + 22) Control:self.txtViewCity];
        [self moveToPositionYPixel:(DefalutMargin + 36 + 30 + 72 + 22 + 72 + 38) Control:self.btnCheckMale];
        [self moveToPositionYPixel:(DefalutMargin + 36 + 30 + 72 + 22 + 72 + 38) Control:self.btnCheckFemale];
        [self moveToPositionYPixel:(DefalutMargin + 36 + 30 + 72 + 22 + 72 + 38 + 56 + 432) Control:self.btnSave];
    }
    else{
        
        DefalutMargin = 118;
        [self moveToPositionYPixel:(DefalutMargin) Control:self.lblEmail];
        [self moveToPositionYPixel:(DefalutMargin + 36 + 30) Control:self.txtViewBirth];
        [self moveToPositionYPixel:(DefalutMargin + 36 + 30 + 72 + 22) Control:self.txtViewCity];
        [self moveToPositionYPixel:(DefalutMargin + 36 + 30 + 72 + 22 + 72 + 38) Control:self.btnCheckMale];
        [self moveToPositionYPixel:(DefalutMargin + 36 + 30 + 72 + 22 + 72 + 38) Control:self.btnCheckFemale];
        [self moveToPositionYPixel:(DefalutMargin + 36 + 30 + 72 + 22 + 72 + 38 + 56 + 374) Control:self.btnSave];
    }
    
    [self moveToPositionY:self.btnCheckMale.frame.origin.y Control:self.lblMale];
    [self moveToPositionY:self.btnCheckFemale.frame.origin.y Control:self.lblFemale];
}

- (void)checkNetwork
{
    if (NO == self.isEnableNetwork){

//        
//        [self setEnableInputControls:NO];
//        
//        [ToastController showToastWithMessage:
//         NSLocalizedString(@"The network is unstable. \nPlease try again after checking the network", @"") duration:kToastMessageDurationType_Server_Long];
    }
    else{
        
        [self setEnableInputControls:YES];
        
        if (nil == self.userConfig.cachedCitynames)
        {
            if ([[self englishCountryNamesAndCities] objectForKey:[[self countriesCodesAndEnglishCommonNames] objectForKey:self.currentCountryCode]] == nil) {
                self.userConfig.cachedCitynames = @[];
            } else
            {
                self.userConfig.cachedCitynames = [[self englishCountryNamesAndCities] objectForKey:[[self countriesCodesAndEnglishCommonNames] objectForKey:self.currentCountryCode]];
            }
        }
        
        [self fillUserData];
    }
}

-(void)fillUserInfoWithServerBirthYear:(NSInteger)birthYear City:(NSString*)city Gender:(enum GenderType)gender
{
    _birthYearOnServer = birthYear;
    _cityOnServer = city;
    _genderOnServer = gender;
    
    NSString *birthYearText = [NSString stringWithFormat:@"%lu", (long)birthYear, nil];
    
    [self.txtViewBirth setText:birthYearText];
    if (gender == kGenderTypeMale){
        self.btnCheckMale.selected = YES;
    }
    else{
        self.btnCheckFemale.selected = YES;
    }
    
    [self.txtViewCity setText:city];
}


#pragma mark TextDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField        // return NO to disallow editing.
{
    if ((textField != self.txtViewBirth.textField) && (textField != self.txtViewCity.textField))
        return YES;
    
    [self.view.findFirstResponder resignFirstResponder];

    if (textField == self.txtViewBirth.textField){
    
//        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Popup" bundle:nil];
//        BirthPopupController *birthdayController = [story instantiateViewControllerWithIdentifier:@"BirthPopup"];
//        birthdayController.completion = ^(BirthPopupController *controller){
//            self.txtViewBirth.textField.text = [NSString stringWithFormat:@"%ld", (unsigned long)controller.birthYear, nil];
//            [controller releaseModal];
//        };
//        [birthdayController showModalOnTheViewController:nil];
        
        [self showBirthPopup];
    }
    else if (textField == self.txtViewCity.textField){
        [self showCityPopup];
    }
    
    return NO;
}

-(void)showBirthPopup
{
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Popup" bundle:nil];
    BirthPopupController *birthdayController = [story instantiateViewControllerWithIdentifier:@"BirthPopup"];
    if (self.txtViewBirth.textField.text.length > 0){
        birthdayController.birthYear = self.txtViewBirth.textField.text.integerValue;
    }
    birthdayController.completion = ^(BirthPopupController *controller){
        NSString *text = [NSString stringWithFormat:@"%lu", (unsigned long)controller.birthYear, nil];
        [self.txtViewBirth setText:text];
        [controller releaseModal];
    };
    [birthdayController showModalOnTheViewController:nil];
}

-(void)showCityPopup
{
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Popup" bundle:nil];
    CityPopupController *cityPopup = [story instantiateViewControllerWithIdentifier:@"City"];
    cityPopup.citynames = self.userConfig.cachedCitynames;
    
    if (self.txtViewCity.textField.text.length > 0){
        cityPopup.selectedCityname = self.txtViewCity.textField.text;
    }
    
    cityPopup.completion = ^(CityPopupController *controller){
        NSString *text = controller.selectedCityname;
        [self.txtViewCity setText:text];
        [controller releaseModal];
    };
    [cityPopup showModalOnTheViewController:nil];
}

#pragma mark Action

- (BOOL)isValidInput
{

    
    if (self.txtViewBirth.textField.text.length <= 0){
        
        [WarningCoverView addWarningToTargetView:self.txtViewBirth warningtype:kInputFieldErrorTypeWrongEmailFormat];
        
        return NO;
    }
    
    //    if (self.txtViewBirth.textField.text.length <= 0){
    //
    //        [WarningCoverView addWarningToTargetView:self.txtViewEmail warningtype:kInputFieldErrorTypeWrongEmail];
    //
    //        return NO;
    //    }
    
    
    return YES;
}

- (IBAction)clickSave:(id)sender
{
    [self.view.findFirstResponder resignFirstResponder];
    
    if ([self enableActionWithCurrentNetwork] == NO)return;
    
    if ([self isValidInput]){
        
        if (self.isRegisterMode)
            [self doRegisterFacebookUser];
        else
            [self doModifyUserInfo];
        
    }
}

- (void)doRegisterFacebookUser
{
    [self requestSignUpWithFacebookID:self.facebookID
                         FacebookName:self.facebookName
                        FacebookEmail:self.facebookEmail
                            BirthYear:[self.txtViewBirth.textField.text integerValue]
                                 City:self.txtViewCity.textField.text
                               Gender:self.btnCheckMale.selected ? kGenderTypeMale : kGenderTypeFemale];
}

- (void)doModifyUserInfo
{
    NSString *city = self.txtViewCity.textField.text;
    NSInteger birthYear = [self.txtViewBirth.textField.text integerValue];
    enum GenderType genType = self.btnCheckMale.selected ? kGenderTypeMale : kGenderTypeFemale;

//    [self requestEditUserInfoWithUserNo:self.userConfig.userNo Password:password BirthYear:birthYear City:city Gender:genType];
    [self requestEditFaceUserInfoWithBirthYear:birthYear City:city Gender:genType];
    
}

#pragma mark Server

- (void)fillUserData
{
    if (self.isRegisterMode){
        self.txtViewCity.textField.text = self.city;
        if (self.birthYear != -1){
            self.txtViewBirth.textField.text = [NSString stringWithFormat:@"%lu", self.birthYear];
        }
        self.btnCheckMale.selected = (self.genderType == kGenderTypeMale);
        self.btnCheckFemale.selected = (self.genderType == kGenderTypeFemale);
    }
    else{
        [self requestUserInfoWithUserNo:self.userConfig.userNo AuthKey:self.userConfig.authKey];
    }
}

- (NSMutableData*)receivedBufferData
{
    if (_receivedData == nil){
        _receivedData = [NSMutableData dataWithCapacity:0];
    }
    
    return _receivedData;
}

//-(void)didCompleteRecevedWithResultType:(DoleServerResultType)resultType ParsedData:(NSDictionary*)recevedData ParseError:(NSError*)error
//{
//    if (recevedData && error==nil && [recevedData[@"Return"] boolValue] ){
//        
//        if (resultType == kDoleServerResultTypeUserInfo){
//            
//            NSString *birthday = recevedData[@"BirthDay"];
//            NSNumber *gender = recevedData[@"Gender"];
//            
//            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//            [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
//            NSDate *birthdate = [formatter dateFromString:birthday];
//            
//            NSCalendar *calendar = [[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];
//            NSDateComponents *comp = [calendar components:NSCalendarUnitYear fromDate:birthdate];
//            NSInteger year = [comp year];
//            
////            self.txtViewBirth.textField.text = [NSString stringWithFormat:@"%lu", (long)year, nil];
////            if ([gender integerValue] == kGenderTypeMale){
////                self.btnCheckMale.selected = YES;
////            }
////            else{
////                self.btnCheckFemale.selected = YES;
////            }
//            
//            NSString *city = recevedData[@"AddressMain"];
//            
//            [self fillUserInfoWithServerBirthYear:year City:city Gender:(enum GenderType)[gender integerValue]];
//            
////            [self.txtViewCity setText:city];
//            
//            [self setEnableInputControls:YES];
//        }
//        else if (resultType == kDoleServerResultTypeEditSnsUserInfo){
//            
////            [ToastController showToastWithMessage:@"유저정보를 변경하였습니다!!!" duration:5];
//            [ToastController showToastWithMessage:
//             NSLocalizedString(@"It has been changed.", @"") duration:5];
//        }
//        else if (resultType == kDoleServerResultTypeSignUp){
//            NSInteger userNo = [recevedData[@"UserNo"] integerValue];
//            NSString *message = [NSString stringWithFormat:@"회원번호[%d]로 가입하였습니다", userNo, nil];
//            
//            //[self.userConfig signUpByEmail:self.txtViewEmail.textField.text password:self.txtViewPassword.textField.text];
//            [self.userConfig signUpByFacebookEmail:self.facebookEmail FacebookID:self.facebookID];
//            
//            [ToastController showToastWithMessage:message duration:2 completion:^{
//                //    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
//            }];
//            
//            //[self requestLogInWithUserID:self.userConfig.email Password:self.userConfig.password];
//            [self requestLogInWithSNSID:self.facebookID];
//        }
//        else if (resultType == kDoleServerResultTypeGetCityNames){
//            NSArray *citynames = recevedData[@"Provinces"];
//            self.userConfig.cachedCitynames = citynames;
//        }
//        else if (resultType == kDoleServerResultTypeSnsLogIn){
////            [ToastController showToastWithMessage:@"로그인 완료했습니다" duration:2 completion:^{
////                [self.navigationController dismissViewControllerAnimated:YES completion:nil];
////            }];
//            
//            [self popToLonOnViewController];
//
//        }
//
//        
//    }
//    else{
//        NSInteger errorCode = [recevedData[@"ReturnCode"] integerValue];
////        if ([self queryErrorCode:errorCode EmailView:nil PasswordView:self.txtViewPassword]) return;
//        
//        [self toastNetworkErrorWithErrorCode:errorCode];
//
//    }
//}

-(void)didCompleteRecevedWithResultType:(DoleServerResultType)resultType ParsedData:(NSDictionary*)recevedData ParseError:(NSError*)error
{
    if (recevedData && error==nil && [recevedData[@"Return"] boolValue] ){
        
        switch (resultType) {
            case kDoleServerResultTypeUserInfo: [self handleGetUserInfo:recevedData ParseError:error]; break;
            case kDoleServerResultTypeEditSnsUserInfo: [self handleEditUserInfo:recevedData ParseError:error]; break;
            case kDoleServerResultTypeSnsLogIn: [self handleLogInSnsUser:recevedData ParseError:error]; break;
            case kDoleServerResultTypeSignUp: [self handleSignUpSnsUser:recevedData ParseError:error]; break;
            case kDoleServerResultTypeGetCityNames: [self handleGetCityInfo:recevedData ParseError:error]; break;
            default: assert(0);
                break;
        }
    }
    else{
        NSInteger errorCode = [recevedData[@"ReturnCode"] integerValue];
        //        if ([self queryErrorCode:errorCode EmailView:nil PasswordView:self.txtViewPassword]) return;
        [self toastNetworkErrorWithErrorCode:errorCode];
    }
}

-(void)handleGetUserInfo:(NSDictionary*)recevedData ParseError:(NSError*)error
{
    NSString *birthday = recevedData[@"BirthDay"];
    NSNumber *gender = recevedData[@"Gender"];
    
    if (!gender) {
        gender = [NSNumber numberWithInteger:kGenderTypeMale];
    }
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [NSDate date];
    if (birthday && ![birthday isKindOfClass:[NSNull class]]) {
        date = [formatter dateFromString:birthday];
    }
    NSCalendar *calendar = [[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *comp = [calendar components:NSCalendarUnitYear fromDate:date];
    NSInteger year = [comp year];
    
    NSString *city = recevedData[@"AddressMain"];
    if ([city isEqual:[NSNull null]]) {
        city = @"";
    }
    [self fillUserInfoWithServerBirthYear:year City:city Gender:(enum GenderType)[gender integerValue]];
    [self setEnableInputControls:YES];
}
-(void)handleEditUserInfo:(NSDictionary*)recevedData ParseError:(NSError*)error
{
    [self closeMe:nil];
    
    [ToastController showToastWithMessage:
     NSLocalizedString(@"It has been changed.", @"") duration:kToastMessageDurationType_Auto];
}
-(void)handleLogInSnsUser:(NSDictionary*)recevedData ParseError:(NSError*)error
{
    NSNumber *no = recevedData[@"UserNo"];
    NSString *authKey = recevedData[@"AuthKey"];
    
    self.userConfig.authKey = authKey;
    self.userConfig.userNo = [no integerValue];
    self.userConfig.loginStatus = kUserLogInStatusFacebook;
    
//    [self popToLonOnViewController];
    [self closeWhenLoggedIn];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"SystemMigrationWarningShown"];
}
-(void)handleSignUpSnsUser:(NSDictionary*)recevedData ParseError:(NSError*)error
{
    // JP Added
    [[Mixpanel sharedInstance] registerSuperProperties:@{@"User Type": @"Registered"}];
    //longzhe cui added
    [[Mixpanel sharedInstance] track:@"Signup Success" properties:@{@"Authentication":@"Facebook"}];
    
    // JP added post 1.4.2
    [[Mixpanel sharedInstance] createAlias:self.facebookManager.emailAddress
                             forDistinctID:[Mixpanel sharedInstance].distinctId];
    [[Mixpanel sharedInstance] identify:[Mixpanel sharedInstance].distinctId];
    
    //[[Mixpanel sharedInstance] identify:self.facebookManager.emailAddress];
    [[Mixpanel sharedInstance].people set:@{@"$email":self.facebookManager.emailAddress}];
    [[Mixpanel sharedInstance].people set:@{@"Deleted":@"false"}];
    [[Mixpanel sharedInstance].people addPushDeviceToken:((AppDelegate*)([UIApplication sharedApplication].delegate)).userConfig.deviceTokenData];
    //longzhe cui added end
    
//    NSInteger userNo = [recevedData[@"UserNo"] integerValue];
//    NSString *message = [NSString stringWithFormat:@"회원번호[%d]로 가입하였습니다", userNo, nil];
    
    [self.userConfig signUpByFacebookEmail:self.facebookEmail FacebookID:self.facebookID];
    
//    [ToastController showToastWithMessage:message duration:2 completion:^{
//        //    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
//    }];
    
    [self requestLogInWithSNSID:self.facebookID];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"SystemMigrationWarningShown"];
}

-(void)handleGetCityInfo:(NSDictionary*)recevedData ParseError:(NSError*)error
{
    NSArray *citynames = recevedData[@"Provinces"];
    self.userConfig.cachedCitynames = citynames;
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)closeMe:(id)sender
{
    [[self.view findFirstResponder] resignFirstResponder];

    if (self.isRegisterMode){
//    [self showOkCancelConfirmMessage: NSLocalizedString(@"Do you want to cancel sign up?", @"") completion:^(BOOL isYes) {
    [self showYesNoConfirmMessage: NSLocalizedString(@"Do you want to cancel sign up?", @"") completion:^(BOOL isYes) {
        if (NO == isYes)return;
        
//        [self closeViewControllerAnimated:NO];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }];
    }
    else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)closeWhenLoggedIn
{
    if (self.appFirstTimeLogIn == NO &&
        self.requireLoginCompletion == nil){
        [self popToLonOnViewController];
        return;
    }
    
    [self.navigationController.presentingViewController dismissViewControllerAnimated:NO completion:nil];
    
    if (self.requireLoginCompletion){
        self.requireLoginCompletion(YES);
    }
}

@end
