//
//  CameraCollectionCell.m
//  HeightChart
//
//  Created by ne on 2014. 3. 13..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "CameraCollectionCell.h"

@interface CameraCollectionCell(){
    UIImageView *_frameImage;
}

@end

@implementation CameraCollectionCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self initFrameImage:self.bounds];
    }
    return self;
}


-(void)initFrameImage:(CGRect)frame{
    _frameImage = [[UIImageView alloc]initWithFrame:frame];
    [self addSubview:_frameImage];
}

-(void)updateFrameImage:(UIImage*)frameImage
{
    _frameImage.image = frameImage; 
}

@end
