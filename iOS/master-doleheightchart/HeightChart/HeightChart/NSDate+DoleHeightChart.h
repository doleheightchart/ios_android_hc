//
//  NSDate+DoleHeightChart.h
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 8..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (DoleHeightChart)
-(NSString*)stringDefaultFormat;
-(NSString*)stringForHeightViewer;
-(NSInteger)getYear;
-(NSInteger)getHours;
-(NSInteger)getMins;
-(NSInteger)getHoursFromNow;
-(NSInteger)getMinsFromNow;
-(NSInteger)getGlobalAge;
-(NSInteger)getGlobalAgeToDate:(NSDate*)toDate;
@end
