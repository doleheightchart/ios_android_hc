//
//  GuideView.h
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 4. 1..
//  Copyright (c) 2014년 Kim Sehyun. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    kGuideTypeHomeAddnickname,
    kGuideTypeAddHeight,
    kGuideTypeDetail,
    kGuideTypeInputHeight,
    kGuideTypeCameraFrame,
    kGuideTypeImportPhoto,
    kGuideTypeHomeAddnicknamePaper,
    kGuideTypeFirstTimeLogIn,
} GuideType;

@interface GuideView : UIView

@property (nonatomic) GuideType guideType;
@property (nonatomic, copy) void (^completion)();


+(instancetype)addGuideTarget:(UIView*)target Type:(GuideType)gType checkFirstTime:(BOOL)firstTime;

@end
