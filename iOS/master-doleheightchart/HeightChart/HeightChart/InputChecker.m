//
//  InputChecker.m
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 4. 9..
//  Copyright (c) 2014년 Kim Sehyun. All rights reserved.
//

#import "InputChecker.h"
#import "TextViewDualPlaceHolder.h"
#import "UIViewController+DoleHeightChart.h"
#import "WarningCoverView.h"

@interface InputCheckView : UIView
@property (nonatomic, copy) void (^pointInsideHook)(CGPoint);
@end
@implementation InputCheckView

-(BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event
{
    self.pointInsideHook(point);
    
    return NO;
}

@end

@interface InputChecker (){
    
    NSArray *_textviewArray;
    NSArray *_checkBoxArray;
    UIButton *_submitButton;
    UIView *_hostView;
    InputCheckView *_inputCheckView;
}

@end

@implementation InputChecker

-(void)addTextViews:(NSArray*)textviewArray
{
    _textviewArray = textviewArray;
    
    for (TextViewDualPlaceHolder *textView in _textviewArray) {
        [textView.textField addTarget:self action:@selector(endEditing:) forControlEvents:UIControlEventAllEditingEvents];
    }
}

-(void)removeDTextView:(TextViewDualPlaceHolder*)textview
{
    [textview.textField removeTarget:self action:@selector(endEditing:) forControlEvents:UIControlEventAllEditingEvents];
    
    NSMutableArray *textViewArray = (NSMutableArray*)_textviewArray;
    [textViewArray removeObject:textview];
}


-(void)addCheckBoxButtons:(NSArray*)checkBoxButtons
{
    _checkBoxArray = checkBoxButtons;
    for (UIButton *checkButton in _checkBoxArray) {
        [checkButton addTarget:self action:@selector(touchUpInside:) forControlEvents:UIControlEventTouchUpInside];
    }
}

-(void)touchUpInside:(id)sender
{
    [self updateSubmitEnable];
}

-(void)updateSubmitEnable
{
    if ([self isAllFieldFill]){
        [_submitButton setEnabled:YES];
    }
    else{
        [_submitButton setEnabled:NO];
    }
}

-(void)endEditing:(id)sender
{
//    UITextField *textField = sender;
    [self updateSubmitEnable];
}

-(BOOL)isAllFieldFill
{
    for (TextViewDualPlaceHolder *textView in _textviewArray) {
        
        if (self.ignorCurPasswordText &&
            self.ignorCurPasswordText == textView.textField &&
            textView.textField.text.length == 0){
            continue;
        }
        
        if (self.ignorPasswordTextOrg &&
            self.ignorPasswordTextOrg == textView.textField &&
            textView.textField.text.length == 0){
            continue;
        }
        
        if (self.ignorPasswordTextConfirm &&
            self.ignorPasswordTextConfirm == textView.textField &&
            textView.textField.text.length == 0){
            continue;
        }
        
        if ((self.ignorPasswordTextOrg) && (self.ignorPasswordTextConfirm) && (NO == [self.ignorPasswordTextOrg.text isEqualToString:self.ignorPasswordTextConfirm.text]))
            return FALSE;
        
        if (self.ignorCurPasswordText &&
            self.ignorCurPasswordText.text.length > 0 &&
            self.ignorPasswordTextOrg.text.length <= 0) {
            return FALSE;
        }
        
        if (self.ignorCurPasswordText &&
            self.ignorCurPasswordText.text.length <= 0 &&
            self.ignorPasswordTextOrg.text.length > 0) {
            return FALSE;
        }
        
        if (self.bigTextView && self.bigTextView.text.length <= 0)
            return FALSE;
        
        if (textView.textField.text.length == 0)
            return FALSE;
        
        if ([textView hasWarningCover])
            return FALSE;
    }
    
    for (UIButton *checkButton in _checkBoxArray) {
        if (checkButton.selected == NO)
            return FALSE;
    }
    
    return TRUE;
}


-(void)addSubmitButton:(UIButton*)submitButton
{
    _submitButton = submitButton;
    _submitButton.enabled = NO;
}

-(void)addHostView:(UIView*)hostView
{
    _hostView = hostView;
    
    _inputCheckView = [[InputCheckView alloc]initWithFrame:hostView.bounds];
    _inputCheckView.backgroundColor = [UIColor clearColor];
    [_hostView addSubview:_inputCheckView];

    __weak UIView *hv = _hostView;
    __weak NSArray *fieldArray = _textviewArray;
    __weak InputChecker *thisObject = self;
    __weak InputCheckView *checkView = _inputCheckView;
    _inputCheckView.pointInsideHook = ^(CGPoint point){
        
        UIView *currentResponder = [hv findFirstResponder];
        
//        NSLog(@"Responder frame is %@", NSStringFromCGRect(currentResponder.frame));
        NSLog(@"PointInside Point is %@", NSStringFromCGPoint(point));
        
        if (hv){
//            CGPoint pt = [hv convertPoint:point toView:currentResponder];
            CGPoint pt = [checkView convertPoint:point toView:nil];
            NSLog(@"Converted Point is %@", NSStringFromCGPoint(pt));
//            CGPoint pt = point;
            CGRect frame = currentResponder.frame;
            
            frame = [currentResponder convertRect:frame toView:nil];
//            NSLog(@"Converted frame is %@", NSStringFromCGRect(frame));

            if (NO == CGRectContainsPoint(frame, pt)){
                
                for (UIView *cv in fieldArray) {
                    
                    CGRect f = cv.frame;
                    f = [hv convertRect:f toView:nil];
                    NSLog(@"Child frame is %@", NSStringFromCGRect(f));
                    
                    if (CGRectContainsPoint(f, pt)){
                        return;
                    }
                }
                
                //[currentResponder resignFirstResponder];
                NSLog(@"InputChecker - ResignResponder");
                [thisObject updateSubmitEnable];
            }
        }
    };
}

-(void)addHostViewRecursive:(UIView*)hostView submitButton:(UIButton*)submitButton checkBoxButtons:(NSArray*)checkBoxArray
{
    NSMutableArray *childArray = [NSMutableArray array];
    
    for (id child in hostView.subviews) {
        if ([child isKindOfClass:[UITextView class]]) continue;
        
        if ([child isKindOfClass:[UITextField class]]){
            [childArray addObject:child];
        }
        else if ([child isKindOfClass:[TextViewDualPlaceHolder class]]){
            TextViewDualPlaceHolder *dualTextHolder = child;
            [childArray addObject:dualTextHolder];
        }
    }
    
    [self addTextViews:childArray];
    
    [self addCheckBoxButtons:checkBoxArray];
    
    [self addHostView:hostView];
    
    [self addSubmitButton:submitButton];
}

@end
