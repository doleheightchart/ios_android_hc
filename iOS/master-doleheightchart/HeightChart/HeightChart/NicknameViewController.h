//
//  NicknameViewController.h
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 2. 16..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "iCarousel.h"
#import "MyChild.h"

@interface NicknameViewController : UIViewController<iCarouselDataSource, iCarouselDelegate, UITextFieldDelegate>
@property (nonatomic) NSUInteger characterNo;
@property (nonatomic, copy) NSDate *inputBirthday;
@property (nonatomic, copy) NSString *inputNickName;
@property (nonatomic) BOOL isGirl;
@property (nonatomic, weak) NSFetchedResultsController *fetchResultController;
@property (nonatomic, weak) MyChild *myChild;

@property (nonatomic, copy) void (^saveCompletion)(NicknameViewController* sender);

@end
