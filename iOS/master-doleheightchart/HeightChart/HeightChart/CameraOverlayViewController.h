//
//  CameraOverlayViewController.h
//  HeightChart
//
//  Created by ne on 2014. 3. 7..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PhotoToolbarView.h"
#import "HeightSaveContext.h"


@interface CameraOverlayViewController : UIViewController <UIImagePickerControllerDelegate,
                                        UICollectionViewDataSource, UICollectionViewDelegate,
                                        UINavigationControllerDelegate >

@property (nonatomic, strong) UIImageView *imageSaveView;
@property (nonatomic, copy) void (^completion)(BOOL isSaved, CameraOverlayViewController *controller);
@property (nonatomic, retain) HeightSaveContext *heightSaveContext;
@property (nonatomic, weak) id<HeightSaveDelegate> heightSaveDelegate;



-(void) loadFrames;
-(void) loadCameraManager;
-(void) changeFrame:(PhotoButtonControl*)btn;
-(void) ChangeFocus:(bool)isFocus;




@end

