//
//  OnOffSwitch.m
//  HeightChart
//
//  Created by mytwoface on 2014. 3. 29..
//  Copyright (c) 2014년 Kim Sehyun. All rights reserved.
//

#import "OnOffSwitch.h"
#import "UIColor+ColorUtil.h"
#import "UIFont+DoleHeightChart.h"

@interface OnOffSwitch(){

    UIImageView     *_backgroundImageView;
    UIImageView     *_thumbImageView;
    UILabel         *_labelOn;
    UILabel         *_labelOff;

}

@end


@implementation OnOffSwitch

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initControl];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self){
        [self initControl];
    }
    return self;
}

- (void)initControl
{
    self.backgroundColor = [UIColor clearColor];

    _backgroundImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 136/2, 62/2)];
    [self addSubview:_backgroundImageView];

    _labelOn = [[UILabel alloc]initWithFrame:CGRectMake(8/2, 14/2, 61/2, 34/2)];
    _labelOn.backgroundColor = [UIColor clearColor];
    _labelOn.textAlignment = NSTextAlignmentCenter;
    _labelOn.textColor = [UIColor colorWithRGB:0xffffff];
    _labelOn.font = [UIFont defaultBoldFontWithSize:30/2];
    _labelOn.text = @"On";
    [self addSubview:_labelOn];

    _labelOff = [[UILabel alloc]initWithFrame:CGRectMake((59+8)/2, 14/2, 61/2, 34/2)];
    _labelOff.backgroundColor = [UIColor clearColor];
    _labelOff.textAlignment = NSTextAlignmentCenter;
    _labelOff.textColor = [UIColor colorWithRGB:0xffffff];
    _labelOff.font = [UIFont defaultBoldFontWithSize:30/2];
    _labelOff.text = @"Off";
    [self addSubview:_labelOff];

    _thumbImageView = [[UIImageView alloc]initWithFrame:[self frameThumbOff]];
    _thumbImageView.image = [UIImage imageNamed:@"switch_button"];
    [self addSubview:_thumbImageView];

    [self setLayoutByOnOff:FALSE Animated:NO];
}

- (CGRect)frameThumbOn
{
    return CGRectMake((136-62)/2, 0, 62/2, 62/2);
}

- (CGRect)frameThumbOff
{
    return CGRectMake(0, 0, 62/2, 62/2);
}

-(void)setLayoutByOnOff:(BOOL)isOn Animated:(BOOL)animated
{
    void (^changeLayout)() = ^{

//        if (isOn){
//            _backgroundImageView.image = [UIImage imageNamed:@"on_switch_button"];
//        }
//        else{
//            _backgroundImageView.image = [UIImage imageNamed:@"off_switch_button"];
//        }

        _labelOn.layer.opacity = isOn ? 1 : 0;
        _labelOff.layer.opacity = isOn ? 0 : 1;

        _thumbImageView.frame = isOn ? [self frameThumbOn] : [self frameThumbOff];

    };

    void (^changeBackImage)() = ^{
        if (isOn){
            _backgroundImageView.image = [UIImage imageNamed:@"on_switch_button"];
        }
        else{
            _backgroundImageView.image = [UIImage imageNamed:@"off_switch_button"];
        }
    };

    if (NO == animated){
        changeBackImage();
        changeLayout();
    }
    else {

//        [UIView transitionWithView:_backgroundImageView duration:0.2 options:(UIViewAnimationOptionTransitionCrossDissolve) animations:changeBackImage completion:^(BOOL fisnished){
//            [UIView animateWithDuration:0.3 animations:changeLayout];
//        }];

        [UIView animateWithDuration:0.15 animations:changeLayout completion:^(BOOL finished) {
            [UIView transitionWithView:_backgroundImageView
                              duration:0.15
                               options:(UIViewAnimationOptionTransitionCrossDissolve)
                            animations:changeBackImage
                            completion:nil];
        }];
    }

}

-(void)setIsCheckedOn:(BOOL)isCheckedOn
{
    _isCheckedOn = isCheckedOn;

    [self setLayoutByOnOff:_isCheckedOn Animated:YES];
}


- (void)finishEvent
{
    self.isCheckedOn = !self.isCheckedOn;

	self.highlighted = NO;
	[self sendActionsForControlEvents:UIControlEventValueChanged];

    [self sendActionsForControlEvents:UIControlEventTouchUpInside];
}

- (void)cancelEvent
{
    self.highlighted = NO;
}

#pragma mark Tracking

-(BOOL)beginTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
{
	self.highlighted = YES;
	[self sendActionsForControlEvents:UIControlEventTouchDown];
	return YES;
}

-(BOOL)continueTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
{
    [self setNeedsDisplay];
	[self sendActionsForControlEvents:UIControlEventTouchDragInside];
	return YES;
}
-(void)cancelTrackingWithEvent:(UIEvent *)event
{
    [self cancelEvent];
}
-(void)endTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
{
    CGPoint pt = [touch locationInView:self];

    if (CGRectContainsPoint(self.bounds, pt)){
        [self finishEvent];
    }
    else{
        [self cancelEvent];
        [self sendActionsForControlEvents:UIControlEventTouchUpOutside];
    }
}

@end
