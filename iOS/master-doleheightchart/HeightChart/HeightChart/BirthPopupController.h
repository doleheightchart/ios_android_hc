//
//  BirthPopupController.h
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 2. 11..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "PopupController.h"

@interface BirthPopupController : PopupController<UICollectionViewDataSource, UICollectionViewDelegate>
@property (nonatomic) NSUInteger birthYear;
@property (nonatomic, copy) void (^completion)(BirthPopupController* controller);
@end
