//
//  NicknameViewController.m
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 2. 16..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "NicknameViewController.h"
#import "UIView+Animation.h"
#import "UIView+ImageUtil.h"
#import "UIColor+ColorUtil.h"
#import "TextViewDualPlaceHolder.h"
#import "UIViewController+DoleHeightChart.h"
#import "UIButton+CheckAndRadio.h"
#import "UIFont+DoleHeightChart.h"
#import "BirthdayPopupController.h"
#import "NSDate+DoleHeightChart.h"
#import "WarningCoverView.h"
#import "DoleInfo.h"
#import "MyChild.h"
#import "InputChecker.h"

#import "Mixpanel.h"

#pragma mark SelectedNicknameView

@interface SelectedNicknameView : UIView {
    UIImageView *_lightView;
    UIImageView *_nicknameBoxView;
    UILabel     *_nicknameLabel;
}
@end

@implementation SelectedNicknameView

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self){
        [self initializeView];
    }
    return self;
}

-(void)initializeView
{
    _lightView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 258/2, 258/2)];
    _lightView.image = [UIImage imageNamed:@"add_nickname_light"];
    
    _nicknameBoxView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 211/2, 258/2, 59/2)];
    _nicknameBoxView.image = [UIImage imageNamed:@"add_nickname_box"];
    
    _nicknameLabel = [[UILabel alloc] initWithFrame:CGRectMake(49/2, (211+23)/2, 160/2, 31/2)];
    _nicknameLabel.backgroundColor = [UIColor clearColor];
    _nicknameLabel.font = [UIFont defaultBoldFontWithSize:28/2];
    _nicknameLabel.textColor = [UIColor colorWithRGB:0xffffff];
    _nicknameLabel.textAlignment = NSTextAlignmentCenter;
    
    [self addSubview:_lightView];
    [self addSubview:_nicknameBoxView];
    [self addSubview:_nicknameLabel];
}

-(void)setNickname:(NSString*)nickName
{
    _nicknameLabel.text = nickName;
}

@end

#pragma mark NicknameViewController

@interface NicknameViewController (){
//    NSArray     *_nicknameArray;
    
    InputChecker    *_inputChecker;
}

@property (nonatomic, weak) IBOutlet iCarousel *carousel;
@property (nonatomic, strong) IBOutlet UISlider *arcSlider;
@property (nonatomic, strong) IBOutlet UISlider *radiusSlider;
@property (nonatomic, strong) IBOutlet UISlider *tiltSlider;
@property (nonatomic, strong) IBOutlet UISlider *spacingSlider;

@property (weak, nonatomic) IBOutlet UILabel *labelArc;
@property (weak, nonatomic) IBOutlet UILabel *labelRadius;
@property (weak, nonatomic) IBOutlet UILabel *labelSpacing;


@property (nonatomic, weak) IBOutlet UITextField *nickName;
@property (nonatomic, weak) IBOutlet UITextField *birthday;
@property (nonatomic, weak) IBOutlet UIButton *boyRadio;
@property (nonatomic, weak) IBOutlet UIButton *girlRadio;
@property (nonatomic, weak) IBOutlet UILabel *boyLabel;
@property (nonatomic, weak) IBOutlet UILabel *girlLabel;
@property (nonatomic, weak) IBOutlet UIButton *btnSave;
@property (weak, nonatomic) IBOutlet TextViewDualPlaceHolder *nickNameTextView;
@property (weak, nonatomic) IBOutlet TextViewDualPlaceHolder *birthdayTextView;

@property (nonatomic, strong) SelectedNicknameView *selectedMaskView;

@property (nonatomic, weak) IBOutlet UIImageView *bottomImageView;

-(IBAction)updateCaroul:(id)sender;
-(IBAction)closeForm:(id)sender;
- (IBAction)pressSave:(id)sender;


@end

@implementation NicknameViewController

const CGFloat kPerfectArcValue = 0.2;
const CGFloat kPerfectRadiusValue = 1.5;
const CGFloat kPerfectSpacingValue = 0.4;

#define kTAG_NameView 1818
#define kTAG_ChooseAvatarLabel 1919

- (void)setupDoleCharacterNames
{
    //DoleInfo에서 종합관리한다.
//   _nicknameArray = @[@"Dolekey",@"Rilla",@"Smong",@"Faka",@"Panzee"];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self setupDoleCharacterNames];

    [self.btnSave setResizableImageWithType:DoleButtonImageTypeOrange];
    
    self.carousel.type = iCarouselTypeRotary;
    self.carousel.dataSource = self;
    self.carousel.delegate = self;
    [self.carousel reloadDataFirstTime];

    [self decorateUI];

    //[self createChooseAvatarLabel];
    
//    self.nickNameTextView.textField.delegate = self;
    self.birthdayTextView.textField.delegate = self;

    [self setupControlFlexiblePosition];
    
    [self addCommonCloseButton];

    [self initInputData];
    
    _inputChecker = [[InputChecker alloc]init];
    [_inputChecker addHostViewRecursive:self.view submitButton:self.btnSave checkBoxButtons:nil];
    
    if (self.inputNickName){ // Edit Mode
        [_inputChecker updateSubmitEnable];
    }
}

-(void)initInputData
{
    if (self.inputNickName) {
        
        self.nickNameTextView.textField.text = self.inputNickName;
        self.birthdayTextView.textField.text = [self.inputBirthday stringForHeightViewer];
        
        self.girlRadio.selected = self.isGirl;
        self.boyRadio.selected = !self.isGirl;
        
        [self.carousel scrollByNumberOfItems:self.characterNo - 1 duration:0];
    }
    else{
        [self.carousel scrollByNumberOfItems:0 duration:0];
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillAnimate:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillAnimate:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)keyboardWillAnimate:(NSNotification *)notification
{
    CGRect keyboardBounds;
    [[notification.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardBounds];

    [UIView animateWithDuration:0.3 animations:^{
        
//        CGPoint c = self.view.center;
        CGFloat moveY = 0;

        if([notification name] == UIKeyboardWillShowNotification){
//            c.y -= (96/2);
            _carousel.layer.opacity = 0.5;
            moveY = -(96/2);
        }
        else{
//            c.y += (96/2);
            _carousel.layer.opacity = 1;
            moveY = (96/2);
            
            // only self.view만 적용되기에
            moveY = self.view.frame.origin.y * -1.0;
        }
        
//        self.view.center = c;
        
//        NSArray *array = @[self.girlLabel, self.girlRadio, self.boyRadio, self.boyLabel, self.birthdayTextView, self.nickNameTextView, self.carousel, self.bottomImageView ];
        NSArray *array = @[self.view]; //그냥 뷰만 올리기만 하기로함.

        for (UIView *v  in array) {
            CGPoint ct = v.center;
            v.center = CGPointMake(ct.x, ct.y + moveY);
        }
        
    }];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)decorateUI
{
    [self.nickNameTextView setDualPlaceHolderTextView:DoleDualPlaceHolderTextTypeNickName];
    self.nickNameTextView.textField.secondPlaceHodler = @""; 
    [self.birthdayTextView setDualPlaceHolderTextView:DoleDualPlaceHolderTextTypeBirth];
    
    [self.nickNameTextView.textField addTarget:self.nickNameTextView.textField
                                        action:@selector(resignFirstResponder)
                              forControlEvents:UIControlEventEditingDidEndOnExit];

//    self.nickNameTextView.textField.keyboardType = UIKeyboardTypeASCIICapable;
    
    [self makeSkyBackground];
    [UIButton makeUIRadioButtons:self.boyRadio secondButton:self.girlRadio];
    self.boyRadio.selected = YES;
    
    self.boyLabel.font = [UIFont defaultRegularFontWithSize:34/2];
    self.boyLabel.textColor = [UIColor colorWithRGB:0x606060];
    
    self.girlLabel.font = [UIFont defaultRegularFontWithSize:34/2];
    self.girlLabel.textColor = [UIColor colorWithRGB:0x606060];
    
    self.birthdayTextView.textField.delegate = self;
    
    self.nickNameTextView.maxLength = 15;
    
//    CGRect r = CGRectMake((self.carousel.bounds.size.width - (258/2)) / 2 ,
//                          (94+258+12+24)/2,
//                          258/2,
//                          32/2);
    
    CGRect r = CGRectMake((self.carousel.bounds.size.width - (400/2)) / 2 ,
                          (94+258+12+24)/2,
                          400/2,
                          32/2);
    
    UILabel *label = [[UILabel alloc]initWithFrame:r];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor colorWithRGB:0x00a6ac];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont defaultBoldFontWithSize:28/2];
    label.text = NSLocalizedString(@"Choose an avatar",@"");
    [self.carousel addSubview:label];
}

//- (void)createChooseAvatarLabel
//{
//    CGRect r = CGRectMake(0, 0, 0, 0);
//
//    UILabel *label = [[UILabel alloc]initWithFrame:r];
//    label.textAlignment = NSTextAlignmentCenter;
//    label.textColor = [UIColor colorWithRGB:0xffffff];
//    label.font = [UIFont defaultBoldFontWithSize:28/2];
//    label.text = NSLocalizedString(@"Choose an avatar",@"");
//
//    [self.carousel addSubview:label];
//}

#pragma mark TextDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField        // return NO to disallow editing.
{
    if (textField == self.nickNameTextView.textField) return YES;
    
    [self.nickNameTextView.textField resignFirstResponder];
    
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Popup" bundle:nil];
    BirthdayPopupController *birthdayController = [story instantiateViewControllerWithIdentifier:@"BirthdayPopup"];
    birthdayController.birthday = self.inputBirthday;
    birthdayController.completion = ^(NSDate* birthday){
        
        NSString *text = [birthday stringForHeightViewer];
//        self.birthdayTextView.textField.text = [birthday stringForHeightViewer];
        [self.birthdayTextView setText:text];
        self.inputBirthday = birthday;
    };
    [birthdayController showModalOnTheViewController:nil];
    
    return NO;
}

//- (void)textFieldDidBeginEditing:(UITextField *)textField;           // became first responder
//- (BOOL)textFieldShouldEndEditing:(UITextField *)textField;          // return YES to allow editing to stop and to resign first responder status. NO to disallow the editing session to end
//- (void)textFieldDidEndEditing:(UITextField *)textField;

-(void)setupControlFlexiblePosition
{
    if ([UIScreen isFourInchiScreen]){
        
    }
    else{
        NSArray *adjustControls = @[
                                    self.boyRadio,
                                    self.girlRadio,
                                    self.nickNameTextView,
                                    self.birthdayTextView,
                                    self.boyLabel,
                                    self.girlLabel
                                    ];
        [self moveByPositionY:-30 Controls:adjustControls];
    }
}

-(SelectedNicknameView*)selectedMaskView
{
    if (_selectedMaskView == nil){
        
        _selectedMaskView = [[SelectedNicknameView alloc] initWithFrame:CGRectMake(0, 0, 258/2, 258/2)];
        [_selectedMaskView setNickname:@"nickname"];



    }
    
    return _selectedMaskView;
}

-(IBAction)updateCaroul:(id)sender
{
    UISlider *slider = (UISlider*)sender;
    UILabel *label = nil;
    if (slider.tag == 101){
        label =  self.labelArc;
    }
    else if (slider.tag == 102)
        label = self.labelRadius;
    else if (slider.tag == 103)
        label = self.labelSpacing;
    
    label.text = [NSString stringWithFormat:@"Arc[%.1f]", slider.value, nil];

    
    [self.carousel reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




#pragma mark -
#pragma mark iCarousel methods

- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    return 5;
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view
{
    //create new view if no view is available for recycling
    
    int imageIndex = index % 5;
    if (view == nil)
    {
        UIImageView *imgview = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 258/2, 258/2)];
        NSString *imageName = [NSString stringWithFormat:@"Add_nickname_center_monkey_%02d", (imageIndex+1), nil];
        imgview.image = [UIImage imageNamed:imageName];
    
        view = imgview;
    }
    else
    {
        UIImageView *imgview = (UIImageView*)view;
        NSString *imageName = [NSString stringWithFormat:@"Add_nickname_center_monkey_%02d", (imageIndex+1), nil];
        imgview.image = [UIImage imageNamed:imageName];
    }
    
    
    return view;
}

- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    switch (option)
    {
        case iCarouselOptionWrap:
        {
            return YES;
        }
        case iCarouselOptionFadeMax:
        {
            if (carousel.type == iCarouselTypeCustom)
            {
                return 0.0f;
            }
            return value;
        }
        case iCarouselOptionArc:
        {
            return 2 * M_PI * kPerfectArcValue;
        }
        case iCarouselOptionRadius:
        {
            return value * kPerfectRadiusValue;
        }
        case iCarouselOptionTilt:
        {
            return 1;
        }
        case iCarouselOptionSpacing:
        {
            return value * kPerfectSpacingValue;
        }
        default:
        {
            return value;
        }
    }
}

- (void)carouselDidScroll:(iCarousel *)carousel
{
    [self.selectedMaskView removeFromSuperview];
}

- (void)carouselCurrentItemIndexDidChange:(iCarousel *)carousel
{
    
}

- (void)carouselDidEndScrollingAnimation:(iCarousel *)carousel
{
    for (int i = 0; i < 50; i++) {
        CGFloat offSet = [carousel offsetForItemAtIndex:i];
        UIView *view = [carousel itemViewAtIndex:i];
        //UIView *lightView = [view viewWithTag:1818];
        if (offSet == 0){
            
            //[view animateScaleEffectWithDuration:1];
            
            [view animateScaleEffectWithDuration:0.3 MinScale:1 MaxScale:1.2 completionBlock:^{
                NSLog(@"Completed");
                //lightView.hidden = NO;
                
                NSInteger indexAfterAnimation = [carousel indexOfItemView:view];
                CGFloat offSetAfterAnimation = [carousel offsetForItemAtIndex:indexAfterAnimation];
                
                if (offSetAfterAnimation == 0){
                    if ([view viewWithTag:kTAG_NameView] == nil){
                        if (self.selectedMaskView.superview){
                            [self.selectedMaskView removeFromSuperview];
                            _selectedMaskView = nil;
                        }

//                        NSString *characterName = _nicknameArray[indexAfterAnimation];
                        enum DoleCharacterType characterType = (enum DoleCharacterType)indexAfterAnimation;
                        NSString *characterName = [DoleInfo doleCharaterNameWithType:characterType];

                        [self.selectedMaskView setNickname:characterName];
                        [view addSubview:self.selectedMaskView];
                        self.characterNo = indexAfterAnimation;
                    }
                }
                
            }];
            
            break;
        }
        else{
            
            //lightView.hidden = YES;
            
            if ([view viewWithTag:kTAG_NameView]){
                [[view viewWithTag:kTAG_NameView] removeFromSuperview];
            }
        }
    }
}

-(IBAction)closeForm:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(BOOL)isCheckExistNickname
{
    NSString *nickname = self.nickNameTextView.textField.text;
    
    for (MyChild *child in self.fetchResultController.fetchedObjects) {
        
        if (self.myChild != child && [child.nickname isEqualToString:nickname]){
            
            [WarningCoverView addWarningToTargetView:self.nickNameTextView warningtype:kInputFieldErrorTypeWrongAlreadyExistNickname];
            return NO;
        }
        
    }
    
    return YES;
}

-(BOOL)isValidInput
{
    
    if ([self.nickNameTextView checkIsValidNickname] == NO) return NO;

    if ([self isCheckExistNickname] == NO) return NO;
    
//    if ((self.nickNameTextView.textField.text.length <= 0) || (self.nickNameTextView.textField.text.length > 15)) {
//        [WarningCoverView addWarningToTargetView:self.nickNameTextView warningtype:kInputFieldErrorTypeWrongNickname];
//        return NO;
//    }
    
//    if (self.birthdayTextView.textField.text.length <= 0){
//        
//        [WarningCoverView addWarningToTargetView:self.birthdayTextView warningmessage:@"생년월일을 선택하세요."];
//        
//        return NO;
//    }
    
    return YES;
}

- (IBAction)pressSave:(id)sender {
    
    if (NO == [self isValidInput]) return;
    self.isGirl = self.girlRadio.selected;
    self.inputNickName = self.nickNameTextView.textField.text;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *dateComponents = [calendar components:(NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit|NSTimeZoneCalendarUnit)
                                                   fromDate:self.inputBirthday];
    
    
    
    NSInteger year = dateComponents.year;
    NSInteger month = dateComponents.month;
    NSInteger day = dateComponents.day;
    NSString *gender = self.isGirl? @"Girl":@"Boy";
    
    NSDictionary *props = @{@"Child Birth Year":[NSNumber numberWithInteger:year],
                            @"Child Birth Month" : [NSNumber numberWithInteger:month],
                            @"Child Birth Day" : [NSNumber numberWithInteger:day],
                            @"Child Gender": gender
                                };
    //longzhe cui added
    [[Mixpanel sharedInstance] track:@"Add Child" properties:props];
    

    
    
    self.saveCompletion(self);
}

#pragma mark KeyBoard




@end
