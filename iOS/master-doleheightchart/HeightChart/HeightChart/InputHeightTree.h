//
//  InputHeightTree.h
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 2. 28..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InputHeightTree : UIControl<UICollectionViewDataSource, UICollectionViewDelegate,UITextFieldDelegate>

@property (nonatomic) CGFloat height;

@property (nonatomic, copy) void (^saveHandler)(CGFloat inputHeight);

-(void)initialize;

-(void)setInputHeight:(CGFloat)height textOutput:(BOOL)isTextoutput;

@end
