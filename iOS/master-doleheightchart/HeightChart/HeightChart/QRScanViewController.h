//
//  QRScanViewController.h
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 2. 25..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBarSDK.h"
#import "HeightSaveContext.h"

@interface QRScanViewController : UIViewController<ZBarReaderViewDelegate>

@property (nonatomic, copy) void (^completion)(BOOL finished, NSString* qrCode);
@property (nonatomic, retain) HeightSaveContext *heightSaveContext;
@property (nonatomic, weak) id<HeightSaveDelegate> heightSaveDelegate;
@end
