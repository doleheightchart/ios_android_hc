//
//  SignInTermsViewController.m
//  HeightChart
//
//  Created by ne on 2014. 2. 11..
//  Copyright (c) 2014년 ne. All rights reserved.
//

#import "SignInTermsViewController.h"
#import "UIView+ImageUtil.h"
#import "UIViewController+DoleHeightChart.h"

@interface SignInTermsViewController ()

@end

@implementation SignInTermsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self addCommonCloseButton];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
