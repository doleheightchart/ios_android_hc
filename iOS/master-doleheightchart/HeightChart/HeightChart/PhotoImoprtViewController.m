//
//  PhotoImoprtViewController.m
//  HeightChart
//
//  Created by ne on 2014. 3. 14..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "PhotoImoprtViewController.h"
#import "UIViewController+DoleHeightChart.h"
#import "UIFont+DoleHeightChart.h"
#import "GuideView.h"
#import "UIViewController+DoleHeightChart.h"

typedef enum : NSUInteger {
    
    kGestureStateNone,
    kGestureStatePinch,
    kGestureStatePan,
    kGestureStateRotate,
} GestureState;

@interface PhotoImoprtViewController ()
{
    CGFloat _previousRotation;
    
    CGFloat _priviousZoom;
    
    CGPoint _previousPanPoint;
    
    GestureState _curGestureState;
    UIButton * _btnClose;
}
//@property (nonatomic, retain) UIImageView *photoView;
@property (weak, nonatomic) IBOutlet UIScrollView *importScrollView;
@property (weak, nonatomic) IBOutlet UIImageView *importImageView;
@property (weak, nonatomic) IBOutlet UIImageView *importGuideView;
@property (weak, nonatomic) IBOutlet UIImageView *importBGImgView;

@property (weak, nonatomic) IBOutlet UIButton *btnDone;
- (IBAction)clickDone:(id)sender;
@end

@implementation PhotoImoprtViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self setupControlByScreenSize];
    
    [self initPhotoViewScroll];
    
    
    [self addCameraCloseButton];
    
}

-(void) setupControlByScreenSize
{
    if ([UIScreen isFourInchiScreen])
    {
        
    }
    else{
        CGRect framescroll = self.importScrollView.frame;
        framescroll.origin.x = 24;
        framescroll.origin.y = 40;
        framescroll.size.height = 408;
        self.importScrollView.frame = framescroll;
        self.importBGImgView.frame = framescroll;
        self.importBGImgView.image = [UIImage imageNamed:@"import_bg_960"];
        
        //스크롤뷰 내에 붙어 있는 화면들
        CGRect frame = self.importImageView.frame;
        frame.origin.y = 0;
        frame.size.height = 408;
        self.importImageView.frame =frame;
        self.importGuideView.frame = frame;
        self.importGuideView.image = [UIImage imageNamed:@"import_guide_960"];
        
    }
}

-(void) initPhotoViewScroll
{
    [self.importImageView setContentMode:UIViewContentModeScaleAspectFit];
    self.importImageView.image = self.photo;
    
    self.importScrollView.contentSize = self.importImageView.image.size;
    self.importScrollView.delegate = self;

    self.btnDone.adjustsImageWhenHighlighted = NO;
    self.btnDone.backgroundColor = [UIColor clearColor];
    self.btnDone.titleLabel.font = [UIFont defaultBoldFontWithSize:32/2];
    [self.btnDone setTitle:NSLocalizedString(@"Done", @"")  forState:UIControlStateNormal];

    [self.btnDone setBackgroundImage:[UIImage imageNamed:@"camera_btn_center_normal"] forState:UIControlStateNormal];
    [self.btnDone setBackgroundImage:[UIImage imageNamed:@"camera_btn_center_press"] forState:UIControlStateHighlighted];
    
    UIRotationGestureRecognizer *rotationGesture = [[UIRotationGestureRecognizer alloc]
                                                    initWithTarget:self action:@selector(rotationGestureImage:)];

    [self.view addGestureRecognizer:rotationGesture];
    
    
    UIPinchGestureRecognizer *pinchRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self
                                                                                          action:@selector(scaleGesturePinchImage:)];
    
    [self.view addGestureRecognizer:pinchRecognizer];
    
    
    
    UIPanGestureRecognizer *panRecognizer = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(positionHandlePanImage:)];
    [self.view addGestureRecognizer:panRecognizer];
    
    
    panRecognizer.maximumNumberOfTouches = 1;
    
    pinchRecognizer.delegate = self;
    panRecognizer.delegate = self;
    rotationGesture.delegate = self;
}



- (IBAction)clickDone:(id)sender {
    
//    self.photo = [self drawCropedImageWithTransform];
    self.photo = [self drawCropedImageWithTransformNew];
    self.OnDoneClick(self.photo);
}



-(void)closeMe:(id)sender
{
    [[self.view findFirstResponder] resignFirstResponder];

    [self closeViewControllerAnimated:NO];
}


- (void)viewDidAppear:(BOOL)animated
{
    GuideView *gv = [GuideView addGuideTarget:self.view Type:kGuideTypeImportPhoto checkFirstTime:YES];
    if (gv){
        gv.completion = ^{};
    }
    
    self.importScrollView.zoomScale = 1.0;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark Draw Image [Photo Edit & Draw Image]

//-(UIImage*)drawCropedImageWithTransform
//{
//    
//    //기본 이미지에 맞는 이미지를 생성한다.
//    //기본이미지
//    UIImage *baseImage;
//    CGSize baseSize;
//    if ([UIScreen isFourInchiScreen]) {
//        baseImage = [UIImage imageNamed:@"import_bg"];
//        baseSize = CGSizeMake(640, 1136);
//    }
//    else{
//        baseImage = [UIImage imageNamed:@"import_bg_960"];
//        baseSize = CGSizeMake(640, 960);
//    }
//
//    
//    UIImage *srcImage = self.importImageView.image;
//    CGSize orgSize = self.importImageView.image.size;
//    CGPoint orgMoveOffset;
//    // *2를 하는 이유는 포인트 좌표를 -> 픽셀좌표로 변경,  나누기 0.85는 현제 화면 에 보여지는 값이 0.75 축소된값이기 때문에 다시 재보정 해준다.
//    // 센터값이므로 화면의 반값을 빼줘야 offset값이 나온다.
//    orgMoveOffset.x =   self.importImageView.center.x*2/0.85 - baseSize.width/2;
//    orgMoveOffset.y =   self.importImageView.center.y*2/0.85 - baseSize.height/2;
//    
//    NSLog(@"Center X = %.1f Y = %.1f ", self.importImageView.center.x , self.importImageView.center.y);
//    //모든 비율을 기본 사이즈에 맞춘다.
//    CGFloat scaleX =  baseSize.width / orgSize.width;
//    CGFloat scaleY = baseSize.height / orgSize.height;
//    //    CGFloat scaleBaseXY = baseSize.width/baseSize.height;
//    //    CGFloat scaleOrgXY = orgSize.width/orgSize.height;
//
//    //모든 Scale은 X에 맞춘다 //AspectFit으로 설정해 놓는다.
//    CGFloat orgScreenWidth = orgSize.width * scaleX;
//    CGFloat orgScreenHeight = orgSize.height * scaleX;
//    
//    
//    CGFloat offsetHeight0Point = (baseSize.height - orgScreenHeight)/2;
//    
//    
//    // 어떤 사이즈에 그림을 그릴지
//    // 일단 백그라운드 이미지 사이즈에 맞춘다.
//    UIGraphicsBeginImageContextWithOptions(baseSize, YES, 1);
//    
//    //배경을 그린다.
//    [baseImage drawInRect:CGRectMake(0, 0, baseSize.width, baseSize.height)];
//    
//    NSLog(@"image_bg size  width = %.1f height = %.1f ", baseSize.width , baseSize.height);
//    
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    CGAffineTransform tm = self.importImageView.transform;
//    
//    // *2를 하는 이유는 포인트 좌표를 -> 픽셀좌표로 변경,  나누기 0.85는 현제 화면 에 보여지는 값이 0.75 축소된값이기 때문에 다시 재보정 해준다.
//    //    CGFloat movePosX = orgMoveOffset.x * 2 * scaleX  / 0.85 ;
//    //    CGFloat movePosY = orgMoveOffset.y * 2 * scaleY  / 0.85 ;
//    
//    //그릴 위치의 원정음 화면중앙으로 변경한다.
//    CGFloat translateX =  (srcImage.size.width*scaleX )/2 + orgMoveOffset.x;
//    CGFloat translateY =  (srcImage.size.height*scaleX)/2 + orgMoveOffset.y + offsetHeight0Point;
//    
//    
//    CGContextTranslateCTM(context, translateX, translateY );
//    CGContextConcatCTM(context, tm);
//    
//    // 그림은 화면중앙에서 그림 사이즈만큼 마이너스 한곳에서 그린다. (각도 때문에 )
//    CGFloat destImagePosX = -(srcImage.size.width*scaleX)/2;
//    CGFloat destImagePosY = -(srcImage.size.height*scaleX)/2;
//    
//    [self.importImageView.image drawInRect:CGRectMake(destImagePosX , destImagePosY ,
//                                                     srcImage.size.width*scaleX, srcImage.size.height*scaleX)];
//    
//    UIImage *resultImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    return resultImage;
//}

-(UIImage*)drawCropedImageWithTransformNew
{
    
    //기본 이미지에 맞는 이미지를 생성한다.
    //기본이미지
    UIImage *baseImage;
    CGSize baseSize;
    if ([UIScreen isFourInchiScreen]) {
        baseImage = [UIImage imageNamed:@"import_bg"];
        baseSize = CGSizeMake(640, 1136);
    }
    else{
        baseImage = [UIImage imageNamed:@"import_bg_960"];
        baseSize = CGSizeMake(640, 960);
    }
    
    
    UIImage *srcImage = self.importImageView.image;
    CGSize orgSize = self.importImageView.image.size;
    CGPoint orgMoveOffset;
    // *2를 하는 이유는 포인트 좌표를 -> 픽셀좌표로 변경,  나누기 0.85는 현제 화면 에 보여지는 값이 0.75 축소된값이기 때문에 다시 재보정 해준다.
    // 센터값이므로 화면의 반값을 빼줘야 offset값이 나온다.
    orgMoveOffset.x =   self.importImageView.center.x*2/0.85 - baseSize.width/2;
    orgMoveOffset.y =   self.importImageView.center.y*2/0.85 - baseSize.height/2;
    
    NSLog(@"Center X = %.1f Y = %.1f ", self.importImageView.center.x , self.importImageView.center.y);
    //모든 비율을 기본 사이즈에 맞춘다.
    CGFloat scaleX =  baseSize.width / orgSize.width;
    CGFloat scaleY = baseSize.height / orgSize.height;
    //    CGFloat scaleBaseXY = baseSize.width/baseSize.height;
    //    CGFloat scaleOrgXY = orgSize.width/orgSize.height;
    
    
    CGFloat changeScale = scaleX;
    
    //둘다 큰경우 한쪽의 비율로 맞추면 된다. 그런데 한개는 크고 한개는 작은경우 넘어간 쪽에 맞춰야 한다.
    if ((scaleY<1)&&(scaleX>=1)) {
        changeScale = scaleY;
    }
    
    // 둘다 작을 경우 큰쪽에다 맞춘다.
    if ((scaleX>1)&&(scaleY>1)) {
        if (scaleY>scaleX) {
            changeScale  = scaleX;
        }
        else{
            changeScale = scaleY;
        }
    }
    
    //모든 Scale은 X에 맞춘다 //AspectFit으로 설정해 놓는다.
    CGFloat orgScreenWidth = orgSize.width * changeScale;
    CGFloat orgScreenHeight = orgSize.height * changeScale;
    
    

    
    CGFloat offsetHeight0Point = (baseSize.height - orgScreenHeight)/2;
    CGFloat offsetWidth0Point = (baseSize.width - orgScreenWidth)/2;
    
    
    // 어떤 사이즈에 그림을 그릴지
    // 일단 백그라운드 이미지 사이즈에 맞춘다.
    UIGraphicsBeginImageContextWithOptions(baseSize, YES, 1);
    
    //배경을 그린다.
    [baseImage drawInRect:CGRectMake(0, 0, baseSize.width, baseSize.height)];
    
    NSLog(@"image_bg size  width = %.1f height = %.1f ", baseSize.width , baseSize.height);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGAffineTransform tm = self.importImageView.transform;
    
    // *2를 하는 이유는 포인트 좌표를 -> 픽셀좌표로 변경,  나누기 0.85는 현제 화면 에 보여지는 값이 0.75 축소된값이기 때문에 다시 재보정 해준다.
    //    CGFloat movePosX = orgMoveOffset.x * 2 * scaleX  / 0.85 ;
    //    CGFloat movePosY = orgMoveOffset.y * 2 * scaleY  / 0.85 ;
    
    //그릴 위치의 원정음 화면중앙으로 변경한다.
    CGFloat translateX =  (srcImage.size.width*changeScale )/2 + orgMoveOffset.x + offsetWidth0Point;
    CGFloat translateY =  (srcImage.size.height*changeScale)/2 + orgMoveOffset.y + offsetHeight0Point;
    
    
    CGContextTranslateCTM(context, translateX, translateY );
    CGContextConcatCTM(context, tm);
    
    // 그림은 화면중앙에서 그림 사이즈만큼 마이너스 한곳에서 그린다. (각도 때문에 )
    CGFloat destImagePosX = -(srcImage.size.width*changeScale)/2;
    CGFloat destImagePosY = -(srcImage.size.height*changeScale)/2;
    
    [self.importImageView.image drawInRect:CGRectMake(destImagePosX , destImagePosY ,
                                                      srcImage.size.width*changeScale, srcImage.size.height*changeScale)];
    
    UIImage *resultImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return resultImage;
}



////비율대로 잘라야 한다.
//-(UIImage*)cropedImage
//{
//    
//    //import_bg_960.png
//    //import_bg
//    
//    CGFloat scale = self.importScrollView.zoomScale;
//    
//    CGPoint co = self.importScrollView.contentOffset;
//    CGSize cs = self.importScrollView.contentSize;
//    CGRect rect  = self.importBGImgView.bounds;
//    
//    CGFloat rsx = self.photo.size.width / (rect.size.width * 2);
//    CGFloat rsy = self.photo.size.height / (rect.size.height * 2);
//    
//    CGRect cropRect = CGRectMake(co.x / scale * 2 * rsx,
//                                 co.y / scale * 2 * rsy,
//                                 rect.size.width / scale * 2 * rsx,
//                                 rect.size.height / scale * 2 * rsx);
//    
//    
//    CGImageRef cropImageRef = CGImageCreateWithImageInRect(self.importImageView.image.CGImage, cropRect);
//    
//    
//    
//    UIImage* cropped = [UIImage imageWithCGImage:cropImageRef];
//    
//    CGImageRelease(cropImageRef);
//    return cropped;
//}
//
//static inline double radians2 (double degrees) {return degrees * M_PI/180;}
//
//-(UIImage*)imageEdited
//{
//    UIImage *srcImage = self.importImageView.image;
//    CGSize imgSize = srcImage.size;
//    UIGraphicsBeginImageContextWithOptions(imgSize, NO, srcImage.scale);
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    //    CGContextTranslateCTM(context, -imgSize.width/2, -imgSize.height/2);
//    CGContextRotateCTM(context, radians2(45));
//    [srcImage drawInRect:CGRectMake(-imgSize.width/2, -imgSize.height/2, imgSize.width, imgSize.height)];
//    UIImage *resultImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    return resultImage;
//}
//
//-(UIImage*)imageEdited_n2
//{
//    
//    //    CGFloat radians = atan2f(self.photoImageView.transform.b, self.photoImageView.transform.a);
//    //    CGFloat degrees = radians * (180 / M_PI);
//    
//    CGRect f = self.importImageView.frame;
//    UIImage *srcImage = self.importImageView.image;
//    
//    //    CGAffineTransform transform = CGAffineTransformRotate(self.photoImageView.transform, radians2(degrees));
//    //    self.photoImageView.transform = transform;
//    
//    
//    //    CGSize imgSize = srcImage.size;
//    CGSize imgSize = CGSizeMake(640, 1136);
//    CGSize orgSize = self.importImageView.image.size;
//    
//    //    CGFloat scaleX = orgSize.width / imgSize.width;
//    //    CGFloat scaleY = orgSize.height / imgSize.height;
//    //
//    
//    UIGraphicsBeginImageContextWithOptions(imgSize, YES, srcImage.scale);
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    
//    CGContextSetRGBFillColor(context, 0, 0,0, 1);
//    CGContextFillRect(context, CGRectMake(0, 0, 640, 1136));
//    
//    CGFloat scaleXY = orgSize.height / orgSize.width;
//    CGFloat imgHeight = imgSize.width * scaleXY;
//    
//    
//    //    CGContextTranslateCTM(context, imgSize.width/2 - 100 , imgSize.height/2 - 100);
//    //    CGContextRotateCTM(context, radians2(degrees));
//    
//    CGFloat screenScaleX = 640.0/self.importScrollView.bounds.size.width;
//    CGFloat screenScaleY = 1136.0/self.importScrollView.bounds.size.height;
//    CGAffineTransform tm = self.importImageView.transform;
//    //    tm = CGAffineTransformTranslate(tm, f.origin.x*screenScaleX, f.origin.y*screenScaleY*scaleXY);
//    //
//    CGFloat offX =  f.origin.x*screenScaleX*2.0 - 320;
//    CGFloat offY =  f.origin.y*screenScaleY*2.0 - 578;
//    //    CGFloat offX =  0;
//    //    CGFloat offY =  0;
//    
//    
//    CGContextTranslateCTM(context, imgSize.width/2 - offX , imgSize.height/2 - offY);
//    
//    //    tm = CGAffineTransformTranslate(tm, offX, offY);
//    
//    CGContextConcatCTM(context, tm);
//    
//    imgSize.height = imgHeight;
//    
//    
//    [self.importImageView.image drawInRect:CGRectMake(-imgSize.width/2 , -imgSize.height/2 , imgSize.width, imgSize.height)];
//    UIImage *resultImage = UIGraphicsGetImageFromCurrentImageContext();
//    
//    
//    UIGraphicsEndImageContext();
//    return resultImage;
//}
//
//-(UIImage*)imageEdited_n2_0403
//{
//    //기본 이미지에 맞는 이미지를 생성한다.
//    //기본이미지
//    UIImage *baseImage = [UIImage imageNamed:@"import_bg"];
//    CGSize baseSize = CGSizeMake(640, 1136);
//    
//    UIImage *srcImage = self.importImageView.image;
//    CGSize orgSize = self.importImageView.image.size;
//    CGPoint orgMoveOffset;
//    // *2를 하는 이유는 포인트 좌표를 -> 픽셀좌표로 변경,  나누기 0.85는 현제 화면 에 보여지는 값이 0.75 축소된값이기 때문에 다시 재보정 해준다.
//    // 센터값이므로 화면의 반값을 빼줘야 offset값이 나온다.
//    orgMoveOffset.x =   self.importImageView.center.x*2/0.85 - baseSize.width/2;
//    orgMoveOffset.y =   self.importImageView.center.y*2/0.85 - baseSize.height/2;
//    
//    NSLog(@"Center X = %.1f Y = %.1f ", self.importImageView.center.x , self.importImageView.center.y);
//    //모든 비율을 기본 사이즈에 맞춘다.
//    CGFloat scaleX =  baseSize.width / orgSize.width;
//    CGFloat scaleY = baseSize.height / orgSize.height;
//    
//    // 어떤 사이즈에 그림을 그릴지
//    // 일단 백그라운드 이미지 사이즈에 맞춘다.
//    UIGraphicsBeginImageContextWithOptions(baseSize, YES, 1);
//    
//    //배경을 그린다.
//    [baseImage drawInRect:CGRectMake(0, 0, baseSize.width, baseSize.height)];
//    
//    NSLog(@"image_bg size  width = %.1f height = %.1f ", baseSize.width , baseSize.height);
//    
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    CGAffineTransform tm = self.importImageView.transform;
//    
//    //*2를 하는 이유는 포인트 좌표를 -> 픽셀좌표로 변경,  나누기 0.85는 현제 화면 에 보여지는 값이 0.75 축소된값이기 때문에 다시 재보정 해준다.
//    //CGFloat movePosX = orgMoveOffset.x * 2 * scaleX  / 0.85 ;
//    //CGFloat movePosY = orgMoveOffset.y * 2 * scaleY  / 0.85 ;
//    
//    //그릴 위치의 원정음 화면중앙으로 변경한다.
//    CGFloat translateX =  (srcImage.size.width*scaleX )/2 + orgMoveOffset.x;
//    CGFloat translateY =  (srcImage.size.height*scaleY)/2 + orgMoveOffset.y;
//    
//    
//    CGContextTranslateCTM(context, translateX, translateY );
//    CGContextConcatCTM(context, tm);
//    
//    // 그림은 화면중앙에서 그림 사이즈만큼 마이너스 한곳에서 그린다. (각도 때문에 )
//    CGFloat destImagePosX = -(srcImage.size.width*scaleX)/2;
//    CGFloat destImagePosY = -(srcImage.size.height*scaleY)/2;
//    
//    [self.importImageView.image drawInRect:CGRectMake(destImagePosX , destImagePosY ,
//                                                     srcImage.size.width*scaleX, srcImage.size.height*scaleY)];
//    
//    UIImage *resultImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    return resultImage;
//}
//
//
//-(UIImage*)cropedImage2
//{
//    CGFloat radians = atan2f(self.importImageView.transform.b, self.importImageView.transform.a);
//    CGFloat degrees = radians * (180 / M_PI);
//    
//    CGRect rect = CGRectMake(0, 0, 640, 1136);
//    
//    UIGraphicsBeginImageContextWithOptions(rect.size, YES, 1.0f);
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    
//    CGContextSetRGBFillColor(context, 1, 0, 0, 0.5);
//    CGContextFillRect(context, rect);
//    
//    //    UIImage *srcImage = [self.photoImageView.image imageRotatedByDegrees:120];
//    
//    CGRect imgFrame = self.importImageView.frame;
//    CGPoint leftTop = imgFrame.origin;
//    
//    CGSize orgSize = self.importImageView.image.size;
//    
//    CGFloat scale = imgFrame.size.width / orgSize.width;
//    
//    CGRect r = self.importScrollView.frame;
//    r.size.width *= 2;
//    r.size.height *=2;
//    r.origin.x *= 2;
//    r.origin.y *= 2;
//    
//    //    CGContextScaleCTM(context, 1, -1);
//    //    CGContextTranslateCTM(context, -orgSize.width/2, orgSize.height/2);
//    //    CGContextConcatCTM(context, self.photoImageView.transform);
//    //    [self.photoImageView.image drawInRect:rect];
//    //        CGContextRotateCTM(context, radians2(90));
//    
//    r = CGRectMake(100, 100, 600, 600);
//    CGContextSetRGBFillColor(context, 0, 0,1, 0.5);
//    CGContextFillRect(context, r);
//    
//    
//    
//    //     r = CGRectMake(-50, -50, 100, 100);
//    //    CGContextRotateCTM(context, radians2(145));
//    //    CGContextTranslateCTM(context, 300, 300);
//    
//    //    [self.photoImageView.image drawInRect:r];
//    
//    [[self imageEdited] drawInRect:r];
//    
//    //[self.photoImageView.layer drawInContext:context];
//    //    CGContextTranslateCTM(context, 200, 200);
//    
//    
//    UIImage *resultImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    return resultImage;
//}
//
//-(UIImage *)drawImage:(UIImage*)profileImage withBadge:(UIImage *)badge
//{
//    CGRect dstRect = CGRectMake(0, 0, badge.size.width, badge.size.height); // 960 or 1136 ?
//    
//    UIGraphicsBeginImageContextWithOptions(dstRect.size, NO, 2.0f);
//    
//    // draw taken image
//    [profileImage drawInRect:dstRect];
//    
//    // draw photo frame image
//    [badge drawInRect:dstRect];
//    
//    UIImage *resultImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    return resultImage;
//}


#pragma mark - Scroll View Delegate
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return self.importImageView;
}


- (void)scrollViewWillBeginZooming:(UIScrollView *)scrollView withView:(UIView *)view NS_AVAILABLE_IOS(3_2)
{
    
} // called before the scroll view begins zooming its content

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale// scale between minimum and maximum. called after any 'bounce' animations
{
    
}

#pragma mark UI GestureRecognizer Delagte & Action
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    NSLog(@"UIGestureRecognizer ");
    
    return YES;
}

-(void)scaleGesturePinchImage:(UIPinchGestureRecognizer*)recognizer
{
    if (recognizer.state == UIGestureRecognizerStateBegan)
	{
        
        _priviousZoom = 1.0;
        
        NSLog(@"UIPinchGestureRecognizer UIGestureRecognizerStateBegan");
        
    }
    if(recognizer.state == UIGestureRecognizerStateCancelled){
        NSLog(@"UIPinchGestureRecognizer UIGestureRecognizerStateCancelled");
    }
    
    if (recognizer.state == UIGestureRecognizerStateEnded) {
        NSLog(@"UIPinchGestureRecognizer UIGestureRecognizerStateEnded");
    }
    
    [self gestureBoarderOnWithGestureRecognizer:recognizer.state];
    
    float thisScale = 1 + (recognizer.scale - _priviousZoom);
    _priviousZoom = recognizer.scale;
    
    self.importImageView.transform = CGAffineTransformScale(self.importImageView.transform, thisScale, thisScale);
    
}

-(void)gestureBoarderOnWithGestureRecognizer:(UIGestureRecognizerState)state
{
    if (state == UIGestureRecognizerStateBegan) {
        self.importImageView.layer.borderWidth = 1;
        self.importImageView.layer.cornerRadius = 3;
        self.importImageView.layer.masksToBounds = YES;
        self.importImageView.layer.shadowRadius = 5;
        self.importImageView.layer.shadowColor = [UIColor grayColor].CGColor;
        self.importImageView.layer.borderColor = [UIColor grayColor].CGColor;
    }
    
    if(state == UIGestureRecognizerStateEnded)
    {
        self.importImageView.layer.borderWidth = 0;
        self.importImageView.layer.cornerRadius = 0;
        self.importImageView.layer.masksToBounds = NO;
        //        self.photoImageView.layer.borderColor = [UIColor redColor].CGColor;
    }
}

- (void)positionHandlePanImage:(UIPanGestureRecognizer *)recognizer {
    
    if (recognizer.state == UIGestureRecognizerStateBegan){
        _previousPanPoint = [recognizer locationInView:self.importImageView.superview];
        
        
    }
    else if (recognizer.state == UIGestureRecognizerStateEnded) {
        
    }
    
    [self gestureBoarderOnWithGestureRecognizer:recognizer.state];
    
    
    
    NSLog(@"Pan number of touches is %lu", (unsigned long)recognizer.numberOfTouches);
    
    CGPoint curr = [recognizer locationInView:self.importImageView.superview];
    
	float diffx = curr.x - _previousPanPoint.x;
	float diffy = curr.y - _previousPanPoint.y;
    
	CGPoint centre = self.importImageView.center;
	centre.x += diffx;
	centre.y += diffy;
	self.importImageView.center = centre;
    
    _previousPanPoint = curr;
    
}

- (void)rotationGestureImage:(UIRotationGestureRecognizer *)recognizer
{
    
    if (recognizer.state == UIGestureRecognizerStateBegan)
	{
        NSLog(@"UIRotationGestureRecognizer UIGestureRecognizerStateBegan");
        
    }
    if(recognizer.state == UIGestureRecognizerStateCancelled){
        NSLog(@"UIRotationGestureRecognizer UIGestureRecognizerStateCancelled");
    }
    
    if (recognizer.state == UIGestureRecognizerStateEnded) {
        NSLog(@"UIRotationGestureRecognizer UIGestureRecognizerStateEnded");
    }
    
    [self gestureBoarderOnWithGestureRecognizer:recognizer.state];
    
    if([recognizer state] == UIGestureRecognizerStateEnded) {
        
        _previousRotation = 0.0;
        return;
    }
    
    CGFloat newRotation = 0.0 - (_previousRotation - [recognizer rotation]);
    
    CGAffineTransform currentTransformation = self.importImageView.transform;
    CGAffineTransform newTransform = CGAffineTransformRotate(currentTransformation, newRotation);
    
    self.importImageView.transform = newTransform;
    
    _previousRotation = [recognizer rotation];
}

@end
