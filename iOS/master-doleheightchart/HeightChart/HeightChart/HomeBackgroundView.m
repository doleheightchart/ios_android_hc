//
//  HomeBackgroundView.m
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 21..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "HomeBackgroundView.h"
#import "MyAnimationTime.h"

@interface HomeBackgroundView (){
    UIImageView     *_cloud1_1;
    UIImageView     *_cloud1_2;
    UIImageView     *_cloud2_1;
    UIImageView     *_cloud2_2;
    
    CGRect          _frame_Cloud1_1;
    CGRect          _frame_Cloud1_2;
    CGRect          _frame_Cloud2_1;
    CGRect          _frame_Cloud2_2;

    UIImageView     *_deleteStarsView;
}

@end


@implementation HomeBackgroundView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize
{
    [self setBackgroundColorWithDeleteMode:NO];
    [self addClouds];
    
    _deleteStarsView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sky_delete_stars"]];
    _deleteStarsView.frame = CGRectMake(0, 0, self.bounds.size.width, 348/2);
    [self addSubview:_deleteStarsView];
    _deleteStarsView.hidden = YES;
}

- (void)setBackgroundColorWithDeleteMode:(BOOL)deleteMode
{
//    [UIView transitionWithView:self duration:0.5 options:(UIViewAnimationOptionTransitionCrossDissolve) animations:^{
//        if (deleteMode){
//            self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"sky_delete_bg"]];
//        }
//        else{
//            self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"sky_bg"]];
//        }
//    } completion:^(BOOL finished) {
//        
//    }];
    
    if (deleteMode){
        self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"sky_delete_bg"]];
    }
    else{
        self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"sky_bg"]];
    }
}

-(void)addClouds
{
    _frame_Cloud1_1 = CGRectMake(351/2, 82/2, 270/2, 131/2);
    _cloud1_1 = [[UIImageView alloc]initWithFrame:_frame_Cloud1_1];
    _cloud1_1.image = [UIImage imageNamed:@"main_cloud_01"];
    [self addSubview:_cloud1_1];
    
    _frame_Cloud1_2 = CGRectMake(-(270+19)/2, 82/2, 270/2, 131/2);
    _cloud1_2 = [[UIImageView alloc]initWithFrame:_frame_Cloud1_2];
    _cloud1_2.image = [UIImage imageNamed:@"main_cloud_01"];
    [self addSubview:_cloud1_2];
    
    _frame_Cloud2_1 = CGRectMake(19/2, 312/2, 176/2, 95/2);
    _cloud2_1 = [[UIImageView alloc]initWithFrame:_frame_Cloud2_1];
    _cloud2_1.image = [UIImage imageNamed:@"main_cloud_02"];
    [self addSubview:_cloud2_1];
    
    _frame_Cloud2_2 = CGRectMake(-(640-19)/2, 312/2, 176/2, 95/2);
    _cloud2_2 = [[UIImageView alloc]initWithFrame:_frame_Cloud2_2];
    _cloud2_2.image = [UIImage imageNamed:@"main_cloud_02"];
    [self addSubview:_cloud2_2];
}

-(void)startScrolling
{
    _cloud1_1.frame = _frame_Cloud1_1;
    _cloud1_2.frame = _frame_Cloud1_2;
    _cloud2_1.frame = _frame_Cloud2_1;
    _cloud2_2.frame = _frame_Cloud2_2;
    
    
    [UIView animateWithDuration:kDuration_Cloud delay:0 options:UIViewAnimationOptionCurveLinear|UIViewAnimationOptionRepeat animations:^{
        
        _cloud1_1.center = CGPointMake(_cloud1_1.center.x + 320, _cloud1_1.center.y);
        _cloud1_2.center = CGPointMake(_cloud1_2.center.x + 320, _cloud1_2.center.y);
        
        _cloud2_1.center = CGPointMake(_cloud2_1.center.x + 320, _cloud2_1.center.y);
        _cloud2_2.center = CGPointMake(_cloud2_2.center.x + 320, _cloud2_2.center.y);
        
    } completion:^(BOOL finished){
        
//        if (finished){
//            [self switchCloudPositionToOffscreenPosition];
//        }
        
    }];
    
}

-(void)switchCloudPositionToOffscreenPosition
{
    static BOOL needChangePosition = YES;
    
    if (!needChangePosition){
        _cloud1_1.frame = _frame_Cloud1_2;
        _cloud1_2.frame = _frame_Cloud1_1;
        _cloud2_1.frame = _frame_Cloud2_2;
        _cloud2_2.frame = _frame_Cloud2_1;
    }
    else{
        _cloud1_1.frame = _frame_Cloud1_1;
        _cloud1_2.frame = _frame_Cloud1_2;
        _cloud2_1.frame = _frame_Cloud2_1;
        _cloud2_2.frame = _frame_Cloud2_2;
    }
    
    needChangePosition = !needChangePosition;
    
    [self startScrolling];
}

- (void)setIsDeleteMode:(BOOL)isDeleteMode
{
    _isDeleteMode = isDeleteMode;
    
    _cloud1_1.hidden = _isDeleteMode;
    _cloud1_2.hidden = _isDeleteMode;
    _cloud2_1.hidden = _isDeleteMode;
    _cloud2_2.hidden = _isDeleteMode;
    
    [self setBackgroundColorWithDeleteMode:_isDeleteMode];
    _deleteStarsView.hidden = !_isDeleteMode;
}

@end
