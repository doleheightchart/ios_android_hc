//
//  HeightDetailSticker.m
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 6..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "HeightDetailSticker.h"
#import "UIView+ImageUtil.h"
#import "UIFont+DoleHeightChart.h"
#import "UIColor+ColorUtil.h"
#import "NSDate+DoleHeightChart.h"
#import "HeightDetailCell.h"

#define kStickerImageWidth (134/2)
#define kStickerImageHeight (116/2)

#define kStickerDateLabelHeightGap (6/2)
#define kStickerDateLabelHeight (26/2)
#define kStickerDateLabelWidth (117/2)
#define kStickerDateLabelFontSize (22/2)

#define kStickerHeightLabelWidth (117/2)
#define kStickerHeightLabelHeight (30/2)
#define kStickerHeightLabelFontSize (25/2)

#define kStickerExtendGapHeight (6/2)



//@interface NSDate (Sticker)
//-(NSString*)takenDateText;
//@end
//
//@implementation NSDate (Sticker)
//
//-(NSString*)takenDateText
//{
//    NSDateFormatter *formmatter = [[NSDateFormatter alloc] init];
//    formmatter.dateFormat = @"yy.MM.dd";
//    return [formmatter stringFromDate:self];
//}
//
//@end

@interface NSString (Sticker)
+(instancetype)stringWithKideHeight:(CGFloat)kidHeight;
@end
@implementation NSString (Sticker)

+(instancetype)stringWithKideHeight:(CGFloat)kidHeight
{
//    int h = kidHeight * 10.0;
//    if ( (h%10) > 0 )
//        return [NSString stringWithFormat:@"%.1fcm", kidHeight, nil];
//    else
//        return [NSString stringWithFormat:@"%dcm", (int)kidHeight, nil];
//    

    CGFloat fd = fmodf(kidHeight, 1);

    if ( fd > 0 )
        return [NSString stringWithFormat:@"%.1fcm", kidHeight, nil];
    else
        return [NSString stringWithFormat:@"%dcm", (int)kidHeight, nil];
}

@end

@interface HeightDetailSticker (){
//    UIImageView     *_transparentImageView;
}
@property (nonatomic, strong) UIImageView *transparentImageView;
@end

@implementation HeightDetailSticker

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(id)initWithFrame:(CGRect)frame
             photo:(UIImage*)image
         takenDate:(NSDate*)date
  stickerDirection:(StickerDirection)direction
{
    self = [super initWithFrame:frame];
    if (self){
        self.photo = image;
        self.takenDate = date;
        self.stickerdirection = direction;
    }
    
    return self;
}

-(CGRect)framePhoto
{
    return CGRectMake(0, 0, kStickerImageWidth, kStickerImageHeight);
}

-(CGRect)frameShadow
{
    return CGRectMake(0, (6/2), kStickerImageWidth, kStickerImageHeight);
}

-(CGRect)frameDateLabel
{
    CGFloat posX = self.stickerdirection == kStickerDirectionLeft ? -((5.0/2.0) + kStickerDateLabelWidth) : kStickerImageWidth + (5.0/2.0);
    return CGRectMake(posX, (28+30+3)/2.0, kStickerDateLabelWidth, kStickerDateLabelHeight);
}

-(CGRect)frameHeightLabel
{
    CGFloat posX = self.stickerdirection == kStickerDirectionLeft ? -((5.0/2.0) + kStickerDateLabelWidth) : kStickerImageWidth + (5.0/2.0);
    return CGRectMake(posX, (28)/2.0, kStickerHeightLabelWidth, kStickerHeightLabelHeight);
}

-(CGRect)frameSelectedSticker
{
    return CGRectMake(0, 0, kStickerImageWidth, kStickerImageHeight);
}

+(CGRect)boundsDetailSticker
{
    return CGRectMake(0, 0, kStickerImageWidth, kStickerImageHeight + kStickerDateLabelHeightGap + kStickerDateLabelHeight);
}

//-(NSString*)takenDateText
//{
//    NSDateFormatter *formmatter = [[NSDateFormatter alloc] init];
//    formmatter.dateFormat = @"yy.MM.dd";
//    return [formmatter stringFromDate:self.takenDate];
//}

-(UIImage*)checkImage
{
    NSString *checkImageNameFormat = self.stickerdirection == kStickerDirectionLeft ? @"left_arrow_check_monkey_%02d" : @"right_arrow_check_monkey_%02d";
    NSString *checkImageName = [NSString stringWithFormat:checkImageNameFormat, [self.stickerDelegate monkeyNumber], nil];
    return [UIImage imageNamed:checkImageName];
}

-(void)setStickerChecekd:(BOOL)isChecked
{
    [_checkImageView removeFromSuperview];
    if (isChecked){
//        NSString *checkImageName = self.stickerdirection == kStickerDirectionLeft ?
//        @"left_arrow_check_monkey_01" : @"right_arrow_check_monkey_01";
        _checkImageView = [[UIImageView alloc]initWithFrame:[self frameSelectedSticker]];
//        _checkImageView.image = [UIImage imageNamed:checkImageName];
        _checkImageView.image = [self checkImage];
        [self insertSubview:_checkImageView aboveSubview:_topPhotoImageView];
    }
    else{
        _checkImageView = nil;
    }
}

//-(void)tapSticker:(UITapGestureRecognizer*)recognizer
//{
//    if (recognizer.state == UIGestureRecognizerStateEnded) {
//        NSLog(@"UIGestureRecognizerStateEnded");
//        //Do Whatever You want on End of Gesture
//        if ([self.stickerDelegate isDeleteModeRequest]){
//            self.detailInfo.isChecked = !self.detailInfo.isChecked;
//            [self setStickerChecekd:self.detailInfo.isChecked];
//        }
//        else{
//            [self.stickerDelegate clickSticker:self.detailInfo];
//        }
//        
//    }
//    else if (recognizer.state == UIGestureRecognizerStateBegan){
//        NSLog(@"UIGestureRecognizerStateBegan.");
//        //Do Whatever You want on Began of Gesture
//        
//    }
//}


-(UIImage*)transparentImage
{
    
    NSString *shadowImageName = self.stickerdirection == kStickerDirectionLeft ? @"left_arrow_transparent": @"right_arrow_transparent";
    return [UIImage imageNamed:shadowImageName];
}

-(void)setTransprentView:(BOOL)isShow
{
    [self.transparentImageView removeFromSuperview];
    if (isShow && self.detailInfo){
        self.transparentImageView = [[UIImageView alloc]initWithFrame:[self framePhoto]];
        UIImage *image = [self transparentImage];
        self.transparentImageView.image = image;
        [self insertSubview:self.transparentImageView belowSubview:_topPhotoImageView];
    }
    else{
        self.transparentImageView = nil;
    }
}

-(void)setIsDeleteMode:(BOOL)isDeleteMode
{
    _isDeleteMode = isDeleteMode;
    
    [self setTransprentView:_isDeleteMode];
}


@end

#pragma mark TopArrowSticker

#define kDuration_ExpandAnimation 0.3

@interface TopArrowSticker (){
    UIImageView     *_shadowImageView;

    UILabel         *_topTakenDateLabel;
    UILabel         *_heightLabel;
    UIImageView     *_frontShadowImageView;
}

@end

@implementation TopArrowSticker

-(id)initWithFrame:(CGRect)frame
  stickerDirection:(StickerDirection)direction
            detail:(KidHeightDetail*)kidHeightDetail
{
    self = [super initWithFrame:frame photo:nil takenDate:nil stickerDirection:direction];
    if (self){
        self.heightDetail = kidHeightDetail;
        [self initSticker];
    }
    return self;
}

-(UIImage*)shadowImage
{
    NSString *shadowImageNameFormat = self.stickerdirection == kStickerDirectionLeft ? @"detail_left_monkey_%02d": @"detail_right_monkey_%02d";
    NSString *shadowImageName = [NSString stringWithFormat:shadowImageNameFormat, [self.stickerDelegate monkeyNumber], nil];
    return [UIImage imageNamed:shadowImageName];
}





-(void)setShadowView:(BOOL)isShow
{
    [_shadowImageView removeFromSuperview];
    if (!isShow && self.heightDetail.takenHeightHistory.count > 1){
        //NSString *shadowImageName = self.stickerdirection == kStickerDirectionLeft ? @"detail_left_monkey_01": @"detail_right_monkey_01";
        _shadowImageView = [[UIImageView alloc]initWithFrame:[self frameShadow]];
        //_shadowImageView.image = [UIImage imageNamed:shadowImageName];
        _shadowImageView.image = [self shadowImage];
        [self insertSubview:_shadowImageView belowSubview:_topPhotoImageView];
    }
    else{
        _shadowImageView = nil;
    }
}




-(CGRect)frameExtendStickerWithIndex:(NSUInteger)index
{
    //CGRect topPhotoFrame = [self framePhoto];

//    CGFloat stickerGapHeight = kStickerImageHeight + kStickerDateLabelHeightGap + kStickerDateLabelHeight + kStickerExtendGapHeight;
    CGFloat stickerGapHeight = kStickerImageHeight + kStickerDateLabelHeightGap;

    //CGFloat left = topPhotoFrame.origin.x;
    CGFloat left = self.frame.origin.x;
    CGFloat top = stickerGapHeight * (index+1);
//    CGFloat top = stickerGapHeight * (index);
    
    return CGRectMake(left, top, kStickerImageWidth, stickerGapHeight);
}

-(void)removeAllExtendStickers:(BOOL)animated
{
    void(^animation)() = ^{
        for (ExtendSticker* extendSticker in self.extendStickers) {
            //extendSticker.transform = CGAffineTransformMakeScale(0.1, 0.1);
            CGPoint toCenter = self.center;
            toCenter.y += 3;
            extendSticker.center = toCenter;
            
        }
        
        [self setShadowView:NO];
    };
    
    void(^completion)(BOOL) = ^(BOOL finished){
        for (ExtendSticker* extendSticker in self.extendStickers) {
            [extendSticker removeFromSuperview];
        }
        
        [self.extendStickers removeAllObjects];
        
            
    };
    
    if (animated)
        [UIView animateWithDuration:kDuration_ExpandAnimation animations:animation completion:completion];
    else
        completion(YES);
    
//    return;
//    
//    if (animated)
//    {
//        [UIView animateWithDuration:0.2 animations:^{
//            for (ExtendSticker* extendSticker in self.extendStickers) {
//                extendSticker.transform = CGAffineTransformMakeScale(0.1, 0.1);
//            }
//        } completion:^(BOOL finished){
//            
//    //         NSLog(@"RemoveAll Extend Sticker count is %lu // Arraycount is %d", (unsigned long)self.heightDetail.takenHeightHistory.count, self.extendStickers.count);
//            
//            for (ExtendSticker* extendSticker in self.extendStickers) {
//                [extendSticker removeFromSuperview];
//            }
//            
//            [self.extendStickers removeAllObjects];
//        }];
//    }
//    else{
//        for (ExtendSticker* extendSticker in self.extendStickers) {
//            [extendSticker removeFromSuperview];
//        }
//        
//        [self.extendStickers removeAllObjects];
//    }
}

-(void)showAllExtendStickers:(BOOL)animated
{
    if (animated)
    {
        [UIView animateWithDuration:kDuration_ExpandAnimation animations:^{
            for (int i = 1; i < self.heightDetail.takenHeightHistory.count; i++) {
                HeightDetailInfo *detailInfo = self.heightDetail.takenHeightHistory[i];
                CGRect frameExtendSticker = [self frameExtendStickerWithIndex:(i-1)];
                CGRect startFrame = self.frame;
                startFrame.origin.y += 3;
                
                ExtendSticker *extendSticker = [[ExtendSticker alloc]initWithFrame:startFrame
                                                                      heightDetail:detailInfo
                                                                  stickerDirection:self.stickerdirection
                                                                StickerDelegate:self.stickerDelegate
                                                ];
                extendSticker.topArrowSticker = self;
                [self.extendStickers addObject:extendSticker];
//                extendSticker.transform = CGAffineTransformMakeScale(0.1, 0.1);
                [self.superview insertSubview:extendSticker belowSubview:self];
//                extendSticker.stickerDelegate = self.stickerDelegate;
//                extendSticker.transform = CGAffineTransformMakeScale(1, 1);
                extendSticker.frame = frameExtendSticker;
            }
            
            [self setShadowView:YES];

            
            //NSLog(@"Show Extend Sticker count is %d // Arraycount is %d", self.heightDetail.takenHeightHistory.count, self.extendStickers.count);
            
        } completion:^(BOOL finished){
            
            //NSLog(@"Complete Show AllExtension Stickers!!!");
                    }];
    }
    else{
        for (int i = 1; i < self.heightDetail.takenHeightHistory.count; i++) {
            HeightDetailInfo *detailInfo = self.heightDetail.takenHeightHistory[i];
            CGRect frameExtendSticker = [self frameExtendStickerWithIndex:(i-1)];
            
            ExtendSticker *extendSticker = [[ExtendSticker alloc]initWithFrame:frameExtendSticker
                                                                  heightDetail:detailInfo
                                                              stickerDirection:self.stickerdirection
                                                StickerDelegate:self.stickerDelegate
                                            ];
            extendSticker.topArrowSticker = self;
            [self.extendStickers addObject:extendSticker];
            [self.superview insertSubview:extendSticker belowSubview:self];
            extendSticker.stickerDelegate = self.stickerDelegate;
        }
    }
    

}

-(void)updateExpandWithAnimated:(BOOL)animated
{
    BOOL expanded = _heightDetail.expanded;
    
    if (expanded){
        //add extend sticker list
        NSLog(@"Show All ExtendStickers");
        [self showAllExtendStickers:animated];
    }
    else if (self.extendStickers.count > 0){ // 애니메이션 블럭을 남발하면 완료핸들러가 마구 불려져서 꼬인다 조심하자
        //remove extend sticker list
        NSLog(@"Remove All ExtendStickers");
        [self removeAllExtendStickers:animated];
    }
    
    if (!animated)
        [self setShadowView:expanded];
}

-(void)initSticker
{
    self.backgroundColor = [UIColor clearColor];
    self.extendStickers = [NSMutableArray array];
    
    _topPhotoImageView = [[UIImageView alloc]initWithFrame:[self framePhoto]];
    [self addSubview:_topPhotoImageView];
    
    _frontShadowImageView = [[UIImageView alloc]initWithFrame:_topPhotoImageView.frame];
    [_topPhotoImageView addSubview:_frontShadowImageView];
    if (self.stickerdirection == kStickerDirectionLeft){
        _frontShadowImageView.image = [UIImage imageNamed:@"detail_left_arrow_shadow"];
    }
    else{
        _frontShadowImageView.image = [UIImage imageNamed:@"detail_right_arrow_shadow"];
    }
    
    _topTakenDateLabel = [[UILabel alloc]initWithFrame:[self frameDateLabel]];
    _topTakenDateLabel.backgroundColor = [UIColor clearColor];
    _topTakenDateLabel.font = [UIFont defaultBoldFontWithSize:kStickerDateLabelFontSize];
    _topTakenDateLabel.textAlignment = self.stickerdirection == kStickerDirectionLeft ? NSTextAlignmentRight : NSTextAlignmentLeft;
    _topTakenDateLabel.textColor = [UIColor colorWithMonkeyNumber:[self.stickerDelegate monkeyNumber]];
    [self addSubview:_topTakenDateLabel];

    _heightLabel = [[UILabel alloc]initWithFrame:[self frameHeightLabel]];
    _heightLabel.backgroundColor = [UIColor clearColor];
    _heightLabel.font = [UIFont defaultBoldFontWithSize:kStickerHeightLabelFontSize];
    _heightLabel.textAlignment = self.stickerdirection == kStickerDirectionLeft ? NSTextAlignmentRight : NSTextAlignmentLeft;
    _heightLabel.textColor = [UIColor colorWithMonkeyNumber:[self.stickerDelegate monkeyNumber]];
    [self addSubview:_heightLabel];


    UILongPressGestureRecognizer *longpressRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self
                                                                                                      action:@selector(longPressed:)];
//    longpressRecognizer.minimumPressDuration = 3;
    [self addGestureRecognizer:longpressRecognizer];
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                    action:@selector(tapTopArrowSticker:)];
    [self addGestureRecognizer:tapRecognizer];
//    tapRecognizer.delegate = (id <UIGestureRecognizerDelegate>) self;
}

-(void)longPressed:(UILongPressGestureRecognizer*)recognizer
{
    if (recognizer.state == UIGestureRecognizerStateEnded) {
        NSLog(@"UIGestureRecognizerStateEnded");
        //Do Whatever You want on End of Gesture
        
    }
    else if (recognizer.state == UIGestureRecognizerStateBegan){
        NSLog(@"UIGestureRecognizerStateBegan.");
        //Do Whatever You want on Began of Gesture
        //[self.cellDelegate longPressedWithHeight:self.kidHeight];
//        [self.cellDelegate longPressedWithCell:self Height:self.kidHeight];
        if (self.detailInfo)
            [self.stickerDelegate longpressedSticker:self];
    }
}

-(void)setHeightDetail:(KidHeightDetail *)heightDetail
{
    _heightDetail = heightDetail;
    
    [self updateExpandWithAnimated:NO];
    
    HeightDetailInfo *firstDetail = _heightDetail.takenHeightHistory.firstObject;
    
    UIImage* maskedPhotoImage = [UIImage stickerImageFromPhoto:firstDetail.image
                                                     Direction:self.stickerdirection == kStickerDirectionLeft];
    
//    UIImage* maskedPhotoImage = firstDetail.image;

    _topPhotoImageView.image = maskedPhotoImage;
    
    if (_heightDetail){
        if (self.stickerdirection == kStickerDirectionLeft){
            _frontShadowImageView.image = [UIImage imageNamed:@"detail_left_arrow_shadow"];
        }
        else{
            _frontShadowImageView.image = [UIImage imageNamed:@"detail_right_arrow_shadow"];
        }
    }
    else{
        _frontShadowImageView.image = nil;
    }
    
    self.takenDate = firstDetail.takenDate;
    _topTakenDateLabel.text = [self.takenDate stringForHeightViewer]; //[firstDetail.takenDate description];
    _topTakenDateLabel.textColor = [UIColor colorWithMonkeyNumber:[self.stickerDelegate monkeyNumber]];

    _heightLabel.textColor = [UIColor colorWithMonkeyNumber:[self.stickerDelegate monkeyNumber]];
    if (firstDetail)
        //_heightLabel.text = [NSString stringWithFormat:@"%.1fcm", firstDetail.kidHeight];
        _heightLabel.text = [NSString stringWithKideHeight:firstDetail.kidHeight];
    else
        _heightLabel.text = nil;
    
    [self setStickerChecekd:firstDetail.isChecked];
    
    self.detailInfo = firstDetail;
}

-(void)tapTopArrowSticker:(UITapGestureRecognizer*)recognizer
{
    if (self.detailInfo == nil) return;
    
    if (recognizer.state == UIGestureRecognizerStateEnded) {
        
        if ([self.stickerDelegate isDeleteModeRequest]){
            self.detailInfo.isChecked = !self.detailInfo.isChecked;
            [self setStickerChecekd:self.detailInfo.isChecked];
        }
        
        [self.stickerDelegate clickSticker:self HeightDetail:self.heightDetail clickedStickerInfo:self.detailInfo];
        
    }
    else if (recognizer.state == UIGestureRecognizerStateBegan){
    }
}

//-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
//    // Determine if the touch is inside the custom subview
//    NSLog(@"gestureRecognizer!!!! Delegate");
//    if ([touch view] == self){
//        // If it is, prevent all of the delegate's gesture recognizers
//        // from receiving the touch
//        return YES;
//    }
//    return YES;
//    
//    
//}

-(BOOL)doAddAnimationWithDetailInfo:(HeightDetailInfo*)detailInfo
{
    UIView *target;
    
    if (self.detailInfo == detailInfo){
        target = _topPhotoImageView;
    }
    else {
        for (ExtendSticker *st in _extendStickers) {
            if (st.detailInfo == detailInfo){
                target = st;
                break;
            }
        }
    }
    
    if (target == nil) return NO;
    
    target.transform = CGAffineTransformMakeScale(0.1, 0.1);
    [UIView animateWithDuration:2 animations:^{
        target.transform = CGAffineTransformMakeScale(1, 1);
    }];
    
    return YES;
}

-(void)doDeleteAnimationWithDetailInfoArray:(NSArray*)detailInfoArray
{
    NSMutableArray *deleteTargets = [NSMutableArray array];
    
    for (HeightDetailInfo *di in detailInfoArray) {
        if (self.detailInfo == di){
            [deleteTargets addObject:_topPhotoImageView];
            if (_checkImageView) [deleteTargets addObject:_checkImageView];
            if (_shadowImageView) [deleteTargets addObject:_shadowImageView];
            if (_heightLabel) [deleteTargets addObject:_heightLabel];
            if (_topTakenDateLabel) [deleteTargets addObject:_topTakenDateLabel];
            if (self.transparentImageView) [deleteTargets addObject:self.transparentImageView];
        }
        else {
            for (ExtendSticker *st in _extendStickers) {
                if (st.detailInfo == di){
                    [deleteTargets addObject:st];
                }
            }
        }
    }

    if (deleteTargets.count == 0) return;
    
    [UIView animateWithDuration:2 animations:^{
        for (UIView *deleteTarget in deleteTargets) {
            deleteTarget.transform = CGAffineTransformMakeScale(0, 0);
        }
    } completion:^(BOOL finished) {
        
    }];
}

-(void)setIsDeleteMode:(BOOL)isDeleteMode
{
    [super setIsDeleteMode:isDeleteMode];
    
    for (ExtendSticker *exsticker in self.extendStickers) {
        [exsticker setIsDeleteMode:isDeleteMode];
    }
}

-(void)updateCheckedStatus
{
    
}


@end

#pragma mark ExtendSticker

@interface ExtendSticker ()

@end

@implementation ExtendSticker

-(id)initWithFrame:(CGRect)frame
      heightDetail:(HeightDetailInfo*)detail
  stickerDirection:(StickerDirection)direction
   StickerDelegate:(id<HeightDetailStickerDelegate>)stickerDelegate
{
//    self = [self initWithFrame:frame photo:detail.image takenDate:detail.takenDate stickerDirection:direction isChecked:detail.isChecked];
//    if (self){
//        self.detailInfo = detail;
//    }
//    return self;
    
    self = [super initWithFrame:frame photo:detail.image takenDate:detail.takenDate stickerDirection:direction];
    if (self){
        self.stickerDelegate = stickerDelegate;
        self.detailInfo = detail;
        [self initStickerWithChecked:detail.isChecked];
    }
    return self;
}

-(id)initWithFrame:(CGRect)frame
             photo:(UIImage*)image
         takenDate:(NSDate*)date
  stickerDirection:(StickerDirection)direction
         isChecked:(BOOL)checked
{
    self = [super initWithFrame:frame photo:image takenDate:date stickerDirection:direction];
    if (self){
        [self initStickerWithChecked:checked];
    }
    return self;
}

-(UIImage*)shadowImage
{
    NSString *imageName = self.stickerdirection == kStickerDirectionLeft ? @"detail_left_over_shadow" : @"detail_right_over_shadow";
    
    return [UIImage imageNamed:imageName];
}

-(void)initStickerWithChecked:(BOOL)checked
{
//    NSLog(@"InitExtendSticker!!! == %@", NSStringFromCGRect(self.frame));
    
    self.backgroundColor = [UIColor clearColor];
//    self.backgroundColor = [UIColor redColor];

    // Masked Photo Image
    UIImage *maskedPhoto = [UIImage stickerExtendImageFromPhoto:self.photo
                                                      Direction:self.stickerdirection == kStickerDirectionLeft];
    
    _topPhotoImageView = [[UIImageView alloc]initWithFrame:self.framePhoto];
    _topPhotoImageView.image = maskedPhoto;
    [self addSubview:_topPhotoImageView];
    
    // Shadow Image
    UIImageView *shadowImageView = [[UIImageView alloc]initWithFrame:self.framePhoto];
    shadowImageView.image = [self shadowImage];
    [self addSubview:shadowImageView];
    
    // Taken Date Label
    UILabel *dateLabel = [[UILabel alloc]initWithFrame:self.frameDateLabel];
    dateLabel.backgroundColor = [UIColor clearColor];
    dateLabel.font = [UIFont defaultBoldFontWithSize:kStickerDateLabelFontSize];
    dateLabel.textAlignment = self.stickerdirection == kStickerDirectionLeft ? NSTextAlignmentRight : NSTextAlignmentLeft;;
    dateLabel.textColor = [UIColor colorWithMonkeyNumber:[self.stickerDelegate monkeyNumber]];
    dateLabel.text = [self.takenDate stringForHeightViewer]; // [self.takenDate description];
    [self addSubview:dateLabel];

    UILabel *heightLabel = [[UILabel alloc]initWithFrame:[self frameHeightLabel]];
    heightLabel.backgroundColor = [UIColor clearColor];
    heightLabel.font = [UIFont defaultBoldFontWithSize:kStickerHeightLabelFontSize];
    heightLabel.textAlignment = self.stickerdirection == kStickerDirectionLeft ? NSTextAlignmentRight : NSTextAlignmentLeft;
    heightLabel.textColor = [UIColor colorWithMonkeyNumber:[self.stickerDelegate monkeyNumber]];
//    heightLabel.text = [NSString stringWithFormat:@"%.1fcm", self.detailInfo.kidHeight, nil];
    heightLabel.text = [NSString stringWithKideHeight:self.detailInfo.kidHeight];

    [self addSubview:heightLabel];
    
    [self setStickerChecekd:checked];
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                    action:@selector(tapExtendSticker:)];
    
    [self addGestureRecognizer:tapRecognizer];
}

-(void)tapExtendSticker:(UITapGestureRecognizer*)recognizer
{
    if (self.detailInfo == nil) return;
    
    if (recognizer.state == UIGestureRecognizerStateEnded) {
        
        if ([self.stickerDelegate isDeleteModeRequest]){
            self.detailInfo.isChecked = !self.detailInfo.isChecked;
            [self setStickerChecekd:self.detailInfo.isChecked];
        }
        
        [self.stickerDelegate clickSticker:self HeightDetail:self.topArrowSticker.heightDetail clickedStickerInfo:self.detailInfo];
        
    }
    else if (recognizer.state == UIGestureRecognizerStateBegan){
    }
}


@end;

#pragma mark ZoomSticker

#define ZoomStickerHeight (24/2)

@interface ZoomSticker ()


@end

@implementation ZoomSticker

-(id)initWithFrame:(CGRect)frame
         takenDate:(NSDate*)date
         kidHeight:(CGFloat)height
  stickerDirection:(StickerDirection)direction
          delegate:(id<HeightDetailStickerDelegate>) stickerDelegate
{
    self = [super initWithFrame:frame];
    if (self){
        self.stickerdirection = direction;
        self.stickerDelegate = stickerDelegate;
        [self initStickerWithHeight:height takenDate:date];
    }
    return self;
}

-(CGRect)frameDateLabel
{
    return CGRectMake(self.stickerdirection == kStickerDirectionLeft ? 0 : (6+8+4+90+4)/2.0 ,
                      0,
                      (96/2),
                      (24/2));
}

-(CGRect)frameHeightLabel
{
    return CGRectMake(self.stickerdirection == kStickerDirectionLeft ? (96+4)/2.0 : (6+8+4)/2,
                      0,
                      (90/2),
                      (24/2));
}

-(CGRect)frameHeightPoint
{
    return CGRectMake(self.stickerdirection == kStickerDirectionLeft ? (96+4+90+4)/2.0 : (6/2),
                      ( 24/2.0 - 8/2.0 )/2.0 + 0.5,
                      (8/2),
                      (8/2));
}

-(UIImage*)indicatorImage
{
    NSString *imageName = [NSString stringWithFormat:@"detail_all_indicator_monkey_%02d", [self.stickerDelegate monkeyNumber], nil];
    return [UIImage imageNamed:imageName];
}

-(void)initStickerWithHeight:(CGFloat)kidHeight
                   takenDate:(NSDate*)date
{
    self.backgroundColor = [UIColor clearColor];
    
    UIImageView *heightPointImageView = [[UIImageView alloc]initWithFrame:[self frameHeightPoint]];
//    heightPointImageView.image = [UIImage imageNamed:@"detail_all_indicator_monkey_01"];
    heightPointImageView.image = [self indicatorImage];
    [self addSubview:heightPointImageView];
    
    UILabel *heightLabel = [[UILabel alloc]initWithFrame:[self frameHeightLabel]];
    heightLabel.backgroundColor = [UIColor clearColor];
    heightLabel.font = [UIFont defaultBoldFontWithSize:(22/2)];
    heightLabel.textAlignment = self.stickerdirection == kStickerDirectionLeft ? NSTextAlignmentRight : NSTextAlignmentLeft;
    heightLabel.textColor = [UIColor colorWithMonkeyNumber:[self.stickerDelegate monkeyNumber]];
//    heightLabel.text = [NSString stringWithFormat:@"%.1fcm", kidHeight, nil];
    heightLabel.text = [NSString stringWithKideHeight:kidHeight];
    
    [heightLabel sizeToFit];
    
    [self adjustPositionHeightLabel:heightLabel];

    [self addSubview:heightLabel];
    
    UILabel *dateLabel = [[UILabel alloc]initWithFrame:[self frameDateLabel]];
    dateLabel.backgroundColor = [UIColor clearColor];
    dateLabel.font = [UIFont defaultBoldFontWithSize:(18/2)];
    dateLabel.textAlignment = self.stickerdirection == kStickerDirectionLeft ? NSTextAlignmentRight : NSTextAlignmentLeft;
    dateLabel.textColor = [UIColor colorWithMonkeyNumber:[self.stickerDelegate monkeyNumber]];
    dateLabel.text = [date stringForHeightViewer];
    
    dateLabel.frame = CGRectMake(self.stickerdirection == kStickerDirectionLeft ?  heightLabel.frame.origin.x - (4+96)/2.0: heightLabel.frame.origin.x + heightLabel.frame.size.width + (4.0/2.0),
                                 dateLabel.frame.origin.y,
                                 dateLabel.frame.size.width,
                                 dateLabel.frame.size.height);
    
    [self addSubview:dateLabel];
    
    
    
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                    action:@selector(tapZoomSticker:)];
    [self addGestureRecognizer:tapRecognizer];
}

-(void)adjustPositionHeightLabel:(UILabel*)heightLabel
{
    if (self.stickerdirection == kStickerDirectionLeft){
        CGFloat x = self.bounds.size.width - (6+8+4)/2.0 - heightLabel.bounds.size.width;
        CGFloat y = self.bounds.size.height / 2.0 - (heightLabel.bounds.size.height/2.0);
        heightLabel.frame = CGRectMake(x, y, heightLabel.frame.size.width, heightLabel.frame.size.height);
    }
    else{
        CGFloat x = (6+8+4)/2.0;
        CGFloat y = self.bounds.size.height / 2.0 - (heightLabel.bounds.size.height/2.0);
        heightLabel.frame = CGRectMake(x, y, heightLabel.frame.size.width, heightLabel.frame.size.height);
    }
}

-(void)tapZoomSticker:(UITapGestureRecognizer*)recognizer
{
    if (self.detailInfo == nil) return;
    
    if (recognizer.state == UIGestureRecognizerStateEnded) {
        
//        if ([self.stickerDelegate isDeleteModeRequest]){
//            self.detailInfo.isChecked = !self.detailInfo.isChecked;
//            [self setStickerChecekd:self.detailInfo.isChecked];
//        }
        
//        HeightZoomDetailCell *parentCell = (HeightZoomDetailCell*)self.superview;
        
        [self.stickerDelegate clickSticker:self HeightDetail:nil clickedStickerInfo:self.detailInfo];
        
    }
    else if (recognizer.state == UIGestureRecognizerStateBegan){
    }
}

-(BOOL)doAddAnimationWithDetailInfo:(HeightDetailInfo*)detailInfo
{
    if (self.detailInfo != detailInfo) return NO;
    
    self.transform = CGAffineTransformMakeScale(0.1, 0.1);
    [UIView animateWithDuration:2 animations:^{
        self.transform = CGAffineTransformMakeScale(1, 1);
    }];
    
    return YES;
}


@end
