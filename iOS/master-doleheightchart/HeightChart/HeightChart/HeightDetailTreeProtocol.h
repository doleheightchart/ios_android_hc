//
//  HeightDetailTreeProtocol.h
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 6..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KidHeightDetail.h"
#import "MyChild.h"

typedef enum eStickerDirection {
    kStickerDirectionLeft,
    kStickerDirectionRight,
}StickerDirection;



@protocol HeightDetailTreeDelegate <NSObject>

-(void)longpressedWithHeight:(CGFloat)kidHeight;
-(void)addHeight;
//-(void)clickTopArrowSticker:(KidHeightDetail*)kidHeightDetail;
-(void)clickSticker:(KidHeightDetail*)kidHeightDetail stickerDetail:(HeightDetailInfo*)detailInfo;
-(void)clickMonkey;
-(void)didPinchZoom:(BOOL)isZoomMode;
-(void)beginScroll;
-(NSInteger)requestAge;

@end

@protocol HeightDetailTreeProtocol <NSObject>

@end

@protocol HeightDetailDataSource<NSObject>
-(NSUInteger)countOfHeightDetail;
-(HeightDetailInfo*)heightDetailAtIndex:(NSUInteger)index;
@optional
-(MyChild*)myChildOfDetails;

@end

@class HeightViewerController;

@protocol HeightViewerDelgate <NSObject>

-(void)deleteHeightViewerWithIndex:(NSInteger)index HeightViewer:(HeightViewerController*)controller;

@end

