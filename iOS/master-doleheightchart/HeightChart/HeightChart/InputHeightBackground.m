//
//  InputHeightBackground.m
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 11..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "InputHeightBackground.h"
#import "UIView+Animation.h"
#import "MyAnimationTime.h"


@interface InputHeightBackground (){
    UIImageView     *_cloud1_1;
    UIImageView     *_cloud1_2;
    UIImageView     *_cloud2_1;
    UIImageView     *_cloud2_2;
    
    CGRect          _frameOffScreen_Cloud1;
    CGRect          _frameOffScreen_Cloud2;
    
    UIImageView     *_ballon1;
    UIImageView     *_ballon2;

}

@end

@implementation InputHeightBackground

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)awakeFromNib
{
    //[self startScrolling];
    [super awakeFromNib];
    
    [self initBackground];
}

-(void)initBackground
{
    [self addSkyBackgroundImage];
    
    [self addClouds];
    
    //[self startScrolling];
    
    [self loadAnimals];
}

-(void)addSkyBackgroundImage
{
    self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"sky_bg"]];
}

-(void)addClouds
{
    _cloud1_1 = [[UIImageView alloc]initWithFrame:CGRectMake(209, 73, 102, 49)];
    _cloud1_1.image = [UIImage imageNamed:@"input_height_cloud1"];
    [self addSubview:_cloud1_1];
    
    _frameOffScreen_Cloud1 = CGRectMake(-111, 73, 102, 49);
    _cloud1_2 = [[UIImageView alloc]initWithFrame:_frameOffScreen_Cloud1];
    _cloud1_2.image = [UIImage imageNamed:@"input_height_cloud1"];
    [self addSubview:_cloud1_2];
    
    _cloud2_1 = [[UIImageView alloc]initWithFrame:CGRectMake(39, self.bounds.size.height-143, 116, 63)];
    _cloud2_1.image = [UIImage imageNamed:@"input_height_cloud2"];
    [self addSubview:_cloud2_1];
    
    _frameOffScreen_Cloud2 = CGRectMake(-281, self.bounds.size.height-143, 116, 63);
    _cloud2_2 = [[UIImageView alloc]initWithFrame:_frameOffScreen_Cloud2];
    _cloud2_2.image = [UIImage imageNamed:@"input_height_cloud2"];
    [self addSubview:_cloud2_2];
}

-(void)startScrolling
{
    [UIView animateWithDuration:kDuration_Cloud delay:0 options:UIViewAnimationOptionCurveLinear|UIViewAnimationOptionRepeat animations:^{
        
        _cloud1_1.center = CGPointMake(_cloud1_1.center.x + 320, _cloud1_1.center.y);
        _cloud1_2.center = CGPointMake(_cloud1_2.center.x + 320, _cloud1_2.center.y);
        
        _cloud2_1.center = CGPointMake(_cloud2_1.center.x + 320, _cloud2_1.center.y);
        _cloud2_2.center = CGPointMake(_cloud2_2.center.x + 320, _cloud2_2.center.y);
        
    } completion:^(BOOL finished){
        
//        if (finished){
//            [self switchCloudPositionToOffscreenPosition];
//        }
        
    }];

}

-(void)switchCloudPositionToOffscreenPosition
{
    if (_cloud1_1.center.x > 320) _cloud1_1.frame = _frameOffScreen_Cloud1;
    if (_cloud1_2.center.x > 320) _cloud1_2.frame = _frameOffScreen_Cloud1;
    
    if (_cloud2_1.center.x > 320) _cloud2_1.frame = _frameOffScreen_Cloud2;
    if (_cloud2_2.center.x > 320) _cloud2_2.frame = _frameOffScreen_Cloud2;
    
    [self startScrolling];
}

-(void)loadAnimals
{
    if (_ballon1) return;
    
    CGRect frameBallon1 = CGRectMake(-50, 600, 138/2, 170/2);
    CGRect frameBallon2 = CGRectMake(-50, 600, 138/2, 170/2);
    
    _ballon1 = [[UIImageView alloc]initWithFrame:frameBallon1];
    _ballon1.image = [UIImage imageNamed:@"input_height_ballon_01"];
    //[_ballon1 addFrameImagesWithNameFormat:@"01_fox_detail_%02d" frameCount:10 duration:3 repeat:0];
    
    [self addSubview:_ballon1];
    
    _ballon2 = [[UIImageView alloc]initWithFrame:frameBallon2];
    //[_ballon2 addFrameImagesWithNameFormat:@"01_raccoon_detail_%02d" frameCount:10 duration:3 repeat:0];
    _ballon2.image = [UIImage imageNamed:@"input_height_ballon_02"];
    [self addSubview:_ballon2];
    
//    [self startMovingAnimation];
}

-(void)startMovingAnimation
{
    [self moveAnimateBallon1];
    [self moveAnimateBallon2];

}

-(void)moveAnimateBallon1
{
    NSValue *pos4 = [NSValue valueWithCGPoint:CGPointMake(-50, 600)];
    NSValue *pos1 = [NSValue valueWithCGPoint:CGPointMake(400, 300)];
    NSValue *pos2 = [NSValue valueWithCGPoint:CGPointMake(-100, -50)];
    NSValue *pos3 = [NSValue valueWithCGPoint:CGPointMake(-50, 600)];
    
    NSArray *positions = @[pos4, pos1, pos2, pos3];
    
//    __weak InputHeightBackground *thisView = self;
//    [_ballon1 addMoveAnimationWithPosition:positions duration:20 completion:^(BOOL finished) {
//        [thisView moveAnimateBallon2];
//    }];
    
    [_ballon1 addMoveAnimationWithPosition:positions Delay:kDuration_Ballon Repeat:HUGE_VALF FillMode:kCAFillModeForwards RepeatDuration:0 duration:kDuration_Ballon completion:nil];

}

-(void)moveAnimateBallon2
{
    NSValue *pos4 = [NSValue valueWithCGPoint:CGPointMake(-50, 600)];
    NSValue *pos1 = [NSValue valueWithCGPoint:CGPointMake(400, 300)];
    NSValue *pos2 = [NSValue valueWithCGPoint:CGPointMake(-100, -50)];
    NSValue *pos3 = [NSValue valueWithCGPoint:CGPointMake(-50, 600)];
    
    NSArray *positions = @[pos4, pos1, pos2, pos3];
    
//    __weak InputHeightBackground *thisView = self;
//    [_ballon2 addMoveAnimationWithPosition:positions duration:20 completion:^(BOOL finished) {
//        [thisView moveAnimateBallon1];
//    }];
    
    [_ballon2 addMoveAnimationWithPosition:positions Delay:kDuration_Ballon_Delay Repeat:HUGE_VALF FillMode:kCAFillModeForwards RepeatDuration:0 duration:kDuration_Ballon completion:nil];

}


@end
