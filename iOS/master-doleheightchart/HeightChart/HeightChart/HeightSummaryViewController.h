//
//  HeightSummaryViewController.h
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 9..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HeightDetailTreeProtocol.h"
#import "MyChild.h"
#import "FacebookManager.h"

@interface HeightSummaryViewController : UIViewController<FacebookManagerDelegate>
@property (nonatomic, weak) id<HeightDetailDataSource> heightDetailDataSource;
@property (nonatomic) NSUInteger shownIndex;
@property (nonatomic, weak) MyChild *myChild;
@end
