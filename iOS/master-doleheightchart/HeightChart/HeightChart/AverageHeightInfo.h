//
//  AverageHeightInfo.h
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 4. 6..
//  Copyright (c) 2014년 Kim Sehyun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AverageHeightInfo : NSObject

+(CGFloat)averageHeightByBirthday:(NSDate*)birthday IsGril:(BOOL)isGirl;
+(CGFloat)averageHeightByAge:(NSInteger)age IsGril:(BOOL)isGirl;

@end
