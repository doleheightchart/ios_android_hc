//
//  DoleInfo.h
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 21..
//  Copyright (c) 2014년 Kim Sehyun. All rights reserved.
//

#ifndef HeightChart_DoleInfo_h
#define HeightChart_DoleInfo_h


enum DoleMonkeyType
{
    MonkeyTreeType_OWL = 0,
    MonkeyTreeType_MonkeyA = 1,
    MonkeyTreeType_MonKeyB = 2,
    MonkeyTreeType_MonkeyC = 3,
    MonkeyTreeType_MonkeyD = 4,
    MonkeyTreeType_MonkeyE = 5,
};

enum DoleStickerType
{
    DoleStikerRacoon,
    DoleStikerToucan,
    DoleStikerLion,
    DoleStikerAlligator,
    DoleStikerFox,
    DoleStikerUnknown = -1,
};

enum DoleCharacterType
{
    kDoleCharacterTypeDolekey = MonkeyTreeType_MonkeyA,
    kDoleCharacterTypeRilla = MonkeyTreeType_MonKeyB,
    kDoleCharacterTypeTeeny = MonkeyTreeType_MonkeyC,
    kDoleCharacterTypeCoco = MonkeyTreeType_MonkeyD,
    kDoleCharacterTypePanzee = MonkeyTreeType_MonkeyE,
};

//1 - 남자, 2 - 여자 서버와 통일한다.
enum GenderType
{
    kGenderTypeMale = 1,
    kGenderTypeFemale = 2,
    kGenderTypeBoy = kGenderTypeMale,
    kGenderTypeGirl = kGenderTypeFemale,
};

@interface DoleInfo : NSObject

+(NSString*)doleCharaterNameWithType:(enum DoleCharacterType)charaterType;
+(enum DoleStickerType)convertStickerTypeWithString:(NSString*)stickerValue;
+(enum DoleStickerType)convertStickerTypeWithStringQR:(NSString*)stickerValue;

@end


#endif
