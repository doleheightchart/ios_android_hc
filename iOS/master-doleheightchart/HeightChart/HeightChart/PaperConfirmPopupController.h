//
//  PaperConfirmPopupController.h
//  HeightChart
//
//  Created by ne on 2014. 4. 7..
//  Copyright (c) 2014년 Kim Sehyun. All rights reserved.
//

#import "PopupController.h"
typedef void (^OKCancelPaperHandler)(BOOL);

@interface PaperConfirmPopupController : PopupController

@property (nonatomic, copy) OKCancelPaperHandler completionBlock;

-(void)popupAddressText:(NSString*)adress;

@end
