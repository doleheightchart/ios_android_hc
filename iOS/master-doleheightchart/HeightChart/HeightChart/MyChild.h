//
//  MyChild.h
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 13..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class ChildHeight;

@interface MyChild : NSManagedObject

@property (nonatomic, retain) NSDate * birthday;
@property (nonatomic, retain) NSNumber * charterNo;
@property (nonatomic, retain) NSDate * createdDate;
@property (nonatomic, retain) NSNumber * isGirl;
@property (nonatomic, retain) NSString * nickname;
@property (nonatomic, retain) NSSet *heightHistory;

@property (nonatomic) CGFloat lastTakenHeight;
@property (nonatomic) CGFloat maxTakenHeight;

@end

@interface MyChild (CoreDataGeneratedAccessors)

- (void)addHeightHistoryObject:(ChildHeight *)value;
- (void)removeHeightHistoryObject:(ChildHeight *)value;
- (void)addHeightHistory:(NSSet *)values;
- (void)removeHeightHistory:(NSSet *)values;

@end

@interface MyChild (DoleHeightChart)

-(NSArray*)childHeightByHeight;
-(void)loadLastTakenHeight;
-(void)refreshMaxTakenHeight;

-(NSUInteger)currentAge;

@end
