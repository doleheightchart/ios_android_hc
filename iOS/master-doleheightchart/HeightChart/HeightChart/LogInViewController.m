//
//  LogInViewController.m
//  HeightChart
//
//  Created by ne on 2014. 2. 11..
//  Copyright (c) 2014년 ne. All rights reserved.
//

#import "LogInViewController.h"
#import "UIView+ImageUtil.h"
#import "TextViewDualPlaceHolder.h"
#import "DualPlaceHolderTextField.h"
#import "BirthdayPopupController.h"
#import "UIColor+ColorUtil.h"
#import "UIViewController+DoleHeightChart.h"
#import "UIButton+CheckAndRadio.h"
#import "UIViewController+PolyServer.h"
#import "LogInBeforeViewController.h"
#import "WarningCoverView.h"
#import "ToastController.h"
#import "PaperNewzealandViewController.h"
#import "GuideView.h"
#import "InputChecker.h"
#import "EditFacebookViewController.h"
#import "TermsViewController.h"
#import "MPFoldTransition/DoleTransition.h"
#import "SignInViewController.h"
#import "MessageBoxController.h"

#import "AppDelegate.h"

#import "Mixpanel.h"

@interface LogInViewController (){
    UIView          *_newzelandpaperBanner;
    
    UIButton        *_skipButton;
    UILabel         *_skiptLabel;
    UIButton        *_closeButton;
    
    InputChecker    *_inputChecker;
    
    NSMutableData   *_receivedData;
    BOOL            _requestedFacebookLogIn;
}
@property (weak, nonatomic) IBOutlet UIButton *btnLogIn;
@property (weak, nonatomic) IBOutlet UIButton *btnForgotPassword;
@property (weak, nonatomic) IBOutlet UIButton *btnSignIn;
@property (weak, nonatomic) IBOutlet UIButton *btnViaFaceBook;
@property (weak, nonatomic) IBOutlet TextViewDualPlaceHolder *txtViewID;
@property (weak, nonatomic) IBOutlet TextViewDualPlaceHolder *txtPassword;
@property (weak, nonatomic) IBOutlet UIImageView *bgSky;
- (IBAction)clickLogin:(id)sender;

@end

@implementation LogInViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    [self.btnLogIn setResizableImageWithType:DoleButtonImageTypeOrange];
    [self.btnLogIn setupSizeWhenPressed];
    [self.txtViewID setDualPlaceHolderTextView:DoleDualPlaceHolderTextTypeEmail];
    [self.txtPassword setDualPlaceHolderTextView:DoleDualPlaceHolderTextTypePassword];
        self.txtPassword.textField.secondPlaceHodler = @"";
    
    [self.txtPassword.textField addTarget:self.txtPassword.textField
                       action:@selector(resignFirstResponder)
             forControlEvents:UIControlEventEditingDidEndOnExit];
    
    [self.txtViewID.textField addTarget:self.txtViewID.textField
                                 action:@selector(resignFirstResponder)
                              forControlEvents:UIControlEventEditingDidEndOnExit];
    
    if (self.userConfig.isFirstTimeAppLaunch == NO)
        _closeButton = [self addCommonCloseButton];
    
    self.txtPassword.textField.secureTextEntry = YES;
    
    //11a6b4
    //9453e4
    UIColor *normalColor = [UIColor colorWithRGB:0x11a6b4];
    UIColor *selectColor= [UIColor colorWithRGB:0x9453e4];
    [self.btnViaFaceBook makeUnderlineStyleWithSelectedColor:selectColor normalColor:normalColor];
    [self.btnSignIn makeUnderlineStyleWithSelectedColor:selectColor normalColor:normalColor];
    [self.btnForgotPassword makeUnderlineStyleWithSelectedColor:selectColor normalColor:normalColor];
    
//    [self.txtViewID initwithBackground];
    
    if(self.userConfig.isFirstTimeAppLaunch)
    {
        _skipButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_skipButton setImage:[UIImage imageNamed:@"login_skip_btn"] forState:UIControlStateNormal];
        [_skipButton setImage:[UIImage imageNamed:@"login_skip_press_btn"] forState:UIControlStateHighlighted];
        [_skipButton addTarget:self action:@selector(clickSkipBtn:) forControlEvents:UIControlEventTouchUpInside];
        
        _skiptLabel = [[UILabel alloc]init];
        _skiptLabel.textColor = [UIColor colorWithRGB:0x9453e4];
        _skiptLabel.font = [UIFont defaultBoldFontWithSize:37/2];
        _skiptLabel.textAlignment = NSTextAlignmentRight;
        _skiptLabel.text = NSLocalizedString(@"Skip", @"");
        
        [self.view addSubview:_skipButton];
        [self.view addSubview:_skiptLabel];
    }
    
    [self setupControlFlexiblePosition];
    
    [self.btnViaFaceBook addTarget:self action:@selector(clickLoginFacebook:) forControlEvents:UIControlEventTouchUpInside];
    

    [self makeNZPaper];
    
    _inputChecker = [[InputChecker alloc]init];
    [_inputChecker addHostViewRecursive:self.view submitButton:self.btnLogIn checkBoxButtons:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    if (NO == self.isEnableNetwork){
        self.btnLogIn.enabled = NO;
        self.btnViaFaceBook.enabled = NO;
        
        [self toastNetworkError];
    }
}

-(void)makeNZPaper
{
    
    _newzelandpaperBanner = [[UIView alloc]initWithFrame:CGRectMake((self.view.bounds.size.width - 500/2)/2,
                                                                    //                                                                    self.view.bounds.size.height - (72 + 70)/2,
                                                                    self.view.bounds.size.height + 100,
                                                                    500/2, 72/2)];
    
    _newzelandpaperBanner.backgroundColor = [UIColor colorWithRGB:0x606060];
    _newzelandpaperBanner.layer.opacity = 0.85;
    _newzelandpaperBanner.layer.cornerRadius = 2;
    _newzelandpaperBanner.layer.masksToBounds = YES;
    
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(22/2, 18/2, 400/2, 34/2)];
    label.backgroundColor = [UIColor clearColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont defaultRegularFontWithSize:28/2];
    label.textColor = [UIColor colorWithRGB:0xffffff];
    label.text = @"Receive the Height Chart Paper";
    [_newzelandpaperBanner addSubview:label];
    
    UIButton *closebtn = [ UIButton buttonWithType:UIButtonTypeCustom];
    [closebtn setImage:[UIImage imageNamed:@"popup_banner_x_btn_normal"] forState:UIControlStateNormal];
    [closebtn setImage:[UIImage imageNamed:@"popup_banner_x_btn_press"] forState:UIControlStateHighlighted];
    [closebtn addTarget:self action:@selector(closeBanner) forControlEvents:UIControlEventTouchUpInside];
    closebtn.frame = CGRectMake(_newzelandpaperBanner.frame.size.width - (45 + 14)/2, 14/2, 45/2, 45/2);
    [_newzelandpaperBanner addSubview:closebtn];
    
    [self.view addSubview:_newzelandpaperBanner] ;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(showPaperView)];
    [_newzelandpaperBanner addGestureRecognizer:tap];
}

-(void)showPaperView
{
    PaperNewzealandViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"paperNewzealand"];
    
    UINavigationController *navi = [[UINavigationController alloc]initWithRootViewController:vc];
    navi.navigationBarHidden = YES;
    
    [self modalTransitionController:navi Animated:YES];
    [_newzelandpaperBanner removeFromSuperview];
    
}

-(void)closeBanner
{
    [UIView animateWithDuration:1.0 animations:^{
        
        CGRect rect = _newzelandpaperBanner.frame;
        rect.origin.y = self.view.bounds.size.height + 150;
        
        _newzelandpaperBanner.frame = rect;
        
    } completion:^(BOOL finished){
        self.userConfig.isHeightChartPaparRecievedShowBanner = YES;
        [_newzelandpaperBanner removeFromSuperview];
        
    }
     ];
}

-(void)enableControlByNetworkStatus
{
    if (!self.isEnableNetwork){
        
        self.btnLogIn.enabled = NO;
        self.btnForgotPassword.enabled = NO;
        self.btnSignIn.enabled = NO;
        self.btnViaFaceBook.enabled = NO;
        self.txtViewID.textField.enabled = NO;
        self.txtPassword.textField.enabled = NO;
        
        
        [self toastNetworkError];
    }

}

-(void)viewDidAppear:(BOOL)animated
{
    GuideView *gv = [GuideView addGuideTarget:self.view Type:kGuideTypeFirstTimeLogIn checkFirstTime:YES];
    
    if (gv){
        gv.completion = ^{
            [self enableControlByNetworkStatus];

        };
    }
    else{
    }
}


-(void)setupControlFlexiblePosition
{
    if ([UIScreen isFourInchiScreen]) {
        
        [self addTitleByBigSizeImage:YES Visible:YES];
        [self moveToPositionYPixel:(110 + 168 + 84) Control:self.txtViewID];
        [self moveToPositionYPixel:(110 + 168 + 84 + 72 + 22) Control:self.txtPassword];
        [self moveToPositionYPixel:(110 + 168 + 84 + 72 + 22 + 72 + 78) Control:self.btnLogIn];
        [self moveToPositionYPixel:(748 + 96) Control:self.btnForgotPassword];
        [self moveToPositionYPixel:(748 + 96 + 32 + 44) Control:self.btnSignIn];
        [self moveToPositionYPixel:(748 + 96 + 32 + 44 + 32 + 44) Control:self.btnViaFaceBook];
        
        [self changeToHeightPixel:748 Control:self.bgSky];
    }
    else{
        
        CGFloat movePositon =  748 - 582 + 96 - 86 ;

        [self addTitleByBigSizeImage:NO Visible:YES];
        [self moveToPositionYPixel:(242) Control:self.txtViewID];
        [self moveToPositionYPixel:(242 + 72 + 22) Control:self.txtPassword];
        [self moveToPositionYPixel:(242 + 72 + 22 + 72 + 34) Control:self.btnLogIn];
        
        [self moveByPositionY: -(movePositon)/2 Controls:@[self.btnForgotPassword, self.btnSignIn, self.btnViaFaceBook]];
        
        [self changeToHeightPixel:582 Control:self.bgSky];
    }
    
    if(self.userConfig.isFirstTimeAppLaunch)
    {
        CGRect frameB = _skipButton.frame;
        frameB.origin.x = self.view.bounds.size.width - (41 + 41 )/2;
        frameB.origin.y = self.view.bounds.size.height - (41 + 20)/2;
        frameB.size.height  = 41/2;
        frameB.size.width = 41/2;
        _skipButton.frame = frameB;
        
        
        CGRect frame = _skiptLabel.frame;
        frame.origin.x = self.view.bounds.size.width - 80 - (41 + 41 + 16)/2;
        frame.origin.y = self.view.bounds.size.height - (41 + 20 + 2)/2;
        frame.size.height  = 41/2;
        frame.size.width = 80;
        _skiptLabel.frame = frame;
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clickSkipBtn:(id)sender {

    [self closeViewControllerAnimated:NO];
    
}

- (BOOL)isValidInput
{
    if ([self.txtViewID checkIsValidEmail] == NO) return NO;
    if ([self.txtPassword checkIsValidPassword] == NO) return NO;
    
    if (self.txtViewID.textField.text.length <= 0){
        
        [WarningCoverView addWarningToTargetView:self.txtViewID warningtype:kInputFieldErrorTypeWrongEmailFormat];
        
        return NO;
    }
    
    if (self.txtPassword.textField.text.length <= 0){
        
        [WarningCoverView addWarningToTargetView:self.txtPassword warningtype:kInputFieldErrorTypeWrongPassword];
        
        return NO;
    }
    
    return YES;
}

- (IBAction)clickLogin:(id)sender
{
    [[self.view findFirstResponder] resignFirstResponder];
    
    if ([self enableActionWithCurrentNetwork] == NO)return;
    
    if ([self isValidInput]){
        NSString *email = self.txtViewID.textField.text;
        NSString *pwd = self.txtPassword.textField.text;
        
        [self requestLogInWithUserID:email Password:pwd];
        _requestedFacebookLogIn = NO;
    }
}

-(void)clickLoginFacebook:(id)sender
{
    if ([self enableActionWithCurrentNetwork] == NO)return;
    
    [[Mixpanel sharedInstance] timeEvent:@"Signup Success"];
    
    self.facebookManager.delegate = self;
    
    [self.facebookManager logout];
    [self.facebookManager login];
}

#pragma mark FacebookManager Delegate

- (void) loginResult:(BOOL)isSucess withAcessToken:(NSString*)accessToken
{
    if (isSucess){
        
    }
}

- (void) didReceiveUserID:(NSString*)userID Email:(NSString*)email Error:(id)error
{
    if (nil == error){
    
        NSLog(@"Facebook UserId is %@", userID);
        NSLog(@"Facebook Email is %@", email);
        
        
        NSString *facebookID = userID;
        NSString *facebookEmail = email;
        NSString *facebookName = self.facebookManager.facebookName;
        
        
        
//        [self requestSignUpWithFacebookID:facebookID FacebookName:facebookName FacebookEmail:facebookEmail BirthYear:year City:city Gender:gender];
//        _requestedFacebookLogIn = YES;

        
        // 페이스북에 로그인 해보고 유저데이터가 없다면
        // 등록을 시도해 본다.
        [self requestLogInWithSNSID:userID];
        
//        [self showSignUpFacebookUser];

    }
    else{
        
    }
    
    
}

-(void)showSignUpFacebookUser
{
    if (self.facebookManager.emailAddress == nil){ //페이스북의 몇몇 사람들은 이메일이 깨졌다, 실제 이메일은 프로필에서 나오나, 오픈그래프API로는 불러오지 못 한다.
        [self showOKConfirmMessage:NSLocalizedString(@"Facebook Log in failed.\nPlease check on your\nFacebook account.", @"") completion:nil];
        return;
    }
    
    NSCalendar *calendar = [[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *comp = [calendar components:NSCalendarUnitYear fromDate:[NSDate date]];
    NSInteger year = [comp year];
    
    NSString *city = @"";
    
    enum GenderType gender = kGenderTypeMale;
    
    if (self.facebookManager.address){
        //주소파싱
    }
    
    if (self.facebookManager.gender){
        if ([self.facebookManager.gender isEqualToString:@"male"])
            gender = kGenderTypeMale;
        else
            gender = kGenderTypeFemale;
    }
    
    if (self.facebookManager.birthday){
        NSDateFormatter *formater = [[NSDateFormatter alloc] init];
        NSDate *bd = [formater dateFromString:self.facebookManager.birthday];
        
        if (bd){
            NSCalendar *calendar = [[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];
            NSDateComponents *comp = [calendar components:NSCalendarUnitYear fromDate:bd];
            year = [comp year];
        }
    }

    

    
    
//    EditFacebookViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"EditFacebooklUser"];
//    vc.isRegisterMode = YES;
//    vc.facebookID = self.facebookManager.facebookID;
//    vc.facebookName = self.facebookManager.facebookName;
//    vc.facebookEmail = self.facebookManager.emailAddress;
//    vc.birthYear = year;
//    vc.city = city;
//    vc.genderType = gender;
//    
//    [self pushTransitionController:vc Animated:YES];
    
    
    TermsViewController * vc = (TermsViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"termsofservice"];
    vc.requireLoginCompletion = self.requireLoginCompletion;
    vc.appFirstTimeLogIn = self.appFirstTimeLogIn;
    vc.isFacebookSignUpMode = YES;
    [self.navigationController pushViewController:vc animated:NO];
}


#pragma mark Server

- (NSMutableData*)receivedBufferData
{
    if (_receivedData == nil){
        _receivedData = [NSMutableData dataWithCapacity:0];
    }
    
    return _receivedData;
}



- (void)didConnectionFail:(NSError *)error
{
//    [ToastController showToastWithMessage:error.description duration:5];
    [self toastNetworkError];
}

- (void)didCompleteRecevedWithResultType:(DoleServerResultType)resultType ParsedData:(NSDictionary *)recevedData ParseError:(NSError *)error
{
    if (recevedData && error==nil && [recevedData[@"Return"] boolValue] ){
        
        if (resultType == kDoleServerResultTypeLogIn){
        
            NSNumber *userNo = recevedData[@"UserNo"];
            NSString *authKey = recevedData[@"AuthKey"];
            
            if (userNo && authKey){

                // JP Added
                [[Mixpanel sharedInstance] registerSuperProperties:@{@"User Type": @"Registered"}];
                [[Mixpanel sharedInstance] registerSuperProperties:@{@"Email": self.txtViewID.textField.text}];
                //longzhe cui added
                [[Mixpanel sharedInstance] track:@"Direct Login" properties:@{@"Authentication":@"Email"}];
                
                // JP added post 1.4.2
                [[Mixpanel sharedInstance] createAlias:self.txtViewID.textField.text
                                         forDistinctID:[Mixpanel sharedInstance].distinctId];
                [[Mixpanel sharedInstance] identify:[Mixpanel sharedInstance].distinctId];
                
                //[[Mixpanel sharedInstance] identify:self.txtViewID.textField.text];
                [[Mixpanel sharedInstance].people set:@{@"$email":self.txtViewID.textField.text}];
                [[Mixpanel sharedInstance].people set:@{@"Deleted":@"false"}];
                [[Mixpanel sharedInstance].people addPushDeviceToken:((AppDelegate*)([UIApplication sharedApplication].delegate)).userConfig.deviceTokenData];
                
                
                [self.userConfig signUpByEmail:self.txtViewID.textField.text password:self.txtPassword.textField.text];
                
                self.userConfig.authKey = authKey;
                self.userConfig.userNo = [userNo integerValue];
                
                self.userConfig.loginStatus = kUserLogInStatusDoleLogOn;

                //[self popToLonOnViewController];
                [self closeWhenLoggedIn];
                
                 [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"SystemMigrationWarningShown"];
            }
            else{
                [self toastNetworkError];
            }
            
        }
        else if (resultType == kDoleServerResultTypeSnsLogIn){
            
            NSNumber *userNo = recevedData[@"UserNo"];
            NSString *authKey = recevedData[@"AuthKey"];
            
            if (userNo && authKey){
                
                // JP Added
                [[Mixpanel sharedInstance] registerSuperProperties:@{@"User Type": @"Registered"}];
                [[Mixpanel sharedInstance] registerSuperProperties:@{@"Email": self.facebookManager.emailAddress}];
                
                //longzhe cui added
                [[Mixpanel sharedInstance] track:@"Direct Login" properties:@{@"Authentication":@"Facebook"}];
                // JP added post 1.4.2
                [[Mixpanel sharedInstance] createAlias:self.facebookManager.emailAddress
                                         forDistinctID:[Mixpanel sharedInstance].distinctId];
                [[Mixpanel sharedInstance] identify:[Mixpanel sharedInstance].distinctId];
                // [[Mixpanel sharedInstance] identify:email];
                [[Mixpanel sharedInstance].people set:@{@"$email":self.facebookManager.emailAddress}];
                [[Mixpanel sharedInstance].people set:@{@"Deleted":@"false"}];
                [[Mixpanel sharedInstance].people addPushDeviceToken:((AppDelegate*)([UIApplication sharedApplication].delegate)).userConfig.deviceTokenData];
                //longzhe cui added end

                [self.userConfig signUpByFacebookEmail:self.facebookManager.emailAddress FacebookID:self.facebookManager.facebookID];
                
                self.userConfig.authKey = authKey;
                self.userConfig.userNo = [userNo integerValue];
                
                self.userConfig.loginStatus = kUserLogInStatusFacebook;
                
//                [self popToLonOnViewController];
                [self closeWhenLoggedIn];
                
                 [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"SystemMigrationWarningShown"];
            }
            else{
                [self toastNetworkError];
            }
        }
    }
    else{
        
        //페이스북으로 가입정보를 찾을 수 없다.
        //페이스북으로 로그인 하려고 했을 경우만!
        const NSInteger kNotExistUserErrorCode = 90107;
        if (recevedData && error==nil &&
            [recevedData[@"ReturnCode"] integerValue] == kNotExistUserErrorCode &&
            resultType == kDoleServerResultTypeSnsLogIn){
            
            [self showSignUpFacebookUser];
            return;
        }
        
        NSInteger errorCode = [recevedData[@"ReturnCode"] integerValue];
        if ([self queryErrorCode:errorCode EmailView:self.txtViewID PasswordView:self.txtPassword]) return;

        [self toastNetworkErrorWithErrorCode:errorCode];
    }
}

-(void)closeViewControllerAnimated:(BOOL)animated
{
    
    if (self.navigationController){
        if (self.navigationController.viewControllers.count > 1){
            //            [self.navigationController popViewControllerAnimated:animated];
            [self.navigationController popViewControllerWithFoldStyle:0];
        }
        else{
            //            [self.navigationController dismissViewControllerAnimated:animated completion:nil];
            [self.navigationController.presentingViewController dismissViewControllerWithFoldStyle:0 completion:nil];
        }
    }
    else{
        //[self dismissViewControllerAnimated:animated completion:nil];
        //[self.presentedViewController dismissViewControllerWithFoldStyle:0 completion:nil];
        [self.presentingViewController dismissViewControllerWithFoldStyle:0 completion:nil];
    }
}


-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    NSArray *checkSegueArray = @[@"segueForgotPassword", @"segueSignup"];
    if ([checkSegueArray indexOfObject:identifier] != NSNotFound){
        if ([self enableActionWithCurrentNetwork] == NO){
            return NO;
        }
    }
    
    return YES;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"segueSignup"]){
        SignInViewController *signupVC = segue.destinationViewController;
        signupVC.requireLoginCompletion = self.requireLoginCompletion;
        signupVC.appFirstTimeLogIn = self.appFirstTimeLogIn;
    }
}

-(void)closeWhenLoggedIn
{
    if (self.appFirstTimeLogIn == NO &&
        self.requireLoginCompletion == nil){
        [self popToLonOnViewController];
        return;
    }

    [self.navigationController.presentingViewController dismissViewControllerAnimated:NO completion:nil];
    
    if (self.requireLoginCompletion){
        self.requireLoginCompletion(YES);
    }
}

@end
