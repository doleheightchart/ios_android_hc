//
//  FacebookManager.m
//  HeightChart
//
//  Created by ne on 2014. 3. 27..
//  Copyright (c) 2014년 Kim Sehyun. All rights reserved.
//

#import <FacebookSDK/FacebookSDK.h>
#import "FacebookManager.h"
#import "UIViewController+DoleHeightChart.h"

#define kPOSTING_MESSAGE @"Dole Height Chart"

@interface FacebookManager()
{
//    NSString *_facebookID;
//    NSString *_facebookEmail;
    NSMutableArray *_friendInfoList;
    FBFrictionlessRecipientCache *ms_friendCache;

}
@end

@implementation FacebookManager


- (void) FB_CreateNewSession
{
    FBSession* session = [[FBSession alloc]init];
    [FBSession setActiveSession:session];
    
    self.facebookAcessToken = session.accessTokenData.accessToken;
    
    NSLog(@"Cached AccessToken is %@", self.facebookAcessToken);
    
}

- (void) FB_Login
{
    NSArray *permission = [[NSArray alloc] initWithObjects:@"email", nil];
    
    
    [FBSession openActiveSessionWithReadPermissions:permission allowLoginUI:YES
                                  completionHandler:^(FBSession *session, FBSessionState status, NSError *error)
     {
         if (status == FBSessionStateClosedLoginFailed || status == FBSessionStateCreatedOpening)
         {
             [[FBSession activeSession] closeAndClearTokenInformation];
             [FBSession setActiveSession:nil];
             [self FB_CreateNewSession];
         }
         else
         {
             
             self.facebookAcessToken = session.accessTokenData.accessToken;

              // Update our game now we've logged in
             //업로드 관련 화면 업데이트
             //성공인지 실패인지 보낸다.
             [self.delegate loginResult:YES withAcessToken:self.facebookAcessToken];
             
             //성공하면 무조건 아이디와 이메일을 가져온다.
             
             [self FB_UserID];
             
             
         }
     }];
    
}
- (void) FB_Logout
{
    self.facebookID = nil;
    self.emailAddress = nil;
    self.birthday = nil;
    self.gender = nil;
    self.address = nil;
    self.facebookName = nil;
    
    [[FBSession activeSession] closeAndClearTokenInformation];
    [FBSession setActiveSession:nil];
}

- (void) FB_UserID
{
    // Start the facebook request
    [[FBRequest requestForMe]startWithCompletionHandler:
     ^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *result, NSError *error)
     {
         // Did everything come back okay with no errors?
         if (!error && result)
         {
             self.facebookID = [result objectForKey:@"id"];
             self.emailAddress = [result objectForKey:@"email"];
             self.birthday = [result objectForKey:@"birthday"];
             self.gender = [result objectForKey:@"gender"];
             self.address = [result objectForKey:@"address"];
             self.facebookName = [result objectForKey:@"name"];
             [self.delegate didReceiveUserID:self.facebookID Email:self.emailAddress Error:nil];

         }
         else {
             NSLog(@"FB_UserID GetUser ID Error %@", error.description);
             
             [self.delegate didReceiveUserID:nil Email:nil Error:error];

             
         }
     }];

}

- (void) FB_ProcessIncomingURL
{
    
}

- (void) FB_ProcessIncomingRequest:(NSString*) urlString
{
    
}


-(void) FB_UploadPhotoWithimage:(UIImage *)image
{
    
    
    
    // Check if the Facebook app is installed and we can present the share dialog

    // If the Facebook app is installed and we can present the share dialog
    if ([FBDialogs canPresentShareDialogWithPhotos]) {
    
        
        [FBDialogs presentShareDialogWithPhotos:@[image] handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {
            
            NSLog(@"Result %@", error.description);
            
        }];
        
        return;
    }
    
    
    
    //업로드 후 URL을 받는다.
    //직접 업로드 포토 하는 방범.
    //이것도 그래프 API를 이용한다. 
    [FBRequestConnection startForUploadPhoto:image completionHandler:^(FBRequestConnection *concetion, id result, NSError *error)
    {
        
        
    }];
    ///
    //Graph API 이용
    NSMutableDictionary* params = [[NSMutableDictionary alloc] init];
    [params setObject:@"[Dole Height Chart] 사진을 업로드 합니다." forKey:@"message"];
    [params setObject:UIImagePNGRepresentation(image) forKey:@"picture"];
    
//    [FBRequestConnection startWithGraphPath:@"me/photos"
//                                 parameters:params
//                                 HTTPMethod:@"POST"
//                          completionHandler:^(FBRequestConnection *connection,
//                                              id result,
//                                              NSError *error)
    
     
    [FBRequestConnection startForUploadStagingResourceWithImage:image completionHandler:^(FBRequestConnection *connection, id result, NSError *error)
    {
        
     
         if (error)
         {
             //showing an alert for failure
             NSLog(@"Error = %@", error);
         }
         else
         {
             
             NSLog(@"Successfuly staged image with staged URI: %@", [result objectForKey:@"uri"]);

             //showing an alert for success
             NSLog(@"PhotoShare성공");
             FBGraphObject *fbobject = result;
             id url = fbobject[@"uri"];
             
             [self postStatusUpdateWithShareDialog:url];
         }
     }];

    
    
}


- (IBAction)postStatusUpdateWithShareDialog:(NSString*)link
{
    
    
    
    // Check if the Facebook app is installed and we can present the share dialog
    FBShareDialogParams *params = [[FBShareDialogParams alloc] init];
    //    params.link = [NSURL URLWithString:link];
    params.picture = [NSURL URLWithString:link];

    
    // If the Facebook app is installed and we can present the share dialog
    if ([FBDialogs canPresentShareDialogWithParams:params]) {
        
        
        
        // Present share dialog
        [FBDialogs presentShareDialogWithLink:nil
                                      handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {
                                          if(error) {
                                              // An error occurred, we need to handle the error
                                              // See: https://developers.facebook.com/docs/ios/errors
                                              NSLog(@"Error publishing story: %@", error.description);
                                          } else {
                                              // Success
                                              NSLog(@"result %@", results);
                                          }
                                      }];
        
        // If the Facebook app is NOT installed and we can't present the share dialog
    } else {
        // FALLBACK: publish just a link using the Feed dialog
        // Show the feed dialog
        FBShareDialogParams *shareParams = [[FBShareDialogParams alloc] init];
        shareParams.link = [NSURL URLWithString:link];
        shareParams.name = @"Good Dole!";
        shareParams.caption= @"Dole Caption";
        shareParams.picture= [NSURL URLWithString:link];
        
        [FBWebDialogs presentFeedDialogModallyWithSession:nil
                                               parameters:shareParams
                                                  handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
                                                      if (error) {
                                                          // An error occurred, we need to handle the error
                                                          // See: https://developers.facebook.com/docs/ios/errors
                                                          NSLog(@"Error publishing story: %@", error.description);
                                                      } else {
                                                          if (result == FBWebDialogResultDialogNotCompleted) {
                                                              // User cancelled.
                                                              NSLog(@"User cancelled.");
                                                          } else {
                                                              // Handle the publish feed callback
                                                              NSDictionary *urlParams = [self parseURLParams:[resultURL query]];
                                                              
                                                              if (![urlParams valueForKey:@"post_id"]) {
                                                                  // User cancelled.
                                                                  NSLog(@"User cancelled.");
                                                                  
                                                              } else {
                                                                  // User clicked the Share button
                                                                  NSString *result = [NSString stringWithFormat: @"Posted story, id: %@", [urlParams valueForKey:@"post_id"]];
                                                                  NSLog(@"result %@", result);
                                                              }
                                                          }
                                                      }
                                                  }];
    }
}


- (void) FB_TEST
{

}

- (void) FB_GraphFriendList
{
    _friendInfoList = [[NSMutableArray alloc]init];
    [FBRequestConnection startWithGraphPath:[NSString stringWithFormat:@"%@/friends?fields=installed,name,id", _facebookID]
                                 parameters:nil HTTPMethod:@"GET" completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                                     
                                     if (result && !error)
                                     {
                                         for (NSDictionary *dict in [result objectForKey:@"data"])
                                         {
                                             
                                             FacebookFriendInfo *friendInfo;
                                             friendInfo.name = [dict objectForKey:@"name"];
                                             friendInfo.facebookID = [dict objectForKey:@"id"];
                                             friendInfo.installed = [dict objectForKey:@"installed"];
                                             
                                             [_friendInfoList addObject:friendInfo];
                                         }

                                     }
                                 }];
}



- (void)FB_ShareDialog
{
    // Check if the Facebook app is installed and we can present the share dialog
    
    FBShareDialogParams *params = [[FBShareDialogParams alloc] init];
    params.link = [NSURL URLWithString:@"https://developers.facebook.com/docs/ios/share/"];
    
    // If the Facebook app is installed and we can present the share dialog
    if ([FBDialogs canPresentShareDialogWithParams:params]) {
        
        // Present share dialog
        [FBDialogs presentShareDialogWithLink:nil
                                      handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {
                                          if(error) {
                                              // An error occurred, we need to handle the error
                                              // See: https://developers.facebook.com/docs/ios/errors
                                              NSLog(@"Error publishing story: %@", error.description);
                                          } else {
                                              // Success
                                              NSLog(@"result %@", results);
                                          }
                                      }];
        
        // If the Facebook app is NOT installed and we can't present the share dialog
    } else {
        // FALLBACK: publish just a link using the Feed dialog
        // Show the feed dialog
        [FBWebDialogs presentFeedDialogModallyWithSession:nil
                                               parameters:nil
                                                  handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
                                                      if (error) {
                                                          // An error occurred, we need to handle the error
                                                          // See: https://developers.facebook.com/docs/ios/errors
                                                          NSLog(@"Error publishing story: %@", error.description);
                                                      } else {
                                                          if (result == FBWebDialogResultDialogNotCompleted) {
                                                              // User cancelled.
                                                              NSLog(@"User cancelled.");
                                                          } else {
                                                              // Handle the publish feed callback
                                                              NSDictionary *urlParams = [self parseURLParams:[resultURL query]];
                                                              
                                                              if (![urlParams valueForKey:@"post_id"]) {
                                                                  // User cancelled.
                                                                  NSLog(@"User cancelled.");
                                                                  
                                                              } else {
                                                                  // User clicked the Share button
                                                                  NSString *result = [NSString stringWithFormat: @"Posted story, id: %@", [urlParams valueForKey:@"post_id"]];
                                                                  NSLog(@"result %@", result);
                                                              }
                                                          }
                                                      }
                                                  }];
    }
}

// A function for parsing URL parameters returned by the Feed Dialog.
- (NSDictionary*)parseURLParams:(NSString *)query {
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val =
        [kv[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        params[kv[0]] = val;
    }
    return params;
}


#pragma mark Interface 

- (void) login
{
    if (nil == [FBSession activeSession]){
        [FBSession setActiveSession:nil];
        [self FB_CreateNewSession];
        
        
    }
    
    [self FB_Login];
//    [[FBSession activeSession] closeAndClearTokenInformation];
//    [FBSession setActiveSession:nil];
//    [self FB_CreateNewSession];
    
    
}

- (void) logout
{
    [self FB_Logout];
}

- (void)postingImageTest:(UIImage*)image
{
    // Request publish_actions
    [FBSession.activeSession requestNewPublishPermissions:[NSArray arrayWithObject:@"publish_actions"]
                                          defaultAudience:FBSessionDefaultAudienceFriends
                                        completionHandler:^(FBSession *session, NSError *error) {
                                            __block NSString *alertText;
                                            __block NSString *alertTitle;
                                            if (!error) {
                                                if ([FBSession.activeSession.permissions
                                                     indexOfObject:@"publish_actions"] == NSNotFound){
//                                                    // Permission not granted, tell the user we will not publish
//                                                    alertTitle = @"Permission not granted";
//                                                    alertText = @"Your action will not be published to Facebook.";
//                                                    [[[UIAlertView alloc] initWithTitle:title
//                                                                                message:text
//                                                                               delegate:self
//                                                                      cancelButtonTitle:@"OK!"
//                                                                      otherButtonTitles:nil] show];
                                                    
                                                    
                                                    
                                                    
                                                } else {
                                                    // Permission granted, publish the OG story
//                                                    [self publishStory];
                                                    
                                                    [self FB_UploadPhotoWithimage:image];

                                                    
                                                }
                                                
                                            } else {
                                                // There was an error, handle it
                                                // See https://developers.facebook.com/docs/ios/errors/
                                            }
                                        }];
    
    
    
}

- (void) postingPhotoWithImage:(UIImage *)image
{
   
    
    //[self postingImageTest:image];
    
//    return;
    
    
    
    
    
    
    //로그인이 되어있는지 안되어있는지 모른다 . 무조건 로그인 시도한다.
    NSArray *permission = [[NSArray alloc] initWithObjects:@"email", nil];
    
    [FBSession openActiveSessionWithReadPermissions:permission allowLoginUI:true
                                  completionHandler:^(FBSession *session, FBSessionState status, NSError *error)
     {
         if (status == FBSessionStateClosedLoginFailed || status == FBSessionStateCreatedOpening) {
             [[FBSession activeSession] closeAndClearTokenInformation];
             [FBSession setActiveSession:nil];
             [self FB_CreateNewSession];
         }
         else{
             
             self.facebookAcessToken = session.accessTokenData.accessToken;
             
             // Update our game now we've logged in
             //업로드 관련 화면 업데이트
             //성공인지 실패인지 보낸다.
//             UIImage *image = [UIImage imageNamed:@"frame_08_castle"];
             //[self FB_UploadPhotoWithimage:image];
             
             
             
             
             if ([FBSession.activeSession.permissions
                  indexOfObject:@"publish_actions"] == NSNotFound){
                 
                 [self postingImageTest:image];
                 
             } else {
                 // Permission granted, publish the OG story
                 //                                                    [self publishStory];
                 
                 [self FB_UploadPhotoWithimage:image];
                 
                 
             }

         }
         
     }];


}

- (void)requestFacebookIDWithCompletion:(void(^)(NSString*))completion
{
    // Start the facebook request
    [[FBRequest requestForMe]startWithCompletionHandler:
     ^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *result, NSError *error)
     {
         // Did everything come back okay with no errors?
         if (!error && result)
         {
//             _facebookID = result.id;
//             _facebookEmail = [result objectForKey:@"email"];
             
             self.facebookID = [result objectForKey:@"id"];
             self.emailAddress = [result objectForKey:@"email"];
             completion(self.facebookID);
         }
         else {
             NSLog(@"FB_UserID GetUser ID Error %@", error.description);
             completion(nil);
         }
     }];
}

- (void)inviteFriendWithReturnValue:(void(^)(enum FacebookInviteReturn))completion
{
    
}
- (void)checkFacebookIDWithCompletion:(void(^)(NSString*))completion
{
    if ([FBSession.activeSession.permissions indexOfObject:@"email"] == NSNotFound){
        
        [FBSession openActiveSessionWithReadPermissions:@[@"email"] allowLoginUI:true
                                      completionHandler:^(FBSession *session, FBSessionState status, NSError *error)
         {
             NSLog(@"FBError is %@", error);
             
             if (status == FBSessionStateClosedLoginFailed || status == FBSessionStateCreatedOpening) {
                 
                 [[FBSession activeSession] closeAndClearTokenInformation];
                 [FBSession setActiveSession:nil];
                 [self FB_CreateNewSession];
                 
             }
             else{
                 
                 self.facebookAcessToken = session.accessTokenData.accessToken;
                 
                 [self requestFacebookIDWithCompletion:^(NSString* facebookid) {
                     completion(facebookid);
                 }];
                 
             }
         }];
        
    } else {
        
        [self requestFacebookIDWithCompletion:^(NSString* facebookid) {
            completion(facebookid);
        }];
        
    }

}


- (NSArray *) getInviteFriendList
{
    return nil;
//    
//    [FBRequestConnection startWithGraphPath:[NSString stringWithFormat:@"%llu/friends?fields=installed,name,id", m_myFacebookID] parameters:nil HTTPMethod:@"GET" completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
//        
//        if (result && !error)
//        {
//            for (NSDictionary *dict in [result objectForKey:@"data"])
//            {
//                //NSString *name = [[[[dict objectForKey:@"user"] objectForKey:@"name"] componentsSeparatedByString:@" "] objectAtIndex:0];
//                NSString *strInstalled = [dict objectForKey:@"installed"];
//                NSString *strName = [dict objectForKey:@"name"];
//                NSString *strID = [dict objectForKey:@"id"];
//                NSLog(@" Name[%@] , id[%@] Installed = %@",strName, strID, strInstalled );
//                
//                FacebookFriendInfo finfo;
//                //                    finfo.friend_name = [strName UTF8String];
//                //                    finfo.friend_id = [strID UTF8String];
//                finfo.friend_name = GetSafeUTF8Char(strName); // [[StringCheck nullCheck:strName] UTF8String];
//                finfo.friend_id = GetSafeUTF8Char(strID); // [[StringCheck nullCheck:strID] UTF8String];
//                
//                finfo.game_user = (strInstalled)?true:false ;
//                finfo.best_score = -1;
//                finfo.canReceiveDisc = true;
//                finfo.heartSentTime = 0;
//                finfo.donePictureRefresh = false;
//                finfo.mailboxlength = 0;
//                
//                m_FriendInfoList.push_back(finfo);
//                
//                if (finfo.game_user){
//                    m_GameUserList.push_back(finfo);
//                }
//            }
//            
//            FacebookFriendInfo myInfo;
//            myInfo.best_score = -1;
//            myInfo.friend_id = m_myFacebookIDString;
//            myInfo.friend_name = m_myFacebookName;
//            myInfo.game_user = true;
//            myInfo.mailboxlength = 0;
//            
//            m_GameUserList.push_back(myInfo);
//            
//            this->changeStatus(kFaceBookManStatusGotFriendInfos);
//        }
//    }];
}

- (void)requestFriendListForInviteWithCompletion:(void(^)(NSArray*))completion;
{
    [FBRequestConnection startWithGraphPath:[NSString stringWithFormat:@"%@/friends?fields=installed,name,id", _facebookID] parameters:nil HTTPMethod:@"GET" completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        
        if (result && !error)
        {
            NSMutableArray *friendList = [NSMutableArray array];
            for (NSDictionary *dict in [result objectForKey:@"data"])
            {
                //NSString *name = [[[[dict objectForKey:@"user"] objectForKey:@"name"] componentsSeparatedByString:@" "] objectAtIndex:0];
                NSString *strInstalled = [dict objectForKey:@"installed"];
                
                if (nil != strInstalled)continue;
                
                NSString *strName = [dict objectForKey:@"name"];
                NSString *strID = [dict objectForKey:@"id"];
                NSLog(@" Name[%@] , id[%@] Installed = %@",strName, strID, strInstalled );
                //[friendList addObject:strID];
                
                FacebookFriendInfo *fi = [[FacebookFriendInfo alloc]init];
                fi.name = strName;
                fi.facebookID = strID;
                fi.checked = NO;
                
                [friendList addObject:fi];
            }
            
            completion(friendList);
        }
        else{
            completion(nil);
        }
    }];
}

- (void)friendListForInviteWithCompletion2:(void(^)(NSArray*))completion;
{
    [self initSession];

    
    if ([FBSession.activeSession.permissions indexOfObject:@"email"] == NSNotFound){
        
        [FBSession openActiveSessionWithReadPermissions:@[@"email"] allowLoginUI:true
                                      completionHandler:^(FBSession *session, FBSessionState status, NSError *error)
         {
             if (status == FBSessionStateClosedLoginFailed || status == FBSessionStateCreatedOpening) {
                 [[FBSession activeSession] closeAndClearTokenInformation];
                 [FBSession setActiveSession:nil];
                 [self FB_CreateNewSession];
                 
                 [self friendListForInviteWithCompletion:completion];
             }
             else{
                 
                 self.facebookAcessToken = session.accessTokenData.accessToken;
                 
                 [self requestFriendListForInviteWithCompletion:^(NSArray *friendList) {
                     completion(friendList);
                 }];
                 
             }
         }];
        
    } else {
        
        [self requestFriendListForInviteWithCompletion:^(NSArray *friendList) {
            completion(friendList);
        }];
        
    }
}

- (void)friendListForInviteWithCompletion:(void(^)(NSArray*))completion;
{
    [self initSession];
    if ([FBSession activeSession].state != FBSessionStateOpenTokenExtended && [FBSession activeSession].state != FBSessionStateOpen){
        [self FB_RequestOpenSessionWithHandler:^(BOOL success1) {
            if (success1){

                    [self FB_RequestUserIDWithHandler:^(BOOL success) {
                        if (success){
                            [self requestFriendListForInviteWithCompletion:^(NSArray *friendList) {
                                completion(friendList);
                            }];
                        }
                        else{
                            completion(nil);
                        }
                        
                    }];
            }
            else{
                completion(nil);
            }
        }];
    }
    else{
        [self FB_RequestUserIDWithHandler:^(BOOL success) {
            if (success){
                [self requestFriendListForInviteWithCompletion:^(NSArray *friendList) {
                    completion(friendList);
                }];
            }
            else{
                completion(nil);
            }
            
        }];
    }

}

-(BOOL)initSession
{
//    if (nil == self.facebookAcessToken){
//    FBSessionState s = [FBSession activeSession].state;
    
    if (FBSession.activeSession.state == FBSessionStateClosed ||
        FBSession.activeSession.state == FBSessionStateCreated){
//        [[FBSession activeSession] closeAndClearTokenInformation];
//        [FBSession setActiveSession:nil];
        [self FB_CreateNewSession];
        
        
        return TRUE;
    }
    
    return FALSE;
}

- (void)postingPhoto3:(UIImage*)photo completion:(void(^)(BOOL))completion
{
    [self initSession];
    
    
    // We need to request write permissions from Facebook
    static BOOL bHaveRequestedPublishPermissions = FALSE;
    
    if (!bHaveRequestedPublishPermissions)
    {
        NSArray *permissions = [[NSArray alloc] initWithObjects:
                                @"publish_actions", nil];
        
        [[FBSession activeSession] requestNewPublishPermissions:permissions defaultAudience:FBSessionDefaultAudienceEveryone completionHandler:^(FBSession *session, NSError *error) {
            NSLog(@"Reauthorized with publish permissions.");
            
            
            if (error == nil){
                
                bHaveRequestedPublishPermissions = TRUE;
                
                self.facebookAcessToken = session.accessTokenData.accessToken;
                
                [self uploadPhoto:photo Message:@"Dole Height Chart" completion:^(BOOL isSuccess){
                    completion(isSuccess);
                }];
                
            }
            else{
                bHaveRequestedPublishPermissions = FALSE;
            }
            
        }];
    }
    else{
        [self uploadPhoto:photo Message:@"Dole Height Chart" completion:^(BOOL isSuccess){
            completion(isSuccess);
        }];
    }
}

- (void)postingPhoto:(UIImage*)photo Message:(NSString*)message completion:(void(^)(BOOL))completion
{
    [self initSession];
    if ([FBSession activeSession].state != FBSessionStateOpenTokenExtended && [FBSession activeSession].state != FBSessionStateOpen){
//        __block BOOL requestedPublishPermission = NO;
//        [self FB_RequestOpenSessionWithHandler:^(BOOL success1) {
//            if (success1){
//                if (requestedPublishPermission == NO){
//                    requestedPublishPermission = YES;
//                    
////                    dispatch_async(dispatch_get_main_queue(), ^{
////                        
////                        [self FB_RequestPublishPermissionWithHandler:^(BOOL success2) {
////                            if (success2){
////                                [self uploadPhoto:photo Message:kPOSTING_MESSAGE completion:^(BOOL isSuccess){
////                                    completion(isSuccess);
////                                }];
////                            }
////                            else{
////                                completion(FALSE);
////                            }
////                        }];
////
////                    });
//
//                }
//            }
//            else{
//                completion(FALSE);
//            }
//        }];
        [FBSession openActiveSessionWithPublishPermissions:@[@"publish_actions"] defaultAudience:FBSessionDefaultAudienceEveryone allowLoginUI:YES completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
            NSLog(@"Reauthorized with publish permissions.");
            
            if (error == nil){

                [self uploadPhoto:photo Message:message completion:^(BOOL isSuccess){
                    completion(isSuccess);
                }];
            }
            else{
                
                completion(FALSE);
            }

        }];
    }
    else{
        [self FB_RequestPublishPermissionWithHandler:^(BOOL success3) {
            if (success3){
                [self uploadPhoto:photo Message:message completion:^(BOOL isSuccess){
                    completion(isSuccess);
                }];
            }
            else{
                completion(FALSE);
            }
        }];
    }
}

-(void)FB_RequestOpenSessionWithHandler:(void(^)(BOOL success))completion
{
    
    if ( [FBSession activeSession].state != FBSessionStateOpen && [FBSession activeSession].state != FBSessionStateOpenTokenExtended){
    
        [FBSession openActiveSessionWithReadPermissions:@[@"basic_info"] allowLoginUI:YES
                                      completionHandler:^(FBSession *session, FBSessionState status, NSError *error)
        {
             if (status == FBSessionStateClosedLoginFailed || status == FBSessionStateCreatedOpening) {
                 [[FBSession activeSession] closeAndClearTokenInformation];
                 [FBSession setActiveSession:nil];
                 [self FB_CreateNewSession];
                 completion(NO);
             }
             else{
                 self.facebookAcessToken = session.accessTokenData.accessToken;
                 completion(TRUE);
             }
         }];
        
    } else {
        completion(TRUE);
    }
    
}
-(void)FB_RequestPublishPermissionWithHandler:(void(^)(BOOL success))completion
{
    // We need to request write permissions from Facebook
    if ([[FBSession activeSession].permissions indexOfObject:@"publish_actions"] == NSNotFound){
       
        NSArray *permissions = [[NSArray alloc] initWithObjects:
                                @"publish_actions", nil];
        
        [[FBSession activeSession] requestNewPublishPermissions:permissions defaultAudience:FBSessionDefaultAudienceEveryone completionHandler:^(FBSession *session, NSError *error) {
            NSLog(@"Reauthorized with publish permissions.");
            
            
            if (error == nil){
                
                self.facebookAcessToken = session.accessTokenData.accessToken;
                
                completion(TRUE);
                
            }
            else{
                
                completion(FALSE);
            }
            
        }];
    }
    else{
        completion(TRUE);
    }
}

- (void) FB_RequestUserIDWithHandler:(void(^)(BOOL success))completion
{
    // Start the facebook request
    [[FBRequest requestForMe]startWithCompletionHandler:
     ^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *result, NSError *error)
     {
         // Did everything come back okay with no errors?
         if (!error && result)
         {
             self.facebookID = [result objectForKey:@"id"];
             self.emailAddress = [result objectForKey:@"email"];
             self.birthday = [result objectForKey:@"birthday"];
             self.gender = [result objectForKey:@"gender"];
             self.address = [result objectForKey:@"address"];
             self.facebookName = [result objectForKey:@"name"];
             completion(TRUE);
         }
         else {
             NSLog(@"FB_UserID GetUser ID Error %@", error.description);
             completion(FALSE);
         }
     }];
}

- (void)uploadPhoto:(UIImage*)photo Message:(NSString*)message completion:(void(^)(BOOL))completion
{
    NSDictionary *params = @{@"message": message, @"picture": UIImagePNGRepresentation(photo)};
    
    
    
    [FBRequestConnection startWithGraphPath:@"me/photos"
                                 parameters:params
                                 HTTPMethod:@"POST"
                          completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                              
                              if (nil == error){
                                  FBGraphObject *resultObj = result;
                                  NSLog(@"uploadPhoto url is %@", resultObj);
                                  completion(TRUE);
                              }
                              else{
                                  NSLog(@"uploadPhoto Error is %@", error);
                                  completion(FALSE);
                              }
                              
    }];
}


- (void)TEMP_inviteFriendWithFriendIDList:(NSArray *)friendList
{

    // Normally this won't be hardcoded but will be context specific, i.e. players you are in a match with, or players who recently played the game etc
    
//    NSArray *suggestedFriends = [[NSArray alloc] initWithObjects:
//                                 @"223400030", @"286400088", @"767670639", @"516910788",
//                                 nil];
    
    NSMutableArray *suggestedFriends = [NSMutableArray array];
    for (FacebookFriendInfo *info in friendList) {
        if (info.checked)
            [suggestedFriends addObject:info.facebookID];
    }
    
//    SBJsonWriter *jsonWriter = [SBJsonWriter new];
//    NSDictionary *challenge =  [NSDictionary dictionaryWithObjectsAndKeys: [NSString stringWithFormat:@"%d", nScore], @"challenge_score", nil];
//    NSString *challengeStr = [jsonWriter stringWithObject:challenge];
    
    
    // Create a dictionary of key/value pairs which are the parameters of the dialog
    
    // 1. No additional parameters provided - enables generic Multi-friend selector
    NSMutableDictionary* params =   [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                     // 2. Optionally provide a 'to' param to direct the request at a specific user
                                     //@"286400088", @"to", // Ali
                                     // 3. Suggest friends the user may want to request, could be game context specific?
                                     [suggestedFriends componentsJoinedByString:@","], @"suggestions",
                                     @"data", @"data",
                                     nil];
    
    if (ms_friendCache == NULL) {
        ms_friendCache = [[FBFrictionlessRecipientCache alloc] init];
    }
    
    [ms_friendCache prefetchAndCacheForSession:nil];
    
    [FBWebDialogs presentRequestsDialogModallyWithSession:nil
                                                  message:@"Dole Height Chart"
                                                    title:@"Dole"
                                               parameters:params
                                                  handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
                                                      if (error) {
                                                          // Case A: Error launching the dialog or sending request.
                                                          NSLog(@"Error sending request.");
                                                          
                                                          [self.invitedelegate inviteResult:FacebookInviteReturn_Error];
                                                          
                                                      } else {
                                                          if (result == FBWebDialogResultDialogNotCompleted) {
                                                              // Case B: User clicked the "x" icon
                                                              NSLog(@"User canceled request.");
//                                                              [self.delegate inviteResult:FacebookInviteReturn_Cancel];
                                                              [self.invitedelegate inviteResult:FacebookInviteReturn_Cancel];

                                                          } else {
                                                              NSLog(@"Request Sent OK.");
//                                                              [self.delegate inviteResult:FacebookInviteReturn_OK];
                                                              [self.invitedelegate inviteResult:FacebookInviteReturn_OK];

                                                          }
                                                      }}
                                              friendCache:ms_friendCache];

}

- (void)inviteFriendWithFriendIDList:(NSArray *)friendList
{
    NSMutableArray *idArray = [NSMutableArray array];
    for (FacebookFriendInfo *fi in friendList) {
        [idArray addObject:fi.facebookID];
    }
    NSMutableDictionary* params = [NSMutableDictionary dictionary];
    
    if (idArray != nil && [idArray count] > 0) {
        NSString *selectIDsStr = [idArray componentsJoinedByString:@","];
        [params setObject:selectIDsStr forKey:@"to"];
    }
    
    // Display the requests dialog
    [FBWebDialogs
     presentRequestsDialogModallyWithSession:nil
     message:@"Dole Height Chart"
     title:@"Dole"
     parameters:params
     handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
         if (error) {
             // Error launching the dialog or sending request.
             NSLog(@"Error sending request.");
             [self.invitedelegate inviteResult:FacebookInviteReturn_Error];
         } else {
             if (result == FBWebDialogResultDialogNotCompleted) {
                 // User clicked the "x" icon
                 NSLog(@"User canceled request.");
                 [self.invitedelegate inviteResult:FacebookInviteReturn_Cancel];

             } else {
                 // Handle the send request callback
                 NSDictionary *urlParams = [self parseURLParams:[resultURL query]];
                 if (![urlParams valueForKey:@"request"]) {
                     // User clicked the Cancel button
                     NSLog(@"User canceled request.");
                     [self.invitedelegate inviteResult:FacebookInviteReturn_Cancel];

                 } else {
                     // User clicked the Send button
                     NSString *requestID = [urlParams valueForKey:@"request"];
                     NSLog(@"Request ID: %@", requestID);
                     [self.invitedelegate inviteResult:FacebookInviteReturn_OK];
                 }
             }
         }
     }];
}


@end

@implementation FacebookFriendInfo
@end
