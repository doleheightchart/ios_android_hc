//
//  HeightDetailTree.m
//  HeightChart
//
//  Created by mytwoface on 2014. 3. 2..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "HeightDetailTree.h"
#import "UIColor+ColorUtil.h"
#import "UIFont+DoleHeightChart.h"
#import "UIView+ImageUtil.h"
#import "UIViewController+DoleHeightChart.h"

#pragma mark DetailCell

#define kDetailLeftCellID           @"DetailLeftCellID"
#define kDetailRightCellID          @"DetailRightCellID"
#define kDetailZoomCellID           @"DetailZoomCellID"
#define kDetailAddHeightCellID      @"AddHeightCellID"
#define kDetailZoomAddHeightCellID  @"AddHeightZoomCellID"

#define kDetailTreeMinKidHeight     0
#define kDetailTreeMaxKidHeight     200
#define kDetailTreeDefaultMinHeight 16

#define kDetailTreeCountKidHeights  (kDetailTreeMaxKidHeight - kDetailTreeMinKidHeight)

#define kDetailTreeCellHeight   ((43 + 22 + 43)/2)

#define kDetailTreeZoomCellHeightOnFourInchi    (20)
#define kDetailTreeZoomCellHeight               (16)

#define kDetailBackTreeWidth    (46/2)
#define kDetailBackZoomInTreeWidth (20/2)

#pragma mark DoleKey Nickname

@interface DoleKeyNicknameControl : UIControl{
    UIImageView     *_backgroundView;
    UILabel         *_nickNameLabel;
}

-(void)decorateWithNickname:(NSString*)nickName zoomMode:(BOOL)isZoomMode monkeyNumber:(NSUInteger)monkeyNumber;

@end

@implementation DoleKeyNicknameControl

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self){
        [self initialize];
    }
    return self;
}

-(id)init
{
    self = [super init];
    if (self){
        [self initialize];
    }
    return self;
}

-(void)initialize
{
    _backgroundView = [[UIImageView alloc]init];
    [self addSubview:_backgroundView];
    
    _nickNameLabel = [[UILabel alloc]init];
    [self addSubview:_nickNameLabel];
}

-(void)decorateWithNickname:(NSString*)nickName
                   zoomMode:(BOOL)isZoomMode
               monkeyNumber:(NSUInteger)monkeyNumber
{
    _nickNameLabel.textColor = [UIColor colorWithMonkeyNumber:monkeyNumber];
    _nickNameLabel.text = nickName;
    _nickNameLabel.textAlignment = NSTextAlignmentCenter;
    CGFloat centerX = self.superview.bounds.size.width/2;
    
    if (NO == isZoomMode){
        UIFont *font = [UIFont defaultBoldFontWithSize:33/2];
        CGSize textSize = [nickName sizeWithFont:font];
        CGFloat textWidth = MIN(MAX((88/2), textSize.width), (158/2));
        
        CGFloat backImageWidth = textWidth + 16;
        
        
        CGRect frameThis = CGRectMake(centerX - (backImageWidth/2),
                                      254/2,
                                      backImageWidth,
                                      52/2);
        
        
        
        CGRect frameNickname = CGRectMake(frameThis.size.width/2 - (textWidth/2),
                                          6/2,
                                          textWidth,
                                          36/2);
        
        CGRect frameBackImg = CGRectMake(0, 0, backImageWidth, 52/2);
        
        self.frame = frameThis;
        _nickNameLabel.frame = frameNickname;
        _backgroundView.frame = frameBackImg;
        
        
        _nickNameLabel.font = font;
        UIImage *backgroundImage = [UIImage resizableImageWithName:@"detail_namebox"
                                                         CapInsets:UIEdgeInsetsMake(0, 10, 0, 10)];
        _backgroundView.image = backgroundImage;
        
    }
    else{
        
        UIFont *font = [UIFont defaultBoldFontWithSize:20/2];
        CGSize textSize = [nickName sizeWithFont:font];
        CGFloat textWidth = MIN(MAX((52/2), textSize.width), (94/2));
        
        CGFloat backImageWidth = textWidth + 10;
        
        
        CGRect frameThis = CGRectMake(centerX - (backImageWidth/2),
                                      128/2,
                                      backImageWidth,
                                      32/2);
        
        
        
        CGRect frameNickname = CGRectMake(frameThis.size.width/2 - (textWidth/2),
                                          4/2,
                                          textWidth,
                                          22/2);
        
        CGRect frameBackImg = CGRectMake(0, 0, backImageWidth, 32/2);
        
        self.frame = frameThis;
        _nickNameLabel.frame = frameNickname;
        _backgroundView.frame = frameBackImg;
        
        _nickNameLabel.font = font;
        UIImage *backgroundImage = [UIImage resizableImageWithName:@"Detail_all_namebox"
                                                         CapInsets:UIEdgeInsetsMake(0, 10, 0, 10)];
        _backgroundView.image = backgroundImage;
        
    }
}

@end





@interface FadeOutFlowLayout : UICollectionViewFlowLayout
@end
@implementation FadeOutFlowLayout

- (void)prepareLayout {
}

- (NSArray*)layoutAttributesForElementsInRect:(CGRect)rect {
    NSMutableArray *array = [NSMutableArray arrayWithArray:[super layoutAttributesForElementsInRect:rect]];
    
    for (FadeOutInAttributes *attr in array) {
        [self applyAttributes:attr];
    }
    
    return array;
}

- (UICollectionViewLayoutAttributes*)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath {
    FadeOutInAttributes *attributes = (FadeOutInAttributes*)[super layoutAttributesForItemAtIndexPath:indexPath];
    [self applyAttributes:attributes];
    NSLog(@"layoutAttributesForItemAtIndexPath--->>%@", indexPath);
    return attributes;
}

- (void) applyAttributes:(FadeOutInAttributes*)attributes {
    
    
    
//    NSLog(@"applyAttributes--->> contentOffset = %@, center is %@", NSStringFromCGPoint(self.collectionView.contentOffset),
//          NSStringFromCGPoint(attributes.center)
//          );
//    CGRect visibleRect;
//    
//    visibleRect.origin = self.collectionView.contentOffset;
//    visibleRect.size = self.collectionView.frame.size;
//    
//    CGFloat distance = CGRectGetMidY(visibleRect) - attributes.center.y;
//    if (distance < self.itemSize.height/2 && distance > -(self.itemSize.height/2)) {
//        attributes.offSetScroll = distance;
//        return;
//    }
    
//    attributes.offSetScroll = self.collectionView.contentOffset.y;
    
    attributes.offSetScroll = attributes.frame.origin.y - self.collectionView.contentOffset.y;
    
//    NSLog(@"OffSet is %f", attributes.offSetScroll);
    
}

- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds {
    return YES;
}

+(Class) layoutAttributesClass {
    
    return [FadeOutInAttributes class];
    
}

@end


#pragma mark DetailHeightCollectionView

@interface DetailHeightCollectionView : UICollectionView

@end

@implementation DetailHeightCollectionView

@end

#pragma mark HeightDetailTree

@interface HeightDetailTree (){
    DetailHeightCollectionView  *_detailCollectionView;
    UICollectionViewFlowLayout  *_collectionViewLayout;
    UIImageView                 *_backgroundTreeImageView;
    UIImageView                 *_dolekeyImageView;
    UIImageView                 *_dolekeyTreeImageView;
    DoleKeyNicknameControl      *_nickNameLabelControl;
    NSInteger                   _currentShownMaxHeight;
    UIImageView                 *_treeCoverImageView;
    NSInteger                   _scrollIndexByTimer;
    NSInteger                   _scrollIndexCurrent;
    NSTimer                     *_scrollTimer;


    //Size
    CGFloat                     _heightOfDetailCell;
    CGFloat                     _heightOfZoomDetailCell;
}

@end

@implementation HeightDetailTree

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
//        [self initialize];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self){
//        [self initialize];
    }

    return self;
}

-(void)initialize
{
    self.backgroundColor = [UIColor clearColor];

    _heightOfDetailCell = kDetailTreeCellHeight;
    _heightOfZoomDetailCell = [UIScreen isFourInchiScreen] ? kDetailTreeZoomCellHeightOnFourInchi : kDetailTreeZoomCellHeight;
    
    [self addBackgroundTree];
    
    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_6_1){
        _collectionViewLayout = [[FadeOutFlowLayout alloc] init];
    }
    else{
        _collectionViewLayout = [[UICollectionViewFlowLayout alloc] init];
    }
    _collectionViewLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    _collectionViewLayout.minimumInteritemSpacing = 0;
    _collectionViewLayout.minimumLineSpacing = 0;
    _collectionViewLayout.itemSize = CGSizeMake(self.bounds.size.width, _heightOfDetailCell);
    _collectionViewLayout.sectionInset = UIEdgeInsetsMake(50, 0, 0, 0);
    
    _detailCollectionView = [[DetailHeightCollectionView alloc]initWithFrame:[self frameCollectionView]
                                                        collectionViewLayout:_collectionViewLayout];
    _detailCollectionView.bounces = NO;
    _detailCollectionView.showsHorizontalScrollIndicator = NO;
    _detailCollectionView.showsVerticalScrollIndicator = NO;
    _detailCollectionView.delegate = self;
    _detailCollectionView.dataSource = self;
    _detailCollectionView.backgroundColor = [UIColor clearColor];
    
    [_detailCollectionView registerClass:[LeftDetailCell class] forCellWithReuseIdentifier:kDetailLeftCellID];
    [_detailCollectionView registerClass:[RightDetailCell class] forCellWithReuseIdentifier:kDetailRightCellID];
    [_detailCollectionView registerClass:[HeightZoomDetailCell class] forCellWithReuseIdentifier:kDetailZoomCellID];
    [_detailCollectionView registerClass:[AddDetailCell class] forCellWithReuseIdentifier:kDetailAddHeightCellID];
    [_detailCollectionView registerClass:[AddZoomDetailCell class] forCellWithReuseIdentifier:kDetailZoomAddHeightCellID];
    
    [self addSubview:_detailCollectionView];
    
    
    _treeCoverImageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"Detail_Height_tree_cover"]];
    _treeCoverImageView.frame = CGRectMake(164/2, 148/2, 312/2, 252/2);
    [self addSubview:_treeCoverImageView];
    
    _dolekeyTreeImageView = [[UIImageView alloc] init];
    [self addSubview:_dolekeyTreeImageView];
    
    _nickNameLabelControl = [[DoleKeyNicknameControl alloc]init];
    [self addSubview:_nickNameLabelControl];
    
    _dolekeyImageView = [[UIImageView alloc]init];
    [self addSubview:_dolekeyImageView];
    
    UIPinchGestureRecognizer *pinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinch:)];
    [_detailCollectionView addGestureRecognizer:pinchGesture];
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                    action:@selector(tapMonkey:)];
    
    [self addGestureRecognizer:tapRecognizer];

}

-(void)tapMonkey:(UITapGestureRecognizer*)recognizer
{
    if (recognizer.state == UIGestureRecognizerStateEnded) {
        CGPoint pt = [recognizer locationInView:_dolekeyImageView];
        if (CGRectContainsPoint(_dolekeyImageView.bounds, pt)){
            [self.treeDelegate clickMonkey];
        }
//        else if (CGRectContainsPoint(_detailCollectionView.bounds, pt)){
        else{
            if (NO == self.isZoomMode){
                for (UICollectionViewCell *cell in _detailCollectionView.visibleCells) {
                    if ([cell isKindOfClass:[HeightDetailCell class]]){
                        HeightDetailCell *hCell = (HeightDetailCell*)cell;
                        if (hCell && hCell.topArrowSticker && hCell.topArrowSticker.heightDetail){
                            hCell.topArrowSticker.heightDetail.expanded = NO;
                            [hCell.topArrowSticker updateExpandWithAnimated:YES];
                        }
                    }
                }
            }
            
        }
    }
    else if (recognizer.state == UIGestureRecognizerStateBegan){
    }
}

-(void)handlePinch:(UIPinchGestureRecognizer*)recognizer
{
    if (recognizer.state == UIGestureRecognizerStateEnded)
    {
//        self.transform = CGAffineTransformMakeScale(1, 1);
//        if (recognizer.scale > 1 && self.isZoomMode == YES){
//            self.isZoomMode = NO;
//        }
//        else if (recognizer.scale < 1 && self.isZoomMode == NO){
//            self.isZoomMode = YES;
//        }
        
//        [UIView animateWithDuration:5 animations:^{
//            //[_detailCollectionView setContentOffset:CGPointMake(0, 0) animated:YES];
//            [_detailCollectionView setContentOffset:CGPointMake(0, 0)];
//        }];

        
      //  [_detailCollectionView selectItemAtIndexPath:[NSIndexPath indexPathForRow:30 inSection:0] animated:YES scrollPosition:UICollectionViewScrollPositionCenteredVertically];
      //  [_detailCollectionView selectItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UICollectionViewScrollPositionCenteredVertically];
        
        
        
    }
    else{
        
        if (self.isDeleteMode)return;
//        self.transform = CGAffineTransformMakeScale(recognizer.scale, 1);
        if (recognizer.scale > 1 && self.isZoomMode == YES){
//            self.isZoomMode = NO;
            [self.treeDelegate didPinchZoom:NO];
        }
        else if (recognizer.scale < 1 && self.isZoomMode == NO){
//            self.isZoomMode = YES;
            [self.treeDelegate didPinchZoom:YES];

        }
    }
}

-(CGRect)frameCollectionView
{
    NSLog(@"This Detail Control Size is %@", NSStringFromCGRect(self.bounds));
    
    if (NO == self.isZoomMode){
        
        CGFloat posY = (306)/2;
        
        return CGRectMake(0, posY, self.bounds.size.width, self.bounds.size.height-posY);
    }
    else{

        CGFloat posY = (114)/2;

        return CGRectMake(0, posY, self.bounds.size.width, self.bounds.size.height-posY);
    }
}

-(CGRect)frameBackgroundTreeImage
{
    CGFloat treeImageWidth = self.isZoomMode ? kDetailBackZoomInTreeWidth : kDetailBackTreeWidth;

    if (NO == self.isZoomMode){
        return CGRectMake((self.bounds.size.width/2) - (treeImageWidth/2) + 0.5, 140, treeImageWidth, self.bounds.size.height-140);
    }
    else{
        return CGRectMake((self.bounds.size.width/2) - (treeImageWidth/2), 70, treeImageWidth, self.bounds.size.height-70);
    }
}

-(void)addBackgroundTree
{
    //[_backgroundTreeImageView removeFromSuperview];
    CGRect backFrame = CGRectMake((self.bounds.size.width/2) - (kDetailBackTreeWidth/2),
                                  0,
                                  kDetailBackTreeWidth,
                                  self.bounds.size.height);

    _backgroundTreeImageView = [[UIImageView alloc] initWithFrame:backFrame];
//    _backgroundTreeImageView.image = [UIImage imageNamed:@"detail_tree"];
    [self addSubview:_backgroundTreeImageView];
    
    [self chageBackgroundTreeImage];
}

-(void)chageBackgroundTreeImage
{
    _backgroundTreeImageView.frame = [self frameBackgroundTreeImage];
    NSString *backtreeImageName = self.isZoomMode ? @"detail_all_tree_node" : @"detail_tree";
    UIImage *backTreeImage = [UIImage imageNamed:backtreeImageName];
    _backgroundTreeImageView.image = backTreeImage;
}

-(void)setMaxHeight:(NSInteger)maxHeight
{
    _maxHeight = maxHeight;
    
    if (_currentShownMaxHeight < _maxHeight){
        _currentShownMaxHeight = _maxHeight;
        
        [self reloadDetails];
    }
}

- (void)reloadDetails
{
    _currentShownMaxHeight = MAX([self.dataSource currentKidHeight], kDetailTreeDefaultMinHeight);
    
    [_detailCollectionView reloadData];
    
//    NSIndexPath *selectedItem = [NSIndexPath indexPathForRow:50 inSection:0];
//    [_detailCollectionView selectItemAtIndexPath:selectedItem animated:YES scrollPosition:(UICollectionViewScrollPositionCenteredVertically)];
    
}

-(void)addNewHeightDetailWith:(KidHeightDetail*)kidHeightDetail
                   DetailInfo:(HeightDetailInfo*)detailInfo
{

//    NSInteger addedIndexCount = detailInfo.kidHeight - _currentShownMaxHeight;
//    if (addedIndexCount <= 0) return;
//    
//    NSMutableArray *arrayIndex = [NSMutableArray arrayWithCapacity:addedIndexCount];
//    for (int i = 0; i < addedIndexCount; i++) {
//        [arrayIndex addObject:[NSIndexPath indexPathForRow:_currentShownMaxHeight+i inSection:0]];
//    }
//    
//    
//    
//    
//    [_detailCollectionView performBatchUpdates:^{
//        
//        [_detailCollectionView insertItemsAtIndexPaths:arrayIndex];
//        
//    } completion:^(BOOL finished) {
//        
//        
//    }];

//    [_detailCollectionView selectItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UICollectionViewScrollPositionTop];
//
//    NSArray *indexPathArray = _detailCollectionView.indexPathsForVisibleItems;
//    NSIndexPath *first = indexPathArray.firstObject;
//    
//    [UIView animateWithDuration:3 animations:^{
//        [_detailCollectionView selectItemAtIndexPath:[NSIndexPath indexPathForRow:first.row inSection:0] animated:YES scrollPosition:UICollectionViewScrollPositionTop];
//
//    }];


//    [_collectionViewLayout invalidateLayout];
//    
//    
//    
//    NSInteger indexOfHeight = _currentShownMaxHeight - detailInfo.kidHeight;
//    indexOfHeight = MAX(0, indexOfHeight-1);
//    
//    [self scrollSlowlyAtIndex:indexOfHeight];
    
//
//    int index = 0;
//    
//    [UIView animateWithDuration:5 animations:^{
//        
//
//  
//    } completion:^(BOOL finished) {
//        NSLog(@"Complted scroll");
//    }];
//    
//    for (int i = 0; i < indexOfHeight; i++) {
//        [_detailCollectionView selectItemAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0] animated:YES scrollPosition:UICollectionViewScrollPositionTop];
//    }
    

    //_detailCollectionView.contentOffset = CGPointMake(0, 1000);
    
    
    
    
//    NSInteger row = _currentShownMaxHeight - detailInfo.kidHeight;
//    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
//    [_detailCollectionView selectItemAtIndexPath:indexPath
//                                        animated:NO
//                                  scrollPosition:UICollectionViewScrollPositionTop];
//    
//    
//    [_collectionViewLayout invalidateLayout];
//    
//    HeightDetailCell *cell = (HeightDetailCell*)[_detailCollectionView cellForItemAtIndexPath:indexPath];
//    
//    [cell updateWithHeightDetail:kidHeightDetail kidHeight:detailInfo.kidHeight];
//    [cell doAddNewAnimationWithDetailInfo:detailInfo];
}

-(void)scrollToNewHeightDetailWith:(NSInteger)oldMaxHeight DetailInfo:(HeightDetailInfo*)detailInfo
{
//    [_collectionViewLayout invalidateLayout];
//    
//    NSInteger oldIndex = _currentShownMaxHeight - oldMaxHeight;
//    
    NSInteger indexOfHeight = _currentShownMaxHeight - detailInfo.kidHeight;
//    indexOfHeight = MAX(0, indexOfHeight-1);
//    
//    [self scrollSlowlyFromIndex:oldIndex To:indexOfHeight];

    indexOfHeight = MAX(0, indexOfHeight-1);
    [_detailCollectionView selectItemAtIndexPath:[NSIndexPath indexPathForRow:indexOfHeight inSection:0] animated:YES scrollPosition:UICollectionViewScrollPositionTop];
}


-(void)doDeleteAnimationWithCheckedDetails:(NSArray*)detailsArray
{
    for (UICollectionViewCell *cell in _detailCollectionView.visibleCells) {
        if ([cell.reuseIdentifier isEqualToString:kDetailLeftCellID] ||
            [cell.reuseIdentifier isEqualToString:kDetailRightCellID]){
            
            HeightDetailCell *hcell = (HeightDetailCell*)cell;
            [hcell doDeleteAnimationWithDetailInfoArray:detailsArray];
        }
    }
    
//    [_collectionViewLayout invalidateLayout];
}

//-(void)doDeleteAnimationWithDetails:(NSArray*)detailsArray
//{
//    for (UICollectionViewCell *cell in _detailCollectionView.visibleCells) {
//        if ([cell.reuseIdentifier isEqualToString:kDetailLeftCellID] ||
//            [cell.reuseIdentifier isEqualToString:kDetailRightCellID]){
//            
//            HeightDetailCell *hcell = (HeightDetailCell*)cell;
//            [hcell doDeleteAnimationWithDetailInfoArray:detailsArray];
//        }
//    }
//    
//    //    [_collectionViewLayout invalidateLayout];
//}

-(void)createDoleKey
{
    [self setupNomalDoleKey];
}

-(void)setupNomalDoleKey
{
    CGRect frameLeaf = CGRectMake((164/2), (148/2), (312/2), (252/2));
    _dolekeyTreeImageView.frame = frameLeaf;
    _dolekeyTreeImageView.image = [UIImage imageNamed:@"detail_Height_tree"];
    
    
    [_nickNameLabelControl decorateWithNickname:[self.dataSource nickname] zoomMode:NO monkeyNumber:[self.dataSource characterNumber]];
    
    CGRect frameMonkey = CGRectMake((228/2), (56/2), (184/2), (252/2));
    _dolekeyImageView.frame = frameMonkey;
    NSString *monkeyImageName = [NSString stringWithFormat:@"detail_monkey_%02lu", (unsigned long)[self.dataSource characterNumber], nil];
    _dolekeyImageView.image = [UIImage imageNamed:monkeyImageName];
}

-(void)setupZoomModeDoleKey
{
    CGRect frameLeaf = CGRectMake((225/2), (64/2), (190/2), (152/2));
    _dolekeyTreeImageView.frame = frameLeaf;
    _dolekeyTreeImageView.image = [UIImage imageNamed:@"detail_all_Height_tree"];
    
    [_nickNameLabelControl decorateWithNickname:[self.dataSource nickname] zoomMode:YES monkeyNumber:[self.dataSource characterNumber]];
    
    CGRect frameMonkey = CGRectMake((265/2), (10/2), (110/2), (154/2));
    _dolekeyImageView.frame = frameMonkey;
    NSString *monkeyImageName = [NSString stringWithFormat:@"detail_all_monkey_%02lu", (unsigned long)[self.dataSource characterNumber], nil];
    _dolekeyImageView.image = [UIImage imageNamed:monkeyImageName];
}



#pragma mark UICollectionView Delegates



-(CGFloat)kidHeightFromIndexRow:(NSUInteger)index
{
    if (!self.isZoomMode){
        CGFloat kidHeight = index;
        CGFloat shownMaxHeight = (CGFloat)_currentShownMaxHeight;
        kidHeight = shownMaxHeight - kidHeight;
        return kidHeight;
    }
    else{
        CGFloat kidHeight = kDetailTreeMaxKidHeight - ((index-1)*10);
        return kidHeight;
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0){
        if (!self.isZoomMode){
            return CGSizeMake(self.bounds.size.width, ((43 + 22 + 43)/2));
        }
        else{
            return CGSizeMake(self.bounds.size.width, (40/2));
        }
    }
    
    if (!self.isZoomMode){
        CGFloat kidHeight = [self kidHeightFromIndexRow:indexPath.row];
        
        KidHeightDetail *heightInfo = [self.dataSource heightDetailOfKidHeight:kidHeight];
        
        CGFloat nodeHeight = _heightOfDetailCell;
        
        if (heightInfo && heightInfo.expanded && heightInfo.takenHeightHistory.count > 1){
            
            int nodeCount = heightInfo.takenHeightHistory.count;
            int extendCount = nodeCount - 1;
            CGFloat extendStickerHeight = (116+6)/2.0;
            nodeHeight = nodeHeight + (extendCount * extendStickerHeight); // - (nodeHeight/2.0); 가이드에 나온대로 한다면 터치가 안먹는 버그가 발생한다.
        }
        
        return CGSizeMake(self.bounds.size.width, nodeHeight);
    }
    else{
        
        return CGSizeMake(self.bounds.size.width, _heightOfZoomDetailCell);
        
    }
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.isZoomMode ? 22 : _currentShownMaxHeight;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellID;
    
    if (indexPath.row == 0){
        if (NO == self.isZoomMode){
            cellID = kDetailAddHeightCellID;
            AddDetailCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellID forIndexPath:indexPath];
            cell.addCellDelegate = self;
            [cell updateDeleteMode:self.isDeleteMode Animated:NO];
            return cell;
        }
        else{
            cellID = kDetailZoomAddHeightCellID;
            AddZoomDetailCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellID forIndexPath:indexPath];
            cell.addCellDelegate = self;
            [cell updateDeleteMode:self.isDeleteMode Animated:NO];
            return cell;
        }
    }
    
    if (!self.isZoomMode){
        cellID = (indexPath.row % 2) ? kDetailLeftCellID : kDetailRightCellID;
        HeightDetailCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellID forIndexPath:indexPath];
        [cell setAllDelegate:self];
        
        CGFloat kidHeight = [self kidHeightFromIndexRow:indexPath.row];
        
        KidHeightDetail *heightInfo = [self.dataSource heightDetailOfKidHeight:kidHeight];
        
        [cell updateWithHeightDetail:heightInfo kidHeight:kidHeight];
//        cell.isAverageLine = (kidHeight == self.aveHeight);
        cell.isAverageLine = ((int)(kidHeight) == ((int)self.aveHeight));

        if ([self.treeDelegate requestAge] <= 0) cell.isAverageLine = FALSE;
        if (self.lastAddedDetailInfo && (NSInteger)self.lastAddedDetailInfo.kidHeight == kidHeight){
            if ([cell doAddNewAnimationWithDetailInfo:self.lastAddedDetailInfo]){
                self.lastAddedDetailInfo = nil;
            }
        }
        
        cell.isDeleteMode = self.isDeleteMode;
        
        return cell;
    }
    else{
        cellID = kDetailZoomCellID;
        HeightZoomDetailCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellID forIndexPath:indexPath];
        [cell setAllDelegate:self];
        CGFloat kidHeight = [self kidHeightFromIndexRow:indexPath.row];
        NSMutableArray *kidHeightArray = [NSMutableArray array];
        for (int h = 0; h < 10; h++) {
            CGFloat availableHeight = kidHeight + h;
            KidHeightDetail *heightInfo = [self.dataSource heightDetailOfKidHeight:availableHeight];
            if (heightInfo != nil)
               [kidHeightArray addObject:heightInfo];
        }
        
        [cell updateHeightDetails:kidHeightArray kidHeight:kidHeight];
        
        if ([self.treeDelegate requestAge] <= 0) {
            [cell checkAverageLineWithHeight:-100];
        }
        else {
            [cell checkAverageLineWithHeight:self.aveHeight];
        }

        if (self.lastAddedDetailInfo && (self.lastAddedDetailInfo.kidHeight >= kidHeight && self.lastAddedDetailInfo.kidHeight < kidHeight+10)){
            if ([cell doAddNewAnimationWithDetailInfo:self.lastAddedDetailInfo]){
                self.lastAddedDetailInfo = nil;
            }
        }

//        [cell debug_setIndex:indexPath.row];
        
        return cell;
    }

}



-(void)longpressedSticker:(TopArrowSticker*)topSticker
{
//    topSticker.heightDetail.expanded = !topSticker.heightDetail.expanded;
//    
//    [_collectionViewLayout invalidateLayout];
//    
//    
//    [topSticker updateExpandWithAnimated:YES];
    
    if (self.isDeleteMode == NO && self.isZoomMode == NO){

        [self.treeDelegate longpressedWithHeight:topSticker.heightDetail.kidHeight];
        
        topSticker.detailInfo.isChecked = YES;
        [topSticker setStickerChecekd:YES];
    }
}

-(void)addHeight
{
    [self.treeDelegate addHeight];
}

//-(void)clickSticker:(KidHeightDetail*)kidHeight
//{
//    [self.treeDelegate clickTopArrowSticker:kidHeight];
//}

-(void)clickSticker:(id)sticker HeightDetail:(KidHeightDetail *)kidHeight clickedStickerInfo:(HeightDetailInfo *)detailInfo
{
    if (!self.isZoomMode){
        
        if (sticker && [sticker isKindOfClass:[TopArrowSticker class]] && kidHeight && kidHeight.takenHeightHistory.count > 1){
        
            if (kidHeight.expanded == NO){
                
                TopArrowSticker *topSticker = sticker;
                topSticker.heightDetail.expanded = !topSticker.heightDetail.expanded;
                [_collectionViewLayout invalidateLayout];
                [topSticker updateExpandWithAnimated:YES];
                
                return;
            }
        }
                
        [self.treeDelegate clickSticker:kidHeight stickerDetail:detailInfo];
        
        
    }
    else{ // ZoomMode
        //[self.treeDelegate clickSticker:kidHeight stickerDetail:detailInfo];
        
//        [self scrollToHeight:detailInfo.kidHeight];
        
        int h = detailInfo.kidHeight;
        int index = _currentShownMaxHeight - h;
        index = MIN(_currentShownMaxHeight, index);
        index = MAX(0, index);
        
        
        [self changeZoomMode:NO completion:^{
            [_detailCollectionView selectItemAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]
                                                animated:YES
                                          scrollPosition:UICollectionViewScrollPositionCenteredVertically];
        }];
    }
}

-(NSUInteger)monkeyNumber
{
    //return self.monkeyTypeNo;
    NSUInteger no = [self.dataSource characterNumber];
    return no;
}

-(BOOL)isDeleteModeRequest
{
    return self.isDeleteMode;
}

-(void)doubleTap:(HeightZoomDetailCell*)zoomCell OffSetY:(CGFloat)offSetY
{
    if (self.isZoomMode){
        int h = zoomCell.kidHeight + (int)(offSetY);
        int index = _currentShownMaxHeight - h;
        index = MIN(_currentShownMaxHeight, index);
        index = MAX(0, index);
        
        
        [self changeZoomMode:NO completion:^{
            [_detailCollectionView selectItemAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]
                                                animated:YES
                                          scrollPosition:UICollectionViewScrollPositionCenteredVertically];
        }];
    }
}

-(void)changeZoomMode:(BOOL)isZoomMode completion:(void(^)())completion
{
    _isZoomMode = isZoomMode;

    if (_isZoomMode){
        _treeCoverImageView.layer.opacity = 0;
    }
    
    NSMutableArray *adjustCellArray = [NSMutableArray arrayWithArray:_detailCollectionView.visibleCells];
    
    [UIView animateWithDuration:0.2 animations:^{
        
        for (UICollectionViewCell *cell in adjustCellArray) {
            if (_isZoomMode){
                cell.transform = CGAffineTransformMakeScale(0.1, 0.1);
            }
        }
        
        
        if (NO == _isZoomMode){
            [self chageBackgroundTreeImage];
            [self setupNomalDoleKey];
            _detailCollectionView.frame = [self frameCollectionView];
        }
        
        
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3 animations:^{
            
            if (_isZoomMode){
                [self chageBackgroundTreeImage];
                [self setupZoomModeDoleKey];
                _detailCollectionView.frame = [self frameCollectionView];
            }
            
            
            if (_isZoomMode){
                _collectionViewLayout.itemSize = CGSizeMake(self.bounds.size.width, _heightOfZoomDetailCell);
                _collectionViewLayout.sectionInset = UIEdgeInsetsMake(65, 0, 5, 0);
            }
            else{
                _collectionViewLayout.itemSize = CGSizeMake(self.bounds.size.width, _heightOfDetailCell);
                _collectionViewLayout.sectionInset = UIEdgeInsetsMake(47, 0, 0, 0);
            }
            [self reloadDetails];
        } completion:^(BOOL finished) {
            completion();
        }];
    }];
    
    
    return;
}

-(void)setIsZoomMode2:(BOOL)isZoomMode
{
    _isZoomMode = isZoomMode;
    
    if (_isZoomMode){
        _treeCoverImageView.layer.opacity = 0;
    }

    NSMutableArray *adjustCellArray = [NSMutableArray arrayWithArray:_detailCollectionView.visibleCells];

    CGFloat zoomScaleWidth = 20.0 / 46.0;
    CGFloat zoomScaleHeight = 40.0 / 116.0;
    
    [UIView animateWithDuration:3 animations:^{
        
        if (NO == _isZoomMode){
            [self chageBackgroundTreeImage];
            [self setupNomalDoleKey];
            _detailCollectionView.frame = [self frameCollectionView];
        }
        else{
            

        }
        
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:3 animations:^{

            if (_isZoomMode){
                
                for (UICollectionViewCell *cell in adjustCellArray) {
                    if (_isZoomMode){
                        cell.transform = CGAffineTransformMakeScale(zoomScaleWidth, zoomScaleHeight);
                        //cell.bounds = CGRectMake(0, 0, 10, 20);
                    }
                }
                
                [self chageBackgroundTreeImage];
                [self setupZoomModeDoleKey];
                _detailCollectionView.frame = [self frameCollectionView];

            }
            

            
        } completion:^(BOOL finished) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self reloadDetails];
                if (_isZoomMode){
                    _collectionViewLayout.itemSize = CGSizeMake(self.bounds.size.width, _heightOfZoomDetailCell);
                    _collectionViewLayout.sectionInset = UIEdgeInsetsMake(65, 0, 5, 0);
                }
                else{
                    _collectionViewLayout.itemSize = CGSizeMake(self.bounds.size.width, _heightOfDetailCell);
                    _collectionViewLayout.sectionInset = UIEdgeInsetsMake(47, 0, 0, 0);
                }
            });

        }];
    }];
 
}

-(void)setIsZoomMode:(BOOL)isZoomMode
{
    _isZoomMode = isZoomMode;
    
    //    [self chageBackgroundTreeImage];
    //
    //    if (_isZoomMode)
    //        [self setupZoomModeDoleKey];
    //    else
    //        [self setupNomalDoleKey];
    //    _detailCollectionView.frame = [self frameCollectionView];
    //    [self reloadDetails];
    
    if (_isZoomMode){
        _treeCoverImageView.layer.opacity = 0;
    }
    
    NSMutableArray *adjustCellArray = [NSMutableArray arrayWithArray:_detailCollectionView.visibleCells];
    
    [UIView animateWithDuration:0.15 animations:^{
        
        for (UICollectionViewCell *cell in adjustCellArray) {
            if (_isZoomMode){
                //                cell.layer.opacity = 0;
                cell.transform = CGAffineTransformMakeScale(0.1, 0.1);
                
            }
        }
        
        
        if (NO == _isZoomMode){
            [self chageBackgroundTreeImage];
            [self setupNomalDoleKey];
            _detailCollectionView.frame = [self frameCollectionView];
        }
        
        
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3 animations:^{
            
            
            
            if (_isZoomMode){
                [self chageBackgroundTreeImage];
                [self setupZoomModeDoleKey];
                _detailCollectionView.frame = [self frameCollectionView];
            }
            
            
            if (_isZoomMode){
                _collectionViewLayout.itemSize = CGSizeMake(self.bounds.size.width, _heightOfZoomDetailCell);
                _collectionViewLayout.sectionInset = UIEdgeInsetsMake(65, 0, 5, 0);
            }
            else{
                _collectionViewLayout.itemSize = CGSizeMake(self.bounds.size.width, _heightOfDetailCell);
                _collectionViewLayout.sectionInset = UIEdgeInsetsMake(47, 0, 0, 0);
            }
            [self reloadDetails];
        } completion:^(BOOL finished) {
            //            if (_isZoomMode){
            //                _collectionViewLayout.itemSize = CGSizeMake(self.bounds.size.width, _heightOfZoomDetailCell);
            //                _collectionViewLayout.sectionInset = UIEdgeInsetsMake(65, 0, 5, 0);
            //            }
            //            else{
            //                _collectionViewLayout.itemSize = CGSizeMake(self.bounds.size.width, _heightOfDetailCell);
            //                _collectionViewLayout.sectionInset = UIEdgeInsetsMake(47, 0, 0, 0);
            //            }
            for (UICollectionViewCell *cell in adjustCellArray) {
                if (_isZoomMode){
                    //                cell.layer.opacity = 0;
                    cell.transform = CGAffineTransformMakeScale(1, 1);
                    
                }
            }
        }];
    }];
    
    
    return;
    
    
    
    if (_isZoomMode){
        _collectionViewLayout.itemSize = CGSizeMake(self.bounds.size.width, _heightOfZoomDetailCell);
        _collectionViewLayout.sectionInset = UIEdgeInsetsMake(65, 0, 5, 0);
    }
    else{
        _collectionViewLayout.itemSize = CGSizeMake(self.bounds.size.width, _heightOfDetailCell);
        _collectionViewLayout.sectionInset = UIEdgeInsetsMake(47, 0, 0, 0);
    }
    
    
    
    [UIView animateWithDuration:0.5
                     animations:^{
                         [self chageBackgroundTreeImage];
                         
                         if (_isZoomMode){
                             
                             //                             for (UICollectionViewCell *cell in adjustCellArray) {
                             //                                 cell.transform = CGAffineTransformMakeScale(0.1, 0.1);
                             //                             }
                             
                             [self setupZoomModeDoleKey];
                             
                             //                             for (UICollectionViewCell *cell in _detailCollectionView.visibleCells) {
                             //                                     cell.transform = CGAffineTransformMakeScale(0.1, 0.1);
                             //                             }
                             
                         }
                         else
                             [self setupNomalDoleKey];
                         
                         
                         _detailCollectionView.frame = [self frameCollectionView];
                         
                         
                         
                     }
                     completion:^(BOOL finished) {
                         
                         
                         
                         if (NO == _isZoomMode) _treeCoverImageView.layer.opacity = 1;
                         
                         if (_isZoomMode){
                             for (UICollectionViewCell *cell in adjustCellArray) {
                                 cell.transform = CGAffineTransformMakeScale(1, 1);
                                 //                                 cell.layer.opacity = 1;
                             }
                         }
                         
                         //                         if (_isZoomMode){
                         //                             _collectionViewLayout.itemSize = CGSizeMake(self.bounds.size.width, _heightOfZoomDetailCell);
                         //                             _collectionViewLayout.sectionInset = UIEdgeInsetsMake(65, 0, 5, 0);
                         //                         }
                         //                         else{
                         //                             _collectionViewLayout.itemSize = CGSizeMake(self.bounds.size.width, _heightOfDetailCell);
                         //                             _collectionViewLayout.sectionInset = UIEdgeInsetsMake(47, 0, 0, 0);
                         //                         }
                         
                         
                         
                         [self reloadDetails];
                         //[_collectionViewLayout invalidateLayout];
                         
                         
                         
                     }];
}

- (void)scrollToHeight:(CGFloat)kidHeight
{
    NSInteger index = _currentShownMaxHeight - kidHeight + 1;
    [_detailCollectionView selectItemAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]
                                        animated:YES
                                  scrollPosition:UICollectionViewScrollPositionCenteredVertically];
}

- (void)scrollSlowlyFromIndex:(NSInteger)fromIndex To:(NSInteger)toIndex
{
    _scrollIndexByTimer = toIndex;
    _scrollIndexCurrent = fromIndex;
    CGFloat interval = 1.0 / fabsf(_scrollIndexByTimer - _scrollIndexCurrent);
    _scrollTimer = [NSTimer scheduledTimerWithTimeInterval:interval target:self selector:@selector(scrollSlowlyToPoint) userInfo:nil repeats:YES];
}


- (void)scrollSlowlyToPoint {
    
    if (_scrollIndexCurrent == _scrollIndexByTimer)
    {
        [_scrollTimer invalidate];
        return;
    }
    
    if (_scrollIndexByTimer < _scrollIndexCurrent)
        _scrollIndexCurrent--;
    else
        _scrollIndexCurrent++;
    
    
    
    [_detailCollectionView selectItemAtIndexPath:[NSIndexPath indexPathForRow:_scrollIndexCurrent inSection:0] animated:YES scrollPosition:UICollectionViewScrollPositionCenteredVertically];
}

- (void)setIsDeleteMode:(BOOL)isDeleteMode
{
    _isDeleteMode = isDeleteMode;
    
    if (!self.isZoomMode){
        AddDetailCell *cell = (AddDetailCell*)[_detailCollectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        [cell updateDeleteMode:_isDeleteMode Animated:YES];
        
        for (UICollectionViewCell *dc in _detailCollectionView.visibleCells) {
            if ([dc isKindOfClass:[HeightDetailCell class]] == YES){
                HeightDetailCell *hcell = (HeightDetailCell*)dc;
                hcell.isDeleteMode = isDeleteMode;
            }
        }
    }
    else{
        AddZoomDetailCell *cell = (AddZoomDetailCell*)[_detailCollectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        [cell updateDeleteMode:_isDeleteMode Animated:YES];
    }
}

-(void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
{
    [self collapsedTopSticker];
}
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self collapsedTopSticker];
}

-(void)collapsedTopSticker
{
    if (self.isZoomMode)return;
    if (self.isDeleteMode)return;
    
    for (UICollectionViewCell *cell in _detailCollectionView.visibleCells) {
        if ([cell isKindOfClass:[HeightDetailCell class]] == YES){
            HeightDetailCell *hdc = (HeightDetailCell*)cell;
            TopArrowSticker *topSticker = hdc.topArrowSticker;
            if (topSticker.heightDetail.expanded){
                topSticker.heightDetail.expanded = NO;
                [topSticker updateExpandWithAnimated:YES];
                
                [_collectionViewLayout invalidateLayout];
            }
        }
    }
}

-(NSInteger)requestAge
{
    return [self.treeDelegate requestAge];
}

@end
