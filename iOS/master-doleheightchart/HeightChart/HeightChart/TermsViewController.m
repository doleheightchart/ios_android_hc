//
//  TermsViewController.m
//  HeightChart
//
//  Created by ne on 2014. 3. 10..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "TermsViewController.h"
#import "UIView+ImageUtil.h"
#import "UIViewController+DoleHeightChart.h"
#import "UIButton+CheckAndRadio.h"
#import "UIFont+DoleHeightChart.h"
#import "UIColor+ColorUtil.h"
#import "EditFacebookViewController.h"

@interface TermsViewController ()
@property (weak, nonatomic) IBOutlet UITextView *txtViewTop;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckAgreeTop;
@property (weak, nonatomic) IBOutlet UILabel *lblAgreeTop;
@property (weak, nonatomic) IBOutlet UITextView *txtViewBottom;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckAgreeBottom;
@property (weak, nonatomic) IBOutlet UILabel *lblAgreebottom;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;
- (IBAction)clickCancel:(id)sender;
- (IBAction)clickNext:(id)sender;

@end

@implementation TermsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.txtViewTop.font = [UIFont defaultRegularFontWithSize:22/2];
    self.txtViewTop.textColor = [UIColor colorWithRGB:0x606060];
    self.txtViewBottom.font = [UIFont defaultRegularFontWithSize:22/2];
    self.txtViewBottom.textColor = [UIColor colorWithRGB:0x606060];
    

    //언어별 설정에서 국가 설정으로 파일 읽어 오는 부분을 변경한다.
//    NSString *terms = NSLocalizedString(@"TermsndConditions", @"");
//    NSString *privacy = NSLocalizedString(@"PrivacyPolicy", @"");
    
//    NSString *terms = @"PrivacyPolicy_en";
//    NSString *privacy = @"TermsndConditions_en";
    
    NSString *ppFileName = @"PrivacyPolicy_en";
    NSString *tcFileName = @"TermsndConditions_en";
    
    if ([self.currentCountryCode isEqualToString:@"KR"]) {
        ppFileName = @"PrivacyPolicy_kor";
        tcFileName = @"TermsndConditions_kor";
    }
    else if([self.currentCountryCode isEqualToString:@"JP"]) {
        ppFileName = @"PrivacyPolicy_jp";
        tcFileName = @"TermsndConditions_jp";
        
    }
    else if([self.currentCountryCode isEqualToString:@"NZ"]) {
        ppFileName = @"PrivacyPolicy_nz";
    }
    else if([self.currentCountryCode isEqualToString:@"SG"]){
        ppFileName = @"PrivacyPolicy_SG";
        tcFileName = @"TermsndConditions_SG";
    }

    
    if (NSFoundationVersionNumber <= NSFoundationVersionNumber_iOS_6_1){
        
//        self.txtViewTop.text = [self loadRTFPlainTextResource:terms];
//        self.txtViewBottom.text = [self loadRTFPlainTextResource:privacy];
        
        [self fakeLoadRtfResource:ppFileName HostView:self.txtViewTop];
        [self fakeLoadRtfResource:tcFileName HostView:self.txtViewBottom];
    }
    else{
        // iOS7
        self.txtViewTop.textContainer.lineFragmentPadding = 20/2;
        self.txtViewBottom.textContainer.lineFragmentPadding = 20/2;
        
//        self.txtViewTop.attributedText = [self loadRTFResource:ppFileName];
//        self.txtViewBottom.attributedText = [self loadRTFResource:tcFileName];
        self.txtViewTop.attributedText = [self loadRTFResource:tcFileName];
        self.txtViewBottom.attributedText = [self loadRTFResource:ppFileName];

    }

    
    self.txtViewTop.layer.cornerRadius = 3;
    self.txtViewTop.layer.masksToBounds = YES;
    
    self.txtViewBottom.layer.cornerRadius = 3;
    self.txtViewBottom.layer.masksToBounds = YES;
    
    if (self.isJustShowMode) {
        [self decorateAndAdjustUIJustShow];
        return;
    }
	// Do any additional setup after loading the view.
    [self decorateUI];
//    [self addCommonCloseButton];
    [self setupControlFlexiblePosition];
    

}

-(void)decorateUI
{
    self.lblAgreeTop.font = [UIFont defaultRegularFontWithSize:24/2];
    self.lblAgreeTop.textColor = [UIColor colorWithRGB:0x606060];
    
    self.lblAgreebottom.font = [UIFont defaultRegularFontWithSize:24/2];
    self.lblAgreebottom.textColor = [UIColor colorWithRGB:0x606060];
    
    [self.btnCancel setResizableImageWithType:DoleButtonImageTypeOrange];
    [self.btnNext setResizableImageWithType:DoleButtonImageTypeRed];
    
    
    [self.btnCheckAgreeBottom makeUICheckBox];
    [self.btnCheckAgreeTop makeUICheckBox];
    
    self.btnNext.enabled = NO;
    
    [self.btnCheckAgreeTop addTarget:self action:@selector(onCheckUP:) forControlEvents:UIControlEventTouchUpInside];
    [self.btnCheckAgreeBottom addTarget:self action:@selector(onCheckBottom:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)decorateAndAdjustUIJustShow
{
    
    //라벨 만들기
//    UILabel *termsLabel = [UILabel commonBoldLabel];
//    termsLabel.text = NSLocalizedString(@"Terms of Service", @"");
//    
//    UILabel *privacy = [UILabel commonBoldLabel];
//    privacy.text = NSLocalizedString(@"Privacy Policy", @"");
    
//    [self.view addSubview:termsLabel];
//    [self.view addSubview:privacy];
    
    
    [self.lblAgreeTop setHidden:YES];
    [self.lblAgreebottom setHidden:YES];
    [self.btnCancel setHidden:YES];
    [self.btnNext setHidden:YES];
    [self.btnCheckAgreeTop setHidden:YES];
    [self.btnCheckAgreeBottom setHidden:YES];
    
    [self addCommonCloseButton];
    
    CGFloat DefalutMargin = 112;
    CGFloat txtViewHeight = 428;
    
    if ([UIScreen isFourInchiScreen]) {
        
       
        
        [self moveToPositionYPixel:(DefalutMargin) Control:self.txtViewTop];
        [self moveToPositionYPixel:(DefalutMargin + txtViewHeight + 56) Control:self.txtViewBottom];
        
        [self changeToHeightPixel:txtViewHeight Control:self.txtViewTop];
        [self changeToHeightPixel:txtViewHeight Control:self.txtViewBottom];
        
    }
    else{
        
        CGFloat height = self.view.bounds.size.height;
        
        txtViewHeight = (height*2 - 112 - 50 - 70)/2;
        
        
        [self moveToPositionYPixel:(DefalutMargin) Control:self.txtViewTop];
        [self moveToPositionYPixel:(DefalutMargin + txtViewHeight + 50) Control:self.txtViewBottom];
        [self changeToHeightPixel:txtViewHeight Control:self.txtViewTop];
        [self changeToHeightPixel:txtViewHeight Control:self.txtViewBottom];
        

        
    }
    
//    CGRect recttermsLabel = termsLabel.frame;
//    recttermsLabel.origin.x = 56/2;
//    recttermsLabel.origin.y = DefalutMargin/2 - termsLabel.frame.size.height*1.2;
//    termsLabel.frame = recttermsLabel;
//    
//    
//    CGRect rectprivacy = privacy.frame;
//    rectprivacy.origin.x = 56/2;
//    rectprivacy.origin.y = self.txtViewBottom.frame.origin.y - rectprivacy.size.height * 1.2;
//    privacy.frame = rectprivacy;

    
}

-(void)setupControlFlexiblePosition
{
    CGFloat DefalutMargin = 54;
    CGFloat txtViewHeight = 361;
    
    if ([UIScreen isFourInchiScreen]) {
        [self moveToPositionYPixel:(DefalutMargin) Control:self.txtViewTop];
        [self moveToPositionYPixel:(DefalutMargin + txtViewHeight + 16) Control:self.btnCheckAgreeTop];
        [self moveToPositionYPixel:(DefalutMargin + txtViewHeight + 16) Control:self.lblAgreeTop];
        [self moveToPositionYPixel:(DefalutMargin + txtViewHeight + 16 + 51 + 41) Control:self.txtViewBottom];
        [self moveToPositionYPixel:(DefalutMargin + txtViewHeight + 16 + 51 + 41 + txtViewHeight + 16) Control:self.btnCheckAgreeBottom];
        [self moveToPositionYPixel:(DefalutMargin + txtViewHeight + 16 + 51 + 41 + txtViewHeight + 16) Control:self.lblAgreebottom];

        [self moveToPositionYPixel:(DefalutMargin + txtViewHeight + 16 + 51 + 41 + txtViewHeight + 16 + 51 + 44) Control:self.btnCancel];
        [self moveToPositionYPixel:(DefalutMargin + txtViewHeight + 16 + 51 + 41 + txtViewHeight + 16 + 51 + 44) Control:self.btnNext];
        
    }
    else{
        txtViewHeight = 361 - 88;
        [self changeToHeightPixel:(361-88) Control:self.txtViewTop];
        [self changeToHeightPixel:(361-88) Control:self.txtViewBottom];
        
        [self moveToPositionYPixel:(DefalutMargin) Control:self.txtViewTop];
        [self moveToPositionYPixel:(DefalutMargin + txtViewHeight + 16) Control:self.btnCheckAgreeTop];
        [self moveToPositionYPixel:(DefalutMargin + txtViewHeight + 16) Control:self.lblAgreeTop];
        [self moveToPositionYPixel:(DefalutMargin + txtViewHeight + 16 + 51 + 41) Control:self.txtViewBottom];
        [self moveToPositionYPixel:(DefalutMargin + txtViewHeight + 16 + 51 + 41 + txtViewHeight + 16) Control:self.btnCheckAgreeBottom];
        [self moveToPositionYPixel:(DefalutMargin + txtViewHeight + 16 + 51 + 41 + txtViewHeight + 16) Control:self.lblAgreebottom];
        
        [self moveToPositionYPixel:(DefalutMargin + txtViewHeight + 16 + 51 + 41 + txtViewHeight + 16 + 51 + 44) Control:self.btnCancel];
        [self moveToPositionYPixel:(DefalutMargin + txtViewHeight + 16 + 51 + 41 + txtViewHeight + 16 + 51 + 44) Control:self.btnNext];
        
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clickCancel:(id)sender {
    if (NO == self.isFacebookSignUpMode){
        self.isAgree = NO;
        [self closeViewControllerAnimated:NO];
        self.completion(self);
    }
    else{
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

- (IBAction)clickNext:(id)sender {
    if (NO == self.isFacebookSignUpMode){
        self.isAgree = YES;
        [self closeViewControllerAnimated:NO];
        self.completion(self);
    }
    else{
        [self showSignUpFacebookUser];
    }
}

- (IBAction)onCheckUP:(id)sender {
    
    BOOL isTopAgree = self.btnCheckAgreeTop.selected;
    BOOL isBottomAgree = self.btnCheckAgreeBottom.selected;

    if (isTopAgree && isBottomAgree) {
        self.btnNext.enabled = YES;
    }
    else{
        self.btnNext.enabled = NO;

    }
}

- (IBAction)onCheckBottom:(id)sender {
    BOOL isTopAgree = self.btnCheckAgreeTop.selected;
    BOOL isBottomAgree = self.btnCheckAgreeBottom.selected;
    
    if (isTopAgree && isBottomAgree) {
        self.btnNext.enabled = YES;
    }
    else{
        self.btnNext.enabled = NO;
        
    }
}

-(void)showSignUpFacebookUser
{
//    NSCalendar *calendar = [[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];
//    NSDateComponents *comp = [calendar components:NSCalendarUnitYear fromDate:[NSDate date]];
//    NSInteger year = [comp year];
    
    
    NSString *city = @"";
    
    enum GenderType gender = kGenderTypeMale;
    
    if (self.facebookManager.address){
        //주소파싱
    }
    
    if (self.facebookManager.gender){
        if ([self.facebookManager.gender isEqualToString:@"male"])
            gender = kGenderTypeMale;
        else
            gender = kGenderTypeFemale;
    }
    
    NSInteger year = -1;
    if (self.facebookManager.birthday){
        NSDateFormatter *formater = [[NSDateFormatter alloc] init];
        NSDate *bd = [formater dateFromString:self.facebookManager.birthday];
        
        if (bd){
            NSCalendar *calendar = [[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];
            NSDateComponents *comp = [calendar components:NSCalendarUnitYear fromDate:bd];
            year = [comp year];
        }
    }
    
    
    EditFacebookViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"EditFacebooklUser"];
    vc.isRegisterMode = YES;
    vc.facebookID = self.facebookManager.facebookID;
    vc.facebookName = self.facebookManager.facebookName;
    vc.facebookEmail = self.facebookManager.emailAddress;
    vc.birthYear = year;
    vc.city = city;
    vc.genderType = gender;

    vc.appFirstTimeLogIn = self.appFirstTimeLogIn;
    vc.requireLoginCompletion = self.requireLoginCompletion;
    
    
    [self.navigationController pushViewController:vc animated:NO];
}

-(NSAttributedString*)loadRTFResource:(NSString*)resourceName
{
    NSString *path = [[NSBundle mainBundle] pathForResource:resourceName ofType:@"rtf"];
    NSURL *url = [NSURL fileURLWithPath:path];
    NSError *error;
    NSDictionary *options = @{};
    NSDictionary *attributes;
    NSAttributedString *text = [[NSAttributedString alloc]initWithFileURL:url
                                                                  options:options
                                                       documentAttributes:&attributes
                                                                    error:&error];
    
    NSLog(@"%@", text);
    
    return text;
}

-(NSAttributedString*)loadRTFResourceLowVersion:(NSString*)resourceName
{
    NSString *path = [[NSBundle mainBundle] pathForResource:resourceName ofType:@"rtf"];
    NSURL *url = [NSURL fileURLWithPath:path];
    NSError *error;
    NSDictionary *options = @{};
    NSDictionary *attributes;
    
    
    
//    NSAttributedString *text = [[NSAttributedString alloc]initWithFileURL:url
//                                                                  options:options
//                                                       documentAttributes:&attributes
//                                                                    error:&error];
    NSString *plaintext = [[NSString alloc]initWithContentsOfFile:path encoding:NSUTF8StringEncoding error:&error];
//    NSAttributedString *text = [[NSAttributedString alloc] initWithString:plaintext];
    
    //NSAttributedString *text = [[NSAttributedString alloc] initWithString:plaintext];
    
    NSData *data = [NSData dataWithContentsOfFile:path ];
    NSAttributedString *text = [[NSAttributedString alloc] initWithData:data options:0 documentAttributes:0 error:&error];
    
    
    NSLog(@"%@", text);
    
    return text;
}


-(NSString*)loadRTFPlainTextResource:(NSString*)resourceName
{
    NSString *path = [[NSBundle mainBundle] pathForResource:resourceName ofType:@"rtf"];
    NSURL *url = [NSURL fileURLWithPath:path];
    NSError *error;
    NSDictionary *options = @{};
    NSDictionary *attributes;
//    NSAttributedString *text = [[NSAttributedString alloc]initWithFileURL:url
//                                                                  options:options
//                                                       documentAttributes:&attributes
//                                                                    error:&error];
    NSString *text = [[NSString alloc]initWithContentsOfFile:path encoding:NSASCIIStringEncoding error:&error];
    
    NSLog(@"%@", text);
    
    return text;
}

-(void)fakeLoadRtfResource:(NSString*)resourceName HostView:(UIView*)hostView
{
    UIWebView *fakeWebview = [[UIWebView alloc]initWithFrame:hostView.frame];
    //[hostView addSubview:fakeWebview];
    [hostView.superview addSubview:fakeWebview];
    hostView.hidden = YES;
    
    NSString *path = [[NSBundle mainBundle] pathForResource:resourceName ofType:@"rtf"];
    NSURL *url = [NSURL fileURLWithPath:path];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    fakeWebview.scalesPageToFit = YES;
    [fakeWebview loadRequest:request];
}

@end
