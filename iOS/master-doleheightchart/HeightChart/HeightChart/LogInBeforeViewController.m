//
//  LogInBeforeViewController.m
//  HeightChart
//
//  Created by ne on 2014. 3. 10..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "LogInBeforeViewController.h"
#import "UIView+ImageUtil.h"
#import "TextViewDualPlaceHolder.h"
#import "UIColor+ColorUtil.h"
#import "UIFont+DoleHeightChart.h"
#import "UIViewController+DoleHeightChart.h"
#import "OnOffSwitch.h"
#import "PaperNewzealandViewController.h"
#import "UIViewController+PolyServer.h"

@interface LogInBeforeViewController ()
{
    UIView * _newzelandpaperBanner;
}
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
@property (weak, nonatomic) IBOutlet UIButton *btnInviteFacebook;
@property (weak, nonatomic) IBOutlet UIButton *btnAboutService;
@property (weak, nonatomic) IBOutlet UIButton *btnHelp;
@property (weak, nonatomic) IBOutlet UILabel *txtOther;
@property (weak, nonatomic) IBOutlet UILabel *txtDescript;
@property (weak, nonatomic) IBOutlet UIImageView *bgSky;
@property (weak, nonatomic) IBOutlet UIImageView *iconOther;
@property (weak, nonatomic) IBOutlet OnOffSwitch *musicOnOff;
@property (weak, nonatomic) IBOutlet UILabel *labelBackmusicOnOff;

@end

@implementation LogInBeforeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self decorateUI];

    [self addCommonCloseButton];

    [self setupControlFlexiblePosition];

    self.musicOnOff.isCheckedOn = self.userConfig.backgroundMusicOn;
    [self.musicOnOff addTarget:self action:@selector(backgroundOnOffChanged:) forControlEvents:UIControlEventValueChanged];
    
    
    [self makeNZPaper];
    
    


    
    

}


-(void)showMoveUpNewzelandBanner
{
    if ([self.currentCountryCode isEqualToString:@"NZ"]) {
        
        if (!self.userConfig.isHeightChartPaparRecievedShowBanner) {
            
            [UIView animateWithDuration:1.0 animations:^{
                
                CGRect rect = _newzelandpaperBanner.frame;
                rect.origin.y = self.view.bounds.size.height - (72 + 70)/2;
                
                _newzelandpaperBanner.frame = rect;
                
            } completion:^(BOOL finished){
            }];
        }
    }

}

-(void)viewWillAppear:(BOOL)animated
{
    if (self.isEnableNetwork){
        
        [self showMoveUpNewzelandBanner];
    }
    else{
//        self.btnLogin.enabled = NO;
//        self.btnInviteFacebook.enabled = NO;
//        self.btnHelp.enabled = NO;
//        
//        [self toastNetworkError];
    }
}


-(void)makeNZPaper
{
    
    _newzelandpaperBanner = [[UIView alloc]initWithFrame:CGRectMake((self.view.bounds.size.width - 500/2)/2,
//                                                                    self.view.bounds.size.height - (72 + 70)/2,
                                                                    self.view.bounds.size.height + 100,
                                                                    500/2, 72/2)];
    
    _newzelandpaperBanner.backgroundColor = [UIColor colorWithRGB:0x606060];
    _newzelandpaperBanner.layer.opacity = 0.85;
    _newzelandpaperBanner.layer.cornerRadius = 2;
    _newzelandpaperBanner.layer.masksToBounds = YES;
    
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(22/2, 18/2, 400/2, 34/2)];
    label.backgroundColor = [UIColor clearColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont defaultRegularFontWithSize:28/2];
    label.textColor = [UIColor colorWithRGB:0xffffff];
    label.text = @"Receive the Height Chart Paper";
    [_newzelandpaperBanner addSubview:label];
    
    UIButton *closebtn = [ UIButton buttonWithType:UIButtonTypeCustom];
    [closebtn setImage:[UIImage imageNamed:@"popup_banner_x_btn_normal"] forState:UIControlStateNormal];
    [closebtn setImage:[UIImage imageNamed:@"popup_banner_x_btn_press"] forState:UIControlStateHighlighted];
    [closebtn addTarget:self action:@selector(closeBanner) forControlEvents:UIControlEventTouchUpInside];
    closebtn.frame = CGRectMake(_newzelandpaperBanner.frame.size.width - (45 + 14)/2, 14/2, 45/2, 45/2);
    [_newzelandpaperBanner addSubview:closebtn];
    
    [self.view addSubview:_newzelandpaperBanner] ;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(showPaperView)];
    [_newzelandpaperBanner addGestureRecognizer:tap];
    
    
    
}

-(void)showPaperView
{
    PaperNewzealandViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"paperNewzealand"];
    
    UINavigationController *navi = [[UINavigationController alloc]initWithRootViewController:vc];
    navi.navigationBarHidden = YES;
    
    [self modalTransitionController:navi Animated:YES];
    [_newzelandpaperBanner removeFromSuperview];
    
}

-(void)closeBanner
{

    
    
    [UIView animateWithDuration:1.0 animations:^{
        
        CGRect rect = _newzelandpaperBanner.frame;
        rect.origin.y = self.view.bounds.size.height + 150;
        
        _newzelandpaperBanner.frame = rect;
        
    } completion:^(BOOL finished){
        self.userConfig.isHeightChartPaparRecievedShowBanner = YES;
        [_newzelandpaperBanner removeFromSuperview];
        
    }
     ];
}

-(void)decorateUI
{
    [self.btnLogin setResizableImageWithType:DoleButtonImageTypeOrange];
    [self.btnInviteFacebook setResizableImageWithType:DoleButtonImageTypeFacebook];
    [self.btnAboutService setResizableImageWithType:DoleButtonImageTypeLightBlue];
    [self.btnHelp setResizableImageWithType:DoleButtonImageTypeLightBlue];

    self.txtDescript.font = [UIFont defaultRegularFontWithSize:26/2];
    self.txtDescript.textColor = [UIColor colorWithRGB:0x606060];
    
    self.txtOther.font = [UIFont defaultBoldFontWithSize:30/2];
    self.labelBackmusicOnOff.font = [UIFont defaultBoldFontWithSize:30/2];
    
    
    self.labelBackmusicOnOff.font = [UIFont defaultBoldFontWithSize:30/2];
    self.labelBackmusicOnOff.textColor = [UIColor colorWithRGB:0xff8516];
    
    self.txtOther.font = [UIFont defaultBoldFontWithSize:30/2];
    self.txtOther.textColor = [UIColor colorWithRGB:0xff8516];
}

-(void)setupControlFlexiblePosition
{
    if ([UIScreen isFourInchiScreen]) {
        
        [self addTitleByBigSizeImage:YES Visible:YES];
        
        [self moveToPositionYPixel:(110 + 168 + 96) Control:self.txtDescript];
        [self moveToPositionYPixel:(110 + 168 + 96 + 66 + 32) Control:self.btnLogin];
        [self changeToHeightPixel:652 Control:self.bgSky];
        
    }
    else{
        
        [self addTitleByBigSizeImage:NO Visible:YES];
        
        [self moveToPositionYPixel:(254) Control:self.txtDescript];
        [self moveToPositionYPixel:(254 + 66 + 32) Control:self.btnLogin];
        [self changeToHeightPixel: (254+ 66 + 32 + 72 + 52) Control:self.bgSky];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)backgroundOnOffChanged:(OnOffSwitch*)sender
{
//    self.userConfig.backgroundMusicOn = sender.isCheckedOn;
    
    BOOL playBackMusic = sender.isCheckedOn;
    
    if (playBackMusic) {
        [self playMusic];
    }
    else {
        [self stopMusic];
    }
    
    self.userConfig.backgroundMusicOn = playBackMusic;
    
}

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{

    NSArray *checkSegueArray = @[@"segueInvite", @"segueHelp", @"segueLogin"];
    if ([checkSegueArray indexOfObject:identifier] != NSNotFound){
        if ([self enableActionWithCurrentNetwork] == NO){
            return NO;
        }
    }
    
    return YES;
}

@end
