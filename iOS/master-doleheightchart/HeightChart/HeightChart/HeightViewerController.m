//
//  HeightViewerController.m
//  HeightChart
//
//  Created by ne on 2014. 3. 12..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "HeightViewerController.h"
#import "HeightViewerCollectionCell.h"
#import "ChildHeight.h"
#import "DoleInfo.h"
#import "UIView+Animation.h"
#import "UIView+ImageUtil.h"
#import "UIViewController+DoleHeightChart.h"
#import "ToastController.h"
#import "ConfirmPopupController.h"
#import "MyChild.h"
#import "UIColor+ColorUtil.h"
#import "NSDate+DoleHeightChart.h"
#import "UIFont+DoleHeightChart.h"
#import "SelectShareAppPopupController.h"
#import "HeightSummaryViewController.h"
#import "DoleCoinPopupViewController.h"
#import "UIViewController+PolyServer.h"
#import "MessageBoxController.h"
#import "NSString+EtcUtil.h"

#import "Mixpanel.h"

@interface HeightViewerFlowLayout : UICollectionViewFlowLayout
@property (nonatomic) NSInteger nowPage;
@end

@implementation HeightViewerFlowLayout

-(CGPoint)targetContentOffsetForProposedContentOffset:(CGPoint)proposedContentOffset withScrollingVelocity:(CGPoint)velocity
{
    
    CGFloat contentOffsetX =  self.collectionView.contentOffset.x;
    NSInteger nearbyPageIndex = roundf(contentOffsetX / self.itemSize.width );
    //    NSInteger nearbyMinPageIndex = (contentOffsetX / self.itemSize.width);
    
    
    if (velocity.x > 0) {
        NSLog(@" 가속도 마이너스  ");
        nearbyPageIndex = roundf(contentOffsetX / self.itemSize.width + 0.3);
    }
    else if (velocity.x < 0)
    {
        nearbyPageIndex = roundf(contentOffsetX / self.itemSize.width - 0.3);
    }
    NSLog(@" 가속도 %f ", velocity.x);
    
    self.nowPage = nearbyPageIndex;
    CGFloat moveToX = nearbyPageIndex * self.itemSize.width;
    
    return CGPointMake(moveToX, proposedContentOffset.y);
    
}

@end

@interface HeightViewerController ()
{
    UICollectionView            *_heightViewerCollectionView;
    UICollectionViewFlowLayout  *_collectionViewLayout;
    
    NSMutableArray              *_dataArray;
    
    UIButton *_btnDelete;
    UIButton *_btnGraph ;
    UIButton *_btnShare;
    
    UIImageView *_labelDate_bg; //height viewer_dates_bg
    UILabel * _labelDate;
    
    
    UIImageView *_imageViewSticker;
    
    NSInteger _nowPageIndex;
    NSInteger _adjustBeforePageIndex;
    NSInteger _deletePageIndex;
    
    UIButton    *_closeButton;
    
    
    
    //Server
    NSString    *_lastOrderID;
    NSMutableData   *_recevedBufferData;
}

//@property (nonatomic) CGFloat height;
//@property (nonatomic) enum DoleMonkeyType monkeyType;
//@property (nonatomic) enum DoleStickerType stickerType;
//@property (nonatomic , retain) NSString * myname;height
//@property (nonatomic, retain) NSDate *saveDate;height

@property (nonatomic) BOOL isVisibleToolBar;
@end

@implementation HeightViewerController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initCollectionView];
    
    
    [self loadAnimationStickerView];
    [self loadToolbar];
    [self loadHeightInfoLabel];
    
    _nowPageIndex = self.startIndex;
    _adjustBeforePageIndex = self.startIndex;
    
    
    
    [_heightViewerCollectionView selectItemAtIndexPath:[NSIndexPath indexPathForRow:self.startIndex inSection:0]
                                              animated:NO
                                        scrollPosition:UICollectionViewScrollPositionLeft];
    
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                    action:@selector(tapView:)];
    [_heightViewerCollectionView addGestureRecognizer:tapRecognizer];
    
    //최초에 한번
    [self setIsVisibleToolBar:YES];
    [_imageViewSticker setHidden:NO];
    
    //    self.facebookManager.delegate = self;
    
    
    if (self.isConfirmMode) {
        
        [self confirmMode];
        
    }
    else{
        
        _closeButton = [self addCameraCloseButton];
    }
    
    
}

-(void)confirmClose:(id)sender
{
    
    
    self.completion(YES , self);
    
    
}

-(void) confirmMode
{
    _closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *noraml = [UIImage imageNamed:@"confirm_btn_normal"];
    UIImage *press = [UIImage imageNamed:@"confirm_btn_press"];
    
    [_closeButton setBackgroundImage:noraml forState:UIControlStateNormal];
    [_closeButton setBackgroundImage:press forState:UIControlStateSelected];
    [_closeButton setBackgroundImage:press forState:UIControlStateHighlighted];
    
    _closeButton.frame = CGRectMake(self.view.bounds.size.width - (102)/2,
                                    0,
                                    102/2,
                                    96/2);
    
    [_closeButton addTarget:self action:@selector(confirmClose:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_closeButton];
    
    _heightViewerCollectionView.scrollEnabled = false;
    
}


-(void)tapView:(UITapGestureRecognizer*)recognizer
{
    if (recognizer.state == UIGestureRecognizerStateEnded) {
        self.isVisibleToolBar = !self.isVisibleToolBar;
    }
    else if (recognizer.state == UIGestureRecognizerStateBegan){
    }
}

-(void)setIsVisibleToolBar:(BOOL)isVisibleToolBar
{
    _isVisibleToolBar = isVisibleToolBar;
    
    CGFloat opacity = _isVisibleToolBar ? 1 : 0;
    
    [UIView animateWithDuration:0.3 animations:^{
        _btnDelete.layer.opacity = opacity;
        _btnGraph.layer.opacity = opacity;
        _btnShare.layer.opacity = opacity;
        _closeButton.layer.opacity = opacity;
        _labelDate.layer.opacity = opacity;
    }];
    
}

- (void)initCollectionView
{
    self.view.backgroundColor = [UIColor clearColor];
    
    _collectionViewLayout = [[HeightViewerFlowLayout alloc] init];
    _collectionViewLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    _collectionViewLayout.minimumInteritemSpacing = 0;
    _collectionViewLayout.minimumLineSpacing = 0;
    _collectionViewLayout.itemSize = CGSizeMake(self.view.bounds.size.width, self.view.bounds.size.height);
    
    
    _heightViewerCollectionView = [[UICollectionView alloc]initWithFrame:self.view.bounds
                                                    collectionViewLayout:_collectionViewLayout];
    _heightViewerCollectionView.bounces = YES;
    _heightViewerCollectionView.showsHorizontalScrollIndicator = NO;
    _heightViewerCollectionView.showsVerticalScrollIndicator = NO;
    _heightViewerCollectionView.delegate = self;
    _heightViewerCollectionView.dataSource = self;
    _heightViewerCollectionView.backgroundColor = [UIColor clearColor];
    
    
    [_heightViewerCollectionView registerClass:[HeightViewerCollectionCell class] forCellWithReuseIdentifier:@"HeightViewCell33"];
    
    [self.view addSubview:_heightViewerCollectionView];
    
    [self reloadData];
    
    [self moveWithPageIndex:self.startIndex];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

-(void)reloadData
{
    [_heightViewerCollectionView reloadData];
}

#pragma mark - Load & Init Toolbar Animation
- (void)loadToolbar
{
    CGFloat iconWidth = 88.0/2;
    CGFloat margin = 18/2;
    CGFloat marginX = 326/2;
    
    CGFloat x = marginX ;
    CGFloat y = self.view.frame.size.height - iconWidth - 8/2 ;
    
    _btnDelete = [UIButton buttonWithType:UIButtonTypeCustom];
    _btnDelete.frame = CGRectMake(x, y,  iconWidth, iconWidth);
    [_btnDelete setImage:[UIImage imageNamed:@"height_viewer_btn_delete_normal"] forState:UIControlStateNormal];
    [_btnDelete setImage:[UIImage imageNamed:@"height_viewer_btn_delete_press"] forState:UIControlStateHighlighted];
    [_btnDelete addTarget:self action:@selector(onDelete) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_btnDelete];
    
    x = marginX + iconWidth + margin;
    
    _btnGraph = [UIButton buttonWithType:UIButtonTypeCustom];
    _btnGraph.frame = CGRectMake(x, y,  iconWidth, iconWidth);
    [_btnGraph setImage:[UIImage imageNamed:@"height_viewer_btn_graph_normal"] forState:UIControlStateNormal];
    [_btnGraph setImage:[UIImage imageNamed:@"height_viewer_btn_graph_press"] forState:UIControlStateHighlighted];
    [_btnGraph addTarget:self action:@selector(onGraph) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_btnGraph];
    
    x = marginX + iconWidth + margin + iconWidth + margin;
    
    _btnShare = [UIButton buttonWithType:UIButtonTypeCustom];
    _btnShare.frame = CGRectMake(x, y,  iconWidth, iconWidth);
    [_btnShare setImage:[UIImage imageNamed:@"height_viewer_btn_share_normal"] forState:UIControlStateNormal];
    [_btnShare setImage:[UIImage imageNamed:@"height_viewer_btn_share_press"] forState:UIControlStateHighlighted];
    [_btnShare addTarget:self action:@selector(onShare) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_btnShare];
    
    //    self.view.backgroundColor = [UIColor redColor];
    
}


- (void)loadAnimationStickerView
{
    
    CGFloat width = 422/2;
    CGFloat height = 652/2;
    
    _imageViewSticker = [[UIImageView alloc]initWithImage:[UIImage imageNamed:nil]];
    _imageViewSticker.frame = CGRectMake(self.view.frame.size.width - width, self.view.frame.size.height - height,
                                         width, height);
    [self.view addSubview:_imageViewSticker];
    
}


- (void)loadHeightInfoLabel
{
    _labelDate_bg =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"heightviewer_dates_bg"]];
    _labelDate_bg.frame = CGRectMake(14/2, self.view.frame.size.height - (68+18)/2, 204/2, 68/2);
    
    
    _labelDate = [[UILabel alloc]initWithFrame:CGRectMake( (32)/2, self.view.frame.size.height - (36+34)/2,
                                                          160, 24/2)];
    _labelDate.font = [UIFont defaultBoldFontWithSize:33/2];
    _labelDate.backgroundColor = [UIColor clearColor];
    _labelDate.textColor = [UIColor whiteColor];
    
    
    //    Roboto 21 Bold
    //    Align : left
    //    ▣ Color #ffffff ▣ Text-shadow
    //    angle 90o / Distance 2px / Size 12px / #000000 / Opacity 60%
    
    _labelDate.layer.shadowColor = [UIColor blackColor].CGColor;
    _labelDate.layer.shadowOffset = CGSizeMake(0, 0);
    _labelDate.layer.shadowRadius = 1;
    _labelDate.layer.shadowOpacity = 0.6;
    _labelDate.layer.masksToBounds = NO;
    
    [self.view addSubview:_labelDate_bg];
    [self.view addSubview:_labelDate];
    
}
-(void)runStickerAnimationWithType:(enum DoleStickerType)type WithHeight:(CGFloat)height{
    
    NSArray *frameCountArray = @[
                                 [NSNumber numberWithInt:25],
                                 [NSNumber numberWithInt:24],
                                 [NSNumber numberWithInt:25],
                                 [NSNumber numberWithInt:23],
                                 [NSNumber numberWithInt:25]];
    NSInteger frameCount = [frameCountArray[type] intValue];
    
    NSArray *imageList = @[@"01_raccoon_", @"02_toucan_", @"03_lion_", @"04_alligator_", @"05_fox_"];
    NSString *stickerImageName = [NSString stringWithFormat: @"%@%02d", imageList[type], frameCount-1];
    
    _imageViewSticker.image = [UIImage imageNamed:stickerImageName];
    //    NSArray *imageNameFormats = @[@"01_raccoon_%02d", @"02_toucan_%02d", @"03_lion_%02d", @"04_alligator_%02d", @"05_fox_%02d"];
    
    NSArray *stillImageNames = @[@"01_raccoon_24", @"02_toucan_23", @"03_lion_24", @"04_alligator_22", @"05_fox_24"];
    CGPoint lastPositions[] = {
        {(422-123-193)/2,(652-52-170)/2},
        {(422-123-100)/2, (652-52-116)/2},
        {(422-123-94)/2, (652-52-212)/2},
        {(422-123-178)/2, (652-52-224)/2},
        {(422-123-159)/2, (652-52-144)/2}
    };
    
    UIImage *lastStillImage = [UIImage imageName:stillImageNames[type] kidHeight:height leftTop:lastPositions[type]];
    _imageViewSticker.image = lastStillImage;
    
    [_imageViewSticker addHeightViewerAnimalAnimationWithType:type KidHeight:height];
    [_imageViewSticker startAnimating];
    
}


#pragma mark - ToolBar
//파일을 삭제 합니다.

-(void)onDelete
{
    //페이지에서 데이터를 삭제한다.
    
    //    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Popup" bundle:nil];
    //    ConfirmPopupController *vc = [storyboard instantiateViewControllerWithIdentifier:@"confirmPopup"];
    //    [vc popupTextUp:NSLocalizedString(@"The chosen record will be deleted.", @"")
    //         TextBottom:NSLocalizedString(@"It cannot be restored after deletion.", @"")];
    //    vc.completionBlock = ^(BOOL result){
    //        NSLog(@"Click is %@", result ? @"OK" : @"Cancel");
    //
    //        if (result) {
    //            _deletePageIndex = _nowPageIndex;
    //            [self.hightViewerDelegate deleteHeightViewerWithIndex:_deletePageIndex HeightViewer:self];
    //        }
    //        else{
    ////            [ToastController showToastWithMessage:@"It was not deleted." duration:2.0];
    //        }
    //    };
    //    [vc showModalOnTheViewController:nil];
    
    
    [self showOkCancelConfirmMessage:NSLocalizedString(@"The chosen record will be deleted.\nIt cannot be restored after deletion.",
                                                       @"")completion:^(BOOL isOK) {
        
        if (isOK) {
            _deletePageIndex = _nowPageIndex;
            [self.hightViewerDelegate deleteHeightViewerWithIndex:_deletePageIndex HeightViewer:self];
        }
        else{
            //            [ToastController showToastWithMessage:@"It was not deleted." duration:2.0];
        }
        
        
    }];
    
}

- (void)reloadPageViewWithIndex:(NSInteger)index
{
    [self reloadData] ;
    [self moveWithPageIndex:index];
    [self reloadHeightInfomationWithAnimatin];
}

-(void)reloadHeightInfomationWithAnimatin
{
    NSInteger nowIndex = _nowPageIndex;
    HeightDetailInfo *hdi = [self.datasource heightDetailAtIndex:nowIndex];
    enum DoleStickerType stickerType =  hdi.childHeight.stickerType.intValue;
    
    
    _labelDate.text = [hdi.takenDate stringForHeightViewer];
    [self runStickerAnimationWithType:stickerType WithHeight:hdi.kidHeight];
}


// Summary페이지로 넘어간다
-(void)onGraph
{
    
    //    HeightSummaryViewController *summaryVC = [self.storyboard instantiateViewControllerWithIdentifier:@"Summary"];
    //    summaryVC.heightDetailDataSource = self.datasource;
    //    summaryVC.shownIndex = _nowPageIndex;
    //
    //
    //    [self presentViewController:summaryVC animated:NO completion:nil];
    
    if (self.userConfig.accountStatus == kNotSingUp){
        __weak HeightViewerController *weakSelf = self;
        [self presentLogInViewControllerOKCancelWithCompletion:^(BOOL isLoggedIn) {
            if (isLoggedIn){
                [weakSelf showSummaryViewController];
            }
        }];
    }
    else{
        [self showSummaryViewController];
    }
}

-(void)showSummaryViewController
{
    HeightSummaryViewController *summaryVC = [self.storyboard instantiateViewControllerWithIdentifier:@"Summary"];
    summaryVC.heightDetailDataSource = self.datasource;
    summaryVC.shownIndex = _nowPageIndex;
    summaryVC.myChild = self.mychild;
    
    [self presentViewController:summaryVC animated:NO completion:nil];
}

//페이스북 공유
-(void)onShare
{
    if([self enableActionWithCurrentNetwork] == NO) return;
    
    //    UIStoryboard *popupStory = [UIStoryboard storyboardWithName:@"Popup" bundle:nil];
    //    SelectShareAppPopupController *vc = [popupStory instantiateViewControllerWithIdentifier:@"ShareFacebook"];
    //
    //    vc.CompletionBlock = ^(BOOL result){
    //
    //        if (result) {
    //            if (self.facebookManager.facebookID){
    //                [self sharePhoto];
    //            }
    //            else{
    //                //[self.facebookManager login];
    //                [self sharePhoto];
    //            }
    //        }
    //    };
    //
    //    [vc showModalOnTheViewController:nil];
    
    if (self.userConfig.accountStatus == kNotSingUp){
        __weak HeightViewerController *weakSelf = self;
        [self presentLogInViewControllerOKCancelWithCompletion:^(BOOL isLoggedIn) {
            if (isLoggedIn){
                [weakSelf doSharePhoto];
            }
        }];
    }
    else{
        [self doSharePhoto];
    }
}

-(void)doSharePhoto
{
    UIStoryboard *popupStory = [UIStoryboard storyboardWithName:@"Popup" bundle:nil];
    SelectShareAppPopupController *vc = [popupStory instantiateViewControllerWithIdentifier:@"ShareFacebook"];
    
    vc.CompletionBlock = ^(BOOL result){
        
        if (result) {
            if (self.facebookManager.facebookID){
                [self sharePhoto];
            }
            else{
                //[self.facebookManager login];
                [self sharePhoto];
            }
        }
    };
    
    [vc showModalOnTheViewController:nil];
}

- (void)sharePhoto
{
    NSInteger nowIndex = _nowPageIndex;
    HeightDetailInfo *hdi = [self.datasource heightDetailAtIndex:nowIndex];
    
    
    NSLog(@"Start == > %@", [NSDate date]);
    UIImage *sharePhoto = [self createImageForShare];
    //    UIImage *sharePhoto = [self drawImageForShare];
    NSLog(@"END == > %@", [NSDate date]);
    
    //    HeightDetailInfo *hdi = [self.datasource heightDetailAtIndex:0];
    //    NSString *nickName = hdi.childHeight.parent.nickname;
    
    NSString *nickName = self.mychild.nickname;
    
    //    NSString *uploadMessage = [NSString stringWithFormat:NSLocalizedString(@"%@'s height", @""), nickName];
    //NSString *height = self.mychild.
    NSString *heightText = [NSString stringWithKideHeightWithOutCmUnit:hdi.kidHeight];
    NSString *uploadMessage = [NSString stringWithFormat:@""];
                               //FB pre-filling not allowed NSLocalizedString(@"%@'s height is now %@cm. I used the #DoleHeightChart App!", @""), nickName, heightText, nil];
    
    [self addActiveIndicator];
    [self.facebookManager postingPhoto:sharePhoto Message:uploadMessage completion:^(BOOL isSuccess) {
        if (isSuccess){
            [ToastController showToastWithMessage:NSLocalizedString(@"Successful! Your photo has been shared.", @"") duration:kToastMessageDurationType_Auto onViewController:self];
            
            [self removeActiveIndicator];
            
            [self countUpDolePoint];
            
            [[Mixpanel sharedInstance] track:@"Shared Height Photo via Facebook"];
        }
        else {
            [self removeActiveIndicator];
            
            //            [ToastController showToastWithMessage:@"Facebook에 업로드 실패 하였습니다" duration:5 onViewController:self];
            [self showOKConfirmMessage:NSLocalizedString(@"Failed to share on Facebook.\nPlease check network connection\nor Facebook status and try again", @"") completion:nil];
            
        }
    }];
}

- (UIImage *)createImageForShare
{
    HeightDetailInfo *detailHeightInfo = [self.datasource heightDetailAtIndex:_nowPageIndex];
    
    UIImage *image = [self drawImage:detailHeightInfo.imageFullSize withBadge:_imageViewSticker.image];
    
    
    return image;
}

-(UIImage *)drawImage:(UIImage*)profileImage withBadge:(UIImage *)badge
{
    
    [_imageViewSticker stopAnimating];
    
    CGRect dstRect = CGRectMake(0, 0, profileImage.size.width, profileImage.size.height); // 960 or 1136 ?
    dstRect.size.width = dstRect.size.width * profileImage.scale;
    dstRect.size.height = dstRect.size.height * profileImage.scale;
    
    CGRect bgRect = CGRectMake( dstRect.size.width - badge.size.width*2 ,
                               dstRect.size.height - badge.size.height*2
                               , badge.size.width*2, badge.size.height*2); // 960 or 1136 ?
    
    UIImage *titleImage = [UIImage imageNamed:@"height_viewer_height_chart_logo"];
    //height_viewer_height_chart_logo
    CGRect titleRect = CGRectMake( 510 , 18,
                                  titleImage.size.width*2 , titleImage.size.height*2); // 960 or 1136 ?
    
    UIGraphicsBeginImageContextWithOptions(dstRect.size, NO, 2.0f);
    
    
    // draw taken image
    [profileImage drawInRect:dstRect];
    
    
    // draw photo frame image
    [badge drawInRect:bgRect];
    
    [titleImage drawInRect:titleRect];
    
    UIImage *resultImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return resultImage;
}


-(void)visibleToolBarHidden:(BOOL)isHidden
{
    _btnDelete.hidden = isHidden;
    _btnGraph.hidden = isHidden;
    _btnShare.hidden = isHidden;
    _labelDate_bg.hidden = isHidden;
    _labelDate.hidden = isHidden;
    _closeButton.hidden = isHidden;
    
}

-(UIImage *)drawImageForShare
{
    [_imageViewSticker stopAnimating];
    
    [self visibleToolBarHidden:YES];
    
    UIImage *titleImage = [UIImage imageNamed:@"height_viewer_height_chart_logo"];
    
    CGRect titleRect = CGRectMake( 510/2 , 18/2,
                                  titleImage.size.width , titleImage.size.height); // 960 or 1136 ?
    UIImageView *titleVIew = [[UIImageView alloc]initWithImage:titleImage];
    titleVIew.frame = titleRect;
    [self.view addSubview:titleVIew];
    
    
    UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, NO, 2.0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    [self.view.layer renderInContext:context];
    
    
    UIImage *resultImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [self visibleToolBarHidden:NO];
    
    return resultImage;
}

- (void)viewDidAppear:(BOOL)animated
{
    [self reloadHeightInfomationWithAnimatin];
}

-(void)moveWithPageIndex:(NSInteger)inedex
{
    NSIndexPath *path = [NSIndexPath indexPathForRow:inedex inSection:0];
    [_heightViewerCollectionView selectItemAtIndexPath:path animated:YES scrollPosition:UICollectionViewScrollPositionLeft ];
    
    
    _nowPageIndex = inedex;
}

#pragma  mark CollercionView

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.datasource countOfHeightDetail];
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellID;
    
    cellID = @"HeightViewCell33";
    
    HeightViewerCollectionCell *cell = [_heightViewerCollectionView
                                        dequeueReusableCellWithReuseIdentifier:cellID
                                        forIndexPath:indexPath];
    
    HeightDetailInfo *detailHeightInfo = [self.datasource heightDetailAtIndex:indexPath.row];
    
    UIImage *image = detailHeightInfo.imageFullSize;
    
    [cell updateCellWithImage:image];
    
    return cell;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self unVisibleStickerAndToolBar];
}

- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    //[self updateGraph];
}

-(void) unVisibleStickerAndToolBar
{
    [self setIsVisibleToolBar:false];
    [_imageViewSticker setHidden:YES];
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (NO == decelerate){
        [self adjustPage];
    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self adjustPage];
}

-(void)adjustPage
{
    [self setIsVisibleToolBar:true];
    [_imageViewSticker setHidden:NO];
    
    
    _nowPageIndex = _heightViewerCollectionView.contentOffset.x/320;
    
    
    if (_adjustBeforePageIndex != _nowPageIndex) {
        _adjustBeforePageIndex = _nowPageIndex;
        
        [self reloadHeightInfomationWithAnimatin];
    }
    
    return;
    
}


#pragma mark Server

-(void)countUpDolePoint
{
    if (self.userConfig.loginStatus != kUserLogInStatusOffLine){
        
        _lastOrderID = [self UUID];
        [self requestDoleCoinChargeFreeCash:100 UserNo:self.userConfig.userNo AuthKey:self.userConfig.authKey OrderID:_lastOrderID];
    }
    //    else{
    //        [ToastController showToastWithMessage:
    //         NSLocalizedString(@"Successful! Your photo has been shared.", @"") duration:5 onViewController:self];
    //    }
}

- (void)didConnectionFail:(NSError *)error
{
    [self toastNetworkError];
}

- (void)didCompleteRecevedWithResultType:(DoleServerResultType)resultType ParsedData:(NSDictionary *)recevedData ParseError:(NSError *)error
{
    if (!error){
        if ([recevedData[@"Return"] boolValue]){
            
            if (resultType == kDoleServerResultTypeChargeFreeCash){
                
                self.userConfig.doleCoin += 100;
                [self.userConfig saveDoleCoinToLocal];
                
                UIStoryboard *story = [UIStoryboard storyboardWithName:@"Popup" bundle:nil];
                DoleCoinPopupViewController *dvc = [story instantiateViewControllerWithIdentifier:@"dolecoinPopup"];
                [dvc dolcoinCount:100];
                dvc.completion = ^(DoleCoinPopupViewController *controller){
                    
                };
                [dvc showModalOnTheViewController:nil];
            }
        }
        else{
            NSInteger errorCode = [recevedData[@"ReturnCode"] integerValue];
            [self toastNetworkErrorWithErrorCode:errorCode];
        }
    }
    else{
        
        [self toastNetworkError];
    }
}

- (NSMutableData*)receivedBufferData
{
    if (nil == _recevedBufferData){
        _recevedBufferData = [NSMutableData dataWithCapacity:0];
    }
    
    return _recevedBufferData;
}

- (void) loginResult:(BOOL)isSucess withAcessToken:(NSString*)accessToken
{
    
}

- (void) didReceiveUserID:(NSString*)userID Email:(NSString*)email Error:(id)error
{
    if (nil == error && userID)
        [self sharePhoto];
    else {
        
    }
}

-(void)viewDidDisappear:(BOOL)animated
{
    //    self.facebookManager.delegate = nil;
}

@end



