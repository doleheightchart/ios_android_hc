//
//  InputHeightViewController.h
//  HeightChart
//
//  Created by mytwoface on 2014. 3. 2..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HeightSaveContext.h"

@interface InputHeightViewController : UIViewController
@property (nonatomic, copy) void (^completion)(CGFloat kidHeight);
@property (nonatomic, retain) HeightSaveContext *heightSaveContext;
@property (nonatomic, weak) id<HeightSaveDelegate> heightSaveDelegate;
@end
