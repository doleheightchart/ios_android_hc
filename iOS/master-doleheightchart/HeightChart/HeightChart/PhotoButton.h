//
//  PhotoButton.h
//  HeightChart
//
//  Created by ne on 2014. 3. 3..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import <UIKit/UIKit.h>

enum DolePhotoButtonType {
    DolePhotoButtonTypeJungle,
    DolePhotoButtonTypeSpace,
    DolePhotoButtonTypeSea,
    DolePhotoButtonTypeSeafloor,
    DolePhotoButtonTypePark,
    DolePhotoButtonTypeFruit,
    DolePhotoButtonTypeRobot,
    DolePhotoButtonTypeCastle,
    DolePhotoButtonTypeLock,
};

enum DoleCaptureButtonState {
    DoleCaptureButtonStateCamera,
    DoleCaptureButtonStateSave,
    DoleCaptureButtonStateLogin,
    DoleCaptureButtonStateDone,
};

@interface CameraButton : UIButton

@property (nonatomic) enum DoleCaptureButtonState cameraState;

-(void)initCameraButtonImage;
-(id)initWithCameraButton;
-(void)setCameraButtonState:(enum DoleCaptureButtonState) state;

+(instancetype)camerabutton;
+(CGSize)size;

@end

@interface PhotoButtonControl : UIControl

@property (nonatomic) enum DolePhotoButtonType type;
@property (nonatomic) BOOL isLockFrame;
@property (nonatomic) BOOL isSelected;

-(id)initWithPhotoButtonType:(enum DolePhotoButtonType)type;

@end
