#import "DoleInfo.h"

@implementation DoleInfo


NSString* kDoleCharacterNames[] = {
    @"Dolekey",
    @"Rilla",
    @"Teeny",
    @"Coco",
    @"Panzee"
};

+(NSString*)doleCharaterNameWithType:(enum DoleCharacterType)charaterType
{
    
    return NSLocalizedString(kDoleCharacterNames[charaterType],@"");
//    return kDoleCharacterNames[charaterType];
}


+(enum DoleStickerType)convertStickerTypeWithString:(NSString*)stickerValue
{
    if ([stickerValue isEqualToString:@"DOLE-A01"]) return DoleStikerLion;

    if ([stickerValue isEqualToString:@"DOLE-A02"]) return DoleStikerToucan;

    if ([stickerValue isEqualToString:@"DOLE-A03"]) return DoleStikerFox;

    if ([stickerValue isEqualToString:@"DOLE-A04"]) return DoleStikerAlligator;

    if ([stickerValue isEqualToString:@"DOLE-A05"]) return DoleStikerRacoon;

    return DoleStikerUnknown;
}

+(enum DoleStickerType)convertStickerTypeWithStringQR:(NSString*)stickerValue
{
    if ([stickerValue isEqualToString:@"A01"]) return DoleStikerLion;
    
    if ([stickerValue isEqualToString:@"A02"]) return DoleStikerToucan;
    
    if ([stickerValue isEqualToString:@"A03"]) return DoleStikerFox;
    
    if ([stickerValue isEqualToString:@"A04"]) return DoleStikerAlligator;
    
    if ([stickerValue isEqualToString:@"A05"]) return DoleStikerRacoon;
    
    return DoleStikerUnknown;
}



@end

