//
//  EditFacebookViewController.h
//  HeightChart
//
//  Created by ne on 2014. 3. 24..
//  Copyright (c) 2014년 Kim Sehyun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DoleInfo.h"

@interface EditFacebookViewController : UIViewController<UITextFieldDelegate>

@property (nonatomic) BOOL isRegisterMode;

@property (nonatomic) enum GenderType genderType;
@property (nonatomic, retain) NSString* city;
@property (nonatomic) NSInteger birthYear;

@property (nonatomic, retain) NSString *facebookID;
@property (nonatomic, retain) NSString *facebookName;
@property (nonatomic, retain) NSString *facebookEmail;

@property (nonatomic) BOOL appFirstTimeLogIn;
@property (nonatomic, copy) void(^requireLoginCompletion)(BOOL isLoggedIn);

@end
