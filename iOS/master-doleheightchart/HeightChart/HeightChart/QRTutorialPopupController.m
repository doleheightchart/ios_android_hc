//
//  QRTutorialPopupController.m
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 16..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "QRTutorialPopupController.h"

@interface QRTutorialPopupController ()

-(IBAction)clickClose:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblTop;
@property (weak, nonatomic) IBOutlet UILabel *lblBottom;

@end

@implementation QRTutorialPopupController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.lblTop.font = [UIFont defaultKoreanFontWithSize:34/2];
    self.lblBottom.font = [UIFont defaultKoreanFontWithSize:34/2];
    self.lblTop.textColor = [UIColor colorWithRGB:0x787878];
    self.lblBottom.textColor = [UIColor colorWithRGB:0x787878];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)clickClose:(id)sender
{
    self.completion(self);
}

@end
