//
//  HeightViewerCollectionCell.m
//  HeightChart
//
//  Created by ne on 2014. 3. 12..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "HeightViewerCollectionCell.h"
#import "UIFont+DoleHeightChart.h"

@interface HeightViewerCollectionCell()
{
    UIImageView *_heigtImageView;
    UILabel *_kidNameLabel;
}

@end

@implementation HeightViewerCollectionCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self initHeightViewerCellWithFrame:self.bounds];
    }
    return self;
}

- (void)initHeightViewerCellWithFrame:(CGRect)frame
{
    
//    CGRect f = CGRectMake(0, 0, 320, 568);
//    self.backgroundColor = [UIColor whiteColor];
    self.backgroundColor = [UIColor whiteColor];

    _heigtImageView = [[UIImageView alloc]initWithFrame:frame];
    _heigtImageView.contentMode = UIViewContentModeScaleAspectFit;
    
    _kidNameLabel = [[UILabel alloc]initWithFrame:frame];

    _kidNameLabel.textColor = [UIColor blueColor];
    _kidNameLabel.backgroundColor = [UIColor clearColor];
    _kidNameLabel.font = [UIFont defaultBoldFontWithSize:30];
    
    [self addSubview:_heigtImageView];
    [self addSubview:_kidNameLabel];
    
    NSLog(@"initHeightViewerCellWithFrame");
    
}

-(void)updateCellWithImage:(UIImage*)image
{
    _heigtImageView.image = image;
}

-(void)updateCellWithImage:(UIImage*)image text:(NSString*)text
{
    assert(_heigtImageView);
//    assert(image);
    assert(_kidNameLabel);
    
    _heigtImageView.image = image;
    _kidNameLabel.text = text;
    
    NSLog(@"updateCellWithImage");
}


@end
