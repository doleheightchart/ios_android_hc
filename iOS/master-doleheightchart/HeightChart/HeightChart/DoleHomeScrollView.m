//
//  DoleHomeScrollView.m
//  HeightChart
//
//  Created by ne on 2014. 3. 21..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "DoleHomeScrollView.h"
#import "DoleInfo.h"
#import "DoleTree.h"
#import "DoleMonkeyTree.h"

@interface DoleHomeScrollView()
{
    NSMutableArray *_MonkeyTrees;
    UIScrollView *_homeScrollView;
    UIView *_homeScrollContentView;
//    NSInteger _monkeyCount;
    DoleOwlTree* _owlTree;
};

@end

@implementation DoleHomeScrollView

CGFloat kScrollMonkeyTreeViewHeight = 650 + 101 + 64; // tree Height + Monkey Margin  + Max NameTag Margin
CGFloat kScrollMonkeyNameTagMargin = 101 + 64;
CGFloat kScrollMonkeyTreeViewWidth = 230;
CGFloat kMonkeyTreeViewMargin = 25;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self initializeContent];
    }
    return self;
}


- (void)initializeContent
{
    _homeScrollView = [[UIScrollView alloc]initWithFrame:self.bounds];
    _homeScrollView.scrollEnabled = YES;
    _homeScrollView.alwaysBounceHorizontal = YES;
    _homeScrollView.delegate = self;
    _homeScrollView.pagingEnabled = NO;
    
    _homeScrollView.contentSize = self.bounds.size;
    _homeScrollContentView = [[UIView alloc]initWithFrame:self.bounds];
    
    [_homeScrollView addSubview:_homeScrollContentView];
    [self resizeScrollContentViewWithTreeCount:0];
    [self addSubview:_homeScrollView];
    
    _MonkeyTrees = [[NSMutableArray alloc]init];
}

-(void)deleteAllMonkeyTreeData
{
    //모든 데이터를 삭제한다.
    for (NSInteger i = (_MonkeyTrees.count-1); i >= 0; i --) {
        DoleMonkeyTree* mTree = (DoleMonkeyTree*)_MonkeyTrees[i];
        //[_MonkeyTrees removeObject:mTree];
        [mTree removeFromSuperview];
    }
    
    [_MonkeyTrees removeAllObjects];
    
    
    //화면에 모든 트리 객체를 삭제한다.
    
//    NSArray *subViews = [_homeScrollContentView subviews];
//    for(int s = 0 ; s < subViews.count; s++)
//    {
//        if([subViews[s] isKindOfClass:[DoleTree class]])
//        {
//            [_homeScrollContentView.subviews[s] removeFromSuperview];
//        }
//    }
}

-(void)reloadMonkeyTree
{
    //모든화면데이터 삭제
    [self deleteAllMonkeyTreeData];
    
    [_owlTree removeFromSuperview];
    
        //Data source에서 있는데이터를 읽어와서 몽키트리에 넣는다.
    NSInteger dataCount = [self.datasource monkeyCount];
    
    for (NSInteger i = 0 ; i < dataCount; i++) {
        MyChild *monkeyInfo = [self.datasource monkeyTreeInfoAtIndex:i];
        enum DoleMonkeyType monkeyType = monkeyInfo.charterNo.shortValue;
        
        DoleMonkeyTree *monkey = [[DoleMonkeyTree alloc]initWithFrame:[self initMonkeyTreePositonWithIndex:i]
                                                       doleMonkeyType:monkeyType];

        
        monkey.clickMonkeyDelegate = self;
        monkey.nickname = monkeyInfo.nickname;
//        monkey.height = monkeyInfo.lastTakenHeight;
        monkey.height = monkeyInfo.maxTakenHeight;
        
        [_MonkeyTrees addObject:monkey];
        [_homeScrollContentView addSubview:monkey];

    }
    
    // 부엉이 추가
    _owlTree = [[DoleOwlTree alloc]initWithFrame:[self initMonkeyTreePositonWithIndex:_MonkeyTrees.count]
                                  doleMonkeyType:MonkeyTreeType_OWL];
    _owlTree.clickOwlDelegate = self;
    [_homeScrollContentView addSubview:_owlTree];
    
    //부엉이 위치까지 계산해서 화면을 늘려준다. 
    [self resizeScrollContentViewWithTreeCount:_MonkeyTrees.count + 1];
    //각자 위치를 잡는다. 그리고 에니메이션을 한다.
    
    [UIView animateWithDuration:1.5 animations:^{
//        NSInteger index[] = {30,50,70,100,170};
        for (int i = 0; i < _MonkeyTrees.count; i ++) {
            DoleMonkeyTree* mTree = (DoleMonkeyTree*)_MonkeyTrees[i];
            CGRect frame = mTree.frame;
            frame.origin.y = [self monkeyTreeHeightChangeByChildHight:mTree.height];
            mTree.frame = frame;

        }
        CGRect frame = _owlTree.frame;
        frame.origin.y = [self maxPosY];
        _owlTree.frame = frame;
    } completion:^(BOOL finished) {
        [self playAllTreeAnimation];
        
    }];
}

-(void)playAllTreeAnimation
{
    static BOOL isFisrtTime = TRUE;
    
    // 삭제모드일 경우 에니메이션 하지 않는다 .
    if (self.isDeleteMode) {
        return;
    }
    
    // 아울트리 이미지 사이즈가 달라서 먼저 설정해 준다.
    [_owlTree changeOwlRectWithMonkeyCount:_MonkeyTrees.count];
    
    [UIView animateWithDuration:2 animations:^{

        [_owlTree playOwlAnimationWithMonkeyCount:_MonkeyTrees.count];
        
        for (int i = 0; i < _MonkeyTrees.count; i ++) {
            DoleMonkeyTree* mTree = (DoleMonkeyTree*)_MonkeyTrees[i];
            [mTree playMonkeyAnimation];
        }
    } completion:^(BOOL finished) {

        [_owlTree showTopTagWithAnimation];
        
        for (int i = 0; i < _MonkeyTrees.count; i ++) {
            DoleMonkeyTree* mTree = (DoleMonkeyTree*)_MonkeyTrees[i];
            [mTree showTopTagWithAnimation];
        }
        
        [self.homeListDelegate didFinishedAnimation:isFisrtTime];
        isFisrtTime = FALSE;
        
    }];
}


// 스크롤 콘텐츠로 리사이즈 하면서 옵셋 위치를 부엉이 -3 으로 둔다.
// 최초이니셜 할때 사용
// 닉네임 추가 할경우 사용한다.
- (void)resizeScrollContentViewWithTreeCount:(NSInteger)count{
    
    if (count <= 3) {
        //현재 화면사이즈
        CGRect frame = _homeScrollContentView.frame;
        frame.size.width = [UIScreen mainScreen].bounds.size.width;
        _homeScrollContentView.frame = frame;
    }
    else {
        //늘린다.
        CGRect frame = _homeScrollContentView.frame;
        frame.size.width = [UIScreen mainScreen].bounds.size.width + (kScrollMonkeyTreeViewWidth-kMonkeyTreeViewMargin)/2*(count-3);
        _homeScrollContentView.frame = frame;
    }
    
    _homeScrollView.contentSize = _homeScrollContentView.frame.size;
    
    NSInteger offsetMonkey = MAX(0, count-3-1);
    
    CGPoint scrollOffset = CGPointMake(offsetMonkey* ((kScrollMonkeyTreeViewWidth-kMonkeyTreeViewMargin)/2), 0);
    [_homeScrollView setContentOffset:scrollOffset];
//    [_homeScrollView setContentOffset:scrollOffset animated:YES];
}

#pragma mark ======= Add Delete Update Reload

//Add Delete Reload
-(void)addNewMonkeyWithMyChildInfo:(MyChild*)child
{
    enum DoleMonkeyType monkeyType = child.charterNo.shortValue;
    
    //새로운 데이터를 추가한다.
    DoleMonkeyTree *monkey = [[DoleMonkeyTree alloc]initWithFrame:[self initMonkeyTreePositonWithIndex:_MonkeyTrees.count]
                                                   doleMonkeyType:monkeyType];
    
    monkey.nickname = child.nickname;
//    monkey.height = child.lastTakenHeight;
    monkey.height = child.maxTakenHeight;
    monkey.clickMonkeyDelegate = self;
    [_MonkeyTrees addObject:monkey];

    
    [_homeScrollContentView addSubview:monkey];
    
    [self adjustMonkeyTreePosX];
    
    [self resizeScrollContentViewWithTreeCount:_MonkeyTrees.count +1];
    
    [UIView animateWithDuration:2.0 animations:^{
        CGRect frame = monkey.frame;
        frame.origin.y = [self monkeyTreeHeightChangeByChildHight:monkey.height];
        monkey.frame = frame;


    } completion:^(BOOL finished) {
        
        [_owlTree playOwlAnimationWithMonkeyCount:_MonkeyTrees.count];
        
        for (int i = 0; i < _MonkeyTrees.count; i ++) {
            DoleMonkeyTree* mTree = (DoleMonkeyTree*)_MonkeyTrees[i];
            [mTree playMonkeyAnimation];
        }
        [monkey showTopTagWithAnimation];
    }];

}

-(void)deleteMonkeysWithIndexArray:(NSArray*)indexArray
{
    //TODO :: delete Testcode
    // test code -->
    NSMutableArray *array = [[NSMutableArray alloc]init];
    for (int i = 0; i < _MonkeyTrees.count; i ++) {
        DoleMonkeyTree* mTree = (DoleMonkeyTree*)_MonkeyTrees[i];
        if (mTree.isChecked) {
            [array addObject:[NSNumber numberWithInt:i]];
        }
    }
    indexArray = [NSArray arrayWithArray:array];
    // <-- TEST code
    
    
    NSMutableArray *deleteMonkeyTree = [[NSMutableArray alloc]init];
    
    [UIView animateWithDuration:1.0 animations:^{
        
        //삭제 대기 시키고 낮은 위치로 이동시킨다.
        for (int i = 0; i < indexArray.count ; i++) {
            NSInteger index = [[indexArray objectAtIndex:i] intValue];
            DoleMonkeyTree * monkeyTree = [_MonkeyTrees objectAtIndex:index];
            [deleteMonkeyTree addObject:monkeyTree];
            CGRect frame = monkeyTree.frame;
            frame.origin.y = [self minPosY] + 200;
            monkeyTree.frame = frame;
        }
        
    } completion:^(BOOL finished) {
        
        
        
        //다 끝나고 나면 삭제 처리 한후 스크롤을 조정한다.
        
        for (NSInteger i = indexArray.count-1; i >= 0 ; i--) {
            NSInteger index = [[indexArray objectAtIndex:i] intValue];
            
            DoleMonkeyTree * monkeyTree = [_MonkeyTrees objectAtIndex:index];
            //데이터 삭제
            [_MonkeyTrees removeObject:monkeyTree];
            
            // 화면에서 삭제
            NSArray *subViews = [_homeScrollContentView subviews];
            for(int s = 0 ; s < subViews.count; s++)
            {
                if(subViews[s] == monkeyTree)
                {
                    [_homeScrollContentView.subviews[s] removeFromSuperview];
                }
            }
        }
        
        //스크롤뷰 처리
        [self adjustMonkeyTreePosX];

    }];
}

-(void)checkedMonkeyTreeWithIndex:(NSInteger)monkeyIndex
{
    DoleMonkeyTree* mTree = (DoleMonkeyTree*)_MonkeyTrees[monkeyIndex];
    assert(mTree);
    mTree.isDeleteMode = YES;
    mTree.isChecked = YES;
}

-(void)updateMonekyTreehegithWithIndex:(NSInteger)monkeyIndex
{
    // 셋팅된 센티 를 넣어준다.
    MyChild *monkeyInfo =  [self.datasource monkeyTreeInfoAtIndex:monkeyIndex];
    
    DoleMonkeyTree* mTree = (DoleMonkeyTree*)_MonkeyTrees[monkeyIndex];
    assert(mTree);
//    mTree.height = monkeyInfo.lastTakenHeight;
    mTree.height = monkeyInfo.maxTakenHeight; 
    mTree.nickname = monkeyInfo.nickname;
    
    if (mTree.monkeyType != (enum DoleMonkeyType)monkeyInfo.charterNo.shortValue) {
        mTree.monkeyType = (enum DoleMonkeyType)monkeyInfo.charterNo.shortValue;
        [mTree changeMonkeytreeType];
    }
    
    // 스크롤을 이동한다.
    NSInteger offsetMonkey = _MonkeyTrees.count - 3;
    offsetMonkey = MAX(0, offsetMonkey);
    [self moveScrollOffsetByMonkeyTreeIndex:offsetMonkey animated:YES];
    
    // 키를 업데이트 한다
    [UIView animateWithDuration:1.5 animations:^{
        
        DoleMonkeyTree* mTree = (DoleMonkeyTree*)_MonkeyTrees[monkeyIndex];
        CGRect rect = mTree.frame;
        rect.origin.y = [self monkeyTreeHeightChangeByChildHight:mTree.height]; ;
        mTree.frame = rect;

    } completion:^(BOOL finished) {
        //끝나고 모든 몽키 에니메이션을 한다.
        [self playAllTreeAnimation];
        
    }];
}


-(NSArray*)checkedMonkeyIndexList
{
    NSMutableArray * checkedMonkey = [[NSMutableArray alloc]init];
    
    for (int i = 0; i < _MonkeyTrees.count; i ++) {
        DoleMonkeyTree* mTree = (DoleMonkeyTree*)_MonkeyTrees[i];
        if (mTree.isChecked) {
            NSNumber *indexNumber = [NSNumber numberWithInt:i];
            [checkedMonkey addObject:indexNumber];
        }
    }
    
    return [NSArray arrayWithArray:checkedMonkey];
}

#pragma mark ======= SetPosition

- (void) moveScrollOffsetByMonkeyTreeIndex:(NSInteger)index animated:(BOOL)animated
{
    CGPoint scrollOffset = CGPointMake(index* ((kScrollMonkeyTreeViewWidth-kMonkeyTreeViewMargin)/2), 0);
    [_homeScrollView setContentOffset:scrollOffset animated:animated];
}


-(void)adjustMonkeyTreePosXForDelete
{
    [UIView animateWithDuration:1.0 animations:^{
        
        for (int i = 0; i < _MonkeyTrees.count; i ++) {
            DoleMonkeyTree* mTree = (DoleMonkeyTree*)_MonkeyTrees[i];
            CGRect rect = mTree.frame;
            rect.origin.x = [self adjustTreePositonXWithIndex:i];
            mTree.frame = rect;
        }
        
        
        CGRect rect = _owlTree.frame;
        rect.origin.x = [self adjustTreePositonXWithIndex:_MonkeyTrees.count ];
        _owlTree.frame = rect;
        
        
    } completion:^(BOOL finished) {
        
        
        [UIView animateWithDuration:1.0 animations:^{
            NSInteger offsetMonkey = _MonkeyTrees.count - 3;
            offsetMonkey = MAX(0, offsetMonkey);
            
            [self moveScrollOffsetByMonkeyTreeIndex:offsetMonkey animated:YES];
            
        } completion:^(BOOL finished){
            
            [self playAllTreeAnimation];
            
        }
         ];
        
        
    }];
    
    [self resizeScrollContentViewWithTreeCount:_MonkeyTrees.count+1];
    
}



-(void)adjustMonkeyTreePosX
{
    [UIView animateWithDuration:1.0 animations:^{
        
        for (int i = 0; i < _MonkeyTrees.count; i ++) {
            DoleMonkeyTree* mTree = (DoleMonkeyTree*)_MonkeyTrees[i];
            CGRect rect = mTree.frame;
            rect.origin.x = [self adjustTreePositonXWithIndex:i];
            mTree.frame = rect;
        }
        

        CGRect rect = _owlTree.frame;
        rect.origin.x = [self adjustTreePositonXWithIndex:_MonkeyTrees.count ];
        _owlTree.frame = rect;
        
        
        
        [self resizeScrollContentViewWithTreeCount:_MonkeyTrees.count+1];
        
    } completion:^(BOOL finished) {
        
        
        [UIView animateWithDuration:1.0 animations:^{
            NSInteger offsetMonkey = _MonkeyTrees.count - 3;
            offsetMonkey = MAX(0, offsetMonkey);
            
            [self moveScrollOffsetByMonkeyTreeIndex:offsetMonkey animated:YES];
            
        } completion:^(BOOL finished){
        
            [self playAllTreeAnimation];
            
            }
        ];
        

       
        
    }];
    
    
    
}





// 키를 넣어서 나무 높이를 계산하단.
-(CGFloat)monkeyTreeHeightChangeByChildHight:(NSInteger)childHeightCM
{
    NSInteger minChildCM = 50;
    NSInteger maxChildCM = 170;
    
    CGFloat minTreeHeightPositionPixel = [self minPosY];
    CGFloat maxTreeHeightPositionPixel = [self maxPosY];
    
    //960대응 최소 값은 똑같고 최대값이 다르다.
    
    CGFloat calcTreeHegiht = 0.0;
    
    // 키와 거리같의 비율 (작은 키에서 큰키의 차를 뺀다)
    CGFloat ratioForPixel = (minTreeHeightPositionPixel - maxTreeHeightPositionPixel) / (maxChildCM - minChildCM);
    
    if (minChildCM >= childHeightCM) {
        // 키가 기준치보다 작으면 최소 나무 값
        calcTreeHegiht = minTreeHeightPositionPixel;
    }
    else if(maxChildCM <= childHeightCM){
        //키가 기준치보다 크면 최대 나무 값
        calcTreeHegiht = maxTreeHeightPositionPixel;
    }
    else{
        
        CGFloat calcHeight = (maxChildCM - childHeightCM ) * ratioForPixel;
        
        calcTreeHegiht = maxTreeHeightPositionPixel + calcHeight;
    }
    
    NSLog(@"Calca Hieght %ld -> %f", (long)childHeightCM, calcTreeHegiht);
    
    return calcTreeHegiht;
}

-(BOOL)isMaxHeightByPosY:(CGFloat)posY
{
    BOOL isMAX = false;
    CGFloat maxTreeHeightPositionPixel = [self maxPosY];
    
    if (posY >= maxTreeHeightPositionPixel) {
        isMAX = true;
    }
    
    return isMAX;
}


- (CGRect)initMonkeyTreePositonWithIndex:(NSInteger)index
{
//    CGFloat xPos = index * ((kScrollMonkeyTreeViewWidth-kMonkeyTreeViewMargin)/2);
    CGFloat xPos = [self adjustTreePositonXWithIndex:index];
    CGFloat yPos = [self minPosY] + 100;  // 최대 크기 보다 100정도 작게하여 보이지 않게한다.
    CGRect rect =CGRectMake(xPos, yPos, kScrollMonkeyTreeViewWidth/2, kScrollMonkeyTreeViewHeight/2);
    return rect;
}

- (CGFloat)adjustTreePositonXWithIndex:(NSInteger)index
{
    NSInteger count = [self.datasource monkeyCount];
    if (count == 0) {
        
        return ((kScrollMonkeyTreeViewWidth-kMonkeyTreeViewMargin)/2) * 1; // 센터 차리
        
    }
    else if(count == 1)
    {
        CGFloat firstPosX = 69 / 2 ; // 마진 69
        CGFloat secondPosX = (69 + kScrollMonkeyTreeViewWidth + 42) / 2; //사양서 마진
       
        CGFloat fixPosX[] = { firstPosX , secondPosX };
        
        return fixPosX[index]; //인덱스가 1이상이면 문제

    }
    else {
        return index * ((kScrollMonkeyTreeViewWidth-kMonkeyTreeViewMargin)/2);
    }
}

- (CGFloat)maxPosY
{
    //나무가 올라갈수 있는 최대높이
    // 전체 화면 크기  - 몽키트리 전체 사이즈 - 지정된 마진
    CGFloat posY = [UIScreen mainScreen].bounds.size.height*2 - kScrollMonkeyTreeViewHeight -  73;
    if ([UIScreen mainScreen].bounds.size.height == 480) {
        posY = [UIScreen mainScreen].bounds.size.height*2 - kScrollMonkeyTreeViewHeight + 10;
    }
    
    return posY/2;
}

- (CGFloat)minPosY
{
    //화면전체 크기 - 사양서에 지정된 높이 - 몽키&네임테그마진
    //나무가 올라갈수 있는 최소높이
    
    CGFloat posY = [UIScreen mainScreen].bounds.size.height*2 - 350 - kScrollMonkeyNameTagMargin;
    return posY/2;
}


- (void)setIsDeleteMode:(BOOL)isDeleteMode
{
    if (_isDeleteMode == isDeleteMode) {
        return;
    }
    
    _isDeleteMode = isDeleteMode;
    
    _owlTree.isDeleteMode = _isDeleteMode;
    for (int i = 0; i < _MonkeyTrees.count; i ++) {
        DoleMonkeyTree* mTree = (DoleMonkeyTree*)_MonkeyTrees[i];
        mTree.isDeleteMode = _isDeleteMode;
    }
    
    if (_isDeleteMode){
        // change images
        
    }
    else{
        // change images
        [self playAllTreeAnimation];
    }
}

#pragma mark [Delegate] Scrollveiw
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    // any offset changes
    //의미없음
}

// called on finger up if the user dragged. decelerate is true if it will continue moving afterwards
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    NSLog(@"scrollViewDidEndDragging");
    [self playAllTreeAnimation]; 
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView
                     withVelocity:(CGPoint)velocity
              targetContentOffset:(inout CGPoint *)targetContentOffset
{
    NSLog(@"scrollViewWillEndDragging Velocity = %.1f", velocity.x);
    CGFloat pageWidth = 320/3;
    
    CGFloat x = targetContentOffset->x;
    x = roundf(x / pageWidth) * pageWidth;
    targetContentOffset->x = x;
}

#pragma mark [Delegate] clickMonkeyDelegate
-(void)clickMonkeyWithData:(DoleTree*) monkeyTree
{
    NSInteger index = 0;
    for (int i = 0; i < _MonkeyTrees.count; i ++) {
        if(_MonkeyTrees[i] == monkeyTree){
            index = i;
            break;
        }
    }
    [self.homeListDelegate clickMonkeyAtIndex:index];
}

-(void)clickLongPress:(DoleTree*)monkeyTree
{
    NSInteger index = 0;
    for (int i = 0; i < _MonkeyTrees.count; i ++) {
        if(_MonkeyTrees[i] == monkeyTree){
            index = i;
            break;
        }
    }
    [self.homeListDelegate longPressedAtIndex:index];
}

-(void)didCheckedMonkeyWithData:(DoleTree *)monkeyTree Checked:(BOOL)checked;
{
    
    [self.homeListDelegate didCheckedMonkeyWithIndex:index Checked:checked];
}

#pragma mark [Delegate] clickOwlDelegate
-(void)clickAddNickName
{
    [self.homeListDelegate clickAddNewNickname];
}

@end
