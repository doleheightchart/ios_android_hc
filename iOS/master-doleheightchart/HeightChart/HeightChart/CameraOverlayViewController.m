//
//  CameraOverlayViewController.m
//  HeightChart
//
//  Created by ne on 2014. 3. 7..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "CameraOverlayViewController.h"
#import "UIView+ImageUtil.h"
#import "UIViewController+DoleHeightChart.h"
#import "CameraCollectionCell.h"
#import "CaptureSessionManager.h"
#import "PhotoImoprtViewController.h"
#import "MPFoldTransition/DoleTransition.h"
#import "ToastController.h"
#import "LogInViewController.h"
#import "GuideView.h"
#import "DoleCoinPopupViewController.h"
#import "UIViewController+PolyServer.h"
#import "MessageBoxController.h"

#import "Mixpanel.h"

#define kCameraCollectionCell @"CameraCell"

@interface CameraOverlayViewController ()
{
    CaptureSessionManager *_captureManager;
    
    UIImageView *_imageGuideView;
    UIImageView *_imageFocusView;
    
    PhotoToolbarView *_toolbar;
    
    NSArray *_frameNameArray;
    enum DoleCaptureButtonState _captureButtonState;
    enum DoleCaptureButtonState _lockBeforButtonState;
    UIButton * _btnPhotoAlbum;
    UIButton * _btnReTabke;
    
    UIButton * _btnClose;
    
    UICollectionView *_collectionView;
    UICollectionViewFlowLayout *_collectionVeiwLayout;
    
    NSInteger _selectedPage;
    
    
    BOOL    _willCloseThisController;
    
    //Server
    NSMutableData   *_recevedBufferData;
    
    
}
@end

@implementation CameraOverlayViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self loadCameraManager];
    
    [self loadFrames];
    
    [self initCollectionView];
    
    [self addButtons];
    
    [self addCameraCloseButton];
    
    if(self.userConfig.accountStatus == kNotSingUp)
    {
        [_toolbar stateChangeWithLogInOption:NO];
    }
    else{
        [_toolbar stateChangeWithLogInOption:YES];
        [self cameraViewStateShowLockBefore];
    }
}



-(void) viewDidAppear:(BOOL)animated
{
    if(self.userConfig.accountStatus == kNotSingUp)
    {
        [_toolbar stateChangeWithLogInOption:NO];
    }
    else{
        [_toolbar stateChangeWithLogInOption:YES];
        [self cameraViewStateShowLockBefore];
    }
    
    GuideView *gv = [GuideView addGuideTarget:self.view Type:kGuideTypeCameraFrame checkFirstTime:YES];
    if (gv){
        gv.completion = ^{};
    }
}

- (void)closeViewControllerAnimated:(BOOL)animated
{
    _willCloseThisController = YES;
    [self.navigationController popToRootViewControllerWithFoldStyle:0];
}

-(void)initCollectionView
{
    _collectionVeiwLayout = [[UICollectionViewFlowLayout alloc]init];
    _collectionVeiwLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    _collectionVeiwLayout.minimumInteritemSpacing = 0;
    _collectionVeiwLayout.minimumLineSpacing = 0;
    _collectionVeiwLayout.itemSize = self.view.bounds.size;
    
    _collectionView = [[UICollectionView alloc]initWithFrame:self.view.bounds
                                        collectionViewLayout:_collectionVeiwLayout];
    _collectionView.bounces = YES;
    _collectionView.showsHorizontalScrollIndicator = NO;
    _collectionView.showsVerticalScrollIndicator = NO;
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    _collectionView.backgroundColor = [UIColor clearColor] ;
    _collectionView.pagingEnabled = true;
    
    [_collectionView registerClass:[CameraCollectionCell class] forCellWithReuseIdentifier:kCameraCollectionCell];
    [self.view insertSubview:_collectionView aboveSubview:self.imageSaveView];
    [self reloadData];
}


-(void) loadCameraManager
{
    _captureManager = [[CaptureSessionManager alloc]init];
    [_captureManager addVideoInputFrontCamera:NO] ;// set to YES for Front Camera, No for Back camera
    [_captureManager addStillImageOutput];
    [_captureManager addVideoPreviewLayer];
    
    CGRect layerRect = [[[self view] layer] bounds];
    [_captureManager.previewLayer setBounds:layerRect];
    [_captureManager.previewLayer setPosition:CGPointMake(CGRectGetMidX(layerRect),CGRectGetMidY(layerRect))];
    [self.view.layer addSublayer:_captureManager.previewLayer];
    
    __weak CameraOverlayViewController *thisController =self;
    _captureManager.onFocus = ^(bool isFocus){
        [thisController ChangeFocus:isFocus];
    };
}

-(void) ChangeFocus:(bool)isFocus
{
    if (isFocus) {
        _imageFocusView.image = [UIImage imageNamed:@"camera_focus_set"];
    }
    else{
        _imageFocusView.image = [UIImage imageNamed:@"camera_focus_normal"];
    }
}

-(void) addButtons
{
    _btnPhotoAlbum = [UIButton buttonWithType:UIButtonTypeCustom];
    _btnPhotoAlbum.frame = CGRectMake(self.view.bounds.size.width - 102/2 - (18 + 88)/2, (8/2), 44  , 44);
    
    [_btnPhotoAlbum setImage:[UIImage imageNamed:@"camera_btn_album_normal"] forState:UIControlStateNormal];
    [_btnPhotoAlbum setImage:[UIImage imageNamed:@"camera_btn_album_press"] forState:UIControlStateHighlighted];
    [_btnPhotoAlbum addTarget:self action:@selector(loadPhotoAlbum) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:_btnPhotoAlbum];
    
    
    _btnReTabke = [UIButton buttonWithType:UIButtonTypeCustom] ;
    //    _btnReTabke.frame = CGRectMake(276/2,
    //                                   self.view.bounds.size.height - (142+88)/2,
    //                                   44, 44);
    _btnReTabke.frame = CGRectMake(self.view.bounds.size.width - 102/2 - (18 + 88)/2, (8/2), 44  , 44);
    [_btnReTabke setImage:[UIImage imageNamed:@"camera_btn_retake_normal"] forState:UIControlStateNormal];
    [_btnReTabke setImage:[UIImage imageNamed:@"camera_btn_retake_press"] forState:UIControlStateHighlighted];
    [_btnReTabke addTarget:self action:@selector(onClickRetake) forControlEvents:UIControlEventTouchUpInside];
    [_btnReTabke setHidden:YES];
    [self.view addSubview:_btnReTabke];
}

-(void) loadFrames
{
    NSLog(@"[UIScreen mainScreen].bounds.size.height %.1f", [UIScreen mainScreen].bounds.size.height);
    if ([UIScreen mainScreen].bounds.size.height == 568)
    {
        
        _frameNameArray = @[@"frame_01",
                            @"frame_02",
                            @"frame_03",
                            @"frame_04_lock",
                            @"frame_05_lock",
                            @"frame_06_lock",
                            @"frame_07_lock",
                            @"frame_08_lock"
                            ];
        _imageGuideView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"camera_guide"]];
    }
    else{
        _frameNameArray = @[@"frame_01_960",
                            @"frame_02_960",
                            @"frame_03_960",
                            @"frame_04_lock_960",
                            @"frame_05_lock_960",
                            @"frame_06_lock_960",
                            @"frame_07_lock_960",
                            @"frame_08_lock_960"
                            ];
        _imageGuideView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"camera_guide_960"]];
    }
    
    //add ImageSave
    self.imageSaveView = [[UIImageView alloc]init];
    //    self.imageSaveView.contentMode = UIViewContentModeScaleAspectFit;
    self.imageSaveView.contentMode = UIViewContentModeScaleAspectFill;
    
    [self.imageSaveView setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [self.imageSaveView setHidden:YES];
    [self.view addSubview:self.imageSaveView];
    
    
    //add Camera Guide
    //    _imageGuideView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"camera_guide"]];
    [_imageGuideView setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [self.view addSubview:_imageGuideView];
    
    //Add Focus
    CGFloat focusSize = 53.0f;
    _imageFocusView  = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"camera_focus_normal"]];
    [_imageFocusView setFrame:CGRectMake(self.view.frame.size.width/2 - focusSize/2, self.view.frame.size.height/2 - focusSize/2, focusSize, focusSize)];
    [self.view addSubview:_imageFocusView];
    
    _toolbar = [[PhotoToolbarView alloc]initWithPhotoToolbar];
    _toolbar.frame = CGRectMake(0, self.view.frame.size.height - _toolbar.frame.size.height,
                                _toolbar.frame.size.width, _toolbar.frame.size.height);
    
    __weak CameraOverlayViewController* thiscontroller = self;
    
    _toolbar.onClickSelectFrameButton = ^(PhotoButtonControl* btnPho){
        [thiscontroller changeFrame:btnPho];
    };
    
    _toolbar.onClickCenterCameraButton = ^{
        [thiscontroller onClickCameraButton];
    };
    
    _toolbar.onClickShowLogin = ^(BOOL isShowLogInToast) {
        if (isShowLogInToast) {
            [thiscontroller cameraViewStateShowLogInToast];
            
        }
        else{
            [thiscontroller cameraViewStateShowLockBefore];
            
        }
    };
    
    [self.view addSubview:_toolbar];
    [self setCaptureButtonState:DoleCaptureButtonStateCamera];
}



#pragma mark Camera State
-(void)setCaptureButtonState:(enum DoleCaptureButtonState)state
{
    
    _captureButtonState = state;
    [_toolbar setCaptureButtonState:state];
    
    //로그인은 임시 상태이다.
    if (_captureButtonState != DoleCaptureButtonStateLogin) {
        _lockBeforButtonState = _captureButtonState;
    }
    else{
        [_btnReTabke setHidden:YES];
    }
    
}

-(void)onClickRetake
{
    [self cameraViewStateReadyToCapture];
}

-(void)onClickCameraButton
{
    switch (_captureButtonState) {
        case DoleCaptureButtonStateCamera:
            [self cameraViewStateCapturePhoto];
            [self setCaptureButtonState:DoleCaptureButtonStateSave];
            break;
        case DoleCaptureButtonStateDone:
            // Done을 하면 불러온 화면에 띄운다.
            [self cameraViewStateShowSaveImageWithPhotoLibrary];
            [self setCaptureButtonState:DoleCaptureButtonStateSave];
            break;
        case DoleCaptureButtonStateLogin:
            //로그인화면을 띄운다.
            [self showLogInViewController];
            break;
        case DoleCaptureButtonStateSave:
            [self cameraViewStateSaveImageAndCloseForm];
            
            [[Mixpanel sharedInstance] track:@"Recorded child picture"];
            break;
        default:
            break;
    }
}


-(void)cameraViewStateShowSaveImage
{
    [_captureManager stopCaptureRunning];
    
    [_imageGuideView setHidden:NO];
    [_imageFocusView setHidden:YES];
    
    UIImage *image = _captureManager.stillImage;
    //_imageSaveView.image = image;
    [self settingCaptureCameraImage:image];
    [_imageSaveView setHidden:NO];
    [_btnReTabke setHidden:NO];
    [_btnPhotoAlbum setHidden:YES];
    [self setCaptureButtonState:DoleCaptureButtonStateSave];
}

-(void)cameraViewStateShowSaveImageWithPhotoLibrary
{
    [_captureManager stopCaptureRunning];
    [_imageGuideView setHidden:NO];
    [_imageFocusView setHidden:YES];
    
    [_btnReTabke setHidden:NO];
    [_btnPhotoAlbum setHidden:YES];
    
    [self.imageSaveView setHidden:NO];
    [self setCaptureButtonState:DoleCaptureButtonStateSave];
}

-(void)cameraViewStateShowLogInToast
{
    [ToastController showToastWithMessage: NSLocalizedString(@"You need to log in.", "") duration:kToastMessageDurationType_Auto];
    [self setCaptureButtonState:DoleCaptureButtonStateLogin];
}

-(void)cameraViewStateShowLockBefore
{
    if (_lockBeforButtonState == DoleCaptureButtonStateSave) {
        [_btnReTabke setHidden:NO];
    }
    _captureButtonState = _lockBeforButtonState;
    [_toolbar setCaptureButtonState:_captureButtonState];
}

-(void)showLogInViewController
{
    //    UIStoryboard *story = [UIStoryboard storyboardWithName:@"LogIn" bundle:nil];
    //    LogInViewController *vc = [story instantiateViewControllerWithIdentifier:@"LogIn"];
    //
    //    UINavigationController *navi = [[UINavigationController alloc]initWithRootViewController:vc];
    //    navi.navigationBarHidden = YES;
    //
    //    [self modalTransitionController:navi Animated:YES];
    
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"LogIn" bundle:nil];
    LogInViewController *vc = [story instantiateViewControllerWithIdentifier:@"LogIn"];
    vc.requireLoginCompletion = ^(BOOL isLoggedIn){
        if (isLoggedIn){
            
        }
    };
    UINavigationController *navi = [[UINavigationController alloc]initWithRootViewController:vc];
    navi.navigationBarHidden = YES;
    [self presentViewController:navi animated:YES completion:nil];
}

-(void)cameraViewStateCapturePhoto
{
    [_captureManager captureStillImage];
}


- (void)cameraViewStateReadyToCapture
{
    [_captureManager startCaptureRunning];
    [_imageGuideView setHidden:NO];
    [_imageFocusView setHidden:NO];
    [self.imageSaveView setHidden:YES];
    [_btnReTabke setHidden:YES];
    [_btnPhotoAlbum setHidden:NO];
    [self setCaptureButtonState:DoleCaptureButtonStateCamera];
}

- (void)cameraViewStateSaveImageToPhotoAlbum
{
    UIImage *image = [UIImage imageNamed:_frameNameArray[_selectedPage]];
    UIImage *saveImage = [self drawImage:self.imageSaveView.image withBadge:image];
    
    UIImageWriteToSavedPhotosAlbum(saveImage, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
}

- (void)cameraViewStateSaveImageAndCloseForm
{
    
    UIImage *image = [UIImage imageNamed:_frameNameArray[_selectedPage]];
    UIImage *saveImage = [self drawImage:self.imageSaveView.image withBadge:image];
    self.imageSaveView.image = saveImage;
    
    
    //    if (self.userConfig.accountStatus != kUserLogInStatusOffLine &&
    //        self.heightSaveContext.freeQRCode == NO &&
    //        self.heightSaveContext.reuseQRCode == NO){
    //        //서버에 사용쿠폰을 적립한다.
    //
    //        [self requestUseCouponWithCode:self.heightSaveContext.qrCode
    //                                UserNo:self.userConfig.userNo
    //                               AuthKey:self.userConfig.authKey
    //                               OrderID:[self UUID]];
    //    }
    //    else{
    //
    //        _willCloseThisController = YES;
    //
    //        self.heightSaveContext.photo = self.imageSaveView.image;
    //        [self.heightSaveDelegate didTakenPhoto:self.heightSaveContext];
    //    }
    
    if (self.heightSaveContext.freeQRCode == NO &&
        self.heightSaveContext.reuseQRCode == NO){
        //서버에 사용쿠폰을 적립한다.
        
        if (self.userConfig.accountStatus != kUserLogInStatusOffLine){
            [self requestUseCouponWithCode:self.heightSaveContext.qrCode
                                    UserNo:self.userConfig.userNo
                                   AuthKey:self.userConfig.authKey
                                   OrderID:[self UUID]];
        }
        else{
            //로그오프 상태에서도 쿠폰사용을 등록한다.
            //로그오프라 인증정보가 없으므로
            //UserNo = 0 / AuthKey = @""
            [self requestUseCouponWithCode:self.heightSaveContext.qrCode
                                    UserNo:0
                                   AuthKey:@""
                                   OrderID:[self UUID]];
            
        }
    }
    else{
        
        _willCloseThisController = YES;
        
        self.heightSaveContext.photo = self.imageSaveView.image;
        [self.heightSaveDelegate didTakenPhoto:self.heightSaveContext];
    }
}

-(void) changeFrame:(PhotoButtonControl*)btn
{
    
    _selectedPage = btn.type;
    NSIndexPath *path = [NSIndexPath indexPathForRow:btn.type inSection:0];
    [_collectionView selectItemAtIndexPath:path animated:YES scrollPosition:UICollectionViewScrollPositionLeft ];
    
    if (btn.isLockFrame) {
        [self cameraViewStateShowLogInToast];
    }
    else {
        [self cameraViewStateShowLockBefore];
    };
    
}

-(UIImage *)drawImage:(UIImage*)profileImage withBadge:(UIImage *)badge
{
    CGRect dstRect = CGRectMake(0, 0, badge.size.width, badge.size.height); // 960 or 1136 ?
    
    UIGraphicsBeginImageContextWithOptions(dstRect.size, NO, 2.0f);
    
    
    // draw taken image
    //    [profileImage drawInRect:dstRect];
    //    CGRect captureimageRect = CGRectMake(0, 0, profileImage.size.width, profileImage.size.height);
    //    [profileImage drawInRect:captureimageRect];
    
    //CGRect adjustRect = [self getCaptureDrawRectWithImage:profileImage drawRect:dstRect];
    //[profileImage drawInRect:adjustRect];
    CGContextRef context = UIGraphicsGetCurrentContext();
    [self.imageSaveView.layer renderInContext:context];
    // draw photo frame image
    [badge drawInRect:dstRect];
    
    UIImage *resultImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return resultImage;
}

-(CGRect)getCaptureDrawRectWithImage:(UIImage*)profileImage drawRect:(CGRect)dstRect
{
    CGSize imageSize = profileImage.size;
    CGSize dstSize = dstRect.size;
    
    CGFloat mW = imageSize.width / dstSize.width;
    CGFloat mH = imageSize.height / dstSize.height;
    
    CGFloat scale = MIN(mW, mH);
    
    CGSize drawRectSize = CGSizeMake(imageSize.width * scale, imageSize.height * scale);
    
    //    if( mH > mW )
    //        drawRectSize.width = dstSize.height / imageSize.height * imageSize.width;
    //    else if( mW > mH )
    //        drawRectSize.height = dstSize.width / imageSize.width * imageSize.height;
    
    return CGRectMake(0, 0, drawRectSize.width, drawRectSize.height);
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    if (error != NULL) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!" message:@"Image couldn't be saved" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        //CCLOG(@"Error No %@" , error.description);
    }
    else{
        
        self.completion(YES, self);
        
        //에러가 없는경우 .. 이미지 세이브 완료된것을 넘긴다.
        //
        //
        //        [_captureManager closeSession];
        
        if (self.navigationController)
            [self.navigationController popViewControllerAnimated:YES];
        else
            [self dismissViewControllerAnimated:YES completion:nil];
        
        
        //세이브 된이미지 이다.
        //self.imageSaveView.image
        
    }
    //    [self cameraViewStateReadyToCapture];
    
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [_captureManager stopCaptureRunning];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kImageCapturedSuccessfully object:nil];
    
    if (_willCloseThisController){
        [_captureManager closeSession];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cameraViewStateShowSaveImage)
                                                 name:kImageCapturedSuccessfully object:nil];
    
    [_captureManager startCaptureRunning];
}

#pragma mark UIPickerController

-(void) loadPhotoAlbum
{
    
    UIImagePickerController * pickerController = [[UIImagePickerController alloc]init];
    [pickerController setDelegate:self];
    [pickerController setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    [pickerController setAllowsEditing:NO];
    
    [self presentViewController:pickerController animated:YES completion:nil];
    
}

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    [picker dismissViewControllerAnimated:YES completion:^{
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:NO];
        [self showImportPhotoWithImage:image];
    }];
    
    
}

-(void) showImportPhotoWithImage:(UIImage *)importedImage
{
    __weak PhotoImoprtViewController *pvc = [self.storyboard instantiateViewControllerWithIdentifier:@"photoImport"];
    pvc.photo = importedImage;
    
    [self presentViewController:pvc animated:NO completion:nil];
    
    pvc.OnDoneClick = ^(UIImage* savedImage)
    {
        self.imageSaveView.image = savedImage;
        
        CGSize imgSize = savedImage.size;
        NSLog(@"Camera image size is %@", NSStringFromCGSize(imgSize));
        
        
        //        self.imageSaveView.contentMode = UIViewContentModeScaleAspectFit;
        self.imageSaveView.contentMode = UIViewContentModeScaleAspectFill;
        [pvc dismissViewControllerAnimated:NO completion:nil];
        [self cameraViewStateShowSaveImageWithPhotoLibrary];
    };
    
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:nil];
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:NO];
    [self cameraViewStateReadyToCapture];
    
}


#pragma mark CollectionVeiw Delegate
-(void)reloadData
{
    [_collectionView reloadData];
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [_frameNameArray count];
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CameraCollectionCell *cell = [_collectionView dequeueReusableCellWithReuseIdentifier:kCameraCollectionCell
                                                                            forIndexPath:indexPath];
    [cell updateFrameImage:[UIImage imageNamed:_frameNameArray[indexPath.row]]];
    return cell;
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    NSLog(@"scrollViewDidEndDecelerating");
    [self changeFrameButtonType];
}

//-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
//    NSLog(@"scrollViewDidEndDragging");
//
//}
//
//-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
//    NSLog(@"scrollViewDidEndScrollingAnimation");
//}
//
//-(void)scrollViewDidScroll:(UIScrollView *)scrollView
//{
//     //NSLog(@"scrollViewDidScroll");
//}

//- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    NSLog(@"didSelectItemAtIndexPath");
//    // TODO: Select Item
//}
//- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
//    // TODO: Deselect item
//        NSLog(@"didDeselectItemAtIndexPath");
//}

-(void)changeFrameButtonType
{
    CGFloat selectedContentOffset = _collectionView.contentOffset.x;
    NSInteger pagenumber = selectedContentOffset / _collectionVeiwLayout.itemSize.width;
    [_toolbar selectFrameToolBarFrameByType:(enum DolePhotoButtonType)pagenumber];
    
    _selectedPage = pagenumber;
}
- (void)pagingFrameView
{
    
    CGFloat selectedContentOffset = _collectionView.contentOffset.x;
    CGPoint scrollVelocity = [_collectionView.panGestureRecognizer velocityInView:_collectionView.superview];
    BOOL isLeft = false;
    NSInteger page = 0;
    
    NSLog(@"Scroll Velocity X = %f" , scrollVelocity.x);
    if (scrollVelocity.x < 0) {
        // 왼쪽방향
        page = roundf(selectedContentOffset/_collectionVeiwLayout.itemSize.width + 0.2);
        isLeft = true;
    }
    else if(scrollVelocity.x > 0){
        //오른쪽
        page = roundf(selectedContentOffset/_collectionVeiwLayout.itemSize.width - 0.2);
        isLeft =false;
    }
    else{
        page = roundf(selectedContentOffset/_collectionVeiwLayout.itemSize.width );
        isLeft =false;
    }
    CGPoint setOffset = CGPointMake(page * _collectionVeiwLayout.itemSize.width, 0);
    _selectedPage = page;
    [_collectionView setContentOffset:setOffset animated:YES];
    
}



#pragma mark Dole Server


- (void)didConnectionFail:(NSError *)error
{
    [self toastNetworkError];
}

- (void)didCompleteRecevedWithResultType:(DoleServerResultType)resultType ParsedData:(NSDictionary *)recevedData ParseError:(NSError *)error
{
    
    if (recevedData && error==nil && [recevedData[@"Return"] boolValue] ){
        
        if (resultType == kDoleServerResultTypeUseCoinCoupon){
            
            if (self.userConfig.accountStatus != kUserLogInStatusOffLine){
                
                UIStoryboard *story = [UIStoryboard storyboardWithName:@"Popup" bundle:nil];
                DoleCoinPopupViewController *dvc = [story instantiateViewControllerWithIdentifier:@"dolecoinPopup"];
                [dvc dolcoinCount:500];
                dvc.completion = ^(DoleCoinPopupViewController *controller){
                    
                    _willCloseThisController = YES;
                    
                    self.heightSaveContext.photo = self.imageSaveView.image;
                    [self.heightSaveDelegate didTakenPhoto:self.heightSaveContext];
                };
                
                [dvc showModalOnTheViewController:nil];
            }
            else{
                _willCloseThisController = YES;
                
                self.heightSaveContext.photo = self.imageSaveView.image;
                [self.heightSaveDelegate didTakenPhoto:self.heightSaveContext];
            }
            
        }
        
        
    }
    else{
        
        NSInteger errorCode = [recevedData[@"ReturnCode"] integerValue];
        
        [self toastNetworkErrorWithErrorCode:errorCode];
    }
}


- (NSMutableData*)receivedBufferData
{
    if (nil == _recevedBufferData){
        _recevedBufferData = [NSMutableData dataWithCapacity:0];
    }
    
    return _recevedBufferData;
}


-(void)settingCaptureCameraImage:(UIImage*)image
{
    CGSize imgSize = image.size;
    NSLog(@"Camera image size is %@", NSStringFromCGSize(imgSize));
    
    self.imageSaveView.image = image;
    
    NSLog(@"ImageSaveView Bounds = %@", NSStringFromCGRect(self.imageSaveView.frame));
}

@end

