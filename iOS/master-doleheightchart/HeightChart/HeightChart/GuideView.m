//
//  GuideView.m
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 4. 1..
//  Copyright (c) 2014년 Kim Sehyun. All rights reserved.
//

#import "GuideView.h"
#import "UIViewController+DoleHeightChart.h"
#import "AppDelegate.h"
#import "UserConfig.h"
#import "UIColor+ColorUtil.h"
#import "UIFont+DoleHeightChart.h"
#import "UIViewController+DoleHeightChart.h"
#import "UIView+ImageUtil.h"


@implementation GuideView

int guideFontSize ;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        
        if ([self.currentLanguageCode isEqualToString:@"ja"]){
            guideFontSize = 30/2;
        }
        else{
            guideFontSize = 48/2;
        }
    }
    return self;
}

+(instancetype)addGuideTarget:(UIView*)target Type:(GuideType)gType checkFirstTime:(BOOL)firstTime
{


    
    AppDelegate* appController = (AppDelegate*)[UIApplication sharedApplication].delegate;
    UserConfig *config = appController.userConfig;

    
    BOOL isFirstTime = NO;
    
    switch (gType) {
        case kGuideTypeHomeAddnickname: isFirstTime = config.isNeedGuideAddNickname;
            break;
        case kGuideTypeDetail: isFirstTime = config.isNeedGuideDetail;
            break;
        case kGuideTypeInputHeight: isFirstTime = config.isNeedGuideInputHeight;
            break;
        case kGuideTypeCameraFrame: isFirstTime = config.isNeedGuideCameraFrame;
            break;
        case kGuideTypeImportPhoto: isFirstTime = config.isNeedGuideImportPhoto;
            break;
//        case kGuideTypeHomeAddnicknamePaper: isFirstTime = config.isNeedGuideAddnicknamePaper;
//            break;
        case kGuideTypeFirstTimeLogIn: isFirstTime = config.isNeedGuideFirstTimeLogIn;
            break;
        case kGuideTypeAddHeight: isFirstTime = config.isNeedGuideAddHeight;
            break;
    }
    
    if (firstTime && isFirstTime == YES) return nil;
    
    GuideView *gv = [[GuideView alloc]initWithFrame:target.bounds];
    //init
    
    gv.backgroundColor = [UIColor colorWithRGB:0x000000 alpha:0.8];
    
    switch (gType) {
        case kGuideTypeHomeAddnickname: [gv initAddnickname]; break;
        case kGuideTypeDetail: [gv initDetail]; break;
        case kGuideTypeInputHeight: [gv initInputHeight]; break;
        case kGuideTypeCameraFrame: [gv initCameraFrame]; break;
        case kGuideTypeImportPhoto: [gv initImportPhoto]; break;
        case kGuideTypeHomeAddnicknamePaper: [gv initAddnicknamePaper]; break;
        case kGuideTypeFirstTimeLogIn: [gv initFirstTimeLogIn]; break;
        case kGuideTypeAddHeight: [gv initAddHeight]; break;
    }
    
    [target addSubview:gv];
    
    return gv;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    AppDelegate* appController = (AppDelegate*)[UIApplication sharedApplication].delegate;
    UserConfig *config = appController.userConfig;
    
    
    switch (self.guideType) {
        case kGuideTypeHomeAddnickname: config.isNeedGuideAddNickname = YES;
            break;
        case kGuideTypeDetail: config.isNeedGuideDetail = YES;
            break;
        case kGuideTypeInputHeight: config.isNeedGuideInputHeight = YES;
            break;
        case kGuideTypeCameraFrame: config.isNeedGuideCameraFrame = YES;
            break;
        case kGuideTypeImportPhoto: config.isNeedGuideImportPhoto = YES;
            break;
//        case kGuideTypeHomeAddnicknamePaper: config.isNeedGuideAddnicknamePaper = YES;
//            break;
        case kGuideTypeFirstTimeLogIn: config.isNeedGuideFirstTimeLogIn = YES;
            break;
        case kGuideTypeAddHeight: config.isNeedGuideAddHeight = YES;
            break;
    }
    
    [UIView transitionWithView:self.superview duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        [self removeFromSuperview];
    } completion:^(BOOL finished) {
        self.completion();
    }];
}



-(void)initAddnickname
{
    NSString *currentCountryCode = self.currentCountryCode;
    self.guideType = kGuideTypeHomeAddnickname;
    
    if ([currentCountryCode isEqualToString:@"NZ"]){
        [self initAddnicknamePaper];
    }
    else{
        [self initAddnicknameNormal]; 
    }
}

-(void)initAddnicknameNormal
{
    
    UIImageView *handView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"guide_addnickname"]];
    CGFloat y;
    if ([UIScreen isFourInchiScreen]) {
        y = (412-24) /2;
    }
    else{
        y = (320 - 24)/2;
    }
    
    handView.frame = CGRectMake(272/2, y, 278/2, 244/2);

    [self addSubview:handView];
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(294/2, (338+ 30+244+6)/2, 300/2, 120/2)];
    label.backgroundColor = [UIColor clearColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.numberOfLines = 2;
    label.font = [UIFont defaultGuideFondWithSize:guideFontSize];
    label.textColor = [UIColor colorWithRGB:0xffb91c];
    label.text = NSLocalizedString(@"Add a nickname ", @"");
    [self addSubview:label];
}


-(void)initDetail
{
    self.guideType = kGuideTypeDetail;
    
    UIImageView *handView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"guide_detail"]];
    handView.frame = CGRectMake(342/2, 400/2, 278/2, 244/2);
    [self addSubview:handView];
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(294/2, (400+244+6)/2, 326/2, 120/2)];
    label.backgroundColor = [UIColor clearColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont defaultGuideFondWithSize:guideFontSize];
    label.textColor = [UIColor colorWithRGB:0xffb91c];
    label.numberOfLines = 2;
    label.text = NSLocalizedString(@"Record your height ", @"");
    [self addSubview:label];
    
}
-(void)initInputHeight
{
    self.guideType = kGuideTypeInputHeight;
    
    self.backgroundColor = [UIColor clearColor];
    
    CGFloat h = self.bounds.size.height;
    
    UIImageView *handView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"guide_input_height"]];
    handView.frame = CGRectMake(302/2, h - (76+52+6+250)/2, 160/2, 250/2);
    [self addSubview:handView];
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(144/2, h - (76+52)/2, 476/2, 120/2)];
    label.backgroundColor = [UIColor clearColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont defaultGuideFondWithSize:guideFontSize];
    label.textColor = [UIColor colorWithRGB:0xffb91c];
        label.numberOfLines = 2;
    label.text = NSLocalizedString(@"Move up and down to enter your height", @"");
    [self addSubview:label];
    
}
-(void)initCameraFrame
{
    self.guideType = kGuideTypeCameraFrame;

    
   
    CGRect r1,r2;
    NSString *frameName;
    NSString *guideString;
    NSInteger lineCount = 1;
    
    if ([UIScreen isFourInchiScreen]){
        r1 = CGRectMake(181/2, 292/2, 278/2, 244/2);
        r2 = CGRectMake(30/2, (292+244+6)/2, 580/2, 120/2);
        
        frameName = @"frame_01_jungle";
        guideString = @"화면을 좌우로 밀어 사진을 꾸미세요.";
        guideString = NSLocalizedString(@"Slide the screen left and right to decorate the picture", @"");
    }
    else{
         frameName = @"frame_01_jungle_960";
        
        r1 = CGRectMake(181/2, 160/2, 278/2, 244/2);
        r2 = CGRectMake(30/2, (160+244+6)/2, 580/2, 120/2); //두줄
        guideString = @"화면을 좌우로 밀어 \n사진을 꾸미세요.";
        lineCount = 2;
        guideString = NSLocalizedString(@"Slide the screen left and right \nto decorate the picture", @"");
    }
    

    UIImageView *junggle = [[UIImageView alloc]initWithImage:[UIImage imageNamed:frameName]];
    junggle.frame = self.bounds;
    [self addSubview:junggle];
    
    
    UIImageView *handView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"guide_camera_frame"]];
    handView.frame = r1;
    [self addSubview:handView];
    
    
    UILabel *label = [[UILabel alloc]initWithFrame:r2];
    label.backgroundColor = [UIColor clearColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont defaultGuideFondWithSize:guideFontSize];
    label.textColor = [UIColor colorWithRGB:0xffb91c];
    label.numberOfLines = 2;
    label.text = guideString;
    [self addSubview:label];
    
}
-(void)initImportPhoto
{
    self.guideType = kGuideTypeImportPhoto;
    
    CGFloat h = self.bounds.size.height;

    
    UIImageView *handView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"guide_photo_edit"]];
    handView.frame = CGRectMake(181/2, h - (238+52+6+244)/2, 278/2, 244/2);
    [self addSubview:handView];
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(30/2, h - (238+52)/2, 580/2, 120/2)];
    label.backgroundColor = [UIColor clearColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont defaultGuideFondWithSize:guideFontSize];
    label.textColor = [UIColor colorWithRGB:0xffb91c];
    label.text = NSLocalizedString(@"Adjust the picture according to the guideline", @"");
    label.numberOfLines = 2;
    [self addSubview:label];
}

-(void)initAddnicknamePaper
{
//
    
    UIImageView *handView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"guide_addnickname"]];
    
    CGFloat y;
    if ([UIScreen isFourInchiScreen]) {
        y = (412-24 + 4) /2;
    }
    else{
        y = (320 - 24)/2;
    }
    
    handView.frame = CGRectMake(272/2, y, 278/2, 244/2);
    [self addSubview:handView];
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(202/2, (338+ 30+244+6)/2, 418/2, 120/2)];
    label.backgroundColor = [UIColor clearColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont defaultGuideFondWithSize:guideFontSize];
    label.textColor = [UIColor colorWithRGB:0xffb91c];
    label.text = NSLocalizedString(@"Add a nickname ", @"");
    [self addSubview:label];
    
    
    CGFloat h = self.bounds.size.height;
    CGFloat w = self.bounds.size.width;
    
    UIImageView *icon1 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"guide_addnickname_menu"]];
    icon1.frame = CGRectMake(472/2, h - 120/2, 168/2, 120/2);
    [self addSubview:icon1];
    
    UILabel *iconlabel1 = [[UILabel alloc]initWithFrame:CGRectMake(266/2, h - 120/2 - 156/2, w - 266/2 - 34/2, 156/2)];
    iconlabel1.backgroundColor = [UIColor clearColor];
    iconlabel1.textAlignment = NSTextAlignmentCenter;
    iconlabel1.font = [UIFont defaultGuideFondWithSize:guideFontSize];
    iconlabel1.textColor = [UIColor colorWithRGB:0xffb91c];
    iconlabel1.numberOfLines = 3;
    iconlabel1.lineBreakMode = NSLineBreakByWordWrapping;
    iconlabel1.text = NSLocalizedString(@"The Height Chart is \nsent via post mail", @"");
    [self addSubview:iconlabel1];

}
-(void)initFirstTimeLogIn
{
    self.guideType = kGuideTypeFirstTimeLogIn;
    
    
    /////////////////////////////
    UIColor *normalColor = [UIColor colorWithRGB:0x11a6b4];
    
    UIButton *signUpButton = [[UIButton alloc]initWithFrame:CGRectMake(34/2, 0, 572/2, 16)];
    signUpButton.titleLabel.text = NSLocalizedString(@"Sign up", @"");
    signUpButton.titleLabel.font = [UIFont defaultBoldFontWithSize:28/2];
    signUpButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    [signUpButton makeUnderlineStyleWithSelectedColor:normalColor normalColor:normalColor];
    
    
    UIButton *logInFacebookButton = [[UIButton alloc]initWithFrame:CGRectMake(34/2, 0, 572/2, 16)];
    logInFacebookButton.titleLabel.text = NSLocalizedString(@"Log in with Facebook", @"");
    logInFacebookButton.titleLabel.font = [UIFont defaultBoldFontWithSize:28/2];
    logInFacebookButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    [logInFacebookButton makeUnderlineStyleWithSelectedColor:normalColor normalColor:normalColor];
    
    CGRect signUpframe = signUpButton.frame;
    CGRect logInFacebookframe = logInFacebookButton.frame;
    if ([UIScreen isFourInchiScreen]) {
        signUpframe.origin.y = (748 + 96 + 32 + 44) /2;
        signUpButton.frame = signUpframe;
        
        logInFacebookframe.origin.y = (748 + 96 + 32 + 44 + 32 + 44)/2;
        logInFacebookButton.frame = logInFacebookframe;
    }
    else{
        signUpframe.origin.y = (582 + 86 + 32 + 44 -2 )/2;
        signUpButton.frame = signUpframe;
        
        logInFacebookframe.origin.y = (582 + 86 + 32 + 44 + 32 + 44 -4)/2;
        logInFacebookButton.frame = logInFacebookframe;
    }
    
    //이상하게 영문만 겹치는 글위치가 다르다 왜??
    if ([self.currentLanguageCode isEqualToString:@"en"]) {
        signUpframe.origin.x = 32/2;
        signUpButton.frame = signUpframe;
        
        logInFacebookframe.origin.x = 28/2;
        logInFacebookButton.frame = logInFacebookframe;
    }
    
    [self addSubview:signUpButton];
    [self addSubview:logInFacebookButton];
    /////////////////////////////
    
    
    CGFloat h = self.bounds.size.height;
    
    CGFloat iconPosY = h - (58+184+82+162)/2;
    CGFloat iconLabelPosY = h - (58+184+82+162-92-14)/2;
    CGFloat iconW = 128/2;
    CGFloat iconH = 92/2;
    CGFloat iconLabelH = (28*3)/2;
    
    UIImageView *icon1 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"guide_login_coin"]];
    icon1.frame = CGRectMake(112/2, iconPosY, iconW, iconH);
    [self addSubview:icon1];
    
    UILabel *iconlabel1 = [[UILabel alloc]initWithFrame:CGRectMake(112/2, iconLabelPosY, iconW, iconLabelH)];
    iconlabel1.backgroundColor = [UIColor clearColor];
    iconlabel1.textAlignment = NSTextAlignmentCenter;
    iconlabel1.font = [UIFont defaultRegularFontWithSize:24/2];
    iconlabel1.textColor = [UIColor colorWithRGB:0xffffff];
    iconlabel1.numberOfLines = 2;
    iconlabel1.lineBreakMode = NSLineBreakByWordWrapping;
//    iconlabel1.text = @"Dole coin\n적립";
    iconlabel1.text = NSLocalizedString(@"Earn Dole Coins", @"");
    [self addSubview:iconlabel1];


    UIImageView *icon2 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"guide_login_picture"]];
    icon2.frame = CGRectMake((112+128+16)/2, iconPosY, iconW, iconH);
    [self addSubview:icon2];
    
    UILabel *iconlabel2 = [[UILabel alloc]initWithFrame:CGRectMake((112+128+16)/2, iconLabelPosY, iconW, iconLabelH)];
    iconlabel2.backgroundColor = [UIColor clearColor];
    iconlabel2.textAlignment = NSTextAlignmentCenter;
    iconlabel2.font = [UIFont defaultRegularFontWithSize:24/2];
    iconlabel2.textColor = [UIColor colorWithRGB:0xffffff];
    iconlabel2.numberOfLines = 2;
    iconlabel2.lineBreakMode = NSLineBreakByWordWrapping;
//    iconlabel2.text = @"사진 꾸미기";
    iconlabel2.text = NSLocalizedString(@"Decorate picture", @"");
    [self addSubview:iconlabel2];

    UIImageView *icon3 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"guide_login_cloud"]];
    icon3.frame = CGRectMake((112+128+16+128+16)/2, iconPosY, iconW, iconH);
    [self addSubview:icon3];
    
    UILabel *iconlabel3 = [[UILabel alloc]initWithFrame:CGRectMake((112+128+16+128+16)/2, iconLabelPosY, iconW, iconLabelH)];
    iconlabel3.backgroundColor = [UIColor clearColor];
    iconlabel3.textAlignment = NSTextAlignmentCenter;
    iconlabel3.font = [UIFont defaultRegularFontWithSize:24/2];
    iconlabel3.textColor = [UIColor colorWithRGB:0xffffff];
    iconlabel3.numberOfLines = 2;
    iconlabel3.lineBreakMode = NSLineBreakByWordWrapping;
//    iconlabel3.text = @"클라우드에\n저장하기";
    iconlabel3.text = NSLocalizedString(@"Sync to Cloud", @"");

    [self addSubview:iconlabel3];
    
    UIImageView *handView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"guide_login_launcher"]];
    handView.frame = CGRectMake(324/2, h - (58+184)/2, 184/2, 184/2);
    [self addSubview:handView];
    
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(34/2, h - (58+184+82+162+34+104)/2, 572/2, 114/2)];
    label.backgroundColor = [UIColor clearColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont defaultGuideFondWithSize:guideFontSize];
    label.textColor = [UIColor colorWithRGB:0xffb91c];
//    label.text = @"Height chart를 가입해\n특별한 기능을 사용하세요.";
    label.text = NSLocalizedString(@"Register to Height Chart\nto use special functions", @"");
    label.numberOfLines = 2;
    label.lineBreakMode = NSLineBreakByWordWrapping;
    [self addSubview:label];
    
    
    

}

-(void)initAddHeight
{
    self.guideType = kGuideTypeAddHeight;
    
    UIImageView *handView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"guide_addheight"]];
    
    
    
    CGFloat y = self.bounds.size.height - (136+52+6+244)/2.0;
//    if ([UIScreen isFourInchiScreen]) {
//        y = (412-24) /2;
//    }
//    else{
//        y = (320 - 24)/2;
//    }
    
    handView.frame = CGRectMake(82/2, y, 278/2, 244/2);
    
    [self addSubview:handView];
    
    y = self.bounds.size.height - (136+52)/2.0;
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(34/2, y, 374/2, 120/2)];
    label.backgroundColor = [UIColor clearColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.numberOfLines = 2;
    label.font = [UIFont defaultGuideFondWithSize:guideFontSize];
    label.textColor = [UIColor colorWithRGB:0xffb91c];
    label.text = NSLocalizedString(@"Record your height ", @"");
    [self addSubview:label];
}

@end
