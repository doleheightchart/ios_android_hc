//
//  InputHeightViewController.m
//  HeightChart
//
//  Created by mytwoface on 2014. 3. 2..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "InputHeightViewController.h"
#import "UIView+ImageUtil.h"
#import "InputHeightBackground.h"
#import "UIViewController+DoleHeightChart.h"
#import "InputHeightTree.h"
#import "QRTutorialPopupController.h"
#import "UserConfig.h"
#import "MPFoldTransition/DoleTransition.h"
#import "GuideView.h"

@interface InputHeightViewController (){
    UIView  *_closeButton;
    UIView  *_miniTitle;
}

@property (weak, nonatomic) IBOutlet InputHeightBackground *backGround;
@property (weak, nonatomic) IBOutlet InputHeightTree *inputHeightControl;
@end

@implementation InputHeightViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self.inputHeightControl initialize];

    CGSize sizeOfView = self.view.bounds.size;
    NSLog(@"Size Width = %f, Height = %f", sizeOfView.width, sizeOfView.height);
    
//    __weak InputHeightViewController *thisViewController = self;
//    void (^finish)(CGFloat) = self.completion;

    self.inputHeightControl.saveHandler = ^(CGFloat kidHeight){
//        [thisViewController dismissViewControllerAnimated:YES completion:^{
//            //thisViewController.completion(kidHeight);
//            finish(kidHeight);
//        }];
        
//        [thisViewController closeViewControllerAnimated:NO];
//        [self.userConfig recordLastInputHeight:kidHeight];
        
        self.heightSaveContext.inputHeight = kidHeight;
        [self.heightSaveDelegate didInputHeight:self.heightSaveContext];
    };
    
    [self decorateUI];
}

-(void)decorateUI
{
    _closeButton = [self addCommonCloseButton];
    _miniTitle = [self addTitleMiniToLeftTop];
}

-(void)viewDidAppear:(BOOL)animated
{
    
    GuideView *gv = [GuideView addGuideTarget:self.view Type:kGuideTypeInputHeight checkFirstTime:YES];
    
    if (gv){
        _closeButton.layer.opacity = 0;
        _miniTitle.layer.opacity = 0;
        
        UIView *backview = [[UIView alloc]initWithFrame:self.view.bounds];
        backview.backgroundColor = [UIColor colorWithRGB:0x000000 alpha:0.8];
        [self.view insertSubview:backview belowSubview:self.inputHeightControl];
        backview.tag = 1818; //이건좀...
        
        gv.completion = ^{
            
            _closeButton.layer.opacity = 1;
            _miniTitle.layer.opacity = 1;
            
            [backview removeFromSuperview];
            [self.backGround startScrolling];
            [self.backGround startMovingAnimation];
            
            [self updateLastInputHeight];

        };
    }
    else{
        [self.backGround startScrolling];
        [self.backGround startMovingAnimation];
        
        [self updateLastInputHeight];
    }

}

- (void)updateLastInputHeight
{
    if (self.heightSaveContext.saveTargetChild.lastTakenHeight){
        CGFloat lastInputHeight = self.heightSaveContext.saveTargetChild.lastTakenHeight;
        [self.inputHeightControl setInputHeight:lastInputHeight textOutput:YES];
    }
    else {
        [self.inputHeightControl setInputHeight:100 textOutput:NO];
    }
}

-(CGFloat)lastTakenHeightWithMyChild:(MyChild*)myChild
{
    return myChild.lastTakenHeight;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)closeViewControllerAnimated:(BOOL)animated
{
    //[self.navigationController popToRootViewControllerAnimated:animated];
    [self.navigationController popToRootViewControllerWithFoldStyle:0];
}

//- (void)viewWillAppear:(BOOL)animtated {
//    
//    // Register the observer for the keyboardWillShow event
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
//    
//}
//
//- (void)viewWillDisappear:(BOOL)animtated {
//    [[NSNotificationCenter defaultCenter] removeObserver:self];
//}
//
//- (void)keyboardWillShow:(NSNotification *)notification {
//    // create custom buttom
//    UIButton *doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    doneButton.frame = CGRectMake(0, 163, 106, 53);
//    doneButton.adjustsImageWhenHighlighted = NO;
//    [doneButton setImage:[UIImage imageNamed:@"DoneUp.png"] forState:UIControlStateNormal];
//    [doneButton setImage:[UIImage imageNamed:@"DoneDown.png"] forState:UIControlStateHighlighted];
//    [doneButton addTarget:self action:@selector(textFieldShouldReturn:) forControlEvents:UIControlEventTouchUpInside];
//    
//    // locate keyboard view
//    UIWindow *tempWindow = [[[UIApplication sharedApplication] windows] objectAtIndex:1];
//    UIView *keyboard;
//    for (int i = 0; i < [tempWindow.subviews count]; i++) {
//        keyboard = [tempWindow.subviews objectAtIndex:i];
//        // keyboard view found; add the custom button to it
//        if ([[keyboard description] hasPrefix:@"<UIKeyboard"] == YES) {
//            [keyboard addSubview:doneButton];
//        }
//    }
//}
//
//-(BOOL)textFieldShouldReturn:(UITextField *)theTextField {
//    // Set the FirstResponder of the UITextField on the layout
//    [self.textField resignFirstResponder];
//    return YES;
//}

@end
