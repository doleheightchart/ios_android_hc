//
//  ToastControllerViewController.h
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 2. 21..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "OverlayViewController.h"

typedef enum{
    kToastMessageType_YouNeedLogIn,
    kToastMessageType_ChooseDeleteRecord,
    kToastMessageType_NotShownAverageHeight,
    kToastMessageType_ShowAverageHeight,
    kToastMessageType_ThankYouForUseHeight,
    kToastMessageType_Modified,
    kToastMessageType_ReceivedCompleteQuestion,
    kToastMessageType_DeletedComplete,
    kToastMessageType_ChooseDeleteNickname,
    kToastMessageType_NetworkStatusWarning,
}ToastMessageType;

typedef enum {
    kToastMessageDurationType_Auto = -1,
    kToastMessageDurationType_UI_Short = 1,
    kToastMessageDurationType_UI = 3,
    kToastMessageDurationType_Server_Short = 3,
    kToastMessageDurationType_Server_Long = 5
    
}ToastMessageDurationType;

@interface ToastController : NSObject

@property (nonatomic, retain) NSString *message;
@property (nonatomic) NSTimeInterval duration;

+(void)showToastWithMessage:(NSString*)message duration:(NSTimeInterval)duration;
+(void)showToastWithMessage:(NSString*)message duration:(NSTimeInterval)duration completion:(void(^)())completion;
+(void)showToastWithMessage:(NSString *)message duration:(NSTimeInterval)duration onViewController:(UIViewController*)controller;
@end



