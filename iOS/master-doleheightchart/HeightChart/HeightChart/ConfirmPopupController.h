//
//  ConfirmPopupController.h
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 2. 11..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "PopupController.h"

typedef void (^OKCancelHandler)(BOOL);

@interface ConfirmPopupController : PopupController

@property (nonatomic, copy) OKCancelHandler completionBlock;

@property (nonatomic) BOOL isOneButton;

-(void)popupTextUp:(NSString*)upText TextBottom:(NSString*)bottom;

@end
