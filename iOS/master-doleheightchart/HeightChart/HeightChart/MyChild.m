//
//  MyChild.m
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 13..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "MyChild.h"
#import "ChildHeight.h"
#import "KidHeightDetail.h"
#import "NSDate+DoleHeightChart.h"

@implementation MyChild

@dynamic birthday;
@dynamic charterNo;
@dynamic createdDate;
@dynamic isGirl;
@dynamic nickname;
@dynamic heightHistory;

@synthesize lastTakenHeight;
@synthesize maxTakenHeight;

@end

@implementation MyChild(DoleHeightChart)

-(NSArray*)childHeightByHeight
{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    for (ChildHeight* childHeight in self.heightHistory) {
        
        //소수점 사용으로 인해 키값을 근사값으로 변경
        // 120.3 , 120.8 은 둘다 120으로 그룹핑된다.
        NSNumber *heightKey = [NSNumber numberWithFloat:((int)[childHeight.height floatValue])]; // int변환 값 소수점 버림.
        NSMutableArray *array = dic[heightKey];
        if (nil == array){
            array = [NSMutableArray array];
            [dic setObject:array forKey:heightKey];
        }
        
        [array addObject:childHeight];
    }
    
    //키높이 순서로 정렬한다.
    NSArray *sortedKeys = [[dic allKeys] sortedArrayUsingComparator:^NSComparisonResult(NSNumber* heightKey1, NSNumber* heightKey2) {
        return [heightKey1 compare:heightKey2];
    }];
    
    NSArray *sortedArray = [dic objectsForKeys:sortedKeys notFoundMarker:[NSNull null]];
    
    NSMutableArray *resultArray = [NSMutableArray array];
    
    for (NSArray* hs in sortedArray) {
        
        // 같은 그룹안에서는 날짜순으로 기록한다.
        NSArray *sorted2 = [hs sortedArrayUsingComparator:^NSComparisonResult(ChildHeight* obj1, ChildHeight* obj2) {
            return [obj1.takenDate compare:obj2.takenDate];
        }];
        
        KidHeightDetail* kidHeightDetail = [[KidHeightDetail alloc]init];
        
        for (ChildHeight* ch in sorted2) {
            HeightDetailInfo *hdi = [[HeightDetailInfo alloc] initWithChildHeight:ch];
            [kidHeightDetail.takenHeightHistory addObject:hdi];
        }
        
        [resultArray addObject:kidHeightDetail];
    }
    
    return resultArray;
}

-(void)loadLastTakenHeight
{
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"takenDate" ascending:YES comparator:^NSComparisonResult(NSDate* obj1, NSDate* obj2) {
        //return [obj1.takenDate compare:obj2.takenDate];
        return [obj2 compare:obj1];
    }];
    
    NSArray *array = [self.heightHistory sortedArrayUsingDescriptors:@[sort]];
    
    ChildHeight *first = array.firstObject;
    
    self.lastTakenHeight = [first.height floatValue];
}

-(void)refreshMaxTakenHeight
{

    
    
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"height" ascending:YES comparator:^NSComparisonResult(NSNumber* height1, NSNumber* height2) {
        return [height2 compare:height1];
    }];
    
    NSArray *array = [self.heightHistory sortedArrayUsingDescriptors:@[sort]];
    
    ChildHeight *first = array.firstObject;
    
    self.maxTakenHeight = [first.height floatValue];
}

-(NSUInteger)currentAge
{
//    한국식나이계산
    /*
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *comp1 = [calendar components:(NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit| NSHourCalendarUnit | NSMinuteCalendarUnit |NSTimeZoneCalendarUnit)
                                          fromDate:self.birthday];
    NSDateComponents *comp2 = [calendar components:(NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit| NSHourCalendarUnit | NSMinuteCalendarUnit |NSTimeZoneCalendarUnit)
                                          fromDate:[NSDate date]];

    return comp2.year - comp1.year;
    */
    
    NSInteger age = [self.birthday getGlobalAge];
    
    return age;
}


@end
