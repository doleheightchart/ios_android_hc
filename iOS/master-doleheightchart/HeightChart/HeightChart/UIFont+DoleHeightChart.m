//
//  UIFont+DoleHeightChart.m
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 6..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "UIFont+DoleHeightChart.h"

@implementation UIFont (DoleHeightChart)
+(instancetype)defaultBoldFontWithSize:(CGFloat)size
{
//    "DoleFontDefalut"="DroidSansFallback";
//    "DoleFontMedium" = "DroidSansFallback";
//    "DoleFontRegular" = "DroidSansFallback";
//    "DoleFontBold" ="DroidSansFallback";
//    "DoleGuideFont" = "NanumPen";
//    return [UIFont fontWithName: @"Roboto-Bold" size:size];
    return [UIFont fontWithName: NSLocalizedString(@"DoleFontBold", @"") size:size];
}

+(instancetype)defaultRegularFontWithSize:(CGFloat)size
{
//    return [UIFont fontWithName:@"Roboto-Regular" size:size];
    return [UIFont fontWithName: NSLocalizedString(@"DoleFontRegular", @"") size:size];
}

+(instancetype)defaultMediumFontWithSize:(CGFloat)size
{
//    return [UIFont fontWithName:@"Roboto-Medium" size:size];
    return [UIFont fontWithName: NSLocalizedString(@"DoleFontMedium", @"") size:size];
}


+(instancetype)defaultKoreanFontWithSize:(CGFloat)size
{
    //DroidSansFallback.ttf
//    return [UIFont fontWithName:@"DroidSansFallback" size:size];
    return [UIFont fontWithName: NSLocalizedString(@"DoleFontRegular", @"") size:size];
}

+(instancetype)defaultGuideFondWithSize:(CGFloat)size
{
//    return [UIFont fontWithName:@"NanumPen" size:size];
    return [UIFont fontWithName: NSLocalizedString(@"DoleGuideFont", @"") size:size];
}

+(instancetype)defaultFondByLanguageWithSize:(CGFloat)size
{
    //    return [UIFont fontWithName:@"NanumPen" size:size];
    return [UIFont fontWithName: NSLocalizedString(@"DoleByLanguageDefalutFont", @"") size:size];
}

@end
