//
//  LogInAboutServiceViewController.m
//  HeightChart
//
//  Created by ne on 2014. 3. 16..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "LogInAboutServiceViewController.h"
#import "UIView+ImageUtil.h"
#import "TextViewDualPlaceHolder.h"
#import "UIViewController+DoleHeightChart.h"
#import "UIFont+DoleHeightChart.h"
#import "UIColor+ColorUtil.h"
#import "TermsViewController.h"

@interface LogInAboutServiceViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *imageAbout;
@property (weak, nonatomic) IBOutlet UILabel *lblDescript;

@property (weak, nonatomic) IBOutlet UILabel *lblVersion;
@property (weak, nonatomic) IBOutlet UIButton *btnService;



@end

@implementation LogInAboutServiceViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.btnService setResizableImageWithType:DoleButtonImageTypeOrange];
    [self addCommonCloseButton];
    [self setupControlFlexiblePosition];
//    self.lblCoin.font = [UIFont defaultBoldFontWithSize:120/2];
//    self.lblCoin.textColor = [UIColor colorWithRGB:0xff5411];
    
    self.lblDescript.font = [UIFont defaultKoreanFontWithSize:26/2];
    self.lblDescript.textColor = [UIColor colorWithRGB:0x606060];
    
    self.lblVersion.font = [UIFont defaultBoldFontWithSize:24/2];
    self.lblVersion.textColor = [UIColor colorWithRGB:0x00b3ba];
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString* versionNum = [infoDict objectForKey:@"CFBundleShortVersionString"];

    self.lblVersion.text = [NSString stringWithFormat:@"Ver.%@", versionNum];
    
    [self.btnService addTarget:self action:@selector(showTerms) forControlEvents:UIControlEventTouchUpInside];
        
}

-(void)showTerms
{
    TermsViewController * vc = (TermsViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"termsofservice"];
    vc.isJustShowMode = YES; 
    vc.completion = ^(TermsViewController *controller){

    };
    
    [self.navigationController pushViewController:vc animated:NO];
}

- (void)goDoleWebsite
{
    //http://www.dole.co.kr/
    NSLog(@"Go Dole Website");
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.dole.com/"]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setupControlFlexiblePosition
{
    CGFloat imageSize = 442;
    CGFloat lblDescSize = 66;
    CGFloat lblVersionSize = 20;
    
    if ([UIScreen isFourInchiScreen]) {
        
        [self addTitleByBigSizeImage:NO Visible:YES];
        
        CGFloat initMargin = 254 + imageSize + 74;
        
        [self moveToPositionYPixel:(initMargin ) Control:self.lblDescript ];
        [self moveToPositionYPixel:(initMargin + lblDescSize + 56) Control:self.lblVersion];
        [self moveToPositionYPixel:(initMargin + lblDescSize + 56 + lblVersionSize  + 74) Control:self.btnService];
        
    }
    else{
        
        [self addTitleByBigSizeImage:NO Visible:NO];
        
        CGFloat initMargin = 78 + imageSize + 74;
        [self moveToPositionYPixel:(78) Control:self.imageAbout];
        [self moveToPositionYPixel:(initMargin) Control:self.lblDescript];
        [self moveToPositionYPixel:(initMargin + lblDescSize + 56) Control:self.lblVersion];
        [self moveToPositionYPixel:(initMargin + lblDescSize + 56 + lblVersionSize  + 74) Control:self.btnService];

    }
}


@end
