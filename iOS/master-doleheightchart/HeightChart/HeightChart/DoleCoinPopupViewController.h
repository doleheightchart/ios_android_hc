//
//  DoleCoinPopupViewController.h
//  HeightChart
//
//  Created by ne on 2014. 3. 24..
//  Copyright (c) 2014년 Kim Sehyun. All rights reserved.
//

#import "PopupController.h"

@interface DoleCoinPopupViewController : PopupController

@property (nonatomic, copy) void (^completion)(DoleCoinPopupViewController* controller);

-(void)dolcoinCount:(NSInteger)count;


@end
