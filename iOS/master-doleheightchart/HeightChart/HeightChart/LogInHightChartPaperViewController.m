//
//  LogInHightChartPaperViewController.m
//  HeightChart
//
//  Created by ne on 2014. 3. 16..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "LogInHightChartPaperViewController.h"
#import "UIView+ImageUtil.h"
#import "TextViewDualPlaceHolder.h"
#import "UIViewController+DoleHeightChart.h"
@interface LogInHightChartPaperViewController ()
@property (weak, nonatomic) IBOutlet UILabel *lblDescript;
@property (weak, nonatomic) IBOutlet UIImageView *imagePaperImage;
@property (weak, nonatomic) IBOutlet TextViewDualPlaceHolder *txtInputAddress;
@property (weak, nonatomic) IBOutlet UIButton *btnRequest;

@end

@implementation LogInHightChartPaperViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    [self.btnRequest setResizableImageWithType:DoleButtonImageTypeOrange];
    [self.txtInputAddress setDualPlaceHolderTextView:DoleDualPlaceHolderTextTypeAddress];
    [self addCommonCloseButton];
    [self setupControlFlexiblePosition];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)setupControlFlexiblePosition
{
    CGFloat imageSize = 442;
    CGFloat lblDescSize = 138;
    CGFloat TxtAdressSize = 72;

    
    if ([UIScreen isFourInchiScreen]) {
        
        [self addTitleByBigSizeImage:NO Visible:YES];
        
        CGFloat initMargin = 226 ;
        
        [self moveToPositionYPixel:(initMargin) Control:self.lblDescript];
        [self moveToPositionYPixel:(initMargin + lblDescSize + 38) Control:self.imagePaperImage];
        [self moveToPositionYPixel:(initMargin + lblDescSize + 38 + imageSize + 44) Control:self.txtInputAddress];
        [self moveToPositionYPixel:(initMargin + lblDescSize + 38 + imageSize+ 44 + TxtAdressSize + 34) Control:self.btnRequest];
        
        
    }
    else{
        
        [self addTitleByBigSizeImage:NO Visible:NO];
        
        CGFloat initMargin = 50;
        
        [self moveToPositionYPixel:(initMargin) Control:self.lblDescript];
        [self moveToPositionYPixel:(initMargin + lblDescSize + 38) Control:self.imagePaperImage];
        [self moveToPositionYPixel:(initMargin + lblDescSize + 38 + imageSize+ 44) Control:self.txtInputAddress];
        [self moveToPositionYPixel:(initMargin + lblDescSize + 38 + imageSize+ 44 + TxtAdressSize + 34) Control:self.btnRequest];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
