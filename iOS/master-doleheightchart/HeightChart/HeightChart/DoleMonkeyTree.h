//
//  DoleMonkeyTree.h
//  HeightChart
//
//  Created by ne on 2014. 3. 18..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DoleTree.h"
#import "DoleProtocolList.h"

@interface DoleMonkeyTree : DoleTree

@property (nonatomic) BOOL isChecked;
@property (nonatomic) CGFloat height;
@property (nonatomic, copy) NSString *nickname;
//@property (nonatomic) enum DoleMonkeyType monkeyType;
@property (nonatomic, weak) id<clickMonkeyDelegate> clickMonkeyDelegate;
@property (nonatomic) BOOL isDeleteMode;

- (void)playMonkeyAnimation;
- (void)changeMonkeytreeType;

@end
