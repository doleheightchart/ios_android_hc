//
//  InputHeightTree.m
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 2. 28..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "InputHeightTree.h"
#import "UIColor+ColorUtil.h"
#import "UIFont+DoleHeightChart.h"

#import "Mixpanel.h"

#define kWIDTH_HEIGHT_TREE      (46/2)
#define kHEIGHT_HEIGHT_RULLER   (22/2)
#define kRangeOfHeightRuller    (200)

//#define kHeightCollectionCellID @"HeightCollectionCellID"
#define kHeightLabelCellID          @"HeightLabelCellID"
#define kHeightSmallRullerCellID    @"HeightSmallRullerCellID"
#define kHeightBigRullerCellID      @"HeightBigRullerCellID"
#define kBlankSkipCellID            @"BlankSkipCell"

@interface HeightCollectionCell : UICollectionViewCell{
    UILabel *_heightLabel;
}

@property (nonatomic) CGFloat kidHeight;
@end

@implementation HeightCollectionCell

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self){
    }
    
    return self;
}

// 22 * 46 backsize
// 4 * 46 ruller size

-(CGRect)validBounds
{
    return CGRectMake((self.bounds.size.width/2) - (kWIDTH_HEIGHT_TREE/2),
                      0,
                      kWIDTH_HEIGHT_TREE,
                      kHEIGHT_HEIGHT_RULLER);
}

-(void)addRullerWithType:(BOOL)bigRuller
{
    CGRect validBounds = self.validBounds;
    CGRect rullerFrame = CGRectMake(validBounds.origin.x, kHEIGHT_HEIGHT_RULLER/2 - 1, kWIDTH_HEIGHT_TREE, 4/2);
    UIImageView *ruller = [[UIImageView alloc] initWithFrame:rullerFrame];
    
    if (bigRuller)
        ruller.image = [UIImage imageNamed:@"input_height_tree_nod2"];
    else
        ruller.image = [UIImage imageNamed:@"input_height_tree_nod1"];
    
    [self addSubview:ruller];
}

-(void)addRullerLabel
{
//    NSLog(@"frame is %@", NSStringFromCGRect(self.frame));
//    NSLog(@"bound is %@", NSStringFromCGRect(self.bounds));
    
    UIImageView *labelBack = [[UIImageView alloc] initWithFrame:self.validBounds];
    labelBack.image = [UIImage imageNamed:@"detail_tree_cover"];
    [self addSubview:labelBack];
    
    UILabel *label = [[UILabel alloc] initWithFrame:self.validBounds];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"Roboto-Bold" size:10];
    
    //label.font = [UIFont systemFontOfSize:10];
//    label.autoresizingMask = UIViewAutoresizingNone;
//    label.adjustsFontSizeToFitWidth = NO;
    
//    label.adjustsLetterSpacingToFitWidth = NO;
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor colorWithRGB:0xffb528];
    [self addSubview:label];
    
    label.text = [NSString stringWithFormat:@"%f", self.kidHeight, nil];
    
    _heightLabel = label;
    
//    UIImageView *left = [[UIImageView alloc] initWithFrame:self.bounds];
//    left.contentMode = UIViewContentModeLeft;
//    left.image = [UIImage imageNamed:@"left_arrow_fox_normal"];
//    [self addSubview:left];
    
    
}

@end

@interface HeightSmallRullerCell : HeightCollectionCell

@end

@implementation HeightSmallRullerCell

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self){
        [self addRullerWithType:NO];
    }
    
    return self;
}

@end

@interface HeightBigRullerCell : HeightCollectionCell

@end

@implementation HeightBigRullerCell

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self){
        [self addRullerWithType:YES];
    }
    
    return self;
}

@end

@interface HeightLabelRullerCell : HeightCollectionCell
-(void)updateHeightLabel;
@end

@implementation HeightLabelRullerCell

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self){
        [self addRullerLabel];
    }
    
    return self;
}

-(void)updateHeightLabel
{
    _heightLabel.text = [NSString stringWithFormat:@"%d", (int)self.kidHeight, nil];
}

@end

@interface HeightTextField : UITextField
@end
@implementation HeightTextField
// placeholder position
- (CGRect)textRectForBounds:(CGRect)bounds {
    return CGRectMake(80/2, 19.0/2.0, 126/2, 52/2);
}
// text position
- (CGRect)editingRectForBounds:(CGRect)bounds {
    //return CGRectInset( bounds , -20 , -20 );
    
    return CGRectMake(80/2, 19.0/2.0, 126/2, 52/2);
}
@end

@interface InputHeightTree (){

    UICollectionView    *_heightTreeView;

    UITextField         *_inputHeightText;
    UIButton            *_saveButton;
    UILabel             *_placeHolderLabel;
    UILabel             *_rightCmLabel;
    
    UIImageView         *_backTreeView;
    CGFloat             _ratioHeight;
}


@end

@implementation InputHeightTree

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
//        [self initialize];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self){
//        [self initialize];
    }
    
    return self;
}

-(void)initialize
{
    self.backgroundColor = [UIColor clearColor];

    _ratioHeight = 1;
    
    CGRect viewRect = CGRectMake(self.bounds.size.width - (kWIDTH_HEIGHT_TREE/2),
                                 0,
                                 kWIDTH_HEIGHT_TREE,
                                 self.bounds.size.height);
    
    viewRect = self.bounds;
    
    _backTreeView = [[UIImageView alloc]initWithFrame:CGRectMake(self.bounds.size.width / 2 - (kWIDTH_HEIGHT_TREE/2),
                                                                 0,
                                                                 kWIDTH_HEIGHT_TREE,
                                                                 self.bounds.size.height)];
    _backTreeView.image = [UIImage imageNamed:@"detail_tree"];
    [self addSubview:_backTreeView];
    
    // Create Collection Tree
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    flowLayout.minimumInteritemSpacing = 0;
    flowLayout.minimumLineSpacing = 0;
    flowLayout.itemSize = CGSizeMake(self.bounds.size.width, kHEIGHT_HEIGHT_RULLER );
    //flowLayout.itemSize = CGSizeMake(kWIDTH_HEIGHT_TREE, kHEIGHT_HEIGHT_RULLER );
    flowLayout.sectionInset = UIEdgeInsetsMake(viewRect.size.height/2, 0, viewRect.size.height/2, 0);
    
    _heightTreeView = [[UICollectionView alloc] initWithFrame:viewRect
                                         collectionViewLayout:flowLayout];
    
    _heightTreeView.bounces = NO;
    _heightTreeView.showsHorizontalScrollIndicator = NO;
    _heightTreeView.showsVerticalScrollIndicator = NO;
    _heightTreeView.delegate = self;
    _heightTreeView.dataSource = self;
    _heightTreeView.backgroundColor = [UIColor clearColor];
    
    //[_heightTreeView registerClass:[HeightCollectionCell class] forCellWithReuseIdentifier:kHeightCollectionCellID];
    [_heightTreeView registerClass:[HeightSmallRullerCell class] forCellWithReuseIdentifier:kHeightSmallRullerCellID];
    [_heightTreeView registerClass:[HeightBigRullerCell class] forCellWithReuseIdentifier:kHeightBigRullerCellID];
    [_heightTreeView registerClass:[HeightLabelRullerCell class] forCellWithReuseIdentifier:kHeightLabelCellID];
    [_heightTreeView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:kBlankSkipCellID];
    
    [self addSubview:_heightTreeView];
    
    [_heightTreeView reloadData];

    [self sendActionsForControlEvents:UIControlEventValueChanged];
    
    
    // Create InputBox
    
    CGFloat centerX = self.bounds.size.width / 2;
    CGFloat centerY = self.bounds.size.height / 2;
    
    CGFloat inputBoxWidth = 286/2;
    CGFloat inputBoxHeight = 90/2;
    
    CGRect inputBoxFrame = CGRectMake(centerX - (inputBoxWidth/2) + 0.5,
                                      centerY - (inputBoxHeight/2),
                                      inputBoxWidth,
                                      inputBoxHeight);
    
    UIImageView *inputBox = [[UIImageView alloc]initWithFrame:inputBoxFrame];
    inputBox.image = [UIImage imageNamed:@"input_height_input_box"];
    [self addSubview:inputBox];
    
//    CGRect inputTextFrame = CGRectMake(centerX - (126/2)/2 ,
//                                       centerY - (52/2)/2,
//                                       126/2,
//                                       52/2);
    
//    CGRect inputTextFrame = CGRectMake(centerX - (202/2)/2 ,
//                                       centerY - (52/2)/2,
//                                       202/2,
//                                       52/2);
    
    CGRect inputTextFrame = CGRectMake(centerX - (126/2)/2 ,
                                       centerY - (52/2)/2,
                                       126/2,
                                       52/2);
    
    UITextField *inputText = [[UITextField alloc] initWithFrame:inputTextFrame];
//    UITextField *inputText = [[HeightTextField alloc] initWithFrame:inputBoxFrame];

    
//    inputText.placeholder = @"Input Height";
    inputText.returnKeyType = UIReturnKeyDone;
    inputText.delegate = self;
    inputText.textAlignment = NSTextAlignmentCenter;
    inputText.keyboardType = UIKeyboardTypeDecimalPad;
    inputText.backgroundColor = [UIColor clearColor];
    


//    _placeHolderLabel = [[UILabel alloc]initWithFrame:CGRectMake(centerX - (202/2/2), centerY - (34/2/2), 202/2, 34/2)];
    _placeHolderLabel = [[UILabel alloc]initWithFrame:CGRectMake(centerX - (212/2/2), centerY - (42/2/2), 212/2, 38/2)];
    
//    _placeHolderLabel = [[UILabel alloc]initWithFrame:CGRectMake(42/2, 28/2, 202/2, 34/2)];  //leftviewmode
    _placeHolderLabel.backgroundColor = [UIColor clearColor];
    _placeHolderLabel.font = [UIFont defaultBoldFontWithSize:30/2];
    _placeHolderLabel.textAlignment = NSTextAlignmentCenter;
    _placeHolderLabel.textColor = [UIColor colorWithRGB:0xd0d1d5];
    _placeHolderLabel.text = NSLocalizedString(@"Enter your height", @"");
    _placeHolderLabel.adjustsFontSizeToFitWidth = YES;
    [self addSubview:_placeHolderLabel];

    _rightCmLabel = [[UILabel alloc]initWithFrame:CGRectMake(centerX + (126/2/2) + (3.0/2.0), centerY - (52/2/2), 53/2.0, 52/2)];
//    _rightCmLabel = [[UILabel alloc]initWithFrame:CGRectMake((80+126+3)/2.0, 19.0/2.0, 53/2.0, 52/2)]; //rightViewMode
    
    _rightCmLabel.backgroundColor = [UIColor clearColor];
    _rightCmLabel.font = [UIFont defaultBoldFontWithSize:30/2];
    _rightCmLabel.textAlignment = NSTextAlignmentCenter;
    _rightCmLabel.textColor = [UIColor colorWithRGB:0x6d402c];
    _rightCmLabel.text = @"cm";
    [self addSubview:_rightCmLabel];

//    inputText.rightViewMode = UITextFieldViewModeWhileEditing;
//    inputText.rightView = _rightCmLabel;
//    inputText.leftViewMode = UITextFieldViewModeUnlessEditing;
//    inputText.leftView = _placeHolderLabel;

    inputText.font = [UIFont defaultBoldFontWithSize:48/2];
    inputText.textColor = [UIColor colorWithRGB:0xff5411];
    inputText.textAlignment = NSTextAlignmentCenter;
    
    [inputText addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    [self addSubview:inputText];
    _inputHeightText = inputText;
    
    
    CGFloat saveButtonWidth = 96/2;
    CGFloat saveButtonHeight = 96/2;
    CGRect saveButtonFrame = CGRectMake(centerX + (inputBoxWidth/2) + 10,
                                        centerY - (saveButtonHeight/2),
                                        saveButtonWidth,
                                        saveButtonHeight);
    
    UIButton *saveButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    saveButton.contentMode = UIViewContentModeCenter;
    saveButton.frame = saveButtonFrame;
//    saveButton.titleLabel.text = @"Save";
    saveButton.titleLabel.textColor = [UIColor colorWithRGB:0xffffff];
    saveButton.titleLabel.font = [UIFont defaultBoldFontWithSize:32/2];
    
    UIImage *saveButtonImageNoraml = [UIImage imageNamed:@"input_height_btn_save_normal"];
    UIImage *saveButtonImagePress = [UIImage imageNamed:@"input_height_btn_save_press"];
    [saveButton setBackgroundImage:saveButtonImageNoraml forState:UIControlStateNormal];
    [saveButton setBackgroundImage:saveButtonImagePress forState:UIControlStateHighlighted];
//    [saveButton setImage:saveButtonImageNoraml forState:UIControlStateNormal];
//    [saveButton setImage:saveButtonImagePress forState:UIControlStateHighlighted];
    UILabel *textLabel = [[UILabel alloc]initWithFrame:CGRectMake(10/2, 30/2, 74/2, 36/2)];
    textLabel.text = NSLocalizedString(@"Save", @"");
    textLabel.backgroundColor = [UIColor clearColor];
    textLabel.textColor = [UIColor colorWithRGB:0xffffff];
    textLabel.textAlignment = NSTextAlignmentCenter; 
    textLabel.font = [UIFont defaultBoldFontWithSize:32/2];
    [saveButton addSubview:textLabel];
    

    [saveButton addTarget:self action:@selector(clickSave:) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:saveButton];
    _saveButton = saveButton;
    
    [self createMonkeys];
    
    self.height = 100;
    [_heightTreeView selectItemAtIndexPath:[NSIndexPath indexPathForRow:100 inSection:0]
                                  animated:NO
                            scrollPosition:UICollectionViewScrollPositionCenteredVertically];
    
    _inputHeightText.text = nil;
    _placeHolderLabel.hidden = NO;
    _rightCmLabel.hidden = YES;
    _saveButton.hidden = YES;
}

-(void)createMonkeys
{
    CGFloat centerY = self.bounds.size.height / 2;
    
    CGRect monkeyFrame1 = CGRectMake((204/2) + 0.5,
                                     centerY - 225,
                                     (142/2),
                                     (162/2));
    
    UIImageView *monkey1 = [[UIImageView alloc] initWithFrame:monkeyFrame1];
    monkey1.image = [UIImage imageNamed:@"input_height_monkey1"];
    [self addSubview:monkey1];

    CGRect monkeyFrame2 = CGRectMake((290/2),
                                     centerY - 148,
                                     (150/2),
                                     (210/2));
    
    UIImageView *monkey2 = [[UIImageView alloc] initWithFrame:monkeyFrame2];
    monkey2.image = [UIImage imageNamed:@"input_height_monkey2"];
    [self addSubview:monkey2];
    
    CGRect monkeyFrame3 = CGRectMake((204/2) - 1.5,
                                     centerY + 88,
                                     (150/2),
                                     (160/2));
    
    UIImageView *monkey3 = [[UIImageView alloc] initWithFrame:monkeyFrame3];
    monkey3.image = [UIImage imageNamed:@"input_height_monkey3"];
    [self addSubview:monkey3];
}

#pragma mark CollectionView

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return  kRangeOfHeightRuller * _ratioHeight;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < 0 || indexPath.row > (kRangeOfHeightRuller  * _ratioHeight) ) {
        return [collectionView dequeueReusableCellWithReuseIdentifier:kBlankSkipCellID forIndexPath:indexPath];
    }
    
    NSInteger rullerIndex = indexPath.row ;
    rullerIndex = (kRangeOfHeightRuller * _ratioHeight) - rullerIndex;
    
    HeightCollectionCell *cell;
    
    if ( (rullerIndex % 10) == 0 ){
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:kHeightLabelCellID forIndexPath:indexPath];
    }
    else if ((rullerIndex % 5) == 0){
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:kHeightBigRullerCellID forIndexPath:indexPath];
    }
    else{
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:kHeightSmallRullerCellID forIndexPath:indexPath];
    }
    
    cell.kidHeight = rullerIndex / _ratioHeight;
    
    if ([cell isKindOfClass:[HeightLabelRullerCell class]]){
        HeightLabelRullerCell *labelCell = (HeightLabelRullerCell*)cell;
        [labelCell updateHeightLabel];
    }
    
    return cell;
}

#pragma mark Action and method

-(IBAction)clickSave:(id)sender
{
    [_inputHeightText resignFirstResponder];
    
    [self notifySave];
    
    //longzhe cui added
    [[Mixpanel sharedInstance] track:@"Recorded Height"];
}

-(CGFloat)currentInputHeight
{
    double offSetY = _heightTreeView.contentOffset.y;
    double offSetIndex = offSetY / (double)kHEIGHT_HEIGHT_RULLER;

    //소수점 199.9xxxxxx xxxx부분은 없애야 한다.
//    offSetIndex = (NSInteger)(offSetIndex*10.0);
//    offSetIndex *= 0.1;
    
    double result = (kRangeOfHeightRuller - offSetIndex);
    return result;
    
    //return (kRangeOfHeightRuller - offSetIndex);
}
-(CGFloat)contentOffSetYAtInputHeight:(CGFloat)inputHeight
{
    NSInteger indexOfHeight = inputHeight;
    indexOfHeight += kRangeOfHeightRuller;
    
    return indexOfHeight * kHEIGHT_HEIGHT_RULLER;
}

-(void)notifySave
{
//    self.height = (int)_heightTreeView.contentOffset.y;
    CGFloat inputHeight = [self currentInputHeight];
    double h2 = inputHeight * 10.0;
    NSInteger intHeightValue = h2;
    inputHeight = intHeightValue * 0.1;
    self.saveHandler(inputHeight);
}

-(void)updateControls
{
    BOOL existText = _inputHeightText.text.length > 0;
    _placeHolderLabel.hidden = existText;
    _rightCmLabel.hidden = !existText;
    _saveButton.hidden = !existText;
}

#pragma mark scroll methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView;
{
    if (_inputHeightText.isFirstResponder == NO)
        [self updateTextFieldWithCurrentRullerHeight];
    else{
//        [_inputHeightText resignFirstResponder];
//        [self updateTextFieldWithCurrentRullerHeight];
    }
}

-(void)updateTextFieldWithCurrentRullerHeight
{
    _inputHeightText.text = [NSString stringWithFormat:@"%.1f", [self currentInputHeight], nil];
//    self.height = [self currentInputHeight];
//    _placeHolderLabel.hidden = YES;
    [self updateControls];
    
    NSLog(@"Scroll Content Offset is %@", NSStringFromCGPoint(_heightTreeView.contentOffset));
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if (_inputHeightText.isFirstResponder)
        [_inputHeightText resignFirstResponder];
}

- (void) scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    NSLog(@"-->>> scrollViewDidEndDragging is decelerate is %@", decelerate ? @"YES" : @"NO");
    
    
    if (decelerate == YES) return;
    
//소수점 살리기 위해 안한다.
//    [self autoAdjustRullerWithScale:1 Animated:YES];
}

- (void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    //소수점 살리기 위해 안한다.
//    [self autoAdjustRullerWithScale:5 Animated:YES];
}

- (void)autoAdjustRullerWithScale:(CGFloat)scale Animated:(BOOL)animated
{
    NSInteger offSetIndex = (_heightTreeView.contentOffset.y) / kHEIGHT_HEIGHT_RULLER;
    offSetIndex /= scale;
    offSetIndex *= scale;
    CGFloat moveToY = (offSetIndex * kHEIGHT_HEIGHT_RULLER);
    
    if (animated){
        [UIView animateWithDuration:0.3
                         animations:^{
                             CGPoint point = CGPointMake(0, moveToY);
                             _heightTreeView.contentOffset = point;
                         }
                         completion:^(BOOL finished) {
                             
                         }
         ];
    }
    else{
        CGPoint point = CGPointMake(0, moveToY);
        _heightTreeView.contentOffset = point;
    }
    
}

-(void)updateScrollWithHeight:(CGFloat)kidHeight Animated:(BOOL)animated
{
    CGFloat reverseHeight = kRangeOfHeightRuller - kidHeight;
    CGFloat offSetY = (reverseHeight / kRangeOfHeightRuller) * (kHEIGHT_HEIGHT_RULLER*kRangeOfHeightRuller);
    CGPoint contentOffSet = CGPointMake(0, offSetY);
    
    if (animated){
        [UIView animateWithDuration:0.3
                         animations:^{
                             _heightTreeView.contentOffset = contentOffSet;
                         }
                         completion:^(BOOL finished) {
                         }
         ];
    }
    else{
        _heightTreeView.contentOffset = contentOffSet;
    }
}

#pragma mark Text Delegate

-(void)textFieldDidChange:(UITextField *)textField
{
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc]init];
    NSNumber *inputHeight = [formatter numberFromString:textField.text];
    
    if (inputHeight == nil) return;
    
    CGFloat kidHeight = [inputHeight floatValue];
    [self updateScrollWithHeight:kidHeight Animated:YES];
    
//    NSInteger index = [inputHeight intValue];
//    index = 200 - index;
//    
//    index = MIN(200, index);
//    index = MAX(0, index);
//    
//    [_heightTreeView selectItemAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]
//                                  animated:YES
//                            scrollPosition:UICollectionViewScrollPositionCenteredVertically];
    
    
    // update placeholder / savebutton
    [self updateControls];

}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField // return NO to disallow editing.
{
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField           // became first responder
{
//    _placeHolderLabel.hidden = YES;
//    _rightCmLabel.hidden = NO;
//    _saveButton.hidden = NO;
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField          // return YES to allow editing to stop and to resign first responder status. NO to disallow the editing session to end
{
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField             // may be called if forced even if shouldEndEditing returns NO (e.g. view removed from window) or endEditing:YES called
{
////    _placeHolderLabel.hidden = YES;
//    _rightCmLabel.hidden = textField.text.length <= 0;
//    _saveButton.hidden = textField.text.length <= 0;
//    _placeHolderLabel.hidden = textField.text.length > 0;
////    _placeHolderLabel.hidden = (textField.text.length < 0);
    
    [self updateControls];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *changedText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (NO == [changedText isEqual:@""]){

        NSNumberFormatter *formatter = [[NSNumberFormatter alloc]init];
        NSNumber *inputHeight = [formatter numberFromString:changedText];
        
        if (inputHeight == nil) return NO;
        
        CGFloat kidHeight = [inputHeight floatValue];
        
        if (kidHeight < 0 || kidHeight > kRangeOfHeightRuller ) return NO;
        
        NSError *error = NULL;
        NSString *pattern = @"([0-9]{0,3}.[0-9]{0,1})";
        
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:NSRegularExpressionCaseInsensitive error:&error];
        
        if (error)
        {
            NSLog(@"Couldn't create regex with given string and options");
        }
        
        if (1 != [regex numberOfMatchesInString:changedText options:0 range:NSMakeRange(0, [changedText length])])
            return NO;
        
//        NSError *error;
//        NSRegularExpression* exp = [NSRegularExpression regularExpressionWithPattern:@"(put)-(expression)-(here)" options: NSRegularExpressionSearch error:&error];
//        
//        if (error) {
//            NSLog(@"%@", error);
//        } else {
//            
//            NSTextCheckingResult* result = [exp firstMatchInString:testString options:0 range:NSMakeRange(0, [productDescription length] ) ];
//            
//            if (result) {
//                
//                NSRange groupOne = [result rangeAtIndex:1]; // 0 is the WHOLE string.
//                NSRange groupTwo = [result rangeAtIndex:2];
//                NSRange groupThree = [result rangeAtIndex:3];
//                
//                NSLog( [testString substringWithRange:groupOne] );
//                NSLog( [testString substringWithRange:groupTwo] );
//                NSLog( [testString substringWithRange:groupThree] );
//            } 
//        }
        

    }
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return (newLength > 5) ? NO : YES;
    
    
}


// return NO to not change text

//- (BOOL)textFieldShouldClear:(UITextField *)textField;               // called when clear button pressed. return NO to ignore (no notifications)
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return YES;
}
// called when 'return' key pressed. return NO to ignore.


-(void)setInputHeight:(CGFloat)height textOutput:(BOOL)isTextoutput;
{
    if (isTextoutput){
        _inputHeightText.text = [NSString stringWithFormat:@"%.1f", height];
        _placeHolderLabel.hidden = YES;
    }
    [self updateScrollWithHeight:height Animated:YES];
    if (isTextoutput == NO){
        _inputHeightText.text = @"";
        _rightCmLabel.hidden = YES;
        _placeHolderLabel.hidden = NO;
        _saveButton.hidden = YES;
    }
}



@end
