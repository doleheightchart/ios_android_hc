//
//  DualPlaceHolderTextField.m
//  HeightChart
//
//  Created by mytwoface on 2014. 2. 21..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "DualPlaceHolderTextField.h"

//RGB color macro
#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

//RGB color macro with alpha
#define UIColorFromRGBWithAlpha(rgbValue,a) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:a]


@interface DualPlaceHolderTextField()
{
    CGFloat _defalutfontSize;
}
@end
@implementation DualPlaceHolderTextField

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        _defalutfontSize = 30/2;

    }
    return self;
}

-(void)setReDrawPlaceHolder:(NSString*)firstText SecondText:(NSString*)secondText
{
    self.firtstPlaceHolder = firstText;
    self.secondPlaceHodler = secondText;

    self.placeholder = firstText;
}

-(void)drawPlaceholderInRect:(CGRect)rect
{

    if (self.isMultiLine) {
        [self drawPlaceholderInRectMultiLine:rect];
        return;
    }
    //    self.placeholder = @"Nickname 1~15charactors";
    //    self.textColor = UIColorFromRGB(0xffffff);
    
    NSString* strFirst = self.firtstPlaceHolder;
    NSString* strSecond = self.secondPlaceHodler;
    
    self.placeholder = [NSString stringWithFormat:@"%@ %@", strFirst, strSecond];
    
    NSMutableParagraphStyle *paraGraphStyle = [[NSMutableParagraphStyle alloc]init];
    paraGraphStyle.alignment = NSTextAlignmentLeft;
    
    //#9fa0a3
    //#c2c2c2
    
    //Size, Color
    NSString *fontName = @"Roboto-Regular";
    NSInteger fontSize = _defalutfontSize;
    UIColor *fontColor = UIColorFromRGB(0x9fa0a3);
    
    NSDictionary *dicFirst = @{
                               NSFontAttributeName:[UIFont fontWithName:fontName size:fontSize],
                               NSForegroundColorAttributeName:fontColor
                               };
    
    fontSize = 11;
    fontColor = UIColorFromRGB(0xc2c2c2);
    NSDictionary *dicSecond = @{
                                NSFontAttributeName:[UIFont fontWithName:fontName size:11],
                                NSForegroundColorAttributeName:fontColor
                                };
    
    
    NSMutableAttributedString *strAttribute =
    [[NSMutableAttributedString alloc] initWithString:self.placeholder attributes:dicFirst];
    //
    [strAttribute addAttributes:dicSecond range:NSMakeRange(strFirst.length+1, strSecond.length)];
    //
    [strAttribute addAttribute:NSParagraphStyleAttributeName value:paraGraphStyle range:NSMakeRange(0, strSecond.length)];
    
    //        CCLOG(@"Rect %f  %f", rect.size.width, rect.size.height);
    //
    CGRect r1 = [strAttribute boundingRectWithSize:rect.size options:NSStringDrawingUsesFontLeading context:nil];
    //
    //    CGSize sizeR1 = [strAttrFirst size];
//    r1.origin.y = (rect.size.height / 2) - (r1.size.height/2);
    r1.origin.y = (rect.size.height / 2) - (r1.size.height* 0.55);
    
    //    CCLOG(@"Rect %f  %f", rect.size.width, r1.size.height);
    
    [strAttribute drawInRect:r1];
    
}

-(void)setReDrawPlaceHolder:(NSString*)firstText SecondTopText:(NSString*)secondTopText SecondBottomText:(NSString*)secondBottomText
{
    self.isMultiLine = YES;
    self.firtstPlaceHolder = firstText;
    self.secondPlaceHodler = secondTopText;
    self.secondBottomPlaceHolder = secondBottomText;
    
    self.placeholder = firstText;
}
-(void)drawPlaceholderInRectMultiLine:(CGRect)rect
{
    
    NSString* strFirst = self.firtstPlaceHolder;
    NSString* strSecond = self.secondPlaceHodler;
    NSString* strSeconBottom = self.secondBottomPlaceHolder; 
    
    self.placeholder = [NSString stringWithFormat:@"%@ %@ %@", strFirst, strSecond, strSeconBottom];
    
    NSMutableParagraphStyle *paraGraphStyle = [[NSMutableParagraphStyle alloc]init];
    paraGraphStyle.alignment = NSTextAlignmentLeft;
    
    //#9fa0a3
    //#c2c2c2
    
    //Size, Color
    NSString *fontName = @"Roboto-Regular";
    NSInteger fontSize = _defalutfontSize;
    UIColor *fontColor = UIColorFromRGB(0x9fa0a3);
    
    
    
    NSDictionary *dicFirst = @{
                           NSFontAttributeName:[UIFont fontWithName:fontName size:fontSize],
                           NSForegroundColorAttributeName:fontColor
                           };
    
    fontSize = 11;
    fontColor = UIColorFromRGB(0xc2c2c2);
    NSDictionary *dicSecond = @{
                           NSFontAttributeName:[UIFont fontWithName:fontName size:11],
                           NSForegroundColorAttributeName:fontColor
                           };
    
    
    NSMutableAttributedString *strAttribute =
    [[NSMutableAttributedString alloc] initWithString:strFirst attributes:dicFirst];
    [strAttribute addAttribute:NSParagraphStyleAttributeName value:paraGraphStyle range:NSMakeRange(0, strFirst.length)];
    CGRect r1 = [strAttribute boundingRectWithSize:rect.size options:NSStringDrawingUsesFontLeading context:nil];
    r1.origin.y = (rect.size.height / 2) - (r1.size.height/2);
    
    [strAttribute drawInRect:r1];
    
    
    
    NSMutableAttributedString *strAttributeTop =
    [[NSMutableAttributedString alloc] initWithString:strSecond attributes:dicSecond];
    [strAttributeTop addAttribute:NSParagraphStyleAttributeName value:paraGraphStyle range:NSMakeRange(0, strSecond.length)];
    
    CGRect r2 = [strAttribute boundingRectWithSize:rect.size options:NSStringDrawingUsesFontLeading context:nil];
    r2.origin.y =  (rect.size.height / 2) - (r1.size.height)*0.7; ;
    r2.origin.x = r1.origin.x + r1.size.width + 10;
    [strAttributeTop drawInRect:r2];
   
    
    NSMutableAttributedString *strAttributeBottom =
    [[NSMutableAttributedString alloc] initWithString:strSeconBottom attributes:dicSecond];
    [strAttributeBottom addAttribute:NSParagraphStyleAttributeName value:paraGraphStyle range:NSMakeRange(0, strSeconBottom.length)];
    
    CGRect r3 = [strAttribute boundingRectWithSize:rect.size options:NSStringDrawingUsesFontLeading context:nil];
    r3.origin.y = (rect.size.height / 2) - (r1.size.height)*0.1;
    r3.origin.x = r1.origin.x + r1.size.width  + 10 ;
    [strAttributeBottom drawInRect:r3];
    


}


-(void)setDualPlaceHoler
{
    //1. Nickname 1~15charactor
    //2. Password Over 4
    
    
    
    NSString* strSecond = @"Password Over 4";
    
    NSMutableParagraphStyle *paraGraphStyle = [[NSMutableParagraphStyle alloc]init];
    paraGraphStyle.alignment = NSTextAlignmentCenter;
    

    //#9fa0a3
    //#c2c2c2
    
    self.textColor = UIColorFromRGB(0xffffff);
    
    NSDictionary *dic1 = @{
                           NSFontAttributeName:[UIFont fontWithName:@"Roboto-Regular" size:15],
                           NSForegroundColorAttributeName:UIColorFromRGB(0x9fa0a3),
                           //NSUnderlineStyleAttributeName:@1,
                           //                           NSStrokeWidthAttributeName:@-2.0
                           };
    
    NSDictionary *dic2 = @{
                           NSFontAttributeName:[UIFont fontWithName:@"Roboto-Regular" size:11],
                           NSForegroundColorAttributeName:UIColorFromRGB(0xc2c2c2),
                           //NSUnderlineStyleAttributeName:@1,
                           //                           NSStrokeWidthAttributeName:@-1.0
                           };
    
    //    [strSecond drawInRect:rect withAttributes:dic1];
    
    NSAttributedString *strAttr = [[NSAttributedString alloc] initWithString:strSecond attributes:dic1];
    
    NSMutableAttributedString *strMa = [[NSMutableAttributedString alloc] initWithAttributedString:strAttr];
    [strMa addAttributes:dic2 range:NSMakeRange(8, 7)];
    
    [strMa addAttribute:NSParagraphStyleAttributeName value:paraGraphStyle range:NSMakeRange(0, strSecond.length)];
    

    
}

-(void)changeFontSize:(CGFloat)fontSize
{
    _defalutfontSize = fontSize; 
}

@end
