//
//  ChildHeight.m
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 13..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "ChildHeight.h"
#import "MyChild.h"
#import "UIView+ImageUtil.h"


@implementation ChildHeight

@dynamic createdDate;
@dynamic height;
@dynamic photo;
@dynamic qrcode;
@dynamic takenDate;
@dynamic thumbphoto;
@dynamic parent;
@dynamic stickerType;

//@synthesize faceCropImage=_faceCropImage;


//-(UIImage*)faceCropImage
//{
//    if (_faceCropImage) return _faceCropImage;
//    
//    _faceCropImage = [self.photo facecropImage];
//    
//    return _faceCropImage;
//}

@end
