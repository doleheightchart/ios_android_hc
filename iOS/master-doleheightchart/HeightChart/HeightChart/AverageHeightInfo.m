//
//  AverageHeightInfo.m
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 4. 6..
//  Copyright (c) 2014년 Kim Sehyun. All rights reserved.
//

#import "AverageHeightInfo.h"
#import "NSDate+DoleHeightChart.h"


@interface AveHeightInfoObject : NSObject
@property (nonatomic) NSInteger age;
@property (nonatomic) CGFloat height;
-(id)initWithAge:(NSInteger)age Height:(CGFloat)height;
@end

@implementation AveHeightInfoObject
-(id)initWithAge:(NSInteger)age Height:(CGFloat)height
{
    self = [super init];
    if (self){
        self.age = age;
        self.height = height;
    }
    return self;
}
@end

@implementation AverageHeightInfo

static NSArray *kKoreanGirlStatic;
static NSArray *kKoreanBoyStatic;

static NSArray *kNewZealandGirlStatic;
static NSArray *kNewZealandBoyStatic;

static NSArray *kPhilippineGirlStatic;
static NSArray *kPhilippineBoyStatic;

static NSArray *kSingaporeGirlStatic;
static NSArray *kSingaporeBoyStatic;

static NSArray *kJapanessGirlStatic;
static NSArray *kJapanessBoyStatic;




+(NSArray*)koreanGirlStatic
{
    if (kKoreanGirlStatic != nil) return kKoreanGirlStatic;

    kKoreanGirlStatic = @[
                          [[AveHeightInfoObject alloc]initWithAge:1 Height:75],
                          [[AveHeightInfoObject alloc]initWithAge:2 Height:85],
                          [[AveHeightInfoObject alloc]initWithAge:3 Height:90],
                          [[AveHeightInfoObject alloc]initWithAge:4 Height:100],
                          [[AveHeightInfoObject alloc]initWithAge:5 Height:110],
                          [[AveHeightInfoObject alloc]initWithAge:6 Height:115],
                          [[AveHeightInfoObject alloc]initWithAge:7 Height:121],
                          [[AveHeightInfoObject alloc]initWithAge:8 Height:127],
                          [[AveHeightInfoObject alloc]initWithAge:9 Height:133],
                          [[AveHeightInfoObject alloc]initWithAge:10 Height:139],
                          [[AveHeightInfoObject alloc]initWithAge:11 Height:144],
                          [[AveHeightInfoObject alloc]initWithAge:12 Height:151],
                          [[AveHeightInfoObject alloc]initWithAge:13 Height:156],
                          [[AveHeightInfoObject alloc]initWithAge:14 Height:158],
                          [[AveHeightInfoObject alloc]initWithAge:15 Height:160],
                          [[AveHeightInfoObject alloc]initWithAge:16 Height:160],
                          [[AveHeightInfoObject alloc]initWithAge:17 Height:161],
                          [[AveHeightInfoObject alloc]initWithAge:18 Height:161],
                          [[AveHeightInfoObject alloc]initWithAge:24 Height:162],
                          [[AveHeightInfoObject alloc]initWithAge:29 Height:161],
                          [[AveHeightInfoObject alloc]initWithAge:34 Height:159],
                          [[AveHeightInfoObject alloc]initWithAge:39 Height:159],
                          [[AveHeightInfoObject alloc]initWithAge:44 Height:158],
                          [[AveHeightInfoObject alloc]initWithAge:49 Height:158],
                          [[AveHeightInfoObject alloc]initWithAge:50 Height:155]
                          ];
    
    return kKoreanGirlStatic;
}
+(NSArray*)koreanBoyStatic
{
    if (kKoreanBoyStatic != nil) return kKoreanBoyStatic;
    
    kKoreanBoyStatic = @[
                          [[AveHeightInfoObject alloc]initWithAge:1 Height:75],
                          [[AveHeightInfoObject alloc]initWithAge:2 Height:85],
                          [[AveHeightInfoObject alloc]initWithAge:3 Height:90],
                          [[AveHeightInfoObject alloc]initWithAge:4 Height:100],
                          [[AveHeightInfoObject alloc]initWithAge:5 Height:110],
                          [[AveHeightInfoObject alloc]initWithAge:6 Height:115],
                          [[AveHeightInfoObject alloc]initWithAge:7 Height:122],
                          [[AveHeightInfoObject alloc]initWithAge:8 Height:129],
                          [[AveHeightInfoObject alloc]initWithAge:9 Height:134],
                          [[AveHeightInfoObject alloc]initWithAge:10 Height:139],
                          [[AveHeightInfoObject alloc]initWithAge:11 Height:144],
                          [[AveHeightInfoObject alloc]initWithAge:12 Height:150],
                          [[AveHeightInfoObject alloc]initWithAge:13 Height:158],
                          [[AveHeightInfoObject alloc]initWithAge:14 Height:164],
                          [[AveHeightInfoObject alloc]initWithAge:15 Height:169],
                          [[AveHeightInfoObject alloc]initWithAge:16 Height:172],
                          [[AveHeightInfoObject alloc]initWithAge:17 Height:173],
                          [[AveHeightInfoObject alloc]initWithAge:18 Height:174],
                          [[AveHeightInfoObject alloc]initWithAge:24 Height:175],
                          [[AveHeightInfoObject alloc]initWithAge:29 Height:175],
                          [[AveHeightInfoObject alloc]initWithAge:34 Height:174],
                          [[AveHeightInfoObject alloc]initWithAge:39 Height:173],
                          [[AveHeightInfoObject alloc]initWithAge:44 Height:172],
                          [[AveHeightInfoObject alloc]initWithAge:49 Height:170],
                          [[AveHeightInfoObject alloc]initWithAge:50 Height:167]
                          ];
    
    return kKoreanBoyStatic;
}

+(NSArray*)NewZealandGirlStatic
{
    if (kNewZealandGirlStatic != nil) return kNewZealandGirlStatic;
    
    kNewZealandGirlStatic = @[
                          [[AveHeightInfoObject alloc]initWithAge:2 Height:85],
                          [[AveHeightInfoObject alloc]initWithAge:3 Height:94.5],
                          [[AveHeightInfoObject alloc]initWithAge:4 Height:100],
                          [[AveHeightInfoObject alloc]initWithAge:5 Height:108],
                          [[AveHeightInfoObject alloc]initWithAge:6 Height:115],
                          [[AveHeightInfoObject alloc]initWithAge:7 Height:122],
                          [[AveHeightInfoObject alloc]initWithAge:8 Height:128],
                          [[AveHeightInfoObject alloc]initWithAge:9 Height:133],
                          [[AveHeightInfoObject alloc]initWithAge:10 Height:138],
                          [[AveHeightInfoObject alloc]initWithAge:11 Height:144.5],
                          [[AveHeightInfoObject alloc]initWithAge:12 Height:151.5],
                          [[AveHeightInfoObject alloc]initWithAge:13 Height:157.5],
                          [[AveHeightInfoObject alloc]initWithAge:14 Height:160.5],
                          [[AveHeightInfoObject alloc]initWithAge:15 Height:162],
                          [[AveHeightInfoObject alloc]initWithAge:16 Height:162.5],
                          [[AveHeightInfoObject alloc]initWithAge:17 Height:163],
                          [[AveHeightInfoObject alloc]initWithAge:18 Height:163]
                          ];
    
    return kNewZealandGirlStatic;
}
+(NSArray*)NewZealandBoyStatic
{
    if (kNewZealandBoyStatic != nil) return kNewZealandBoyStatic;
    
    kNewZealandBoyStatic = @[
                              [[AveHeightInfoObject alloc]initWithAge:2 Height:86.5],
                              [[AveHeightInfoObject alloc]initWithAge:3 Height:95],
                              [[AveHeightInfoObject alloc]initWithAge:4 Height:102.5],
                              [[AveHeightInfoObject alloc]initWithAge:5 Height:109],
                              [[AveHeightInfoObject alloc]initWithAge:6 Height:115.5],
                              [[AveHeightInfoObject alloc]initWithAge:7 Height:122],
                              [[AveHeightInfoObject alloc]initWithAge:8 Height:128],
                              [[AveHeightInfoObject alloc]initWithAge:9 Height:133.5],
                              [[AveHeightInfoObject alloc]initWithAge:10 Height:139],
                              [[AveHeightInfoObject alloc]initWithAge:11 Height:144],
                              [[AveHeightInfoObject alloc]initWithAge:12 Height:149.5],
                              [[AveHeightInfoObject alloc]initWithAge:13 Height:156.5],
                              [[AveHeightInfoObject alloc]initWithAge:14 Height:164],
                              [[AveHeightInfoObject alloc]initWithAge:15 Height:170],
                              [[AveHeightInfoObject alloc]initWithAge:16 Height:173.5],
                              [[AveHeightInfoObject alloc]initWithAge:17 Height:175.5],
                              [[AveHeightInfoObject alloc]initWithAge:18 Height:176]
                              ];
    
    return kNewZealandBoyStatic;
}




+(NSArray*)PhilippineGirlStatic
{
    if (kPhilippineGirlStatic != nil) return kPhilippineGirlStatic;
    
    kPhilippineGirlStatic = @[
                              [[AveHeightInfoObject alloc]initWithAge:1 Height:72.8],
                              [[AveHeightInfoObject alloc]initWithAge:2 Height:81.5],
                              [[AveHeightInfoObject alloc]initWithAge:3 Height:88.2],
                              [[AveHeightInfoObject alloc]initWithAge:4 Height:95],
                              [[AveHeightInfoObject alloc]initWithAge:5 Height:101],
                              [[AveHeightInfoObject alloc]initWithAge:6 Height:107],
                              [[AveHeightInfoObject alloc]initWithAge:7 Height:111],
                              [[AveHeightInfoObject alloc]initWithAge:8 Height:117],
                              [[AveHeightInfoObject alloc]initWithAge:9 Height:122.7],
                              [[AveHeightInfoObject alloc]initWithAge:10 Height:127],
                              [[AveHeightInfoObject alloc]initWithAge:11 Height:133.8],
                              [[AveHeightInfoObject alloc]initWithAge:12 Height:139.6],
                              [[AveHeightInfoObject alloc]initWithAge:13 Height:145.5],
                              [[AveHeightInfoObject alloc]initWithAge:14 Height:148.5],
                              [[AveHeightInfoObject alloc]initWithAge:15 Height:150],
                              [[AveHeightInfoObject alloc]initWithAge:16 Height:150.7],
                              [[AveHeightInfoObject alloc]initWithAge:17 Height:151],
                              [[AveHeightInfoObject alloc]initWithAge:18 Height:152],
                              [[AveHeightInfoObject alloc]initWithAge:24 Height:151.6]
                              ];
    
    return kPhilippineGirlStatic;
}

+(NSArray*)PhilippineBoyStatic
{
    if (kPhilippineBoyStatic != nil) return kPhilippineBoyStatic;
    
    kPhilippineBoyStatic = @[
                             [[AveHeightInfoObject alloc]initWithAge:1 Height:73.9],
                             [[AveHeightInfoObject alloc]initWithAge:2 Height:81.5],
                             [[AveHeightInfoObject alloc]initWithAge:3 Height:89.8],
                             [[AveHeightInfoObject alloc]initWithAge:4 Height:95.6],
                             [[AveHeightInfoObject alloc]initWithAge:5 Height:101.5],
                             [[AveHeightInfoObject alloc]initWithAge:6 Height:107.5],
                             [[AveHeightInfoObject alloc]initWithAge:7 Height:112.7],
                             [[AveHeightInfoObject alloc]initWithAge:8 Height:117.5],
                             [[AveHeightInfoObject alloc]initWithAge:9 Height:122.7],
                             [[AveHeightInfoObject alloc]initWithAge:10 Height:126.9],
                             [[AveHeightInfoObject alloc]initWithAge:11 Height:131.9],
                             [[AveHeightInfoObject alloc]initWithAge:12 Height:135.8],
                             [[AveHeightInfoObject alloc]initWithAge:13 Height:144.1],
                             [[AveHeightInfoObject alloc]initWithAge:14 Height:152],
                             [[AveHeightInfoObject alloc]initWithAge:15 Height:156],
                             [[AveHeightInfoObject alloc]initWithAge:16 Height:160.6],
                             [[AveHeightInfoObject alloc]initWithAge:17 Height:161],
                             [[AveHeightInfoObject alloc]initWithAge:18 Height:162.5],
                             [[AveHeightInfoObject alloc]initWithAge:24 Height:163.4]
                             ];
    
    return kPhilippineBoyStatic;
}




+(NSArray*)SingaporeGirlStatic
{
    if (kSingaporeGirlStatic != nil) return kSingaporeGirlStatic;
    
    kSingaporeGirlStatic = @[
                             [[AveHeightInfoObject alloc]initWithAge:1 Height:75],
                             [[AveHeightInfoObject alloc]initWithAge:2 Height:87],
                             [[AveHeightInfoObject alloc]initWithAge:3 Height:90],
                             [[AveHeightInfoObject alloc]initWithAge:4 Height:103],
                             [[AveHeightInfoObject alloc]initWithAge:5 Height:109.5],
                             [[AveHeightInfoObject alloc]initWithAge:6 Height:115],
                             [[AveHeightInfoObject alloc]initWithAge:7 Height:119],
                             [[AveHeightInfoObject alloc]initWithAge:8 Height:127],
                             [[AveHeightInfoObject alloc]initWithAge:9 Height:133],
                             [[AveHeightInfoObject alloc]initWithAge:10 Height:138.5],
                             [[AveHeightInfoObject alloc]initWithAge:11 Height:145],
                             [[AveHeightInfoObject alloc]initWithAge:12 Height:151],
                             [[AveHeightInfoObject alloc]initWithAge:13 Height:157],
                             [[AveHeightInfoObject alloc]initWithAge:14 Height:160],
                             [[AveHeightInfoObject alloc]initWithAge:15 Height:162],
                             [[AveHeightInfoObject alloc]initWithAge:16 Height:163],
                             [[AveHeightInfoObject alloc]initWithAge:17 Height:163.5],
                             [[AveHeightInfoObject alloc]initWithAge:18 Height:163.5],
                             [[AveHeightInfoObject alloc]initWithAge:24 Height:163.5]
                              ];
    
    return kSingaporeGirlStatic;
}

+(NSArray*)SingaporeBoyStatic
{
    if (kSingaporeBoyStatic != nil) return kSingaporeBoyStatic;
    
    kSingaporeBoyStatic = @[
                            [[AveHeightInfoObject alloc]initWithAge:1 Height:76],
                            [[AveHeightInfoObject alloc]initWithAge:2 Height:88],
                            [[AveHeightInfoObject alloc]initWithAge:3 Height:96],
                            [[AveHeightInfoObject alloc]initWithAge:4 Height:101],
                            [[AveHeightInfoObject alloc]initWithAge:5 Height:110],
                            [[AveHeightInfoObject alloc]initWithAge:6 Height:116],
                            [[AveHeightInfoObject alloc]initWithAge:7 Height:122],
                            [[AveHeightInfoObject alloc]initWithAge:8 Height:128],
                            [[AveHeightInfoObject alloc]initWithAge:9 Height:133],
                            [[AveHeightInfoObject alloc]initWithAge:10 Height:138.5],
                            [[AveHeightInfoObject alloc]initWithAge:11 Height:143.5],
                            [[AveHeightInfoObject alloc]initWithAge:12 Height:149],
                            [[AveHeightInfoObject alloc]initWithAge:13 Height:156],
                            [[AveHeightInfoObject alloc]initWithAge:14 Height:163],
                            [[AveHeightInfoObject alloc]initWithAge:15 Height:169],
                            [[AveHeightInfoObject alloc]initWithAge:16 Height:173],
                            [[AveHeightInfoObject alloc]initWithAge:17 Height:175],
                            [[AveHeightInfoObject alloc]initWithAge:18 Height:176],
                            [[AveHeightInfoObject alloc]initWithAge:24 Height:176.5]
                             ];
    
    return kSingaporeBoyStatic;
}


+(NSArray*)JapanessGirlStatic
{
    if (kJapanessGirlStatic != nil) return kJapanessGirlStatic;
    
    kJapanessGirlStatic = @[
                             [[AveHeightInfoObject alloc]initWithAge:1 Height:73.4],
                             [[AveHeightInfoObject alloc]initWithAge:2 Height:84.3],
                             [[AveHeightInfoObject alloc]initWithAge:3 Height:92.2],
                             [[AveHeightInfoObject alloc]initWithAge:4 Height:99.5],
                             [[AveHeightInfoObject alloc]initWithAge:5 Height:106.2],
                             [[AveHeightInfoObject alloc]initWithAge:6 Height:112.7],
                             [[AveHeightInfoObject alloc]initWithAge:7 Height:121.6],
                             [[AveHeightInfoObject alloc]initWithAge:8 Height:127.4],
                             [[AveHeightInfoObject alloc]initWithAge:9 Height:133.4],
                             [[AveHeightInfoObject alloc]initWithAge:10 Height:140.1],
                             [[AveHeightInfoObject alloc]initWithAge:11 Height:146.7],
                             [[AveHeightInfoObject alloc]initWithAge:12 Height:151.9],
                             [[AveHeightInfoObject alloc]initWithAge:13 Height:155],
                             [[AveHeightInfoObject alloc]initWithAge:14 Height:156.5],
                             [[AveHeightInfoObject alloc]initWithAge:15 Height:157.2],
                             [[AveHeightInfoObject alloc]initWithAge:16 Height:157.6],
                             [[AveHeightInfoObject alloc]initWithAge:17 Height:158],
                             [[AveHeightInfoObject alloc]initWithAge:18 Height:157.76],
                             [[AveHeightInfoObject alloc]initWithAge:24 Height:158.63],
                             [[AveHeightInfoObject alloc]initWithAge:34 Height:158.92],
                             [[AveHeightInfoObject alloc]initWithAge:44 Height:158.56],
                             [[AveHeightInfoObject alloc]initWithAge:50 Height:157.18]
                             ];
    
    return kJapanessGirlStatic;
}

+(NSArray*)JapanessBoyStatic
{
    if (kJapanessBoyStatic != nil) return kJapanessBoyStatic;
    
    kJapanessBoyStatic = @[
                           [[AveHeightInfoObject alloc]initWithAge:1 Height:76],
                           [[AveHeightInfoObject alloc]initWithAge:2 Height:85.4],
                           [[AveHeightInfoObject alloc]initWithAge:3 Height:93],
                           [[AveHeightInfoObject alloc]initWithAge:4 Height:100],
                           [[AveHeightInfoObject alloc]initWithAge:5 Height:106],
                           [[AveHeightInfoObject alloc]initWithAge:6 Height:113.3],
                           [[AveHeightInfoObject alloc]initWithAge:7 Height:119.6],
                           [[AveHeightInfoObject alloc]initWithAge:8 Height:128.2],
                           [[AveHeightInfoObject alloc]initWithAge:9 Height:133.6],
                           [[AveHeightInfoObject alloc]initWithAge:10 Height:138.9],
                           [[AveHeightInfoObject alloc]initWithAge:11 Height:145],
                           [[AveHeightInfoObject alloc]initWithAge:12 Height:152.4],
                           [[AveHeightInfoObject alloc]initWithAge:13 Height:159.5],
                           [[AveHeightInfoObject alloc]initWithAge:14 Height:165.1],
                           [[AveHeightInfoObject alloc]initWithAge:15 Height:168.4],
                           [[AveHeightInfoObject alloc]initWithAge:16 Height:169.8],
                           [[AveHeightInfoObject alloc]initWithAge:17 Height:170.7],
                           [[AveHeightInfoObject alloc]initWithAge:18 Height:171.43],
                           [[AveHeightInfoObject alloc]initWithAge:24 Height:171.62],
                           [[AveHeightInfoObject alloc]initWithAge:34 Height:171.73],
                           [[AveHeightInfoObject alloc]initWithAge:44 Height:171.56],
                           [[AveHeightInfoObject alloc]initWithAge:50 Height:167]
                            ];
    
    return kJapanessBoyStatic;
}

+(NSArray*)heightStaticByIsGirl:(BOOL)isGirl
{
    NSString *countryCode = [[NSLocale currentLocale] objectForKey:NSLocaleCountryCode];
    
    if ([countryCode isEqualToString:@"KR"]){
        return isGirl ? [AverageHeightInfo koreanGirlStatic] : [AverageHeightInfo koreanBoyStatic];
    }
    else if ([countryCode isEqualToString:@"NZ"]){
        return isGirl ? [AverageHeightInfo NewZealandGirlStatic] : [AverageHeightInfo NewZealandBoyStatic];
    }
    else if ([countryCode isEqualToString:@"PH"]){
        return isGirl ? [AverageHeightInfo PhilippineGirlStatic] : [AverageHeightInfo PhilippineBoyStatic];
    }
    else if ([countryCode isEqualToString:@"JP"]){
        return isGirl ? [AverageHeightInfo JapanessGirlStatic] : [AverageHeightInfo JapanessBoyStatic];
    }
    else {
        return isGirl ? [AverageHeightInfo SingaporeGirlStatic] : [AverageHeightInfo SingaporeBoyStatic];
    }
}

//+(CGFloat)averageHeightByAge:(NSInteger)age IsGril:(BOOL)isGirl
//{
//    NSArray *statics = [AverageHeightInfo heightStaticByIsGirl:isGirl];
//
//    AveHeightInfoObject *prevObjet = nil;
//    
//    for (AveHeightInfoObject *heightInfo in statics) {
//        if (heightInfo.age > age) break;
//        prevObjet = heightInfo;
//    }
//    
//    return prevObjet.height;
//}


+(CGFloat)averageHeightByBirthday:(NSDate*)birthday IsGril:(BOOL)isGirl
{
    NSInteger nowYear = [[NSDate date] getYear];
    NSInteger birthYear = [birthday getYear];
    
    /*
    NSInteger age = nowYear - birthYear;
    
    //assert(age > 0);
    
    age = MAX(0, age);
    */
    
    NSInteger age = [birthday getGlobalAge];
     
    return [AverageHeightInfo averageHeightByAge:age IsGril:isGirl];
}

+(CGFloat)averageHeightByAge:(NSInteger)age IsGril:(BOOL)isGirl
{
    NSArray *statics = [AverageHeightInfo heightStaticByIsGirl:isGirl];
    
    AveHeightInfoObject *prevObjet = nil;
    
    for (AveHeightInfoObject *heightInfo in statics) {
        if (heightInfo.age > age) break;
        prevObjet = heightInfo;
    }
    
    return prevObjet.height;
}


@end
