//
//  UIView+Animation.h
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 2. 11..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DoleInfo.h"

@interface UIView (Animation)
-(void)animateScaleEffect;
-(void)animateScaleEffectWithDuration:(CGFloat)duration;
-(void)animateScaleEffectWithDuration:(CGFloat)duration MinScale:(CGFloat)minScale MaxScale:(CGFloat)maxScale completionBlock:(void(^)())block;


-(CAKeyframeAnimation*)addMoveAnimationWithPosition:(NSArray*)positions
                           duration:(NSTimeInterval)duration
                         completion:(void (^)(BOOL finished))completion;

-(CAKeyframeAnimation*)addMoveAnimationWithPosition:(NSArray*)positions
                                              Delay:(NSTimeInterval)delay
                                             Repeat:(NSInteger)repeat
                                           FillMode:(NSString*)fillMode
                                     RepeatDuration:(NSTimeInterval)repeatduration
                                           duration:(NSTimeInterval)duration
                                         completion:(void (^)(BOOL finished))completion;

@end




@interface UIImageView (FrameAnimationUtil)

-(void)addFrameImagesWithNameFormat:(NSString*)nameFormat frameCount:(NSUInteger)frameCount
                           duration:(NSTimeInterval)duration repeat:(NSUInteger)repeat;

-(void)addFrameImagesWithNameFormat:(NSString*)nameFormat
                         frameCount:(NSUInteger)frameCount
                           duration:(NSTimeInterval)duration
                             repeat:(NSUInteger)repeat
                     animationStart:(BOOL)start;

-(void)addFrameImagesWithNameList:(NSArray*)framelist
                         frameCount:(NSUInteger)frameCount
                           duration:(NSTimeInterval)duration
                             repeat:(NSUInteger)repeat
                     animationStart:(BOOL)start;

//-(void)addMoveAnimationWithPosition:(NSArray*)positions
//                           duration:(NSTimeInterval)duration
//                         completion:(void (^)(BOOL finished))completion;

-(void)addHeightViewerAnimalAnimationWithType:(enum DoleStickerType)stickerType KidHeight:(CGFloat)kidHeight;

@end
