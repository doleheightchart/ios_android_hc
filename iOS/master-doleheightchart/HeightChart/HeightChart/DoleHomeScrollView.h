//
//  DoleHomeScrollView.h
//  HeightChart
//
//  Created by ne on 2014. 3. 21..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DoleProtocolList.h"
#import "DoleOwlTree.h"
#import "MyChild.h"

@interface DoleHomeScrollView : UIView <UIScrollViewDelegate, clickMonkeyDelegate , clickOwlDelegate>

@property (nonatomic, weak) id<MonkeyTreeDataSource> datasource;
@property (nonatomic, weak) id<MonkeyHomeListDelegate> homeListDelegate; 
@property (nonatomic) BOOL isDeleteMode;


//Add Delete Reload
-(void)addNewMonkeyWithMyChildInfo:(MyChild*)child;
-(void)deleteMonkeysWithIndexArray:(NSArray*)indexArray;
-(void)reloadMonkeyTree;
-(void)updateMonekyTreehegithWithIndex:(NSInteger)monkeyIndex;
-(void)checkedMonkeyTreeWithIndex:(NSInteger)monkeyIndex;

-(NSArray*)checkedMonkeyIndexList;

// animation
-(void)playAllTreeAnimation;

/////////////clickOwlDelegate
-(void)clickAddNickName;

///////////// ClcikmonkeyDeleage
-(void)clickMonkeyWithData:(DoleTree*) monkeyTree;
-(void)clickLongPress:(DoleTree*)monkeyTree;
-(void)didCheckedMonkeyWithData:(DoleTree *)monkeyTree Checked:(BOOL)checked;


@end
