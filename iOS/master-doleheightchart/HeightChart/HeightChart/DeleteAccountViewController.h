//
//  DeleteAccountViewController.h
//  HeightChart
//
//  Created by ne on 2014. 4. 11..
//  Copyright (c) 2014년 Kim Sehyun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DeleteAccountViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@end

@interface DoleFamilyAccountData : NSObject
@property (nonatomic, retain) NSString *imageName;
@property (nonatomic, retain) NSString *AccountName;
@property (nonatomic, retain) NSString *AccountDesc;


@end
