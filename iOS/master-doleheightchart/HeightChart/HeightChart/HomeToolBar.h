//
//  HomeToolBar.h
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 21..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    kDoleCoinButton = 100,
    kAddHeightButton = 101,
    kDeleteModeButton = 102,
    kSettingButton = 103,
    kDeleteNickNameButton = 104,
    kDeleteBackButton = 105,
}HomeToolBarButton;

@protocol HomeToolBarDelegate<NSObject>

- (void)clickButtonWithType:(HomeToolBarButton)toolBarButtonType;

@end

@interface HomeToolBar : UIControl

@property (nonatomic) BOOL isDeleteMode;
@property (nonatomic) BOOL enableDeleteBtn;
@property (nonatomic) BOOL availableSelected;
@property (nonatomic) NSUInteger doleCoinCount;
@property (nonatomic, weak) id<HomeToolBarDelegate> toolBarDelegate;

- (void)playSplashAnimation;



@end
