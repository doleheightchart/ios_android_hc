//
//  QRScanViewController.m
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 2. 25..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import "QRScanViewController.h"
#import "ZBarSDK.h"
#import "DoleInfo.h"
#import "UIView+ImageUtil.h"
#import "UIViewController+DoleHeightChart.h"
#import "TextViewDualPlaceHolder.h"
#import "UIFont+DoleHeightChart.h"
#import "UserConfig.h"
#import "QRTutorialPopupController.h"
#import "ToastController.h"
#import "WarningCoverView.h"
#import "UIViewController+PolyServer.h"
#import "ConfirmPopupController.h"
#import "MessageBoxController.h"

#import "Mixpanel.h"

NSString const *kDoleStickerValueKey = @"DoleStickerValueKey";
NSString const *kCouponCodeValueKey = @"CouponCodeValueKey";

@interface QRScanViewController (){
    ZBarImageScanner    *_scanner;
    ZBarReaderView      *_qrview;
    //Server
    NSMutableData   *_recevedBufferData;
    BOOL                _messageBoxPresented;
    
    BOOL                bIsScanned;
}

@property (weak, nonatomic) IBOutlet TextViewDualPlaceHolder *inputQRCodeView;
@property (nonatomic, strong) ZBarReaderViewController *qrController;
@property (nonatomic, weak) IBOutlet UIButton *helpButton;
@property (nonatomic, weak) IBOutlet UILabel *helpLabel;
@property (nonatomic, weak) IBOutlet UILabel *infoLabel;
@property (nonatomic, weak) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UIImageView *qrFrameImageView;

@property (weak, nonatomic) IBOutlet UIImageView *sampleQRImageView;
@property (weak, nonatomic) IBOutlet UIImageView *bottomTutoImageView;

@end

@implementation QRScanViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self createScanner];
    [self createQRView];
    
    [self addCommonCloseButton];
    [self decorateUI];
    
    [self setupPositionAndSize];
    
    self.inputQRCodeView.textField.returnKeyType = UIReturnKeyDone;
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"qr_code_gradient_bg"]];
    
    //longzhe cui added
    [[Mixpanel sharedInstance] track:@"Open QRCode"];
    [[Mixpanel sharedInstance] timeEvent:@"Scanned QRCode"];
    [[Mixpanel sharedInstance] timeEvent:@"Recorded Height"];
}

-(void)decorateUI
{
    self.backgroundView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"qr_code_gradient_bg"]];
    
    self.helpButton.adjustsImageWhenHighlighted = NO;
    self.helpButton.backgroundColor = [UIColor clearColor];
    [self.helpButton setBackgroundImage:[UIImage imageNamed:@"qr_code_btn_help_normal"] forState:UIControlStateNormal];
    [self.helpButton setBackgroundImage:[UIImage imageNamed:@"qr_code_btn_help_press"] forState:UIControlStateHighlighted];
    
    self.helpLabel.font = [UIFont defaultRegularFontWithSize:37/2];
    
    //다국어 버전...대응
    //    self.infoLabel.text = @"등록 후 24시간 유효합니다.";
    
    self.infoLabel.font = [UIFont defaultKoreanFontWithSize:26/2];
    
    [self.inputQRCodeView setDualPlaceHolderTextView:DoleDualPlaceHolderTextTypeNormal];
    
    [self.inputQRCodeView.textField setReDrawPlaceHolder:NSLocalizedString(@"Enter the alphanumeric code",@"") SecondText:@""];// = NSLocalizedString(@"Enter the alphanumeric code",@"");
    //    self.inputQRCodeView.secondText = NSLocalizedString(@"N characters", @"");
    
    [self.inputQRCodeView.textField addTarget:self action:@selector(enteredQRCodeDirect) forControlEvents:UIControlEventEditingDidEndOnExit];
    
    
    self.inputQRCodeView.textField.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
}

-(void)enteredQRCodeDirect
{
    //    if ([self.inputQRCodeView checkIsValidDirectQRCodeWithMinimumLength:10] == FALSE) return;
    if ([self.inputQRCodeView checkIsValidDirectQRCodeWithFixLength:16] == FALSE) return;
    
    NSString *qrcodeText = self.inputQRCodeView.textField.text;
    
    self.heightSaveContext.qrCode = qrcodeText;
    
    //for TEST
    //[self requestCouponQueryWithQRCode:qrcodeText];
    
    [self validationWithCouponCode:qrcodeText];
    
    bIsScanned = false;
}

-(void)setupPositionAndSize
{
    if ([UIScreen isFourInchiScreen] == YES){
    }
    else{
        [self moveToPositionYPixel:90 Control:self.sampleQRImageView];
        [self moveToPositionYPixel:90 + 115 Control:self.qrFrameImageView];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)closeViewControllerAnimated:(BOOL)animated
{
    [self.navigationController popToRootViewControllerAnimated:animated];
}

- (void)createScanner
{
    _scanner = [ZBarImageScanner new];
    [_scanner setSymbology:0 config:ZBAR_CFG_X_DENSITY to:3];
    [_scanner setSymbology:0 config:ZBAR_CFG_Y_DENSITY to:3];
    
    [_scanner setSymbology:ZBAR_I25 config:ZBAR_CFG_ENABLE to:0];
    
}

- (void)createQRView
{
    _qrview = [[ZBarReaderView alloc] initWithImageScanner:_scanner];
    _qrview.readerDelegate = self;
    _qrview.scanCrop = CGRectMake(0, 0, 1, 1);
    _qrview.previewTransform = CGAffineTransformIdentity;
    _qrview.tracksSymbols = YES;
    _qrview.enableCache = YES;
    _qrview.torchMode = 0; // 플래쉬를 사용하지 않는다.
    _qrview.allowsPinchZoom = NO;
    _qrview.frame = self.view.bounds;
    
    
    
    //    [self.view addSubview:_qrview];
    [self.view insertSubview:_qrview atIndex:0];
}

-(void)animateTutorial
{
    self.sampleQRImageView.hidden = NO;
    
    CAShapeLayer *boxLayer = [CAShapeLayer layer];
    boxLayer.lineWidth = 2.0;
    boxLayer.strokeColor = [UIColor colorWithRGB:0x3fc4d0].CGColor;
    boxLayer.fillColor = [UIColor clearColor].CGColor;
    boxLayer.frame = CGRectMake(0, 0, 273/2, 273/2);
    boxLayer.cornerRadius = 2;
    boxLayer.masksToBounds = TRUE;
    
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathAddRect(path, NULL, boxLayer.frame);
    CGPathCloseSubpath(path);
    
    boxLayer.path = path;
    
    CGPathRelease(path);
    
    [self.view.layer addSublayer:boxLayer];
    
    [CATransaction begin];
    
    [CATransaction setValue:[NSNumber numberWithFloat:1] forKey:kCATransactionAnimationDuration];
    [CATransaction setCompletionBlock:^{
        
        self.heightSaveContext.qrCode = @"FreeQRCode";
        self.heightSaveContext.stickerType = DoleStikerLion;
        [self.heightSaveDelegate didScanQRCode:self.heightSaveContext];
        
    }];
    
    CABasicAnimation *moveAni = [CABasicAnimation animationWithKeyPath:@"position"];
    moveAni.duration = 1;
    moveAni.fillMode = kCAFillModeForwards;
    moveAni.removedOnCompletion = NO;
    moveAni.toValue = [NSValue valueWithCGPoint:self.view.center];
    [boxLayer addAnimation:moveAni forKey:nil];
    [CATransaction commit];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //    if (_qrview.device.isFlashAvailable){
    //        if ([_qrview.device isFlashModeSupported:AVCaptureFlashModeOff]){
    //            [_qrview.device lockForConfiguration:nil];
    //            _qrview.device.flashMode = AVCaptureFlashModeOff;
    //            [_qrview.device unlockForConfiguration];
    //        }
    //    }
    [_qrview start];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    //     QRTutorial
    if (NO == self.userConfig.usedFirstFreeSticker){
        _messageBoxPresented = TRUE;
        self.sampleQRImageView.hidden = NO;
        
        UIStoryboard *popupStory = [UIStoryboard storyboardWithName:@"Popup" bundle:nil];
        QRTutorialPopupController *pc = [popupStory instantiateViewControllerWithIdentifier:@"QRTutorial"];
        __weak QRScanViewController *thisController = self;
        pc.completion = ^(PopupController *controller){
            
            self.heightSaveContext.freeQRCode = YES;
            [controller releaseModal];
            //[UserConfig sharedConfig].usedFirstFreeSticker = YES;
            
            
            [thisController animateTutorial];
            
        };
        [pc showModalOnTheViewController:nil];
    }
    else{
        if (NO == self.isEnableNetwork){
            
            //            [self showOKConfirmMessage:@"네트워크가 연결되지 않아\n코드를인식 할 수 없습니다." completion:^{
            //            "The code cannot be recognized\nbecause there is no network\nconnection."
            [self showOKConfirmMessage:NSLocalizedString(@"The code cannot be recognized\nbecause there is no network\nconnection.", @"") completion:^{
                [self.navigationController closeViewControllerAnimated:YES];
            }];
            
        }
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [_qrview stop];
    [super viewWillDisappear:animated];
}

- (void) readerView: (ZBarReaderView*) readerView
     didReadSymbols: (ZBarSymbolSet*) symbols
          fromImage: (UIImage*) image
{
    if (NO == self.view.userInteractionEnabled) return;
    if (_messageBoxPresented) return;
    
    ZBarSymbol *symbol = nil;
    for(symbol in symbols)
        // EXAMPLE: just grab the first barcode
        break;
    
    
    self.qrFrameImageView.image = [UIImage imageNamed:@"qr_code_frame_select"];
    
    
    // EXAMPLE: do something useful with the barcode data
    NSString *qrcodeText = symbol.data;
    
    // EXAMPLE: do something useful with the barcode image
    
    
    // ADD: dismiss the controller (NB dismiss from the *reader*!)
    
    //    [self dismissViewControllerAnimated:YES completion:^{
    //        self.completion(YES, qrcodeText);
    //    }];
    
    //    [self closeViewControllerAnimated:NO];
    
    NSDictionary *scanResult = [self validationScanQRCode:qrcodeText];
    
    if (scanResult){
        NSString *couponCode = scanResult[kCouponCodeValueKey];
        self.heightSaveContext.qrCode = couponCode;
        
        //        if ([self.heightSaveDelegate alreadyTakenQRCode:couponCode]){
        //            if ([self.heightSaveDelegate canTakenQRCode:couponCode Context:self.heightSaveContext]){
        //
        //                //[self requestCouponQueryWithQRCode:couponCode];
        //                ConfirmPopupController *confirm = [[UIStoryboard storyboardWithPopup] instantiateViewControllerWithIdentifier:@"confirmPopup"];
        //                [confirm popupTextUp:@"사용한 QRCode" TextBottom:@"사용시간이 24이내 재사용가능함"];
        //                confirm.isOneButton = YES;
        //                confirm.completionBlock = ^(BOOL yesno){
        //                    if (yesno){
        //                        self.heightSaveContext.reuseQRCode = YES;
        //                        [self.heightSaveDelegate didScanQRCode:self.heightSaveContext];
        //                    }
        //                    else
        //                        [self.navigationController closeViewControllerAnimated:YES];
        //                };
        //                [confirm showModalOnTheViewController:nil];
        //            }
        //            else{
        //                //사용할수없는 QRCode 24시간이 지난 코드임
        //                ConfirmPopupController *confirm = [[UIStoryboard storyboardWithPopup] instantiateViewControllerWithIdentifier:@"confirmPopup"];
        //                [confirm popupTextUp:@"사용한 QRCode" TextBottom:@"사용시간이 24시간이 지나 사용못합니다"];
        //                confirm.isOneButton = YES;
        //                confirm.completionBlock = ^(BOOL yesno){
        //                    [self.navigationController closeViewControllerAnimated:YES];
        //                };
        //                [confirm showModalOnTheViewController:nil];
        //            }
        //        }
        //        else{
        //            [self requestCouponQueryWithQRCode:couponCode];
        //        }
        [self validationWithCouponCode:couponCode];
        bIsScanned = true;
    }
    else{
        //팝업을 띄우고 다시 찍게 할것..
        
        [self warningCantuseCode];
        
        self.qrFrameImageView.image = [UIImage imageNamed:@"qr_code_frame"];
    }
    
    
    //    self.heightSaveContext.qrCode = qrcodeText;
    //
    //    NSMutableString *qrcode = [NSMutableString stringWithString:qrcodeText];
    //    [qrcode replaceOccurrencesOfString:@"URL:" withString:@"" options:(NSCaseInsensitiveSearch) range:NSMakeRange(0, 5)];
    //
    //    NSURL *url = [NSURL URLWithString:qrcode];
    //    NSLog(@"host is %@, query is %@", [url host], [url query]);
    //    NSMutableString *query = [NSMutableString stringWithString:url.query];
    //    NSArray *array = [query componentsSeparatedByString:@"&"];
    //
    //    //for TEST
    //    [self requestCouponQueryWithQRCode:@"PAJKVGKNGY36Z5SH"];
    //
    ////
    
    
}

-(void)validationWithCouponCode:(NSString*)couponCode
{
    if ([self.heightSaveDelegate alreadyTakenQRCode:couponCode]){
        if ([self.heightSaveDelegate canTakenQRCode:couponCode Context:self.heightSaveContext]){
            //            [self showOkCancelConfirmMessage:@"사용된 코드입니다.\n새로 등록시 \n이전 기록은 삭제됩니다." completion:^(BOOL yesno) {
            _messageBoxPresented = TRUE;
            [self showOkCancelConfirmMessage:NSLocalizedString(@"This code has been used.\nWould you like to replace\nyour previous photo?", @"") completion:^(BOOL yesno) {
                _messageBoxPresented = FALSE;
                if (yesno){
                    self.heightSaveContext.reuseQRCode = YES;
                    [self.heightSaveDelegate didScanQRCode:self.heightSaveContext];
                }
                else
                    //[self.navigationController closeViewControllerAnimated:YES];
                    return;
            }];
        }
        else{
            //사용할수없는 QRCode 24시간이 지난 코드임
            
            //            [self showOKConfirmMessage:@"사용할 수 없는 코드입니다." completion:^{
            _messageBoxPresented = TRUE;
            [self showOKConfirmMessage:NSLocalizedString(@"This code cannot be used", @"") completion:^{
                //                [self.navigationController closeViewControllerAnimated:YES];
                _messageBoxPresented = FALSE;
            }];
            
            self.qrFrameImageView.image = [UIImage imageNamed:@"qr_code_frame"];
            
        }
    }
    else{
        [self requestCouponQueryWithQRCode:couponCode];
    }
    
}

-(NSDictionary*)validationScanQRCode:(NSString*)scanQRCode
{
    NSMutableString *qrcode = [NSMutableString stringWithString:scanQRCode];
    
    // Remove "URL:"
    [qrcode replaceOccurrencesOfString:@"URL:" withString:@"" options:(NSCaseInsensitiveSearch) range:NSMakeRange(0, 5)];
    
    
    NSURL *url = [NSURL URLWithString:qrcode];
    
    if (nil == url.query) return nil;
    
    NSArray *values = [url.query componentsSeparatedByString:@"&"];
    
    //ex) T=A01&CP=PAJKVGKNGY36Z5SH
    if (values.count != 2) return nil;
    
    NSString *stickerParam = values[0];
    NSString *couponParam = values[1];
    
    NSArray *stickerKeyValue = [stickerParam componentsSeparatedByString:@"="];
    NSArray *couponKeyValue = [couponParam componentsSeparatedByString:@"="];
    
    if (stickerKeyValue.count != 2 || couponKeyValue.count != 2) return nil;
    
    NSString *stickerKey = stickerKeyValue[0];
    NSString *stickerValue = stickerKeyValue[1];
    
    NSString *couponKey = couponKeyValue[0];
    NSString *couponValue = couponKeyValue[1];
    
    if (NO == [stickerKey isEqualToString:@"T"])return nil;
    if (NO == [couponKey isEqualToString:@"CP"])return nil;
    
    enum DoleStickerType stickerType = [DoleInfo convertStickerTypeWithStringQR:stickerValue];
    
    if (stickerType == DoleStikerUnknown){
        NSLog(@"Unknown Sticker Type Text is %@", stickerValue);
        return nil;
    }
    
    //QRScanResult *result = [[QRScanResult alloc]initWithStickerType:stickerType CouponCode:couponValue];
    
    
    //    NSString const *kDoleStickerValueKey = @"DoleStickerValueKey";
    //    NSString const *kCouponCodeValueKey = @"CouponCodeValueKey";
    
    NSDictionary *result = @{kDoleStickerValueKey: [NSNumber numberWithInt:stickerType],
                             kCouponCodeValueKey: couponValue};
    
    return result;
}

#pragma mark Dole Server


- (void)didConnectionFail:(NSError *)error
{
    [self toastNetworkError];
}

- (void)didCompleteRecevedWithResultType:(DoleServerResultType)resultType ParsedData:(NSDictionary *)recevedData ParseError:(NSError *)error
{
    if (!error){
        
        //        //TEST CODE
        //        [self.heightSaveDelegate didScanQRCode:self.heightSaveContext];
        //        return;
        
        if (resultType == kDoleServerResultTypeCoupon){
            
            NSDictionary *couponResult = recevedData[@"CheckMobileCouponResult"];
            
            if (couponResult == nil || [couponResult[@"Return"] boolValue] == NO) {
                [self warningCantuseCode];
                return;
            }
            
            BOOL canUse = [couponResult[@"CanUse"] boolValue];
            NSArray *productInfos = couponResult[@"ProductInfos"];
            NSDictionary *info = nil;
            if (productInfos && [productInfos count])
            {
               info = productInfos[0];
            }
            
            NSDictionary *promotion = couponResult[@"Promotion"];
            NSString *stickerValue = promotion[@"Name"];
            
            //
            //                사자 = A01
            //                새   = A02
            //                여우 = A03
            //                악어 = A04
            //                너구리 = A05
            
            enum DoleStickerType stickerType = [DoleInfo convertStickerTypeWithString:stickerValue];
            self.heightSaveContext.stickerType = stickerType;
            
            if (canUse && stickerType != DoleStikerUnknown){
                //                if (canUse){
                
                [self.view.findFirstResponder resignFirstResponder];
                
                [self.heightSaveDelegate didScanQRCode:self.heightSaveContext];
                
                //longzhe cui added
                if (bIsScanned)
                {
                    [[Mixpanel sharedInstance] track:@"Scanned QRCode"];
                }
                else
                {
                    [[Mixpanel sharedInstance] track:@"Input Alphanumeric Code"];
                }
            }
            else{
                //                    [ToastController showToastWithMessage:@"사용못하는 코드임" duration:5 completion:^{
                //                        [self closeViewControllerAnimated:YES];
                //                    }];
                
                [self warningCantuseCode];
            }
            
        }
        else if (resultType == kDoleServerResultTypeEventInfo){
            
            
            
        }
    }
    else{
        
    }
    
    
    
}

-(void)warningCantuseCode
{
    [self.inputQRCodeView.textField resignFirstResponder];
    [self showOKConfirmMessage:NSLocalizedString(@"This code cannot be used", @"") completion:^{
        //                [self.navigationController closeViewControllerAnimated:YES];
    }];
}

- (NSMutableData*)receivedBufferData
{
    if (nil == _recevedBufferData){
        _recevedBufferData = [NSMutableData dataWithCapacity:0];
    }
    
    return _recevedBufferData;
}

@end
