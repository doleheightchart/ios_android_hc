//
//  ChooseNicknamePopupController.m
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 2. 11..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "ChooseNicknamePopupController.h"
#import "UIView+ImageUtil.h"
#import "UIViewController+DoleHeightChart.h"
#import "MyChild.h"
#import "UIFont+DoleHeightChart.h"
#import "NSObject+HeightDatabase.h"

#define kNickNameCellId     @"NicknameCell"
#define kAddNickNameCellId  @"NewnicknameCell"

#define kCharacterIconTag   101
#define kNicknameLabelTag   102

@interface ChooseNicknamePopupController (){
    NSFetchedResultsController *_children;
}

@end

@implementation ChooseNicknamePopupController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.cancelButton.titleLabel.font = [UIFont defaultBoldFontWithSize:34/2];

    _children = [self requestFetchedResultController];
    
    self.popupTitleLabel.text = NSLocalizedString(@"Select a nickname", @"");
//    [self.cancelButton setTitle:NSLocalizedString(@"Cancel", @"") forState:UIControlStateNormal];
    
    if (_children.fetchedObjects.count < 3){
        
    }
    
    self.popupContainerView.frame = CGRectMake(0, 0, 100, 100);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark TableView DataSource & Delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _children.fetchedObjects.count + 1; // 1 is Add Nickname Cell
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    int index = indexPath.row;
//    int count = _children.fetchedObjects.count;

    if (index == 0){
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kAddNickNameCellId forIndexPath:indexPath];
        
        return cell;
    }
    
    MyChild *myChild = _children.fetchedObjects[indexPath.row-1];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kNickNameCellId forIndexPath:indexPath];
    
    UIImageView *imageView = (UIImageView*)[cell viewWithTag:kCharacterIconTag];
    UILabel *nicknameLabel = (UILabel*)[cell viewWithTag:kNicknameLabelTag];
    
    NSString *charaterImgName = [NSString stringWithFormat:@"popup_monkey_%02d", myChild.charterNo.shortValue, nil];
    
    UIImage *characterIcon = [UIImage imageNamed:charaterImgName];
//    NSString *nickname = @"John Michel";
    NSString *nickname = myChild.nickname;
    
    imageView.image = characterIcon;
    nicknameLabel.text = nickname;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    int index = indexPath.row;
//    int count = _children.fetchedObjects.count;
    BOOL isNewNickName = NO;
    NSString *nickName;
    
//    if (index >= count){
//        isNewNickName = YES;
//    }
//    else {
//        isNewNickName = NO;
//        MyChild *myChild = _children.fetchedObjects[index];
//        nickName = myChild.nickname;
//    }
    if (index == 0) {
        isNewNickName = YES;
    }
    else{
        isNewNickName = NO;
        MyChild *myChild = _children.fetchedObjects[index-1];
        nickName = myChild.nickname;
    }
    
    
    
//    self.completionBlock(isNewNickName, nickName);
    self.selectedCompletionBlock(isNewNickName ? -1 :index-1);

    NSLog(@"Selected Nickname row index is %d", indexPath.row);
    [self releaseModal];
}

@end
