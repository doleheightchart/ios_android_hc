//
//  MyAnimationTime.h
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 4. 17..
//  Copyright (c) 2014년 Kim Sehyun. All rights reserved.
//

#ifndef HeightChart_MyAnimationTime_h
#define HeightChart_MyAnimationTime_h


#define kDuration_Cloud                                 15
#define kDuration_Animal                                15
#define kDuration_Animal_Delay                          5
#define kDuration_Ballon                                15
#define kDuration_Ballon_Delay                          5
#define kDuration_InputHeight_Objects                   8
#define kDuration_Detail_ZoomChange                     0.3


#define kDuration_Toast_Warning                         5
#define kDuration_Toast_Error                           5
#define kCuration_Toast_Confirm                         3







#endif
