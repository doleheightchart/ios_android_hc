//
//  UIViewController+DoleHeightChart.h
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 10..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "UserConfig.h"
#import "DoleInfo.h"
#import "FacebookManager.h"
//#import <AVFoundation/AVFoundation.h>

#pragma mark DoleHeightChartUI Category

@interface UIViewController (DoleHeightChartUI)
-(UIButton*)addCommonCloseButton;
-(UIButton*)addCameraCloseButton;

-(void)addTitleByBigSizeImage:(BOOL)isBig Visible:(BOOL)visible;
-(void)makeSkyBackground;
-(void)moveByPositionY:(CGFloat)moveY Controls:(NSArray*)controls;
-(UIView*)addTitleMiniToLeftTop;
-(void)moveToPositionY:(CGFloat)moveY Control:(UIView*)control;
-(void)moveToPositionYPixel:(CGFloat)moveY Control:(UIView*)control;
-(void)moveToPositionXPixel:(CGFloat)moveX Control:(UIView*)control;
-(void)changeToHeight:(CGFloat)height Control:(UIView*)control;
-(void)changeToHeightPixel:(CGFloat)height Control:(UIView*)control;
-(void)closeMe:(id)sender;

-(void)addActiveIndicator;
-(void)removeActiveIndicator;

-(BOOL)enableActionWithCurrentNetwork;

-(void)presentLogInViewControllerWithCompletion:(void(^)(BOOL isLoggedIn))completion;
-(void)presentLogInViewControllerOKCancelWithCompletion:(void(^)(BOOL isLoggedIn))completion;

@end

#pragma mark DoleHeightChart Transition

@interface UIViewController (DoleHeightChartTransition)
-(void)closeViewControllerAnimated:(BOOL)animated;
-(void)pushTransitionController:(UIViewController*)dstViewController Animated:(BOOL)animated;
-(void)modalTransitionController:(UIViewController*)dstViewController Animated:(BOOL)animated;
@end

@interface UIViewController (KeyboardControl)

-(void)addKeyboardObserver;
-(UITextField*)atLeatAdjustHeightControl;
-(void)willAppearKeyboard:(NSNotification *)notification;
-(void)willDisAppearKeyboard:(NSNotification *)notification;
-(void)prepareKeyboardAppear;
-(void)prepareKeyboardDisAppear;
@end

@interface NSObject (DoleHeightChartConfig)


@property (nonatomic, readonly) UserConfig* userConfig;

@property (nonatomic, readonly) BOOL isEnableNetwork;

@property (nonatomic, readonly) BOOL isCellularNetwork;

//-(CGFloat)standardHeightByAge:(NSUInteger)age Gender:(NSUInteger)gender;
-(NSDictionary *)englishCountryNamesAndCities;
-(NSDictionary *)countriesCodesAndEnglishCommonNames;
-(NSString*)currentCountryCode;

-(NSString*)currentLanguageCode;
-(NSString*)currentLanguageName;

@end

@interface NSObject (Audio)

- (void)playMusic;
- (void)pauseMusic;
- (void)stopMusic;


@end


@interface UIScreen(DoleHeightChartUI)
+(BOOL)isFourInchiScreen;
@end

@interface UIView(KeyboardControl)
- (id)findFirstResponder;
@end

@interface NSObject (FacebookManager)

@property (nonatomic, readonly) FacebookManager* facebookManager;


@end

@interface UIStoryboard (Localized)

+(instancetype)storyboardWithLogIn;
+(instancetype)storyboardWithPopup;
+(instancetype)storyboardWithMain;

@end

//@interface UINavigationController(DoleHeightChartUI)
//
//@end
