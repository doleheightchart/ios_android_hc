//
//  CityPopupController.m
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 26..
//  Copyright (c) 2014년 Kim Sehyun. All rights reserved.
//

#import "CityPopupController.h"
#import "TwoLinePickerView.h"
#import "UIColor+ColorUtil.h"

@interface CityPopupController (){
    TwoLinePickerView   *_cityView;
}

@end

@implementation CityPopupController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    CGRect frame = CGRectMake(19, 42, 233, 137);
    
    _cityView = [TwoLinePickerView pickerviewWithFrame:frame];
    _cityView.backgroundColor = [UIColor whiteColor];
    _cityView.dataSource = self;
    _cityView.delegate = self;
    
    [self.popupContainerView addSubview:_cityView];
    UIColor *lineColor = [UIColor colorWithRGB:0x6fd4dd];
    [_cityView addTwoLineLayerWithColor:lineColor];
    
//    NSCalendar *calendar = [[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];
//    NSDateComponents *comp = [calendar components:NSCalendarUnitYear fromDate:[NSDate date]];
//    NSInteger year = [comp year];
//    
//    _yearRange = NSMakeRange(year-99, 100);
    
//    [_cityView selectItemAtIndexPath:[NSIndexPath indexPathForRow:99 inSection:0] animated:NO scrollPosition:UICollectionViewScrollPositionBottom];
    
    
    self.popupTitleLabel.text = NSLocalizedString(@"Enter the city", @"");
//    self.okButton.titleLabel.text = NSLocalizedString(@"Set", @"");
//    self.cancelButton.titleLabel.text = NSLocalizedString(@"Cancel", @"");
    
}

-(void)viewDidAppear:(BOOL)animated
{
    if (!self.selectedCityname || self.selectedCityname.length == 0){
        self.selectedCityname = self.citynames.firstObject;
    }
    else{
        NSInteger index = [self.citynames indexOfObject:self.selectedCityname];
        if (index >= 0){
            [_cityView selectItemAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0] animated:NO scrollPosition:UICollectionViewScrollPositionCenteredVertically];
        }
        else{
            self.selectedCityname = self.citynames.firstObject;
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.citynames.count;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    TwoLinePickerView *picker = (TwoLinePickerView*)collectionView;
    
    PickerViewCell *cell = [picker dequeueReusableCellForIndexPath:indexPath];
    
    //cell.textLabel.text = [NSString stringWithFormat:@"%02ld",  _yearRange.location + (long)indexPath.row, nil];
    cell.textLabel.text = self.citynames[indexPath.row];
    
    return cell;
}

-(void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
{
    self.okButton.enabled = NO;
}
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    self.okButton.enabled = NO;
}

- (void) scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (decelerate == YES) return;
    
    CGFloat heightOfCell = scrollView.bounds.size.height / 3;
    
    NSInteger offSetIndex = roundf((scrollView.contentOffset.y) / heightOfCell);
    CGFloat moveToY = (offSetIndex * heightOfCell);
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         CGPoint point = CGPointMake(0, moveToY);
                         scrollView.contentOffset = point;
                     }
                     completion:^(BOOL finished) {
                         [self updateSelectedYear:scrollView];
                     }
     ];
}

- (void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGFloat heightOfCell = scrollView.bounds.size.height / 3;
    
    NSInteger offSetIndex = roundf((scrollView.contentOffset.y) / heightOfCell);
    CGFloat moveToY = (offSetIndex * heightOfCell);
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         CGPoint point = CGPointMake(0, moveToY);
                         scrollView.contentOffset = point;
                     }
                     completion:^(BOOL finished) {
                         [self updateSelectedYear:scrollView];
                     }
     ];
}

- (void)updateSelectedYear:(UIScrollView*)scrollView
{
    CGFloat heightOfCell = scrollView.bounds.size.height / 3;
    
    NSInteger offSetIndex = roundf((scrollView.contentOffset.y) / heightOfCell);
    
    self.selectedCityname = self.citynames[offSetIndex];
    
    self.okButton.enabled = YES;
}

-(IBAction)clickSet:(id)sender
{
    self.completion(self);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [collectionView selectItemAtIndexPath:indexPath animated:NO scrollPosition:UICollectionViewScrollPositionCenteredVertically];
    [self updateSelectedYear:collectionView];
}


@end
