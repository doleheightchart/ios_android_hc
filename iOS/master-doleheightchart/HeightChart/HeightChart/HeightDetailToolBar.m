//
//  HeightDetailToolBar.m
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 5..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "HeightDetailToolBar.h"
#import "ToastController.h"

@interface HeightDetailToolBar (){
    CGRect _frameZoomBtn;
    CGRect _frameDeleteBtn;
    CGRect _frameGraphBtn;
    
    UIButton *_zoomBtn;
    UIButton *_zoomFullBtn;
    UIButton *_deleteEnterBtn;
    UIButton *_deleteBtn;
    UIButton *_graphBtn;
    UIButton *_returnBtn;
}

@end

@implementation HeightDetailToolBar

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self initialize];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self){
        [self initialize];
    }
    return self;
}

- (void)initialize
{
    self.backgroundColor = [UIColor clearColor];
    CGFloat w = self.bounds.size.width;
    CGFloat btnWidth = (88/2);
    CGFloat btnHeight = (88/2);
    
    
    _frameZoomBtn   = CGRectMake((14/2), 0, btnWidth, btnHeight);
    _frameDeleteBtn = CGRectMake(w - (btnWidth*2+9+7), 0, btnWidth, btnHeight);
    _frameGraphBtn  = CGRectMake(w - (btnWidth+7), 0, btnWidth, btnHeight);

    _zoomBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _zoomBtn.frame = _frameZoomBtn;
    [_zoomBtn setBackgroundImage:[UIImage imageNamed:@"detail_btn_full_view_normal"] forState:UIControlStateNormal];
    [_zoomBtn setBackgroundImage:[UIImage imageNamed:@"detail_btn_full_view_press"] forState:UIControlStateHighlighted];
    [_zoomBtn addTarget:self action:@selector(clickButton:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_zoomBtn];
    
    _deleteEnterBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _deleteEnterBtn.adjustsImageWhenHighlighted = NO;
    _deleteEnterBtn.backgroundColor = [UIColor clearColor];
    [_deleteEnterBtn setBackgroundImage:[UIImage imageNamed:@"delete_btn_normal"] forState:UIControlStateNormal];
    [_deleteEnterBtn setBackgroundImage:[UIImage imageNamed:@"delete_btn_dim_01"] forState:UIControlStateDisabled];
    [_deleteEnterBtn setBackgroundImage:[UIImage imageNamed:@"delete_btn_press"] forState:UIControlStateHighlighted];
    [_deleteEnterBtn addTarget:self action:@selector(clickButton:) forControlEvents:UIControlEventTouchUpInside];
    _deleteEnterBtn.frame = _frameDeleteBtn;
    [self addSubview:_deleteEnterBtn];
    
    
    _deleteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _deleteBtn.frame = _frameDeleteBtn;
    [_deleteBtn setBackgroundImage:[UIImage imageNamed:@"delete_btn_normal_02"] forState:UIControlStateNormal];
    [_deleteBtn setBackgroundImage:[UIImage imageNamed:@"delete_btn_press_02"] forState:UIControlStateHighlighted];
    [_deleteBtn setBackgroundImage:[UIImage imageNamed:@"delete_btn_dim_02"] forState:UIControlStateDisabled];
    [_deleteBtn addTarget:self action:@selector(clickButton:) forControlEvents:UIControlEventTouchUpInside];
    _deleteBtn.layer.opacity = 0;
    [self addSubview:_deleteBtn];
    
    _graphBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _graphBtn.frame = _frameGraphBtn;
    [_graphBtn setBackgroundImage:[UIImage imageNamed:@"detail_btn_graph_normal"] forState:UIControlStateNormal];
    [_graphBtn setBackgroundImage:[UIImage imageNamed:@"detail_btn_graph_press"] forState:UIControlStateSelected];
    [_graphBtn addTarget:self action:@selector(clickButton:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_graphBtn];
    
    _returnBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _returnBtn.frame = _frameDeleteBtn;
    [_returnBtn setBackgroundImage:[UIImage imageNamed:@"delete_btn_back_normal"] forState:UIControlStateNormal];
    [_returnBtn setBackgroundImage:[UIImage imageNamed:@"delete_btn_back_press"] forState:UIControlStateSelected];
    [_returnBtn addTarget:self action:@selector(clickButton:) forControlEvents:UIControlEventTouchUpInside];
    _returnBtn.layer.opacity = 0;
    [self addSubview:_returnBtn];
    
    

    _zoomFullBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _zoomFullBtn.frame = _frameZoomBtn;
    [_zoomFullBtn setBackgroundImage:[UIImage imageNamed:@"detail_btn_zoom_normal"] forState:UIControlStateNormal];
    [_zoomFullBtn setBackgroundImage:[UIImage imageNamed:@"detail_btn_zoom_press"] forState:UIControlStateSelected];
    [_zoomFullBtn addTarget:self action:@selector(clickButton:) forControlEvents:UIControlEventTouchUpInside];
    _zoomFullBtn.layer.opacity = 0;
    [self addSubview:_zoomFullBtn];
    
    
    _zoomBtn.tag = kDetailToolBarButtonIndexZoomOut;
    _zoomFullBtn.tag = kDetailToolBarButtonIndexZoomIn;
    _deleteBtn.tag = kDetailToolBarButtonIndexDelete;
    _returnBtn.tag = kDetailToolBarButtonIndexReturn;
    _graphBtn.tag = kDetailToolBarButtonIndexGraph;
    _deleteEnterBtn.tag = kDetailToolBarButtonIndexDeleteMode;
    
    //_deleteEnterBtn.enabled = NO;
    
}

-(void)clickButton:(UIButton*)button
{
//    self.isDeleteMode = !self.isDeleteMode;
    
//    [ToastController showToastWithMessage:@"This is Toast !!!!" duration:1];
    
    [self.toolBarDelegate clickToolbarWithIndex:button.tag];
}

-(void)setIsDeleteMode:(BOOL)isDeleteMode
{
    _isDeleteMode = isDeleteMode;
    [UIView animateWithDuration:1 animations:^{
        if (_isDeleteMode){
            _zoomBtn.layer.opacity = 0;
            _graphBtn.layer.opacity = 0;
            _deleteEnterBtn.layer.opacity = 0;
            _zoomFullBtn.layer.opacity = 0;
            
            _returnBtn.layer.opacity = 1;
            _deleteBtn.frame = _frameGraphBtn;
            _deleteBtn.layer.opacity = 1;
            _deleteBtn.enabled = NO;
        }
        else{
            _graphBtn.layer.opacity = 1;
            _zoomBtn.layer.opacity = !self.isZoomMode;
            _zoomFullBtn.layer.opacity = self.isZoomMode;
            _deleteEnterBtn.layer.opacity = 1;
            
            _returnBtn.layer.opacity = 0;
            _deleteBtn.frame = _frameDeleteBtn;
            _deleteBtn.layer.opacity = 0;
        }
    }];
}

-(void)setIsZoomMode:(BOOL)isZoomMode
{
    _isZoomMode = isZoomMode;
    [UIView animateWithDuration:1 animations:^{
        if (_isZoomMode){
            _zoomBtn.layer.opacity = 0;
            _zoomFullBtn.layer.opacity = 1;
            _deleteEnterBtn.layer.opacity = 0;
        }
        else{
            _zoomBtn.layer.opacity = 1;
            _zoomFullBtn.layer.opacity = 0;
            _deleteEnterBtn.layer.opacity = 1;
            
        }
    }];

}

-(void)setEnableDelete:(BOOL)enableDelete
{
    _enableDelete = enableDelete;
    
    _deleteBtn.enabled = enableDelete;
}

-(void)setEnableDeleteMode:(BOOL)enableDeleteMode
{
    _enableDeleteMode = enableDeleteMode;
    
    _deleteEnterBtn.enabled = _enableDeleteMode;
}


-(BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event
{
    if (point.x > 50 && point.x < (320 - (88+88+14)/2) )
        return NO;
    
    return [super pointInside:point withEvent:event];
    //return [_deleteBtn pointInside:point withEvent:event] || [_graphBtn pointInside:point withEvent:event];
}


@end
