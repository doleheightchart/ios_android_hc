//
//  NSObject+HeightDatabase.m
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 4. 5..
//  Copyright (c) 2014년 Kim Sehyun. All rights reserved.
//

#import "NSObject+HeightDatabase.h"
#import "AppDelegate.h"
#import "MyChild.h"
#import "ChildHeight.h"

@implementation NSObject (HeightDatabase)

#define kDidChangeDBStore @"DidChangeDBStore"

-(NSManagedObjectModel*)usedObjectModel
{
    AppDelegate* appController = (AppDelegate*)[UIApplication sharedApplication].delegate;
    return appController.managedObjectModel;
}

-(NSManagedObjectContext*)usedObjectContext
{
    AppDelegate* appController = (AppDelegate*)[UIApplication sharedApplication].delegate;
    return appController.currentObjectContext;
}

-(NSFetchedResultsController*)requestFetchedResultController
{
    AppDelegate* appController = (AppDelegate*)[UIApplication sharedApplication].delegate;

    if (appController.userConfig.useiCloud)
        return self.requestCloudFetchedResultController;
    else
        return self.requestLocalFetchedResultController;
}


-(NSFetchedResultsController*)requestLocalFetchedResultController
{
    AppDelegate* appController = (AppDelegate*)[UIApplication sharedApplication].delegate;

    NSManagedObjectContext *moc = appController.localManagedObjectContext;

    NSFetchRequest *rsq = [NSFetchRequest fetchRequestWithEntityName:@"MyChild"];
    NSSortDescriptor *st = [NSSortDescriptor sortDescriptorWithKey:@"createdDate" ascending:YES];
    rsq.sortDescriptors = @[st];
    NSFetchedResultsController *fc = [[NSFetchedResultsController alloc] initWithFetchRequest:rsq
                                                                         managedObjectContext:moc
                                                                           sectionNameKeyPath:Nil
                                                                                    cacheName:@"MyChild"];

    NSError *error;

    [fc performFetch:&error];

    if (error){
        assert(0);
    }
    
    return fc;
}

-(NSFetchedResultsController*)requestCloudFetchedResultController
{
    AppDelegate* appController = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    NSManagedObjectContext *moc = appController.cloudManagedObjectContext;
    
    NSFetchRequest *rsq = [NSFetchRequest fetchRequestWithEntityName:@"MyChild"];
    NSSortDescriptor *st = [NSSortDescriptor sortDescriptorWithKey:@"createdDate" ascending:YES];
    rsq.sortDescriptors = @[st];
    NSFetchedResultsController *fc = [[NSFetchedResultsController alloc] initWithFetchRequest:rsq
                                                                         managedObjectContext:moc
                                                                           sectionNameKeyPath:Nil
                                                                                    cacheName:@"MyChild"];
    
    NSError *error;
    
    [fc performFetch:&error];
    
    if (error){
        assert(0);
    }
    else{
        MyChild *first = fc.fetchedObjects.firstObject;
        NSLog(@"Nickname is %@", first.nickname);
    }
    
    return fc;
}


-(BOOL)moveLocalDBToCloudDBWithLocalFetchedResultController:(NSFetchedResultsController*)localFetchedResultController //처음시작시 여러번 불리기에 캐시된 콘트롤러를 받아야 한다.
{
    if (localFetchedResultController.fetchedObjects.count > 0){
        
        //Merge Process
        NSLog(@"Merge Local Database To Cloud Database.");
        
        AppDelegate* appController = (AppDelegate*)[UIApplication sharedApplication].delegate;
        
        NSManagedObjectContext *coc = appController.cloudManagedObjectContext;
        NSManagedObjectContext *loc = appController.localManagedObjectContext;
        
        for (MyChild *child in localFetchedResultController.fetchedObjects) {
            
            MyChild *newObj = [NSEntityDescription insertNewObjectForEntityForName:@"MyChild"
                                                            inManagedObjectContext:coc];
            newObj.nickname = child.nickname;
            newObj.birthday = child.birthday;
            newObj.charterNo = child.charterNo;
            newObj.isGirl = child.isGirl;
            newObj.createdDate = child.createdDate;
            
            for (ChildHeight *heightInfo in child.heightHistory) {
                
                ChildHeight *newHeightInfo = [NSEntityDescription insertNewObjectForEntityForName:@"ChildHeight"
                                                                           inManagedObjectContext:coc];
                
                newHeightInfo.takenDate = heightInfo.takenDate;
                newHeightInfo.height = heightInfo.height;
                newHeightInfo.photo = heightInfo.photo;
                newHeightInfo.qrcode = heightInfo.qrcode;
                newHeightInfo.thumbphoto = heightInfo.thumbphoto;
                newHeightInfo.stickerType = heightInfo.stickerType;
                
                [newObj addHeightHistoryObject:newHeightInfo];
                
            }
            
            [loc deleteObject:child];
            [coc insertObject:newObj];
        }

        if ([coc hasChanges]){
            NSError *error;
            
            [coc save:&error];
            
            if (!error)
                [loc save:&error];
            else{
                NSLog(@"Error is %@", error);
                abort();
            }
        }
        
        return YES;
    }
    
    return NO;
}



-(void)moveCloudDBToLocalDB
{
        //Merge Process
    NSLog(@"Merge Cloud Database To Local Database.");
    
    AppDelegate* appController = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    NSManagedObjectContext *coc = appController.cloudManagedObjectContext;
    NSManagedObjectContext *loc = appController.localManagedObjectContext;
    NSError *error;
    
    NSFetchRequest *rsq = [NSFetchRequest fetchRequestWithEntityName:@"MyChild"];
    NSArray *childArray = [coc executeFetchRequest:rsq error:&error];

    
    for (MyChild *child in childArray) {
        
        MyChild *newObj = [NSEntityDescription insertNewObjectForEntityForName:@"MyChild"
                                                        inManagedObjectContext:loc];
        newObj.nickname = child.nickname;
        newObj.birthday = child.birthday;
        newObj.charterNo = child.charterNo;
        newObj.isGirl = child.isGirl;
        newObj.createdDate = child.createdDate;
        
        for (ChildHeight *heightInfo in child.heightHistory) {
            
            ChildHeight *newHeightInfo = [NSEntityDescription insertNewObjectForEntityForName:@"ChildHeight"
                                                                       inManagedObjectContext:loc];
            
            newHeightInfo.takenDate = heightInfo.takenDate;
            newHeightInfo.height = heightInfo.height;
            newHeightInfo.photo = heightInfo.photo;
            newHeightInfo.qrcode = heightInfo.qrcode;
            newHeightInfo.thumbphoto = heightInfo.thumbphoto;
            newHeightInfo.stickerType = heightInfo.stickerType;
            
            [newObj addHeightHistoryObject:newHeightInfo];
            
        }
        
//        [coc deleteObject:child]; //클라우드를 지우지 않는다.
        [loc insertObject:newObj];
    }
    
    if ([loc hasChanges]){
        [loc save:&error];
        
        if (!error){
//            [coc save:&error]; //클라우드를 지우지 않는다.
        }
        else{
            
        }
    }
    
//    [NSFileCoordinator removeFilePresenter:self];
    
    /* 아이클라우드를 지우지 않는다.
    if ([appController.cloudPersistentStoreCoordinator removePersistentStore:appController.iCloudStore error:&error]) {
        NSLog(@"Removed iCloud Store");
        appController.iCloudStore = nil;
    } else {
        NSLog(@"Error removing iCloud Store: %@", error);
    }
    */
    
    if (NO == appController.deleteAccountMode)
        appController.iCloudStore = nil;
    
    
}

-(void)removeCloudDB
{
    
    
    AppDelegate* appController = (AppDelegate*)[UIApplication sharedApplication].delegate;
    NSError *error;
    
    

//    if ([appController.cloudPersistentStoreCoordinator removePersistentStore:appController.iCloudStore error:&error]) {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    // ** Note: if you adapt this code for your own use, you should change this variable:
    NSString *dataFileName = @"HeightChart.sqlite";
    
    // ** Note: For basic usage you shouldn't need to change anything else
    
    NSString *iCloudDataDirectoryName = @"Data.nosync";
    NSString *iCloudLogsDirectoryName = @"Logs";
    NSURL *localStore = [[appController applicationDocumentsDirectory] URLByAppendingPathComponent:dataFileName];
    NSURL *iCloud = [fileManager URLForUbiquityContainerIdentifier:nil];
    if (iCloud == nil) return;
    
    NSURL *iCloudLogsPath = [NSURL fileURLWithPath:[[iCloud path] stringByAppendingPathComponent:iCloudLogsDirectoryName]];
//    NSString *iCloudEnabledAppID = @"FGUL65Q4E4.com.dole.HeightChart";
    NSString *iCloudEnabledAppID = APP_iCLOUD_APPID;

    NSMutableDictionary *options = [NSMutableDictionary dictionary];
    [options setObject:[NSNumber numberWithBool:YES] forKey:NSMigratePersistentStoresAutomaticallyOption];
    [options setObject:[NSNumber numberWithBool:YES] forKey:NSInferMappingModelAutomaticallyOption];
    [options setObject:iCloudEnabledAppID            forKey:NSPersistentStoreUbiquitousContentNameKey];
    [options setObject:iCloudLogsPath                forKey:NSPersistentStoreUbiquitousContentURLKey];
    
    
    NSString *iCloudData = [[[iCloud path]
                             stringByAppendingPathComponent:iCloudDataDirectoryName]
                            stringByAppendingPathComponent:dataFileName];
    
    if ([NSPersistentStoreCoordinator removeUbiquitousContentAndPersistentStoreAtURL:[NSURL fileURLWithPath:iCloudData] options:options error:&error]){
        NSLog(@"Removed iCloud Store");
    } else {
        NSLog(@"Error removing iCloud Store: %@", error);
    }
    
    [appController removeCloudProperties];
}

-(void)didChangeDBStore:(NSNotification *)notification
{
}

-(void)iCloudReady:(NSNotification*)notification
{
    
}

-(void)saveContext
{
    AppDelegate* appController = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appController saveContext];
}

- (void)addObserverSelfChangeDBStore
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangeDBStore:) name:kDidChangeDBStore object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(iCloudReady:) name:@"iCloudReady" object:nil];
}

- (void)sendDBStoreChangeNotification
{
    [[NSNotificationCenter defaultCenter]
     postNotificationName:kDidChangeDBStore
     object:nil];
}



- (void)registerForiCloudNotifications {
    
    AppDelegate* appController = (AppDelegate*)[UIApplication sharedApplication].delegate;

    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
	NSPersistentStoreCoordinator *cps = appController.cloudPersistentStoreCoordinator;
    
    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_6_1){
        [notificationCenter addObserver:self
                               selector:@selector(storesWillChange:)
                                   name:NSPersistentStoreCoordinatorStoresWillChangeNotification
                                 object:cps];
    }
    
    [notificationCenter addObserver:self
                           selector:@selector(storesDidChange:)
                               name:NSPersistentStoreCoordinatorStoresDidChangeNotification
                             object:cps];
    
    [notificationCenter addObserver:self
                           selector:@selector(persistentStoreDidImportUbiquitousContentChanges:)
                               name:NSPersistentStoreDidImportUbiquitousContentChangesNotification
                             object:cps];
}

- (void)removeiCloudNotifications
{
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    AppDelegate* appController = (AppDelegate*)[UIApplication sharedApplication].delegate;
	NSPersistentStoreCoordinator *cps = appController.cloudPersistentStoreCoordinator;
    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_6_1){
        [notificationCenter removeObserver:self name:NSPersistentStoreCoordinatorStoresWillChangeNotification object:cps];
    }
    [notificationCenter removeObserver:self name:NSPersistentStoreCoordinatorStoresDidChangeNotification object:cps];
    [notificationCenter removeObserver:self name:NSPersistentStoreDidImportUbiquitousContentChangesNotification object:cps];
}

- (void)storesWillChange:(NSNotification *)notification {}
- (void)storesDidChange:(NSNotification *)notification {}
- (void)persistentStoreDidImportUbiquitousContentChanges:(NSNotification *)notification {}

-(void)migrationCloudDB
{
    //Merge Process
    NSLog(@"Merge Cloud Database To Local Database.");
    
    AppDelegate* appController = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    NSManagedObjectContext *coc = appController.cloudManagedObjectContext;
    NSError *error;
    
    NSFetchRequest *rsq = [NSFetchRequest fetchRequestWithEntityName:@"MyChild"];
    NSArray *childArray = [coc executeFetchRequest:rsq error:&error];
    
    NSMutableDictionary *childByNickname = [NSMutableDictionary dictionary];
    
    for (MyChild *child in childArray) {
        
        if (nil == childByNickname[child.nickname]){
            NSMutableArray *dupArray = [NSMutableArray array];
            [childByNickname setObject:dupArray forKey:child.nickname];
        }
        
        NSMutableArray *dArray = childByNickname[child.nickname];
        [dArray addObject:child];
    }

    NSMutableArray *deleteArray = [NSMutableArray array];
    
    for (NSString *nickname in childByNickname.allKeys) {
        NSMutableArray *array = childByNickname[nickname];
        if (array.count <= 1) continue;
        
        int duplicount = 0;
        while (array.count){
            NSMutableArray *sameNicknameArray = [NSMutableArray array];
            MyChild *finder = array.lastObject;
            for (int i = array.count - 2; i >= 0; i--) {
                MyChild *ch = array[i];
                
                if (finder.isGirl == ch.isGirl && [finder.birthday isEqualToDate:ch.birthday]){
                    [sameNicknameArray addObject:ch];
                    [array removeObjectAtIndex:i];
                }
            }
            
            if (sameNicknameArray.count){
                
                //Merge
                for (MyChild *sameChild in sameNicknameArray) {
                    NSSet *heights = sameChild.heightHistory;
                    [finder addHeightHistory:heights];
                    [sameChild removeHeightHistory:heights];
                    [deleteArray addObject:sameChild];
                }
                
                if (duplicount > 0){
                    finder.nickname = [NSString stringWithFormat:@"%@(%d)", finder.nickname, duplicount];
                }
            }
            
            duplicount++;
        }
        
    }
    
    for (MyChild *deleteObj in deleteArray) {
        [coc deleteObject:deleteObj];
    }
    
    if ([coc hasChanges]){
        [coc save:&error];
        
        if (!error){
            NSLog(@"Error is %@", error);
            abort();
        }
    }
    
}

-(void)mergeCloudDB
{
    //Merge Process
    NSLog(@"Merge Cloud Database To Local Database.");
    
    AppDelegate* appController = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    NSManagedObjectContext *coc = appController.cloudManagedObjectContext;
    NSError *error;
    
    NSFetchRequest *rsq = [NSFetchRequest fetchRequestWithEntityName:@"MyChild"];
    NSArray *childArray = [coc executeFetchRequest:rsq error:&error];
    
    NSMutableDictionary *childByNickname = [NSMutableDictionary dictionary];
    
    for (MyChild *child in childArray) {
        
        if (child.nickname == nil)return;
        
        if (nil == childByNickname[child.nickname]){
            NSMutableArray *dupArray = [NSMutableArray array];
            [childByNickname setObject:dupArray forKey:child.nickname];
        }
        
        NSMutableArray *dArray = childByNickname[child.nickname];
        [dArray addObject:child];
    }
    
    NSMutableArray *deleteArray = [NSMutableArray array];
    
    for (NSString *nickname in childByNickname.allKeys) {
        NSMutableArray *array = childByNickname[nickname];
        if (array.count <= 1) continue;
        
        //이름이 중복된 닉네임 발견
        
        int duplicount = 0;
        while (array.count){
            
            NSMutableArray *sameNicknameArray = [NSMutableArray array];
            MyChild *finder = array.lastObject;
            
            for (int i = array.count - 2; i >= 0; i--) {
                MyChild *ch = array[i];
                
                if (finder.isGirl == ch.isGirl && [finder.birthday isEqualToDate:ch.birthday]){ // 성별이 같고, 생일이 같으면 동일한 닉네임
                    [sameNicknameArray addObject:ch];
                    [array removeObjectAtIndex:i];
                }
                else{ // 닉네임을 바꿔준다.
                    
                }
            }
            
            if (sameNicknameArray.count){
                
                //Merge
                for (MyChild *sameChild in sameNicknameArray) {
                    NSSet *heights = sameChild.heightHistory;
                    [finder addHeightHistory:heights];
                    [sameChild removeHeightHistory:heights];
                    [deleteArray addObject:sameChild];
                }
                
                [self mergeHeightsWithChild:finder];
            }
            
            if (duplicount > 0){
                NSString *dupNickname = [NSString stringWithFormat:@"%@(%d)", finder.nickname, duplicount];
                while (childByNickname[dupNickname]) {
                    duplicount++;
                    dupNickname = [NSString stringWithFormat:@"%@(%d)", finder.nickname, duplicount];
                }
                finder.nickname = dupNickname;
            }
            
            [array removeObject:finder];
            
            duplicount++;
        }
        
    }
    
    for (MyChild *deleteObj in deleteArray) {
        [coc deleteObject:deleteObj];
    }
    
    if ([coc hasChanges]){
        [coc save:&error];
        
        if (error){
            NSLog(@"Error is %@", error);
            abort();
        }
    }
}

-(void)mergeHeightsWithChild:(MyChild*)myChild
{
    NSMutableDictionary *heightByCouponCode = [NSMutableDictionary dictionary];
    for (ChildHeight *ch in myChild.heightHistory) {
        
        if (heightByCouponCode[ch.qrcode] == nil){
            heightByCouponCode[ch.qrcode] = [NSMutableArray array];
        }
        
        NSMutableArray *array = heightByCouponCode[ch.qrcode];
        [array addObject:ch];
    }

    for (NSString *qrcode in heightByCouponCode.allKeys) {
        NSMutableArray *qrcodeSameArray = heightByCouponCode[qrcode];
        
        if (qrcodeSameArray.count <= 1) continue;
        
        [qrcodeSameArray sortUsingComparator:^NSComparisonResult(ChildHeight* obj1, ChildHeight* obj2) {
            //기록된 날짜로 정렬
            NSDate *date1 = obj1.takenDate;
            NSDate *date2 = obj2.takenDate;
            
            return [date2 compare:date1];
        }];
        
        NSDate *test = [qrcodeSameArray.firstObject takenDate];
        
        for (int i = 1; i < qrcodeSameArray.count; i++) {
            ChildHeight *deleteHeight = qrcodeSameArray[i];
            [myChild removeHeightHistoryObject:deleteHeight];
        }
        
    }
}

-(BOOL)canUseiCloud
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *iCloud = [fileManager URLForUbiquityContainerIdentifier:nil];
    
    return (iCloud != nil);
}


@end




//@implementation UIViewController(DoleHeightChartData)
//
//#pragma mark CoreData
//
//
//
//-(NSPersistentStoreCoordinator*)persistentStoreCoordinator
//{
//    AppDelegate* appController = (AppDelegate*)[UIApplication sharedApplication].delegate;
//    return appController.persistentStoreCoordinator;
//}
//
//-(NSFetchedResultsController*)requestChildren
//{
//    NSManagedObjectContext *moc = self.managedObjectContext;
//    
//    NSFetchRequest *rsq = [NSFetchRequest fetchRequestWithEntityName:@"MyChild"];
//    NSSortDescriptor *st = [NSSortDescriptor sortDescriptorWithKey:@"createdDate" ascending:YES];
//    rsq.sortDescriptors = @[st];
//    NSFetchedResultsController *fc = [[NSFetchedResultsController alloc] initWithFetchRequest:rsq
//                                                                         managedObjectContext:moc
//                                                                           sectionNameKeyPath:Nil
//                                                                                    cacheName:@"MyChild"];
//    
//    NSError *error;
//    
//    [fc performFetch:&error];
//    
//    if (error){
//        assert(0);
//    }
//    
//    return fc;
//}
//

//
//-(void)initailizeDB
//{
//    AppDelegate* appController = (AppDelegate*)[UIApplication sharedApplication].delegate;
//    [appController initializeDBWithTarget:self Selector:@selector(reloadFetchedResults:)];
//}
//
//-(void)reloadFetchedResults:(NSNotificationCenter*)noti
//{
//    
//}
//@end
