//
//  PhotoToolbarView.h
//  HeightChart
//
//  Created by ne on 2014. 3. 3..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhotoButton.h"

typedef void (^ClickPhotoFrameDef)(PhotoButtonControl*);
typedef void (^ClickCaptureDef)(void);
typedef void (^ClickShowLogInToast)(BOOL);

@interface PhotoToolbarView : UIView

@property (nonatomic, copy) ClickPhotoFrameDef onClickSelectFrameButton;
@property (nonatomic, copy) ClickCaptureDef onClickCenterCameraButton;
@property (nonatomic, copy) ClickShowLogInToast onClickShowLogin;


- (void)stateChangeWithLogInOption:(BOOL)isLogin;
- (void)selectFrameToolBarFrameByType:(enum DolePhotoButtonType)type;
- (void)setCaptureButtonState:(enum DoleCaptureButtonState)state;

- (id)initWithPhotoToolbar;

@end
