//
//  TwoLinePickerView.h
//  TestProvision
//
//  Created by Kim Sehyun on 2014. 2. 24..
//  Copyright (c) 2014년 Kim Sehyun. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kTwoLinePickerViewCellHeight (90/2)

@interface TwoLinePickerView : UICollectionView
@property (nonatomic, readonly) NSIndexPath *currentSelectedIndexPath;

@end

@interface PickerViewCell : UICollectionViewCell
@property (nonatomic, strong) UILabel *textLabel;
@end

@interface TwoLinePickerView (TwoLine)
-(void)addTwoLineLayerWithColor:(UIColor*)color;
-(PickerViewCell*)dequeueReusableCellForIndexPath:(NSIndexPath*)indexPath;
+(instancetype)pickerviewWithFrame:(CGRect)frame;

@end
