//
//  SignInViewController.m
//  HeightChart
//
//  Created by ne on 2014. 2. 11..
//  Copyright (c) 2014년 ne. All rights reserved.
//

#import "SignInViewController.h"
#import "UIView+ImageUtil.h"
#import "TextViewDualPlaceHolder.h"
#import "UIButton+CheckAndRadio.h"
#import "UIViewController+DoleHeightChart.h"
#import "UIFont+DoleHeightChart.h"
#import "UIColor+ColorUtil.h"
#import "ToastController.h"
#import "BirthPopupController.h"
#import "WarningCoverView.h"
#import "CityPopupController.h"
#import "TermsViewController.h"
#import "UIViewController+PolyServer.h"
#import "MessageBoxController.h"
#import "InputChecker.h"

#import "AppDelegate.h"

#import "Mixpanel.h"

@interface SignInViewController (){
    NSMutableData   *_receivedData;
    
    InputChecker    *_inputChecker;
    
    //longzhe cui added
    BOOL bDidStartDataInput;
}
//@property (weak, nonatomic) IBOutlet UILabel *lblEmail;
@property (weak, nonatomic) IBOutlet TextViewDualPlaceHolder *txtViewEmail;
@property (nonatomic, weak) IBOutlet UIButton *btnSignIn;
@property (weak, nonatomic) IBOutlet TextViewDualPlaceHolder *txtViewPassword;
@property (weak, nonatomic) IBOutlet TextViewDualPlaceHolder *txtViewConfirmPassword;
//@property (weak, nonatomic) IBOutlet TextViewDualPlaceHolder *txtViewHaveChid;
//@property (weak, nonatomic) IBOutlet UILabel *lblhave;

@property (weak, nonatomic) IBOutlet TextViewDualPlaceHolder *txtViewBirth;
//@property (weak, nonatomic) IBOutlet TextViewDualPlaceHolder *txtViewCountry;
@property (weak, nonatomic) IBOutlet TextViewDualPlaceHolder *txtViewCity;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckMale;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckFemale;
@property (weak, nonatomic) IBOutlet UILabel *lblAgree;
@property (weak, nonatomic) IBOutlet UILabel *lblMale;
@property (weak, nonatomic) IBOutlet UILabel *lblFemale;


@property (weak, nonatomic) IBOutlet UIButton *btnAgree;

- (IBAction)onCheckTermsOfService:(id)sender;

- (IBAction)clickSignIn:(id)sender;

@end

@implementation SignInViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    [self.btnSignIn setResizableImageWithType:DoleButtonImageTypeOrange];
    [self.txtViewEmail setDualPlaceHolderTextView:DoleDualPlaceHolderTextTypeEmail];
    [self.txtViewPassword setDualPlaceHolderTextView:DoleDualPlaceHolderTextTypePassword];

    
    [self.txtViewConfirmPassword setDualPlaceHolderTextView:DoleDualPlaceHolderTextTypePassword];
    [self.txtViewConfirmPassword.textField setReDrawPlaceHolder:NSLocalizedString(@"Confirm password", @"") SecondText:@""];
    [self.txtViewBirth setDualPlaceHolderTextView:DoleDualPlaceHolderTextTypeBirth];
    [self.txtViewBirth.textField setReDrawPlaceHolder:NSLocalizedString(@"Year of birth", @"") SecondText:@""];
    [self.txtViewCity setDualPlaceHolderTextView:DoleDualPlaceHolderTextTypeCity];
    [self.txtViewCity.textField setReDrawPlaceHolder:NSLocalizedString(@"City", @"") SecondText:@""];
    
    self.txtViewBirth.textField.delegate = self;
    self.txtViewCity.textField.delegate = self;
    
    //longzhe cui added 3 lines to track data log-in events
    self.txtViewEmail.textField.delegate = self;
    self.txtViewPassword.textField.delegate = self;
    self.txtViewConfirmPassword.textField.delegate = self;
    
    //longzhe cui added
    [[Mixpanel sharedInstance] timeEvent:@"Signup Success"];
    
    [self addCommonCloseButton];
    
    [UIButton makeUIRadioButtons:self.btnCheckMale secondButton:self.btnCheckFemale];
    self.btnCheckMale.selected = YES;
    
    self.lblMale.font = [UIFont defaultRegularFontWithSize:34/2];
    self.lblMale.textColor = [UIColor colorWithRGB:0x606060];
    
    self.lblFemale.font = [UIFont defaultRegularFontWithSize:34/2];
    self.lblFemale.textColor = [UIColor colorWithRGB:0x606060];
    
    self.lblAgree.font = [UIFont defaultRegularFontWithSize:24/2];
    self.lblAgree.textColor = [UIColor colorWithRGB:0x606060];
    
    [self.btnAgree makeUICheckBox];

    
    [self setupControlFlexiblePosition];
    
    [self.btnAgree addTarget:self action:@selector(onCheckTermsOfService:) forControlEvents:UIControlEventTouchUpInside];
    
//    [self addKeyboardObserver];
    
    self.txtViewPassword.textField.secureTextEntry = YES;
    self.txtViewConfirmPassword.textField.secureTextEntry = YES;
    
    self.txtViewConfirmPassword.passwordOriginalView = self.txtViewPassword;
    
    _inputChecker = [[InputChecker alloc]init];
    [_inputChecker addHostViewRecursive:self.view submitButton:self.btnSignIn checkBoxButtons:@[self.btnAgree]];
    
    [[Mixpanel sharedInstance] track:@"Opened Sign Up"];
    
//    [self.btnAgree inflateBounds];
//    [self.btnCheckFemale inflateBounds];
//    [self.btnCheckMale inflateBounds];
}

-(void)closeMe:(id)sender
{
    [[self.view findFirstResponder] resignFirstResponder];
    
    //    if (self.navigationController)
    //        [self.navigationController popViewControllerAnimated:YES];
    //    else
    //        [self dismissViewControllerAnimated:YES completion:nil];
    
//    [self showOkCancelConfirmMessage:NSLocalizedString(@"Do you want to cancel sign up?", @"") completion:^(BOOL isYes) {
    [self showYesNoConfirmMessage:NSLocalizedString(@"Do you want to cancel sign up?", @"") completion:^(BOOL isYes) {
        if (NO == isYes)return;
        
        [self closeViewControllerAnimated:NO];
    }];
    
    
    //[self closeViewControllerAnimated:NO];
}

-(void)viewDidAppear:(BOOL)animated
{
    [self checkNetwork];
}

-(void)setupControlFlexiblePosition
{
    CGFloat DefalutMargin = 210;
    
    if ([UIScreen isFourInchiScreen]) {
        

//        [self moveToPositionYPixel:210 Control:self.txtViewEmail];
        
        [self moveToPositionYPixel:(DefalutMargin) Control:self.txtViewEmail];
        [self moveToPositionYPixel:(DefalutMargin + 72 + 22) Control:self.txtViewPassword];
        [self moveToPositionYPixel:(DefalutMargin + 72 + 22 + 72 + 22) Control:self.txtViewConfirmPassword];
        
//        [self moveToPositionYPixel:(236 + 36 + 30 + 72 + 22 + 72 + 22) Control:self.txtViewHaveChid];
//        [self moveToPositionYPixel:(236 + 36 + 30 + 72 + 22 + 72 + 22) Control:self.lblhave];
        [self moveToPositionYPixel:(DefalutMargin + 72 + 22 + 72 + 22 + 72 + 22 ) Control:self.txtViewBirth];

//        [self moveToPositionYPixel:(236 + 36 + 30 + 72 + 22 + 72 + 22 + 72 + 22 + 72 + 22) Control:self.txtViewCountry];
        [self moveToPositionYPixel:(DefalutMargin + 72 + 22 + 72 + 22 + 72 + 22 + 72 + 22) Control:self.txtViewCity];
        
        [self moveToPositionYPixel:(DefalutMargin + 72 + 22 + 72 + 22 + 72 + 22 + 72 + 22 + 72 + 38) Control:self.btnCheckMale];
        [self moveToPositionYPixel:(DefalutMargin + 72 + 22 + 72 + 22 + 72 + 22 + 72 + 22 + 72 + 38) Control:self.btnCheckFemale];
        
        [self moveToPositionYPixel:(DefalutMargin + 72 + 22 + 72 + 22 + 72 + 22 + 72 + 22 + 72 + 38 + 56+  38) Control:self.btnAgree];
        [self moveToPositionYPixel:(DefalutMargin + 72 + 22 + 72 + 22 + 72 + 22 + 72 + 22 + 72 + 38 + 56+  38) Control:self.lblAgree];
        
        [self moveToPositionYPixel:(DefalutMargin + 72 + 22 + 72 + 22 + 72 + 22 + 72 + 22 + 72 + 38 + 56+  38 + 51+ 153) Control:self.btnSignIn];
        
    }
    else{

        DefalutMargin = 90;
        //        [self moveToPositionYPixel:210 Control:self.txtViewEmail];
        
        [self moveToPositionYPixel:(DefalutMargin) Control:self.txtViewEmail];
        [self moveToPositionYPixel:(DefalutMargin + 72 + 22) Control:self.txtViewPassword];
        [self moveToPositionYPixel:(DefalutMargin + 72 + 22 + 72 + 22) Control:self.txtViewConfirmPassword];
        
        //        [self moveToPositionYPixel:(236 + 36 + 30 + 72 + 22 + 72 + 22) Control:self.txtViewHaveChid];
        //        [self moveToPositionYPixel:(236 + 36 + 30 + 72 + 22 + 72 + 22) Control:self.lblhave];
        [self moveToPositionYPixel:(DefalutMargin + 72 + 22 + 72 + 22 + 72 + 22 ) Control:self.txtViewBirth];
        
        //        [self moveToPositionYPixel:(236 + 36 + 30 + 72 + 22 + 72 + 22 + 72 + 22 + 72 + 22) Control:self.txtViewCountry];
        [self moveToPositionYPixel:(DefalutMargin + 72 + 22 + 72 + 22 + 72 + 22 + 72 + 22) Control:self.txtViewCity];
        
        [self moveToPositionYPixel:(DefalutMargin + 72 + 22 + 72 + 22 + 72 + 22 + 72 + 22 + 72 + 38) Control:self.btnCheckMale];
        [self moveToPositionYPixel:(DefalutMargin + 72 + 22 + 72 + 22 + 72 + 22 + 72 + 22 + 72 + 38) Control:self.btnCheckFemale];
        
        [self moveToPositionYPixel:(DefalutMargin + 72 + 22 + 72 + 22 + 72 + 22 + 72 + 22 + 72 + 38 + 56+  38) Control:self.btnAgree];
        [self moveToPositionYPixel:(DefalutMargin + 72 + 22 + 72 + 22 + 72 + 22 + 72 + 22 + 72 + 38 + 56+  38) Control:self.lblAgree];
        
        [self moveToPositionYPixel:(DefalutMargin + 72 + 22 + 72 + 22 + 72 + 22 + 72 + 22 + 72 + 38 + 56+  38 + 51+ 97) Control:self.btnSignIn];
    }
    
     [self moveToPositionY:self.btnCheckMale.frame.origin.y Control:self.lblMale];
     [self moveToPositionY:self.btnCheckFemale.frame.origin.y Control:self.lblFemale];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)checkNetwork
{
    if (NO == self.isEnableNetwork){
        

//        self.txtViewEmail.textField.enabled = NO;
//        self.btnSignIn.enabled = NO;
//        self.txtViewPassword.textField.enabled = NO;
//        self.txtViewConfirmPassword.textField.enabled = NO;
//        self.txtViewBirth.textField.enabled = NO;
//        //self.txtViewCountry.textField.enabled = NO;
//        //TextViewDualPlaceHolder *txtViewCity;
//        self.btnCheckMale.enabled = NO;
//        self.btnCheckFemale.enabled = NO;
//        self.btnAgree.enabled = NO;
//
//        [ToastController showToastWithMessage:NSLocalizedString(@"The network is unstable. \nPlease try again after checking the network", @"") duration:5];
//
    }
    else{
        if (nil == self.userConfig.cachedCitynames) {
            NSDictionary *englishCountryNamesCities = [self englishCountryNamesAndCities];
            NSDictionary *countryCodesAndEnglishNames = [self countriesCodesAndEnglishCommonNames];
            NSString *countryCode = self.currentCountryCode;
            NSString *englishCountryName = [countryCodesAndEnglishNames objectForKey:countryCode];
            NSArray *sortedArray = [englishCountryNamesCities objectForKey:englishCountryName];// sortedArrayUsingSelector:@selector(compare:)];
            self.userConfig.cachedCitynames = sortedArray;
        }
    }
}

#pragma mark TextDelegate

-(void)showBirthPopup
{
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Popup" bundle:nil];
    BirthPopupController *birthdayController = [story instantiateViewControllerWithIdentifier:@"BirthPopup"];
    birthdayController.birthYear = [self.txtViewBirth.textField.text integerValue];
    birthdayController.completion = ^(BirthPopupController *controller){
//        self.txtViewBirth.textField.text = [NSString stringWithFormat:@"%lu", (unsigned long)controller.birthYear, nil];
        NSString *text = [NSString stringWithFormat:@"%lu", (unsigned long)controller.birthYear, nil];
        [self.txtViewBirth setText:text];

        [controller releaseModal];
    };
    [birthdayController showModalOnTheViewController:nil];
}

-(void)showCityPopup
{
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Popup" bundle:nil];
    CityPopupController *cityPopup = [story instantiateViewControllerWithIdentifier:@"City"];
//    cityPopup.citynames = @[@"Seoul", @"Daegu", @"Daejun", @"Pusan", @"Sejong", @"Suwon", @"Gawngju"];
    cityPopup.citynames = self.userConfig.cachedCitynames;
    cityPopup.selectedCityname = self.txtViewCity.textField.text;
    cityPopup.completion = ^(CityPopupController *controller){
//        self.txtViewCity.textField.text = controller.selectedCityname;
        NSString *text = controller.selectedCityname;
        [self.txtViewCity setText:text];
        
        [controller releaseModal];
    };
    [cityPopup showModalOnTheViewController:nil];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField        // return NO to disallow editing.
{
    //longzhe cui added
    if (bDidStartDataInput == false)
    {
        bDidStartDataInput = true;
        [[Mixpanel sharedInstance] track:@"Signup Data Log-in"];
    }
 
    if ((textField != self.txtViewBirth.textField) && (textField != self.txtViewCity.textField)) return YES;
    
    [self.view.findFirstResponder resignFirstResponder];
    
    if (textField == self.txtViewBirth.textField){
        [self showBirthPopup];
    }
    else if (textField == self.txtViewCity.textField){
        [self showCityPopup];
    }
    
    return NO;
}

//- (void)textFieldDidBeginEditing:(UITextField *)textField;           // became first responder
//- (BOOL)textFieldShouldEndEditing:(UITextField *)textField;          // return YES to allow editing to stop and to resign first responder status. NO to disallow the editing session to end
//- (void)textFieldDidEndEditing:(UITextField *)textField;

#pragma mark Action

- (BOOL)isValidInput
{
    if ([self.txtViewEmail checkIsValidEmail] == NO) return NO;
    if ([self.txtViewPassword checkIsValidPassword] == NO) return NO;
    if ([self.txtViewConfirmPassword checkIsValidConfirmPasswordWithPassword:self.txtViewPassword.textField.text] == NO) return NO;
    
//    if (self.txtViewEmail.textField.text.length <= 0){
//        
//        [WarningCoverView addWarningToTargetView:self.txtViewEmail warningtype:kInputFieldErrorTypeWrongEmailFormat];
//        
//        return NO;
//    }
//
//    if (self.txtViewPassword.textField.text.length <= 0){
//        
//        [WarningCoverView addWarningToTargetView:self.txtViewPassword warningtype:kInputFieldErrorTypeWrongPassword];
//        
//        return NO;
//    }
//    
//    if (self.txtViewConfirmPassword.textField.text.length <= 0){
//        
//        [WarningCoverView addWarningToTargetView:self.txtViewConfirmPassword warningtype:kInputFieldErrorTypeWrongPasswordConfirm];
//        
//        return NO;
//    }
//    
//    if (self.txtViewCity.textField.text.length <= 0){
//        
////        [WarningCoverView addWarningToTargetView:self.txtViewBirth warningtype:kInputFieldErrorTypeWrongEmail];
//        
//        return NO;
//    }
    
    if (self.btnAgree.selected == NO){
        
//        [ToastController showToastWithMessage:@"약관에 동의 해 주세요" duration:5];
        
        return NO;
    }
    
//    if (self.txtViewBirth.textField.text.length <= 0){
//        
//        [WarningCoverView addWarningToTargetView:self.txtViewEmail warningtype:kInputFieldErrorTypeWrongEmail];
//        
//        return NO;
//    }

    
    return YES;
}

- (IBAction)onCheckTermsOfService:(id)sender {
    
   if(self.btnAgree.selected)
   {
       TermsViewController * vc = (TermsViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"termsofservice"];
       vc.completion = ^(TermsViewController *controller){
           if (controller.isAgree) {
               
           }
           else{
               self.btnAgree.selected = NO;
           }
       };
       
       [self.navigationController pushViewController:vc animated:NO];
   }
    
}

- (IBAction)clickSignIn:(id)sender
{
    [self.view.findFirstResponder resignFirstResponder];
    
    if ([self enableActionWithCurrentNetwork] == NO)return;
    
    if ([self isValidInput]){
        [self doSignUp];
    }
}

#pragma mark Server

- (NSMutableData*)receivedBufferData
{
    if (_receivedData == nil){
        _receivedData = [NSMutableData dataWithCapacity:0];
    }
    
    return _receivedData;
}

- (void)doSignUp
{
    NSString *email = self.txtViewEmail.textField.text;
    NSString *password = self.txtViewPassword.textField.text;
    NSString *city = self.txtViewCity.textField.text;
    NSInteger birthYear = [self.txtViewBirth.textField.text integerValue];
    enum GenderType genType = self.btnCheckMale.selected ? kGenderTypeMale : kGenderTypeFemale;
    
    [self requestSignUpWithEmail:email Password:password BirthYear:birthYear City:city Gender:genType];
}

- (void)didConnectionFail:(NSError *)error
{
    [ToastController showToastWithMessage:error.description duration:kToastMessageDurationType_Auto];
    [self toastNetworkError];
}

- (void)didCompleteRecevedWithResultType:(DoleServerResultType)resultType ParsedData:(NSDictionary *)recevedData ParseError:(NSError *)error
{
    if (recevedData && error==nil && [recevedData[@"Return"] boolValue] ){
        
        if (resultType == kDoleServerResultTypeSignUp){

            //longzhe cui added
            NSString *email = self.txtViewEmail.textField.text;
            NSString *city = self.txtViewCity.textField.text;
            NSInteger birthYear = [self.txtViewBirth.textField.text integerValue];
            enum GenderType genType = self.btnCheckMale.selected ? kGenderTypeMale : kGenderTypeFemale;

            // JP Added
            [[Mixpanel sharedInstance] registerSuperProperties:@{@"User Type": @"Registered"}];
            [[Mixpanel sharedInstance] track:@"Signup Success" properties:@{@"Authentication":@"Email"}];
            [[Mixpanel sharedInstance] registerSuperProperties:@{@"Email": email}];
            
            // JP added post 1.4.2
            [[Mixpanel sharedInstance] createAlias:email
                    forDistinctID:[Mixpanel sharedInstance].distinctId];
            [[Mixpanel sharedInstance] identify:[Mixpanel sharedInstance].distinctId];
            //[[Mixpanel sharedInstance] identify:email];
            [[Mixpanel sharedInstance].people set:@{@"$email":email}];
            [[Mixpanel sharedInstance].people set:@{@"city":city}];
            [[Mixpanel sharedInstance].people set:@{@"birthYear":[NSNumber numberWithInteger:birthYear]}];
            [[Mixpanel sharedInstance].people set:@{@"genType":[NSNumber numberWithInteger:genType]}];
            [[Mixpanel sharedInstance].people set:@{@"Deleted":@"false"}];
            [[Mixpanel sharedInstance].people addPushDeviceToken:((AppDelegate*)([UIApplication sharedApplication].delegate)).userConfig.deviceTokenData];
            //longzhe cui added end
            
            NSInteger userNo = [recevedData[@"UserNo"] integerValue];

            NSString *message = [NSString stringWithFormat:@"회원번호[%d]로 가입하였습니다", userNo, nil];
            
            [self.userConfig signUpByEmail:self.txtViewEmail.textField.text password:self.txtViewPassword.textField.text];

//
//            [ToastController showToastWithMessage:message duration:2 completion:^{
//            //    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
//            }];
            
            [self requestLogInWithUserID:self.userConfig.email Password:self.userConfig.password];
        }
//        else if (resultType == kDoleServerResultTypeGetCityNames){
//            NSArray *citynames = recevedData[@"Provinces"];
//            self.userConfig.cachedCitynames = citynames;
//        }
        else if (resultType == kDoleServerResultTypeLogIn){
//            [ToastController showToastWithMessage:@"로그인 완료했습니다" duration:2 completion:^{
//                [self.navigationController dismissViewControllerAnimated:YES completion:nil];
//            }];
            
            NSNumber *userNo = recevedData[@"UserNo"];
            NSString *authKey = recevedData[@"AuthKey"];
            self.userConfig.authKey = authKey;
            self.userConfig.userNo = [userNo integerValue];
            
            self.userConfig.loginStatus = kUserLogInStatusDoleLogOn;
            
            //[self popToLonOnViewController];
            [self closeWhenLoggedIn];

        }
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"SystemMigrationWarningShown"];
    }
    else{
        
        NSInteger errorCode = [recevedData[@"ReturnCode"] integerValue];
        if ([self queryErrorCode:errorCode EmailView:self.txtViewEmail PasswordView:self.txtViewPassword]) return;
        
        [self toastNetworkErrorWithErrorCode:errorCode];
    }
}

-(void)closeWhenLoggedIn
{
    if (self.appFirstTimeLogIn == NO &&
        self.requireLoginCompletion == nil){
        [self popToLonOnViewController];
        return;
    }
    
    [self.navigationController.presentingViewController dismissViewControllerAnimated:NO completion:nil];
    
    if (self.requireLoginCompletion){
        self.requireLoginCompletion(YES);
    }
}


@end
