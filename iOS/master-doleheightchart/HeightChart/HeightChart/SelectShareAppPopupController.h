//
//  SelectShareAppPopupController.h
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 16..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "PopupController.h"

@interface SelectShareAppPopupController : PopupController<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, copy) void (^CompletionBlock)(BOOL);

@end
