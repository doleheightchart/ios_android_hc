//
//  PaperNewzealandViewController.m
//  HeightChart
//
//  Created by ne on 2014. 3. 28..
//  Copyright (c) 2014년 Kim Sehyun. All rights reserved.
//

#import "PaperNewzealandViewController.h"
#import "TextViewDualPlaceHolder.h"
#import "UIColor+ColorUtil.h"
#import "UIFont+DoleHeightChart.h"
#import "UIView+ImageUtil.h"
#import "UIViewController+DoleHeightChart.h"
#import "ToastController.h"
#import "PaperConfirmPopupController.h"
#import "CityPopupController.h"
#import "UIViewController+PolyServer.h"
#import "WarningCoverView.h"
#import "InputChecker.h"

@interface PaperNewzealandViewController (){
    NSMutableData   *_receivedData;
    InputChecker *_inputChecker;
}

@property (weak, nonatomic) IBOutlet UIImageView *imgTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDescript;
@property (weak, nonatomic) IBOutlet UIImageView *imgPaper;
@property (weak, nonatomic) IBOutlet TextViewDualPlaceHolder *txtName;
@property (weak, nonatomic) IBOutlet TextViewDualPlaceHolder *txtPhone;
@property (weak, nonatomic) IBOutlet TextViewDualPlaceHolder *txtAdress;
@property (weak, nonatomic) IBOutlet TextViewDualPlaceHolder *txtCity;
@property (weak, nonatomic) IBOutlet TextViewDualPlaceHolder *txtPostal;
@property (weak, nonatomic) IBOutlet UIButton *btnRequest;
@property (weak, nonatomic) IBOutlet TextViewDualPlaceHolder *txtEmail;

- (IBAction)clickSend:(id)sender;

@end

@implementation PaperNewzealandViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.lblDescript.font = [UIFont defaultRegularFontWithSize:26/2];
    self.lblDescript.textColor = [UIColor colorWithRGB:0x606060];
    
    [self.txtEmail setDualPlaceHolderTextView:DoleDualPlaceHolderTextTypeEmail]; 
    [self.txtName setDualPlaceHolderTextView:DoleDualPlaceHolderTextTypeRecipientName];
    [self.txtPhone setDualPlaceHolderTextView:DoleDualPlaceHolderTextTypePhone];
    [self.txtAdress setDualPlaceHolderTextView:DoleDualPlaceHolderTextTypeAddress];
    [self.txtCity setDualPlaceHolderTextView:DoleDualPlaceHolderTextTypeNormal];
    [self.txtPostal setDualPlaceHolderTextView:DoleDualPlaceHolderTextTypeNormal];
    
    self.txtPhone.textField.keyboardType = UIKeyboardTypeNumberPad;
    self.txtPostal.textField.keyboardType = UIKeyboardTypeNumberPad;
    
    [self.txtCity.textField setReDrawPlaceHolder:@"City" SecondText:@""];
    [self.txtPostal.textField setReDrawPlaceHolder:@"Postal code" SecondText:@""];
    [self.txtAdress.textField setReDrawPlaceHolder:@"Street Address" SecondTopText:@"including" SecondBottomText:@"mail box number"]; 
    
    [self.btnRequest setResizableImageWithType:DoleButtonImageTypeOrange];
    
    
    [self addCommonCloseButton];
    [self setupControlFlexiblePosition];
    

    [self addKeyboardObserver];
    self.txtCity.textField.delegate = self;
    
    _inputChecker = [[InputChecker alloc]init];
    [_inputChecker addHostViewRecursive:self.view submitButton:self.btnRequest checkBoxButtons:nil];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [self checkNetwork];
}

-(UITextField*)atLeatAdjustHeightControl
{
    return self.txtCity.textField;
}

//-(void)textFieldDidBeginEditing:(UITextField *)textField {
//
//    [self moveSelfViewForEditing];
//}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField        // return NO to disallow editing.
{
    
    
    [self.view.findFirstResponder resignFirstResponder];
    if (textField == self.txtCity.textField){
        [self showCityPopup];
        return YES;
    }

    
    return NO;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
//    if (self.view.frame.origin.y < 0 && [self.view findFirstResponder] == nil){
//        [UIView animateWithDuration:0.2 animations:^{
//            self.view.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height);
//        }];
//    }
}

-(void)moveSelfViewForEditing
{
    CGRect rect =   self.view.frame;
//    rect.origin.y = rect.origin.y - 200;
    
    self.view.frame = rect;
}


-(void)setupControlFlexiblePosition
{

    CGFloat DefalutMargin = 120;
    CGFloat lblMargin = 102;
    CGFloat imgPaperMargin = 234;
    
    if ([UIScreen isFourInchiScreen]) {
        
        [self changeToHeightPixel:234  Control:self.imgPaper];
        [self moveToPositionYPixel:(DefalutMargin + lblMargin + 26) Control:self.imgPaper];
        [self moveToPositionYPixel:(DefalutMargin + lblMargin + 26 + imgPaperMargin + 30) Control:self.txtEmail];
        [self moveToPositionYPixel:(DefalutMargin + lblMargin + 26 + imgPaperMargin + 30+ 72 + 22) Control:self.txtName];
        [self moveToPositionYPixel:(DefalutMargin + lblMargin + 26 + imgPaperMargin + 30 + 72 + 22+ 72 + 22)  Control:self.txtPhone];
        [self moveToPositionYPixel:(DefalutMargin + lblMargin + 26 + imgPaperMargin + 30 + 72 + 22 + 72 + 22+ 72 + 22)  Control:self.txtAdress];
        [self moveToPositionYPixel:(DefalutMargin + lblMargin + 26 + imgPaperMargin + 30 + 72 + 22 + 72 + 22 + 72 + 22 + 72 + 22)  Control:self.txtCity];
        [self moveToPositionYPixel:(DefalutMargin + lblMargin + 26 + imgPaperMargin + 30 + 72 + 22 + 72 + 22 + 72 + 22 + 72 + 22)   Control:self.txtPostal];
        [self moveToPositionYPixel:(DefalutMargin + lblMargin + 26 + imgPaperMargin + 30 + 72 + 22 + 72 + 22 + 72 + 22 + 72 + 22 + 72 + 34) Control:self.btnRequest];

    }
    else{
        
        [self.imgTitle setHidden:YES];
    
        
        DefalutMargin = 84;
        imgPaperMargin = 111;
        
        CGRect rect = self.imgPaper.frame;
        rect.size.height = imgPaperMargin/2;
        self.imgPaper.frame = rect;
        self.imgPaper.image = [UIImage imageNamed:@"height_chart_paper_image_960"];
        
        [self changeToHeightPixel:111  Control:self.imgPaper];
        [self moveToPositionYPixel:DefalutMargin Control:self.lblDescript];
        [self moveToPositionYPixel:(DefalutMargin + lblMargin + 17) Control:self.imgPaper];
        [self moveToPositionYPixel:(DefalutMargin + lblMargin + 17 + imgPaperMargin + 22) Control:self.txtEmail];
        [self moveToPositionYPixel:(DefalutMargin + lblMargin + 17 + imgPaperMargin + 22+ 72 + 22) Control:self.txtName];
        [self moveToPositionYPixel:(DefalutMargin + lblMargin + 17 + imgPaperMargin + 22 + 72 + 22+ 72 + 22)  Control:self.txtPhone];
        [self moveToPositionYPixel:(DefalutMargin + lblMargin + 17 + imgPaperMargin + 22 + 72 + 22 + 72 + 22+ 72 + 22)  Control:self.txtAdress];
        [self moveToPositionYPixel:(DefalutMargin + lblMargin + 17 + imgPaperMargin + 22 + 72 + 22 + 72 + 22 + 72 + 22+ 72 + 22)  Control:self.txtCity];
        [self moveToPositionYPixel:(DefalutMargin + lblMargin + 17 + imgPaperMargin + 22 + 72 + 22 + 72 + 22 + 72 + 22+ 72 + 22)   Control:self.txtPostal];
        [self moveToPositionYPixel:(DefalutMargin + lblMargin + 17 + imgPaperMargin + 22 + 72 + 22 + 72 + 22 + 72 + 22 + 72 + 22+ 72 + 34) Control:self.btnRequest];
    }
    
    
    

}

-(void)showCityPopup
{
//    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Popup" bundle:nil];
//    CityPopupController *cityPopup = [story instantiateViewControllerWithIdentifier:@"City"];
//    
//    cityPopup.citynames = self.userConfig.cachedCitynames;
//    cityPopup.completion = ^(CityPopupController *controller){
//        self.txtCity.textField.text = controller.selectedCityname;
//        [controller releaseModal];
//    };
//    [cityPopup showModalOnTheViewController:nil];
    
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Popup" bundle:nil];
    CityPopupController *cityPopup = [story instantiateViewControllerWithIdentifier:@"City"];
    cityPopup.citynames = self.userConfig.cachedCitynames;
    cityPopup.selectedCityname = self.txtCity.textField.text;
    cityPopup.completion = ^(CityPopupController *controller){
        NSString *text = controller.selectedCityname;
        [self.txtCity setText:text];
        [controller releaseModal];
    };
    [cityPopup showModalOnTheViewController:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)checkNetwork
{
    if (NO == self.isEnableNetwork){
        

//        self.txtName.textField.enabled = NO;
//        self.txtCity.textField.enabled = NO;
//        self.txtAdress.textField.enabled = NO;
//        self.txtPhone.textField.enabled = NO;
//        self.txtPostal.textField.enabled = NO;
//
//        [ToastController showToastWithMessage:NSLocalizedString(@"The network is unstable. \nPlease try again after checking the network", @"") duration:5];

    }
    else{
        if (nil == self.userConfig.cachedCitynames) {
            NSDictionary *englishCountryNamesCities = [self englishCountryNamesAndCities];
            NSDictionary *countryCodesAndEnglishNames = [self countriesCodesAndEnglishCommonNames];
            NSString *countryCode = self.currentCountryCode;
            NSString *englishCountryName = [countryCodesAndEnglishNames objectForKey:countryCode];
            NSArray *sortedArray = [englishCountryNamesCities objectForKey:englishCountryName];// sortedArrayUsingSelector:@selector(compare:)];
            self.userConfig.cachedCitynames = sortedArray;
            //[self requestCitynamesWithCountryCode:self.currentCountryCode];
        }
        
    }
}

- (IBAction)clickSend:(id)sender {
    [self.view.findFirstResponder resignFirstResponder];
    
    if ([self enableActionWithCurrentNetwork] == NO)return;
    
    if ([self isValidInput]){
        
        NSString * strAdress = [NSString stringWithFormat:@"%@, %@, %@, %@, %@", self.txtName.textField.text, self.txtPhone.textField.text,
                                self.txtAdress.textField.text, self.txtCity.textField.text, self.txtPostal.textField.text];
        //팝업을 띄운다.
//        [ToastController showToastWithMessage:@"Request popup을 띄운다." duration:4.0];
        PaperConfirmPopupController *confirm = [[UIStoryboard storyboardWithPopup]
                                                instantiateViewControllerWithIdentifier:@"paperPopup"];
        [confirm popupAddressText:strAdress];
        confirm.completionBlock = ^(BOOL yesno){
            
            if (yesno) {
               
                // check
                [self requestRegisterQuestionWithTopic:@"PaperNewzealand" ReceiveEmail:self.txtEmail.textField.text Question:strAdress];
                
//                [self closeViewControllerAnimated:NO];
            }
            
        };
        [confirm showModalOnTheViewController:nil];
    }
}

- (BOOL)isValidInput
{
//    if (self.txtViewTopic.textField.text.length <= 0){
//        
//        [WarningCoverView addWarningToTargetView:self.txtViewTopic warningtype:kInputFieldErrorTypeWrongPasswordFormat];
//        
//        return NO;
//    }
//    
//    if (self.txtViewEmail.textField.text.length <= 0){
//        
//        [WarningCoverView addWarningToTargetView:self.txtViewEmail warningtype:kInputFieldErrorTypeWrongEmail];
//        
//        return NO;
//    }
//    
//    if (self.txtQuestionView.text.length <= 0){
//        
//        [WarningCoverView addWarningToTargetView:self.txtQuestionView warningtype:kInputFieldErrorTypeWrongPassword];
//        
//        return NO;
//        
//        
//    }

    if (self.txtName.textField.text.length <= 0){
        [WarningCoverView addWarningToTargetView:self.txtName warningtype:kInputFieldErrorTypeWrongContent];
        return NO;
    }

    if (self.txtAdress.textField.text.length <= 0){
        [WarningCoverView addWarningToTargetView:self.txtAdress warningtype:kInputFieldErrorTypeWrongContent];
        return NO;
    }

    if (self.txtCity.textField.text.length <= 0){
        [WarningCoverView addWarningToTargetView:self.txtCity warningtype:kInputFieldErrorTypeWrongContent];
        return NO;
    }

//    if (self.txtPhone.textField.text.length <= 0){
//        [WarningCoverView addWarningToTargetView:self.txtPhone warningtype:kInputFieldErrorTypeWrongContent];
//        return NO;
//    }

    if (self.txtPostal.textField.text.length <= 0){
        [WarningCoverView addWarningToTargetView:self.txtPostal warningtype:kInputFieldErrorTypeWrongContent];
        return NO;
    }

    
    
    return YES;
}

    


#pragma mark Server

- (NSMutableData*)receivedBufferData
{
    if (_receivedData == nil){
        _receivedData = [NSMutableData dataWithCapacity:0];
    }
    
    return _receivedData;
}

- (void)didConnectionFail:(NSError *)error
{
//    [ToastController showToastWithMessage:error.description duration:5];
    [self toastNetworkError];
}

- (void)didCompleteRecevedWithResultType:(DoleServerResultType)resultType ParsedData:(NSDictionary *)recevedData ParseError:(NSError *)error
{
    if (recevedData && error==nil && [recevedData[@"Return"] boolValue] ){
        
        if (resultType == kDoleServerResultTypeRegisterCS){
//            NSInteger userNo = [recevedData[@"UserNo"] integerValue];
//            NSString *message = [NSString stringWithFormat:@"회원번호[%d]로 가입하였습니다", userNo, nil];
//            
//            [self.userConfig signUpByEmail:self.txtViewEmail.textField.text password:self.txtViewPassword.textField.text];
//            
//            [ToastController showToastWithMessage:message duration:5 completion:^{
//                [self.navigationController dismissViewControllerAnimated:YES completion:nil];
//            }];
            self.userConfig.isHeightChartPaparRecieved = YES;
            self.userConfig.isHeightChartPaparRecievedShowBanner = YES;
            
//            [ToastController showToastWithMessage:NSLocalizedString(@"Your inquiry was received.", @"")
//                                         duration:5];
            
            [self closeViewControllerAnimated:YES];
        }
//        else if (resultType == kDoleServerResultTypeGetCityNames){
//            NSArray *citynames = recevedData[@"Provinces"];
//            self.userConfig.cachedCitynames = citynames;
//        }
    }
    else{
        
        NSInteger errorCode = [recevedData[@"ReturnCode"] integerValue];
//        if ([self queryErrorCode:errorCode EmailView:self.txtViewEmail PasswordView:self.txtViewPassword]) return;
        
        [self toastNetworkErrorWithErrorCode:errorCode];
    }
}

@end
