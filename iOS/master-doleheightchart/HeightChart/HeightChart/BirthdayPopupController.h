//
//  BirthdayPopupController.h
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 2. 11..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "PopupController.h"

@interface BirthdayPopupController : PopupController<UICollectionViewDataSource, UICollectionViewDelegate>
@property (nonatomic, copy) NSDate* birthday;
@property (nonatomic, copy) void (^completion)(NSDate* birthday);
@end
