//
//  HeightDetailToolBar.h
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 5..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import <UIKit/UIKit.h>

enum DetailToolBarButtonIndex {
    kDetailToolBarButtonIndexZoomOut,
    kDetailToolBarButtonIndexZoomIn,
    kDetailToolBarButtonIndexDelete,
    kDetailToolBarButtonIndexDeleteMode,
    kDetailToolBarButtonIndexReturn,
    kDetailToolBarButtonIndexGraph,
};

@protocol HeightDetailToolBarDelegate <NSObject>
-(void)clickToolbarWithIndex:(NSUInteger)index;
@end

@interface HeightDetailToolBar : UIControl
@property (nonatomic) BOOL isDeleteMode;
@property (nonatomic) BOOL isZoomMode;
@property (nonatomic) BOOL enableDelete;
@property (nonatomic) BOOL enableDeleteMode;
@property (nonatomic, weak) IBOutlet id<HeightDetailToolBarDelegate> toolBarDelegate;
@end
