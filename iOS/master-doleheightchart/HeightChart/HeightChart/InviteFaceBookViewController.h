//
//  InviteFaceBookViewController.h
//  HeightChart
//
//  Created by ne on 2014. 3. 17..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FacebookManager.h"
@interface InviteFaceBookViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, FacebookManagerInviteDelegate,FacebookManagerDelegate>

- (void) inviteResult:(NSInteger)result;

@end

//@interface InviteFriendInfo : NSObject
//
//@property (nonatomic, retain) NSString *friendName;
//@property (nonatomic, retain) NSString *friendID;
//@property (nonatomic) BOOL ischecked;
//
//@end

@interface InviteFacebookCell : UITableViewCell

@end


