//
//  WarningCoverView.m
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 14..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "WarningCoverView.h"
#import "UIColor+ColorUtil.h"
#import "UIView+ImageUtil.h"
#import "UIFont+DoleHeightChart.h"

#define kTAG_WARNINGVIEW 2848



NSArray *kWarningMessages;

@interface WarningCoverView(){
    UIImageView     *_backgroundView;
    UILabel         *_warningMessageLabel;
}

+(NSArray*)waringMessageList;

@end

@implementation WarningCoverView

+(NSArray*)waringMessageList
{
    if (nil == kWarningMessages){
        kWarningMessages = @[
                     NSLocalizedString(@"The email does not exist or is incorrect", @""),
                     NSLocalizedString(@"The password is incorrect", @""),
                     NSLocalizedString(@"Please enter the full email address, \nincluding information after the @ sign", @""),
                     NSLocalizedString(@"Poly:90102 This email is already in use", @""),
                     NSLocalizedString(@"Please enter a password with \nat least 6 characters.", @""),
                     NSLocalizedString(@"ConfirmPasswordIncorrect", @""), //영문키가 같아서 변경 ConfirmPasswordIncorrect
                     NSLocalizedString(@"The nickname has already been registered", @""),
                     NSLocalizedString(@"Enter the N characters of the code", @""),
                     NSLocalizedString(@"1~15charactor", @""),
                     NSLocalizedString(@"항목을 입력하세요!", @""),
                ];
    }
    
    return kWarningMessages;
}


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self initialize];
    }
    return self;
}

-(void)initialize
{
//    self.backgroundColor = [UIColor redColor];
    
    self.backgroundColor = [UIColor clearColor];
    
    _backgroundView = [[UIImageView alloc]initWithFrame:self.bounds];
    UIImage *backImage = [UIImage resizableImageWithName:@"input_error" CapInsets:UIEdgeInsetsMake(0, (43.0/2.0), 0, (43.0/2.0))];
    _backgroundView.image = backImage;
    [self addSubview:_backgroundView];
    _backgroundView.layer.opacity = 0.8;
    
    _warningMessageLabel = [[UILabel alloc]initWithFrame:self.bounds];
    _warningMessageLabel.backgroundColor = [UIColor clearColor];
    _warningMessageLabel.textAlignment = NSTextAlignmentCenter;
    _warningMessageLabel.font = [UIFont defaultBoldFontWithSize:30/2];
//    _warningMessageLabel.minimumScaleFactor = 0.1;
    _warningMessageLabel.adjustsFontSizeToFitWidth = NO;
//    _warningMessageLabel.minimumScaleFactor
    _warningMessageLabel.numberOfLines = 2;
    _warningMessageLabel.lineBreakMode = NSLineBreakByWordWrapping;
    _warningMessageLabel.textColor = [UIColor colorWithRGB:0xffba1f];
    

    [self addSubview:_warningMessageLabel];
    
//    self.layer.opacity = 0.5;
}

-(UILabel*)messageLabel
{
    return _warningMessageLabel;
}

+(instancetype)addWarningToTargetView:(UIView*)target
                       warningmessage:(NSString*)message
{
   
    UIView *prevWarningView = [target viewWithTag:kTAG_WARNINGVIEW];
    [prevWarningView removeFromSuperview];
    
//    CGRect f = CGRectMake(-1, 0, target.bounds.size.width + 2, target.bounds.size.height);
    
//    WarningCoverView *wc = [[WarningCoverView alloc]initWithFrame:f];
    WarningCoverView *wc = [[WarningCoverView alloc]initWithFrame:target.bounds];
    
    [wc messageLabel].text = message;
    
//    CGSize textSize = [[wc messageLabel].text sizeWithAttributes:@{NSFontAttributeName:[[wc messageLabel] font]}];
    CGSize textSize = [[wc messageLabel].text sizeWithFont:wc.messageLabel.font];

    CGFloat txtOneLineHeight = 34; // 30폰트는 20높이 정도 되지만, 여백과 한글폰트의 사이즈를 모르기때문에 디자인가이드 사이즈인 34로 잡음
    CGFloat txtMaximumWidth = (528 - 28 - 28)/2;
    if(( txtOneLineHeight < textSize.height) || (txtMaximumWidth < textSize.width))
    {
        //두라인이 되거나 최소 여백을 넘는 길이일경우 .. 폰트를 작게 준다.
        [wc messageLabel].font = [UIFont defaultBoldFontWithSize:25/2];
    }
    
    
    
    wc.tag = kTAG_WARNINGVIEW;
    [target addSubview:wc];
    
    return wc;
}

+(instancetype)addWarningToTargetView:(UIView*)target
                          warningtype:(InputFieldErrorType)waringType
{
    NSString *message = [WarningCoverView waringMessageList][waringType];
    return [WarningCoverView addWarningToTargetView:target warningmessage:message];
}

//+(instancetype)addWarningToTargetView:(UIView *)target NetworkErrorCode:(NSInteger)errorCode
//{
//    
//    NSDictionary *errorDescription = @{
//                                       @90001: @"파라미터 오류",
//                                       @90002: @"해당 레코드 없음",
//                                       @90003: @"이력 등록 오류",
//                                       @90101: @"user_id 중복오류",
//                                       @90102: @"email 중복오류",
//                                       @90105: @"사용자데이터 없음",
//                                       @90106: @"비밀번호확인 오류",
//                                       @90108: @"사용자등록실패",
//                                       @90109: @"사용자프로필등록실패",
//                                       @90110: @"사용자등급등록 실패",
//                                       @90111: @"SNS계정 등록 실패",
//                                       @90126: @"user id 길이 오류",
//                                       @90127: @"password 길이 오류",
//                                       @90131: @"동일 password 오류",
//                                       @90205: @"인증키 오류",
//                                       @90211: @"로그인 실패 횟수 초과"
//                                       };
//    
//    
//}



-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self removeFromSuperview];
}

-(void)didChangeValueForKey:(NSString *)key
{
    NSLog(@"Changed View's properties is %@", key);
}

@end

@implementation TextViewDualPlaceHolder (WarningCoverView)

-(BOOL)checkIsValidNickname;
{
    NSString *nickname = self.textField.text;
    
    if (nickname.length == 0) return FALSE;
    
    if (nickname.length < 1 || nickname.length > 15){
        [WarningCoverView addWarningToTargetView:self warningtype:kInputFieldErrorTypeWrongNickname];
        return NO;
    }
    
    if ([nickname rangeOfCharacterFromSet:[[NSCharacterSet alphanumericCharacterSet] invertedSet]].location != NSNotFound) {
        // There are non-alphanumeric characters in the replacement string
        [WarningCoverView addWarningToTargetView:self warningtype:kInputFieldErrorTypeWrongNickname];
        return NO;
    }
    
    return YES;
}

-(BOOL)checkIsValidEmail
{
    NSString *email = self.textField.text;
    
    if (email.length <= 0) {
//        [WarningCoverView addWarningToTargetView:self warningtype:kInputFieldErrorTypeWrongEmail];
//        [WarningCoverView addWarningToTargetView:self warningtype:kInputFieldErrorTypeWrongEmailFormat];

        return FALSE;
    };
    
//    NSRange r = [email rangeOfString:@"@"];
//    
//    if ( r.length <= 0 ){
//        [WarningCoverView addWarningToTargetView:self warningtype:kInputFieldErrorTypeWrongEmailFormat];
//        return FALSE;
//    }
    
    if (FALSE == [self validateEmail:email]){
        [WarningCoverView addWarningToTargetView:self warningtype:kInputFieldErrorTypeWrongEmailFormat];
        return FALSE;
    }

    return TRUE;
}

-(BOOL)checkIsValidPassword
{
    NSString *password = self.textField.text;
    
    if (password.length == 0){
        return NO;
    }
    
    if (password.length < 6) {
        [WarningCoverView addWarningToTargetView:self warningtype:kInputFieldErrorTypeWrongPasswordFormat];
        return NO;
    }
    
    if (self.passwordOriginalView){
        if (NO == [self.textField.text isEqualToString:self.passwordOriginalView.textField.text]){
            [WarningCoverView addWarningToTargetView:self warningtype:kInputFieldErrorTypeWrongPasswordConfirm];
            return NO;
        }
    }
    
    return YES;
}

-(BOOL)checkIsValidConfirmPasswordWithPassword:(NSString*)password
{
    NSString *confirmPwd = self.textField.text;
    
    if (NO == [confirmPwd isEqualToString:password]){
        [WarningCoverView addWarningToTargetView:self warningtype:kInputFieldErrorTypeWrongPasswordConfirm];
        return NO;
    }
    
    return YES;
}

//-(BOOL)checkIsValidDirectQRCodeWithMinimumLength:(NSUInteger)minLength;
//{
//    NSString *code = self.textField.text;
//    
//    if (code.length <= 0){
//        return NO;
//    }
//    
//    if (code.length < minLength){
//        [WarningCoverView addWarningToTargetView:self warningtype:kInputFieldErrorTypeWrongQRCodeFormat];
//        return NO;
//    }
//    
//    return YES;
//}

//추가 QR
-(BOOL)checkIsValidDirectQRCodeWithFixLength:(NSUInteger)length;
{
    NSString *code = self.textField.text;
    
    if (code.length <= 0){
        return NO;
    }
    
    
    
    if (code.length != length){
        [WarningCoverView addWarningToTargetView:self warningtype:kInputFieldErrorTypeWrongQRCodeFormat];
        return NO;
    }
    
    return YES;
}

-(BOOL)checkIsEmptyInput
{
    if (self.textField.text.length <= 0){
//        [WarningCoverView addWarningToTargetView:self warningtype:kInputFieldErrorTypeWrongContent];
        return NO;
    }
    
    return YES;
}

-(BOOL)hasWarningCover
{
    return (nil != [self viewWithTag:kTAG_WARNINGVIEW]);
}


- (BOOL) validateEmail: (NSString *) candidate {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:candidate];
}

@end

@implementation UIViewController (NetworkError)

//NSDictionary *errorDescription = @{
//                                   @90001: @"파라미터 오류",
//                                   @90002: @"해당 레코드 없음",
//                                   @90003: @"이력 등록 오류",
//                                   @90101: @"user_id 중복오류",
//                                   @90102: @"email 중복오류",
//                                   @90105: @"사용자데이터 없음",
//                                   @90106: @"비밀번호확인 오류",
//                                   @90108: @"사용자등록실패",
//                                   @90109: @"사용자프로필등록실패",
//                                   @90110: @"사용자등급등록 실패",
//                                   @90111: @"SNS계정 등록 실패",
//                                   @90126: @"user id 길이 오류",
//                                   @90127: @"password 길이 오류",
//                                   @90131: @"동일 password 오류",
//                                   @90205: @"인증키 오류",
//                                   @90211: @"로그인 실패 횟수 초과"
//                                   };

-(BOOL)queryErrorCode:(NSInteger)errorCode EmailView:(UIView*)emailView PasswordView:(UIView*)passwordView
{
    
    NSDictionary *passwordErrorDic = @{
//                                       @90106: @"비밀번호확인 오류",
//                                       @90127: @"password 길이 오류",
//                                       @90131: @"동일 password 오류",
                                       @90106: NSLocalizedString(@"The password is incorrect", @"") ,
                                       @90127: NSLocalizedString(@"The password is incorrect", @"") ,
                                       @90131: NSLocalizedString(@"The password is incorrect", @"") ,
                                       
                                       };
    
    NSDictionary *useridErrorDic = @{
//                                       @90101: @"user_id 중복오류",
//                                       @90102: @"email 중복오류",
//                                       @90126: @"user id 길이 오류",
                                     
                                        @90101: @"user_id Error",
//                                        @90102: @"email Error",
                                        @90102: NSLocalizedString(@"Poly:90102 This email is already in use", @""),
                                        @90126: @"user id Error",
                                        //Add 2014.06.26 Facebook ID
                                        @90113: NSLocalizedString(@"This account is signed up with Facebook", @""),
                                        @90105: NSLocalizedString(@"The email does not exist or is incorrect", @""),
//                                       @90114: @"사용중인 SNS 계정임",
                                        @90114: NSLocalizedString(@"This account is signed up with Facebook.", @"") ,
                                       };
    
    NSNumber *errorKey = [NSNumber numberWithInteger:errorCode];
    
    NSString *errorStr = [passwordErrorDic objectForKey:errorKey];
    
    if (errorStr){
        
        [WarningCoverView addWarningToTargetView:passwordView warningmessage:errorStr];
        
        return YES;
    }
    
    errorStr = [useridErrorDic objectForKey:errorKey];
    
    if (errorStr){

        [WarningCoverView addWarningToTargetView:emailView warningmessage:errorStr];
        
        return YES;
    }
    
    return NO;
}


@end
