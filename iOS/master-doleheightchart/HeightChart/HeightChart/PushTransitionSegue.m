//
//  PushTransitionSegue.m
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 21..
//  Copyright (c) 2014년 Kim Sehyun. All rights reserved.
//

#import "PushTransitionSegue.h"
#import "UIViewController+DoleHeightChart.h"

@implementation PushTransitionSegue


- (id)init
{
	self = [super init];
	if (self)
	{
		
	}
	
	return self;
}

- (id)initWithIdentifier:(NSString *)identifier source:(UIViewController *)source destination:(UIViewController *)destination
{
	self = [super initWithIdentifier:identifier source:source destination:destination];
	if (self)
	{
	}
	
	return self;
}

- (void)perform
{
	//[self.sourceViewController presentViewController:self.destinationViewController foldStyle:[self style] completion:nil];
    UIViewController *srcVC = self.sourceViewController;
    UIViewController *dstVC = self.destinationViewController;
    
    [srcVC pushTransitionController:dstVC Animated:YES];
}


@end
