//
//  HeightDetailViewController.m
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 3..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "HeightDetailViewController.h"
#import "UIView+ImageUtil.h"
#import "KidHeightDetail.h"
#import "DetailCloudBackgroundView.h"
#import "HeightDetailToolBar.h"
#import "QRScanViewController.h"
#import "InputHeightViewController.h"
#import "HeightSummaryViewController.h"
#import "UIViewController+DoleHeightChart.h"
#import "CameraOverlayViewController.h"
#import "ChildHeight.h"
#import "HeightViewerController.h"
#import "KidHeightDetail.h"
#import "NicknameViewController.h"
#import "ToastController.h"
#import "HeightSaveContext.h"
#import "DoleTransition.h"
#import "GuideView.h"

#import "NSObject+HeightDatabase.h"
#import "AverageHeightInfo.h"
#import "NSDate+DoleHeightChart.h"
#import "UIView+Animation.h"

#import "MessageBoxController.h"
#import "LogInViewController.h"



@interface HeightDetailViewController (){
    NSMutableDictionary     *_dataDic;
    NSMutableArray          *_dataArray;
    UIViewController        *_summary;
    UIButton                *_closeButton;
    UIImageView             *_tagImageView;
    
    CGFloat                 _aveHeight;
}
@property (weak, nonatomic) IBOutlet HeightDetailTree *heightDetailTreeView;
@property (weak, nonatomic) IBOutlet DetailCloudBackgroundView *cloudbackgroundView;
@property (weak, nonatomic) IBOutlet HeightDetailToolBar *bottomToolbar;

@end

@implementation HeightDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.heightDetailTreeView initialize];
    _closeButton = [self addCommonCloseButton];
    
    _dataDic = [NSMutableDictionary dictionary];
    _dataArray = [NSMutableArray array];
    
    self.heightDetailTreeView.dataSource = self;
    self.heightDetailTreeView.treeDelegate = self;
    self.heightDetailTreeView.monkeyTypeNo = [self.myChild.charterNo shortValue];
//    self.fetchResultController.delegate = self;
    
//    self.heightDetailTreeView.maxHeight = [self.myChild lastTakenHeight];
    self.heightDetailTreeView.maxHeight = [self.myChild maxTakenHeight];
    

    [self reloadKidHeightDetails];
    [self.heightDetailTreeView reloadDetails];
    [self.heightDetailTreeView createDoleKey];

    
    [self addCheerUpAndhealtyTag];
    [self updateTagByAveHeight];
    [self.cloudbackgroundView loadAnimals];
}



-(void)reloadKidHeightDetails
{
    [_dataDic removeAllObjects];
    [_dataArray removeAllObjects];
    
    NSArray *heightArray = [self.myChild childHeightByHeight];
    for (KidHeightDetail *khd in heightArray) {
        NSNumber *heightKey = [NSNumber numberWithFloat:(int)khd.representDetail.kidHeight]; //소수점이하 버림
        [_dataDic setObject:khd forKey:heightKey];
        
        [_dataArray addObjectsFromArray:khd.takenHeightHistory];
    }
    
    [_dataArray sortUsingComparator:^NSComparisonResult(HeightDetailInfo* obj1, HeightDetailInfo* obj2) {
        
        //키로 정렬
        /*
        NSNumber *height1 = [NSNumber numberWithFloat:obj1.kidHeight];
        NSNumber *height2 = [NSNumber numberWithFloat:obj2.kidHeight];
        return [height1 compare:height2];
        */
        
        //기록된 날짜로 정렬
        NSDate *date1 = obj1.takenDate;
        NSDate *date2 = obj2.takenDate;
        
        return [date1 compare:date2];
    }];
}

//뷰디드로드에서 애니메이션을 시작하면 보통은 시작되지만 그전에 코코스2디가 애니메이션을 중지하는 영향을 받을 수 있기에
//뷰디드로드보단 뷰디드어피어 정도에서 시작하면 된다.
//(주의)할것!!



//-(void)addCheerUpTag
//{
//    UIImageView *tagImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 150/2, 150/2)];
//    tagImageView.image = [UIImage imageNamed:@"cheer_up_tag"];
//    [self.view addSubview:tagImageView];
//}
//
//-(void)addhealtyTag
//{
//    UIImageView *tagImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 150/2, 150/2)];
//    tagImageView.image = [UIImage imageNamed:@"healthy_tag"];
//    [self.view addSubview:tagImageView];
//}

-(void)addCheerUpAndhealtyTag
{
    _tagImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 150/2, 150/2)];
//    tagImageView.image = [UIImage imageNamed:@"healthy_tag"];
    [self.view addSubview:_tagImageView];
}
-(void)updateTagByAveHeight
{
    [self.myChild loadLastTakenHeight];
    [self.myChild refreshMaxTakenHeight];
    
    
    
    _aveHeight = [AverageHeightInfo averageHeightByBirthday:self.myChild.birthday IsGril:[self.myChild.isGirl boolValue]];
    
    if (self.myChild.heightHistory.count <= 0) {
        [_tagImageView setHidden:YES];
    }
    else{
        [_tagImageView setHidden:NO];
    }
    
//    if (self.myChild.lastTakenHeight > _aveHeight){
    if (self.myChild.maxTakenHeight > _aveHeight){
        _tagImageView.image = [UIImage imageNamed:NSLocalizedString(@"healthy_tag", @"")];
        [_tagImageView animateScaleEffect];
    }
    else{
        [UIView transitionWithView:_tagImageView duration:0.5 options:(UIViewAnimationOptionTransitionCrossDissolve) animations:^{
            _tagImageView.image = [UIImage imageNamed:NSLocalizedString(@"cheer_up_tag", @"")];
        } completion:^(BOOL finished) {
        }];
    }
    
    self.heightDetailTreeView.aveHeight = _aveHeight;
    self.bottomToolbar.enableDeleteMode = self.myChild.heightHistory.count > 0;
    
    NSInteger age = [self.myChild currentAge];
    if (age <= 0){
        _tagImageView.hidden = YES;
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    GuideView *gv = [GuideView addGuideTarget:self.view Type:kGuideTypeDetail checkFirstTime:YES];
    
    if (gv){
        gv.completion = ^{
            [self.cloudbackgroundView startScrolling];
            [self.cloudbackgroundView startMovingAnimation];
        };
    }
    else{
        [self.cloudbackgroundView startScrolling];
        [self.cloudbackgroundView startMovingAnimation];
    }
}

-(void)viewDidDisappear:(BOOL)animated
{
    //[self.cloudbackgroundView.layer removeAllAnimations];
    [self.cloudbackgroundView stopAll];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark HeightDetailTreeDataSource

- (KidHeightDetail*) heightDetailOfKidHeight:(CGFloat)kidHeight
{
    NSNumber *heightKey = [NSNumber numberWithFloat:(int)kidHeight]; //소수점이하는 버린다.
    
    return [_dataDic objectForKey:heightKey];
}

- (int)characterNumber
{
    return [self.myChild.charterNo shortValue];
}

-(MyChild*)myChildOfDetails
{
    return self.myChild;
}

- (NSString*)nickname
{
    return self.myChild.nickname;
}


#pragma mark ToolBar Delegate

-(void)clickToolbarWithIndex:(NSUInteger)index
{
    switch (index) {
        case kDetailToolBarButtonIndexZoomIn:
        case kDetailToolBarButtonIndexZoomOut:
            self.heightDetailTreeView.isZoomMode = !self.heightDetailTreeView.isZoomMode;
            self.bottomToolbar.isZoomMode = !self.bottomToolbar.isZoomMode;
            break;
        case kDetailToolBarButtonIndexDelete:{
            [self askDeleteCheckedDetails];
        }break;
        case kDetailToolBarButtonIndexReturn:[self changeUIWithDeleteMode:NO];
            break;
        case kDetailToolBarButtonIndexGraph:[self showSummary];
            break;
        case kDetailToolBarButtonIndexDeleteMode:[self changeUIWithDeleteMode:YES];
            break;
    }
}

-(void)changeUIWithDeleteMode:(BOOL)isDeleteMode
{
    self.heightDetailTreeView.isDeleteMode = isDeleteMode;
    self.bottomToolbar.isDeleteMode = isDeleteMode;
    self.cloudbackgroundView.isDeleteMode = isDeleteMode;
    
    _closeButton.layer.opacity = isDeleteMode ? 0 : 1;
    
    if (NO == isDeleteMode){
        [self doUncheckAllDetails];
        [self.heightDetailTreeView reloadDetails];
    }
    else{
        [ToastController showToastWithMessage: NSLocalizedString(@"Please select a record to delete", @"") duration:kToastMessageDurationType_Auto];
    }
}

-(void)askDeleteCheckedDetails
{
//    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Popup" bundle:nil];
//    ConfirmPopupController *confirm = [story instantiateViewControllerWithIdentifier:@"confirmPopup"];
//   
//    confirm.completionBlock = ^(BOOL isConfirm){
//        if (isConfirm){
//            [wvc deleteCheckedDetails];
//        }
//    };
//    [confirm popupTextUp:NSLocalizedString(@"The chosen record will be deleted.", @"")
//              TextBottom:NSLocalizedString(@"It cannot be restored after deletion.", @"")];
//    
//    [confirm showModalOnTheViewController:nil];
    __weak HeightDetailViewController *wvc = self;
    [self showOkCancelConfirmMessage:NSLocalizedString(@"The chosen record will be deleted.\nIt cannot be restored after deletion.",
                                                       @"")completion:^(BOOL isOK) {
        
        if (isOK){
            [wvc deleteCheckedDetails];
        }
        
        
    }];
}
-(void)deleteCheckedDetails
{
    NSMutableSet *checkedAllChildheights = [NSMutableSet set];
    NSMutableArray *deletedDetailArray = [NSMutableArray array];
    for (NSInteger i = _dataArray.count - 1; i >= 0; i--){
        HeightDetailInfo *di = _dataArray[i];
        if (di.isChecked){
            [checkedAllChildheights addObject:di.childHeight];
            [deletedDetailArray addObject:di];
            [_dataArray removeObjectAtIndex:i];
        }
    }
    
    NSManagedObjectContext *context = self.usedObjectContext;
    [self.myChild removeHeightHistory:checkedAllChildheights];
    
    NSError *error;
    
    if ([context save:&error]){
        NSLog(@"Saved ....");
        
        //[self reloadKidHeightDetails];
        //[self.heightDetailTreeView reloadDetails];
        //[self changeUIWithDeleteMode:NO];
        
        
        //[ToastController showToastWithMessage:@"삭제되었습니다" duration:5];
        
        [self deleteHeightCellWithChildHeightArray:deletedDetailArray];

        [self updateTagByAveHeight];
    }
    else{
        NSLog(@"%@", error);
    }
    
//    [self changeUIWithDeleteMode:NO];
}

-(void)doUncheckAllDetails
{
    for (HeightDetailInfo *di in _dataArray) {
        di.isChecked = NO;
    }
}

-(void)showSummary
{
//    if (self.userConfig.accountStatus == kNotSingUp){
//
//        UIStoryboard *story = [UIStoryboard storyboardWithName:@"LogIn" bundle:nil];
//        LogInViewController *vc = [story instantiateViewControllerWithIdentifier:@"LogIn"];
//        
//        UINavigationController *navi = [[UINavigationController alloc]initWithRootViewController:vc];
//        navi.navigationBarHidden = YES;
//        [self modalTransitionController:navi Animated:YES];
//        
//        return;
//
//    }
//    
//    HeightSummaryViewController *summaryVC = [self.storyboard instantiateViewControllerWithIdentifier:@"Summary"];
//    summaryVC.myChild = self.myChild;
//    summaryVC.heightDetailDataSource = self;
//    [self presentViewController:summaryVC animated:NO completion:nil];
    
    if (self.userConfig.accountStatus == kNotSingUp){
        __weak HeightDetailViewController *weakSelf = self;
        [self presentLogInViewControllerOKCancelWithCompletion:^(BOOL isLoggedIn) {
            if (isLoggedIn){
                [weakSelf showSummaryReal];
            }
        }];
    }
    else{
        [self showSummaryReal];
    }
}

-(void)showSummaryReal
{
    HeightSummaryViewController *summaryVC = [self.storyboard instantiateViewControllerWithIdentifier:@"Summary"];
    summaryVC.myChild = self.myChild;
    summaryVC.heightDetailDataSource = self;
    [self presentViewController:summaryVC animated:NO completion:nil];
}

-(void)addHeight
{
    HeightSaveContext *context = [[HeightSaveContext alloc]init];
    context.stickerType = DoleStikerLion; //DefaultType
    context.saveTargetChild = self.myChild;
    
    QRScanViewController *scanViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ScanQRCode"];
    
    scanViewController.heightSaveDelegate = self;
    scanViewController.heightSaveContext = context;
    
    [self pushTransitionController:scanViewController Animated:YES];
}

-(void)showInputHeightWithContext:(HeightSaveContext*)context
{
    __weak HeightDetailViewController *thisController = self;
    InputHeightViewController *inputHeightController = [self.storyboard instantiateViewControllerWithIdentifier:@"InputHeight"];
    inputHeightController.completion = ^(CGFloat kidHeight){
        NSLog(@"InputHeight is %.1f", kidHeight);
        
        context.inputHeight = kidHeight;
        
        [thisController takePictureWithContext:context];
        
        
    };
    
    //[self presentViewController:inputHeightController animated:YES completion:nil];
    [self pushTransitionController:inputHeightController Animated:YES];
}

-(void)takePictureWithContext:(HeightSaveContext*)context
{
    //Camera Stroryboard Load
    UIStoryboard *cameraBoard = [UIStoryboard storyboardWithName:@"camera" bundle:nil];
    CameraOverlayViewController *vcb = [cameraBoard instantiateInitialViewController];
    __weak HeightDetailViewController *thisController = self;
    vcb.completion =^(BOOL isSaved, CameraOverlayViewController *vc){
        
        if (isSaved){
            UIImage *image = vc.imageSaveView.image;
            context.photo = image;
            
            //[vc closeViewControllerAnimated:YES];
            
            [thisController saveDetailHeightWithContext:context];
        }
        else{
//            [thisController.navigationController popToRootViewControllerAnimated:YES];
            
            [vc closeViewControllerAnimated:YES];
        }
    };
    
    //[self presentViewController:vcb animated:YES completion:nil];
    [self pushTransitionController:vcb Animated:YES];
}

-(void)saveDetailHeightWithContext:(HeightSaveContext*)saveContext
{
    NSManagedObjectContext *context = self.usedObjectContext;
    
    //새로등록하는 QRCode는 그냥 추가한다.
    //기존의 QRCode는
    //같은 닉네임 안에서 추가는 변경으로 처리한다.
    //다른 닉네임의 처리는 추가와 삭제로 처리한다.
    
    ChildHeight *newObj;
    BOOL modified = NO;
    HeightDetailInfo *reuseDetail;
    
    if (NO == saveContext.reuseQRCode || saveContext.freeQRCode){ //새로등록하는 코드
        
        newObj = [NSEntityDescription insertNewObjectForEntityForName:@"ChildHeight"
                                                            inManagedObjectContext:context];
        newObj.height = [NSNumber numberWithFloat:saveContext.inputHeight];
        newObj.takenDate = [NSDate dateWithTimeIntervalSinceNow:0];
        newObj.thumbphoto = [saveContext.photo facecropImage];
        newObj.photo = saveContext.photo;
        newObj.qrcode = saveContext.qrCode;
        newObj.stickerType = [NSNumber numberWithInt:saveContext.stickerType];
        newObj.createdDate = [NSDate dateWithTimeIntervalSinceNow:0];
        [self.myChild addHeightHistoryObject:newObj];
    }
    else if (saveContext.child == self.myChild){ //재사용코드
        
        
        ChildHeight *modifyHeight = saveContext.reuseHeight;
        for (HeightDetailInfo *di in _dataArray) {
            if (di.childHeight == modifyHeight){
                reuseDetail = di;
                break;
            }
        }
        
        modifyHeight.takenDate = [NSDate date];
        modifyHeight.thumbphoto = [saveContext.photo facecropImage];
        modifyHeight.photo = saveContext.photo;
        modifyHeight.stickerType = [NSNumber numberWithShort:saveContext.stickerType];
        modifyHeight.height = [NSNumber numberWithFloat:saveContext.inputHeight];
        
        modified = YES;
        
    }
    else{
        newObj = [NSEntityDescription insertNewObjectForEntityForName:@"ChildHeight"
                                                            inManagedObjectContext:context];
        newObj.height = [NSNumber numberWithFloat:saveContext.inputHeight];
        newObj.takenDate = [NSDate dateWithTimeIntervalSinceNow:0];
        newObj.thumbphoto = [saveContext.photo facecropImage];
        newObj.photo = saveContext.photo;
        newObj.qrcode = saveContext.qrCode;
        newObj.createdDate = saveContext.reuseHeight.createdDate;
        newObj.stickerType = saveContext.reuseHeight.stickerType;

        [self.myChild addHeightHistoryObject:newObj];
        
        [saveContext.child removeHeightHistoryObject:saveContext.reuseHeight];
    }
    
    NSLog(@"Save Detail height[ %f ] / qrcode[ %@ ] /", saveContext.inputHeight, saveContext.qrCode);
    
    NSError *error;
    
    if ([context save:&error]){
        NSLog(@"Saved ....");
        
        if (saveContext.freeQRCode){
            [self.userConfig takenFreeQRCode];
        }
        
        [self updateTagByAveHeight];
        HeightDetailInfo *newDetailInfo;

        if (newObj){
            newDetailInfo = [self addNewHeightDetailWith:newObj];
        }
        else if (modified && reuseDetail){
            [self deleteDetail:reuseDetail];
            newDetailInfo = [self addNewHeightDetailWith:saveContext.reuseHeight];
        }

        
//        [self showHeightViwerWithDetailIndex:[_dataArray indexOfObject:newDetailInfo]];
        [self showHeightViwerConfirmModeWithDetailIndex:[_dataArray indexOfObject:newDetailInfo]];
    }
    else{
        NSLog(@"%@", error);
    }

}

-(HeightDetailInfo*)addNewHeightDetailWith:(ChildHeight*)newHeightDetail
{
//    NSNumber *key = newHeightDetail.height;
    NSNumber *key = [NSNumber numberWithFloat:(int)[newHeightDetail.height floatValue]]; //소수점 버림
    
    HeightDetailInfo *di = [[HeightDetailInfo alloc]initWithChildHeight:newHeightDetail];
    
    KidHeightDetail *group = [_dataDic objectForKey:key];
    if (group == nil){
        group = [[KidHeightDetail alloc] init];
        [group.takenHeightHistory addObject:di];
        [_dataDic setObject:group forKey:key];
    }
    else{
        [group.takenHeightHistory addObject:di];
    }
    
    group.expanded = group.takenHeightHistory.count > 1;
    
    NSInteger curKidHeightIndex = [self currentKidHeight];
    
//    BOOL needReloadData = (di.kidHeight >= [self currentKidHeight]);

    [_dataArray addObject:di];
    
//    [_dataArray sortUsingComparator:^NSComparisonResult(HeightDetailInfo* obj1, HeightDetailInfo* obj2) {
//        NSNumber *height1 = [NSNumber numberWithFloat:obj1.kidHeight];
//        NSNumber *height2 = [NSNumber numberWithFloat:obj2.kidHeight];
//        return [height1 compare:height2];
//    }];
    
    [_dataArray sortUsingComparator:^NSComparisonResult(HeightDetailInfo* obj1, HeightDetailInfo* obj2) {
        
        //키로 정렬
        /*
         NSNumber *height1 = [NSNumber numberWithFloat:obj1.kidHeight];
         NSNumber *height2 = [NSNumber numberWithFloat:obj2.kidHeight];
         return [height1 compare:height2];
         */
        
        //기록된 날짜로 정렬
        NSDate *date1 = obj1.takenDate;
        NSDate *date2 = obj2.takenDate;
        
        return [date1 compare:date2];
    }];
    
    
    _heightDetailTreeView.lastAddedDetailInfo = di;
    return di; //TEST
    
//    if (needReloadData){
        [_heightDetailTreeView reloadDetails];
//    }
    
    
//    [_heightDetailTreeView addNewHeightDetailWith:group DetailInfo:di];
    if (NO == _heightDetailTreeView.isZoomMode){
//        [_heightDetailTreeView scrollToNewHeightDetailWith:curKidHeightIndex DetailInfo:di];
        [_heightDetailTreeView scrollToNewHeightDetailWith:[key floatValue] DetailInfo:di];

    }
 
    return di;
}

-(void)deleteDetail:(HeightDetailInfo*)fromDetailInfo
{
    //    NSNumber *key = newHeightDetail.height;
    NSNumber *key = [NSNumber numberWithFloat:(int)fromDetailInfo.kidHeight]; //소수점 버림
    
//    HeightDetailInfo *di = [[HeightDetailInfo alloc]initWithChildHeight:newHeightDetail];
    
    KidHeightDetail *group = [_dataDic objectForKey:key];
    assert(group);
    [group.takenHeightHistory removeObject:fromDetailInfo];
    
    if (group.takenHeightHistory.count == 0){
        [_dataDic removeObjectForKey:key];
    }
    
    [_dataArray removeObject:fromDetailInfo];
    
    
}

-(void)deleteHeightCellWithChildHeightArray:(NSArray*)childHeightArray
{
    [UIView animateWithDuration:2
                     animations:^{
                        [_heightDetailTreeView doDeleteAnimationWithCheckedDetails:childHeightArray];
                     }
                     completion:^(BOOL finished){
//                         [self reloadKidHeightDetails];
//                         [self.heightDetailTreeView reloadDetails];
//                         [self changeUIWithDeleteMode:NO];
//                         [ToastController showToastWithMessage:@"삭제되었습니다" duration:5];
                     }];
    
    [NSTimer scheduledTimerWithTimeInterval:2.0
                                     target:self
                                   selector:@selector(didDeleteHeightCell:)
                                   userInfo:nil
                                    repeats:NO];
}

-(void)didDeleteHeightCell:(NSTimer*)timer
{
    [timer invalidate];
    
    [self reloadKidHeightDetails];
    [self.heightDetailTreeView reloadDetails];
    [self changeUIWithDeleteMode:NO];
//    [ToastController showToastWithMessage:@"It was deleted." duration:5];
    [ToastController showToastWithMessage: NSLocalizedString(@"Deleted.", @"") duration:kToastMessageDurationType_Auto];
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
}

#pragma mark HeightDetailTree Delegate

//- (void)clickTopArrowSticker:(KidHeightDetail*)kidHeightDetail
//{
//    NSInteger index = [_dataArray indexOfObject:kidHeightDetail.representDetail];
//    
//    [self showHeightViwerWithDetailIndex:(index)];
//}

-(void)longpressedWithHeight:(CGFloat)kidHeight;
{
    [self changeUIWithDeleteMode:YES];
    
    _bottomToolbar.enableDelete = YES;
}

- (NSUInteger)currentKidHeight
{
    //return self.myChild.lastTakenHeight;
    
    //return [_dataArray.lastObject kidHeight] + 1;
    
    return self.myChild.maxTakenHeight+1;
}

-(void)clickMonkey
{
    if (YES == self.heightDetailTreeView.isDeleteMode) return;
    if (YES == self.heightDetailTreeView.isZoomMode) return;
    
    NicknameViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"AddNickname"];
    
    vc.inputNickName = self.myChild.nickname;
    vc.inputBirthday = self.myChild.birthday;
    vc.characterNo = [self.myChild.charterNo shortValue];
    vc.isGirl = [self.myChild.isGirl boolValue];
    
    vc.saveCompletion = ^(NicknameViewController *controller){
        BOOL changed = NO;
        
        if (NO == [self.myChild.nickname isEqualToString:controller.inputNickName]){
            self.myChild.nickname = controller.inputNickName;
            changed = YES;
        }
        if (self.myChild.charterNo.integerValue != (controller.characterNo+1)){
            self.myChild.charterNo = [NSNumber numberWithShort:controller.characterNo+1];
            changed = YES;
        }
        if ( self.myChild.isGirl.boolValue != controller.isGirl){
            self.myChild.isGirl = [NSNumber numberWithBool:controller.isGirl];
            changed = YES;
        }
        if (NO == [self.myChild.birthday isEqualToDate:controller.inputBirthday]){
            self.myChild.birthday = controller.inputBirthday;
            changed = YES;
        }
        
        if (changed){
            [self saveContext];
            self.modifiedMonkey = YES;
        }
        
        if (NO == changed){
            [controller.presentingViewController dismissViewControllerWithFoldStyle:0 completion:nil];
        }
        else{
            [controller.presentingViewController dismissViewControllerWithFoldStyle:0 completion:^(BOOL finished){
                
                [UIView transitionWithView:self.heightDetailTreeView
                                  duration:0.3
                                   options:(UIViewAnimationOptionTransitionCrossDissolve)
                                animations:^{
                                    [self updateTagByAveHeight];
                                    [self.heightDetailTreeView createDoleKey];
                                    [self.heightDetailTreeView reloadDetails];
                                    
                                    
                                } completion:nil];
            }];

        }
    };

    [self modalTransitionController:vc Animated:YES];
}

-(void)clickSticker:(KidHeightDetail*)kidHeightDetail stickerDetail:(HeightDetailInfo*)detailInfo
{
    if (YES == self.heightDetailTreeView.isDeleteMode){
        
        BOOL foundCheckedSticker = NO;
        for (HeightDetailInfo *info in _dataArray) {
            if (info.isChecked){
                foundCheckedSticker = YES;
                break;
            }
        }
        _bottomToolbar.enableDelete = foundCheckedSticker;
    }
    else{
        
        
//        self.heightDetailTreeView.isZoomMode = !self.heightDetailTreeView.isZoomMode;
//        self.bottomToolbar.isZoomMode = !self.bottomToolbar.isZoomMode;

        if (NO == self.heightDetailTreeView.isZoomMode){
            NSInteger index = [_dataArray indexOfObject:detailInfo];
            [self showHeightViwerWithDetailIndex:(index)];
        }
        else if (detailInfo != nil){
            self.heightDetailTreeView.isZoomMode = NO;
            self.bottomToolbar.isZoomMode = NO;
        }
    }
}

-(void)showHeightViwerConfirmModeWithDetailIndex:(NSUInteger)index
{
    HeightViewerController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"HeightViewer"];
    vc.datasource = self;
    vc.hightViewerDelegate = self;
    vc.isConfirmMode = YES;
    vc.startIndex = index;
    vc.mychild = self.myChild;
    

    vc.completion =^(BOOL isConfirm, HeightViewerController *vcc){

        //[vcc closeViewControllerAnimated:NO];
        [vcc.navigationController popToRootViewControllerWithFoldStyle:0];
        
        if (isConfirm) {
            // 이곳에 처리 한다.
            [_heightDetailTreeView reloadDetails];
            if (NO == _heightDetailTreeView.isZoomMode && _heightDetailTreeView.lastAddedDetailInfo){
                [_heightDetailTreeView scrollToNewHeightDetailWith:[self currentKidHeight] DetailInfo:_heightDetailTreeView.lastAddedDetailInfo];
            }
        }

    };

   //[self modalTransitionController:vc Animated:YES];
    [self pushTransitionController:vc Animated:YES];
}


-(void)showHeightViwerWithDetailIndex:(NSUInteger)index
{
    HeightViewerController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"HeightViewer"];
    vc.datasource = self;
    vc.hightViewerDelegate = self;
    vc.startIndex = index;
    vc.mychild = self.myChild;

    //[self presentViewController:vc animated:YES completion:nil];
    [self modalTransitionController:vc Animated:YES];
}

-(void)showHeightViewerSaveModeWithDetailIndex:(NSUInteger)index
{
    HeightViewerController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"HeightViewer"];
    vc.datasource = self;
    vc.hightViewerDelegate = self;
    vc.startIndex = index;
    vc.mychild = self.myChild;


    //[self presentViewController:vc animated:YES completion:nil];
    [self modalTransitionController:vc Animated:YES];
}

-(NSUInteger)hightImageCount
{
    return self.myChild.heightHistory.count;
}


-(NSUInteger)countOfHeightDetail
{
    return _dataArray.count;
}
-(HeightDetailInfo*)heightDetailAtIndex:(NSUInteger)index
{
    return _dataArray[index];
}

-(void)didPinchZoom:(BOOL)isZoomMode
{
    self.heightDetailTreeView.isZoomMode = isZoomMode;
    self.bottomToolbar.isZoomMode = isZoomMode;
}

-(void)beginScroll
{
    
}

#pragma mark HeightSaveDelegate

-(void)didScanQRCode:(HeightSaveContext*)context
{
    InputHeightViewController *inputHeightController = [self.storyboard instantiateViewControllerWithIdentifier:@"InputHeight"];
    inputHeightController.heightSaveContext = context;
    inputHeightController.heightSaveDelegate = self;
    [self pushTransitionController:inputHeightController Animated:YES];
}
-(void)didInputHeight:(HeightSaveContext*)context
{
    UIStoryboard *cameraBoard = [UIStoryboard storyboardWithName:@"camera" bundle:nil];
    CameraOverlayViewController *vcb = [cameraBoard instantiateInitialViewController];
    vcb.heightSaveContext = context;
    vcb.heightSaveDelegate = self;
    [self pushTransitionController:vcb Animated:YES];
}
-(void)didTakenPhoto:(HeightSaveContext*)context
{
    //[self.navigationController popToRootViewControllerAnimated:YES];
    
    [self saveDetailHeightWithContext:context];
}
-(void)saveContext:(HeightSaveContext*)context
{
    
}
-(void)cancelSave
{
    
}

-(BOOL)alreadyTakenQRCode:(NSString*)qrCode
{
    for (MyChild *child in self.fetchResultController.fetchedObjects) {
        for (ChildHeight *ch in child.heightHistory) {
            if ([ch.qrcode isEqualToString:qrCode]){
                return YES;
            }
        }
    }
    
    return NO;
}

-(BOOL)canTakenQRCode:(NSString*)qrCode Context:(HeightSaveContext*)context
{
    for (MyChild *child in self.fetchResultController.fetchedObjects) {
        for (ChildHeight *ch in child.heightHistory) {
            if ([ch.qrcode isEqualToString:qrCode]){
                
                if ([ch.createdDate getHoursFromNow] > -24){
                    
                    context.child = child;
                    context.reuseHeight = ch;
                    context.stickerType = [ch.stickerType shortValue];
                    
                    return YES;
                }
                else{
                    return NO;
                }
            }
        }
    }
    
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

-(void)closeViewControllerAnimated:(BOOL)animated
{
    MyChild *myChild = self.myChild;
    [self.navigationController.presentingViewController dismissViewControllerWithFoldStyle:0 completion:^(BOOL finished){
        self.completion(myChild);
    }];
}

#pragma mark HeightViewerDelgate

-(void)deleteHeightViewerWithIndex:(NSInteger)index HeightViewer:(HeightViewerController*)controller
{
    HeightDetailInfo *di = _dataArray[index];

    NSNumber *key = [NSNumber numberWithFloat:(int)di.kidHeight];
    
    KidHeightDetail *group = _dataDic[key];
    
    assert(group);
    
    ChildHeight *deleteHeight = di.childHeight;
    
    [self.myChild removeHeightHistoryObject:deleteHeight];
    
    NSError *error;
    NSManagedObjectContext *context = self.usedObjectContext;
    
    [context save:&error];
    
    if (!error){ // delete Success
        
        NSLog(@"Deleted from DB By HeightViewer!!!");
        
        [group.takenHeightHistory removeObject:di];
        if (group.takenHeightHistory.count <= 0){
            [_dataDic removeObjectForKey:key];
            group = nil;
        }
        
        
        [_dataArray removeObject:di];
        
        if (controller.isConfirmMode){
            [controller.navigationController popToRootViewControllerWithFoldStyle:0];
        }
        else{
            if (_dataArray.count > 0 && controller.isConfirmMode == NO){
                NSInteger reloadPageIndex = index;
                reloadPageIndex = MIN(reloadPageIndex, _dataArray.count-1);
                reloadPageIndex = MAX(reloadPageIndex, 0);
                
                [controller reloadPageViewWithIndex:reloadPageIndex];
            }
            else{
                [controller closeViewControllerAnimated:YES];
            }
        }
        
        [_heightDetailTreeView doDeleteAnimationWithCheckedDetails:@[di]];
    }
    else{
        
    }
    
    
}

-(NSInteger)requestAge
{
    return [self.myChild currentAge];
}

@end
