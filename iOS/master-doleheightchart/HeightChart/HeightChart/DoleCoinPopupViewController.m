//
//  DoleCoinPopupViewController.m
//  HeightChart
//
//  Created by ne on 2014. 3. 24..
//  Copyright (c) 2014년 Kim Sehyun. All rights reserved.
//

#import "DoleCoinPopupViewController.h"
#import "UIViewController+DoleHeightChart.h"

@interface DoleCoinPopupViewController ()
@property (weak, nonatomic) IBOutlet UILabel *lblTop;
@property (weak, nonatomic) IBOutlet UILabel *lblDown;
- (IBAction)clickOK:(id)sender;


@end

@implementation DoleCoinPopupViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dolcoinCount:(NSInteger)count
{
    self.lblTop.font = [UIFont defaultRegularFontWithSize:34/2];
    self.lblTop.textColor = [UIColor colorWithRGB:0x787878];
    
    self.lblDown.font = [UIFont defaultRegularFontWithSize:34/2];
    self.lblDown.textColor = [UIColor colorWithRGB:0x787878];
    
    
    if([self.currentLanguageCode isEqualToString:@"ja"]){
      self.lblTop.font = [UIFont defaultRegularFontWithSize:32/2];
      self.lblDown.font = [UIFont defaultRegularFontWithSize:28/2];
    }
    
    self.lblTop.text = NSLocalizedString(@"Congratulations!", @"");//@"축하합니다.";
    //self.lblDown.text = [NSString stringWithFormat:@"%ld Dole coin을 받았습니다.", (long)count];
    self.lblDown.text = [NSString stringWithFormat:NSLocalizedString(@"You received %ld Dole Coins.", @""), (long)count];
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskLandscape;
}

- (IBAction)clickOK:(id)sender {
    self.completion(self);
}
@end
