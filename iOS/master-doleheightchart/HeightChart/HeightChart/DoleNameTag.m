//
//  DoleNameTag.m
//  HeightChart
//
//  Created by ne on 2014. 3. 21..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "DoleInfo.h"
#import "DoleNameTag.h"
#import "UIColor+ColorUtil.h"
#import "UIFont+DoleHeightChart.h"

@interface DoleNameTag()
{
    UILabel *_nameLabel;
    UIImageView *_leftView;
    UIImageView *_rightView;
    UIImageView *_arrow;
    
    UIImage *_resizableLeftImage;
    UIImage *_resizableRightImage;
    
}
@end

NSString *NameTagLeftImageName [] = { @"name_tag_left_resize", @"name_tag_left_press_resize", @"name_tag_left_del_resize"};
NSString *NameTagArrowImageName [] = { @"name_tag_arrow", @"name_tag_press_arrow", @"name_tag_arrow_del"};
NSString *NameTagRightImageName [] = { @"name_tag_right_resize", @"name_tag_right_press_resize", @"name_tag_right_del_resize"};

UIEdgeInsets kNameTageLeftEdgeInsets = {0, (16.0/2.0), 0, (1.0/2.0)};
UIEdgeInsets kNameTageRightEdgeInsets = {0, (1.0/2.0), 0, (16.0/2.0)};

@implementation DoleNameTag
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self initNameTagImage];
    }
    return self;
}


- (void)initNameTagImage
{
    // top, left, bottom, right point coordinate
   
//    self.backgroundColor = [UIColor yellowColor];
    
    UIImage *leftimage = [UIImage imageNamed:NameTagLeftImageName[0]];
    UIImage *rightimage = [UIImage imageNamed:NameTagRightImageName[0]];
    UIImage *arrowimage = [UIImage imageNamed:NameTagArrowImageName[0]];
   
    
    _resizableLeftImage = [leftimage resizableImageWithCapInsets:kNameTageLeftEdgeInsets
                                                            resizingMode:UIImageResizingModeTile];
    
    _resizableRightImage = [rightimage resizableImageWithCapInsets:kNameTageRightEdgeInsets
                                                            resizingMode:UIImageResizingModeTile];
    
    
    _leftView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 10, 72/2)];
    [_leftView setImage:_resizableLeftImage];

    
    _arrow = [[UIImageView alloc]initWithImage:arrowimage] ;
    _arrow.frame = CGRectMake(_leftView.frame.size.width, 0, _arrow.frame.size.width, _arrow.frame.size.height);
    
    
    _rightView = [[UIImageView alloc]initWithFrame:CGRectMake(_arrow.frame.size.width +  _leftView.frame.size.width, 0, 10, 72/2)];
    [_rightView setImage:_resizableRightImage];


    
    [self addSubview:_arrow];
    [self addSubview:_leftView];
    [self addSubview:_rightView];
    
    
    _nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, 30, 30)];
    
    [self addSubview:_nameLabel] ;
    
}



-(void)addNickName:(NSString*)name withMonkeyType:(enum DoleMonkeyType)type
{

    UIColor *labelColor = [UIColor colorWithMonkeyNumber:type normalState:YES];
    
    
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc]initWithString:name];
    [attrString addAttribute:NSFontAttributeName
                       value:[UIFont fontWithName:@"Roboto-Bold" size:33.0/2 ]
                       range:NSMakeRange(0, attrString.length)];
    
    
    NSMutableParagraphStyle *paraGraphStyle = [[NSMutableParagraphStyle alloc]init];
    paraGraphStyle.lineBreakMode = NSLineBreakByTruncatingTail;
    paraGraphStyle.alignment = NSTextAlignmentCenter;
    
    [attrString addAttribute:NSParagraphStyleAttributeName
                       value:paraGraphStyle range:NSMakeRange(0, attrString.length)];
    
    [attrString addAttribute:NSForegroundColorAttributeName
                       value:labelColor range:NSMakeRange(0, attrString.length)];
    
    [attrString addAttribute:NSStrokeColorAttributeName
                       value:labelColor range:NSMakeRange(0, attrString.length)];
    
    
    _nameLabel.backgroundColor = [UIColor clearColor];
    
//    _nameLabel.attributedText = attrString;
    _nameLabel.text = name;
    _nameLabel.font = [UIFont fontWithName:@"Roboto-Bold" size:33.0/2.0 ];
    _nameLabel.textColor = labelColor;
    
    
    _nameLabel.frame = CGRectMake(0, 0, 156/2, 40/2);
    _nameLabel.textAlignment = NSTextAlignmentLeft;

    
//    CGSize textSize = [[_nameLabel text] sizeWithAttributes:@{NSFontAttributeName:[_nameLabel font]}];
    CGSize textSize = [[_nameLabel text] sizeWithFont:_nameLabel.font];
    
    CGFloat strikeWidth = textSize.width;
    
    [self nameTagResizeWithPixelTextSize:strikeWidth*2];
   

}

-(void)changeNickName:(NSString*)name
{
    _nameLabel.text = name;
    
    CGSize textSize = [[_nameLabel text] sizeWithAttributes:@{NSFontAttributeName:[_nameLabel font]}];
    
    CGFloat strikeWidth = textSize.width;
    
    [self nameTagResizeWithPixelTextSize:strikeWidth*2];
}

- (void)nameTagResizeWithPixelTextSize:(CGFloat)textSize
{
    //전부 픽셀 좌표임

   
    const CGFloat maximumNameTagWidth = 191;
    const CGFloat minimumNameTagSize = 65;
    
    const CGFloat arrowWidth = 14;
    const CGFloat labelMargin = 15;
    
    const CGFloat maximunNameTaglabelWidth = maximumNameTagWidth - labelMargin*2;
    const CGFloat minimunNameTaglabelWidth = minimumNameTagSize - labelMargin*2;
    
    CGFloat newLeftRightSize = 0;
    
    if (textSize < minimunNameTaglabelWidth) {
        newLeftRightSize =  (minimumNameTagSize - arrowWidth)/2;
    }
    else if(textSize > maximunNameTaglabelWidth){
        newLeftRightSize =  (maximumNameTagWidth - arrowWidth)/2;
    }
    else{
        CGFloat calculateLabelSize = textSize +  labelMargin *2;
        newLeftRightSize = (calculateLabelSize - arrowWidth)/2;
    }
    
    
    // 텍스트 사이즈 1 이하 일경우 최소사이즈
//    NSInteger newLeftRightSize = (maximumNameTagWidth - arrowWidth)/2;
    
    //포인트 좌표

    NSInteger poinArrow = arrowWidth/2;
    NSInteger pointNewWidth = newLeftRightSize/2;
    
    
    CGFloat currentNameTagWidth = pointNewWidth + poinArrow + pointNewWidth;
    
    CGFloat leftMargin = (maximumNameTagWidth/2 - currentNameTagWidth)/2;
    
    
    _leftView.frame = CGRectMake(leftMargin, 0, pointNewWidth, 72/2);
    
    _arrow.frame = CGRectMake( _leftView.frame.origin.x + _leftView.frame.size.width , 0, poinArrow, 72/2);
    
    _rightView.frame = CGRectMake(_arrow.frame.origin.x + _arrow.frame.size.width , 0, pointNewWidth, 72/2);
    
    _nameLabel.frame = CGRectMake(leftMargin + labelMargin/2, 6/2, 156/2, 40/2);
    
    
    self.nameTagWidth = _leftView.frame.size.width + _arrow.frame.size.width + _rightView.frame.size.width;
    
    
   


}

- (void)setIsDeleteMode:(BOOL)isDeleteMode
{
    _isDeleteMode = isDeleteMode;
    
    if (_isDeleteMode){
        
        // change images
        UIImage *leftimage = [UIImage imageNamed:NameTagLeftImageName[2]];
        UIImage *rightimage = [UIImage imageNamed:NameTagRightImageName[2]];
        UIImage *arrowimage = [UIImage imageNamed:NameTagArrowImageName[2]];
        
        
        _resizableLeftImage = [leftimage resizableImageWithCapInsets:kNameTageLeftEdgeInsets
                                                        resizingMode:UIImageResizingModeTile];
        
        _resizableRightImage = [rightimage resizableImageWithCapInsets:kNameTageRightEdgeInsets
                                                          resizingMode:UIImageResizingModeTile];
        
        _leftView.image = _resizableLeftImage;
        _rightView.image = _resizableRightImage;
        _arrow.image = arrowimage;

        
    }
    else{

        UIImage *leftimage = [UIImage imageNamed:NameTagLeftImageName[0]];
        UIImage *rightimage = [UIImage imageNamed:NameTagRightImageName[0]];
        UIImage *arrowimage = [UIImage imageNamed:NameTagArrowImageName[0]];
        
        
        _resizableLeftImage = [leftimage resizableImageWithCapInsets:kNameTageLeftEdgeInsets
                                                        resizingMode:UIImageResizingModeTile];
        
        _resizableRightImage = [rightimage resizableImageWithCapInsets:kNameTageRightEdgeInsets
                                                          resizingMode:UIImageResizingModeTile];
        
        _leftView.image = _resizableLeftImage;
        _rightView.image = _resizableRightImage;
        _arrow.image = arrowimage;
    }
    

}

@end
