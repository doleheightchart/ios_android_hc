//
//  HomeToolBar.m
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 21..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "HomeToolBar.h"
#import "UIColor+ColorUtil.h"
#import "UIView+Animation.h"
#import "UIFont+DoleHeightChart.h"
#import "UIButton+CheckAndRadio.h"
#import "UIView+ImageUtil.h"

@interface HomeToolBar (){
    UIButton    *_addHeightBtn;
    UIButton    *_deleteEnterBtn;
    UIButton    *_deleteNicknameBtn;
    UIButton    *_settingBtn;
    UIButton    *_doleCoinBtn;
    UIButton    *_deleteBackBtn;
    
    UILabel     *_doleCoinLabel;
    
    
    //Grass Images
    UIImageView *_grassBottomImageView;
    UIImageView *_grassImageView;
}

@end

@implementation HomeToolBar

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createGrass];
        [self createToolBar];
    }
    return self;
}

- (void)createToolBar
{
    CGFloat w = self.bounds.size.width;
    CGFloat y = 7;
    
    CGFloat widthBtn = 88/2;
    CGFloat heightBtn = 88/2;
    CGFloat gapBtn = 18/2;
    
    //Visible FirstTime
    
    _addHeightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _addHeightBtn.adjustsImageWhenHighlighted = NO;
    _addHeightBtn.backgroundColor = [UIColor clearColor];
    [_addHeightBtn setBackgroundImage:[UIImage imageNamed:@"add_height_btn_normal"] forState:UIControlStateNormal];
    [_addHeightBtn setBackgroundImage:[UIImage imageNamed:@"add_height_btn_press"] forState:UIControlStateHighlighted];
    _addHeightBtn.frame = CGRectMake(w - (widthBtn*3 + gapBtn*2)-7, y, widthBtn, heightBtn);
    [self addSubview:_addHeightBtn];
    
    _deleteEnterBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _deleteEnterBtn.adjustsImageWhenHighlighted = NO;
    _deleteEnterBtn.backgroundColor = [UIColor clearColor];
    [_deleteEnterBtn setBackgroundImage:[UIImage imageNamed:@"delete_btn_normal"] forState:UIControlStateNormal];
    [_deleteEnterBtn setBackgroundImage:[UIImage imageNamed:@"delete_btn_dim_01"] forState:UIControlStateDisabled];
    [_deleteEnterBtn setBackgroundImage:[UIImage imageNamed:@"delete_btn_press"] forState:UIControlStateHighlighted];
    _deleteEnterBtn.frame = CGRectMake(w - (widthBtn*2 + gapBtn*1)-7, y, widthBtn, heightBtn);
    [self addSubview:_deleteEnterBtn];

    _settingBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _settingBtn.adjustsImageWhenHighlighted = NO;
    _settingBtn.backgroundColor = [UIColor clearColor];
    [_settingBtn setBackgroundImage:[UIImage imageNamed:@"option_menu_btn_normal"] forState:UIControlStateNormal];
    [_settingBtn setBackgroundImage:[UIImage imageNamed:@"option_menu_btn_press"] forState:UIControlStateHighlighted];
    _settingBtn.frame = CGRectMake(w - widthBtn-7, y, widthBtn, heightBtn);
    [self addSubview:_settingBtn];
    
    
    _doleCoinBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _doleCoinBtn.adjustsImageWhenHighlighted = NO;
    _doleCoinBtn.backgroundColor = [UIColor clearColor];
    [_doleCoinBtn setBackgroundImage:[UIImage imageNamed:@"home_dole_coin"] forState:UIControlStateNormal];
    _doleCoinBtn.frame = CGRectMake(7, y, widthBtn, heightBtn);
    [self addSubview:_doleCoinBtn];
    
    UIImageView *imgX = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"home_dole_coin_x"]];
    imgX.frame = CGRectMake(44, (44-23)/2, 15, 23);
    [_doleCoinBtn addSubview:imgX];
    
    _doleCoinLabel = [[UILabel alloc]init];
    _doleCoinLabel.frame = CGRectMake(44 + 15 + 3, (44-23)/2, 77, 23);
    _doleCoinLabel.backgroundColor = [UIColor clearColor];
    _doleCoinLabel.textAlignment = NSTextAlignmentLeft;
    _doleCoinLabel.textColor = [UIColor colorWithRGB:0xff5411];
    _doleCoinLabel.font = [UIFont defaultBoldFontWithSize:42/2];
    _doleCoinLabel.text = @"?";
    [_doleCoinBtn addSubview:_doleCoinLabel];
    
    
    // for splash animation
//    _addHeightBtn.transform = CGAffineTransformMakeScale(0, 0);
//    _deleteEnterBtn.transform = CGAffineTransformMakeScale(0, 0);
//    _settingBtn.transform = CGAffineTransformMakeScale(0, 0);
//    _doleCoinBtn.transform = CGAffineTransformMakeScale(0, 0);
    
    //Not visible FirstTime
    
    _deleteNicknameBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _deleteNicknameBtn.adjustsImageWhenHighlighted = NO;
    _deleteNicknameBtn.backgroundColor = [UIColor clearColor];
    [_deleteNicknameBtn setImage:[UIImage imageNamed:@"delete_btn_normal_02"] forState:UIControlStateNormal];
    [_deleteNicknameBtn setBackgroundImage:[UIImage imageNamed:@"delete_btn_dim_02"] forState:UIControlStateDisabled];
    [_deleteNicknameBtn setBackgroundImage:[UIImage imageNamed:@"delete_btn_press_02"] forState:UIControlStateHighlighted];
    _deleteNicknameBtn.frame = _settingBtn.frame;
    [self addSubview:_deleteNicknameBtn];
    
    _deleteBackBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _deleteBackBtn.adjustsImageWhenHighlighted = NO;
    _deleteBackBtn.backgroundColor = [UIColor clearColor];
    [_deleteBackBtn setImage:[UIImage imageNamed:@"delete_btn_back_normal"] forState:UIControlStateNormal];
    [_deleteBackBtn setBackgroundImage:[UIImage imageNamed:@"delete_btn_back_press"] forState:UIControlStateHighlighted];
    _deleteBackBtn.frame = _deleteEnterBtn.frame;
    [self addSubview:_deleteBackBtn];
    
    _deleteEnterBtn.enabled = NO;
    
//    _deleteNicknameBtn.hidden = YES;
//    _deleteBackBtn.hidden = YES;
//    _deleteNicknameBtn.layer.opacity = 0;
//    _deleteBackBtn.layer.opacity = 0;
    
    _addHeightBtn.hidden = YES;
    _deleteEnterBtn.hidden = YES;
    _deleteNicknameBtn.hidden = YES;
    _settingBtn.hidden = YES;
    _doleCoinBtn.hidden = YES;
    _deleteBackBtn.hidden = YES;
    
    _addHeightBtn.tag = kAddHeightButton;
    _deleteEnterBtn.tag = kDeleteModeButton;
    _deleteNicknameBtn.tag = kDeleteNickNameButton;
    _settingBtn.tag = kSettingButton;
    _doleCoinBtn.tag = kDoleCoinButton;
    _deleteBackBtn.tag = kDeleteBackButton;
    
    [_addHeightBtn setupSizeWhenPressed];
    [_deleteNicknameBtn setupSizeWhenPressed];
    [_settingBtn setupSizeWhenPressed];
    [_deleteEnterBtn setupSizeWhenPressed];
    [_doleCoinBtn setupSizeWhenPressed];
    [_deleteBackBtn setupSizeWhenPressed];
    
    [_addHeightBtn addTarget:self action:@selector(clickToolBarButton:) forControlEvents:UIControlEventTouchUpInside];
    [_deleteEnterBtn addTarget:self action:@selector(clickToolBarButton:) forControlEvents:UIControlEventTouchUpInside];
    [_deleteNicknameBtn addTarget:self action:@selector(clickToolBarButton:) forControlEvents:UIControlEventTouchUpInside];
    [_settingBtn addTarget:self action:@selector(clickToolBarButton:) forControlEvents:UIControlEventTouchUpInside];
    [_doleCoinBtn addTarget:self action:@selector(clickToolBarButton:) forControlEvents:UIControlEventTouchUpInside];
    [_deleteBackBtn addTarget:self action:@selector(clickToolBarButton:) forControlEvents:UIControlEventTouchUpInside];
    
//    [_addHeightBtn animateScaleEffectWithDuration:0.3 MinScale:0.5 MaxScale:1.2 completionBlock:nil];
//    [_deleteNicknameBtn animateScaleEffectWithDuration:0.3 MinScale:0.5 MaxScale:1.2 completionBlock:nil];
//    [_settingBtn animateScaleEffectWithDuration:0.3 MinScale:0.5 MaxScale:1.2 completionBlock:nil];
    
    

}

- (void)playSplashAnimation
{
    
    _addHeightBtn.transform = CGAffineTransformMakeScale(0, 0);
    _deleteEnterBtn.transform = CGAffineTransformMakeScale(0, 0);
    _settingBtn.transform = CGAffineTransformMakeScale(0, 0);
    _doleCoinBtn.transform = CGAffineTransformMakeScale(0, 0);
    
    _addHeightBtn.hidden = NO;
    _deleteEnterBtn.hidden = NO;
    _deleteNicknameBtn.hidden = NO;
    _settingBtn.hidden = NO;
    _doleCoinBtn.hidden = NO;
    _deleteBackBtn.hidden = NO;

    _deleteBackBtn.layer.opacity = 0;
    _deleteNicknameBtn.layer.opacity = 0;
    
    [UIView animateWithDuration:0.3 animations:^{
        _addHeightBtn.transform = CGAffineTransformMakeScale(1.2, 1.2);
        _deleteEnterBtn.transform = CGAffineTransformMakeScale(1.2, 1.2);
        _settingBtn.transform = CGAffineTransformMakeScale(1.2, 1.2);
        _doleCoinBtn.transform = CGAffineTransformMakeScale(1.2, 1.2);
        
//        _addHeightBtn.transform = CGAffineTransformMakeScale(1, 1);
//        _deleteEnterBtn.transform = CGAffineTransformMakeScale(1, 1);
//        _settingBtn.transform = CGAffineTransformMakeScale(1, 1);
//        _doleCoinBtn.transform = CGAffineTransformMakeScale(1, 1);
    } completion:^(BOOL finished) {
        _addHeightBtn.transform = CGAffineTransformMakeScale(1, 1);
        _deleteEnterBtn.transform = CGAffineTransformMakeScale(1, 1);
        _settingBtn.transform = CGAffineTransformMakeScale(1, 1);
        _doleCoinBtn.transform = CGAffineTransformMakeScale(1, 1);
    }];
    
//    [_addHeightBtn animateScaleEffectWithDuration:0.3 MinScale:0.5 MaxScale:1.2 completionBlock:nil];
//    [_deleteEnterBtn animateScaleEffectWithDuration:0.3 MinScale:0.5 MaxScale:1.2 completionBlock:nil];
//    [_settingBtn animateScaleEffectWithDuration:0.3 MinScale:0.5 MaxScale:1.2 completionBlock:nil];
//    [_doleCoinBtn animateScaleEffectWithDuration:0.3 MinScale:0.5 MaxScale:1.2 completionBlock:nil];

}


- (void)setIsDeleteMode:(BOOL)isDeleteMode
{
    _isDeleteMode = isDeleteMode;
    
    CGFloat opacityNormal = _isDeleteMode ? 0 : 1;
    CGFloat opacityDelete = _isDeleteMode ? 1 : 0;
    
    [UIView animateWithDuration:0.5 animations:^{
        _addHeightBtn.layer.opacity = opacityNormal;
        _deleteEnterBtn.layer.opacity = opacityNormal;
        _settingBtn.layer.opacity = opacityNormal;
        _doleCoinBtn.layer.opacity = opacityNormal;
        
        _deleteNicknameBtn.layer.opacity = opacityDelete;
        _deleteNicknameBtn.enabled = NO;
        _deleteBackBtn.layer.opacity = opacityDelete;
        
//        _deleteNicknameBtn.transform = CGAffineTransformMakeScale(1, 1);
//        _deleteBackBtn.transform = CGAffineTransformMakeScale(1, 1);

    }];
}

- (void)setEnableDeleteBtn:(BOOL)enableDeleteBtn
{
    _enableDeleteBtn = enableDeleteBtn;
    
    _deleteNicknameBtn.enabled = _enableDeleteBtn;
}

- (void)setAvailableSelected:(BOOL)availableSelected
{
    _availableSelected = availableSelected;
    
    _deleteEnterBtn.enabled = _availableSelected;
}

- (void)clickToolBarButton:(UIButton*)button
{
    [self.toolBarDelegate clickButtonWithType:button.tag];
}

- (void)updateDoleCoin
{
     _doleCoinLabel.text = [NSString stringWithFormat:@"%lu", (unsigned long)_doleCoinCount];
}

- (void)setDoleCoinCount:(NSUInteger)doleCoinCount
{
    _doleCoinCount = doleCoinCount;
    
    _doleCoinLabel.text = [NSString stringWithFormat:@"%lu", (unsigned long)_doleCoinCount];
}

#pragma mark Grass

- (void)createGrass
{
    CGRect f = CGRectMake(0, 0, self.bounds.size.width, 111/2);
    _grassBottomImageView = [[UIImageView alloc]initWithFrame:f];
    
//    UIImage *resizeGrassImage = [UIImage resizableImageWithName:@"bottom_grass" CapInsets:UIEdgeInsetsMake(0, 35/2, 0, 35/2)];
    UIImage *resizeGrassImage = [UIImage resizableImageWithName:@"bottom_grass" CapInsets:UIEdgeInsetsMake(0, 26/2, 0, 26/2)];
    _grassBottomImageView.image = resizeGrassImage;
    
    [self addSubview:_grassBottomImageView];
    
    CGRect f2 = CGRectMake(35/2, 0, self.bounds.size.width-35, 111/2);
    _grassImageView = [[UIImageView alloc]initWithFrame:f2];
    _grassImageView.image = [UIImage imageNamed:@"grass_02"];
    
    [self addSubview:_grassImageView];
    
}

@end
