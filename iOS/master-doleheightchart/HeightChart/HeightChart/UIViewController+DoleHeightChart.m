//
//  UIViewController+DoleHeightChart.m
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 10..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "UIViewController+DoleHeightChart.h"
#import "UIView+ImageUtil.h"
#import "AppDelegate.h"
#import "Reachability.h"
#import "ToastController.h"
#import "MPFoldTransition/DoleTransition.h"
#import "UIViewController+PolyServer.h"
#import "MessageBoxController.h"
#import "LogInViewController.h"

#define kTAG_Indicator 4545

@implementation UIViewController (DoleHeightChartUI)

-(void)addActiveIndicator
{
    [self removeActiveIndicator];
    
    UIActivityIndicatorView* indicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:(UIActivityIndicatorViewStyleGray)];
    indicator.center = CGPointMake(self.view.bounds.size.width/2, self.view.bounds.size.height/2);
    indicator.tag = kTAG_Indicator;
    [self.view addSubview:indicator];
    //    self.view.layer.opacity = 0.25;
    self.view.userInteractionEnabled = NO; //not use when test
    [indicator startAnimating];
    
   // NSLog(@"UserInteractionEnable = NO");
}

-(void)removeActiveIndicator
{
    UIView *prevIndicator = [self.view viewWithTag:kTAG_Indicator];
    [prevIndicator removeFromSuperview];
    self.view.userInteractionEnabled = YES;
    self.view.layer.opacity = 1;
    
   // NSLog(@"UserInteractionEnable = YES");

}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

-(void)closeMe:(id)sender
{
    [[self.view findFirstResponder] resignFirstResponder];
    
//    if (self.navigationController)
//        [self.navigationController popViewControllerAnimated:YES];
//    else
//        [self dismissViewControllerAnimated:YES completion:nil];
    [self closeViewControllerAnimated:NO];
}


-(UIButton*)addCommonCloseButton
{
    UIButton *button = [UIButton commonCancelButtonWithTarget:self action:@selector(closeMe:)];
    //    button.frame = CGRectMake(269, 4, 44, 44);
    //    self.view.bounds;
    button.frame = CGRectMake(self.view.bounds.size.width - (102)/2,
                              0,
                              102/2,
                              96/2);
    
    [self.view addSubview:button];
    
    return button;
}

-(UIButton*)addCameraCloseButton
{
    UIButton *button = [UIButton cameraCancelButtonWithTarget:self action:@selector(closeMe:)];
    button.frame = CGRectMake(self.view.bounds.size.width - (102)/2,
                              0,
                              102/2,
                              96/2);
    
    [self.view addSubview:button];
    
    return button;
}

-(void)addTitleByBigSizeImage:(BOOL)isBig Visible:(BOOL)visible;
{
    if (!visible) return ;
    
    CGRect frame;
    NSString *imageName;
    if (isBig) {
        frame = CGRectMake(83, 110/2, 310/2, 168/2);
        imageName = @"Login_logo";
    }
    else{
        
        frame = CGRectMake(83, 40/2, 310/2, 168/2);
        imageName = @"Login_option_logo";
    }
    
    UIImageView *title = [[UIImageView alloc]initWithImage:[UIImage imageNamed:imageName]];
    title.frame = frame;
    
    [self.view addSubview:title];
    return ;
}

//-(void)addTitleImageVisible:(BOOL)visible
//{
//    if (!visible) return ;
//        
//    BOOL is4Inch = [UIScreen isFourInchiScreen];
//    
//    CGRect frame;
//    NSString *imageName;
//    if (is4Inch) {
//        frame = CGRectMake(83, 110/2, 310/2, 168/2);
//        imageName = @"Login_logo";
//    }
//    else{
//       
//        frame = CGRectMake(83, 40/2, 310/2, 168/2);
//        imageName = @"Login_option_logo";
//    }
//    
//    UIImageView *title = [[UIImageView alloc]initWithImage:[UIImage imageNamed:imageName]];
//    title.frame = frame;
//    
//    [self.view addSubview:title];
//    return ; 
//    
//}

-(void)moveByPositionY:(CGFloat)moveY Controls:(NSArray*)controls
{
    for (UIView* control in controls) {
        CGRect frame = control.frame;
        frame.origin.y += moveY;
        //control.frame = frame;
        control.layer.position = CGPointMake(control.layer.position.x, control.layer.position.y + moveY);
    }
}

-(void)moveToPositionY:(CGFloat)moveY Control:(UIView*)control
{
    CGRect frame = control.frame;
    frame.origin.y = moveY;
    control.frame = frame;
}

-(void)moveToPositionYPixel:(CGFloat)moveY Control:(UIView*)control
{
    CGRect frame = control.frame;
    frame.origin.y = moveY/2;
    control.frame = frame;
}

-(void)moveToPositionXPixel:(CGFloat)moveX Control:(UIView*)control
{
    CGRect frame = control.frame;
    frame.origin.x = moveX/2;
    control.frame = frame;
}

-(void)changeToHeightPixel:(CGFloat)height Control:(UIView*)control
{
    CGRect frame = control.frame;
    frame.size.height = height/2;
    control.frame = frame;
}

-(void)changeToHeight:(CGFloat)height Control:(UIView*)control
{
    CGRect frame = control.frame;
    frame.size.height = height;
    control.frame = frame;
}

-(void)makeSkyBackground
{
    UIImageView *backImage = [[UIImageView alloc]initWithFrame:self.view.bounds];
    backImage.image = [UIImage imageNamed:@"sky_bg"];
    
    [self.view insertSubview:backImage atIndex:0];
}

-(UIView*)addTitleMiniToLeftTop
{
    UIImageView *titleMiniView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 158/2, 94/2)];
    titleMiniView.image = [UIImage imageNamed:@"title_mini"];
    
    [self.view addSubview:titleMiniView];
    
    return titleMiniView;
}

-(BOOL)enableActionWithCurrentNetwork
{
   
    if (NO == self.isEnableNetwork){
        
        [self toastNetworkError];
        
        return FALSE;
    }
    return TRUE;
}

-(void)presentLogInViewControllerWithCompletion:(void(^)(BOOL isLoggedIn))completion
{
    [self showLogInCancelConfirmMessage:NSLocalizedString(@"Log in to view Dole Coins\ninformation.", @"") completion:^(BOOL isYes) {
        
        if (isYes)
        {
            UIStoryboard *story = [UIStoryboard storyboardWithName:@"LogIn" bundle:nil];
            LogInViewController *vc = [story instantiateViewControllerWithIdentifier:@"LogIn"];
            vc.requireLoginCompletion = completion;
            UINavigationController *navi = [[UINavigationController alloc]initWithRootViewController:vc];
            navi.navigationBarHidden = YES;
            //[self modalTransitionController:navi Animated:YES];
            [self presentViewController:navi animated:YES completion:nil];
        }
        else{
            completion(NO);
        }
        
    }];
}

-(void)presentLogInViewControllerOKCancelWithCompletion:(void(^)(BOOL isLoggedIn))completion
{
    [self showOkCancelConfirmMessage:NSLocalizedString(@"You need to log in.\nLog in now?", @"") completion:^(BOOL isYes) {
        
        if (isYes)
        {
            UIStoryboard *story = [UIStoryboard storyboardWithName:@"LogIn" bundle:nil];
            LogInViewController *vc = [story instantiateViewControllerWithIdentifier:@"LogIn"];
            vc.requireLoginCompletion = completion;
            UINavigationController *navi = [[UINavigationController alloc]initWithRootViewController:vc];
            navi.navigationBarHidden = YES;
            //[self modalTransitionController:navi Animated:YES];
            [self presentViewController:navi animated:YES completion:nil];
        }
        else{
            completion(NO);
        }
        
    }];
}



@end

@implementation UIViewController (DoleHeightChartTransition)

#define kTransitionDuration 0.3
#define kTransitionFromOpacity 0.25
#define kTransitionFromScale 0.75

-(void)pushTransitionController:(UIViewController*)dstViewController Animated:(BOOL)animated
{
//    [self.navigationController pushViewController:dstViewController animated:animated];
    [self.navigationController pushViewController:dstViewController foldStyle:0];
}

-(void)modalTransitionController:(UIViewController*)dstViewController Animated:(BOOL)animated
{
//    [self presentViewController:dstViewController animated:animated completion:nil];
    [self presentViewController:dstViewController foldStyle:0 completion:nil];
}
-(void)closeViewControllerAnimated:(BOOL)animated
{
    if (self.navigationController){
        if (self.navigationController.viewControllers.count > 1){
//            [self.navigationController popViewControllerAnimated:animated];
            [self.navigationController popViewControllerWithFoldStyle:0];
        }
        else{
//            [self.navigationController dismissViewControllerAnimated:animated completion:nil];
            [self.navigationController.presentingViewController dismissViewControllerWithFoldStyle:0 completion:nil];
        }
    }
    else{
        //[self dismissViewControllerAnimated:animated completion:nil];
        //[self.presentedViewController dismissViewControllerWithFoldStyle:0 completion:nil];
        [self.presentingViewController dismissViewControllerWithFoldStyle:0 completion:nil];
    }
}


//-(void)pushTransitionController:(UIViewController*)dstViewController Animated:(BOOL)animated
//{
//    assert(self.navigationController);
//    
//    if (animated == NO){
//        [self.navigationController pushViewController:dstViewController animated:NO];
//        return;
//    }
//    
//    UIView *dstView = dstViewController.view;
//    
//    dstView.layer.opacity = kTransitionFromOpacity;
//    dstView.transform = CGAffineTransformMakeScale(kTransitionFromScale, kTransitionFromScale);
//    
//    [self.navigationController.view addSubview:dstView];
//    
//    [UIView animateWithDuration:kTransitionDuration animations:^{
//        
//        dstView.layer.opacity = 1;
//        dstView.transform = CGAffineTransformMakeScale(1, 1);
//        
//    } completion:^(BOOL finished) {
//        [self.navigationController pushViewController:dstViewController animated:NO];
//    }];
//}
//
//-(void)modalTransitionController:(UIViewController*)dstViewController Animated:(BOOL)animated;
//{
//    if (animated == NO){
//        [self presentViewController:dstViewController animated:NO completion:nil];
//        return;
//    }
//    
//    UIView *superView = self.view.superview;
//    UIView *dstView = dstViewController.view;
//
//    UIView *dstV = dstViewController.view;
//    
//    dstView.layer.opacity = kTransitionFromOpacity;
//    dstView.transform = CGAffineTransformMakeScale(kTransitionFromScale, kTransitionFromScale);
//    
//    [superView addSubview:dstV];
//    
//    [UIView animateWithDuration:kTransitionDuration animations:^{
//        
//        dstView.layer.opacity = 1;
//        dstView.transform = CGAffineTransformMakeScale(1, 1);
//        
//    } completion:^(BOOL finished) {
//        [dstV removeFromSuperview];
//        [self presentViewController:dstViewController animated:NO completion:nil];
//    }];
//}
//
//-(void)closeViewControllerAnimated:(BOOL)animated
//{
//    if (self.navigationController){
//        
//        NSArray *naviStacks = self.navigationController.viewControllers;
//        
//        if (naviStacks.count <= 1) {
//            [self.navigationController closeViewControllerAnimated:NO];
//            return;
//        }
//        
//        UIViewController *pvc = naviStacks[ naviStacks.count - 2];
//        
//        UIView *sv = self.view.superview;
//        [sv insertSubview:pvc.view belowSubview:self.view];
//        pvc.view.layer.opacity = 1;
//        pvc.view.transform = CGAffineTransformMakeScale(1, 1);
//        
//        [UIView animateWithDuration:kTransitionDuration animations:^{
//            self.view.layer.opacity = kTransitionFromOpacity;
//            self.view.transform = CGAffineTransformMakeScale(kTransitionFromScale, kTransitionFromScale);
//
//            pvc.view.layer.opacity = 1;
//            
//        } completion:^(BOOL finished) {
//            [pvc.view removeFromSuperview];
//            [self.navigationController popViewControllerAnimated:NO];
//        }];
//    }
//    else{
//        
//        UIViewController *pvc = self.presentingViewController;
//        
//        UIView *sv = self.view.superview;
//        [sv insertSubview:pvc.view belowSubview:self.view];
//        pvc.view.layer.opacity = 1;
//        pvc.view.transform = CGAffineTransformMakeScale(1, 1);
//        
//        [UIView animateWithDuration:kTransitionDuration animations:^{
//            self.view.layer.opacity = kTransitionFromOpacity;
//            self.view.transform = CGAffineTransformMakeScale(kTransitionFromScale, kTransitionFromScale);
//            pvc.view.transform = CGAffineTransformMakeScale(1, 1);
//            pvc.view.layer.opacity = 1;
//            
//        } completion:^(BOOL finished) {
//            [self dismissViewControllerAnimated:animated completion:nil];
//        }];
//    }
//}


@end

@implementation UIViewController (KeyboardControl)

#define kKeyboardAnimationDuration 0.3
#define kKeyboardGapToTextField 20

-(void)addKeyboardObserver
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(willAppearKeyboard:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(willDisAppearKeyboard:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

-(UITextField*)atLeatAdjustHeightControl
{
    return nil;
}

-(void)willAppearKeyboard:(NSNotification *)notification
{
    UITextField *txtField = [self atLeatAdjustHeightControl];
    
//    UIView *txtField = [self.view findFirstResponder];
    
    if (txtField){
        CGRect keyboardBounds;
        [[notification.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardBounds];
        
        CGRect f = [txtField convertRect:txtField.frame toView:self.view];
        
       
        CGFloat textBottomYPos = f.origin.y + f.size.height + kKeyboardGapToTextField;
        CGFloat keyboardYPos = [UIScreen mainScreen].bounds.size.height - keyboardBounds.size.height;
        
        if (textBottomYPos > keyboardYPos){
            CGFloat moveUpYPos = keyboardYPos - textBottomYPos;
            CGPoint newCenter = CGPointMake(self.view.center.x, self.view.center.y + moveUpYPos);
            [UIView animateWithDuration:kKeyboardAnimationDuration animations:^{
                self.view.center = newCenter;
            }];
        }
    }

    [self prepareKeyboardAppear];
}

-(void)willDisAppearKeyboard:(NSNotification *)notification
{
//    UITextField *txtField = [self atLeatAdjustHeightControl];
    
    if (self.view.frame.origin.y < 0){
        [UIView animateWithDuration:kKeyboardAnimationDuration animations:^{
            CGFloat deltaY = self.view.frame.origin.y;
            self.view.center = CGPointMake(self.view.center.x, self.view.center.y - deltaY);
        }];
    }
    
    [self prepareKeyboardDisAppear];
}

-(void)prepareKeyboardAppear
{
}

-(void)prepareKeyboardDisAppear
{
}


@end





@implementation NSObject (DoleHeightChartConfig)

-(UserConfig*)userConfig
{
    AppDelegate* appController = (AppDelegate*)[UIApplication sharedApplication].delegate;
    return appController.userConfig;
}

-(BOOL)isEnableNetwork
{
    return [[Reachability reachabilityForInternetConnection] currentReachabilityStatus] != NotReachable;
}

-(BOOL)isCellularNetwork
{
    return [[Reachability reachabilityForInternetConnection] currentReachabilityStatus] == ReachableViaWWAN;
}

-(NSDictionary *)englishCountryNamesAndCities
{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"countriesToCities" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    
    return json;
}

- (NSDictionary *)countriesCodesAndEnglishCommonNames
{
    NSLocale *locale = [NSLocale localeWithLocaleIdentifier:@"en_US"];
    NSArray *countryArray = [NSLocale ISOCountryCodes];
    
    NSMutableDictionary *countriesDict = [[NSMutableDictionary alloc] init];
    
    for (NSString *countryCode in countryArray) {
        NSString *displayNameString = [locale displayNameForKey:NSLocaleCountryCode value:countryCode];
        [countriesDict setObject:displayNameString forKey:countryCode];
    }
    
    return countriesDict;
}

-(NSString*)currentCountryCode
{
    NSString *currentCode = [[NSLocale currentLocale] localeIdentifier];
    NSArray *arr = [currentCode componentsSeparatedByString:@"_"];
    NSString *countryCode = [arr lastObject];
    return countryCode;
}

-(NSString*)currentLanguageCode
{
    NSString *langCode = [[NSLocale preferredLanguages] objectAtIndex:0];
    return langCode;
}

-(NSString*)currentLanguageName
{
//    NSString *langCode = [[NSLocale currentLocale] objectForKey:NSLocaleLanguageCode];  // get the current language.
    NSString *langCode = [[NSLocale preferredLanguages] objectAtIndex:0];  // get the current language.
    NSString *country_code = [self currentCountryCode];
    NSString *lang_country = [NSString stringWithFormat:@"%@_%@", langCode, country_code];
    
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:lang_country];
    NSString *langName = [locale displayNameForKey:NSLocaleIdentifier value:lang_country];
    return langName;
}

@end

@implementation NSObject (Audio)

- (void)playMusic
{
    AppDelegate* appController = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appController playBackMusic];
}
- (void)pauseMusic
{
    AppDelegate* appController = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appController pauseBackMusic];
}
- (void)stopMusic
{
    AppDelegate* appController = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appController stopBackMusic];
}

@end

@implementation UIView (KeyboardControl)
- (id)findFirstResponder
{
    if (self.isFirstResponder) {
        return self;
    }
    for (UIView *subView in self.subviews) {
        id responder = [subView findFirstResponder];
        if (responder) return responder;
    }
    return nil;
}
@end

@implementation UIScreen (DoleHeightChartUI)

+(BOOL)isFourInchiScreen
{
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    return screenBounds.size.height == 568;
}

@end

@implementation NSObject (FacebookManager)

-(FacebookManager*)facebookManager
{
    AppDelegate* appController = (AppDelegate*)[UIApplication sharedApplication].delegate;
    return appController.facebookManager;
}

@end

//@implementation UINavigationController (DoleHeightChartUI)
//
//- (NSUInteger)supportedInterfaceOrientations
//{
//    return UIInterfaceOrientationMaskPortrait;
//}
//
//@end

@implementation UIStoryboard (Localized)

+(instancetype)storyboardWithLogIn
{
//    NSBundle *mainBundle = [NSBundle mainBundle];
//    NSBundle *bundle = [NSBundle bundleWithPath:[mainBundle pathForResource:mainBundle.currentLanguageCode
//                                                                     ofType:@"lproj"]];
//    UIStoryboard *storyboard =
//    [UIStoryboard storyboardWithName:@"LogIn" bundle:bundle];

    UIStoryboard *storyboard =
    [UIStoryboard storyboardWithName:@"LogIn" bundle:nil];

    
    return storyboard;
}

+(instancetype)storyboardWithPopup
{
//    NSBundle *mainBundle = [NSBundle mainBundle];
//    NSBundle *bundle = [NSBundle bundleWithPath:[mainBundle pathForResource:mainBundle.currentLanguageCode
//                                                                     ofType:@"lproj"]];
//    UIStoryboard *storyboard =
//    [UIStoryboard storyboardWithName:@"Popup" bundle:bundle];
//    
//    return storyboard;
    
    return [UIStoryboard storyboardWithName:@"Popup" bundle:nil];
    
}

+(instancetype)storyboardWithMain
{
//    NSBundle *mainBundle = [NSBundle mainBundle];
//    NSBundle *bundle = [NSBundle bundleWithPath:[mainBundle pathForResource:mainBundle.currentLanguageCode
//                                                                     ofType:@"lproj"]];
//    UIStoryboard *storyboard =
//    [UIStoryboard storyboardWithName:@"Main" bundle:bundle];
//    
//    return storyboard;

    return [UIStoryboard storyboardWithName:@"Main" bundle:nil];

}


@end
