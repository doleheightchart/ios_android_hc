//
//  SelectShareAppPopupController.m
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 16..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "SelectShareAppPopupController.h"

@interface SelectShareAppPopupController ()
- (IBAction)onCancel:(id)sender;

@end

@implementation SelectShareAppPopupController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.popupTitleLabel.text = NSLocalizedString(@"Share", @"");
//    self.cancelButton.titleLabel.text = NSLocalizedString(@"Cancel", @"");
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1; // 1 is Add Nickname CellShareAppCell
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ShareAppCell" forIndexPath:indexPath];
    
    UIImageView *imageView = (UIImageView*)[cell viewWithTag:101];
    UILabel *nicknameLabel = (UILabel*)[cell viewWithTag:102];
    
    imageView.image = [UIImage imageNamed:@"popup_share_facebook"];
    nicknameLabel.text = NSLocalizedString(@"Facebook", @"");
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Selected Nickname row index is %d", indexPath.row);
    
    [self completionWithResult:YES];
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskLandscape;
}



- (IBAction)onCancel:(id)sender {
    
    [self completionWithResult:NO];
}

-(void)completionWithResult:(BOOL)result
{
    
    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_6_1){
        [self releaseModal];
        self.CompletionBlock(result);

    }
    else{
        
        self.CompletionBlock(result);
        
        [self releaseModal];


    }
}

@end
