//
//  InputChecker.h
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 4. 9..
//  Copyright (c) 2014년 Kim Sehyun. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TextViewDualPlaceHolder;

@interface InputChecker : NSObject

//-(void)addTextFields:(NSArray*)textFieldArray;
//-(void)addSubmitButton:(UIButton*)submitButton;
//-(void)addHostView:(UIView*)hostView;
@property (nonatomic, retain) UITextField *ignorCurPasswordText;
@property (nonatomic, retain) UITextField *ignorPasswordTextOrg;
@property (nonatomic, retain) UITextField *ignorPasswordTextConfirm;
@property (nonatomic, retain) UITextView *bigTextView;

-(void)addHostViewRecursive:(UIView*)hostView submitButton:(UIButton*)submitButton checkBoxButtons:(NSArray*)checkBoxArray;
-(void)removeDTextView:(TextViewDualPlaceHolder*)textview;
-(void)updateSubmitEnable;
@end
