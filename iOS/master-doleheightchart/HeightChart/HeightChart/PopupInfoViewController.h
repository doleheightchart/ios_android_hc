//
//  PopupInfoViewController.h
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 16..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PopupInfoViewController : UIViewController

@property (nonatomic,strong) NSString* linkURL;
@property (nonatomic,strong) NSString* linkMessage;
@property (nonatomic,weak) UIImage *infoImage;
@end
