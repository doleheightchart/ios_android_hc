//
//  DualPlaceHolderTextField.h
//  HeightChart
//
//  Created by mytwoface on 2014. 2. 21..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DualPlaceHolderTextField : UITextField

@property(nonatomic,copy)   NSString *firtstPlaceHolder;
@property(nonatomic,copy)   NSString *secondPlaceHodler;
@property(nonatomic,copy)   NSString *secondBottomPlaceHolder;
@property(nonatomic)        BOOL isMultiLine;


-(void)changeFontSize:(CGFloat)fontSize;

-(void)setReDrawPlaceHolder:(NSString*)firstText SecondText:(NSString*)secondText;

-(void)setReDrawPlaceHolder:(NSString*)firstText SecondTopText:(NSString*)secondTopText SecondBottomText:(NSString*)secondBottomText;

@end
