//
//  ConfirmPopupController.m
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 2. 11..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "ConfirmPopupController.h"
#import "UIView+ImageUtil.h"

@interface ConfirmPopupController ()
//@property (nonatomic, weak) IBOutlet UIImageView *noTitleImage;
//@property (nonatomic, weak) IBOutlet UIImageView *bottomImage;
//@property (nonatomic, weak) IBOutlet UIButton *okButton;
//@property (nonatomic, weak) IBOutlet UIButton *cancelButton;
//
//-(void)decorateUI;
-(IBAction)clickOK:(id)sender;
-(IBAction)clickCancel:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *lblUpLabel;
@property (weak, nonatomic) IBOutlet UILabel *lblBottomLabel;


@end

@implementation ConfirmPopupController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
//    [self decorateUI];
    
//    self.okButton.titleLabel.text = NSLocalizedString(@"OK", @"");
//    self.cancelButton.titleLabel.text = NSLocalizedString(@"Cancel", @"");
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)completedWithResult:(BOOL)result
{
    [self releaseModal];
    
    self.completionBlock(result);
}

-(IBAction)clickOK:(id)sender
{
    [self completedWithResult:YES];
}

-(IBAction)clickCancel:(id)sender
{
    [self completedWithResult:NO];
}

-(void)popupTextUp:(NSString*)upText TextBottom:(NSString*)bottom
{
    //TODO:: 사양서에는 34로 되어있으나 폰트 사이즈가 벗어난다. 그래서 32로변경 <확인>
    // 로컬라이즈를 할때 한국어인지 선택하도록
    
    self.lblBottomLabel.text = bottom;
    self.lblUpLabel.text = upText;
    
    self.lblBottomLabel.font = [UIFont defaultKoreanFontWithSize:32/2];
    self.lblBottomLabel.textColor = [UIColor colorWithRGB:0x787878];
    
    self.lblUpLabel.font = [UIFont defaultKoreanFontWithSize:32/2];
    self.lblUpLabel.textColor = [UIColor colorWithRGB:0x787878];
}

-(void)setIsOneButton:(BOOL)isOneButton
{
    _isOneButton = isOneButton;
    
//    if (_isOneButton) {
//        [self.cancelButton setHidden:YES];
//        CGRect  rect = self.okButton.frame;
//        rect.origin.x = 4;
//        rect.size.width = self.view.bounds.size.width - 8;
//        self.okButton.frame = rect;
//    }
//    else
//    {
//        [self.cancelButton setHidden:NO];
//    }
}

@end
