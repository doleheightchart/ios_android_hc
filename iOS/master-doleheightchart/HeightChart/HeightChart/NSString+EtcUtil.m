//
//  NSString+EtcUtil.m
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 4. 21..
//  Copyright (c) 2014년 Kim Sehyun. All rights reserved.
//

#import "NSString+EtcUtil.h"

@implementation NSString (EtcUtil)
+(instancetype)stringWithKideHeightWithOutCmUnit:(CGFloat)kidHeight
{
    CGFloat fd = fmodf(kidHeight, 1);
    
    if ( fd > 0 )
        return [NSString stringWithFormat:@"%.1f", kidHeight, nil];
    else
        return [NSString stringWithFormat:@"%d", (int)kidHeight, nil];
}
+(instancetype)stringWithKideHeightWithCmUnit:(CGFloat)kidHeight
{
    CGFloat fd = fmodf(kidHeight, 1);
    
    if ( fd > 0 )
        return [NSString stringWithFormat:@"%.1fcm", kidHeight, nil];
    else
        return [NSString stringWithFormat:@"%dcm", (int)kidHeight, nil];
}
@end


@implementation NSString (NewLineCount)
-(NSInteger)countNewLineCharater
{
    NSInteger length = [[self componentsSeparatedByCharactersInSet:
                         [NSCharacterSet newlineCharacterSet]] count];
    return length;
}
@end
