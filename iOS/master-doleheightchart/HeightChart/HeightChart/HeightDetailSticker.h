//
//  HeightDetailSticker.h
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 6..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KidHeightDetail.h"
#import "HeightDetailTreeProtocol.h"


@class HeightDetailSticker;
@class TopArrowSticker;
@class ExtendSticker;
@class HeightZoomDetailCell;

@protocol HeightDetailStickerDelegate <NSObject>

-(void)longpressedSticker:(TopArrowSticker*)topSticker;
-(NSUInteger)monkeyNumber;
-(void)clickSticker:(id)sticker HeightDetail:(KidHeightDetail*)kidHeight clickedStickerInfo:(HeightDetailInfo*)detailInfo;
-(BOOL)isDeleteModeRequest;
-(void)doubleTap:(HeightZoomDetailCell*)zoomCell OffSetY:(CGFloat)offSetY;

@end

@interface HeightDetailSticker : UIView{
    UIImageView     *_topPhotoImageView;
    UIImageView     *_checkImageView;

}
@property (nonatomic, weak) UIImage *photo;
@property (nonatomic, weak) NSDate *takenDate;
@property (nonatomic) StickerDirection stickerdirection;
@property (nonatomic, weak) id<HeightDetailStickerDelegate> stickerDelegate;
@property (nonatomic, weak) HeightDetailInfo *detailInfo;
@property (nonatomic) BOOL isDeleteMode;

-(id)initWithFrame:(CGRect)frame photo:(UIImage*)image takenDate:(NSDate*)date stickerDirection:(StickerDirection)direction;

+(CGRect)boundsDetailSticker;

@end

@interface TopArrowSticker : HeightDetailSticker
@property (nonatomic, weak) KidHeightDetail  *heightDetail;
@property (nonatomic, strong) NSMutableArray *extendStickers;

-(id)initWithFrame:(CGRect)frame stickerDirection:(StickerDirection)direction detail:(KidHeightDetail*)kidHeightDetail;
-(void)updateExpandWithAnimated:(BOOL)animated;
-(void)setHeightDetail:(KidHeightDetail *)heightDetail;
-(BOOL)doAddAnimationWithDetailInfo:(HeightDetailInfo*)detailInfo;
-(void)doDeleteAnimationWithDetailInfoArray:(NSArray*)detailInfoArray;
-(void)setStickerChecekd:(BOOL)isChecked;
@end


@interface ExtendSticker : HeightDetailSticker
@property (nonatomic, weak) TopArrowSticker* topArrowSticker;
-(id)initWithFrame:(CGRect)frame photo:(UIImage*)image takenDate:(NSDate*)date stickerDirection:(StickerDirection)direction isChecked:(BOOL)checked;
-(id)initWithFrame:(CGRect)frame heightDetail:(HeightDetailInfo*)detail stickerDirection:(StickerDirection)direction StickerDelegate:(id<HeightDetailStickerDelegate>)stickerDelegate;
@end

@interface ZoomSticker : UIView
@property (nonatomic, weak) id<HeightDetailStickerDelegate> stickerDelegate;
@property (nonatomic) StickerDirection stickerdirection;
@property (nonatomic, weak) HeightDetailInfo *detailInfo;
-(BOOL)doAddAnimationWithDetailInfo:(HeightDetailInfo*)detailInfo;
-(id)initWithFrame:(CGRect)frame takenDate:(NSDate*)date kidHeight:(CGFloat)height stickerDirection:(StickerDirection)direction delegate:(id<HeightDetailStickerDelegate>) stickerDelegate;
@end
