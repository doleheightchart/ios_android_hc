//
//  UIView+ImageUtil.h
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 2. 11..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import <UIKit/UIKit.h>

enum DoleButtonImageType {
    DoleButtonImageTypeOrange,
    DoleButtonImageTypeLightBlue,
    DoleButtonImageTypeRed,
    DoleButtonImageTypePopupOrange,
    DoleButtonImageTypePopupRed,
    DoleButtonImageTypeFacebook,
};


enum DoleCheckBoxImageType {
    DoleCheckBoxImageTypeNormal,
    DoleCheckBoxImageTypeMale,
    DoleCheckBoxImageTypeFemale,
    DoleCheckBoxImageTypeBoy,
    DoleCheckBoxImageTypeGirl,
};

@interface UIImage (ImageUtil)

+(instancetype)resizableImageWithName:(NSString*)imageNamed CapInsets:(UIEdgeInsets)insets;


@end

@interface UIImage (HeightDetail)
-(instancetype)facecropImage;
-(instancetype)facemaskImageWithDirection:(BOOL)isLeft;
+(instancetype)maskedImage:(UIImage*)image make:(UIImage*)maskImage;
+(instancetype)stickerImageFromPhoto:(UIImage*)photoImage Direction:(BOOL)isLeft;
+(instancetype)stickerExtendImageFromPhoto:(UIImage*)photoImage Direction:(BOOL)isLeft;
+(instancetype)summaryFaceImage:(UIImage*)photoImage;

+(instancetype)imageName:(NSString*)stickerImageName kidHeight:(CGFloat)kidHeight leftTop:(CGPoint)leftTop;
@end

@interface UIButton (ImageUtil)

-(void)setResizableImageWithType:(enum DoleButtonImageType)buttonType;

-(void)setResizableCheckBoxImageWithType:(enum DoleCheckBoxImageType)checkboxType;

-(void)makeUnderlineStyleWithSelectedColor:(UIColor*)selectedColor normalColor:(UIColor*)normalColor;
-(void)makeDefaultUnderlineStyle;

+(UIButton*) commonCancelButtonWithTarget:(id)target action:(SEL)selector;
+(UIButton*) cameraCancelButtonWithTarget:(id)target action:(SEL)selector;

@end

@interface UILabel (ImageUtil)
+(UILabel*) commonDefalutLabel;
+(UILabel*) commonBoldLabel;
@end

@interface UIImageView (ImageUtil)

-(void)setResizableImageWithName:(NSString*)imageNamed;

@end

@interface UIViewController (ImageUtil)
//-(UIButton*)addCommonCloseButton;
@end







