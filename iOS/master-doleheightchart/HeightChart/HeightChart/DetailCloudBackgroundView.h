//
//  CloudBackgroundView.h
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 7..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailCloudBackgroundView : UIView

@property (nonatomic) BOOL isDeleteMode;

-(void)startScrolling;
-(void)loadAnimals;
-(void)startMovingAnimation;
-(void)stopAll;
@end
