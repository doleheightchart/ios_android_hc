//
//  HeightChartProtocolList.h
//  HeightChart
//
//  Created by ne on 2014. 2. 16..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//



#import "ChildHeight.h"

//@protocol clickMonkeyDelegate <NSObject>
//
////-(void)clickMonkeyWithData:(MonkeyTree*) tree;
//
//@end

@protocol ContentSetPositionDelegate <NSObject>

-(void)contentSetPosition:(CGPoint)pos;

@end

@protocol HomeLayerDataSource <NSObject>

//-(NSUInteger)monkeyCount;
//-(TreeInfo *)monkeyTreeInfoAtIndex:(NSUInteger)index;
//-(void)addMonkeyTreeInfoData:(TreeInfo*)treeInfo completion:(void(^)(BOOL isSuccess))completion;
//-(void)deleteMonekyTreeInfoAtIndexList:(NSArray*)indexList completion:(void(^)(BOOL isSuccess))completion;

@end

//@protocol MonkeyHomeListDelegate<NSObject>
//
//-(void)clickMonkeyAtIndex:(NSUInteger)index;
//-(void)longPressedAtIndex:(NSUInteger)index;
//-(void)clickAddNewNickname;
//
//@end

@protocol HeightViewerDataSource <NSObject>

-(NSUInteger)hightImageCount;
-(ChildHeight*)hightViewerInfoAtIndex:(NSUInteger)index;


@end


