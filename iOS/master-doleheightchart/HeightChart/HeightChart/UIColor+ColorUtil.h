//
//  UIColor+ColorUtil.h
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 2. 24..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (ColorUtil)

+(instancetype)colorWithRGB:(int32_t)rgbValue;
+(instancetype)colorWithRGB:(int32_t)rgbaValue alpha:(CGFloat)alpha;

@end

@interface UIColor (Sticker)
+(instancetype)colorWithMonkeyNumber:(NSUInteger)monkeyNumber;
@end

@interface UIColor (NameTag)
+(instancetype)colorWithMonkeyNumber:(NSUInteger)monkeyNumber normalState:(BOOL)isNormal;
@end
