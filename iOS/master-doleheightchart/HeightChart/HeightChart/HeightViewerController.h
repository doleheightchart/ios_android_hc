//
//  HeightViewerController.h
//  HeightChart
//
//  Created by ne on 2014. 3. 12..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HeightChartProtocolList.h"
#import "HeightDetailTreeProtocol.h"
#import "FacebookManager.h"
@interface HeightViewerController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate ,FacebookManagerDelegate >

@property (nonatomic) NSInteger startIndex;

@property (nonatomic, copy) void (^completion)(BOOL isConfirmMode, HeightViewerController *controller);

@property (nonatomic, weak) id<HeightDetailDataSource> datasource;
@property (nonatomic, weak) id<HeightViewerDelgate> hightViewerDelegate;
@property (nonatomic, retain) MyChild *mychild;
@property (nonatomic) BOOL isConfirmMode;

- (void)reloadPageViewWithIndex:(NSInteger)index;

@end
