//
//  ChooseNicknameBox.m
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 4. 8..
//  Copyright (c) 2014년 Kim Sehyun. All rights reserved.
//

#import "ChooseNicknameBox.h"
#import "UIColor+ColorUtil.h"
#import "UIFont+DoleHeightChart.h"
#import "UIView+ImageUtil.h"
#import "UIView+Animation.h"
#import "MyChild.h"

@interface NewNicknameCell : UITableViewCell
@end
@implementation NewNicknameCell
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        UIImageView *iconView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"popup_add_btn"]];
        iconView.frame = CGRectMake(26/2, 8/2, 76/2, 76/2);
        [self addSubview:iconView];
        
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake((26+76+22)/2, (92-62)/2/2, 394/2, 62/2)];
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = NSTextAlignmentLeft;
        label.textColor = [UIColor colorWithRGB:0x787878];
        label.font = [UIFont defaultRegularFontWithSize:34/2];
        label.text = NSLocalizedString(@"Add a nickname", @"");
        [self addSubview:label];
    }
    return self;
}
@end

@interface NicknameCell : UITableViewCell
@property (nonatomic, retain) UILabel *nicknameLabel;
@property (nonatomic, retain) UIImageView *iconView;
@end
@implementation NicknameCell
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.iconView = [[UIImageView alloc]initWithFrame:CGRectMake(26/2, 8/2, 76/2, 76/2)];
        [self addSubview:self.iconView];
        
        self.nicknameLabel = [[UILabel alloc]initWithFrame:CGRectMake((26+76+22)/2, (92-62)/2/2, 394/2, 62/2)];
        self.nicknameLabel.backgroundColor = [UIColor clearColor];
        self.nicknameLabel.textAlignment = NSTextAlignmentLeft;
        self.nicknameLabel.textColor = [UIColor colorWithRGB:0x787878];
        self.nicknameLabel.font = [UIFont defaultRegularFontWithSize:34/2];
        //label.text = NSLocalizedString(@"New nickname", @"");
        [self addSubview:self.nicknameLabel];
    }
    return self;
}
@end


@interface ChooseNicknameBox(){
    UIView *_popupContainerView;
    UIImageView *_titleImgView;
    UIImageView *_bottomImgView;
    UIImageView *_bodyImgView;
    UIButton *_cancelButton;
    UITableView *_tableView;
}

@end

@implementation ChooseNicknameBox

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)createPopup
{
    self.lockedView.userInteractionEnabled = NO;
    
    self.backgroundColor = [UIColor colorWithRGB:0x000000 alpha:0.6];
    
    NSInteger count = self.fetchedResultController.fetchedObjects.count;
    
    NSInteger c = MIN(count + 1, 3);
    
    CGFloat h = (92 * c)/2 + 84/2 + 90/2;
    CGFloat w = 544/2;
    
    CGFloat left = (self.bounds.size.width - w) / 2;
    CGFloat top = (self.bounds.size.height - h) / 2;
    
    CGRect framePopup = CGRectMake(left, top, w, h);
    
    _popupContainerView = [[UIView alloc]initWithFrame:framePopup];

    
    
    CGRect frameTitle = CGRectMake(0, 0, 544/2, 84/2);
    _titleImgView = [[UIImageView alloc]initWithFrame:frameTitle];
    [_popupContainerView addSubview:_titleImgView];
    
//    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(28/2, 42/2/2, 42/2, 492/2)];
    
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(28/2, 42/2/2, 492/2, 42/2)];

    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textColor = [UIColor colorWithRGB:0xffffff];
    titleLabel.font = [UIFont defaultBoldFontWithSize:37/2];
    titleLabel.textAlignment = NSTextAlignmentLeft;
    titleLabel.text = NSLocalizedString(@"Select a nickname", @"");
//    titleLabel.text = @"Choose nickname";
    [_popupContainerView addSubview:titleLabel];
    
    CGRect frameTable = CGRectMake(0, 84/2, 544/2, (92*c)/2);
    _tableView = [[UITableView alloc]initWithFrame:frameTable style:UITableViewStylePlain];
    [_popupContainerView addSubview:_tableView];
    
    [_tableView registerClass:[NewNicknameCell class] forCellReuseIdentifier:@"NewNicknameCell"];
    [_tableView registerClass:[NicknameCell class] forCellReuseIdentifier:@"NicknameCell"];
    _tableView.rowHeight = 92/2;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    _tableView.delegate = self;
    _tableView.dataSource = self;
    
    CGRect frameBottom = CGRectMake(0, frameTable.origin.y + frameTable.size.height, 544/2, 90/2-1);
    _bottomImgView = [[UIImageView alloc]initWithFrame:frameBottom];
    [_popupContainerView addSubview:_bottomImgView];
    
    _cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _cancelButton.frame = CGRectMake(10/2, frameBottom.origin.y + 10/2, 524/2, 70/2);
    [_popupContainerView addSubview:_cancelButton];
    
    UIEdgeInsets topBarInsets = UIEdgeInsetsMake(0, (22.0/2.0), 0, (22.0/2.0));
    UIEdgeInsets bottomBarInsets = UIEdgeInsetsMake(0, (22.0/2.0), 0, (22.0/2.0));
    
    _titleImgView.image = [UIImage resizableImageWithName:@"popup_top"
                                                  CapInsets:topBarInsets];
    _bottomImgView.image = [UIImage resizableImageWithName:@"poup_bottom"
                                                     CapInsets:bottomBarInsets];
    
    [_cancelButton setResizableImageWithType:DoleButtonImageTypePopupOrange];
    [_cancelButton setTitle:NSLocalizedString(@"Cancel", @"") forState:UIControlStateNormal];
    [_cancelButton setTitleColor:[UIColor colorWithRGB:0xffffff] forState:UIControlStateNormal];
    _cancelButton.titleLabel.font = [UIFont defaultBoldFontWithSize:34/2];
    [_cancelButton addTarget:self action:@selector(clickCancel:) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:_popupContainerView];
    
    [_popupContainerView animateScaleEffectWithDuration:0.3];
}

-(void)clickCancel:(UIButton*)button
{
    [self closePopup];
}

-(void)closePopup
{
    self.lockedView.userInteractionEnabled = YES;
    
    [self removeFromSuperview];
}

+(ChooseNicknameBox*)createOnView:(UIView*)view FetchController:(NSFetchedResultsController*)fetchController;
{
    CGRect f = view.bounds;
    ChooseNicknameBox *box = [[ChooseNicknameBox alloc]initWithFrame:f];
    box.fetchedResultController = fetchController;
    box.lockedView = view;
    [view.superview addSubview:box];
    [box createPopup];
    
    return box;
}

+(ChooseNicknameBox*)popupOnView:(UIView*)view
                 FetchController:(NSFetchedResultsController*)fetchController
                      completion:(void(^)(NSInteger selectedIndex))completion
{
    CGRect f = view.bounds;
    ChooseNicknameBox *box = [[ChooseNicknameBox alloc]initWithFrame:f];
    box.fetchedResultController = fetchController;
    box.lockedView = view;
    box.completion = completion;
    [view.superview addSubview:box];
    [box createPopup];
    
    return box;
}

#pragma mark TableView DataSource & Delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.fetchedResultController.fetchedObjects.count + 1; // 1 is Add Nickname Cell
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0){
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NewNicknameCell" forIndexPath:indexPath];
        return cell;
    }
    
    MyChild *myChild = self.fetchedResultController.fetchedObjects[indexPath.row-1];
    
    NicknameCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NicknameCell" forIndexPath:indexPath];
    
    NSString *charaterImgName = [NSString stringWithFormat:@"popup_monkey_%02d", myChild.charterNo.shortValue, nil];
    UIImage *characterIcon = [UIImage imageNamed:charaterImgName];
    cell.iconView.image = characterIcon;
    cell.nicknameLabel.text = myChild.nickname;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.completion(indexPath.row - 1);
    
    [self closePopup];

}

@end
