//
//  UIButton+CheckAndRadio.h
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 10..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (CheckAndRadio)
-(void)makeUICheckBox;
-(void)makeUICheckBoxWhite;
-(void)makeUIRadioBox;
-(void)bindinRadioBox:(UIButton*)otherButton;
+(void)makeUIRadioButtons:(UIButton*)firstButton secondButton:(UIButton*)secondButton;
-(void)setupSizeWhenPressed;
-(void)inflateBounds;
@end
