//
//  ChildHeight.h
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 13..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class MyChild;

@interface ChildHeight : NSManagedObject

@property (nonatomic, retain) NSDate * createdDate;
@property (nonatomic, retain) NSNumber * height;
@property (nonatomic, retain) UIImage* photo;
@property (nonatomic, retain) NSString * qrcode;
@property (nonatomic, retain) NSDate * takenDate;
@property (nonatomic, retain) UIImage* thumbphoto;
@property (nonatomic, retain) MyChild *parent;
@property (nonatomic, retain) NSNumber *stickerType;

//@property (nonatomic, readonly) UIImage *faceCropImage;

@end
