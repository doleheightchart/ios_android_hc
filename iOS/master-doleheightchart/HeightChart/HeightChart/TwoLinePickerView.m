//
//  TwoLinePickerView.m
//  TestProvision
//
//  Created by Kim Sehyun on 2014. 2. 24..
//  Copyright (c) 2014년 Kim Sehyun. All rights reserved.
//

#import "TwoLinePickerView.h"
#import <CoreGraphics/CoreGraphics.h>
#import "UIColor+ColorUtil.h"

#define kNumberOfRowsOnView 3


@interface PickerLayoutAttributes : UICollectionViewLayoutAttributes
//@property (nonatomic, strong) CAGradientLayer *gradient;
@property (nonatomic, assign) BOOL isCenterCell;
@end

@implementation PickerLayoutAttributes

- (id)copyWithZone:(NSZone *)zone {
    
    PickerLayoutAttributes *attributes = [super copyWithZone:zone];
    attributes.isCenterCell = _isCenterCell;
//    attributes.gradient = _gradient;
    return attributes;
    
}

- (BOOL) isEqual:(id)object {
    BOOL equal = [super isEqual:object];
    if (!equal)
        return NO;
    
    //return [self.gradient isEqual:[(SSDatePickerLayoutAttributes*)object gradient]];
    
    //return YES;
    
    return self.isCenterCell == [(PickerLayoutAttributes*)object isCenterCell];
}

@end


@interface PickerLayout : UICollectionViewFlowLayout

@end

@implementation PickerLayout


- (void)prepareLayout {
    
    self.scrollDirection = UICollectionViewScrollDirectionVertical;
    self.minimumInteritemSpacing = 0;
    self.minimumLineSpacing = 0;
    self.itemSize = CGSizeMake(self.collectionView.frame.size.width, ((CGFloat)(self.collectionView.frame.size.height)/kNumberOfRowsOnView));
    
//    CGFloat h = (CGFloat)self.collectionView.frame.size.height/2.0f - (CGFloat)self.itemSize.height/2.0f;
//    self.sectionInset = UIEdgeInsetsMake(h, 0, h, 0);
    
    self.sectionInset = UIEdgeInsetsMake(kTwoLinePickerViewCellHeight, 0, kTwoLinePickerViewCellHeight, 0);
    
}

- (NSArray*)layoutAttributesForElementsInRect:(CGRect)rect {
    NSMutableArray *array = [NSMutableArray arrayWithArray:[super layoutAttributesForElementsInRect:rect]];
    
    for (PickerLayoutAttributes *attr in array) {
        [self applyAttributes:attr];
    }
    
    return array;
}

- (UICollectionViewLayoutAttributes*)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath {
    PickerLayoutAttributes *attributes = (PickerLayoutAttributes*)[super layoutAttributesForItemAtIndexPath:indexPath];
    
    
    NSInteger page = roundf(self.collectionView.contentOffset.y / self.itemSize.height);
    attributes.isCenterCell = page == indexPath.row;
    
    [self applyAttributes:attributes];
    return attributes;
}

- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds {
    return YES;
}

- (CGPoint)targetContentOffsetForProposedContentOffset:(CGPoint)proposedContentOffset withScrollingVelocity:(CGPoint)velocity {
    
    
    NSLog(@"velocity is x=%.1f, y=%.1f", velocity.x, velocity.y);
    
    if (fabsf(velocity.y) > 0){

  
        return [super targetContentOffsetForProposedContentOffset:proposedContentOffset withScrollingVelocity:velocity];

    }
    
    NSInteger page = roundf(self.collectionView.contentOffset.y / self.itemSize.height);
    return CGPointMake(proposedContentOffset.x, page * self.itemSize.height);
    
}

- (void) applyAttributes:(PickerLayoutAttributes*)attributes {
    
    CGRect visibleRect;
    
    visibleRect.origin = self.collectionView.contentOffset;
    visibleRect.size = self.collectionView.frame.size;
    
    CGFloat distance = CGRectGetMidY(visibleRect) - attributes.center.y;
    if (distance < self.itemSize.height/2 && distance > -(self.itemSize.height/2)) {
        attributes.isCenterCell = YES;
        return;
    }
    
    attributes.isCenterCell = NO;
    

    
}

+(Class) layoutAttributesClass {
    
    return [PickerLayoutAttributes class];
    
}

@end



@implementation PickerViewCell

- (void)initialization {
    self.backgroundColor = [UIColor clearColor];
    self.textLabel = [[UILabel alloc] initWithFrame:(CGRect){CGPointZero, self.frame.size}];
//    self.textLabel.font = [UIFont italicSystemFontOfSize:50];
    self.textLabel.textColor = [UIColor blueColor];
    self.textLabel.backgroundColor = [UIColor clearColor];
    self.textLabel.textAlignment = NSTextAlignmentCenter;
    self.textLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self addSubview:self.textLabel];
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initialization];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialization];
    }
    return self;
}

- (void) applyLayoutAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes {
    [super applyLayoutAttributes:layoutAttributes];
    
    PickerLayoutAttributes *attributes = (PickerLayoutAttributes *)layoutAttributes;
    
    if (attributes.isCenterCell){
        self.textLabel.textColor = [UIColor colorWithRGB:0x606060];
    }
    else {
        self.textLabel.textColor = [UIColor colorWithRGB:0xC2C2C2];
    }
    
//    NSLog(@"apply layout attribute by other in cell");
}

@end


@implementation TwoLinePickerView

#define kRegisterCellId @"MyPickerCell"

- (id)initWithFrame:(CGRect)frame collectionViewLayout:(UICollectionViewLayout *)layout {
    self = [super initWithFrame:frame collectionViewLayout:layout];
    if (self) {
        self.showsVerticalScrollIndicator = NO;
        self.alwaysBounceVertical = NO;
        self.bounces = NO;
        [self registerClass:[PickerViewCell class] forCellWithReuseIdentifier:kRegisterCellId];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self){
        self.showsVerticalScrollIndicator = NO;
        self.alwaysBounceVertical = NO;
        self.bounces = NO;
        [self registerClass:[PickerViewCell class] forCellWithReuseIdentifier:kRegisterCellId]; 
    }
    return self;
}

- (NSIndexPath*)currentSelectedIndexPath {
    
    CGSize itemSize = [(PickerLayout*)self.collectionViewLayout itemSize];
    NSInteger row = round(self.contentOffset.y / itemSize.height);
    return [NSIndexPath indexPathForRow:row inSection:0];
    
}


-(void)addTwoLineLayerWithColor:(UIColor*)color;
{
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    shapeLayer.strokeColor = color.CGColor;
    shapeLayer.lineWidth = 2;
    shapeLayer.frame = self.frame;
    
    
    CGMutablePathRef path = CGPathCreateMutable();
    
    CGPathMoveToPoint(path, NULL, 0, self.bounds.size.height / 3);
    CGPathAddLineToPoint(path, NULL, self.bounds.size.width, self.bounds.size.height / 3);
    
    CGPathMoveToPoint(path, NULL, 0, (self.bounds.size.height / 3) * 2);
    CGPathAddLineToPoint(path, NULL, self.bounds.size.width, (self.bounds.size.height / 3) * 2);
    
    shapeLayer.path = path;
    CGPathRelease(path);
    [self.superview.layer addSublayer:shapeLayer];
}

-(PickerViewCell*)dequeueReusableCellForIndexPath:(NSIndexPath*)indexPath;
{
    PickerViewCell *cell = [self dequeueReusableCellWithReuseIdentifier:kRegisterCellId forIndexPath:indexPath];
    
    return cell;
}

+(instancetype)pickerviewWithFrame:(CGRect)frame
{
    PickerLayout *layout = [[PickerLayout alloc] init];
    TwoLinePickerView *pickerView = [[TwoLinePickerView alloc] initWithFrame:frame collectionViewLayout:layout];
    
    return pickerView;
}

//- (void) scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
//{
//    NSLog(@"-->>> scrollViewDidEndDragging is decelerate is %@", decelerate ? @"YES" : @"NO");
//    
//    
//    if (decelerate == YES) return;
//    
//    NSInteger offSetIndex = (scrollView.contentOffset.y) / kCollectionCellHeight;
//    CGFloat moveToY = (offSetIndex * kCollectionCellHeight);
//    
//    [UIView animateWithDuration:0.3
//                     animations:^{
//                         CGPoint point = CGPointMake(0, moveToY);
//                         scrollView.contentOffset = point;
//                     }
//                     completion:^(BOOL finished) {
//                         
//                     }
//     ];
//}
//
//- (void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView
//{
//    NSLog(@"scrollViewDidEndDecelerating");
//    
//    NSInteger offSetIndex = (scrollView.contentOffset.y) / kCollectionCellHeight;
//    offSetIndex /= 5;
//    offSetIndex *= 5;
//    CGFloat moveToY = (offSetIndex * kCollectionCellHeight);
//    
//    [UIView animateWithDuration:0.3
//                     animations:^{
//                         CGPoint point = CGPointMake(0, moveToY);
//                         scrollView.contentOffset = point;
//                     }
//                     completion:^(BOOL finished) {
//                         
//                     }
//     ];
//}


@end
