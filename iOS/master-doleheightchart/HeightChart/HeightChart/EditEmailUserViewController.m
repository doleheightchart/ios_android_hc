//
//  EditEmailUserViewController.m
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 23..
//  Copyright (c) 2014년 Kim Sehyun. All rights reserved.
//

#import "EditEmailUserViewController.h"
#import "UIView+ImageUtil.h"
#import "TextViewDualPlaceHolder.h"
#import "UIButton+CheckAndRadio.h"
#import "UIViewController+DoleHeightChart.h"
#import "UIFont+DoleHeightChart.h"
#import "UIColor+ColorUtil.h"
#import "ToastController.h"
#import "BirthPopupController.h"
#import "WarningCoverView.h"
#import "UIViewController+PolyServer.h"
#import "InputChecker.h"
#import "CityPopupController.h"

@interface EditEmailUserViewController (){
    NSMutableData   *_receivedData;
    InputChecker    *_inputChecker;
    
    //Server User Info
    NSInteger       _birthYearOnServer;
    NSString        *_cityOnServer;
    enum GenderType _genderOnServer;
    
    TextViewDualPlaceHolder *_txtViewCurrentPassword;
}

@property (weak, nonatomic) IBOutlet TextViewDualPlaceHolder *txtViewPassword;
@property (weak, nonatomic) IBOutlet TextViewDualPlaceHolder *txtViewConfirmPassword;
@property (weak, nonatomic) IBOutlet TextViewDualPlaceHolder *txtViewBirth;
@property (weak, nonatomic) IBOutlet TextViewDualPlaceHolder *txtViewCity;

@property (weak, nonatomic) IBOutlet UIButton *btnCheckMale;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckFemale;
@property (weak, nonatomic) IBOutlet UILabel *lblEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblMale;
@property (weak, nonatomic) IBOutlet UILabel *lblFemale;

@property (nonatomic, weak) IBOutlet UIButton *btnSave;


- (IBAction)clickSave:(id)sender;


@end

@implementation EditEmailUserViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _txtViewCurrentPassword = [[TextViewDualPlaceHolder alloc] initWithFrame:CGRectMake(28, 148, 264, 35)];
    [self.view addSubview:_txtViewCurrentPassword];
    
    self.lblEmail.text = self.userConfig.email;
    self.lblEmail.font = [UIFont defaultBoldFontWithSize:32/2];
    self.lblEmail.textColor = [UIColor colorWithRGB:0x606060];
    
    [self.txtViewPassword setDualPlaceHolderTextView:DoleDualPlaceHolderTextTypePassword];
    //TODO::영어 버전일때 , 플레이스 홀더 2줄로 변경.
    
    if ([self.currentLanguageCode isEqualToString:@"en"]) {
        //        [self.txtViewPassword.textField setReDrawPlaceHolder:NSLocalizedString(@"Change the password", @"")
        //                                                  SecondText:NSLocalizedString(@"(at least 6 characters)", @"")
        //        ];

        [self.txtViewPassword.textField changeFontSize:27/2];
        
        [self.txtViewPassword.textField setReDrawPlaceHolder:NSLocalizedString(@"Change the password", @"")
                                               SecondTopText:@"at least 6" SecondBottomText:@"characters"];
        
    }
    else{
        [self.txtViewPassword.textField setReDrawPlaceHolder:NSLocalizedString(@"Change the password", @"")
                                                  SecondText:NSLocalizedString(@"(at least 6 characters)", @"")
        ];
    }
    
    [_txtViewCurrentPassword setDualPlaceHolderTextView:DoleDualPlaceHolderTextTypePassword];
    [_txtViewCurrentPassword.textField setReDrawPlaceHolder:NSLocalizedString(@"Current password", @"") SecondText:NSLocalizedString(@"", @"")];
    
    [self.txtViewConfirmPassword setDualPlaceHolderTextView:DoleDualPlaceHolderTextTypePassword];
    [self.txtViewConfirmPassword.textField setReDrawPlaceHolder:NSLocalizedString(@"Confirm password", @"") SecondText:NSLocalizedString(@"", @"")];
    self.txtViewConfirmPassword.passwordOriginalView = self.txtViewPassword;
    
    [self.txtViewBirth setDualPlaceHolderTextView:DoleDualPlaceHolderTextTypeBirth];
    self.txtViewBirth.textField.delegate = self;
    [self.txtViewCity setDualPlaceHolderTextView:DoleDualPlaceHolderTextTypeCity];
    self.txtViewCity.textField.delegate = self;
    [self addCommonCloseButton];
    
    [UIButton makeUIRadioButtons:self.btnCheckMale secondButton:self.btnCheckFemale];
//    self.btnCheckMale.selected = YES;
    
    self.lblMale.font = [UIFont defaultRegularFontWithSize:34/2];
    self.lblMale.textColor = [UIColor colorWithRGB:0x606060];
    
    self.lblFemale.font = [UIFont defaultRegularFontWithSize:34/2];
    self.lblFemale.textColor = [UIColor colorWithRGB:0x606060];
    
    self.txtViewPassword.textField.secureTextEntry = YES;
    self.txtViewConfirmPassword.textField.secureTextEntry = YES;
    _txtViewCurrentPassword.textField.secureTextEntry = YES;
    
    [self.btnSave setResizableImageWithType:DoleButtonImageTypeOrange];

    
    [self setupControlFlexiblePosition];
    

    [self setEnableInputControls:NO];
    
    _inputChecker = [[InputChecker alloc]init];
    [_inputChecker addHostViewRecursive:self.view submitButton:self.btnSave checkBoxButtons:nil];
    _inputChecker.ignorPasswordTextOrg = self.txtViewPassword.textField;
    _inputChecker.ignorPasswordTextConfirm = self.txtViewConfirmPassword.textField;
    _inputChecker.ignorCurPasswordText = _txtViewCurrentPassword.textField;
    
    self.btnSave.enabled = NO;
}

- (void)setEnableInputControls:(BOOL)enable
{
    self.btnSave.enabled = enable;
    self.txtViewPassword.textField.enabled = enable;
    self.txtViewConfirmPassword.textField.enabled = enable;
    self.txtViewBirth.textField.enabled = enable;
    self.btnCheckMale.enabled = enable;
    self.btnCheckFemale.enabled = enable;
    _txtViewCurrentPassword.textField.enabled = enable;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)viewDidAppear:(BOOL)animated
{
    [self checkNetwork];
}

-(void)setupControlFlexiblePosition
{
    CGFloat DefalutMargin = 236;
    
    if ([UIScreen isFourInchiScreen]) {
        [self moveToPositionYPixel:(DefalutMargin) Control:self.lblEmail];
        [self moveToPositionYPixel:(DefalutMargin + 36 + 30) Control:_txtViewCurrentPassword];
        [self moveToPositionYPixel:(DefalutMargin + 36 + 30 + 72 + 22) Control:self.txtViewPassword];
        [self moveToPositionYPixel:(DefalutMargin + 36 + 30 + 72 + 22 + 72 + 22 ) Control:self.txtViewConfirmPassword];
        [self moveToPositionYPixel:(DefalutMargin + 36 + 30 + 72 + 22 + 72 + 22 + 72 + 22) Control:self.txtViewBirth];
        [self moveToPositionYPixel:(DefalutMargin + 36 + 30 + 72 + 22 + 72 + 22 + 72 + 22 + 72 + 22) Control:self.txtViewCity];
        [self moveToPositionYPixel:(DefalutMargin + 36 + 30 + 72 + 22 + 72 + 22 + 72 + 22 + 72 + 22 + 72 + 38) Control:self.btnCheckMale];
        [self moveToPositionYPixel:(DefalutMargin + 36 + 30 + 72 + 22 + 72 + 22 + 72 + 22 + 72 + 22 + 72 + 38) Control:self.btnCheckFemale];
        [self moveToPositionYPixel:(DefalutMargin + 36 + 30 + 72 + 22 + 72 + 22 + 72 + 22 + 72 + 22 + 72 + 38 + 56 + 150) Control:self.btnSave];
    }
    else{
        
        DefalutMargin = 118;
        [self moveToPositionYPixel:(DefalutMargin) Control:self.lblEmail];
        [self moveToPositionYPixel:(DefalutMargin + 36 + 30) Control:_txtViewCurrentPassword];
        [self moveToPositionYPixel:(DefalutMargin + 36 + 30 + 72 + 22) Control:self.txtViewPassword];
        [self moveToPositionYPixel:(DefalutMargin + 36 + 30 + 72 + 22 + 72 + 22 ) Control:self.txtViewConfirmPassword];
        [self moveToPositionYPixel:(DefalutMargin + 36 + 30 + 72 + 22 + 72 + 22 + 72 + 22) Control:self.txtViewBirth];
        [self moveToPositionYPixel:(DefalutMargin + 36 + 30 + 72 + 22 + 72 + 22 + 72 + 22 + 72 + 22) Control:self.txtViewCity];
        [self moveToPositionYPixel:(DefalutMargin + 36 + 30 + 72 + 22 + 72 + 22 + 72 + 22 + 72 + 22 + 72 + 38) Control:self.btnCheckMale];
        [self moveToPositionYPixel:(DefalutMargin + 36 + 30 + 72 + 22 + 72 + 22 + 72 + 22 + 72 + 22 + 72 + 38) Control:self.btnCheckFemale];
        [self moveToPositionYPixel:(DefalutMargin + 36 + 30 + 72 + 22 + 72 + 22 + 72 + 22 + 72 + 22 + 72 + 38 + 51 + 97) Control:self.btnSave];
    }
    
    [self moveToPositionY:self.btnCheckMale.frame.origin.y Control:self.lblMale];
    [self moveToPositionY:self.btnCheckFemale.frame.origin.y Control:self.lblFemale];
}

- (void)checkNetwork
{
    if (NO == self.isEnableNetwork){
        

//        [self setEnableInputControls:NO];
//        
//        [ToastController showToastWithMessage:
//         NSLocalizedString(@"The network is unstable. \nPlease try again after checking the network", @"")  duration:kToastMessageDurationType_Server_Long];
    }
    else{
        [self fillUserData];
        
    }
}

-(void)showBirthPopup
{
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Popup" bundle:nil];
    BirthPopupController *birthdayController = [story instantiateViewControllerWithIdentifier:@"BirthPopup"];
    birthdayController.birthYear = [self.txtViewBirth.textField.text integerValue];
    birthdayController.completion = ^(BirthPopupController *controller){
        //        self.txtViewBirth.textField.text = [NSString stringWithFormat:@"%lu", (unsigned long)controller.birthYear, nil];
        NSString *text = [NSString stringWithFormat:@"%lu", (unsigned long)controller.birthYear, nil];
        [self.txtViewBirth setText:text];
        
        [controller releaseModal];
    };
    [birthdayController showModalOnTheViewController:nil];
}

-(void)showCityPopup
{
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Popup" bundle:nil];
    CityPopupController *cityPopup = [story instantiateViewControllerWithIdentifier:@"City"];
    //    cityPopup.citynames = @[@"Seoul", @"Daegu", @"Daejun", @"Pusan", @"Sejong", @"Suwon", @"Gawngju"];
    cityPopup.citynames = self.userConfig.cachedCitynames;
    cityPopup.selectedCityname = self.txtViewCity.textField.text;
    cityPopup.completion = ^(CityPopupController *controller){
        //        self.txtViewCity.textField.text = controller.selectedCityname;
        NSString *text = controller.selectedCityname;
        [self.txtViewCity setText:text];
        
        [controller releaseModal];
    };
    [cityPopup showModalOnTheViewController:nil];
}

-(void)fillUserInfoWithServerBirthYear:(NSInteger)birthYear City:(NSString*)city Gender:(enum GenderType)gender
{
    _birthYearOnServer = birthYear;
    _cityOnServer = city;
    _genderOnServer = gender;
    
    NSString *birthYearText = [NSString stringWithFormat:@"%lu", (long)birthYear, nil];
    
    [self.txtViewBirth setText:birthYearText];
    if (gender == kGenderTypeMale){
        self.btnCheckMale.selected = YES;
    }
    else{
        self.btnCheckFemale.selected = YES;
    }
    
    [self.txtViewCity setText:city];
}

#pragma mark TextDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField        // return NO to disallow editing.
{
    if (textField != self.txtViewBirth.textField &&
        textField != self.txtViewCity.textField ) return YES;
    
    //[self.txtViewBirth.textField resignFirstResponder];
    [self.view.findFirstResponder resignFirstResponder];
    
    if (textField == self.txtViewBirth.textField){
//        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Popup" bundle:nil];
//        BirthPopupController *birthdayController = [story instantiateViewControllerWithIdentifier:@"BirthPopup"];
//        birthdayController.completion = ^(BirthPopupController *controller){
//            NSString *text = [NSString stringWithFormat:@"%ld", (unsigned long)controller.birthYear, nil];
//            [self.txtViewBirth setText:text];
//    //        self.txtViewBirth.textField.text = [NSString stringWithFormat:@"%ld", (unsigned long)controller.birthYear, nil];
//            [controller releaseModal];
//        };
//        [birthdayController showModalOnTheViewController:nil];
        
        [self showBirthPopup];
    }
    else if (textField == self.txtViewCity.textField){
        [self showCityPopup];
    }
    
    return NO;
}

//- (void)textFieldDidBeginEditing:(UITextField *)textField;           // became first responder
//- (BOOL)textFieldShouldEndEditing:(UITextField *)textField;          // return YES to allow editing to stop and to resign first responder status. NO to disallow the editing session to end
//- (void)textFieldDidEndEditing:(UITextField *)textField;

#pragma mark Action

- (BOOL)isValidInput
{
    if (self.txtViewPassword.textField.text.length > 0 ||
        self.txtViewConfirmPassword.textField.text.length > 0){
        
        if ([self.txtViewPassword checkIsValidPassword] == NO) return NO;
        if ([self.txtViewConfirmPassword checkIsValidConfirmPasswordWithPassword:self.txtViewPassword.textField.text] == NO) return NO;
    }
    
    return YES;
}

- (IBAction)clickSave:(id)sender
{
    [self.view.findFirstResponder resignFirstResponder];
    
    if ([self enableActionWithCurrentNetwork] == NO)return;
    
    if ([self isValidInput]){
        
        [self doModifyUserInfo];
        
    }
}

- (void)doModifyUserInfo
{
    NSString *curPassword = _txtViewCurrentPassword.textField.text;
    NSString *newPassword = self.txtViewPassword.textField.text;
    
    NSString *city = self.txtViewCity.textField.text;
    NSInteger birthYear = [self.txtViewBirth.textField.text integerValue];
    enum GenderType genType = self.btnCheckMale.selected ? kGenderTypeMale : kGenderTypeFemale;
    
    BOOL changed = NO;
    BOOL willChangePassword = NO;
    
    if (newPassword.length > 0) willChangePassword = TRUE;
    if (willChangePassword && NO == [self.userConfig.password isEqualToString:newPassword]) changed = TRUE;
    if (NO == [city isEqualToString:_cityOnServer]) changed = YES;
    if (birthYear != _birthYearOnServer) changed = YES;
    if (genType != _genderOnServer) changed = YES;
    
    if (NO == changed){
        [self closeViewControllerAnimated:YES];
        return;
    }
    
    if (willChangePassword){
        [self requestEditUserInfoWithUserNo:self.userConfig.userNo CurPassword:curPassword NewPassword:newPassword BirthYear:birthYear City:city Gender:genType];
    }
    else{
        [self requestEditUserInfoWithUserNo:self.userConfig.userNo CurPassword:self.userConfig.password NewPassword:self.userConfig.password BirthYear:birthYear City:city Gender:genType];
    }
}



#pragma mark Server

- (void)fillUserData
{
    [self requestUserInfoWithUserNo:self.userConfig.userNo AuthKey:self.userConfig.authKey];
}

- (NSMutableData*)receivedBufferData
{
    if (_receivedData == nil){
        _receivedData = [NSMutableData dataWithCapacity:0];
    }
    
    return _receivedData;
}

-(void)didCompleteRecevedWithResultType:(DoleServerResultType)resultType ParsedData:(NSDictionary*)recevedData ParseError:(NSError*)error
{
    if (recevedData && error==nil && [recevedData[@"Return"] boolValue] ){
        
        if (resultType == kDoleServerResultTypeUserInfo){
        
            NSString *birthday = recevedData[@"BirthDay"];
            if (!birthday || [birthday isKindOfClass:[NSNull class]])
            {
                birthday = @"2015-01-01";
            }
            NSNumber *gender = recevedData[@"Gender"];
            if (!gender || [gender isKindOfClass:[NSNull class]])
            {
                gender = [NSNumber numberWithInteger:1];
            }
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"yyyy-MM-dd"];
            NSDate *birthdate = [formatter dateFromString:birthday];
            
            NSCalendar *calendar = [[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];
            NSDateComponents *comp = [calendar components:NSCalendarUnitYear fromDate:birthdate];
            NSInteger year = [comp year];
            
            
            NSString *city = recevedData[@"AddressMain"];
            
            if (!city || [city isKindOfClass:[NSNull class]])
            {
                city = @"";
            }
            
            [self fillUserInfoWithServerBirthYear:year City:city Gender:(enum GenderType)[gender integerValue]];
            
            if (self.userConfig.cachedCitynames == nil){
                NSDictionary *englishCountryNamesCities = [self englishCountryNamesAndCities];
                NSDictionary *countryCodesAndEnglishNames = [self countriesCodesAndEnglishCommonNames];
                NSString *countryCode = self.currentCountryCode;
                NSString *englishCountryName = [countryCodesAndEnglishNames objectForKey:countryCode];
                NSArray *sortedArray = [englishCountryNamesCities objectForKey:englishCountryName];// sortedArrayUsingSelector:@selector(compare:)];
                self.userConfig.cachedCitynames = sortedArray;
                //[self requestCitynamesWithCountryCode:self.currentCountryCode];
            }
            
            [self setEnableInputControls:YES];

        }
        else if (resultType == kDoleServerResultTypeEditUserInfo){
            
            if (self.txtViewPassword.textField.text.length > 0){ //패스워드도 변경함
                [self.userConfig modifyEmailUserPassword:self.txtViewPassword.textField.text];
            }
            
            /*
            [ToastController showToastWithMessage:NSLocalizedString(@"It has been changed.", @"")
              duration:3 completion:^{
                [self closeViewControllerAnimated:YES];
            }];
            */
            
            [self.navigationController popViewControllerAnimated:YES];
            [ToastController showToastWithMessage:NSLocalizedString(@"It has been changed.", @"") duration:kToastMessageDurationType_Auto];
            
        }
        else if (resultType == kDoleServerResultTypeGetCityNames){
            NSArray *citynames = recevedData[@"Provinces"];
            self.userConfig.cachedCitynames = citynames;
            [self setEnableInputControls:YES];
        }
    }
    else{
        
        NSInteger errorCode = [recevedData[@"ReturnCode"] integerValue];
        if ([self queryErrorCode:errorCode EmailView:nil PasswordView:_txtViewCurrentPassword]) return;
        
        [self toastNetworkErrorWithErrorCode:errorCode];
    }
}



@end
