//
//  UserConfig.h
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 2. 17..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>

enum UserLogInStatus {
    kUserLogInStatusOffLine = 0,
    kUserLogInStatusFacebook = 1,
    kUserLogInStatusDoleLogOn = 2,
};

enum AccountStatus {
    kNotSingUp = 0,
    kSignUpByEmail = 1,
    kSignUpByFacebook = 2,
};

@interface UserConfig : NSObject

@property (nonatomic, assign) enum UserLogInStatus loginStatus;
//@property (nonatomic, retain) NSDate *lastSync;
@property (nonatomic, retain) NSString *email;
@property (nonatomic, retain) NSString *facebookID;
@property (nonatomic, retain) NSString *password;
@property (nonatomic) BOOL usedFirstFreeSticker;

@property (nonatomic, readonly) NSString *authKey;
@property (nonatomic, readonly) NSInteger userNo;
@property (nonatomic) enum AccountStatus accountStatus;

//@property (nonatomic) BOOL firstTimeLaunch;
@property (nonatomic, retain) NSArray *cachedCitynames;

@property (nonatomic) NSUInteger doleCoin;

@property (nonatomic) BOOL backgroundMusicOn;

//@property (nonatomic, retain) NSNumber *lastInputHeight;


@property (nonatomic) BOOL isNeedGuideAddNickname;
@property (nonatomic) BOOL isNeedGuideDetail;
@property (nonatomic) BOOL isNeedGuideInputHeight;
@property (nonatomic) BOOL isNeedGuideCameraFrame;
@property (nonatomic) BOOL isNeedGuideImportPhoto;
//@property (nonatomic) BOOL isNeedGuideAddnicknamePaper;
@property (nonatomic) BOOL isNeedGuideFirstTimeLogIn;
@property (nonatomic) BOOL isNeedGuideAddHeight;

@property (nonatomic) BOOL useiCloud;
@property (nonatomic, strong) NSDate* lastSyncDataTime;
@property (nonatomic) BOOL needQueryDoleCoin;

@property (nonatomic) BOOL isFirstTimeAppLaunch;

@property (nonatomic) BOOL isHeightChartPaparRecieved;

@property (nonatomic) BOOL isHeightChartPaparRecievedShowBanner;

@property (nonatomic, retain) NSDate *dateEventNotShown;

@property (nonatomic, retain) NSString *deviceToken;
@property (nonatomic, retain) NSData *deviceTokenData;

@property (nonatomic, retain) NSDate *dateGetAuthkey;

+(UserConfig*)sharedConfig;

-(void)loadConfig;

-(void)setAuthKey:(NSString *)authKey;
-(void)setUserNo:(NSInteger)userNo;

-(void)signUpByEmail:(NSString*)email password:(NSString*)password;
-(void)signUpByFacebookEmail:(NSString*)facebookEmail FacebookID:(NSString*)facebookID;

-(void)modifyEmailUserPassword:(NSString*)password;
//-(void)recordLastInputHeight:(CGFloat)inputHeight;

-(void)markFirstLaunch;

-(void)syncCloudData;

-(void)withDraw;

-(void)notShownEventOneDays;

-(BOOL)shownEventBanner;

-(void)saveDoleCoinToLocal;

-(void)canNotUseiCloud;

-(void)takenFreeQRCode;

@end
