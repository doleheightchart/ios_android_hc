#import <AVFoundation/AVFoundation.h>

#define kImageCapturedSuccessfully @"imageCapturedSuccessfully"


typedef void (^CameraFocusImageDef)(bool);
@interface CaptureSessionManager : NSObject {

}

@property (nonatomic, retain) UIImageView *captureImageView;
@property (retain) AVCaptureVideoPreviewLayer *previewLayer;
@property (retain) AVCaptureSession *captureSession;
@property (retain) AVCaptureStillImageOutput *stillImageOutput;
@property (nonatomic, retain) UIImage *stillImage;
@property (nonatomic, retain) AVCaptureDevice *backCamera;


@property(nonatomic, retain) AVAudioPlayer* audioPlayer;
@property(nonatomic, retain) AVAudioRecorder* audioRecoreder;
@property(nonatomic, copy) CameraFocusImageDef onFocus;

- (void)addVideoPreviewLayer;
- (void)addStillImageOutput;
- (void)captureStillImage;
- (void)addVideoInputFrontCamera:(BOOL)front;
- (void)stopCaptureRunning;
- (void)startCaptureRunning;
- (void)closeSession;
//- (void)reOpenSession;

@end
