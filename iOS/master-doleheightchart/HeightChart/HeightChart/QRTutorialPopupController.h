//
//  QRTutorialPopupController.h
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 16..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "PopupController.h"

@interface QRTutorialPopupController : PopupController
@property (nonatomic, copy) void (^completion)(PopupController* popupController);
@end
