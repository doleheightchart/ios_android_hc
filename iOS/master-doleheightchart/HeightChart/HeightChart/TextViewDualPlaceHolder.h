//
//  TextViewDualPlaceHolder.h
//  HeightChart
//
//  Created by ne on 2014. 2. 24..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DualPlaceHolderTextField.h"

enum DoleDualPlaceHolderTextType {
    DoleDualPlaceHolderTextTypeNormal,
    DoleDualPlaceHolderTextTypeEmail,
    DoleDualPlaceHolderTextTypeDisableEmail,
    DoleDualPlaceHolderTextTypeBirth,
    DoleDualPlaceHolderTextTypePassword,
    DoleDualPlaceHolderTextTypeNickName,
    DoleDualPlaceHolderTextTypeTopic,
    DoleDualPlaceHolderTextTypeRecipientName,
    DoleDualPlaceHolderTextTypeAddress,
    DoleDualPlaceHolderTextTypeCity,
    DoleDualPlaceHolderTextTypePhone,

};

@interface TextViewDualPlaceHolder : UIView<UITextFieldDelegate>
@property (nonatomic, retain) UIImageView* frontImageView;
@property (nonatomic, retain) UIImageView* backgroundImageView;
@property (nonatomic, retain) DualPlaceHolderTextField* textField;
@property (nonatomic, copy) NSString *firstText;
@property (nonatomic, copy) NSString *secondText;
@property (nonatomic) NSUInteger maxLength;
@property (nonatomic) enum DoleDualPlaceHolderTextType holderType;
@property (nonatomic, weak) TextViewDualPlaceHolder *passwordOriginalView;
@property (nonatomic) BOOL enableSpecialCharacter;
-(void)setDualPlaceHolderTextView:(enum DoleDualPlaceHolderTextType)textType;
-(BOOL)checkInputByHolderType;
-(void)setText:(NSString*)text;
@end


