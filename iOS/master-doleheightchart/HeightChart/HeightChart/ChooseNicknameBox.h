//
//  ChooseNicknameBox.h
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 4. 8..
//  Copyright (c) 2014년 Kim Sehyun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface ChooseNicknameBox : UIControl<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, retain) NSFetchedResultsController *fetchedResultController;
@property (nonatomic, retain) UIView *lockedView;
@property (nonatomic, copy) void(^completion)(NSInteger selectedIndex);

+(ChooseNicknameBox*)popupOnView:(UIView*)view
                 FetchController:(NSFetchedResultsController*)fetchController
                      completion:(void(^)(NSInteger selectedIndex))completion;

@end
