//
//  UIView+Animation.m
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 2. 11..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "UIView+Animation.h"
#import "UIView+ImageUtil.h"

@implementation UIView (Animation)

-(void)animateScaleEffect
{
//    CAKeyframeAnimation *animation = [CAKeyframeAnimation
//                                      animationWithKeyPath:@"transform"];
//    
//    CATransform3D scale1 = CATransform3DMakeScale(0.5, 0.5, 1);
//    CATransform3D scale2 = CATransform3DMakeScale(1.2, 1.2, 1);
//    CATransform3D scale3 = CATransform3DMakeScale(0.9, 0.9, 1);
//    CATransform3D scale4 = CATransform3DMakeScale(1.0, 1.0, 1);
//    
//    NSArray *frameValues = [NSArray arrayWithObjects:
//                            [NSValue valueWithCATransform3D:scale1],
//                            [NSValue valueWithCATransform3D:scale2],
//                            [NSValue valueWithCATransform3D:scale3],
//                            [NSValue valueWithCATransform3D:scale4],
//                            nil];
//    [animation setValues:frameValues];//Courtesy zoul.fleuron.cz
//    
//    NSArray *frameTimes = [NSArray arrayWithObjects:
//                           [NSNumber numberWithFloat:0.0],
//                           [NSNumber numberWithFloat:0.5],
//                           [NSNumber numberWithFloat:0.9],
//                           [NSNumber numberWithFloat:1.0],
//                           nil];
//    [animation setKeyTimes:frameTimes];
//    
//    animation.fillMode = kCAFillModeForwards;
//    animation.removedOnCompletion = NO;
//    animation.duration = .2;
//
//    [self.layer addAnimation:animation forKey:@"scaleEffect"];
    
    [self animateScaleEffectWithDuration:.2];
}

-(void)animateScaleEffectWithDuration:(CGFloat)duration
{
    CAKeyframeAnimation *animation = [CAKeyframeAnimation
                                      animationWithKeyPath:@"transform"];
    
    CATransform3D scale1 = CATransform3DMakeScale(0.5, 0.5, 1);
    CATransform3D scale2 = CATransform3DMakeScale(1.2, 1.2, 1);
    CATransform3D scale3 = CATransform3DMakeScale(0.9, 0.9, 1);
    CATransform3D scale4 = CATransform3DMakeScale(1.0, 1.0, 1);
    
    NSArray *frameValues = [NSArray arrayWithObjects:
                            [NSValue valueWithCATransform3D:scale1],
                            [NSValue valueWithCATransform3D:scale2],
                            [NSValue valueWithCATransform3D:scale3],
                            [NSValue valueWithCATransform3D:scale4],
                            nil];
    [animation setValues:frameValues];//Courtesy zoul.fleuron.cz
    
    NSArray *frameTimes = [NSArray arrayWithObjects:
                           [NSNumber numberWithFloat:0.0],
                           [NSNumber numberWithFloat:0.5],
                           [NSNumber numberWithFloat:0.9],
                           [NSNumber numberWithFloat:1.0],
                           nil];
    [animation setKeyTimes:frameTimes];
    
    animation.fillMode = kCAFillModeForwards;
    animation.removedOnCompletion = NO;
    animation.duration = duration;
    
    [self.layer addAnimation:animation forKey:@"scaleEffect"];
}

-(void)animateScaleEffectWithDuration:(CGFloat)duration
                             MinScale:(CGFloat)minScale
                             MaxScale:(CGFloat)maxScale
                      completionBlock:(void(^)())block
{
    CAKeyframeAnimation *animation = [CAKeyframeAnimation
                                      animationWithKeyPath:@"transform"];
    
    CATransform3D scale1 = CATransform3DMakeScale(minScale, minScale, 1);
    CATransform3D scale2 = CATransform3DMakeScale(maxScale, maxScale, 1);
    CATransform3D scale3 = CATransform3DMakeScale(maxScale*0.9, maxScale*0.9, 1);
    CATransform3D scale4 = CATransform3DMakeScale(1.0, 1.0, 1);
    
    NSArray *frameValues = [NSArray arrayWithObjects:
                            [NSValue valueWithCATransform3D:scale1],
                            [NSValue valueWithCATransform3D:scale2],
                            [NSValue valueWithCATransform3D:scale3],
                            [NSValue valueWithCATransform3D:scale4],
                            nil];
    [animation setValues:frameValues];//Courtesy zoul.fleuron.cz
    
    NSArray *frameTimes = [NSArray arrayWithObjects:
                           [NSNumber numberWithFloat:0.0],
                           [NSNumber numberWithFloat:0.5],
                           [NSNumber numberWithFloat:0.9],
                           [NSNumber numberWithFloat:1.0],
                           nil];
    [animation setKeyTimes:frameTimes];
    
    animation.fillMode = kCAFillModeForwards;
    animation.removedOnCompletion = NO;
    animation.duration = duration;
    
    
//    [self.layer addAnimation:animation forKey:@"scaleEffect"];
    
    
    [CATransaction begin];
        [CATransaction setCompletionBlock:^{
            if (block)
                block();
        }];
        [self.layer addAnimation:animation forKey:@"scaleEffect"];
    [CATransaction commit];
}


-(CAKeyframeAnimation*)addMoveAnimationWithPosition:(NSArray*)positions
                           duration:(NSTimeInterval)duration
                         completion:(void (^)(BOOL finished))completion
{
    
//    CAKeyframeAnimation *keyframeAni = [CAKeyframeAnimation animationWithKeyPath:@"position"];
//    keyframeAni.values = positions;
//    keyframeAni.duration = duration;
//    keyframeAni.removedOnCompletion = NO;
//    keyframeAni.fillMode = kCAFillModeForwards;
//    
//    // create a CGPath that implements two arcs (a bounce)
//    CGMutablePathRef thePath = CGPathCreateMutable();
//    for (NSValue *pos in positions) {
//        CGPoint pt = [pos CGPointValue];
//        if (pos == positions.firstObject)
//            CGPathMoveToPoint(thePath,NULL,pt.x,pt.y);
//        else{
//            CGPathAddLineToPoint(thePath, NULL, pt.x, pt.y);
//        }
//    }
//    
//    keyframeAni.path = thePath;
//    CGPathRelease(thePath);
//    
//    [CATransaction begin];
//    [CATransaction setCompletionBlock:^{
//        if (completion != nil)
//            completion(YES);
//    }];
//    [self.layer addAnimation:keyframeAni forKey:@"position"];
//    [CATransaction commit];
//    
//    return keyframeAni;
    
    return [self addMoveAnimationWithPosition:positions Delay:0 Repeat:1 FillMode:kCAFillModeForwards RepeatDuration:0 duration:duration completion:completion];
}

-(CAKeyframeAnimation*)addMoveAnimationWithPosition:(NSArray*)positions
                                              Delay:(NSTimeInterval)delay
                                             Repeat:(NSInteger)repeat
                                           FillMode:(NSString*)fillMode
                                     RepeatDuration:(NSTimeInterval)repeatduration
                                           duration:(NSTimeInterval)duration
                                         completion:(void (^)(BOOL finished))completion
{
    CAKeyframeAnimation *keyframeAni = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    keyframeAni.values = positions;
    keyframeAni.duration = duration;
    keyframeAni.removedOnCompletion = NO;
    keyframeAni.fillMode = fillMode;
    keyframeAni.repeatCount = repeat;
    keyframeAni.repeatDuration = repeatduration;
//    keyframeAni.autoreverses = YES;
    keyframeAni.beginTime = CACurrentMediaTime() + delay;
    
    // create a CGPath that implements two arcs (a bounce)
    CGMutablePathRef thePath = CGPathCreateMutable();
    for (NSValue *pos in positions) {
        CGPoint pt = [pos CGPointValue];
        if (pos == positions.firstObject)
            CGPathMoveToPoint(thePath,NULL,pt.x,pt.y);
        else{
            CGPathAddLineToPoint(thePath, NULL, pt.x, pt.y);
        }
    }
    
    keyframeAni.path = thePath;
    CGPathRelease(thePath);
    
    [CATransaction begin];
    [CATransaction setCompletionBlock:^{
        if (completion != nil)
            completion(YES);
    }];
    [self.layer addAnimation:keyframeAni forKey:@"position"];
    [CATransaction commit];
    
    return keyframeAni;

}

@end

@implementation UIImageView (FrameAnimationUtil)




-(void)addFrameImagesWithNameFormat:(NSString*)nameFormat
                         frameCount:(NSUInteger)frameCount
                           duration:(NSTimeInterval)duration
                             repeat:(NSUInteger)repeat
{
//    NSMutableArray *imageFrames = [NSMutableArray array];
//    for (int i = 0; i < frameCount; i++) {
//        NSString *imageName = [NSString stringWithFormat:nameFormat, i, nil];
//        [imageFrames addObject:[UIImage imageNamed:imageName]];
//    }
//    
//    self.animationImages = imageFrames;
//    self.animationRepeatCount = repeat;
//    self.animationDuration = duration;
//    [self startAnimating];
    [self addFrameImagesWithNameFormat:nameFormat frameCount:frameCount duration:duration repeat:repeat animationStart:YES];
}

-(void)addFrameImagesWithNameList:(NSArray*)framelist
                         frameCount:(NSUInteger)frameCount
                           duration:(NSTimeInterval)duration
                             repeat:(NSUInteger)repeat
                     animationStart:(BOOL)start
{
    
    NSMutableArray *imageFrames = [NSMutableArray array];
    for (int i = 0; i < frameCount; i++) {
        NSString *imageName = (NSString*)[framelist objectAtIndex:i];
        
        [imageFrames addObject:[UIImage imageNamed:imageName]];
    }
    
    self.animationImages = imageFrames;
    self.animationRepeatCount = repeat;
    self.animationDuration = duration;
    
    if (start)
        [self startAnimating];
}



-(void)addFrameImagesWithNameFormat:(NSString*)nameFormat
                         frameCount:(NSUInteger)frameCount
                           duration:(NSTimeInterval)duration
                             repeat:(NSUInteger)repeat
                     animationStart:(BOOL)start
{
    NSMutableArray *imageFrames = [NSMutableArray array];
    for (int i = 0; i < frameCount; i++) {
        NSString *imageName = [NSString stringWithFormat:nameFormat, i, nil];
        
        [imageFrames addObject:[UIImage imageNamed:imageName]];
//        [imageFrames addObject:[UIImage imageName:imageName kidHeight:18.8 leftTop:CGPointMake(50, 100)]];
    }
    
    self.animationImages = imageFrames;
    self.animationRepeatCount = repeat;
    self.animationDuration = duration;
    
    if (start)
        [self startAnimating];
}


CGPoint racconHeightPosition[25] = {
    { -1, -1 },                      //0
    { -1, -1 },
    { -1, -1 },
    { (422-123-193)/2, (652-14)/2 },
    { (422-123-193)/2, (652-52-35)/2 },
    { (422-123-193)/2, (652-52-107)/2 }, //5
    { (422-123-193)/2, (652-52-170)/2 },
    { (422-123-193)/2, (652-52-170)/2 },
    { (422-123-193)/2, (652-52-170)/2 },
    { (422-123-193)/2, (652-52-170)/2 },
    { (422-123-193)/2, (652-52-170)/2 },      //10
    { (422-123-193)/2, (652-52-170)/2 },
    { (422-123-193)/2, (652-52-170)/2 },
    { (422-123-193)/2, (652-52-170)/2 },
    { (422-123-193)/2, (652-52-170)/2 },
    { (422-123-193)/2, (652-52-170)/2 },        //15
    { (422-123-193)/2, (652-52-170)/2 },
    { (422-123-193)/2, (652-52-170)/2 },
    { (422-123-193)/2, (652-52-170)/2 },
    { (422-123-193)/2, (652-52-170)/2 },
    { (422-123-193)/2, (652-52-170)/2 },        //20
    { (422-123-193)/2, (652-52-170)/2 },
    { (422-123-193)/2, (652-52-170)/2 },        //20
    { (422-123-193)/2, (652-52-170)/2 },
    { (422-123-193)/2, (652-52-170)/2 }        //22
};

CGPoint toucanHeightPosition[24] = {
    { -1, -1 },                                 //0
    { -1, -1 },
    { (422-79)/2, (652-52-337)/2 },
    { (422-123-27)/2, (652-52-363)/2 },
    { (422-123-67)/2, (652-52-351)/2 },                                 //5
    { (422-123-95)/2, (652-52-320)/2 },
    { (422-123-100)/2, (652-52-297)/2 },
    { (422-123-100)/2, (652-52-284)/2 },
    { (422-123-100)/2, (652-52-271)/2 },
    { (422-123-100)/2, (652-52-258)/2 },             //10
    { (422-123-100)/2, (652-52-266)/2 },
    { (422-123-100)/2, (652-52-278)/2 },
    { (422-123-100)/2, (652-52-289)/2 },
    { (422-123-100)/2, (652-52-300)/2 },
    { (422-123-100)/2, (652-52-291)/2 },        //15
    { (422-123-100)/2, (652-52-270)/2 },
    { (422-123-100)/2, (652-52-249)/2 },
    { (422-123-100)/2, (652-52-227)/2 },
    { (422-123-100)/2, (652-52-206)/2 },
    { (422-123-100)/2, (652-52-183)/2 },        //20
    { (422-123-100)/2, (652-52-160)/2 },
    { (422-123-100)/2, (652-52-137)/2 },        //22
    { (422-123-100)/2, (652-52-116)/2 },
    { (422-123-100)/2, (652-52-116)/2 }
};

CGPoint lionHeightPosition[25] = {
    { -1, -1 },                                 //0
    { -1, -1 },
    { -1, -1 },
    { -1, -1 },
    { -1, -1 },
    { -1, -1 },                                 //5
    { -1, -1 },
    { -1, -1 },
    { -1, -1 },
    { -1, -1 },
    { (422-90)/2, (652-52-212)/2 },             //10
    { (422-123)/2, (652-52-212)/2 },
    { (422-123-107)/2, (652-52-212)/2 },
    { (422-123-94)/2, (652-52-212)/2 },
    { (422-123-94)/2, (652-52-212)/2 },
    { (422-123-94)/2, (652-52-212)/2 },        //15
    { (422-123-94)/2, (652-52-212)/2 },
    { (422-123-94)/2, (652-52-212)/2 },
    { (422-123-94)/2, (652-52-212)/2 },
    { (422-123-94)/2, (652-52-212)/2 },
    { (422-123-94)/2, (652-52-212)/2 },        //20
    { (422-123-94)/2, (652-52-212)/2 },
    { (422-123-94)/2, (652-52-212)/2 },        //22
    { (422-123-94)/2, (652-52-212)/2 },
    { (422-123-94)/2, (652-52-212)/2 }        //24
};


CGPoint alligatorHeightPosition[23] = {
    { -1, -1 },                                 //0
    { -1, -1 },
    { (422-20)/2, (652-52-224)/2  },
    { (422-48)/2, (652-52-224)/2  },
    { (422-45)/2, (652-52-224)/2 },
    { (422-62)/2, (652-52-224)/2 },                                 //5
    { (422-79)/2, (652-52-224)/2 },
    { (422-95)/2, (652-52-224)/2},
    { (422-112)/2, (652-52-224)/2 },
    { (422-123-6)/2, (652-52-224)/2 },
    { (422-123-22)/2, (652-52-224)/2 },             //10
    { (422-123-39)/2, (652-52-224)/2 },
    { (422-123-56)/2, (652-52-224)/2 },
    { (422-123-73)/2, (652-52-224)/2 },
    { (422-123-89)/2, (652-52-224)/2 },
    { (422-123-106)/2, (652-52-224)/2 },        //15
    { (422-123-123)/2, (652-52-224)/2 },
    { (422-123-139)/2, (652-52-224)/2 },
    { (422-123-156)/2, (652-52-224)/2 },
    { (422-123-173)/2, (652-52-224)/2 },
    { (422-123-178)/2, (652-52-224)/2 },        //20
    { (422-123-178)/2, (652-52-224)/2 },
    { (422-123-178)/2, (652-52-224)/2 }
};


CGPoint foxHeightPosition[25] = {
    { -1, -1 },                                 //0
    { -1, -1 },
    { -1, -1 },
    { -1, -1 },
    { -1, -1 },
    { -1, -1 },                                 //5
    { -1, -1  }, //화면에는 있으나 글짜가 가려지는 곳이라 아직 나타나지 않는것으로 처리 한다.
    { (422-123-159)/2, (652-52-170)/2},
    { (422-123-159)/2, (652-52-147)/2 },
    { (422-123-159)/2, (652-52-144)/2 },
    { (422-123-159)/2, (652-52-144)/2 },             //10
    { (422-123-159)/2, (652-52-144)/2 },
    { (422-123-159)/2, (652-52-144)/2 },
    { (422-123-159)/2, (652-52-144)/2 },
    { (422-123-159)/2, (652-52-144)/2 },
    { (422-123-159)/2, (652-52-144)/2 },        //15
    { (422-123-159)/2, (652-52-144)/2 },
    { (422-123-159)/2, (652-52-144)/2 },
    { (422-123-159)/2, (652-52-144)/2 },
    { (422-123-159)/2, (652-52-144)/2 },
    { (422-123-159)/2, (652-52-144)/2 },        //20
    { (422-123-159)/2, (652-52-144)/2 },
    { (422-123-159)/2, (652-52-144)/2 },        //22
    { (422-123-159)/2, (652-52-144)/2 },
    { (422-123-159)/2, (652-52-144)/2 }        //24
};

-(void)addHeightViewerAnimalAnimationWithType:(enum DoleStickerType)stickerType KidHeight:(CGFloat)kidHeight
{
    NSArray *imageNameFormats = @[@"01_raccoon_%02d", @"02_toucan_%02d", @"03_lion_%02d", @"04_alligator_%02d", @"05_fox_%02d"];
    
    NSString *format = imageNameFormats[stickerType];
    
    CGPoint *positions[] = { racconHeightPosition, toucanHeightPosition, lionHeightPosition, alligatorHeightPosition, foxHeightPosition };
    
    CGPoint *selectedPositions = positions[stickerType];
    
    NSMutableArray *imageFrames = [NSMutableArray array];
    
    NSArray *frameCountArray = @[
                            [NSNumber numberWithInt:25],
                            [NSNumber numberWithInt:24],
                            [NSNumber numberWithInt:25],
                            [NSNumber numberWithInt:23],
                            [NSNumber numberWithInt:25]];
    
    NSInteger frameCount = [frameCountArray[stickerType] intValue];
    
    for (int i = 0; i < frameCount; i++) {
        NSString *srcImageName = [NSString stringWithFormat:format, i, nil];
        CGPoint pos = selectedPositions[i];
        UIImage *renderedImage = [UIImage imageName:srcImageName kidHeight:kidHeight leftTop:pos];
        [imageFrames addObject:renderedImage];
    }
    
    self.animationImages = imageFrames;
    self.animationRepeatCount = 1;
    self.animationDuration = 2;
}




@end

