//
//  LogOnOptionViewController.m
//  HeightChart
//
//  Created by ne on 2014. 2. 11..
//  Copyright (c) 2014년 ne. All rights reserved.
//

#import "LogOnOptionViewController.h"
#import "UIView+ImageUtil.h"
#import "UIViewController+DoleHeightChart.h"
#import "UIFont+DoleHeightChart.h"
#import "UIColor+ColorUtil.h"
#import "OnOffSwitch.h"
#import "UIButton+CheckAndRadio.h"
#import "PaperNewzealandViewController.h"
#import "MessageBoxController.h"
#import "NSDate+DoleHeightChart.h"
#import "DeleteAccountViewController.h"
#import "UIViewController+PolyServer.h"
#import "NSObject+HeightDatabase.h"

@interface LogOnOptionViewController ()
{
    UIView      *_newzelandpaperBanner;
    UIButton    *_btnWhatIsSync;
}


@property (weak, nonatomic) IBOutlet UILabel *lblEmail;
@property (weak, nonatomic) IBOutlet UIImageView *imageFaceBook;
@property (weak, nonatomic) IBOutlet UIImageView *iconSync;
@property (weak, nonatomic) IBOutlet UILabel *lblSync;

//@property (weak, nonatomic) IBOutlet UIImageView *iconOther;
//@property (weak, nonatomic) IBOutlet UILabel *lblOther;

@property (nonatomic, weak) IBOutlet UIButton *btnEdit;
@property (nonatomic, weak) IBOutlet UIButton *btnAboutService;
@property (nonatomic, weak) IBOutlet UIButton *btnHelp;
@property (weak, nonatomic) IBOutlet UIButton *btnLeaveHeightChart;
@property (weak, nonatomic) IBOutlet UIImageView *bgSky;
@property (weak, nonatomic) IBOutlet UIButton *btnInviteFacebook;

@property (weak, nonatomic) IBOutlet OnOffSwitch *onoffSync;
@property (weak, nonatomic) IBOutlet OnOffSwitch *onoffBackmusic;
@property (weak, nonatomic) IBOutlet UILabel *lblCloudSyncDescription;
@property (weak, nonatomic) IBOutlet UILabel *lblCloudSyncDescriptionDate;
@property (weak, nonatomic) IBOutlet UIImageView *iconBackmusic;
@property (weak, nonatomic) IBOutlet UILabel *lblBackmusic;
@property (weak, nonatomic) IBOutlet UIView *viewCloudDescript;
@property (weak, nonatomic) IBOutlet UIView *viewDetail;



@end

@implementation LogOnOptionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self decorateUI];
    [self addCommonCloseButton];
    [self setupControlFlexiblePosition];
    
//    if (self.userConfig.loginStatus == kUserLogInStatusDoleLogOn){
    if (self.userConfig.accountStatus == kSignUpByEmail){
        self.lblEmail.text = self.userConfig.email;
        self.imageFaceBook.hidden = YES;
    }
//    else if (self.userConfig.loginStatus == kUserLogInStatusFacebook){
    else if (self.userConfig.accountStatus == kSignUpByFacebook){
        self.lblEmail.text = self.userConfig.email;
        self.imageFaceBook.hidden = NO;
    }
    
    
    //    [self showPaperView] ;
//    CGSize textSize = [self.lblEmail.text sizeWithAttributes:@{NSFontAttributeName:[self.lblEmail font]}];
    CGSize textSize = [self.lblEmail.text sizeWithFont:self.lblEmail.font];

    CGRect frame = self.imageFaceBook.frame;
    frame.origin.x = self.view.bounds.size.width/2 - textSize.width/2 - 20;
    self.imageFaceBook.frame = frame;

    [self makeNZPaper];
    
    if (self.userConfig.useiCloud){
        if (self.userConfig.lastSyncDataTime){
            
            NSDateFormatter *formmatter = [[NSDateFormatter alloc] init];
            formmatter.dateFormat = @"yy.MM.dd | HH:mm";

            //"Synchronized with iCloud. (Recent sync: DD.MM.YY | HH:MM)"
            
//            self.lblCloudSyncDescriptionDate.text = [NSString stringWithFormat:@"최근 동기화:%@",
//                                                     [formmatter stringFromDate:self.userConfig.lastSyncDataTime], nil];
            
            self.lblCloudSyncDescriptionDate.text = [NSString stringWithFormat:NSLocalizedString(@"Recent synchronization: %@", @""),
                                                     [formmatter stringFromDate:self.userConfig.lastSyncDataTime], nil];
        }
    }
    else{
        
    }
    
    

}

-(void)viewWillAppear:(BOOL)animated
{
//    if (NO == self.isEnableNetwork){
//        
//        self.btnEdit.enabled = NO;
//        self.btnHelp.enabled = NO;
//        self.onoffSync.enabled = NO;
//        self.btnInviteFacebook.enabled = NO;
//        self.btnLeaveHeightChart.enabled = NO;
//        [self toastNetworkError];
//        
//        return;
//    }
    if ([self.currentCountryCode isEqualToString:@"NZ"]) {

        if (!self.userConfig.isHeightChartPaparRecievedShowBanner) {
            
            [UIView animateWithDuration:1.0 animations:^{
                
                CGRect rect = _newzelandpaperBanner.frame;
                rect.origin.y = self.view.bounds.size.height - (72 + 70)/2;
                
                _newzelandpaperBanner.frame = rect;
                
            } completion:^(BOOL finished){
                
            }];
        }
    }
    

//    [self showOKConfirmMessage:@"Height Chart의 기록 정보를\n iCloud에 저장하여\n관리합니다. 앱 재 설치 시\n유용하게 사용 할 수 있습니다." completion:nil Icon:NO];

}

-(void)decorateUI
{
    
    self.lblEmail.font = [UIFont defaultBoldFontWithSize:32/2];
    self.lblEmail.textColor = [UIColor colorWithRGB:0x606060];
    
    [self.btnEdit setResizableImageWithType:DoleButtonImageTypeOrange];
    [self.btnInviteFacebook setResizableImageWithType:DoleButtonImageTypeFacebook];
    [self.btnAboutService setResizableImageWithType:DoleButtonImageTypeLightBlue];
    [self.btnHelp setResizableImageWithType:DoleButtonImageTypeLightBlue];


    
    //Cloud
    self.lblCloudSyncDescription.font = [UIFont defaultKoreanFontWithSize:25/2];
    self.lblCloudSyncDescription.textColor = [UIColor colorWithRGB:0x606060];
//    [self.btnCloudDescription setResizableImageWithType:DoleButtonImageTypeRed];
//    self.lblCloudSyncDescription.enabled = NO;
//    self.lblCloudSyncDescription.layer.cornerRadius = 2;
//    self.lblCloudSyncDescription.layer.masksToBounds = YES;
    
    self.viewCloudDescript.layer.cornerRadius = 2;
    self.viewCloudDescript.layer.masksToBounds = YES;
    
    self.lblCloudSyncDescriptionDate.font = [UIFont defaultRegularFontWithSize:21/2];
    self.lblCloudSyncDescriptionDate.textColor = [UIColor colorWithRGB:0x909090];;

    self.lblSync.font = [UIFont defaultBoldFontWithSize:30/2];
    self.lblSync.textColor = [UIColor colorWithRGB:0xff8516];
    
    if (self.userConfig.lastSyncDataTime == nil){
        _btnWhatIsSync = [UIButton buttonWithType:UIButtonTypeCustom];
        _btnWhatIsSync.frame = self.lblCloudSyncDescriptionDate.frame;
        [_btnWhatIsSync setTitle:NSLocalizedString(@"What is synchronization?", @"") forState:UIControlStateNormal];
        [_btnWhatIsSync makeDefaultUnderlineStyle];
        _btnWhatIsSync.titleLabel.font = self.lblCloudSyncDescription.font;
        
        [self.viewCloudDescript addSubview:_btnWhatIsSync];
        
        [_btnWhatIsSync addTarget:self action:@selector(clickWhatIsSync:) forControlEvents:UIControlEventTouchUpInside];
        
        self.lblCloudSyncDescriptionDate.hidden = YES;
    }
    

    self.lblBackmusic.font = [UIFont defaultBoldFontWithSize:30/2];
    self.lblBackmusic.textColor = [UIColor colorWithRGB:0xff8516];

//    self.lblOther.font = [UIFont defaultBoldFontWithSize:30/2];
//    self.lblOther.textColor = [UIColor colorWithRGB:0xff8516];

    self.btnLeaveHeightChart.titleLabel.font = [UIFont defaultRegularFontWithSize:28/2];
    [self.btnLeaveHeightChart makeDefaultUnderlineStyle];
    [self.btnLeaveHeightChart addTarget:self action:@selector(clickLeaveHeightchart:) forControlEvents:UIControlEventTouchUpInside];
    
    self.onoffSync.isCheckedOn = self.userConfig.useiCloud;
    [self.onoffSync addTarget:self action:@selector(changedCloud:) forControlEvents:UIControlEventValueChanged];
    
    self.onoffBackmusic.isCheckedOn = self.userConfig.backgroundMusicOn;
    [self.onoffBackmusic addTarget:self action:@selector(musicOnOff:) forControlEvents:UIControlEventValueChanged];
    
    [self.btnEdit addTarget:self action:@selector(clickEdit:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)clickLeaveHeightchart:(id)sender
{
    if ([self enableActionWithCurrentNetwork] == NO)return;
    
    DeleteAccountViewController * dvc = [[DeleteAccountViewController alloc]init];
    dvc.view.frame = self.view.bounds;
    
    [self.navigationController pushViewController:dvc animated:YES];
    
    
}

-(void)clickEdit:(id)sender
{
    if ([self enableActionWithCurrentNetwork] == NO)return;
    
    if (self.userConfig.accountStatus == kSignUpByEmail){
        UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"EditEmailUser"];
        [self.navigationController pushViewController:vc animated:YES];
    }
    else if (self.userConfig.accountStatus == kSignUpByFacebook){
        UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"EditFacebooklUser"];
        [self.navigationController pushViewController:vc animated:YES];
    }
    
}

-(void)clickWhatIsSync:(id)sender
{
    
    NSString *strMessage = NSLocalizedString(@"Information recorded in the\nHeight Chart is saved and\nmanaged in iCloud.\nIt can be effectively used when\nthe application is reinstalled", @"");
    [self showOKConfirmMessage:strMessage completion:nil Icon:NO];
}

-(void)makeNZPaper
{
    
    _newzelandpaperBanner = [[UIView alloc]initWithFrame:CGRectMake((self.view.bounds.size.width - 500/2)/2,
                                                                    //                                                                    self.view.bounds.size.height - (72 + 70)/2,
                                                                    self.view.bounds.size.height + 100,
                                                                    500/2, 72/2)];
    
    _newzelandpaperBanner.backgroundColor = [UIColor colorWithRGB:0x606060];
    _newzelandpaperBanner.layer.opacity = 0.85;
    _newzelandpaperBanner.layer.cornerRadius = 2;
    _newzelandpaperBanner.layer.masksToBounds = YES;
    
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(22/2, 18/2, 400/2, 34/2)];
    label.backgroundColor = [UIColor clearColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont defaultRegularFontWithSize:28/2];
    label.textColor = [UIColor colorWithRGB:0xffffff];
    label.text = @"Receive the Height Chart Paper";
    [_newzelandpaperBanner addSubview:label];
    
    UIButton *closebtn = [ UIButton buttonWithType:UIButtonTypeCustom];
    [closebtn setImage:[UIImage imageNamed:@"popup_banner_x_btn_normal"] forState:UIControlStateNormal];
    [closebtn setImage:[UIImage imageNamed:@"popup_banner_x_btn_press"] forState:UIControlStateHighlighted];
    [closebtn addTarget:self action:@selector(closeBanner) forControlEvents:UIControlEventTouchUpInside];
    closebtn.frame = CGRectMake(_newzelandpaperBanner.frame.size.width - (45 + 14)/2, 14/2, 45/2, 45/2);
    [_newzelandpaperBanner addSubview:closebtn];
    
    [self.view addSubview:_newzelandpaperBanner] ;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(showPaperView)];
    [_newzelandpaperBanner addGestureRecognizer:tap];
    
    
    
}

-(void)showPaperView
{
    PaperNewzealandViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"paperNewzealand"];
    
    UINavigationController *navi = [[UINavigationController alloc]initWithRootViewController:vc];
    navi.navigationBarHidden = YES;
    
    [self modalTransitionController:navi Animated:YES];
    [_newzelandpaperBanner removeFromSuperview];
    
}

-(void)closeBanner
{
    
    
    [UIView animateWithDuration:1.0 animations:^{
        
        CGRect rect = _newzelandpaperBanner.frame;
        rect.origin.y = self.view.bounds.size.height + 150;
        
        _newzelandpaperBanner.frame = rect;
        
    } completion:^(BOOL finished){
        self.userConfig.isHeightChartPaparRecievedShowBanner = YES;
        [_newzelandpaperBanner removeFromSuperview];
        
    }
     ];
}


-(void)musicOnOff:(id)sender
{
    BOOL playBackMusic = self.onoffBackmusic.isCheckedOn;
    
    if (playBackMusic) {
        [self playMusic];
    }
    else {
        [self stopMusic]; 
    }
    
    self.userConfig.backgroundMusicOn = playBackMusic;
}


-(void)changedCloud:(id)sender
{
    if ([self enableActionWithCurrentNetwork] == NO){
        self.onoffSync.isCheckedOn = !self.onoffSync.isCheckedOn;
        return;
    }
    
    if (self.onoffSync.isCheckedOn){
        if ([self canUseiCloud] == FALSE){
            NSString *message = NSLocalizedString(@"Synchronization failed.\nPlease check iCloud setting.", @"");
            [self showOKConfirmMessage:message completion:nil];
            self.onoffSync.isCheckedOn = !self.onoffSync.isCheckedOn;
            return;
            
        }
    }
    
    BOOL sync = self.onoffSync.isCheckedOn;
    
    if (sync){ // 아이클라우드 사용
        
        BOOL firstTimeCloud = self.userConfig.lastSyncDataTime == nil;
        NSString *messageSyncCloud = NSLocalizedString(@"iCloud is used for\nSynchronization.", @"");

        if (self.isCellularNetwork){
            NSString *messageCellularNetworkWarning = NSLocalizedString(@"There may be an additional charge\nwhen access is made through\na mobile communication network.", @"");
            
            [self showOkCancelConfirmMessage:messageCellularNetworkWarning completion:^(BOOL isOK) {
                if (isOK){
                    if (firstTimeCloud){
                        [self showOkCancelConfirmMessage:messageSyncCloud completion:^(BOOL isOK2) {
                            if (isOK2){
                                [self doCloudOn];
                            }
                        }];
                    }
                    else{
                        [self doCloudOn];
                    }
                }
                else{
                    self.onoffSync.isCheckedOn = !sync;
                }
            }];
            
        }
        else{
            if (firstTimeCloud){
                [self showOkCancelConfirmMessage:messageSyncCloud completion:^(BOOL isOK2) {
                    if (isOK2){
                        [self doCloudOn];
                    }
                    else{
                        self.onoffSync.isCheckedOn = !sync;
                    }
                }];
            }
            else{
                [self doCloudOn];
            }
        }
    }
    else{
        
//        NSString *messageReleaseSyncCloud = NSLocalizedString(@"", @"");
//        
//        [self showOkCancelConfirmMessage:@"iCloud를 해제합니다." completion:^(BOOL isOK) {
//            
//            if (isOK){
//                [self doCloudOff];
//            }
//            else{
//                self.onoffSync.isCheckedOn = !sync;
//            }
//            
//        }];
        
        [self doCloudOff];
    }
    
    
}

-(void)doCloudOn
{
    [self addActiveIndicator];
    self.userConfig.useiCloud = YES;
    [self removeActiveIndicator];
}
-(void)doCloudOff
{
    [self addActiveIndicator];
    self.userConfig.useiCloud = NO;
    [self removeActiveIndicator];
}

-(void)setupControlFlexiblePosition
{
//    [self.btnCheck3G4G setHidden:YES];
//    [self.lbl3G4GSupport setHidden:YES];
    
    if ([UIScreen isFourInchiScreen]) {
        
        [self addTitleByBigSizeImage:NO Visible:YES];
        

        [self moveToPositionYPixel:(40 + 168 + 20) Control:self.lblEmail];
        [self moveToPositionYPixel:(40 + 168 + 22) Control:self.imageFaceBook];
        [self moveToPositionYPixel:(40 + 168 + 20 + 36 + 30) Control:self.btnEdit];

        CGFloat bcHeight = 40 + 168 + 20 + 36 + 30 + 74 + 68 ;
        [self changeToHeightPixel:bcHeight Control:self.bgSky];
        [self changeToHeightPixel:1136-bcHeight Control:self.viewDetail];
        [self moveToPositionYPixel:bcHeight Control:self.viewDetail];
        
        
        [self moveToPositionYPixel:(53) Control:self.iconSync];
        [self moveToPositionYPixel:(53) Control:self.lblSync];
        [self moveToPositionYPixel:(40) Control:self.onoffSync];
        
        [self moveToPositionYPixel:(40 + 62 + 18)  Control:self.viewCloudDescript];

        CGFloat backmusicYpos = 53 + 35 + 127;
        
        [self moveToPositionYPixel:(backmusicYpos + 54) Control:self.iconBackmusic];
        [self moveToPositionYPixel:(backmusicYpos + 54) Control:self.lblBackmusic];
        [self moveToPositionYPixel:(backmusicYpos + 41) Control:self.onoffBackmusic];
        
//        [self moveToPositionYPixel:(backmusicYpos + 54 + 35 + 60 ) Control:self.iconOther];
//        [self moveToPositionYPixel:(backmusicYpos + 54 + 35 + 60 ) Control:self.lblOther];
        
        [self moveToPositionYPixel:(backmusicYpos + 54 + 35 + 60 + 35 + 23) Control:self.btnInviteFacebook];
        [self moveToPositionYPixel:(backmusicYpos + 54 + 35 + 60 + 35 + 23 + 74 + 22) Control:self.btnAboutService];
        [self moveToPositionYPixel:(backmusicYpos + 54 + 35 + 60 + 35 + 23 + 74 + 22) Control:self.btnHelp];
        [self moveToPositionYPixel:(backmusicYpos + 54 + 35 + 60 + 35 + 23 + 74 + 22 + 74 + 36) Control:self.btnLeaveHeightChart];
        

        
    }
    else{
        
        [self addTitleByBigSizeImage:NO Visible:NO];
        
        [self moveToPositionYPixel:(68) Control:self.lblEmail];
        [self moveToPositionYPixel:(68 + 2) Control:self.imageFaceBook];
        [self moveToPositionYPixel:(68 + 36 + 30) Control:self.btnEdit];
        
        CGFloat bcHeight = 68 + 36 + 30 + 74 + 52;

        [self changeToHeightPixel:bcHeight Control:self.bgSky];
        [self changeToHeightPixel:960-bcHeight Control:self.viewDetail];
        
        [self moveToPositionYPixel:0 Control:self.bgSky];
        [self moveToPositionYPixel:bcHeight Control:self.viewDetail];
        
        
        [self moveToPositionYPixel:(53) Control:self.iconSync];
        [self moveToPositionYPixel:(53) Control:self.lblSync];
        [self moveToPositionYPixel:(40) Control:self.onoffSync];
        
        [self moveToPositionYPixel:(40 + 62 + 18)  Control:self.viewCloudDescript];
        
        CGFloat backmusicYpos = 53 + 35 + 127;
        
        [self moveToPositionYPixel:(backmusicYpos + 54) Control:self.iconBackmusic];
        [self moveToPositionYPixel:(backmusicYpos + 54) Control:self.lblBackmusic];
        [self moveToPositionYPixel:(backmusicYpos + 41) Control:self.onoffBackmusic];
        
//        [self moveToPositionYPixel:(backmusicYpos + 54 + 35 + 60 ) Control:self.iconOther];
//        [self moveToPositionYPixel:(backmusicYpos + 54 + 35 + 60 ) Control:self.lblOther];
        
        [self moveToPositionYPixel:(backmusicYpos + 54 + 35 + 60 + 35 + 23) Control:self.btnInviteFacebook];
        [self moveToPositionYPixel:(backmusicYpos + 54 + 35 + 60 + 35 + 23 + 74 + 22) Control:self.btnAboutService];
        [self moveToPositionYPixel:(backmusicYpos + 54 + 35 + 60 + 35 + 23 + 74 + 22) Control:self.btnHelp];
        [self moveToPositionYPixel:(backmusicYpos + 54 + 35 + 60 + 35 + 23 + 74 + 22 + 74 + 36) Control:self.btnLeaveHeightChart];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)done:(UIStoryboardSegue *)seg
{
    //LogInOptionEditViewController *sourceVC = seg.sourceViewController;
    
    
    
}

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    NSArray *checkSegueArray = @[@"segueInvite", @"segueHelp"];
    if ([checkSegueArray indexOfObject:identifier] != NSNotFound){
        if ([self enableActionWithCurrentNetwork] == NO){
            return NO;
        }
    }
    
    return YES;
}

@end
