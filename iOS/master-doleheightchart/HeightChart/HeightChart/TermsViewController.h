//
//  TermsViewController.h
//  HeightChart
//
//  Created by ne on 2014. 3. 10..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TermsViewController : UIViewController

@property (nonatomic, copy) void (^completion)(TermsViewController* controller);
@property (nonatomic) BOOL isAgree;
@property (nonatomic) BOOL isFacebookSignUpMode;
@property (nonatomic) BOOL isJustShowMode;

@property (nonatomic) BOOL appFirstTimeLogIn;
@property (nonatomic, copy) void(^requireLoginCompletion)(BOOL isLoggedIn);

@end
