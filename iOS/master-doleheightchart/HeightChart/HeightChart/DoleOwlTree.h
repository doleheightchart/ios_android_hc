//
//  DoleOwlTree.h
//  HeightChart
//
//  Created by ne on 2014. 3. 18..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DoleTree.h"
#import "DoleProtocolList.h"
@interface DoleOwlTree : DoleTree

@property (nonatomic, weak) id<clickOwlDelegate> clickOwlDelegate;
@property (nonatomic) BOOL isDeleteMode;

- (void)changeOwlRectWithMonkeyCount:(NSInteger)count;
- (void)playOwlAnimationWithMonkeyCount:(NSInteger)count;
@end
