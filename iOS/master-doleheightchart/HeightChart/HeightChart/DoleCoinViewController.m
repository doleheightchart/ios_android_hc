//
//  DoleCoinViewController.m
//  HeightChart
//
//  Created by ne on 2014. 3. 24..
//  Copyright (c) 2014년 Kim Sehyun. All rights reserved.
//

#import "DoleCoinViewController.h"
#import "UIView+ImageUtil.h"
#import "TextViewDualPlaceHolder.h"
#import "UIColor+ColorUtil.h"
#import "UIFont+DoleHeightChart.h"
#import "UIViewController+DoleHeightChart.h"
#import "DoleCoinPopupViewController.h"
#import "UIViewController+PolyServer.h"

#import "Mixpanel.h"

@interface DoleCoinViewController (){
    NSMutableData   *_receivedData;
}
@property (weak, nonatomic) IBOutlet UILabel *lblCoin;
@property (weak, nonatomic) IBOutlet UIButton *btnInviteFaceBook;
@property (weak, nonatomic) IBOutlet UIImageView *bg_Topbg;

/////////////////////////////////////////////////////////
@property (weak, nonatomic) IBOutlet UIImageView *imgTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgCoin;
@property (weak, nonatomic) IBOutlet UIImageView *imgX;

/////////////////////////////////////////////////////////
@property (weak, nonatomic) IBOutlet UIImageView *imgTalkbox;
@property (weak, nonatomic) IBOutlet UILabel *lbldolkey1;
@property (weak, nonatomic) IBOutlet UILabel *lblPanzee1;
@property (weak, nonatomic) IBOutlet UILabel *lbldolkey2;
@property (weak, nonatomic) IBOutlet UILabel *lblPanzee2;

@end

@implementation DoleCoinViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.btnInviteFaceBook setResizableImageWithType:DoleButtonImageTypeFacebook];
    
//    CGRect frame =   self.btnInviteFaceBook.titleLabel.frame;
//    frame.origin.x = 74/2;
//    frame.size.width = 528/2 -74/2;
//    self.btnInviteFaceBook.titleLabel.frame = frame;
//    self.btnInviteFaceBook.titleLabel.text = NSLocalizedString(@"Invite Facebook friends", @"");
//    self.btnInviteFaceBook.titleLabel.textAlignment = NSTextAlignmentCenter;
//    self.btnInviteFaceBook.titleLabel.backgroundColor = [UIColor redColor];
    
    
    //TODO::angle 90o / Distance 1px / Size 0px / #0d565b / Opacity 12%
    //self.lblCoin 에 미적용
    self.lblCoin.text = [NSString stringWithFormat:@"%lu", (unsigned long)self.userConfig.doleCoin];
    
    NSInteger fontsize = 120;
   
    if(self.lblCoin.text.length == 3)
    {
        fontsize = 106;
    }
    else if(self.lblCoin.text.length == 4)
    {
        fontsize = 90;
    }
    else if(self.lblCoin.text.length == 5)
    {
        fontsize = 76;
    }
    
    self.lblCoin.textColor = [UIColor colorWithRGB:0xff5411];
    self.lblCoin.font = [UIFont defaultBoldFontWithSize:fontsize/2];
    
    self.lbldolkey1.textColor = [UIColor colorWithRGB:0xff8516];
    self.lbldolkey2.textColor = [UIColor colorWithRGB:0xff8516];
    self.lblPanzee1.textColor = [UIColor colorWithRGB:0x606060];
    self.lblPanzee2.textColor = [UIColor colorWithRGB:0x606060];
    
    NSLog(@"%@", self.lbldolkey1.font.fontName);
    
    if ([self.currentLanguageCode isEqualToString:@"ko"]) {
        self.lbldolkey1.font = [UIFont defaultRegularFontWithSize:30/2];
        self.lbldolkey2.font = [UIFont defaultRegularFontWithSize:30/2];
        self.lblPanzee1.font = [UIFont defaultRegularFontWithSize:26/2];
        self.lblPanzee2.font = [UIFont defaultRegularFontWithSize:26/2];
    }
    else if ([self.currentLanguageCode isEqualToString:@"ja"])
    {
        self.lbldolkey1.font = [UIFont defaultRegularFontWithSize:30/2];
        self.lbldolkey2.font = [UIFont defaultRegularFontWithSize:30/2];
        self.lblPanzee1.font = [UIFont defaultRegularFontWithSize:26/2];
        self.lblPanzee2.font = [UIFont defaultRegularFontWithSize:22/2];
        
//        self.lblPanzee2.numberOfLines = 4;
//        [self changeToHeight:58 Control:self.lblPanzee2];
    }
    else{
        self.lbldolkey1.font = [UIFont defaultRegularFontWithSize:30/2];
        self.lbldolkey2.font = [UIFont defaultRegularFontWithSize:30/2];
        self.lblPanzee1.font = [UIFont defaultRegularFontWithSize:26/2];
        self.lblPanzee2.font = [UIFont defaultRegularFontWithSize:26/2];
    }
    
    NSLog(@"%@", self.lbldolkey1.font.fontName);
    
    
    self.lblPanzee1.text = NSLocalizedString(@"Earn and accumulate Dole Coins\nto win rewards and prizes.", @"");
    
    [self addCommonCloseButton];
    [self setupControlFlexiblePosition];
    
    [self.btnInviteFaceBook addTarget:self action:@selector(clickInviteFacebookFriend:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)viewDidAppear:(BOOL)animated
{
    [self updateCoinLabel];
}

-(void)updateCoinLabel
{
    if (self.userConfig.accountStatus != kNotSingUp){
        
        self.lblCoin.text = [NSString stringWithFormat:@"%lu", (unsigned long)self.userConfig.doleCoin];
        
        NSInteger fontsize = 120;
        
        if(self.lblCoin.text.length == 3)
        {
            fontsize = 106;
        }
        else if(self.lblCoin.text.length == 4)
        {
            fontsize = 90;
        }
        else if(self.lblCoin.text.length == 5)
        {
            fontsize = 76;
        }
        
        self.lblCoin.font = [UIFont defaultBoldFontWithSize:fontsize/2];
    }
    else{
        self.lblCoin.text = @"?";
    }
}

-(void)setupControlFlexiblePosition
{
    
    
    if ([UIScreen isFourInchiScreen]) {
        
        CGFloat defaluMargin = 372;
        
        [self moveToPositionYPixel:defaluMargin + (764 - 74 - 68) Control:self.btnInviteFaceBook];
        
    }
    else{
        
        CGFloat defaluMargin = 80 + 124 + 64;
        
        self.imgTalkbox.image = [UIImage imageNamed:@"dole_coin_question_960"];
        
        [self.imgTitle setHidden:YES];
        
        [self changeToHeightPixel: defaluMargin Control:self.bg_Topbg];
        [self moveToPositionYPixel: 60  Control:self.imgCoin];
        [self moveToPositionYPixel: 80  Control:self.imgX];
        [self moveToPositionYPixel: 80  Control:self.lblCoin];

        [self changeToHeightPixel: 474 Control:self.imgTalkbox];
        [self moveToPositionYPixel:defaluMargin + 34 Control:self.imgTalkbox];

        [self moveToPositionYPixel:defaluMargin + 34 + (20) Control:self.lbldolkey1];
        [self moveToPositionYPixel:defaluMargin + 34 + 80 + 26 + 14 Control:self.lblPanzee1];
        [self moveToPositionYPixel:defaluMargin + 34 + 80 + 26+ 100 + 26 + (20) Control:self.lbldolkey2];
        [self moveToPositionYPixel:defaluMargin + 34 + 80 + 26+ 100 + 26 + 80 + 26 +(10) Control:self.lblPanzee2];
        
        if ([self.currentLanguageCode isEqualToString:@"ja"])
        {
            [self moveToPositionYPixel:defaluMargin + 34 + 80 + 26+ 100 + 26 + 80 + 26 +(6) Control:self.lblPanzee2];
        }
        
        [self moveToPositionYPixel:defaluMargin + (692 - 74 - 68) Control:self.btnInviteFaceBook];
        

        
    }
//    self.lbldolkey1.font = [UIFont defaultRegularFontWithSize:30/2];
//    self.lbldolkey2.font = [UIFont defaultRegularFontWithSize:30/2];
//    self.lblPanzee1.font = [UIFont defaultRegularFontWithSize:26/2];
//    self.lblPanzee2.font = [UIFont defaultRegularFontWithSize:26/2];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
//{
//    NSArray *checkSegueArray = @[@"segueInvite"];
//    if ([checkSegueArray indexOfObject:identifier] != NSNotFound){
//        
//        if ([self enableActionWithCurrentNetwork] == NO){
//            return NO;
//        }
//        
//        if ([identifier isEqualToString:@"segueInvite"]){
//            
//            if (self.userConfig.accountStatus == kNotSingUp){
//                __weak DoleCoinViewController *weakSelf = self;
//                [self presentLogInViewControllerWithCompletion:^(BOOL isLoggedIn) {
//                    if (isLoggedIn){
//                        return TRUE;
//                    }
//                    else{
//                        [weakSelf showDoleCoinViewController];
//                    }
//                }];
//            }
//            else{
//                [self showDoleCoinViewController];
//            }
//            
//        }
//    }
//    
//    return YES;
//}

-(void)clickInviteFacebookFriend:(id)sender
{
    if ([self enableActionWithCurrentNetwork] == NO) return;
    
    if (self.userConfig.accountStatus == kNotSingUp){
        __weak DoleCoinViewController *weakSelf = self;
        [self presentLogInViewControllerOKCancelWithCompletion:^(BOOL isLoggedIn) {
            if (isLoggedIn){
                [weakSelf requestDoleCoinQuery];
                [weakSelf showInviteFacebookFriend];
            }
//            else{
//                [weakSelf showInviteFacebookFriend];
//            }
        }];
    }
    else{
        [self showInviteFacebookFriend];
    }
}

-(void)showInviteFacebookFriend
{
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"InviteFacebook"];
    [self pushTransitionController:vc Animated:YES];
}


- (NSMutableData*)receivedBufferData
{
    if (_receivedData == nil){
        _receivedData = [NSMutableData dataWithCapacity:0];
    }
    
    return _receivedData;
}

- (void)didCompleteRecevedWithResultType:(DoleServerResultType)resultType ParsedData:(NSDictionary *)recevedData ParseError:(NSError *)error
{
    if (!error){
        if ([recevedData[@"Return"] boolValue]){
            
            switch (resultType) {
                case kDoleServerResultTypeGetDoleCoin:[self handleServerQueryDoleCoinWithParsedData:recevedData ParseError:error];
                    break;
                default:
                    assert(0);
                    break;
            }
            
        }
        else{
            NSInteger errorCode = [recevedData[@"ReturnCode"] integerValue];
            [self toastNetworkErrorWithErrorCode:errorCode];
        }
    }
    else{
        [self toastNetworkError];
    }
}

-(void)handleServerQueryDoleCoinWithParsedData:(NSDictionary *)recevedData ParseError:(NSError *)error
{
    self.userConfig.doleCoin = [recevedData[@"EventBalance"] integerValue];
    [self.userConfig saveDoleCoinToLocal];
    
    [self updateCoinLabel];
    
    self.userConfig.needQueryDoleCoin = NO;
    
    //longzhe cui added
    [[Mixpanel sharedInstance].people set:@{@"coins":[NSNumber numberWithInteger:self.userConfig.doleCoin]}];
}


@end
