//
//  HeightDetailTree.h
//  HeightChart
//
//  Created by mytwoface on 2014. 3. 2..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KidHeightDetail.h"
#import "HeightDetailTreeProtocol.h"
#import "HeightDetailSticker.h"
#import "HeightDetailCell.h"

@protocol HeightDetailTreeDataSource <NSObject>

- (KidHeightDetail*) heightDetailOfKidHeight:(CGFloat)kidHeight;
- (int)characterNumber;
- (NSString*)nickname;
- (NSUInteger)currentKidHeight;

@end



@interface HeightDetailTree : UIControl<UICollectionViewDataSource, UICollectionViewDelegate, HeightDetailCellDelegate, HeightDetailStickerDelegate,HeightDetailAddCellDelegate>

@property (nonatomic, weak) id<HeightDetailTreeDataSource> dataSource;
@property (nonatomic, weak) id<HeightDetailTreeDelegate> treeDelegate;
@property (nonatomic) BOOL isZoomMode;
@property (nonatomic) BOOL isDeleteMode;
@property (nonatomic) NSUInteger monkeyTypeNo;
@property (nonatomic) NSInteger maxHeight;
@property (nonatomic, weak) HeightDetailInfo *lastAddedDetailInfo;
@property (nonatomic) CGFloat aveHeight;

-(void)reloadDetails;
-(void)createDoleKey;
-(void)initialize;

-(void)addNewHeightDetailWith:(KidHeightDetail*)kidHeightDetail DetailInfo:(HeightDetailInfo*)detailInfo;
-(void)doDeleteAnimationWithCheckedDetails:(NSArray*)detailsArray;

-(void)scrollToNewHeightDetailWith:(NSInteger)oldMaxHeight DetailInfo:(HeightDetailInfo*)detailInfo;

@end

