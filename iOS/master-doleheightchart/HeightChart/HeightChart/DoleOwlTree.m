//
//  DoleOwlTree.m
//  HeightChart
//
//  Created by ne on 2014. 3. 18..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "DoleInfo.h"
#import "DoleOwlTree.h"
#import "UIView+Animation.h"
#import "UIButton+CheckAndRadio.h"

@interface DoleOwlTree()
{
    UIImageView *_owlView;
    UIButton *_addButton;
    UIImageView *_owlTreeBaseView;
}
@end

@implementation DoleOwlTree

- (id)initWithFrame:(CGRect)frame doleMonkeyType:(enum DoleMonkeyType)type
{
    self = [super initWithFrame:frame doleMonkeyType:type];
    {
        [self createImageView];
    }
    return self;
}
//main_owl_01_00~11.png
//add_nickname_owl_02_00~14.png
//add_nickname_tree.png
//Add_nickname_btn_normal.png Add_nickname_btn_press.png
- (void)createImageView{
    
    CGFloat kOwlTreeWidth = 230.0;
    CGFloat kOwlTreeHeight = 496.0;
    
    CGFloat kScrollMonkeyTreeViewHeight = 650 + 101 + 64 ; // tree Height + Monkey Margin  + Max NameTag Margin
    
    
    UIImage *owlImage = [UIImage imageNamed:@"main_owl_01_00"];
    
    
    CGRect viewRect = self.frame;
    viewRect.size.width = kOwlTreeWidth/2;
    viewRect.size.height = kScrollMonkeyTreeViewHeight/2;
    self.frame = viewRect;

    CGFloat owlTreeOriginY = viewRect.size.height - kOwlTreeHeight/2;
    _owlTreeBaseView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"add_nickname_tree"]];
    _owlTreeBaseView.frame = CGRectMake(0, (kScrollMonkeyTreeViewHeight - kOwlTreeHeight + 110 /2)/2,
                                       kOwlTreeWidth/2, kOwlTreeHeight/2);
    
//    _owlTreeBaseView.frame = CGRectMake(0, kScrollMonkeyTreeViewHeight/2 - _owlTreeBaseView.frame.size.height,
//                                       kOwlTreeWidth/2, kOwlTreeHeight/2);
    
    _owlView = [[UIImageView alloc]initWithImage:owlImage];
    _owlView.frame = [self normalOwlImageRect];

    
    [self addSubview:_owlTreeBaseView];
    [self addSubview:_owlView];
    
    _addButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _addButton.adjustsImageWhenHighlighted = NO;
    _addButton.backgroundColor = [UIColor clearColor];
    [_addButton setBackgroundImage:[UIImage imageNamed:@"Add_nickname_btn_normal"] forState:UIControlStateNormal];
    [_addButton setBackgroundImage:[UIImage imageNamed:@"Add_nickname_btn_press"] forState:UIControlStateHighlighted];

    
    [_addButton setupSizeWhenPressed];
    
    CGFloat buttonSize = 113.0;
    CGFloat buttonHeightMargin = 89 + buttonSize;
    _addButton.frame = CGRectMake( (kOwlTreeWidth/2 - buttonSize/2)/2,
//                                  _owlTreeBaseView.frame.origin.y - buttonHeightMargin/2 - 10, buttonSize/2, buttonSize/2);
                                   _owlTreeBaseView.frame.origin.y - buttonHeightMargin/2 , buttonSize/2, buttonSize/2);
    [_addButton addTarget:self action:@selector(onAddButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_addButton];
    

    _addButton.transform = CGAffineTransformMakeScale(0.0, 0.0);
}

- (CGRect)normalOwlImageRect
{
    CGFloat kMainOwlWidth = 185.0;
    CGFloat kMainOwlHeight = 254.0;

//    NSLog(@"Owl tree Frame y = %.1f", _owlTreeBaseView.frame.origin.y);
    
    //사양서 보고 못마춤
//    return CGRectMake(20/2, _owlTreeBaseView.frame.origin.y - 93/2, kMainOwlWidth/2, kMainOwlHeight/2);
//    return CGRectMake(20/2, _owlTreeBaseView.frame.origin.y - 89/2, kMainOwlWidth/2, kMainOwlHeight/2);
    return CGRectMake(20/2, _owlTreeBaseView.frame.origin.y - 90/2, kMainOwlWidth/2, kMainOwlHeight/2);
}

- (CGRect)nickNameOwlImageRect
{
    CGFloat kNickNameOwlWidth = 431.0;
    CGFloat kNickNameOwlHeight = 282.0;
    
//    return CGRectMake(-30/2, _owlTreeBaseView.frame.origin.y - 148/2, kNickNameOwlWidth/2, kNickNameOwlHeight/2);
//    return CGRectMake(-30/2, _owlTreeBaseView.frame.origin.y - 132/2, kNickNameOwlWidth/2, kNickNameOwlHeight/2);
    return CGRectMake(-30/2, _owlTreeBaseView.frame.origin.y - 136/2, kNickNameOwlWidth/2, kNickNameOwlHeight/2);
}

- (void)changeOwlRectWithMonkeyCount:(NSInteger)count
{
    if (count == 0) {
        _owlView.frame = [self nickNameOwlImageRect];
        _owlView.image =  [UIImage imageNamed:NSLocalizedString(@"add_nickname_owl_02_14", @"")];
        
        
    }
    else{
        _owlView.frame = [self normalOwlImageRect];
        _owlView.image =  [UIImage imageNamed:@"main_owl_01_11"];
    }
    

    
}
- (void)playOwlAnimationWithMonkeyCount:(NSInteger)count
{
    if (count == 0) {
        
        _owlView.frame = [self nickNameOwlImageRect];
        _owlView.image =  [UIImage imageNamed:NSLocalizedString(@"add_nickname_owl_02_14", @"")];
        
        NSArray *frameList = @[@"add_nickname_owl_02_00", @"add_nickname_owl_02_01", @"add_nickname_owl_02_02",
                               @"add_nickname_owl_02_03", @"add_nickname_owl_02_04", @"add_nickname_owl_02_05",
                               @"add_nickname_owl_02_06", @"add_nickname_owl_02_07", @"add_nickname_owl_02_08",
                               @"add_nickname_owl_02_09", @"add_nickname_owl_02_10", @"add_nickname_owl_02_11",
                               NSLocalizedString(@"add_nickname_owl_02_12", @""),
                               NSLocalizedString(@"add_nickname_owl_02_13", @""),
                               NSLocalizedString(@"add_nickname_owl_02_14", @"")];
        
        [_owlView addFrameImagesWithNameList:frameList frameCount:frameList.count duration:2.5 repeat:1 animationStart:YES];
     }
    else
    {
        _owlView.frame = [self normalOwlImageRect];
        _owlView.image =  [UIImage imageNamed:@"main_owl_01_11"];
        
        [_owlView addFrameImagesWithNameFormat:@"main_owl_01_%02d" frameCount:12 duration:2.0 repeat:1 ];
    }
}

-(void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    NSLog(@"Animation DidStop");
}

-(void)onAddButtonClick:(UIButton*)button
{
    [self.clickOwlDelegate clickAddNickName]; 
}

- (void)showTopTagWithAnimation;
{
    
    [UIView animateWithDuration:0 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        _addButton.transform = CGAffineTransformMakeScale(1.0, 1.0);
        
        
        CGRect rect = _addButton.frame;
        
        CGFloat kNickNameOwlWidth = 430.0;
        if (_owlView.frame.size.width >= kNickNameOwlWidth/2) {
            rect.origin.y = _owlView.frame.origin.y - _addButton.frame.size.height + 20;
        }
        else{
            rect.origin.y = _owlView.frame.origin.y - _addButton.frame.size.height;
        }
        
        _addButton.frame = rect;
        
        _addButton.transform = CGAffineTransformMakeScale(0, 0);
        
    } completion:^(BOOL finished) {
        
        [UIView animateWithDuration:0.5 delay:2.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            _addButton.transform = CGAffineTransformMakeScale(1.0, 1.0);
            
            
        } completion:nil ];
        
    }];

    
    


}

- (void)setIsDeleteMode:(BOOL)isDeleteMode
{
    _isDeleteMode = isDeleteMode;
    
    if (_isDeleteMode){
        // change images
        [UIView animateWithDuration:0.5 animations:^{
            self.alpha = 0.0;
        } completion:nil ];
        
    }
    else{
        // change images
        [UIView animateWithDuration:0.5 animations:^{
           self.alpha = 1.0;
        } completion:nil ];

    }
}


@end
