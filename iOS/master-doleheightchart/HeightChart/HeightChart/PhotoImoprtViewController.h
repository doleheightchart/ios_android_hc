//
//  PhotoImoprtViewController.h
//  HeightChart
//
//  Created by ne on 2014. 3. 14..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoImoprtViewController : UIViewController <UIScrollViewDelegate, UIGestureRecognizerDelegate>

@property (nonatomic, retain) UIImage *photo;
@property (nonatomic, copy) void(^OnDoneClick)(UIImage* SaveImage);

@end
