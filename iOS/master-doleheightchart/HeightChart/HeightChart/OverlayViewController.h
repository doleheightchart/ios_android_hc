//
//  OverlayViewController.h
//  OverlayTest
//
//  Created by mytwoface on 2014. 2. 6..
//  Copyright (c) 2014년 mytwoface. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OverlayViewController : UIViewController

-(void)prepareModal;
-(void)releaseModal;
-(void)showModalOnTheViewController:(id)parentViewController;

@end
