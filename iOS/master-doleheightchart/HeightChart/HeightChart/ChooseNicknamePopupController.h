//
//  ChooseNicknamePopupController.h
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 2. 11..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "PopupController.h"

typedef void (^ChooseNicknameHandler)(BOOL, NSString*);

@interface ChooseNicknamePopupController : PopupController<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, copy) ChooseNicknameHandler completionBlock;
@property (nonatomic, copy) void (^selectedCompletionBlock)(NSUInteger index);

@end
