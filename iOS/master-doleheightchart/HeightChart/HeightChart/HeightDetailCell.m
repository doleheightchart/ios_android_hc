//
//  HeightDetailCell.m
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 5..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "HeightDetailCell.h"
#import "UIColor+ColorUtil.h"
#import "UIFont+DoleHeightChart.h"
#import "UIView+ImageUtil.h"

#define kDetailTreeNodeWidth (46/2)
#define kDetailTreeNodeHeight ((43 + 22 + 43)/2)
#define kDetailTreeNodeLabelHeight (22/2)

#define kDetailZoomCellWidth (20/2)
#define kDetailZoomCellHeight (40/2)
#define kDetailZoomCellGapPerCenchi (4/2)
//#define kDetailZoomCellHeightPointWidth (8/2)
//#define kDetailZoomCellHeightPointHeight (10/2)
//#define kDetailZoomCellDeteLabelWidth (66/2)
//#define kDetailZoomCellDateLabelHeight (24/2)
//#define kDetailZoomCellHeightLabelWidth (42/2)
//#define kDetailZoomCellHeightLabelHeight (24/2)
#define kZoomStickerWidth ((96+4+90+4+8+6)/2.0)
#define kZoomStickerHeight (24/2)

#pragma mark HeightDetailCell

@interface HeightDetailCell () {
    UILabel     *_heightLabel;
    UIView      *_averageHeightView;
}

@end

@implementation HeightDetailCell

-(void)addHeightLabel
{
    CGRect labelRect = CGRectMake((self.bounds.size.width/2) - (kDetailTreeNodeWidth/2),
                                  (kDetailTreeNodeHeight/2) - (kDetailTreeNodeLabelHeight/2),
                                  kDetailTreeNodeWidth,
                                  kDetailTreeNodeLabelHeight);
    
    UIImageView *labelBack = [[UIImageView alloc] initWithFrame:labelRect];
    labelBack.image = [UIImage imageNamed:@"detail_tree_cover"];
    [self addSubview:labelBack];
    
    UILabel *label = [[UILabel alloc] initWithFrame:labelRect];
    label.font = [UIFont defaultBoldFontWithSize:10];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor colorWithRGB:0xffb528];
    label.backgroundColor = [UIColor clearColor];
    [self addSubview:label];
    
//    label.text = [NSString stringWithFormat:@"%f", 0, nil];
    
    _heightLabel = label;
}

-(void)addAverageHeightView
{
    CGRect frameLine = CGRectMake(0, self.bounds.size.height/2, self.bounds.size.width, 2);
    _averageHeightView = [[UIView alloc]initWithFrame:frameLine];
    _averageHeightView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"graph_line_02_horizontal"]];
//    [self addSubview:_averageHeightView];
    [self insertSubview:_averageHeightView atIndex:0];
    
//    CGRect frameLabel = CGRectMake(self.bounds.size.width-(182/2), 2, 182/2, 28/2);
//    UIImageView *labelImageView = [[UIImageView alloc]initWithFrame:frameLabel];
//    labelImageView.image = [UIImage imageNamed:@"detail_averageheight"];
//    [_averageHeightView addSubview:labelImageView];

    //이미지에서 라벨로 변경됨 다국어 버전 때문에
    NSString *stringFormat = NSLocalizedString(@"Average height of age %d", @"");
    NSInteger age = [self.detailCellDelegate requestAge];
    NSString *stringText = [NSString stringWithFormat:stringFormat, age, nil];
    CGRect frameLabel = CGRectMake(self.bounds.size.width-(220/2), 2, 216/2, 22/2);
    UILabel *label = [[UILabel alloc]initWithFrame:frameLabel];
    label.font = [UIFont defaultBoldFontWithSize:18/2];
    label.textColor = [UIColor colorWithRGB:0x73bdc3];
    label.textAlignment = NSTextAlignmentRight;
    label.backgroundColor = [UIColor clearColor];
    label.text = stringText;
    [_averageHeightView addSubview:label];
}

-(void)setIsAverageLine:(BOOL)isAverageLine
{
    _isAverageLine = isAverageLine;
    
    if (NO == _isAverageLine){
        [_averageHeightView removeFromSuperview];
        _averageHeightView = nil;
    }
    else if (nil == _averageHeightView){
        [self addAverageHeightView];
    }
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self addHeightLabel];
    }
    return self;
}

-(void)updateWithHeightDetail:(KidHeightDetail*)heightDetail kidHeight:(CGFloat)height;
{
    _heightLabel.text = [NSString stringWithFormat:@"%d", (int)height, nil];
    self.topArrowSticker.heightDetail = heightDetail;
}

-(CGRect)frameLeftSticker
{
    CGFloat centerX = self.bounds.size.width / 2;
//    CGFloat centerYOfHeightLabel = (kDetailTreeNodeHeight/2) - (kDetailTreeNodeLabelHeight/2);

    CGRect stickerBounds = [HeightDetailSticker boundsDetailSticker];
    
    return CGRectMake(centerX - stickerBounds.size.width - (kDetailTreeNodeWidth/2),
                      //centerYOfHeightLabel - stickerBounds.size.height,
                      0,
                      stickerBounds.size.width,
                      stickerBounds.size.height);
}
-(CGRect)frameRightSticker
{
    CGFloat centerX = self.bounds.size.width / 2;
//    CGFloat centerYOfHeightLabel = (kDetailTreeNodeHeight/2) - (kDetailTreeNodeLabelHeight/2);

    CGRect stickerBounds = [HeightDetailSticker boundsDetailSticker];
    
    return CGRectMake(centerX + (kDetailTreeNodeWidth/2),
                      //centerYOfHeightLabel - stickerBounds.size.height,
                      0,
                      stickerBounds.size.width,
                      stickerBounds.size.height);
}

-(void)setAllDelegate:(id)delegate
{
    self.detailCellDelegate = (id<HeightDetailCellDelegate>)delegate;
    self.topArrowSticker.stickerDelegate = (id<HeightDetailStickerDelegate>)delegate;
}

-(BOOL)doAddNewAnimationWithDetailInfo:(HeightDetailInfo*)detailInfo
{
    return [self.topArrowSticker doAddAnimationWithDetailInfo:detailInfo];
}

-(void)doDeleteAnimationWithDetailInfoArray:(NSArray*)details
{
    [self.topArrowSticker doDeleteAnimationWithDetailInfoArray:details];
}

-(void)setIsDeleteMode:(BOOL)isDeleteMode
{
    _isDeleteMode = isDeleteMode;
    
    self.topArrowSticker.isDeleteMode = _isDeleteMode;
}

- (void) applyLayoutAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes {
    [super applyLayoutAttributes:layoutAttributes];
    
    if (NSFoundationVersionNumber <= NSFoundationVersionNumber_iOS_6_1) return;
    
    FadeOutInAttributes *attributes = (FadeOutInAttributes *)layoutAttributes;
    CGFloat limitHeight = attributes.bounds.size.height/2;
    if (attributes.offSetScroll < 20){
        //        self.backgroundColor = [UIColor redColor];
//        self.layer.opacity = (attributes.offSetScroll / limitHeight) + 0.5;;
        self.layer.opacity = attributes.offSetScroll / 20.0 + 0.5;
    }
    else{
        //self.backgroundColor = [UIColor clearColor];
        self.layer.opacity = 1;
    }
}

@end

#pragma mark LeftDetailCell

@implementation LeftDetailCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initDetailCell];
    }
    return self;
}

-(void)initDetailCell
{
    self.topArrowSticker = [[TopArrowSticker alloc] initWithFrame:[self frameLeftSticker]
                                                 stickerDirection:kStickerDirectionLeft
                                                           detail:nil];
    [self addSubview:self.topArrowSticker];

//    self.backgroundColor = [UIColor colorWithRGB:0xff0400 alpha:0.4];
}

@end

#pragma mark RightDetailCell

@implementation RightDetailCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initDetailCell];
    }
    return self;
}

-(void)initDetailCell
{
    self.topArrowSticker = [[TopArrowSticker alloc] initWithFrame:[self frameRightSticker]
                                                 stickerDirection:kStickerDirectionRight
                                                           detail:nil];
    [self addSubview:self.topArrowSticker];

//    self.backgroundColor = [UIColor colorWithRGB:0xff04ff alpha:0.4];

}

@end

#pragma mark AddDetailCell

@interface AddDetailCell (){
    UIButton *_addDetailBtn;
}

@end

@implementation AddDetailCell

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initDetailCell];
    }
    return self;
}

-(void)initDetailCell
{
    
    CGRect frameRuller = CGRectMake(self.bounds.size.width/2 - (46/2/2),
                                    48/2,
                                    46/2,
                                    10/2);
    
    UIImageView *centerRullerImgView = [[UIImageView alloc]initWithFrame:frameRuller];
    centerRullerImgView.image = [UIImage imageNamed:@"detail_tree_cover_add"];
    [self addSubview:centerRullerImgView];
    
    //right_arrow_add_normal
    CGRect frameAddBtn = CGRectMake(self.bounds.size.width/2 + (46/2/2),
                                    0 - (10/2/2),
                                    134/2,
                                    116/2);
    _addDetailBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _addDetailBtn.frame = frameAddBtn;
    [_addDetailBtn setBackgroundImage:[UIImage imageNamed:@"right_arrow_add_normal"] forState:UIControlStateNormal];
    [_addDetailBtn setBackgroundImage:[UIImage imageNamed:@"right_arrow_add_press"] forState:UIControlStateHighlighted];
    [self addSubview:_addDetailBtn];
    
    [_addDetailBtn addTarget:self action:@selector(addHeight:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)addHeight:(id)sender
{
    [self.addCellDelegate addHeight];
}

-(void)updateDeleteMode:(BOOL)isDeleteMode Animated:(BOOL)animated
{
    if (animated){
        [UIView animateWithDuration:0.5 animations:^{
            if (isDeleteMode){
                _addDetailBtn.transform = CGAffineTransformMakeScale(0, 0);
            }
            else{
                _addDetailBtn.transform = CGAffineTransformMakeScale(1, 1);
            }
        }];
    }
    else{
        if (isDeleteMode){
            _addDetailBtn.transform = CGAffineTransformMakeScale(0, 0);
        }
        else{
            _addDetailBtn.transform = CGAffineTransformMakeScale(1, 1);
        }
        
    }
}

- (void) applyLayoutAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes {
    [super applyLayoutAttributes:layoutAttributes];
    
    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_6_1){
        FadeOutInAttributes *attributes = (FadeOutInAttributes *)layoutAttributes;
        CGFloat limitHeight = attributes.bounds.size.height/3;
        if (attributes.offSetScroll < limitHeight){
            //        self.backgroundColor = [UIColor redColor];
            self.layer.opacity = (attributes.offSetScroll / limitHeight)*3 + 0.5;
            //self.layer.opacity = 0.8;
        }
        else{
            //self.backgroundColor = [UIColor clearColor];
            self.layer.opacity = 1;
        }
    }
}

@end

#pragma mark HeightZoomDetailCell

@interface HeightZoomDetailCell(){
    NSMutableArray *_ZoomStickerArray;
    UIView      *_averageHeightView;
    UILabel     *_labelDebug;
}

@end

@implementation HeightZoomDetailCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _ZoomStickerArray = [NSMutableArray array];
        [self addBackRuller];

//        self.backgroundColor = [UIColor colorWithRGB:0x000000 alpha:0.4];
    }
    return self;
}

-(void)debug_setIndex:(NSUInteger)index
{
    _labelDebug.text = [NSString stringWithFormat:@"index is %lu", (unsigned long)index, nil];
}


-(void)addAverageHeightViewWithHeight:(CGFloat)kidHeight;
{
    CGRect frameZoomSticker = [self frameZoomStickerWithHeight:kidHeight];
    
    CGRect frameLine = CGRectMake(-0.5, frameZoomSticker.origin.y + 4, self.bounds.size.width, 2);
    _averageHeightView = [[UIView alloc]initWithFrame:frameLine];
    _averageHeightView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"graph_line_02_horizontal"]];
//    [self addSubview:_averageHeightView];
    [self insertSubview:_averageHeightView atIndex:0];
    
    //self insertSubview:_averageHeightView belowSubview:
    
//    CGRect frameLabel = CGRectMake(self.bounds.size.width-(182/2), 2, 182/2, 28/2);
//    UIImageView *labelImageView = [[UIImageView alloc]initWithFrame:frameLabel];
//    labelImageView.image = [UIImage imageNamed:@"detail_averageheight"];
//    [_averageHeightView addSubview:labelImageView];
//    
    //이미지에서 라벨로 변경됨 다국어 버전 때문에
    NSString *stringFormat = NSLocalizedString(@"Average height of age %d", @"");
    NSInteger age = [self.allDelegate requestAge];
    NSString *stringText = [NSString stringWithFormat:stringFormat, age, nil];
    
    CGRect frameLabel = CGRectMake(self.bounds.size.width-(220/2), 2, 216/2, 22/2);
    UILabel *label = [[UILabel alloc]initWithFrame:frameLabel];
    label.font = [UIFont defaultBoldFontWithSize:18/2];
    label.textColor = [UIColor colorWithRGB:0x73bdc3];
    label.textAlignment = NSTextAlignmentRight;
    label.backgroundColor = [UIColor clearColor];
    label.text = stringText;
    [_averageHeightView addSubview:label];
}

-(void)checkAverageLineWithHeight:(CGFloat)kidHeight;
{
    if (self.kidHeight <= kidHeight && (self.kidHeight+10) > kidHeight){
        [self addAverageHeightViewWithHeight:kidHeight];
    }
    else{

        
        [_averageHeightView removeFromSuperview];
        _averageHeightView = nil;
    }
}

-(CGRect)frameRuller
{
    return CGRectMake(self.bounds.size.width/2 - (kDetailZoomCellWidth/2),
                      self.bounds.size.height - 1.5,
                      kDetailZoomCellWidth,
                      1.5);
}

-(void)addBackRuller
{
    UIImageView *backRuller = [[UIImageView alloc]initWithFrame:[self frameRuller]];
    backRuller.image = [UIImage imageNamed:@"detail_all_tree_cover"];
    [self addSubview:backRuller];

    CGRect r = [self frameRuller];
    r.origin.x += 10;
    r.size.height = 20;
    r.size.width = 100;
    
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                    action:@selector(tapZoomSticker:)];
    tapRecognizer.numberOfTapsRequired = 2;
    [self addGestureRecognizer:tapRecognizer];
}

-(void)tapZoomSticker:(UITapGestureRecognizer*)recognizer
{
    
    if (recognizer.state == UIGestureRecognizerStateEnded) {

        if (_ZoomStickerArray.count == 0){
            CGPoint pt = [recognizer locationInView:self];
            
            [self.allDelegate doubleTap:self OffSetY:(pt.y*10/self.bounds.size.height)];
        }
        else{
            ZoomSticker *sticker = _ZoomStickerArray.firstObject;
            CGFloat offSetY = sticker.detailInfo.kidHeight - self.kidHeight;
            [self.allDelegate doubleTap:self OffSetY:offSetY];
        }
        
    }
    else if (recognizer.state == UIGestureRecognizerStateBegan){
    }
}

-(CGRect)frameZoomStickerWithHeight:(CGFloat)kidHeight
{
//    NSLog(@"frameZoomStickerWithHeight:%.1f", kidHeight);
    int offSet = (int)(kidHeight - self.kidHeight);
    offSet = 10 - offSet;
    CGFloat centerX = self.bounds.size.width / 2;

    CGFloat heightOfCm = 1.5;
    if ([UIScreen mainScreen].bounds.size.height > 500){
        heightOfCm = 2.0;
    }
    
    
    CGRect frame = CGRectMake((offSet%2) == 0? centerX - kZoomStickerWidth - (kDetailZoomCellWidth/2.0) : centerX + (kDetailZoomCellWidth/2.0),
//                              self.bounds.size.height - (offSet*heightOfCm) - (kZoomStickerHeight/2),
                              (offSet*heightOfCm) - (kZoomStickerHeight/2.0),
                              kZoomStickerWidth,
                              kZoomStickerHeight);
    
    return frame;
}

-(void)setAllDelegate:(id)allDelegate
{
    _allDelegate = allDelegate;
}

-(void)updateHeightDetails:(NSArray*)heightDetails kidHeight:(CGFloat)height
{
    for (ZoomSticker* zoomSticker in _ZoomStickerArray) {
        [zoomSticker removeFromSuperview];
    }
    
    [_ZoomStickerArray removeAllObjects];
    
    self.kidHeight = height;
    
    if (heightDetails.count > 0){
        
        for (int i = 0; i < heightDetails.count; i++){
            KidHeightDetail *heightDetail = heightDetails[i];
            HeightDetailInfo *firstTakenData = heightDetail.takenHeightHistory[0];
            CGRect frameZoomSticker = [self frameZoomStickerWithHeight:firstTakenData.kidHeight];
            int heightOffset = (int)firstTakenData.kidHeight;
            StickerDirection stickerDirection = (heightOffset % 2) == 0 ? kStickerDirectionLeft : kStickerDirectionRight;
            ZoomSticker *zoomSticker = [[ZoomSticker alloc]initWithFrame:frameZoomSticker
                                                               takenDate:firstTakenData.takenDate
                                                               kidHeight:firstTakenData.kidHeight
                                                        stickerDirection:stickerDirection
                                                                delegate:self.allDelegate];
            zoomSticker.detailInfo = firstTakenData;
            [_ZoomStickerArray addObject:zoomSticker];
            [self addSubview:zoomSticker];
            zoomSticker.stickerDelegate = self.allDelegate;
        }
    }
    else{


    }
}

-(BOOL)doAddNewAnimationWithDetailInfo:(HeightDetailInfo*)detailInfo
{
    //return [self.topArrowSticker doAddAnimationWithDetailInfo:detailInfo];
    
    for (ZoomSticker *sticker in _ZoomStickerArray) {
        if ([sticker doAddAnimationWithDetailInfo:detailInfo]){
            return YES;
        }
    }
    
    return NO;
}

@end

@interface AddZoomDetailCell (){
    UIButton   *_addDetailBtn;
}

@end

@implementation AddZoomDetailCell

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self){
        [self initDetailCell];
    }
    return self;
}

-(void)initDetailCell
{
    CGRect frameRuller = CGRectMake(self.bounds.size.width/2 - (kDetailZoomCellWidth/2),
                                    self.bounds.size.height - 1.5,
                                    kDetailZoomCellWidth,
                                    1.5);
    
    UIImageView *backRuller = [[UIImageView alloc]initWithFrame:frameRuller];
    backRuller.image = [UIImage imageNamed:@"detail_all_tree_cover"];
    [self addSubview:backRuller];

    NSString *addHeightText = NSLocalizedString(@"Record your height", @"");
    
    CGSize textSize = [addHeightText sizeWithFont:[UIFont defaultBoldFontWithSize:10]];
    CGFloat textWidth = textSize.width + 6;
    textWidth *= 2; //pixel
    
    CGRect frameAddButton = CGRectMake(self.bounds.size.width/2 + (16/2),
                                       0,
                                       (44 + textWidth + 6) / 2,
                                       32/2);
    
    UIImage *normal = [UIImage resizableImageWithName:@"detail_all_btn_add_normal"
                                            CapInsets:UIEdgeInsetsMake(2, 44/2, 2, 6/2)];
    UIImage *press = [UIImage resizableImageWithName:@"detail_all_btn_add_press"
                                           CapInsets:UIEdgeInsetsMake(2, 44/2, 2, 6/2)];
    
    _addDetailBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _addDetailBtn.frame = frameAddButton;
    [_addDetailBtn setBackgroundImage:normal forState:UIControlStateNormal];
    [_addDetailBtn setBackgroundImage:press forState:UIControlStateHighlighted];
    
    [self addSubview:_addDetailBtn];
    
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(44/2, 2, textWidth/2, 24/2)];
    label.text = addHeightText;
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont defaultBoldFontWithSize:10];
    label.textColor = [UIColor colorWithRGB:0xffffff];
    label.backgroundColor = [UIColor clearColor];
    
    [_addDetailBtn addSubview:label];
    [_addDetailBtn addTarget:self action:@selector(addHeight:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)addHeight:(id)sender
{
    [self.addCellDelegate addHeight];
}

-(void)updateDeleteMode:(BOOL)isDeleteMode Animated:(BOOL)animated
{
    if (animated){
        [UIView animateWithDuration:0.5 animations:^{
            if (isDeleteMode){
                _addDetailBtn.transform = CGAffineTransformMakeScale(0, 0);
            }
            else{
                _addDetailBtn.transform = CGAffineTransformMakeScale(1, 1);
            }
        }];
    }
    else{
        if (isDeleteMode){
            _addDetailBtn.transform = CGAffineTransformMakeScale(0, 0);
        }
        else{
            _addDetailBtn.transform = CGAffineTransformMakeScale(1, 1);
        }
        
    }
}

@end

@implementation FadeOutInAttributes

- (id)copyWithZone:(NSZone *)zone {
    
    FadeOutInAttributes *attributes = [super copyWithZone:zone];
    attributes.offSetScroll = _offSetScroll;
    return attributes;
}

- (BOOL) isEqual:(id)object {
    BOOL equal = [super isEqual:object];
    if (!equal)
        return NO;
    
    return self.offSetScroll == [(FadeOutInAttributes*)object offSetScroll];
}

@end

