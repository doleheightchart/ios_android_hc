//
//  BirthdayPopupController.m
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 2. 11..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "BirthdayPopupController.h"
#import "TwoLinePickerView.h"
#import "UIColor+ColorUtil.h"


@interface BirthdayPopupController (){
    TwoLinePickerView *_monthView;
    TwoLinePickerView *_dayView;
    TwoLinePickerView *_yearView;
    
    NSArray *_monthArray;
    NSMutableArray *_dayArray;
    NSArray *_yearArray;
    
    NSInteger _selectedIndexYear;
    NSInteger _selectedIndexMonth;
    NSInteger _selectedIndexDay;
    
    NSDate * _initiateDate;
    NSDate * _dateSet;
}
@property (nonatomic, assign) NSRange yearRange;
- (IBAction)pressSet:(id)sender;

@end

@implementation BirthdayPopupController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    NSDate *date = [NSDate date];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *dateComponents = [calendar components:(NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit|NSTimeZoneCalendarUnit)
                                                   fromDate:date];
    
    NSRange range;
    range.location = dateComponents.year - 99;
    range.length = 100;
    self.yearRange = range;
    
    
    CGRect frameMonth = CGRectMake(19, 42, 66, 137);
    
    _monthView = [TwoLinePickerView pickerviewWithFrame:frameMonth];
    _monthView.backgroundColor = [UIColor whiteColor];
    _monthView.dataSource = self;
    _monthView.delegate = self;
    
    [self.popupContainerView addSubview:_monthView];
    
    UIColor *lineColor = [UIColor colorWithRGB:0x6fd4dd];
    
    [_monthView addTwoLineLayerWithColor:lineColor];
    
    CGRect frameDay = CGRectMake(103, 42, 66, 137);
    
    _dayView = [TwoLinePickerView pickerviewWithFrame:frameDay];
    _dayView.backgroundColor = [UIColor whiteColor];
    _dayView.dataSource = self;
    _dayView.delegate = self;
    
    [self.popupContainerView addSubview:_dayView];
    
    [_dayView addTwoLineLayerWithColor:lineColor];
    
    
    CGRect frameYear = CGRectMake(186, 42, 66, 137);
    
    _yearView = [TwoLinePickerView pickerviewWithFrame:frameYear];
    _yearView.backgroundColor = [UIColor whiteColor];
    _yearView.dataSource = self;
    _yearView.delegate = self;
    
    [self.popupContainerView addSubview:_yearView];
    
    [_yearView addTwoLineLayerWithColor:lineColor];
    
    

//    _monthArray = @[@"Jan",@"Feb",@"Mar",@"Apr",@"May",@"Jun",@"Jul",@"Aug",@"Sep",@"Oct",@"Nov",@"Dec"];
    _monthArray = @[NSLocalizedString(@"Jan", @""),
                    NSLocalizedString(@"Feb", @""),
                    NSLocalizedString(@"Mar", @""),
                    NSLocalizedString(@"Apr", @""),
                    NSLocalizedString(@"May", @""),
                    NSLocalizedString(@"Jun", @""),
                    NSLocalizedString(@"Jul", @""),
                    NSLocalizedString(@"Aug", @""),
                    NSLocalizedString(@"Sep", @""),
                    NSLocalizedString(@"Oct", @""),
                    NSLocalizedString(@"Nov", @""),
                    NSLocalizedString(@"Dec", @"")];
    
    
    self.popupTitleLabel.text = NSLocalizedString(@"Enter Birthday", @"");
//    self.okButton.titleLabel.text = NSLocalizedString(@"OK", @"");
//    self.cancelButton.titleLabel.text = NSLocalizedString(@"Cancel", @"");
    

}

-(void)viewDidAppear:(BOOL)animated
{
    if (self.birthday == nil){
        self.birthday = [NSDate dateWithTimeIntervalSinceNow:0];
    }
    
    if (!_initiateDate) {
        [self setDate:self.birthday animated:NO];
    }
}

//-(void)loadMonthArray
//{
////    01월 - January
////    02월 - Febuary
////    03월 - March
////    04월 - April
////    05월 - May
////    06월 - June
////    07월 - July
////    08월 - August
////    09월 - September
////    10월 - October
////    11월 - November
////    12월 - December
//
//    
//    _monthArray = @[@"Jan",@"Feb",@"Mar",@"Apr",@"May",@"Jun",@"Jul",@"Aug",@"Sep",@"Oct",@"Nov",@"Dec"];
//}

-(void)loadDayArray
{
//    NSCan
//    
//    NSMutableArray *array = [NSMutableArray array];
//    
//    for (int y = 1; y < 31; y++) {
//        [array addObject:[NSNumber numberWithInteger:y]];
//    }
//    
//    [_dayArray removeAllObjects];
//    [_dayArray addObjectsFromArray:array];
}

-(void)loadYearArray
{
//    NSMutableArray *array = [NSMutableArray array];
//    
//    for (int y = self.yearRange.location; y < self.yearRange.location + self.yearRange.length; y++) {
//        [array addObject:[NSNumber numberWithInteger:y]];
//    }
//    
//    _yearArray = array;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSInteger numberOfItems = 0;
    
    if (collectionView == _yearView) {
        
        numberOfItems = self.yearRange.length;
        
    } else if (collectionView == _monthView) {
        
        numberOfItems = 12;
        
    } else if (collectionView == _dayView) {
        
        if (_dateSet) {
            NSRange dayRange = [[NSCalendar currentCalendar] rangeOfUnit:NSDayCalendarUnit inUnit:NSMonthCalendarUnit forDate:_dateSet];
            numberOfItems = dayRange.length;
        } else if (self.myBirthday) {
            NSRange dayRange = [[NSCalendar currentCalendar] rangeOfUnit:NSDayCalendarUnit inUnit:NSMonthCalendarUnit forDate:self.myBirthday];
            numberOfItems = dayRange.length;
        } else {
            numberOfItems = 31;
        }
        
    }
    
    return numberOfItems;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    //    static NSString* cellId = @"MyCell";
    //
    //    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellId forIndexPath:indexPath];
    //
    //    UILabel *label = (UILabel*)[cell viewWithTag:100];
    //
    //    label.text = [NSString stringWithFormat:@"%02ld Index", indexPath.row, nil];
    //
    //    return cell;
    
    TwoLinePickerView *picker = (TwoLinePickerView*)collectionView;
    
    PickerViewCell *cell = [picker dequeueReusableCellForIndexPath:indexPath];
    
//    cell.textLabel.text = [NSString stringWithFormat:@"%02ld",  (long)indexPath.row, nil];
//    //    cell.textLabel.font = [UIFont fontWithName:@"System" size:42];
//    //    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    NSString *text;
    if (collectionView == _yearView){
        text = [NSString stringWithFormat:@"%d", (self.yearRange.location + indexPath.row)];
    }
    else if (collectionView == _monthView){
        text = _monthArray[indexPath.row];
    }
    else if (collectionView == _dayView){
        text = [NSString stringWithFormat:@"%d", (indexPath.row +1)];
    }
    
    cell.textLabel.text = text;
    
    return cell;
    
    
}

- (IBAction)pressSet:(id)sender {
    
    void (^resultCompletion)(NSDate* birthday) = self.completion;
    
    [self releaseModal];
    
    resultCompletion([self myBirthday]);
}

-(void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
{
    self.okButton.enabled = NO;
}
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    self.okButton.enabled = NO;
}

- (void) scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (decelerate == YES) return;
    
    CGFloat heightOfCell = scrollView.bounds.size.height / 3;
    
    NSInteger offSetIndex = roundf((scrollView.contentOffset.y) / heightOfCell);
    CGFloat moveToY = (offSetIndex * heightOfCell);
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         CGPoint point = CGPointMake(0, moveToY);
                         scrollView.contentOffset = point;
                     }
                     completion:^(BOOL finished) {
                          [self endScrollForScrollView:scrollView];
                     }
     ];
}

- (void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGFloat heightOfCell = scrollView.bounds.size.height / 3;
    
    NSInteger offSetIndex = roundf((scrollView.contentOffset.y) / heightOfCell);
    CGFloat moveToY = (offSetIndex * heightOfCell);
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         CGPoint point = CGPointMake(0, moveToY);
                         scrollView.contentOffset = point;
                     }
                     completion:^(BOOL finished) {
                         [self endScrollForScrollView:scrollView];
                     }
     ];
}

- (void) setDate:(NSDate *)date animated:(BOOL)animated
{
    if (!_initiateDate)
        _initiateDate = date;
    
    _dateSet = date;
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *dateComponents = [calendar components:(NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit| NSHourCalendarUnit | NSMinuteCalendarUnit |NSTimeZoneCalendarUnit)
                                                   fromDate:self.birthday];
    
    NSInteger yIndex = dateComponents.year - self.yearRange.location;
    NSInteger currentYearIndex = [_yearView currentSelectedIndexPath].row;
    if (yIndex != currentYearIndex) {
        [_yearView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:yIndex inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredVertically animated:animated];
        [_dayView reloadData];
    }
    
    NSInteger currentMonthIndex = [_monthView currentSelectedIndexPath].row;
    if (dateComponents.month != currentMonthIndex) {
        [_monthView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:dateComponents.month-1 inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredVertically animated:animated];
        [_dayView reloadData];
    }
    
    NSInteger currentDayIndex = [_dayView currentSelectedIndexPath].row;
    if (dateComponents.day != currentDayIndex) {
        [_dayView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:dateComponents.day-1 inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredVertically animated:animated];
    }
    
  _dateSet = nil;

}

- (NSDate*)myBirthday {
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *dComps = [[NSDateComponents alloc] init];
    
    NSInteger currentYearIndex = [_yearView currentSelectedIndexPath].row;
    NSInteger currentMonthIndex = [_monthView currentSelectedIndexPath].row;
    NSInteger currentDayIndex = [_dayView currentSelectedIndexPath].row;
    
    if (currentMonthIndex == 1) {
        NSInteger year = currentYearIndex + self.yearRange.location;
        BOOL isLeapYear = ((year % 100 != 0) && (year % 4 == 0)) || (year % 400 == 0);
        NSInteger days = isLeapYear ? 29 : 28;
        if (currentDayIndex+1 > days) {
            currentDayIndex = days - 1;
        }
    } else if(currentMonthIndex==1 || currentMonthIndex==3 || currentMonthIndex==5 || currentMonthIndex==8 || currentMonthIndex==10) {
        if (currentDayIndex+1 > 30) {
            currentDayIndex = 29;
        }
    }
    
    dComps.year = currentYearIndex + self.yearRange.location;
    dComps.month = currentMonthIndex + 1;
    dComps.day = currentDayIndex + 1;
    dComps.hour = 0;
    dComps.minute = 0;
    
//    NSInteger currentHourIndex = [self.scrollerHour currentSelectedIndexPath].row;
//    NSInteger currentMinIndex = [self.scrollerMinute currentSelectedIndexPath].row;
//    NSInteger currentAPMIndex = [self.scrollerAPM currentSelectedIndexPath].row;
    
    //12-hour clock:12:15 AM --> 24-hour clock:00:15
    //http://en.wikipedia.org/wiki/24-hour_clock
    
//    BOOL isAM = currentAPMIndex == 0;
//    if (currentHourIndex == 11) {
//        dComps.hour = isAM ? 0 : 12;
//    } else {
//        dComps.hour = isAM ? (currentHourIndex + 1) : (currentHourIndex + 13);
//    }
    
//    dComps.minute = currentMinIndex;
    
    dComps.timeZone = [NSTimeZone systemTimeZone];
    
    return [calendar dateFromComponents:dComps];
    
}

- (void) endScrollForScrollView:(UIScrollView*)scrollView {
    if (scrollView == _yearView) {
        [_dayView reloadData];
    } else if (scrollView == _monthView) {
        [_dayView reloadData];
    } else if (scrollView == _dayView) {
        // nothing
        
    }
    
    
//    NSCalendar *calendar = [[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];
//    NSDateComponents *comp = [calendar components:NSCalendarUnitYear|kCFCalendarUnitHour|kCFCalendarUnitDay fromDate:[self myBirthday]];
//    NSInteger days = [comp day];
    
    
    NSTimeInterval interval = [[NSDate date]timeIntervalSinceDate:[self myBirthday]];
    
    self.okButton.enabled = (interval > 0);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [collectionView selectItemAtIndexPath:indexPath animated:NO scrollPosition:UICollectionViewScrollPositionCenteredVertically];
    [self endScrollForScrollView:collectionView];
}

@end
