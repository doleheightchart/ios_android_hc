//
//  UIViewController+PolyServer.m
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 4. 6..
//  Copyright (c) 2014년 Kim Sehyun. All rights reserved.
//

#import "UIViewController+PolyServer.h"
#import "UIViewController+DoleHeightChart.h"
#import "ToastController.h"
#import <ifaddrs.h>
#import <arpa/inet.h>

@implementation UIViewController (PolyServer)

#define kTimeOutRequestSeconds  30
#define kServiceCode            @"SVR002"
#define kSNSType                @"SNS001"
#define kMobileOSType           @"iOS"

#define kUseAlphaServer         0

#if !kUseAlphaServer

#define kRestAPI_LogInURL           @"http://doleserver.com:3000/api/v2/Auth/Api.svc/GetAuthKeyMobile"
#define kRestAPI_SNSLogInURL        @"http://doleserver.com:3000/api/v2/Auth/Api.svc/SNSLogin"
#define kRestAPI_FindPasswordURL    @"http://doleserver.com:3000/api/v2/Profile/Api.svc/FindPassword"
#define kRestAPI_GetUserInfo        @"http://doleserver.com:3000/api/v2/Profile/Api.svc/GetMobileProfile"
#define kRestAPI_EditUserInfo       @"http://doleserver.com:3000/api/v2/Profile/Api.svc/ModifyMobileProfile"
#define kRestAPI_EditSnsUserInfo    @"http://doleserver.com:3000/api/v2/Profile/Api.svc/ModifySNSUserProfile"
#define kRestAPI_SignUp             @"http://doleserver.com:3000/api/v2/Profile/Api.svc/MobileSignup"
#define kRestAPI_GetProvinceInfo    @"http://doleserver.com:3000/api/v2/Profile/Api.svc/GetProvinceInfo"
#define kRestAPI_EventInfo          @"http://doleserver.com:3000/api/v2/Portal/Game/Api.svc/GetEventNoticeList"
#define kRestAPI_Coupon             @"http://doleserver.com:3000/api/v2/Coupon/Mobile/Api.svc/CheckMobileCoupon"
#define kRestAPI_GetDoleCoin        @"http://doleserver.com:3000/api/v2/Billing/Wallet/Api.svc/GetBalance"
#define kRestAPI_RegisterCS         @"http://doleserver.com:3000/api/v2/CS/Main/Api.svc/RegisterCS"
#define kRestAPI_ChargeFreeCash     @"http://doleserver.com:3000/api/v2/Billing/Charge/Api.svc/ChargeFreeCash"
#define kRestAPI_UseCoupon          @"http://doleserver.com:3000/api/v2/Billing/Charge/Api.svc/UseCoinCoupon"
#define kRestAPI_CheckVersion       @"http://doleserver.com:3000/api/v2/Profile/Api.svc/GetAppVersion"
#define kRestAPI_WithdrawUser       @"http://doleserver.com:3000/api/v2/Profile/Api.svc/WithdrawUser"
#define kRestAPI_DisconnectSNS      @"http://doleserver.com:3000/api/v2/Profile/Api.svc/DisconnectSNS"
#define kRestAPI_GetRenewAuthKey    @"http://doleserver.com:3000/api/v2/Auth/Api.svc/GetRenewAuthKey"

//
//#define kRestAPI_LogInURL           @"https://dole.ithinkcore.net/Auth/Api.svc/GetAuthKeyMobile"
//#define kRestAPI_SNSLogInURL        @"https://dole.ithinkcore.net/Auth/Api.svc/SNSLogin"
//#define kRestAPI_FindPasswordURL    @"https://dole.ithinkcore.net/Profile/Api.svc/FindPassword"
//#define kRestAPI_GetUserInfo        @"https://dole.ithinkcore.net/Profile/Api.svc/GetMobileProfile"
//#define kRestAPI_EditUserInfo       @"https://dole.ithinkcore.net/Profile/Api.svc/ModifyMobileProfile"
//#define kRestAPI_EditSnsUserInfo    @"https://dole.ithinkcore.net/Profile/Api.svc/ModifySNSUserProfile"
//#define kRestAPI_SignUp             @"https://dole.ithinkcore.net/Profile/Api.svc/MobileSignup"
//#define kRestAPI_GetProvinceInfo    @"https://dole.ithinkcore.net/Profile/Api.svc/GetProvinceInfo"
//#define kRestAPI_EventInfo          @"https://dole.ithinkcore.net/Portal/Game/Api.svc/GetEventNoticeList"
//#define kRestAPI_Coupon             @"https://dole.ithinkcore.net/Coupon/Mobile/Api.svc/CheckMobileCoupon"
//#define kRestAPI_GetDoleCoin        @"https://dole.ithinkcore.net/Billing/Wallet/Api.svc/GetBalance"
//#define kRestAPI_RegisterCS         @"https://dole.ithinkcore.net/CS/Main/Api.svc/RegisterCS"
//#define kRestAPI_ChargeFreeCash     @"https://dole.ithinkcore.net/Billing/Charge/Api.svc/ChargeFreeCash"
//#define kRestAPI_UseCoupon          @"https://dole.ithinkcore.net/Billing/Charge/Api.svc/UseCoinCoupon"
//#define kRestAPI_CheckVersion       @"https://dole.ithinkcore.net/Profile/Api.svc/GetAppVersion"
//#define kRestAPI_WithdrawUser       @"https://dole.ithinkcore.net/Profile/Api.svc/WithdrawUser"
//#define kRestAPI_DisconnectSNS      @"https://dole.ithinkcore.net/Profile/Api.svc/DisconnectSNS"
//#define kRestAPI_GetRenewAuthKey    @"https://dole.ithinkcore.net/Auth/Api.svc/GetRenewAuthKey"
//
#else

#define kRestAPI_LogInURL           @"http://doleserver.com:4000/api/v2/Auth/Api.svc/GetAuthKeyMobile"
#define kRestAPI_SNSLogInURL        @"http://doleserver.com:4000/api/v2/Auth/Api.svc/SNSLogin"
#define kRestAPI_FindPasswordURL    @"http://doleserver.com:4000/api/v2/Profile/Api.svc/FindPassword"
#define kRestAPI_GetUserInfo        @"http://doleserver.com:4000/api/v2/Profile/Api.svc/GetMobileProfile"
#define kRestAPI_EditUserInfo       @"http://doleserver.com:4000/api/v2/Profile/Api.svc/ModifyMobileProfile"
#define kRestAPI_EditSnsUserInfo    @"http://doleserver.com:4000/api/v2/Profile/Api.svc/ModifySNSUserProfile"
#define kRestAPI_SignUp             @"http://doleserver.com:4000/api/v2/Profile/Api.svc/MobileSignup"
#define kRestAPI_GetProvinceInfo    @"http://doleserver.com:4000/api/v2/Profile/Api.svc/GetProvinceInfo"
#define kRestAPI_EventInfo          @"http://doleserver.com:4000/api/v2/Portal/Game/Api.svc/GetEventNoticeList"
#define kRestAPI_Coupon             @"http://doleserver.com:4000/api/v2/Coupon/Mobile/Api.svc/CheckMobileCoupon"
#define kRestAPI_GetDoleCoin        @"http://doleserver.com:4000/api/v2/Billing/Wallet/Api.svc/GetBalance"
#define kRestAPI_RegisterCS         @"http://doleserver.com:4000/api/v2/CS/Main/Api.svc/RegisterCS"
#define kRestAPI_ChargeFreeCash     @"http://doleserver.com:4000/api/v2/Billing/Charge/Api.svc/ChargeFreeCash"
#define kRestAPI_UseCoupon          @"http://doleserver.com:4000/api/v2/Billing/Charge/Api.svc/UseCoinCoupon"
#define kRestAPI_CheckVersion       @"http://doleserver.com:4000/api/v2/Profile/Api.svc/GetAppVersion"
#define kRestAPI_WithdrawUser       @"http://doleserver.com:4000/api/v2/Profile/Api.svc/WithdrawUser"
#define kRestAPI_DisconnectSNS      @"http://doleserver.com:4000/api/v2/Profile/Api.svc/DisconnectSNS"
#define kRestAPI_GetRenewAuthKey    @"http://doleserver.com:4000/api/v2/Auth/Api.svc/GetRenewAuthKey"

#endif


- (NSString *)UUID
{
    NSString *uuid = [[NSUUID UUID] UUIDString];
    return  uuid;
}

- (NSString *)DeviceUUID
{
    NSString *uuid = [[NSUUID UUID] UUIDString];
    return  uuid;
}


// Get IP Address
- (NSString *)IPAddress {
    NSString *address = @"";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            if(temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                }
            }
            temp_addr = temp_addr->ifa_next;
        }
    }
    // Free memory
    freeifaddrs(interfaces);
    return address;
}



NSString* checkNilString(NSString* string){
    if (nil == string) return @"";
    
    return string;
}

-(void)requestLogInWithUserID:(NSString*)userEmail Password:(NSString*)password
{
    if (self.userConfig.deviceToken == nil || [self.userConfig.deviceToken isEqualToString:@""]) {
       self.userConfig.deviceToken = @"NO_DEVICE_TOKEN";
    }
    //    NSString *restUrl = @"http://dole.ithinkcore.net/Auth/Api.svc/GetAuthKeyMobile";
    NSDictionary *params =@{@"UserID":userEmail,
                            @"Password":password,
                            @"UUID":[self DeviceUUID],
                            @"ServiceCode":kServiceCode,
                            @"MobileOS":kMobileOSType,
                            @"DeviceToken":self.userConfig.deviceToken,
                            @"ClientIP":[self IPAddress]};
    
    [self requestWithRestURL:kRestAPI_LogInURL ParameterDic:params];
}

-(void)requestLogInWithSNSID:(NSString*)snsID
{
    if (self.userConfig.deviceToken == nil || [self.userConfig.deviceToken isEqualToString:@""]) {
        self.userConfig.deviceToken = @"NO_DEVICE_TOKEN";
    }
    NSDictionary *params =@{@"UUID":[self DeviceUUID],
                            @"SNSType":@"SNS001",
                            @"SNSID":snsID,
                            @"ServiceCode":kServiceCode,
                            @"MobileOS":kMobileOSType,
                            @"DeviceToken":self.userConfig.deviceToken,
                            @"ClientIP":[self IPAddress],
                            @"Email":self.facebookManager.emailAddress
                            };
    
    [self requestWithRestURL:kRestAPI_SNSLogInURL ParameterDic:params];
}

-(void)requestFindPasswordWithEmail:(NSString*)userEmail
{
    NSDictionary *params =@{@"UserID":userEmail,
                            @"Email":userEmail,
                            @"ClientIP":[self IPAddress]};
    
    //    NSString *restUrl = @"http://dole.ithinkcore.net/Profile/Api.svc/FindPassword";
    
    [self requestWithRestURL:kRestAPI_FindPasswordURL ParameterDic:params];
}

-(void)requestUserInfoWithUserNo:(NSInteger)userNo AuthKey:(NSString*)authKey
{
    
    //    NSString *restUrl = @"http://dole.ithinkcore.net/Profile/Api.svc/GetMobileProfile";
    
    NSDictionary *params =@{@"UserNo":[NSNumber numberWithInteger:userNo],
                            @"AuthKey": checkNilString(authKey),
                            @"ClientIP":[self IPAddress]};
    
    [self requestWithRestURL:kRestAPI_GetUserInfo ParameterDic:params];
}


-(void)requestEditUserInfoWithUserNo:(NSInteger)userNo CurPassword:(NSString*)curPassword NewPassword:(NSString*)newPassword BirthYear:(NSUInteger)birthyear
                                City:(NSString*)city Gender:(enum GenderType)gender
{
    NSDictionary *params =@{@"UserNo":[NSNumber numberWithInteger:userNo],
                            @"UserID":self.userConfig.email,
//                            @"StandardCountryCode":self.currentCountryCode,
                            @"AddressMain":city,
                            @"Country":self.currentCountryCode,
                            @"NewPassword":newPassword,
                            @"OldPassword":curPassword,
                            @"BirthDay":[NSString stringWithFormat:@"%04lu-01-01", (unsigned long)birthyear, nil],
                            @"Gender":[NSNumber numberWithInteger:gender],
                            @"Language":self.currentLanguageName,
                            @"ClientIP":[self IPAddress]};
    
    [self requestWithRestURL:kRestAPI_EditUserInfo ParameterDic:params];
}

-(void)requestSignUpWithEmail:(NSString*)userEmail Password:(NSString*)password
                    BirthYear:(NSUInteger)birthyear City:(NSString*)city Gender:(enum GenderType)gender
{
    NSDictionary *params =@{@"UserID":userEmail,
                            @"Password":password,
                            @"Email":userEmail,
                            @"StandardCountryCode":self.currentCountryCode,
                            @"AddressMain":city,
                            @"BirthDay":[NSString stringWithFormat:@"%04lu-01-01", (unsigned long)birthyear, nil],
                            @"Gender":[NSNumber numberWithInteger:gender],
                            @"Language":self.currentLanguageName,
                            @"SnsCode":@"",
                            @"SnsID":@"",
                            @"SnsUserName":@"",
                            @"UUID":[self DeviceUUID],
                            @"ClientIP":[self IPAddress]};
    
    [self requestWithRestURL:kRestAPI_SignUp ParameterDic:params];
}

-(void)requestSignUpWithFacebookID:(NSString*)facebookID
                      FacebookName:(NSString*)name
                     FacebookEmail:(NSString*)email
                         BirthYear:(NSUInteger)birthyear
                              City:(NSString*)city
                            Gender:(enum GenderType)gender
{
    NSDictionary *params =@{@"UserID":email,
                            @"Password":@"1234567890",
                            @"Email":email,
                            @"StandardCountryCode":self.currentCountryCode,
                            @"AddressMain":city,
                            @"BirthDay":[NSString stringWithFormat:@"%04lu-01-01", (unsigned long)birthyear, nil],
                            @"Gender":[NSNumber numberWithInteger:gender],
                            @"Language":self.currentLanguageName,
                            @"SnsCode":@"SNS001",
                            @"SnsID":facebookID,
                            @"SnsUserName":name,
                            @"UUID":[self DeviceUUID],
                            @"ClientIP":[self IPAddress]};
    
    [self requestWithRestURL:kRestAPI_SignUp ParameterDic:params];
}


-(NSInteger)contentGroupClassNoByCountryCode
{
    NSString *countryCode = self.currentCountryCode;
    
    if ([countryCode isEqualToString:@"KR"]){
        return 156;
    }
    else if ([countryCode isEqualToString:@"NZ"]){
        return 155;
    }
    else if ([countryCode isEqualToString:@"PH"]){
        return 153;
    }
    else if ([countryCode isEqualToString:@"SG"]){
        return 154;
    }
    else if ([countryCode isEqualToString:@"JP"]){
        return 157;
    }
    else if ([countryCode isEqualToString:@"AU"]){
        return 158;
    }
    
    return 154; // Singapore
}

-(void)requestEventQuery
{
    NSInteger contentGroupClassNo = [self contentGroupClassNoByCountryCode];
    
    NSDictionary *params =@{@"ServiceCode":kServiceCode,
                            @"ContentGroupClassNo":[NSNumber numberWithInteger:contentGroupClassNo],
                            @"ContentGroupNo":@4,
                            @"LanguageNo":@1,
                            @"NoticeStatus":@20,
                            @"ClientIP":[self IPAddress]};
    
    [self requestWithRestURL:kRestAPI_EventInfo ParameterDic:params];
}

-(void)requestDoleCoinQuery
{
    self.userConfig.needQueryDoleCoin = NO;
    
    NSDictionary *params =@{@"UserNo":[NSNumber numberWithInteger:self.userConfig.userNo],
                            @"ClientIP":[self IPAddress]};
    
    [self requestWithRestURL:kRestAPI_GetDoleCoin ParameterDic:params];
}

-(void)requestDoleCoinChargeFreeCash:(NSInteger)dolePoint UserNo:(NSInteger)userNo AuthKey:(NSString*)authKey OrderID:(NSString*)guid
{
    NSDictionary *params =@{@"ServiceCode":kServiceCode,
                            @"UserNo":[NSNumber numberWithInteger:userNo],
                            @"AuthKey":checkNilString(authKey),
                            @"OrderID":guid,
                            @"PaymentMethodCode":@"PMC003",
                            @"PaymentAmount":@0,
                            @"ChargeAmount":[NSNumber numberWithInteger:dolePoint],
                            @"ValidityPeriod":@365,
                            @"Description":@"",
                            @"UUID":[self DeviceUUID],
                            @"ClientIP":[self IPAddress]};
    
    [self requestWithRestURL:kRestAPI_ChargeFreeCash ParameterDic:params];
    
    self.userConfig.needQueryDoleCoin = YES;
}



//Use Facebook
-(void)requestEditFaceUserInfoWithBirthYear:(NSUInteger)birthyear City:(NSString*)city Gender:(enum GenderType)gender
{
    NSDictionary *params =@{@"UserNo":[NSNumber numberWithInteger:self.userConfig.userNo],
                            @"UserID":self.userConfig.email,
                            @"AddressMain":city,
                            @"Country":self.currentCountryCode,
                            @"BirthDay":[NSString stringWithFormat:@"%04lu-01-01", (unsigned long)birthyear, nil],
                            @"Gender":[NSNumber numberWithInteger:gender],
                            @"Language":self.currentLanguageName,
                            @"ClientIP":[self IPAddress]};
    
    [self requestWithRestURL:kRestAPI_EditSnsUserInfo ParameterDic:params];

}

-(void)requestCitynamesWithCountryCode:(NSString*)countryCode
{
    NSDictionary *params =@{@"StandardCountryCode":countryCode,
                            @"ClientIP":@""};
    
    [self requestWithRestURL:kRestAPI_GetProvinceInfo ParameterDic:params];
}

-(void)requestCouponQueryWithQRCode:(NSString*)qrCode
{
    NSDictionary *params =@{@"PromotionInfo":@1,
                            @"Serial":qrCode,
                            @"ServiceCode":kServiceCode,
                            @"UUID":[self DeviceUUID],
                            @"UserNo":[NSNumber numberWithInteger:self.userConfig.userNo],
                            @"ClientIP":[self IPAddress]};
    
    [self requestWithRestURL:kRestAPI_Coupon ParameterDic:params];
}

-(void)requestUseCouponWithCode:(NSString*)couponCode
                         UserNo:(NSInteger)userNo
                        AuthKey:(NSString*)authKey
                        OrderID:(NSString*)guid
{
    NSDictionary *params =@{@"Serial":couponCode,
                            @"AuthKey":checkNilString(authKey),
                            @"OrderID":guid,
                            @"ServiceCode":kServiceCode,
                            @"UUID":[self DeviceUUID],
                            @"UserNo":[NSNumber numberWithInteger:self.userConfig.userNo],
                            @"ClientIP":[self IPAddress]};
    
    [self requestWithRestURL:kRestAPI_UseCoupon ParameterDic:params];
    
    self.userConfig.needQueryDoleCoin = YES;
}


-(void)requestRegisterQuestionWithTopic:(NSString*)topic ReceiveEmail:(NSString*)email Question:(NSString*)question
{
    
    NSDateFormatter *formmatter = [[NSDateFormatter alloc] init];
    formmatter.dateFormat = @"yyyy-MM-dd HH:mm";
    NSString *occurTimeString = [formmatter stringFromDate:[NSDate date]];
    NSInteger userNo = self.userConfig.userNo;
    NSString *emailID = self.userConfig.email ? self.userConfig.email : email;
    NSDictionary *params =@{
                            @"ServiceCode":kServiceCode,
                            @"UserNo":[NSNumber numberWithInteger:userNo],
                            @"CategoryNo":@1,
                            @"Description":topic,
                            @"Email":email,
                            @"ISReceiveEmail":@"false",
                            @"InquiryContent":question,
                            @"InquiryTitle":topic,
                            @"OccurrenceDateTime":occurTimeString,
                            @"SubCategoryNo":@1,
                            @"UserID":emailID,
                            @"ClientIP":[self IPAddress]};
    
    [self requestWithRestURL:kRestAPI_RegisterCS ParameterDic:params];
}

-(void)requestRegisterHeightPaperWithAddress:(NSString*)address
{
    NSDateFormatter *formmatter = [[NSDateFormatter alloc] init];
    formmatter.dateFormat = @"yyyy-MM-dd HH:mm";
    NSString *occurTimeString = [formmatter stringFromDate:[NSDate date]];
    
    NSDictionary *params =@{
                            @"ServiceCode":kServiceCode,
                            @"UserNo":[NSNumber numberWithInteger:self.userConfig.userNo],
                            @"CategoryNo":@1,
                            @"Description":@"HeightChartPaper",
                            @"Email":@"",
                            @"ISReceiveEmail":@"false",
                            @"InquiryContent":address,
                            @"InquiryTitle":@"HeightChartPaper",
                            @"OccurrenceDateTime":occurTimeString,
                            @"SubCategoryNo":@1,
                            @"UserID":self.userConfig.email,
                            @"ClientIP":[self IPAddress]};
    
    [self requestWithRestURL:kRestAPI_RegisterCS ParameterDic:params];
}

-(void)requestVersionCheck
{
    NSDictionary *params =@{@"ServiceCode":kServiceCode,
                            @"MobileOS":@"iOS",
                            @"ClientIP":[self IPAddress]};
    
    [self requestWithRestURL:kRestAPI_CheckVersion ParameterDic:params];
}

-(void)requestWithdrawWithEmail:(NSString*)email Password:(NSString*)password UserNo:(NSInteger)userNo AuthKey:(NSString*)authKey
{
    NSDictionary *params =@{
                            @"UserID":email,
                            @"UserNo":[NSNumber numberWithInteger:userNo],
                            @"AuthKey":checkNilString(authKey),
                            @"Password":password,
                            @"Reason":@"",
                            @"ClientIP":[self IPAddress]};
    
    [self requestWithRestURL:kRestAPI_WithdrawUser ParameterDic:params];
}


-(void)requestDissconnectWithSnsID:(NSString*)snsID AuthKey:(NSString*)authKey UserNo:(NSInteger)userNo
{
    NSDictionary *params =@{
//                            @"SNSType":kSNSType,
                            @"SnsCode":kSNSType,
                            @"UserNo":[NSNumber numberWithInteger:userNo],
                            @"AuthKey":checkNilString(authKey),
//                            @"SNSID":snsID,
                            @"SnsID":snsID,
                            @"ClientIP":[self IPAddress]};
    
    [self requestWithRestURL:kRestAPI_DisconnectSNS ParameterDic:params];
}

-(void)requestRenewAuthKey
{
    if (self.userConfig.deviceToken == nil || [self.userConfig.deviceToken isEqualToString:@""]) {
        self.userConfig.deviceToken = @"NO_DEVICE_TOKEN";
    }
    NSDictionary *params =@{
                            @"UserNo":[NSNumber numberWithInteger:self.userConfig.userNo],
                            @"AuthKey":self.userConfig.authKey,
                            @"ServiceCode":kServiceCode,
                            @"MobileOS":kMobileOSType,
                            @"DeviceToken":self.userConfig.deviceToken,
                            @"UUID":[self DeviceUUID],
                            @"ClientIP":[self IPAddress]};
    
    [self requestWithRestURL:kRestAPI_GetRenewAuthKey ParameterDic:params];
}


-(NSMutableData*)receivedBufferData
{
    // You must override this method if use server
    assert(0);
}

-(void)didConnectionFail:(NSError*)error
{
    [self toastNetworkError];
}

-(void)didCompleteRecevedWithResultType:(DoleServerResultType)resultType ParsedData:(NSDictionary*)recevedData ParseError:(NSError*)error;
{
    
}




-(void)requestWithRestURL:(NSString*)url ParameterDic:(NSDictionary*)params
{
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                              cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                          timeoutInterval:60.0];
    theRequest.HTTPMethod = @"POST";
    NSError *error;
    NSData *json = nil;
    
#if DEBUG
    json = [NSJSONSerialization dataWithJSONObject:params options:NSJSONWritingPrettyPrinted error:&error];
#else
    json = [NSJSONSerialization dataWithJSONObject:params options:0 error:&error];
#endif
    NSString *sendString = nil;
    if (json)
    {
        sendString = [[NSString alloc]initWithData:json encoding:NSUTF8StringEncoding];
    }
    NSLog(@"Requesting: %@ %@", url, sendString);
    
    [theRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    theRequest.HTTPBody = json;
    // Create the NSMutableData to hold the received data.
    // receivedData is an instance variable declared elsewhere.
    [[self receivedBufferData] setLength:0];
    
    // create the connection with the request
    // and start loading the data
    NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:theRequest delegate:self startImmediately:YES];
    if (!theConnection) {
        // Release the receivedData object.
        [[self receivedBufferData] setLength:0];
        // Inform the user that the connection failed.
        
        NSLog(@"Connection Error");
    }
    
    [self addActiveIndicator];
}


- (void)connection:(NSURLConnection *)connection didSendBodyData:(NSInteger)bytesWritten totalBytesWritten:(NSInteger)totalBytesWritten totalBytesExpectedToWrite:(NSInteger)totalBytesExpectedToWrite
{
    
}

- (void)connection:(NSURLConnection *)connection didWriteData:(long long)bytesWritten totalBytesWritten:(long long)totalBytesWritten expectedTotalBytes:(long long)expectedTotalBytes
{
    
}
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    // This method is called when the server has determined that it
    // has enough information to create the NSURLResponse object.
    
    // It can be called multiple times, for example in the case of a
    // redirect, so each time we reset the data.
    
    // receivedData is an instance variable declared elsewhere.
    [[self receivedBufferData] setLength:0];
    
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    // Append the new data to receivedData.
    if (data.length) {
        
        NSString *rawDataString = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
        NSLog(@"connectionn didReceiveData Raw String is %@", rawDataString);
    }
    
    // receivedData is an instance variable declared elsewhere.
    [[self receivedBufferData]appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError Error = %@", error);
    [self removeActiveIndicator];
    
    [self didConnectionFail:error];
}

-(DoleServerResultType)resultTypeWithRestURL:(NSString*)restUrl
{
    
    if ([restUrl isEqualToString:kRestAPI_LogInURL]) return kDoleServerResultTypeLogIn;
    
    if ([restUrl isEqualToString:kRestAPI_FindPasswordURL]) return kDoleServerResultTypeFindPassword;
    
    if ([restUrl isEqualToString:kRestAPI_GetUserInfo]) return kDoleServerResultTypeUserInfo;
    
    if ([restUrl isEqualToString:kRestAPI_EditUserInfo]) return kDoleServerResultTypeEditUserInfo;
    
    if ([restUrl isEqualToString:kRestAPI_SignUp]) return kDoleServerResultTypeSignUp;
    
    if ([restUrl isEqualToString:kRestAPI_GetProvinceInfo]) return kDoleServerResultTypeGetCityNames;
    
    if ([restUrl isEqualToString:kRestAPI_EventInfo]) return kDoleServerResultTypeEventInfo;
    
    if ([restUrl isEqualToString:kRestAPI_Coupon]) return kDoleServerResultTypeCoupon;
    
    if ([restUrl isEqualToString:kRestAPI_GetDoleCoin]) return kDoleServerResultTypeGetDoleCoin;
    
    if ([restUrl isEqualToString:kRestAPI_RegisterCS]) return kDoleServerResultTypeRegisterCS;
    
    if ([restUrl isEqualToString:kRestAPI_SNSLogInURL]) return kDoleServerResultTypeSnsLogIn;

    if ([restUrl isEqualToString:kRestAPI_ChargeFreeCash]) return kDoleServerResultTypeChargeFreeCash;

    if ([restUrl isEqualToString:kRestAPI_UseCoupon]) return kDoleServerResultTypeUseCoinCoupon;

    if ([restUrl isEqualToString:kRestAPI_CheckVersion]) return kDoleServerResultTypeCheckAppVersion;

    if ([restUrl isEqualToString:kRestAPI_WithdrawUser]) return kDoleServerResultTypeWithdraw;

    if ([restUrl isEqualToString:kRestAPI_DisconnectSNS]) return kDoleServerResultTypeDissconnectSns;

    if ([restUrl isEqualToString:kRestAPI_EditSnsUserInfo]) return kDoleServerResultTypeEditSnsUserInfo;

    if ([restUrl isEqualToString:kRestAPI_GetRenewAuthKey]) return kDoleServerResultTypeRenewAuthkey;

    assert(0);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [self removeActiveIndicator];
    
    NSLog(@"Connection URL is %@",connection.originalRequest.URL.absoluteString);
    
    NSError *error;
    NSDictionary *result = [NSJSONSerialization JSONObjectWithData:[self receivedBufferData] options:(NSJSONReadingMutableContainers) error:&error];
    
    //NSLog(@"Return is %@ , ReturnCode is %@",  dic2[@"Return"], dic2[@"ReturnCode"]);
    
    if (!error){
        NSLog(@">>>>>>Success didReceiveData: %@", result);
        
//        for (id key in result.allKeys) {
//            NSLog(@"%@[%@]", key, result[key]);
//        }
        
//        NSLog(@"<<<<<<Success didReceiveData");
    }
    else{
        NSLog(@"didReceiveData Error = %@", error);
    }
    
    DoleServerResultType resultType = [self resultTypeWithRestURL:connection.originalRequest.URL.absoluteString];
    
    [self didCompleteRecevedWithResultType:resultType ParsedData:result ParseError:error];
}

-(void)toastNetworkError
{
    NSString *networkError = NSLocalizedString(@"The network is unstable. \nPlease try again after checking the network", @"");
    [ToastController showToastWithMessage:networkError duration:kToastMessageDurationType_Auto];
}

-(void)toastNetworkErrorWithErrorCode:(NSInteger)errorCode
{
    NSDictionary *networkErrorDic = @{
//                                      @90001: @"파라미터 오류",
//                                      @90002: @"해당 레코드 없음",
//                                      @90003: @"이력 등록 오류",
////                                      @90105: @"사용자데이터 없음",
//                                      @90105: NSLocalizedString(@"The email does not exist or is incorrect", @""),
//                                      @90108: @"사용자등록실패",
//                                      @90109: @"사용자프로필등록실패",
//                                      @90110: @"사용자등급등록 실패",
//                                      @90111: @"SNS계정 등록 실패",
//                                      @90205: @"인증키 오류",
//                                      @90211: @"로그인 실패 횟수 초과",
////                                      @90106: @"비밀번호가 맞지 않습니다",
//                                      @90106: NSLocalizedString(@"The password is incorrect", @""),
//                                      @90107: @"등록되지 않은 SNS 계정입니다",
//                                      @90127: @"password 길이 오류",
//                                      @90131: @"동일 password 오류",
//                                     @90101: @"user_id 중복오류",
////                                     @90102: @"email 중복오류",
//                                      @90102: NSLocalizedString(@"This email address is already in use", @""),
//                                     @90126: @"user id 길이 오류",
                                      
                                      @90001: @"Network Error 90001",
                                      @90002: @"Network Error 90002",
                                      @90003: @"Network Error 90003",
                                      //                                      @90105: @"사용자데이터 없음",
                                      @90105: NSLocalizedString(@"The email does not exist or is incorrect", @""),
                                      @90108: @"Network Error 90108",
                                      @90109: @"Network Error 90109",
                                      @90110: @"Network Error 90110",
                                      @90111: @"Network Error 90111",
                                      //Add 2014.06.26 Facebook ID
                                      @90113: NSLocalizedString(@"This account is signed up with Facebook", @""),
                                      
                                      @90205: @"Network Error 90205",
                                      @90211: @"Network Error 90211",
                                      //                                      @90106: @"비밀번호가 맞지 않습니다",The password is incorrect
                                      @90106: NSLocalizedString(@"The password is incorrect", @""),
                                      @90107: @"Network Error 90211",
                                      @90127: NSLocalizedString(@"The password is incorrect", @""),
                                      @90131: NSLocalizedString(@"The password is incorrect", @""),
                                      @90101: @"Network Error 90101",
                                      //                                     @90102: @"email 중복오류",
                                      @90102: NSLocalizedString(@"This email address is already in use", @""),
                                      @90126: @"Network Error 90126",
                                     };
    
    NSNumber *errorKey = [NSNumber numberWithInteger:errorCode];
    
    NSString *errStr = networkErrorDic[errorKey];
    
    if (errStr){
        [ToastController showToastWithMessage:errStr duration:kToastMessageDurationType_Auto];
    }
    else{
        [self toastNetworkError];
    }
}

-(void)popToLonOnViewController
{
    NSMutableArray *naviStack = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"LogOn"];
    [naviStack replaceObjectAtIndex:0 withObject:vc];
    self.navigationController.viewControllers = naviStack;
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}


@end
