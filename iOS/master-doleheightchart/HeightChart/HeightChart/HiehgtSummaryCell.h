//
//  HiehgtSummaryCell.h
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 9..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KidHeightDetail.h"

enum SummaryHeightCellSize {
    kSummaryHeightCellSizeWidth = (108/2),
    
};

@interface HiehgtSummaryCell : UICollectionViewCell
@property (nonatomic, retain) KidHeightDetail *kidHeightDetail;
@property (nonatomic) CGFloat heightValue;
-(void)updateStandardHeightMark:(BOOL)isVisible standardHeight:(CGFloat)kidHeight;
-(void)updateWithHeightDetail:(HeightDetailInfo*)detail;
@end
