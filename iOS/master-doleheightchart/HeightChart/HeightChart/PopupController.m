//
//  PopupController.m
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 2. 11..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "PopupController.h"
#import "UIView+Animation.h"
#import "UIView+ImageUtil.h"

@interface PopupController ()

@end

@implementation PopupController

-(void)loadView
{
    [super loadView];

}

-(void)setOverlayStyle
{
    UIColor *overlayColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6];
    self.view.backgroundColor = overlayColor;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self setOverlayStyle];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self decorateUI];
//    [self.popupContainerView animateScaleEffect];
    [self.popupContainerView animateScaleEffectWithDuration:0.3];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)decorateUI
{
    self.popupContainerView.backgroundColor = [UIColor clearColor];
    
    UIEdgeInsets topBarInsets = UIEdgeInsetsMake(0, (22.0/2.0), 0, (22.0/2.0));
    UIEdgeInsets bottomBarInsets = UIEdgeInsetsMake(0, (22.0/2.0), 0, (22.0/2.0));
    UIEdgeInsets notitleBodyView = UIEdgeInsetsMake((18.0/2.0), 0, 0, 0);
    
    self.topBarView.image = [UIImage resizableImageWithName:@"popup_top"
                                                  CapInsets:topBarInsets];
    self.bottomBarView.image = [UIImage resizableImageWithName:@"poup_bottom"
                                                     CapInsets:bottomBarInsets];
    self.notitleBodyView.image = [UIImage resizableImageWithName:@"popup_no_title"
                                                    CapInsets:notitleBodyView];

    [self.okButton setResizableImageWithType:DoleButtonImageTypePopupRed];
    [self.cancelButton setResizableImageWithType:DoleButtonImageTypePopupOrange];
    
//    self.popupContainerView.layer.shadowColor = [UIColor blackColor].CGColor;
//    self.popupContainerView.layer.shadowRadius = 3;
//    self.popupContainerView.layer.shadowOpacity = 0.7;
}

-(IBAction)cancelClose:(id)sender
{
    [UIView animateWithDuration:0.3 animations:^{
        self.view.backgroundColor = [UIColor clearColor];
        self.popupContainerView.transform = CGAffineTransformMakeScale(0.75, 0.75);
        self.popupContainerView.layer.opacity = 0;
    } completion:^(BOOL finished) {
        [self releaseModal];
    }];
}

@end
