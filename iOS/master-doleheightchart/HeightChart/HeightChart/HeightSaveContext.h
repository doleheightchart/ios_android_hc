//
//  HeightSaveContext.h
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 23..
//  Copyright (c) 2014년 Kim Sehyun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyChild.h"
#import "ChildHeight.h"
#import "DoleInfo.h"

@interface HeightSaveContext : NSObject

@property (nonatomic, copy) NSString *qrCode;
@property (nonatomic) CGFloat inputHeight;
@property (nonatomic, copy) UIImage *photo;

@property (nonatomic) BOOL reuseQRCode;
@property (nonatomic) BOOL freeQRCode;
@property (nonatomic, retain) MyChild *child;
@property (nonatomic, retain) ChildHeight *reuseHeight;
@property (nonatomic) enum DoleStickerType stickerType;
@property (nonatomic, retain) MyChild *saveTargetChild;
@end

@protocol HeightSaveDelegate <NSObject>

-(void)didScanQRCode:(HeightSaveContext*)context;
-(void)didInputHeight:(HeightSaveContext*)context;
-(void)didTakenPhoto:(HeightSaveContext*)context;
-(void)saveContext:(HeightSaveContext*)context;
-(void)cancelSave;

-(BOOL)alreadyTakenQRCode:(NSString*)qrCode;
-(BOOL)canTakenQRCode:(NSString*)qrCode Context:(HeightSaveContext*)context;

@end
