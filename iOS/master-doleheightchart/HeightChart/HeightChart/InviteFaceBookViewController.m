//
//  InviteFaceBookViewController.m
//  HeightChart
//
//  Created by ne on 2014. 3. 17..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "InviteFaceBookViewController.h"
#import "UIViewController+DoleHeightChart.h"
#import "UIView+ImageUtil.h"
#import "UIFont+DoleHeightChart.h"
#import "UIColor+ColorUtil.h"
#import "UIButton+CheckAndRadio.h"
#import "ToastController.h"
#import "FacebookManager.h"
#import "UIViewController+PolyServer.h"
#import "DoleCoinPopupViewController.h"
#import "MessageBoxController.h"

#import "Mixpanel.h"

#define kInveiteCellID @"inviteCell"

#define kAppIconSize 48
@interface FBUserIconDownloader : NSObject
@property (nonatomic, strong) FacebookFriendInfo *friendInfo;
@property (nonatomic, copy) void (^completionHandler)(void);
@property (nonatomic, strong) NSMutableData *activeDownload;
@property (nonatomic, strong) NSURLConnection *imageConnection;

- (void)startDownload;
- (void)cancelDownload;
@end

@implementation FBUserIconDownloader
#pragma mark


- (void)startDownload
{
    self.activeDownload = [NSMutableData data];
    
    NSString *imageUrlStr = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?width=%d&height=%d"
                             ,self.friendInfo.facebookID, kAppIconSize*2, kAppIconSize*2, nil];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:imageUrlStr]];
    
    // alloc+init and start an NSURLConnection; release on completion/failure
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    self.imageConnection = conn;
}

- (void)cancelDownload
{
    [self.imageConnection cancel];
    self.imageConnection = nil;
    self.activeDownload = nil;
}

#pragma mark - NSURLConnectionDelegate

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [self.activeDownload appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    // Clear the activeDownload property to allow later attempts
    self.activeDownload = nil;
    
    // Release the connection now that it's finished
    self.imageConnection = nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    // Set appIcon and clear temporary data/image
    UIImage *image = [[UIImage alloc] initWithData:self.activeDownload];
    
    if (image.size.width != kAppIconSize*2 || image.size.height != kAppIconSize*2)
    {
        CGSize itemSize = CGSizeMake(kAppIconSize*2, kAppIconSize*2);
        UIGraphicsBeginImageContextWithOptions(itemSize, NO, 0.0f);
        CGRect imageRect = CGRectMake(0.0, 0.0, itemSize.width, itemSize.height);
        [image drawInRect:imageRect];
        self.friendInfo.icon = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    else
    {
        self.friendInfo.icon = image;
    }
    
    self.activeDownload = nil;
    
    // Release the connection now that it's finished
    self.imageConnection = nil;
    
    // call our delegate and tell it that our icon is ready for display
    if (self.completionHandler)
        self.completionHandler();
}


@end




@interface InviteFaceBookViewController ()
{
    NSArray *_friendsList;
    BOOL _checkSelectAll;
    
    //Server
    NSString    *_lastOrderID;
    NSMutableData   *_recevedBufferData;
    NSInteger _inviteCount;
    
}
@property (weak, nonatomic) IBOutlet UIImageView *talkboxView;
@property (weak, nonatomic) IBOutlet UIView *baseView;
@property (weak, nonatomic) IBOutlet UITableView *inviteTableView;
@property (weak, nonatomic) IBOutlet UIButton *btnInvite;
@property (weak, nonatomic) IBOutlet UIImageView *imgMonkey;
@property (weak, nonatomic) IBOutlet UIButton *chkSelectAll;

@property (nonatomic, strong) NSMutableDictionary *imageDownloadsInProgress;
@property (weak, nonatomic) IBOutlet UILabel *lblTalk;
@property (weak, nonatomic) IBOutlet UILabel *lblSelect;

- (IBAction)clickAllSelect:(id)sender;
- (IBAction)clickInvitefirends:(id)sender;
- (IBAction)checkFriend:(id)sender;


@end

@implementation InviteFaceBookViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        
    }
    return self;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.imageDownloadsInProgress = [NSMutableDictionary dictionary];
    
    
    [self.chkSelectAll makeUICheckBox];
    
    // top, left, bottom, right point coordinate
    UIEdgeInsets kAllimageEdgeInsets = {(96/2), (16/2), (16/2), (16/2) };
    UIEdgeInsets ktalkboxEdgeInsets = {(22/2), (22/2), (38/2), (22/2)};
    
    UIImage *talkbox = [[UIImage imageNamed:@"invite_talk_box"] resizableImageWithCapInsets:ktalkboxEdgeInsets
                                                                               resizingMode: UIImageResizingModeStretch];
    self.talkboxView.image = talkbox;
    
    UIImage *imageBack = [[UIImage imageNamed:@"invite_facebook_list"] resizableImageWithCapInsets:kAllimageEdgeInsets
                                                                                      resizingMode:UIImageResizingModeTile];
    
    CGFloat sizeHeight = 508;
    if ([UIScreen isFourInchiScreen]){
        sizeHeight = 604;
    }
    
    UIImageView *backImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.baseView.frame.size.width,
                                                                              sizeHeight/2)];
    backImageView.image = imageBack;
    
    
    self.baseView.backgroundColor = [UIColor clearColor];
    [self.baseView insertSubview:backImageView belowSubview:self.inviteTableView];
    self.inviteTableView.delegate = self;
    self.inviteTableView.dataSource = self;
    self.inviteTableView.backgroundColor = [UIColor clearColor];
    
    [self.btnInvite setResizableImageWithType:DoleButtonImageTypeOrange];
    
    [self addCommonCloseButton];
    
    [self setupControlFlexiblePosition];
    
    self.facebookManager.invitedelegate = self;
}

-(void)setupControlFlexiblePosition
{
    
    self.lblTalk.font = [UIFont defaultKoreanFontWithSize:26/2];
    self.lblSelect.font = [UIFont defaultBoldFontWithSize:30/2];
    
    NSInteger num =  self.lblTalk.numberOfLines;
    CGFloat labelMargin = 0 ;
    if (num < 4) {
        labelMargin = 16;
    }
    
    if ([UIScreen isFourInchiScreen]){
        [self addTitleMiniToLeftTop];
        
        [self changeToHeightPixel:(602) Control:self.baseView];
        [self changeToHeightPixel:(602-94) Control:self.inviteTableView];
        
        [self moveToPositionYPixel:(136 ) Control:self.talkboxView];
        [self moveToPositionYPixel:(136 + labelMargin) Control:self.lblTalk];
        [self moveToPositionYPixel:(226) Control:self.imgMonkey];
        
        [self moveToPositionYPixel:(136 + 204) Control:self.baseView];
        [self moveToPositionYPixel:(136 + 204 + 602 +52) Control:self.btnInvite];
        
        
        
    }
    else{
        
        [self changeToHeightPixel:(506) Control:self.baseView];
        [self changeToHeightPixel:(506-94) Control:self.inviteTableView];
        
        [self moveToPositionYPixel:(56) Control:self.talkboxView];
        [self moveToPositionYPixel:(56 + labelMargin) Control:self.lblTalk];
        [self moveToPositionYPixel:(146) Control:self.imgMonkey];
        
        [self moveToPositionYPixel:(56 + 162 + 42) Control:self.baseView];
        [self moveToPositionYPixel:(56 + 162 + 42 + 506 +52) Control:self.btnInvite];
    }
    
    
    [self moveToPositionYPixel:28 Control:self.lblSelect];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
    
    self.btnInvite.enabled = false;
    
    [self.facebookManager friendListForInviteWithCompletion:^(NSArray *friendList) {
        _friendsList = friendList;
        
        [self.inviteTableView reloadData];
    }];
    
    return;
    
    
    if (self.facebookManager.facebookAcessToken == nil){
        self.facebookManager.delegate = self;
        [self.facebookManager login];
    }
    else{
        [self requestFacebookFriendList];
    }
}

-(void)requestFacebookFriendList
{
    [self.facebookManager checkFacebookIDWithCompletion:^(NSString *facebookID) {
        if (facebookID){
            [self.facebookManager friendListForInviteWithCompletion:^(NSArray *friendList) {
                _friendsList = friendList;
                
                [self.inviteTableView reloadData];
            }];
        }
        else{
            //            [ToastController showToastWithMessage:@"Facebook로딩실패" duration:5];
        }
    }];
    
    [self checkedBtnInviteEnable];
}


-(void)checkedBtnInviteEnable
{
    NSInteger checked = 0;
    for (int i = 0; i  < _friendsList.count; i++) {
        FacebookFriendInfo *info = _friendsList[i];
        if(info.checked){checked++;};
    }
    
    //    if (checked >= 5) {
    if (checked >= 1) {
        [self.btnInvite setEnabled:YES];
    }
    else{
        [self.btnInvite setEnabled:NO];
    }
}

#pragma TableView Delegate


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_friendsList count];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self checkedBtnInviteEnable];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSLog(@"Load cell Number %ld", (long)indexPath.row);
    NSInteger kLabelTag = 101;
    //    NSInteger kImageTag = 102;
    NSInteger kcheckboxTag = 103;
    
    FacebookFriendInfo *info = _friendsList[indexPath.row];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kInveiteCellID forIndexPath:indexPath];
    
    //라벨
    UILabel *label =(UILabel *) [cell viewWithTag:kLabelTag];
    label.font = [UIFont defaultRegularFontWithSize:34/2];
    label.textColor = [UIColor colorWithRGB:0x007f8b];
    label.text = info.name;
    //이미지
    
    UIImageView *profileImageView = (UIImageView*)[cell viewWithTag:102];
    profileImageView.layer.cornerRadius = 5;
    profileImageView.layer.masksToBounds = YES;
    if (info.icon){
        profileImageView.image = info.icon;
    }
    else {
        if (tableView.dragging == NO && tableView.decelerating == NO)
        {
            [self startIconDownload:info forIndexPath:indexPath];
        }
        profileImageView.image = [UIImage imageNamed:@"invite_no_image"];
    }
    
    
    //버튼
    UIButton *btnCheck = (UIButton*)[cell viewWithTag:kcheckboxTag];
    [btnCheck makeUICheckBox];
    btnCheck.selected = info.checked;
    
    
    UIView *lineView = (UIView*)[cell viewWithTag:222];
    lineView.backgroundColor = [UIColor colorWithRGB:0x02c2c9];
    CGRect frame = lineView.frame;
    frame.size.height = 1;
    
    return cell;
}

- (IBAction)clickAllSelect:(id)sender {
    // 남아있는 모든 셀을 선택한다.
    
    _checkSelectAll = !_checkSelectAll;
    
    for (int i = 0; i  < _friendsList.count; i++) {
        FacebookFriendInfo *info = _friendsList[i];
        info.checked = _checkSelectAll;
    }
    
    [self.inviteTableView reloadData];
    [self checkedBtnInviteEnable];
}

- (IBAction)clickInvitefirends:(id)sender {
    
    if ([self enableActionWithCurrentNetwork] == NO)return;
    
    NSMutableArray *selectedFriendList = [NSMutableArray array];
    
    NSInteger inviteFriendCount = 0;
    _inviteCount = inviteFriendCount;
    for (FacebookFriendInfo *fi in _friendsList) {
        if (fi.checked){
            [selectedFriendList addObject:fi];
        }
    }
    
    if (selectedFriendList.count == 0) return;
    
    [self.facebookManager inviteFriendWithFriendIDList:selectedFriendList];
}



- (IBAction)checkFriend:(id)sender {
    UITableViewCell *cell;
    UIButton *checkButton = sender;
    
    
    UIView *superView = checkButton.superview;
    
    while (superView && ![superView isKindOfClass:[UITableViewCell class]]) {
        superView = superView.superview;
    }
    
    
    cell = (UITableViewCell*)superView;
    
    
    
    NSIndexPath *indexPath = [self.inviteTableView indexPathForCell:cell];
    
    if (indexPath == nil) return;
    
    NSLog(@"Checked Value is %@ index is %d", checkButton.selected ? @"Check" : @"UnCheck", indexPath.row);
    
    FacebookFriendInfo *fi = _friendsList[indexPath.row];
    fi.checked = !checkButton.selected;
    
    NSLog(@"_friendList Name is %@ Value is %@ index is %d",fi.name, fi.checked ? @"Check" : @"UnCheck", indexPath.row);
    [self checkedBtnInviteEnable];
    
}




#pragma mark - Table cell image support

// -------------------------------------------------------------------------------
//	startIconDownload:forIndexPath:
// -------------------------------------------------------------------------------
- (void)startIconDownload:(FacebookFriendInfo *)fi forIndexPath:(NSIndexPath *)indexPath
{
    FBUserIconDownloader *iconDownloader = [self.imageDownloadsInProgress objectForKey:indexPath];
    if (iconDownloader == nil)
    {
        iconDownloader = [[FBUserIconDownloader alloc] init];
        iconDownloader.friendInfo = fi;
        [iconDownloader setCompletionHandler:^{
            
            UITableViewCell *cell = [self.inviteTableView cellForRowAtIndexPath:indexPath];
            
            // Display the newly loaded image
            UIImageView *profileImageView = (UIImageView*)[cell viewWithTag:102];
            profileImageView.image = fi.icon;
            //cell.imageView.image = appRecord.appIcon;
            
            // Remove the IconDownloader from the in progress list.
            // This will result in it being deallocated.
            [self.imageDownloadsInProgress removeObjectForKey:indexPath];
            
        }];
        [self.imageDownloadsInProgress setObject:iconDownloader forKey:indexPath];
        [iconDownloader startDownload];
    }
}

// -------------------------------------------------------------------------------
//	loadImagesForOnscreenRows
//  This method is used in case the user scrolled into a set of cells that don't
//  have their app icons yet.
// -------------------------------------------------------------------------------
- (void)loadImagesForOnscreenRows
{
    if ([_friendsList count] > 0)
    {
        NSArray *visiblePaths = [self.inviteTableView indexPathsForVisibleRows];
        for (NSIndexPath *indexPath in visiblePaths)
        {
            FacebookFriendInfo *fi = [_friendsList objectAtIndex:indexPath.row];
            
            if (!fi.icon)
                // Avoid the app icon download if the app already has an icon
            {
                [self startIconDownload:fi forIndexPath:indexPath];
            }
        }
    }
}

#pragma mark - UIScrollViewDelegate

// -------------------------------------------------------------------------------
//	scrollViewDidEndDragging:willDecelerate:
//  Load images for all onscreen rows when scrolling is finished.
// -------------------------------------------------------------------------------
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (!decelerate)
    {
        [self loadImagesForOnscreenRows];
    }
}

// -------------------------------------------------------------------------------
//	scrollViewDidEndDecelerating:
// -------------------------------------------------------------------------------
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self loadImagesForOnscreenRows];
}


#pragma mark Server

-(void)countUpDolePoint:(NSInteger)point
{
    
    if (self.userConfig.loginStatus != kUserLogInStatusOffLine){
        //        [ToastController showToastWithMessage:@"Facebook에 업로드 하였습니다" duration:5 onViewController:self];
        _lastOrderID = [self UUID];
        
        
        [self requestDoleCoinChargeFreeCash:point UserNo:self.userConfig.userNo AuthKey:self.userConfig.authKey OrderID:_lastOrderID];
    }
    else{
        //        [ToastController showToastWithMessage:@"Facebook에 업로드 하였습니다" duration:5 onViewController:self];
        
        
        
    }
}

- (void)didConnectionFail:(NSError *)error
{
    [self toastNetworkError];
}

- (void)didCompleteRecevedWithResultType:(DoleServerResultType)resultType ParsedData:(NSDictionary *)recevedData ParseError:(NSError *)error
{
    if (!error){
        if ([recevedData[@"Return"] boolValue]){
            
            if (resultType == kDoleServerResultTypeChargeFreeCash){
                
                self.userConfig.doleCoin += _inviteCount;
                [self.userConfig saveDoleCoinToLocal];
                
                UIStoryboard *story = [UIStoryboard storyboardWithName:@"Popup" bundle:nil];
                DoleCoinPopupViewController *dvc = [story instantiateViewControllerWithIdentifier:@"dolecoinPopup"];
                [dvc dolcoinCount:_inviteCount];
                dvc.completion = ^(DoleCoinPopupViewController *controller){
                    
                };
                [dvc showModalOnTheViewController:nil];
            }
        }
        else{
            NSInteger errorCode = [recevedData[@"ReturnCode"] integerValue];
            [self toastNetworkErrorWithErrorCode:errorCode];
        }
    }
    else{
        
        [self toastNetworkError];
    }
}

- (NSMutableData*)receivedBufferData
{
    if (nil == _recevedBufferData){
        _recevedBufferData = [NSMutableData dataWithCapacity:0];
    }
    
    return _recevedBufferData;
}

#pragma mark Facebook Manager & Invite Delegate

- (void) inviteResult:(NSInteger)result
{
    NSInteger inviteFriendCount = 0;
    for (FacebookFriendInfo *fi in _friendsList) {
        if (fi.checked){
            inviteFriendCount ++;
        }
    }
    
    
    if (result == FacebookInviteReturn_OK) {
        //        [ToastController showToastWithMessage:@"Facebook Invite friends 성공" duration:5 onViewController:self];
        [self countUpDolePoint:inviteFriendCount];
        _inviteCount = inviteFriendCount;
        
        [[Mixpanel sharedInstance] track:@"Invited Friends"];
    }
    else{
        //        [ToastController showToastWithMessage:@"Facebook Invite friends 실패" duration:5 onViewController:self];
    }
}

- (void) loginResult:(BOOL)isSucess withAcessToken:(NSString*)accessToken
{
    if (isSucess){
        
    }
}

- (void) didReceiveUserID:(NSString*)userID Email:(NSString*)email Error:(id)error
{
    if (userID){
        [self requestFacebookFriendList];
    }
}

@end




