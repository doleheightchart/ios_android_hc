//
//  HomeViewController.h
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 18..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeToolBar.h"
#import "DoleProtocolList.h"
#import "HeightSaveContext.h"

typedef enum : NSUInteger {
    kHomeStatusNone,
    kHomeStatusFinishedSplash,
    kHomeStatusNetworkCheck,
    kHomeStatusDidVersionCheck,
    kHomeStatusDidFirstTimeCheck,
    kHomeStatusDidEventCheck,
    kHomeStatusLogIn,
    kHomeStatusDidLogIn,
    kHomeStatusDidLogOff,
} HomeStatus;

@interface HomeViewController : UIViewController<HomeToolBarDelegate,
                                                MonkeyTreeDataSource,
                                                MonkeyHomeListDelegate,
                                                NSFetchedResultsControllerDelegate,
                                                FacebookManagerDelegate
                                                >

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) NSFetchedResultsController *localFetchedResultController;
@property (nonatomic) HomeStatus currentStatus;

-(void)becameActive;

@end
