//
//  AppDelegate.m
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 21..
//  Copyright (c) 2014년 Kim Sehyun. All rights reserved.
//

#import <FacebookSDK/FacebookSDK.h>
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "HomeViewController.h"
#import "OverlayViewController.h"
#import "UserConfig.h"

#import "Mixpanel.h"

@implementation AppDelegate

//@synthesize managedObjectContext=_managedObjectContext;
@synthesize managedObjectModel=_managedObjectModel;
//@synthesize persistentStoreCoordinator=_persistentStoreCoordinator;
@synthesize userConfig=_userConfig;
@synthesize facebookManager = _facebookManager;

@synthesize localPersistentStoreCoordinator=_localPersistentStoreCoordinator;
@synthesize localManagedObjectContext=_localManagedObjectContext;
@synthesize cloudPersistentStoreCoordinator=_cloudPersistentStoreCoordinator;
@synthesize cloudManagedObjectContext=_cloudManagedObjectContext;

//@synthesize currentObjectContext=_currentObjectContext;


#define kCAR_APPKEY     @"ncIdX3la"
#define kCAR_CID        @"11896"
#define kCAR_PID        @"1"
#define kCAR_CPI        @"1"


#define MIXPANEL_TOKEN @"764d9434fe4840b650fd2a9c5da25b1d"
#define kDidCreateMixpanelKey @"DidCreateFirstMixpanelKey"

-(NSString*)CountryCode
{
    NSString *countryCode = [[NSLocale currentLocale] objectForKey:NSLocaleCountryCode];
    return countryCode;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [Fabric with:@[CrashlyticsKit]];

    [Mixpanel sharedInstanceWithToken:MIXPANEL_TOKEN];
    
    bool didCreateFirstMixpanelKey = [[NSUserDefaults standardUserDefaults] boolForKey:kDidCreateMixpanelKey];
    if (!didCreateFirstMixpanelKey) {
        NSString* savedEmail = [[NSUserDefaults standardUserDefaults] stringForKey:@"Email"];
        if (savedEmail) {
            // This means a previous instance wherein a user has logged in but terminated / updated the app
            // This also means that mixpanel people's createAlias has been created on behalf of the user prior to termination
            // Safe to assume alias/email = distinctId
            [[Mixpanel sharedInstance] identify:savedEmail];
            [[Mixpanel sharedInstance].people set:@{@"$email":savedEmail}];
            [[Mixpanel sharedInstance] registerSuperProperties:@{@"Email": savedEmail}];
            [[Mixpanel sharedInstance] registerSuperProperties:@{@"User Type": @"Registered"}];
        } else {
            // Anonymous
            NSString *identifier = [[NSProcessInfo processInfo] globallyUniqueString];
            [[Mixpanel sharedInstance] identify:identifier];
            [[Mixpanel sharedInstance] registerSuperProperties:@{@"User Type": @"Anonymous"}];
        }
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kDidCreateMixpanelKey];
    } else {
        NSString* savedEmail = [[NSUserDefaults standardUserDefaults] stringForKey:@"Email"];
        if (savedEmail) {
            [[Mixpanel sharedInstance] registerSuperProperties:@{@"User Type": @"Registered"}];
            [[Mixpanel sharedInstance] registerSuperProperties:@{@"Email": savedEmail}];
            [[Mixpanel sharedInstance] identify:[Mixpanel sharedInstance].distinctId];
            [[Mixpanel sharedInstance].people set:@{@"$email":savedEmail}];
        } else {
            [[Mixpanel sharedInstance] registerSuperProperties:@{@"User Type": @"Anonymous"}];
            [[Mixpanel sharedInstance] identify:[Mixpanel sharedInstance].distinctId];
        }
    }
    
    [[Mixpanel sharedInstance] track:@"App Launched"];
    
//    [FBLoginView class];
//    [FBProfilePictureView class];
    
    NSDictionary *userInfo = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    
    if(userInfo != nil)
    {
        [self application:application didReceiveRemoteNotification:userInfo];
    }
    
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
    }
    
    // Override point for customization after application launch.

    [self loadingBackMusic];
    
//    return YES;
    
    
    //CAReward
//    if (NO == [[self CountryCode] isEqualToString:@"JP"]) return YES;
//    
//    BOOL result_c = YES;
//    if ((launchOptions != nil) && ([launchOptions valueForKey:UIApplicationLaunchOptionsURLKey] != nil)) {
//        result_c = YES;
//    } else {
//        [CARController init];
//        [CARController setNumberOfRequest:1];
//        [CARController notifyAppLaunch:kCAR_APPKEY cid:kCAR_CID pid:kCAR_PID cpi:kCAR_CPI];
//        result_c = NO;
//    }
//    
//    return result_c;
    return YES;
}

-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{

    NSString* deviceTokenString = [[[[deviceToken description]
                                stringByReplacingOccurrencesOfString: @"<" withString: @""]
                               stringByReplacingOccurrencesOfString: @">" withString: @""]
                              stringByReplacingOccurrencesOfString: @" " withString: @""];

    NSLog(@"device token = %@", deviceTokenString);
    
    self.userConfig.deviceToken = deviceTokenString;
    self.userConfig.deviceTokenData = deviceToken;
    //longzhe cui added
    [[Mixpanel sharedInstance].people addPushDeviceToken:deviceToken];
    
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSLog(@"Failed to register for remote notifications with error= %@", error);
    
    // Sample device token for simulator
    self.userConfig.deviceToken = @"FE66489F304DC75B8D6E8200DFF8A456E8DAEACEC428B427E9518741C92C6660";
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
   // NSString *itemName = [userInfo objectForKey:ToDoItemKey];
    
//    [viewController displayItem:itemName];  // custom method
    

//    application.applicationIconBadgeNumber = 1;
//    application.applicationIconBadgeNumber = 0;
//    [application cancelAllLocalNotifications];
    
//    int badgeNum = [[UIApplication sharedApplication] applicationIconBadgeNumber];
//    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:1];
//    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
//    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:badgeNum];
//        [application cancelAllLocalNotifications];
    
    [self clearNotificationFromNotificationCenter];
}


-(void)clearNotificationFromNotificationCenter
{
    [UIApplication sharedApplication].applicationIconBadgeNumber = 1;
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
}

-(BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    
    //CAReward
    if (NO == [[self CountryCode] isEqualToString:@"JP"]) return YES;
    
    if(([url query] == nil) || [[url query] isEqualToString:@""]){
        return YES;
    }
//    [CARController init];
//    [CARController setNumberOfRequest:1];
//    [CARController notifyAppLaunchToCAR:kCAR_APPKEY cid:kCAR_CID pid:kCAR_PID cpi:kCAR_CPI query:[url query] urlScheme:YES];
    return YES;
}

#pragma mark -
#pragma mark MyManagedDocument [@property]

#define kModelName @"Model"
#define kSQLFileName @"MyModel.sqlite"

-(void)saveContext
{
    NSError *error;
    NSManagedObjectContext *managedObjectContext = self.currentObjectContext;
    
    if (managedObjectContext != nil)
    {
        // 변경 사항이 있지만 저장에 실패한 경우
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error])
        {
            // 실제 구현할때 이 부분은 수정해야한다. abort 메소드는 crash를 유발하므로 사용하면 안된다!
            NSLog(@"Unresolved error : %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

-(NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
                                                   inDomains:NSUserDomainMask]
            lastObject];
}

-(NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil)    return _managedObjectModel;
    
    // 컴파일된 Data Object Model(Xcode상의 .xcdatamodeld)에 접근하여 객체로 생성.
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:kModelName withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    
    return _managedObjectModel;
}

//-(NSManagedObjectContext *)managedObjectContext
//{
//    if (_managedObjectContext != nil)  return _managedObjectContext;
//    
//    //    _managedObjectContext = nil;
//    
//    NSPersistentStoreCoordinator *coordinator = self.persistentStoreCoordinator;
//    if (coordinator != nil)
//    {
//        _managedObjectContext = [[NSManagedObjectContext alloc] init];
//        
//        // Persistent Store Coordinator 연결.
//        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
//    }
//    
//    return _managedObjectContext;
//}

-(NSManagedObjectContext*)currentObjectContext
{
    if (self.userConfig.useiCloud)
        return self.cloudManagedObjectContext;
    else
        return self.localManagedObjectContext;
}

-(NSPersistentStoreCoordinator*)localPersistentStoreCoordinator
{
    if (_localPersistentStoreCoordinator != nil)    return _localPersistentStoreCoordinator;
    
    _localPersistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc]
                                   initWithManagedObjectModel:[self managedObjectModel]];
    
    NSError *error = nil;
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"MyData.heights"];
    NSDictionary *options = @{ NSMigratePersistentStoresAutomaticallyOption : @YES, NSInferMappingModelAutomaticallyOption : @YES };
    // Persistent Store Coordinator 설정. 저장소 타입을 SQLite로 한다.
    if (![_localPersistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                   configuration:nil
                                                             URL:storeURL
                                                         options:options
                                                           error:&error])
    {
        // 여기로 마찬가지로 실제 앱 개발시에는 반드시 수정해야한다.
        NSLog(@"Unresolved error : %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _localPersistentStoreCoordinator;
}
-(NSManagedObjectContext*)localManagedObjectContext
{
    if (_localManagedObjectContext != nil)  return _localManagedObjectContext;
    
    NSPersistentStoreCoordinator *coordinator = self.localPersistentStoreCoordinator;
    if (coordinator != nil)
    {
        _localManagedObjectContext = [[NSManagedObjectContext alloc] init];
        
        // Persistent Store Coordinator 연결.
        [_localManagedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    
    return _localManagedObjectContext;
}

-(NSPersistentStoreCoordinator*)cloudPersistentStoreCoordinator
{
    if (_cloudPersistentStoreCoordinator != nil) return _cloudPersistentStoreCoordinator;
    
        
    _cloudPersistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel: [self managedObjectModel]];
    NSPersistentStoreCoordinator *psc = _cloudPersistentStoreCoordinator;
    
    // Set up iCloud in another thread:
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        // ** Note: if you adapt this code for your own use, you MUST change this variable:
//        NSString *iCloudEnabledAppID = @"FGUL65Q4E4.com.dole.HeightChart";
        NSString *iCloudEnabledAppID = APP_iCLOUD_APPID;
        
        // ** Note: if you adapt this code for your own use, you should change this variable:
        NSString *dataFileName = @"HeightChart.sqlite";
        
        // ** Note: For basic usage you shouldn't need to change anything else
        
        NSString *iCloudDataDirectoryName = @"Data.nosync";
        NSString *iCloudLogsDirectoryName = @"Logs";
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSURL *localStore = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:dataFileName];
        NSURL *iCloud = [fileManager URLForUbiquityContainerIdentifier:nil];
        
        NSLog(@"iCloud is working");
        
        NSURL *iCloudLogsPath = [NSURL fileURLWithPath:[[iCloud path] stringByAppendingPathComponent:iCloudLogsDirectoryName]];
        
        NSLog(@"iCloudEnabledAppID = %@",iCloudEnabledAppID);
        NSLog(@"dataFileName = %@", dataFileName);
        NSLog(@"iCloudDataDirectoryName = %@", iCloudDataDirectoryName);
        NSLog(@"iCloudLogsDirectoryName = %@", iCloudLogsDirectoryName);
        NSLog(@"iCloud = %@", iCloud);
        NSLog(@"iCloudLogsPath = %@", iCloudLogsPath);
        
        if([fileManager fileExistsAtPath:[[iCloud path] stringByAppendingPathComponent:iCloudDataDirectoryName]] == NO) {
            NSError *fileSystemError;
            [fileManager createDirectoryAtPath:[[iCloud path] stringByAppendingPathComponent:iCloudDataDirectoryName]
                   withIntermediateDirectories:YES
                                    attributes:nil
                                         error:&fileSystemError];
            if(fileSystemError != nil) {
                NSLog(@"Error creating database directory %@", fileSystemError);
            }
        }
        
        NSString *iCloudData = [[[iCloud path]
                                 stringByAppendingPathComponent:iCloudDataDirectoryName]
                                stringByAppendingPathComponent:dataFileName];
        
        NSLog(@"iCloudData = %@", iCloudData);
        
        NSMutableDictionary *options = [NSMutableDictionary dictionary];
        [options setObject:[NSNumber numberWithBool:YES] forKey:NSMigratePersistentStoresAutomaticallyOption];
        [options setObject:[NSNumber numberWithBool:YES] forKey:NSInferMappingModelAutomaticallyOption];
        [options setObject:iCloudEnabledAppID            forKey:NSPersistentStoreUbiquitousContentNameKey];
        [options setObject:iCloudLogsPath                forKey:NSPersistentStoreUbiquitousContentURLKey];
        
        [psc lock];
        
        self.iCloudStore = [psc addPersistentStoreWithType:NSSQLiteStoreType
                          configuration:nil
                                    URL:[NSURL fileURLWithPath:iCloudData]
                                options:options
                                  error:nil];
        
        [psc unlock];
            
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:@"iCloudReady" object:self userInfo:nil];
        });
    });
    
    return _cloudPersistentStoreCoordinator;
}

-(NSManagedObjectContext*)cloudManagedObjectContext
{
    if (_cloudManagedObjectContext != nil)  return _cloudManagedObjectContext;
    
    NSPersistentStoreCoordinator *coordinator = self.cloudPersistentStoreCoordinator;
    if (coordinator != nil)
    {
        _cloudManagedObjectContext = [[NSManagedObjectContext alloc] init];
        
        // Persistent Store Coordinator 연결.
        [_cloudManagedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    
    return _cloudManagedObjectContext;
}

-(void)removeCloudProperties
{
    self.iCloudStore = nil;
    _cloudManagedObjectContext = nil;
    _cloudPersistentStoreCoordinator = nil;
}
#pragma mark

-(UserConfig*)userConfig
{
    if (nil == _userConfig){
        _userConfig = [[UserConfig alloc] init];
        [_userConfig loadConfig];
    }
    
    return _userConfig;
}


#pragma mark Facebook

-(FacebookManager*)facebookManager
{
    if (nil == _facebookManager) {
        _facebookManager = [[FacebookManager alloc]init];
    }
    
    return _facebookManager;
}

// In order to process the response you get from interacting with the Facebook login process,
// you need to override application:openURL:sourceApplication:annotation:
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    
    self.openedURL = url;
    
    [[FBSession activeSession] handleOpenURL:url];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:APP_HANDLED_URL object:url];
    
//    return YES;
    
    //CAReward
//    if (NO == [[self CountryCode] isEqualToString:@"JP"]) return YES;
//    
//    if(([url query] == nil) || [[url query] isEqualToString:@""]){
//        return YES; }
//    [CARController init];
//    [CARController setNumberOfRequest:1];
//    [CARController notifyAppLaunchToCAR:kCAR_APPKEY cid:kCAR_CID pid:kCAR_PID cpi:kCAR_CPI query:[url query] urlScheme:YES];
    return YES;
}

- (void)applicationWillTerminate:(UIApplication *)application {
        // FBSample logic
    // if the app is going away, we close the session if it is open
    // this is a good idea because things may be hanging off the session, that need
    // releasing (completion block, etc.) and other components in the app may be awaiting
    // close notification in order to do cleanup
//    [self.session close];
    [[FBSession activeSession] close];
    
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    
//    application.applicationIconBadgeNumber = 0;
//    [application cancelAllLocalNotifications];
    
    [self clearNotificationFromNotificationCenter];
    [FBAppEvents activateApp];
    
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
    
    // FBSample logic
    // We need to properly handle activation of the application with regards to SSO
    //  (e.g., returning from iOS 6.0 authorization dialog or from fast app switching).
    //    [FBAppCall handleDidBecomeActiveWithSession:self.session];
    
    [FBSession.activeSession handleDidBecomeActive];
    //    UIViewController *vc = [application keyWindow].rootViewController;
    
    HomeViewController *homeViewController = (HomeViewController*)self.window.rootViewController;
    [homeViewController becameActive];
}


#pragma mark Sound
-(void)loadingBackMusic
{
    if (self.player == nil) {
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"dole_bgm_main_var4" ofType:@"mp3"];
        NSURL *fileURL = [NSURL fileURLWithPath:filePath];
        NSError *error;
        self.player = [[AVAudioPlayer alloc]initWithContentsOfURL:fileURL error:&error];
        [self.player setNumberOfLoops:-1];
        [self.player setVolume:1.0];
        [self.player prepareToPlay];
        
        if(self.userConfig.backgroundMusicOn)
        {
            [self playBackMusic];
        }
        else{
            [self stopBackMusic];
        }
    }
}
-(void)playBackMusic
{
    [self.player play];
}
-(void)stopBackMusic
{
     [self.player stop];
    [self.player setCurrentTime:0.0f];
}


-(void)pauseBackMusic
{
     [self.player pause];
}
-(void)restartMusic
{
    [self stopBackMusic];
    [self playBackMusic];
}

@end
