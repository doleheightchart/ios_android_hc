//
//  HomeViewController.m
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 18..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//
#import <FacebookSDK/FacebookSDK.h>
#import "AppDelegate.h"
#import "HomeViewController.h"
#import "UIView+ImageUtil.h"
#import "UIViewController+DoleHeightChart.h"
#import "UIView+Animation.h"
#import "UIColor+ColorUtil.h"
#import "UIButton+CheckAndRadio.h"
#import "HomeToolBar.h"
#import "HomeBackgroundView.h"
#import "NicknameViewController.h"
#import "ToastController.h"
#import "ChooseNicknamePopupController.h"
#import "DoleHomeScrollView.h"
#import "HeightDetailViewController.h"
#import "MyChild.h"
#import "ChildHeight.h"
#import "QRScanViewController.h"
#import "ConfirmPopupController.h"
#import "DoleCoinViewController.h"
#import "GuideView.h"
#import "UIViewController+PolyServer.h"
#import "NSObject+HeightDatabase.h"
#import "ChooseNicknameBox.h"
#import "MessageBoxController.h"
#import "LogInViewController.h"
#import "EventBannerViewController.h"
#import "NSDate+DoleHeightChart.h"

#import "Mixpanel.h"

@interface HomeViewController (){
    
    UIImageView *_bottomGrassImageView;
    
    //Moutain
    BOOL        _didMoutainAni;
    UIImageView *_moutainImgView;
    UIImageView *_moutainTreeView01;
    UIImageView *_moutainTreeView02;
    UIImageView *_moutainTreeView03;
    UIImageView *_moutainTreeView04;
    UIImageView *_moutainMushRoomView01;
    UIImageView *_moutainMushRoomView02;
    UIImageView *_moutainRockView;
    
    UIImageView *_copyRightView;
    
    //ToolBar
//    UIButton    *_addHeightBtn;
//    UIButton    *_deleteNicknameBtn;
//    UIButton    *_settingBtn;
    HomeToolBar *_homeToolBar;
    
    //Title
    UIView      *_titleContainerView;
    UIImageView *_titleImgView;
    UIImageView *_titleMonkeyLeftView;
    UIImageView *_titleMonkeyRightView;
    UIImageView *_titleGiraffeView;
    
    //Background
    HomeBackgroundView *_backgroundView;
    
    //Forest
    DoleHomeScrollView *_monkeyScrollView;
    
    //Core Data
//    NSFetchedResultsController  *_fetchedController;
    
    //Server
    NSMutableData   *_recevedBufferData;


}
@property (weak, nonatomic) IBOutlet FBLoginView *fbLogin;

@property (nonatomic) BOOL isDeleteMode;

@end

@implementation HomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    NSString *cll = self.currentLanguageCode;
//    NSString *cl2 = self.currentCountryCode;
//    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
//    NSLocale *lo = [NSLocale currentLocale];
//    NSString *ln = self.currentLanguageName;
    
    
    [UIApplication sharedApplication].statusBarHidden = YES;

    [self createBackgroundView];
    
    UIImageView *doleLogMiniView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"title_height_dole_logo"]];
    doleLogMiniView.frame = CGRectMake(0, 0, 116/2, 72/2);
    [self.view addSubview:doleLogMiniView];
    
    [self createForest];
    [self createMoutain];
//    [self createBottomGrass];
    [self createToolBar];
    [self createTitle];
    
//    [self moveUpMoutain];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handledURL:) name:APP_HANDLED_URL object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadAnimation:) name:UIApplicationDidBecomeActiveNotification object:self];
    
}

- (void)handledURL:(NSNotification*)notification{
//    id obj = notification;
    NSLog(@"handledURL==================================================");
}

- (void)loginViewFetchedUserInfo:(FBLoginView *)loginView
                            user:(id<FBGraphUser>)user {

//    NSLog(@"Login User = %@  name = %@", user.id, user.name);
}

- (void)viewDidAppear:(BOOL)animated
{
    
    if (_didMoutainAni == NO){
        _didMoutainAni = YES;
        [self moveUpMoutain];
        //[self moveUpTitle];
    }

    [self reloadAnimation:nil];
    
    //돌코인 업데이트 룰을 적용한다.  아직 몰라서 화면 갱신만 하고있음
    if(self.userConfig.loginStatus == kUserLogInStatusDoleLogOn)
    {
        _homeToolBar.doleCoinCount = self.userConfig.doleCoin;
    }
    
    
    // JPY 06202015: Weve undergone a system migration popup removed
    /*
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString* savedEmail = [[NSUserDefaults standardUserDefaults] stringForKey:@"Email"];
    NSString *majorVersion = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    if ((([majorVersion isEqualToString: @"1.4.2"] || [majorVersion isEqualToString: @"2.0.0"]) &&
         ![[NSUserDefaults standardUserDefaults] boolForKey:@"SystemMigrationWarningShown"])
        && savedEmail == NULL)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:NSLocalizedString(@"Weve undergone a system migration", "")
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"Close", "") otherButtonTitles:nil, nil];
        [alert show];
     
    }
    if (savedEmail) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"SystemMigrationWarningShown"];
    }
    */
}

- (void)viewWillAppear:(BOOL)animated
{
    // JP added post-1.4.2
    [[Mixpanel sharedInstance] track:@"Landing"];
    
    if (self.currentStatus == kHomeStatusDidVersionCheck){
        self.currentStatus = kHomeStatusDidFirstTimeCheck;
    }
    
    if (self.userConfig.accountStatus != kNotSingUp){
        if (self.userConfig.needQueryDoleCoin)
            [self requestDoleCoinQuery];
        else
            _homeToolBar.doleCoinCount = self.userConfig.doleCoin;
    }
}

-(void)reloadAnimation:(id)sender
{
    [_backgroundView startScrolling];
}

-(void)viewWillDisappear:(BOOL)animated
{
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)createBackgroundView
{
    _backgroundView = [[HomeBackgroundView alloc]initWithFrame:self.view.bounds];
    [self.view addSubview:_backgroundView];
}

-(void)createForest
{
    _monkeyScrollView = [[DoleHomeScrollView alloc]initWithFrame:self.view.bounds];
    _monkeyScrollView.datasource = self;
    _monkeyScrollView.homeListDelegate = self;
    
    [self.view addSubview:_monkeyScrollView] ;
}

-(void)createTitle
{
//    CGFloat startY = ((261+30+140+111)/2)/2 + (261/2/2);
    CGFloat startY = 312/2;

    CGFloat w = self.view.bounds.size.width;
    
    _titleContainerView = [[UIView alloc]initWithFrame:CGRectMake(0 - (w/2), startY , self.view.bounds.size.width, 261/2)];
//    _titleContainerView.backgroundColor = [UIColor colorWithRGB:0x00ff00 alpha:0.3];
    

//    _titleGiraffeView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"title_height_giraffe"]];
    
//    CGRect f1 = CGRectMake(115/2, 0, 410/2, 261/2);
    CGRect f1 = CGRectMake(115/2, 0, 410/2, 261/2);
    CGRect f15 = CGRectMake(115/2 - (410/2/2), (261/2/2), 410/2, 261/2);
    CGRect f2 = CGRectMake(58/2, (80/2), 117/2, 131/2);
    CGRect f3 = CGRectMake((58+117+277)/2, 0, 107/2, 135/2);

    _titleImgView = [[UIImageView alloc] initWithFrame:f1];
    _titleMonkeyLeftView = [[UIImageView alloc] initWithFrame:f2];
    _titleMonkeyRightView = [[UIImageView alloc] initWithFrame:f3];
    _titleGiraffeView = [[UIImageView alloc] initWithFrame:f15];
    
    _titleGiraffeView.layer.anchorPoint = CGPointMake(0, 1);
    
    _titleImgView.image = [UIImage imageNamed:@"sp_title_height_chart"];
    _titleGiraffeView.image = [UIImage imageNamed:@"title_height_giraffe"];
//    _titleMonkeyLeftView.image = [UIImage imageNamed:@"sp_gorilla_01"];
//    _titleMonkeyRightView.image = [UIImage imageNamed:@"sp_monkey_01"];
    
//    [self.view addSubview:_titleMonkeyLeftView];
    
    UIImage *left_frame01 = [UIImage imageNamed:@"sp_gorilla_01"];
    UIImage *left_frame02 = [UIImage imageNamed:@"sp_gorilla_02"];
    _titleMonkeyLeftView.animationImages = @[left_frame01, left_frame02];
    _titleMonkeyLeftView.animationRepeatCount = 0;
    _titleMonkeyLeftView.animationDuration = 0.4;
    [_titleMonkeyLeftView startAnimating];

    UIImage *right_frame01 = [UIImage imageNamed:@"sp_monkey_01"];
    UIImage *right_frame02 = [UIImage imageNamed:@"sp_monkey_02"];
    _titleMonkeyRightView.animationImages = @[right_frame01, right_frame02];
    _titleMonkeyRightView.animationRepeatCount = 0;
    _titleMonkeyRightView.animationDuration = 0.4;
    [_titleMonkeyRightView startAnimating];
    
    [_titleContainerView addSubview:_titleGiraffeView ];
    [_titleContainerView addSubview:_titleMonkeyLeftView ];
    [_titleContainerView addSubview:_titleImgView];
    [_titleContainerView addSubview:_titleMonkeyRightView];

    _titleContainerView.layer.anchorPoint = CGPointMake(0, 1);
    
    [self.view addSubview:_titleContainerView];
    
//    [UIView animateKeyframesWithDuration:1
//                                   delay:0.2
//                                 options:UIViewKeyframeAnimationOptionAutoreverse
//                              animations:^{
//                                  _titleGiraffeView.layer.transform = CATransform3DMakeRotation(-3*M_PI/180, 0, 0, 1);
//                              }
//                              completion:^(BOOL finished){
//                                  _titleGiraffeView.layer.transform = CATransform3DMakeRotation(0, 1, 1, 1);
//                              }];
    
    [UIView animateWithDuration:1 delay:0.2 options:(UIViewAnimationOptionAutoreverse) animations:^{
        _titleGiraffeView.layer.transform = CATransform3DMakeRotation(-3*M_PI/180, 0, 0, 1);
    } completion:^(BOOL finished) {
        _titleGiraffeView.layer.transform = CATransform3DMakeRotation(0, 1, 1, 1);
    }];
}

-(CAAnimation*)animationMoveupWithView:(UIView*)v Index:(NSInteger)index
{
    CGMutablePathRef thePath = CGPathCreateMutable();

    CAKeyframeAnimation *moveup = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    CGFloat py = v.layer.position.y - v.bounds.size.height;
    CGFloat px = v.layer.position.x;
    CGPathMoveToPoint(thePath,NULL,v.layer.position.x,v.layer.position.y);
//    CGPathAddLineToPoint(thePath, NULL, px, py * 0.99); //틈이 보이는게 어색하다고 함
    CGPathAddLineToPoint(thePath, NULL, px, py);
    CGPathAddLineToPoint(thePath, NULL, px, py);
    //CGPathCloseSubpath(thePath);
    moveup.removedOnCompletion = NO;
    moveup.fillMode = kCAFillModeForwards;
    moveup.duration = 0.3;
    moveup.beginTime = CACurrentMediaTime() + (index*0.2);
    moveup.path = thePath;
    CGPathRelease(thePath);

    //v.layer.position = CGPointMake(px, py);

    return moveup;
}




-(void)moveUpMoutain
{

    NSArray *array = @[_moutainTreeView01, _moutainImgView, _moutainTreeView02, _moutainTreeView03, _moutainTreeView04, _moutainRockView, _moutainMushRoomView01, _moutainMushRoomView02];

//    for (UIView *vv in array) {
//        vv.center = CGPointMake(vv.center.x, vv.center.y + vv.bounds.size.height);
//    }


    [CATransaction begin];
    [CATransaction setAnimationDuration:3];
    [CATransaction setCompletionBlock:^{
        for (UIView *vv in array) {
            vv.center = CGPointMake(vv.center.x, vv.center.y - (vv.bounds.size.height));
        }

        [self moveUpTitle];

    }];

    for (int i = 0; i < array.count; i++) {
        UIView *v = array[i];

        CAAnimation *moveup =[self animationMoveupWithView:v Index:i];
        [v.layer addAnimation:moveup forKey:@"position"];

        //scale

        CAKeyframeAnimation *scaleEffect = [CAKeyframeAnimation
                                            animationWithKeyPath:@"transform"];

        CATransform3D scale1 = CATransform3DMakeScale(0.5, 0.5, 1);
        CATransform3D scale2 = CATransform3DMakeScale(1.2, 1.2, 1);
        CATransform3D scale3 = CATransform3DMakeScale(0.9, 0.9, 1);
        CATransform3D scale4 = CATransform3DMakeScale(1.0, 1.0, 1);

        NSArray *frameValues = [NSArray arrayWithObjects:
                                [NSValue valueWithCATransform3D:scale1],
                                [NSValue valueWithCATransform3D:scale2],
                                [NSValue valueWithCATransform3D:scale3],
                                [NSValue valueWithCATransform3D:scale4],
                                nil];
        [scaleEffect setValues:frameValues];//Courtesy zoul.fleuron.cz

        NSArray *frameTimes = [NSArray arrayWithObjects:
                               [NSNumber numberWithFloat:0.0],
                               [NSNumber numberWithFloat:0.5],
                               [NSNumber numberWithFloat:0.9],
                               [NSNumber numberWithFloat:1.0],
                               nil];
        [scaleEffect setKeyTimes:frameTimes];

        scaleEffect.fillMode = kCAFillModeForwards;
        scaleEffect.removedOnCompletion = NO;
        scaleEffect.beginTime = moveup.beginTime;
        scaleEffect.duration = moveup.duration;

        [v.layer addAnimation:scaleEffect forKey:@""];
    }

    [CATransaction commit];

//    for (int i = 0; i < array.count; i++) {
//        UIView *v = array[i];
//
//        CAAnimation *moveup =[self animationMoveupWithView:v Index:i];
//
//        if (i == array.count - 1){
//            moveup.delegate = self;
//            [moveup setValue:@"end" forKey:@"name"];
//        }
//        
//        [v.layer addAnimation:moveup forKey:@"position"];
//
//        //scale
//        
//        CAKeyframeAnimation *scaleEffect = [CAKeyframeAnimation
//                                          animationWithKeyPath:@"transform"];
//        
//        CATransform3D scale1 = CATransform3DMakeScale(0.5, 0.5, 1);
//        CATransform3D scale2 = CATransform3DMakeScale(1.2, 1.2, 1);
//        CATransform3D scale3 = CATransform3DMakeScale(0.9, 0.9, 1);
//        CATransform3D scale4 = CATransform3DMakeScale(1.0, 1.0, 1);
//        
//        NSArray *frameValues = [NSArray arrayWithObjects:
//                                [NSValue valueWithCATransform3D:scale1],
//                                [NSValue valueWithCATransform3D:scale2],
//                                [NSValue valueWithCATransform3D:scale3],
//                                [NSValue valueWithCATransform3D:scale4],
//                                nil];
//        [scaleEffect setValues:frameValues];//Courtesy zoul.fleuron.cz
//        
//        NSArray *frameTimes = [NSArray arrayWithObjects:
//                               [NSNumber numberWithFloat:0.0],
//                               [NSNumber numberWithFloat:0.5],
//                               [NSNumber numberWithFloat:0.9],
//                               [NSNumber numberWithFloat:1.0],
//                               nil];
//        [scaleEffect setKeyTimes:frameTimes];
//        
//        scaleEffect.fillMode = kCAFillModeForwards;
//        scaleEffect.removedOnCompletion = NO;
//        scaleEffect.beginTime = moveup.beginTime;
//        scaleEffect.duration = moveup.duration;
//        
//        [v.layer addAnimation:scaleEffect forKey:@""];
////        CAAnimationGroup *group = [CAAnimationGroup animation];
////        group.animations = @[moveup, scaleEffect];
////        group.duration = 0.3;
////        group.fillMode = kCAFillModeForwards;
////        group.removedOnCompletion = YES;
////        [v.layer addAnimation:group forKey:@"group"];
//
//    }

}

-(void)moveUpTitle
{
    [UIView animateWithDuration:1 animations:^{
        _titleContainerView.center = CGPointMake(_titleContainerView.center.x, -350);
        [_copyRightView removeFromSuperview];
    } completion:^(BOOL finished) {
        
        _titleImgView.image = [UIImage imageNamed:@"title_height_chart"];
        
        [self moveDownTitle];
    }];
    
    [_homeToolBar playSplashAnimation];
}

-(void)moveDownTitle
{
    [_titleMonkeyLeftView removeFromSuperview];
    [_titleMonkeyRightView removeFromSuperview];
    
    CGFloat toY = 80 + (130/2);
    
    
    if ([UIScreen isFourInchiScreen] == NO){
        
        _titleImgView.image = [UIImage imageNamed:@"title_height_chart_960"];
        _titleImgView.bounds = CGRectMake(0, 0, 410/2, 178/2);
        _titleGiraffeView.image = nil;
        
        toY = 30 + (178/2);
    }
    
//    _titleContainerView.layer.transform = CATransform3DMakeRotation(-5*M_PI/180, 0, 0, 1);
    
    [UIView animateWithDuration:1.6 animations:^{
        _titleContainerView.center = CGPointMake(_titleContainerView.center.x, toY);
    } completion:^(BOOL finished) {
        
        [UIView animateWithDuration:0.3 animations:^{
            _titleContainerView.layer.transform = CATransform3DMakeRotation(-3*M_PI/180, 0, 0, 1);
        } completion:^(BOOL finished) {
            
            [UIView animateWithDuration:0.5 animations:^{
                _titleContainerView.layer.transform = CATransform3DMakeRotation(3*M_PI/180, 0, 0, 1);
            } completion:^(BOOL finished) {
                
                [UIView animateWithDuration:0.5 animations:^{
                    _titleContainerView.layer.transform = CATransform3DMakeRotation(0, 1, 1, 1);
                } completion:^(BOOL finished){
                    [self didSplashAnimation];
                }];
            }];
        }];
        
    }];
}

-(void)createCopyRight
{
//    _copyRightView = [UIImageView alloc]initWithImage:[UIImage imageNamed:@""];
}

- (void)createBottomGrass
{
    // 26 * 111
    _bottomGrassImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 568-(111/2), 320, 111/2)];
    
    UIImage *image = [UIImage resizableImageWithName:@"bottom_grass" CapInsets:UIEdgeInsetsMake(0, 26/2, 0, 26/2)];
    
    _bottomGrassImageView.image = image;
 
    [self.view addSubview:_bottomGrassImageView];
    
    UIImageView *g1 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"grass_01"]];
    g1.frame = CGRectMake(35/2, 0, 320-35, 111/2);
    [_bottomGrassImageView addSubview:g1];
    
//    _bottomGrassImageView.layer.anchorPoint = CGPointMake(0.5, 0);
//    _bottomGrassImageView.transform = CGAffineTransformMakeScale(1, 0.5);
}

- (void)createMoutain
{
    CGFloat oy = self.view.bounds.size.height - (98/2) + 2;
    CGFloat w = self.view.bounds.size.width;
    CGFloat h = self.view.bounds.size.height;
    
    _copyRightView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"copyright"]];
//    _copyRightView.frame = CGRectMake((w - (410/2))/2, h - (111+140+30)/2, 410/2, 30/2);
     _copyRightView.frame = CGRectMake((w - (528/2))/2, h - (111+140+30)/2, 528/2, 30/2);
    [self.view addSubview:_copyRightView];
    
    
    CGRect frameMoutainBG = { {0,oy-(126/2)}, {640/2,126/2} }; // origin, size
    
    CGRect frameTree01 = { {44/2,oy-(99/2)}, {41/2,99/2} }; // origin, size
    CGRect frameTree02 = { {165/2,oy-96/2}, {67/2,96/2} }; // origin, size
    CGRect frameTree03 = { {w-(62+281)/2,oy-(69/2)}, {62/2,69/2} }; // origin, size
    CGRect frameTree04 = { {(113/2),oy-(51/2)}, {192/2,51/2} }; // origin, size
    
    CGRect frameMushRoom1 = { {75/2,oy -(31/2)}, {(31/2),(31/2)} }; // origin, size
    CGRect frameMushRoom2 = { {(96/2),oy-(46/2)}, {(47/2),(46/2)} }; // origin, size
    CGRect frameRocks = { {w-(218+87)/2,oy-(58/2)}, {(218/2),(58/2)} }; // origin, size
    
    
    _moutainImgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bottom_bg"]];
    
    _moutainTreeView01 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bottom_tree_01_bg"]];
    _moutainTreeView02 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bottom_tree_02_bg"]];
    _moutainTreeView03 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bottom_tree_03_bg"]];
    _moutainTreeView04 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bottom_tree_04_bg"]];
    
    _moutainMushRoomView01 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bottom_mushroom_01_bg"]];
    _moutainMushRoomView02 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bottom_mushroom_02_bg"]];

    _moutainRockView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bottom_rocks_bg"]];
    
    _moutainImgView.frame = frameMoutainBG;
    _moutainTreeView01.frame = frameTree01;
    _moutainTreeView02.frame = frameTree02;
    _moutainTreeView03.frame = frameTree03;
    _moutainTreeView04.frame = frameTree04;
    
    _moutainMushRoomView01.frame = frameMushRoom1;
    _moutainMushRoomView02.frame = frameMushRoom2;
    
    _moutainRockView.frame = frameRocks;
    
    [self.view addSubview:_moutainTreeView01];
    [self.view addSubview:_moutainImgView];
    [self.view addSubview:_moutainTreeView03];
    [self.view addSubview:_moutainTreeView04];
    [self.view addSubview:_moutainTreeView02];
    [self.view addSubview:_moutainRockView];
    [self.view addSubview:_moutainMushRoomView02];
    [self.view addSubview:_moutainMushRoomView01];
    
    NSMutableArray *array = [NSMutableArray arrayWithArray:@[_moutainTreeView01,
                                                             _moutainImgView,
                                                             _moutainTreeView03,
                                                             _moutainTreeView04,
                                                             _moutainTreeView02,
                                                             _moutainRockView,
                                                             _moutainMushRoomView02,
                                                             _moutainMushRoomView01]];

    for (UIView *vv in array) {
        vv.center = CGPointMake(vv.center.x, vv.center.y + vv.bounds.size.height);
    }

}

-(void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    NSLog(@"DidStop animation is %@", [anim valueForKey:@"name"] );

    if (flag){
        NSLog(@"DidStop animation is %@", [anim valueForKey:@"name"] );
        if ([[anim valueForKey:@"name"] isEqualToString:@"end"]){
            [self moveUpTitle];
        }
    }
}
-(void)animationDidStart:(CAAnimation *)anim
{
    NSLog(@"DidStart animation is %@", [anim valueForKey:@"name"] );
}

- (void)createToolBar
{
//    CGFloat w = self.view.bounds.size.width;
//    CGFloat h = self.view.bounds.size.height;
//    
//    _addHeightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    _addHeightBtn.adjustsImageWhenHighlighted = NO;
//    _addHeightBtn.backgroundColor = [UIColor clearColor];
//    [_addHeightBtn setBackgroundImage:[UIImage imageNamed:@"add_height_btn_normal"] forState:UIControlStateNormal];
//    [_addHeightBtn setBackgroundImage:[UIImage imageNamed:@"add_height_btn_press"] forState:UIControlStateHighlighted];
//    _addHeightBtn.frame = CGRectMake(w - (44*3)-7, h - (88+8)/2, 44, 44);
//    [self.view addSubview:_addHeightBtn];
//    
//    _deleteNicknameBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    _deleteNicknameBtn.adjustsImageWhenHighlighted = NO;
//    _deleteNicknameBtn.backgroundColor = [UIColor clearColor];
//        [_deleteNicknameBtn setImage:[UIImage imageNamed:@"delete_btn_cov_01_normal"] forState:UIControlStateNormal];
//    [_deleteNicknameBtn setBackgroundImage:[UIImage imageNamed:@"delete_btn_normal"] forState:UIControlStateNormal];
//    [_deleteNicknameBtn setBackgroundImage:[UIImage imageNamed:@"delete_btn_press"] forState:UIControlStateHighlighted];
//    _deleteNicknameBtn.frame = CGRectMake(w - (44*2)-7, h - (88+8)/2, 44, 44);
//    [self.view addSubview:_deleteNicknameBtn];
//    
//    _settingBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    _settingBtn.adjustsImageWhenHighlighted = NO;
//    _settingBtn.backgroundColor = [UIColor clearColor];
//    [_settingBtn setBackgroundImage:[UIImage imageNamed:@"option_menu_btn_normal"] forState:UIControlStateNormal];
//    [_settingBtn setBackgroundImage:[UIImage imageNamed:@"option_menu_btn_press"] forState:UIControlStateHighlighted];
//    _settingBtn.frame = CGRectMake(w - (44*1)-7, h - (88+8)/2, 44, 44);
//    [self.view addSubview:_settingBtn];
//    
//    [_addHeightBtn animateScaleEffectWithDuration:0.3 MinScale:0.5 MaxScale:1.2 completionBlock:nil];
//    [_deleteNicknameBtn animateScaleEffectWithDuration:0.3 MinScale:0.5 MaxScale:1.2 completionBlock:nil];
//    [_settingBtn animateScaleEffectWithDuration:0.3 MinScale:0.5 MaxScale:1.2 completionBlock:nil];
//    
//    [_addHeightBtn setupSizeWhenPressed];
//    [_deleteNicknameBtn setupSizeWhenPressed];
//    [_settingBtn setupSizeWhenPressed];
//    
//    [_deleteNicknameBtn addTarget:self action:@selector(deleteNickname:) forControlEvents:UIControlEventTouchUpInside];
    
    _homeToolBar = [[HomeToolBar alloc]initWithFrame:CGRectMake(0, self.view.bounds.size.height - 111/2, self.view.bounds.size.width, 111/2)];
    [self.view addSubview:_homeToolBar];
    
    _homeToolBar.toolBarDelegate = self;
}

- (void)setIsDeleteMode:(BOOL)isDeleteMode
{
    _isDeleteMode = isDeleteMode;
    
    _homeToolBar.isDeleteMode = _isDeleteMode;
    _backgroundView.isDeleteMode = _isDeleteMode;
    _monkeyScrollView.isDeleteMode = _isDeleteMode;
    
    if (_isDeleteMode) {
        [ToastController showToastWithMessage: NSLocalizedString(@"Choose a nickname to delete.", "") duration:kToastMessageDurationType_Auto];
    }
    

}

- (void)setCurrentStatus:(HomeStatus)currentStatus
{
    _currentStatus = currentStatus;
    
//    kHomeStatusNone,
//    kHomeStatusFinishedSplash,
//    kHomeStatusNetworkCheck,
//    kHomeStatusVersionCheck,
//    kHomeStatusEventCheck,
//    kHomeStatusLogIn,
//    kHomeStatusDidLogIn,
//    kHomeStatusDidLogOff,

    dispatch_async(dispatch_get_main_queue(), ^{

        switch (_currentStatus) {
            case kHomeStatusFinishedSplash:
                if (self.isEnableNetwork){
                    //Commented out:
                    [self versionCheck];
                    //self.currentStatus = kHomeStatusDidVersionCheck;
                }
                else{
                    [self toastNetworkError];
                    self.currentStatus = kHomeStatusDidLogOff;
                }
                break;
            case kHomeStatusNetworkCheck:
                break;
            case kHomeStatusDidVersionCheck:
                [self checkFirstLaunch];
                break;
            case kHomeStatusDidFirstTimeCheck:{
                /*
                    if ([self.userConfig shownEventBanner])
                        [self requestEventQuery];
                    else{
                        self.currentStatus = kHomeStatusDidEventCheck;
                    }
                 */
                    self.currentStatus = kHomeStatusDidEventCheck;
                }
                break;
            case kHomeStatusDidEventCheck:
                [self doLogIn];
                break;
            case kHomeStatusLogIn:
                break;
            case kHomeStatusDidLogOff:
                [self initDB];
                break;
            case kHomeStatusDidLogIn:
                [self initDB];
                [self requestDoleCoinQuery];
                break;
                
            default:
                break;
        }
    });
}

-(void)initDB
{
    [self addObserverSelfChangeDBStore];
    
    if (self.userConfig.useiCloud){
        
        if ([self canUseiCloud] == NO){
            
            [self.userConfig canNotUseiCloud];
            
            NSString *message = NSLocalizedString(@"Synchronization failed.\nPlease check iCloud setting.", @"");
            [self showOKConfirmMessage:message completion:nil];
            [self reloadMonkeys];
        }
        else{
            [self registerForiCloudNotifications];
        }
    }
    else{
        [self reloadMonkeys];
    }
}


-(void)versionCheck
{
    [self requestVersionCheck];
}

-(void)checkFirstLaunch
{
    if (self.userConfig.isFirstTimeAppLaunch){
        
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"LogIn" bundle:nil];
        LogInViewController *vc = [story instantiateViewControllerWithIdentifier:@"LogIn"];
        vc.appFirstTimeLogIn = TRUE;
        UINavigationController *navi = [[UINavigationController alloc]initWithRootViewController:vc];
        navi.navigationBarHidden = YES;
        [self modalTransitionController:navi Animated:YES];
        
        
        //self.currentStatus = self.currentStatus = kHomeStatusDidLogOff;
        [self.userConfig markFirstLaunch];
        
    }
    else{
        
        self.currentStatus = kHomeStatusDidFirstTimeCheck;
    }
}

-(void)doLogIn
{
    if (self.userConfig.accountStatus == kSignUpByEmail){
        [self loginByEmail];
    }
    else if (self.userConfig.accountStatus == kSignUpByFacebook){
        self.facebookManager.delegate = self;
        [self.facebookManager login];
    }
    else{
        self.currentStatus = kHomeStatusDidLogOff;
    }
}

- (void)didSplashAnimation
{
    self.currentStatus = kHomeStatusFinishedSplash;
    
    
//    [self addObserverSelfChangeDBStore];
//    
//    if (self.userConfig.useiCloud){
//        [self registerForiCloudNotifications];
//        
//    }
//    
//    //[self.facebookManager logout];
//    
//    if (self.isEnableNetwork){
//        
//        [self requestVersionCheck];
//               
//        if (self.userConfig.accountStatus == kSignUpByEmail){
//            [self loginByEmail];
//        }
//        else if (self.userConfig.accountStatus == kSignUpByFacebook){
////            [_monkeyScrollView reloadMonkeyTree];
//            self.facebookManager.delegate = self;
//            [self.facebookManager login];
//        }
//        else{
//            if (self.userConfig.useiCloud == NO){
////                [_monkeyScrollView reloadMonkeyTree];
//                [self reloadMonkeys];
//
//            }
//        }
//    }
//    else {
////        [_monkeyScrollView reloadMonkeyTree];
//        [self reloadMonkeys];
//
//    }
//

}

- (void)addNonameChild
{
    NSManagedObjectContext *context = self.usedObjectContext;
    MyChild *newObj = [NSEntityDescription insertNewObjectForEntityForName:@"MyChild"
                                                    inManagedObjectContext:context];
    
//    newObj.nickname = @"Noname";
    NSString *freeNoname = [self getFreeName];
    newObj.nickname = freeNoname;

    newObj.charterNo = [NSNumber numberWithShort:kDoleCharacterTypeDolekey];
    newObj.createdDate = [NSDate date];
    
    newObj.birthday = [NSDate date];
    newObj.isGirl = NO;
    
    NSError *error;
    if ([context save:&error]){
        NSLog(@"Saved ....");
        [self loadDB];
        [_monkeyScrollView addNewMonkeyWithMyChildInfo:newObj];
        
        NSInteger index = [self.fetchedResultsController.fetchedObjects indexOfObject:newObj];
        
        [self showHeightDetailViewWithChildIndex:index startAddHeight:YES];
    }
    else{
        NSLog(@"%@", error);
    }
}

-(NSString*)getFreeName
{
    NSString *name = @"Noname";
    
    int dupCount = 0;
    BOOL existDupnameChild = YES;
    
    while (existDupnameChild) {
        existDupnameChild = NO;
        for (MyChild *child in self.fetchedResultsController.fetchedObjects) {
            if ([child.nickname isEqualToString:name]){
                existDupnameChild = YES;
                break;
            }
        }
        
        if (existDupnameChild){
            dupCount++;
            name = [NSString stringWithFormat:@"Noname(%d)", dupCount, nil];
        }
    }
    
    return name;
}

#pragma mark Dole Server

- (void)loginByEmail
{
    NSString *email = self.userConfig.email;
    NSString *password = self.userConfig.password;
    
    [self requestLogInWithUserID:email Password:password];
}

- (void)didConnectionFail:(NSError *)error
{
    [self toastNetworkError];
    
//    [_monkeyScrollView reloadMonkeyTree];
//    [self reloadMonkeys];
    
    self.currentStatus = kHomeStatusDidLogOff;
}

- (void)didCompleteRecevedWithResultType:(DoleServerResultType)resultType ParsedData:(NSDictionary *)recevedData ParseError:(NSError *)error
{
    if (!error){
        if ([recevedData[@"Return"] boolValue]){
            
            switch (resultType) {
                case kDoleServerResultTypeCheckAppVersion:[self handleServerCheckVersionWithParsedData:recevedData ParseError:error];
                    break;
                case kDoleServerResultTypeEventInfo:[self handleServerCheckEventWithParsedData:recevedData ParseError:error];
                    break;
                case kDoleServerResultTypeGetDoleCoin:[self handleServerQueryDoleCoinWithParsedData:recevedData ParseError:error];
                    break;
                case kDoleServerResultTypeLogIn:[self handleServerEmailLogInWithParsedData:recevedData ParseError:error];
                    break;
                case kDoleServerResultTypeSnsLogIn:[self handleServerSnsLogInWithParsedData:recevedData ParseError:error];
                    break;
                case kDoleServerResultTypeRenewAuthkey:[self handleServerRenewAuthkeyWithParsedData:recevedData ParseError:error];
                    break;
                default:
                    assert(0);
                    break;
            }
            
//            if (resultType == kDoleServerResultTypeLogIn){
//            
//                NSNumber *userNo = recevedData[@"UserNo"];
//                NSString *authKey = recevedData[@"AuthKey"];
//                
//                if (userNo && authKey){
//                    self.userConfig.authKey = authKey;
//                    self.userConfig.userNo = [userNo integerValue];
//                    
//                    self.userConfig.loginStatus = kUserLogInStatusDoleLogOn;
//                    
//                    [ToastController showToastWithMessage:@"로그인 완료되었습니다." duration:5 completion:^{
//                        [self showEventBanner];
//                        [self requestDoleCoinQuery];
//                        
//                        //[self loadDB];
//                        
//                        if (_fetchedResultsController == nil){
//                            //[self loadDB];
//                            [self reloadFetchedResults:nil];
//                        }
//                    }];
//                }
//                else{
//                    NSInteger errorCode = [recevedData[@"ReturnCode"] integerValue];
//                    [self toastNetworkErrorWithErrorCode:errorCode];
//                    
//                    [self reloadMonkeys];
//                }
//                
//            }
//            else if (resultType == kDoleServerResultTypeEventInfo){
//                
//                
////                "Return":true,
////                "ReturnCode":2147483647,
////                "NoticeItems":[{
////                    "Content":"String content",
////                    "ContentGroupClassNo":2147483647,
////                    "ContentGroupNo":2147483647,
////                    "ContentNo":2147483647,
////                    "ContentTitle":"String content",
////                    "Keyword":"String content",
////                    "LanguageNo":2147483647,
////                    "ServiceCode":"String content",
////                    "Summary":"String content",
////                    "TargetBeginDateTime":"String content",
////                    "TargetEndDateTime":"String content"
////                }],
////                "TotalRowCount":2147483647
//                
//                if ([recevedData[@"TotalRowCount"] integerValue] > 0){
//                    NSArray *eventArray = recevedData[@"NoticeItems"];
//                    NSDictionary *event = eventArray.firstObject;
//
//                    //밑에 값이 제대로 들어온다..
//                    NSString *urlBanner = event[@"Content"];
//                    NSLog(@"Content is %@", urlBanner);
//                }
//                
//            }
//            else if (resultType == kDoleServerResultTypeGetDoleCoin){
////                {
////                    "Return":true,
////                    "ReturnCode":2147483647,
////                    "EventBalance":2147483647,
////                    "RealBalance":2147483647
////                }
//                
//                self.userConfig.doleCoin = [recevedData[@"EventBalance"] integerValue];
//                _homeToolBar.doleCoinCount = self.userConfig.doleCoin;
//            }
//            else if (resultType == kDoleServerResultTypeSnsLogIn){
//                NSNumber *no = recevedData[@"UserNo"];
//                NSString *authKey = recevedData[@"AuthKey"];
//
//                if (no && authKey){
//                    self.userConfig.authKey = authKey;
//                    self.userConfig.userNo = [no integerValue];
//                    self.userConfig.loginStatus = kUserLogInStatusFacebook;
//                    
//                    
//                    [self showEventBanner];
//                    [self requestDoleCoinQuery];
//                }
//                
//                if (self.userConfig.useiCloud == NO)
//                    [self reloadMonkeys];
//
//            }
        }
        else{
            NSInteger errorCode = [recevedData[@"ReturnCode"] integerValue];
            [self toastNetworkErrorWithErrorCode:errorCode];
            
//            if (resultType == kDoleServerResultTypeLogIn && resultType == kDoleServerResultTypeSnsLogIn)
//                [self reloadMonkeys];
            
            self.currentStatus = kHomeStatusDidLogOff;

        }
    }
    else{
        [self toastNetworkError];
        
//        if (resultType == kDoleServerResultTypeLogIn && resultType == kDoleServerResultTypeSnsLogIn)
//            [self reloadMonkeys];
        
        self.currentStatus = kHomeStatusDidLogOff;
    }
    
//    if (resultType == kDoleServerResultTypeLogIn)
//        [_monkeyScrollView reloadMonkeyTree];
    
}

- (NSMutableData*)receivedBufferData
{
    if (nil == _recevedBufferData){
        _recevedBufferData = [NSMutableData dataWithCapacity:0];
    }
    
    return _recevedBufferData;
}

#pragma mark PolyServerHandler

-(void)handleServerCheckVersionWithParsedData:(NSDictionary *)recevedData ParseError:(NSError *)error
{
    NSLog(@"CheckVersion data is %@", recevedData);
    
    NSString *majorVersion = recevedData[@"MajorVersion"];
    NSString *appStoreURL = recevedData[@"AppURL"];
    
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString* versionNum = [infoDict objectForKey:@"CFBundleShortVersionString"];

    if (majorVersion){
        
        NSArray *svs = [majorVersion componentsSeparatedByString:@"."];
        NSArray *cvs = [versionNum componentsSeparatedByString:@"."];

        if (svs.count < 2 || cvs.count < 2){
            self.currentStatus = kHomeStatusDidLogOff;
            return;
        }
        
        NSInteger serverMajor = [svs[0] integerValue];
        NSInteger serverMinor = [svs[1] integerValue];
        NSInteger serverFix = 0;
        if ([svs count] == 3)
            serverFix = [svs[2] integerValue];
        
        //major1 = 1;
        
        NSInteger appMajor = [cvs[0] integerValue];
        NSInteger appMinor = [cvs[1] integerValue];
        NSInteger appFix = 0;
        
        if ([cvs count] == 3)
            appFix = [cvs[2] integerValue];
        
        
        if ((appMajor < serverMajor) ||
            ((appMajor == serverMajor) && (appMinor < serverMinor)) ||
            ((appMajor == serverMajor) && (appMinor == serverMinor) && (appFix < serverFix))
            )
        {
            
            [self showOKConfirmMessage:NSLocalizedString(@"You need to update Height Chart to continue.", @"") completion:^{
//                self.currentStatus = kHomeStatusDidVersionCheck;
                
//                [[UIApplication sharedApplication]openURL:[NSURL URLWithString:@"http://www.dole.com"]];
                [[UIApplication sharedApplication]openURL:[NSURL URLWithString:appStoreURL]];

            }];
        }
        else{
            self.currentStatus = kHomeStatusDidVersionCheck;
        }
    }
    
    
}
-(void)handleServerCheckEventWithParsedData:(NSDictionary *)recevedData ParseError:(NSError *)error
{
    if ([recevedData[@"TotalRowCount"] integerValue] > 0){
        NSArray *eventArray = recevedData[@"NoticeItems"];
        NSDictionary *event = eventArray.firstObject;
        
        //밑에 값이 제대로 들어온다..
        NSString *urlBanner = event[@"Content"];
        NSLog(@"Content is %@", urlBanner);
        NSString *url = event[@"Summary"];
        
        EventBannerViewController *vc = [[EventBannerViewController alloc]init];
        vc.eventUrlString = url;
        vc.closeCompletion = ^(BOOL isDontShowOneDay){
            if (isDontShowOneDay){
                [self.userConfig notShownEventOneDays];
            }
            self.currentStatus = kHomeStatusDidEventCheck;
        };
        [self presentViewController:vc animated:YES completion:nil];
        
    }
    else{
        self.currentStatus = kHomeStatusDidEventCheck;
    }
}
-(void)handleServerQueryDoleCoinWithParsedData:(NSDictionary *)recevedData ParseError:(NSError *)error
{
    self.userConfig.doleCoin = [recevedData[@"EventBalance"] integerValue];
    [self.userConfig saveDoleCoinToLocal];
    _homeToolBar.doleCoinCount = self.userConfig.doleCoin;
    
    //longzhe cui added
    [[Mixpanel sharedInstance].people set:@{@"coins":[NSNumber numberWithInteger:self.userConfig.doleCoin]}];
}
-(void)handleServerEmailLogInWithParsedData:(NSDictionary *)recevedData ParseError:(NSError *)error
{
    NSNumber *userNo = recevedData[@"UserNo"];
    NSString *authKey = recevedData[@"AuthKey"];
    
    if (userNo && authKey){
        self.userConfig.authKey = authKey;
        self.userConfig.userNo = [userNo integerValue];
        self.userConfig.loginStatus = kUserLogInStatusDoleLogOn;
        self.currentStatus = kHomeStatusDidLogIn;
    }
    else{
        NSInteger errorCode = [recevedData[@"ReturnCode"] integerValue];
        [self toastNetworkErrorWithErrorCode:errorCode];
        
//        [self reloadMonkeys];
        
        self.currentStatus = kHomeStatusDidLogOff;
    }
    
}
-(void)handleServerSnsLogInWithParsedData:(NSDictionary *)recevedData ParseError:(NSError *)error
{
    NSNumber *no = recevedData[@"UserNo"];
    NSString *authKey = recevedData[@"AuthKey"];
    
    if (no && authKey){
        self.userConfig.authKey = authKey;
        self.userConfig.userNo = [no integerValue];
        self.userConfig.loginStatus = kUserLogInStatusFacebook;
        
//        [self showEventBanner];
//        [self requestDoleCoinQuery];
        
        self.currentStatus = kHomeStatusDidLogIn;
    }
    else{
        
        self.currentStatus = kHomeStatusDidLogOff;
    }
    
//    if (self.userConfig.useiCloud == NO)
//        [self reloadMonkeys];

}

-(void)handleServerRenewAuthkeyWithParsedData:(NSDictionary *)recevedData ParseError:(NSError *)error
{
    NSNumber *userNo = recevedData[@"UserNo"];
    NSString *authKey = recevedData[@"AuthKey"];
    
    NSLog(@"handleServerRenewAuthkey-Renew UserNo & Authkey /// UserNo=%@ , Authkey=%@", userNo, authKey);
    
    if (userNo && authKey){
        self.userConfig.authKey = authKey;
        self.userConfig.userNo = [userNo integerValue];
    }
    else{
        NSInteger errorCode = [recevedData[@"ReturnCode"] integerValue];
        [self toastNetworkErrorWithErrorCode:errorCode];
    }
}

#pragma mark HomeToolBarDelegate

- (void)clickButtonWithType:(HomeToolBarButton)toolBarButtonType
{
    if (self.currentStatus < kHomeStatusDidLogIn) return;
    
    switch (toolBarButtonType) {
        case kAddHeightButton: [self menuToolBarAddHeightClick];
            break;
        case kDeleteModeButton: self.isDeleteMode = YES;
            break;
        case kDeleteBackButton: self.isDeleteMode = NO;
            break;
        case kDeleteNickNameButton: [self menuToolBarDeleteNicknameClick];
            break;
        case kSettingButton: [self menuToolBarSettingClick];
            break;
        case kDoleCoinButton: [self menuToolBarDoleCoinClick];
            break;
    }
}



#pragma mark Core Data

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
//    // Set up the fetched results controller
//    //
//    // Create the fetch request for the entity
//    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
//    // Edit the entity name as appropriate.
//    NSEntityDescription *entity = [NSEntityDescription entityForName:@"MyChild"
//                                              inManagedObjectContext:self.managedObjectContext];
//    [fetchRequest setEntity:entity];
//    
//    // Set the batch size to a suitable number
//    [fetchRequest setFetchBatchSize:20];
//    
//    // Edit the sort key as appropriate.
//    NSSortDescriptor *lastNameSortDesc = [[NSSortDescriptor alloc] initWithKey:@"lastName" ascending:YES];
//    NSSortDescriptor *firstNameSortDesc = [[NSSortDescriptor alloc] initWithKey:@"firstName" ascending:YES];
//    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:lastNameSortDesc, firstNameSortDesc, nil];
//    
//    [fetchRequest setSortDescriptors:sortDescriptors];
//    
//    // Edit the section name key path and cache name if appropriate,
//    // nil for section name key path means "no sections"
//    NSFetchedResultsController *aFetchedResultsController =
//    [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
//                                        managedObjectContext:self.managedObjectContext
//                                          sectionNameKeyPath:nil
//                                                   cacheName:nil];
//    aFetchedResultsController.delegate = self;
//    self.fetchedResultsController = aFetchedResultsController;
    
    _fetchedResultsController = [self requestFetchedResultController];
//    _fetchedResultsController.delegate = self;
    return _fetchedResultsController;
}

- (void)controller:(NSFetchedResultsController *)controller
   didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath
     forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    NSLog(@"FetchController didChangeObject!!!!");

    if (controller != self.fetchedResultsController) return;
    
    if (type == NSFetchedResultsChangeInsert){
        //MyChild *myChild = _fetchResultController.fetchedObjects[newIndexPath.row];
        //NSInteger row = newIndexPath.row;
        //UITableViewCell *cell = [self tableView:self.tableView cellForRowAtIndexPath:newIndexPath];
//        [self.tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationBottom];
        
        
//        if (anObject) {
//            MyChild *newChild = anObject;
//            [_monkeyScrollView addNewMonkeyWithMyChildInfo:newChild];
//        }
    }
}


-(NSFetchedResultsController*)localFetchedResultController
{
    if (_localFetchedResultController != nil)return _localFetchedResultController;
    
    _localFetchedResultController = [self requestLocalFetchedResultController];
    
    return _localFetchedResultController;
}

-(void)reloadMonkeys
{
    _fetchedResultsController = nil;
    for (MyChild *child in self.fetchedResultsController.fetchedObjects) {
        [child loadLastTakenHeight];
        [child refreshMaxTakenHeight];
        //[child lastTakenHeight];
        if (child.nickname == nil)return;
    }

    _homeToolBar.availableSelected = self.fetchedResultsController.fetchedObjects.count > 0;
    [_monkeyScrollView reloadMonkeyTree];
}

-(void)loadDB
{
//    _fetchedController = [self requestChildren];
//    
//    for (MyChild *child in _fetchedController.fetchedObjects) {
//        [child loadLastTakenHeight];
//    }
//    
//    _homeToolBar.availableSelected = _fetchedController.fetchedObjects.count > 0;
    
//    _fetchedController = [self requestChildren];
    
    _fetchedResultsController = nil;
    
    for (MyChild *child in self.fetchedResultsController.fetchedObjects) {
        [child loadLastTakenHeight];
    }
    
    _homeToolBar.availableSelected = self.fetchedResultsController.fetchedObjects.count > 0;
}

- (void)reloadFetchedResults:(NSNotification *)note {
    dispatch_async(dispatch_get_main_queue(), ^{
        NSError *error = nil;
        
        if (self.fetchedResultsController)
        {
            if (![[self fetchedResultsController] performFetch:&error])
            {
                /*
                 Replace this implementation with code to handle the error appropriately.
                 
                 abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. If it is not possible to recover from the error, display an alert panel that instructs the user to quit the application by pressing the Home button.
                 */
                NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
                abort();
            } else {

//                for (MyChild *child in self.fetchedResultsController.fetchedObjects) {
//                    [child loadLastTakenHeight];
//                }
//
//                _homeToolBar.availableSelected = self.fetchedResultsController.fetchedObjects.count > 0;
//                [_monkeyScrollView reloadMonkeyTree];

                [self reloadMonkeys];
            }
        }
    });
}

- (void)didChangeDBStore:(NSNotification *)notification
{
    if (self.userConfig.useiCloud){
        [self registerForiCloudNotifications];
        
    }
    else{
        [self removeiCloudNotifications];
        [self moveCloudDBToLocalDB];
        [self loadDB];
    }
}

-(void)iCloudReady:(NSNotification *)notification
{
        [self moveLocalDBToCloudDBWithLocalFetchedResultController:self.localFetchedResultController];
        [self mergeCloudDB];
        [self reloadMonkeys];
}

- (void)storesWillChange:(NSNotification *)notification {} // Reset 
- (void)storesDidChange:(NSNotification *)notification
{
    dispatch_async(dispatch_get_main_queue(), ^{
    

        
        [self reloadMonkeys];
        
        [self.userConfig syncCloudData];
    });
}

- (void)persistentStoreDidImportUbiquitousContentChanges:(NSNotification *)notification
{
//    [_monkeyScrollView reloadMonkeyTree];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self migrationCloudDB];
        [self reloadFetchedResults:nil];
        
        [self loadDB];
        
        for (MyChild *cd in self.fetchedResultsController.fetchedObjects) {
            NSLog(@"Nickname is %@", cd.nickname);
        }
        
//        [_monkeyScrollView reloadMonkeyTree];
        [self reloadMonkeys];
        
        [self.userConfig syncCloudData];
    });
}

#pragma mark ToolBar Click



- (void)menuToolBarDoleCoinClick
{
//    if([self enableActionWithCurrentNetwork] == NO) return;
    
//    if (self.userConfig.loginStatus == kUserLogInStatusOffLine) {
//        [self showLogInCancelConfirmMessage:NSLocalizedString(@"Login to view Dole Coins\ninformation.", @"") completion:^(BOOL isYes) {
//            
//            if (isYes)
//            {
//                UIStoryboard *story = [UIStoryboard storyboardWithName:@"LogIn" bundle:nil];
//                LogInViewController *vc = [story instantiateViewControllerWithIdentifier:@"LogIn"];
//                
//                UINavigationController *navi = [[UINavigationController alloc]initWithRootViewController:vc];
//                navi.navigationBarHidden = YES;
//                [self modalTransitionController:navi Animated:YES];
//            }
//            
//        }];
//    }
//    else{
//        
//        ///////////////////////////////////////////////////////////////////////////////////////////////////////
//        UIStoryboard *story = [UIStoryboard storyboardWithName:@"LogIn" bundle:nil];
//        DoleCoinViewController *vc = [story instantiateViewControllerWithIdentifier:@"DoleCoinView"];
//        
//        UINavigationController *navi = [[UINavigationController alloc]initWithRootViewController:vc];
//        navi.navigationBarHidden = YES;
//        
//        [self modalTransitionController:navi Animated:YES];
//        ///////////////////////////////////////////////////////////////////////////////////////////////////////
//    }
    
    if (self.userConfig.accountStatus == kNotSingUp){
        __weak HomeViewController *weakSelf = self;
        [self presentLogInViewControllerWithCompletion:^(BOOL isLoggedIn) {
            if (isLoggedIn){
                [weakSelf showDoleCoinViewController];
            }
            else{
                [weakSelf showDoleCoinViewController];
            }
        }];
    }
    else{
        [self showDoleCoinViewController];
    }
}

-(void)showDoleCoinViewController
{
    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"LogIn" bundle:nil];
    DoleCoinViewController *vc = [story instantiateViewControllerWithIdentifier:@"DoleCoinView"];
    
    UINavigationController *navi = [[UINavigationController alloc]initWithRootViewController:vc];
    navi.navigationBarHidden = YES;
    
    [self modalTransitionController:navi Animated:YES];
    ///////////////////////////////////////////////////////////////////////////////////////////////////////
}

- (void)menuToolBarDeleteNicknameClick
{
    
    if (!self.isDeleteMode) return;
    
    NSArray *checkedIndexList = _monkeyScrollView.checkedMonkeyIndexList;
    
    if (checkedIndexList.count <= 0) return;
    
    [self showOkCancelConfirmMessage:NSLocalizedString(@"Selected nickname will be deleted.\nDeleted records can't be restored.",
                                                       @"") completion:^(BOOL isYes) {
        // 컴펌체크
        if (isYes) {

            NSManagedObjectContext *context = self.usedObjectContext;

            int count = (int)checkedIndexList.count;

            for (int i = count - 1; i >= 0; i--) {
                int willDeleteIndex = [checkedIndexList[i] intValue];
                MyChild *child = self.fetchedResultsController.fetchedObjects[willDeleteIndex];
                [context deleteObject:child];
            }


            NSError *error;
            if ([context save:&error]){
                NSLog(@"Saved ....");
                [self loadDB];
                [_monkeyScrollView deleteMonkeysWithIndexArray:checkedIndexList];
            }
            else{
                NSLog(@"%@", error);
            }
            
            
            self.isDeleteMode = NO;
            
            if (nil == error){
                [ToastController showToastWithMessage:NSLocalizedString(@"Deleted.", @"") duration:kToastMessageDurationType_Auto];
            }
        }
    }];
}

- (void)menuToolBarAddHeightClick
{
    if (self.fetchedResultsController.fetchedObjects.count <= 0){
        [self addNonameChild];
        return;
    }
    
    [ChooseNicknameBox popupOnView:self.view FetchController:self.fetchedResultsController completion:^(NSInteger selectedIndex) {
        if (selectedIndex == -1){
            //[self showNewnicknameModal];
            [self addNonameChild];
        }
        else{
            [self showHeightDetailViewWithChildIndex:selectedIndex startAddHeight:YES];
        }
    }];
}

- (void)menuToolBarSettingClick
{
    UIStoryboard *loginStory = [UIStoryboard storyboardWithLogIn];
    
    UIViewController *vc;
    
    switch (self.userConfig.accountStatus) {
        case kNotSingUp: vc = [loginStory instantiateViewControllerWithIdentifier:@"LogInBefore"];
            break;
        case kSignUpByFacebook: vc = [loginStory instantiateViewControllerWithIdentifier:@"LogOn"];
            break;
        case kSignUpByEmail: vc = [loginStory instantiateViewControllerWithIdentifier:@"LogOn"];
            break;
    }
    
    UINavigationController *navi = [[UINavigationController alloc]initWithRootViewController:vc];
    navi.navigationBarHidden = YES;
    
    [self modalTransitionController:navi Animated:YES];
}

-(void)showNewnicknameModal
{
    NicknameViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"AddNickname"];
    vc.fetchResultController = self.fetchedResultsController;
    vc.saveCompletion = ^(NicknameViewController *controller){
        
        NSManagedObjectContext *context = self.usedObjectContext;
        MyChild *newObj = [NSEntityDescription insertNewObjectForEntityForName:@"MyChild"
                                                        inManagedObjectContext:context];
        
        newObj.nickname = controller.inputNickName;
        newObj.charterNo = [NSNumber numberWithShort:controller.characterNo+1];
        newObj.createdDate = [NSDate dateWithTimeIntervalSinceNow:0];
        
        newObj.birthday = controller.inputBirthday;
        newObj.isGirl = [NSNumber numberWithBool:controller.isGirl];
        
        NSError *error;
        BOOL createdNewnickname = NO;
        if ([context save:&error]){
            NSLog(@"Saved ....");
            [self loadDB];
            createdNewnickname = YES;
        }
        else{
            NSLog(@"%@", error);
        }
        
        [controller dismissViewControllerAnimated:YES completion:^{
            // add new monkeyTree
            //[_homeScrollContentView addNewMonkey];
            
            if (createdNewnickname) {
                [_monkeyScrollView addNewMonkeyWithMyChildInfo:newObj];
                
                [self checkAddHeightGuide];
            }
            
        }];
    };
    
    [self modalTransitionController:vc Animated:YES];
}


- (void)showHeightDetailViewWithChildIndex:(NSUInteger)childIndex startAddHeight:(BOOL)addHeight
{
    MyChild *selectedChild = self.fetchedResultsController.fetchedObjects[childIndex];
    
    HeightDetailViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"HeightDetailView"];
    vc.monkeyNumber = [selectedChild.charterNo shortValue];
    vc.startAddHeightMode = addHeight;
    vc.myChild = selectedChild;
    vc.fetchResultController = self.fetchedResultsController;
    
    UINavigationController *naviController = [[UINavigationController alloc]initWithRootViewController:vc];
    naviController.navigationBarHidden = YES;
    
    __weak HeightDetailViewController *weakController = vc;
    
    vc.completion = ^(MyChild *child){
        
        [child loadLastTakenHeight];
        [child refreshMaxTakenHeight];
        [_monkeyScrollView updateMonekyTreehegithWithIndex:childIndex];
        
        if (weakController.modifiedMonkey)
            [ToastController showToastWithMessage:NSLocalizedString(@"It has been changed.", @"") duration:kToastMessageDurationType_Auto];
        else
            [self checkAddHeightGuide];
    };
    
    [self modalTransitionController:naviController Animated:YES];

    if (addHeight){
        [vc addHeight];
    }
}

- (void)takeAddHeightQuickModeWithChildIndex:(NSInteger)childIndex
{
    HeightSaveContext *context = [[HeightSaveContext alloc]init];
    
    QRScanViewController *scanViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ScanQRCode"];
    scanViewController.heightSaveContext = context;
    
    UINavigationController *naviController = [[UINavigationController alloc]initWithRootViewController:scanViewController];
    naviController.navigationBarHidden = YES;
    
    [self modalTransitionController:naviController Animated:YES];
}

#pragma mark MonkeyTreeDataSource

-(NSUInteger)monkeyCount
{
    return self.fetchedResultsController.fetchedObjects.count;
}

-(MyChild*)monkeyTreeInfoAtIndex:(NSUInteger)index
{
    return self.fetchedResultsController.fetchedObjects[index];
}

#pragma mark MonkeyHomeListDelegate

-(void)clickMonkeyAtIndex:(NSUInteger)index
{
    if (!self.isDeleteMode){
        [self showHeightDetailViewWithChildIndex:index startAddHeight:NO];
    }
}

-(void)longPressedAtIndex:(NSUInteger)index
{
    if (!self.isDeleteMode) self.isDeleteMode = YES;
//    [_monkeyScrollView checkedMonkeyTreeWithIndex:index];
    
    _homeToolBar.enableDeleteBtn = YES;

}

-(void)clickAddNewNickname
{
    [self showNewnicknameModal];
}

-(void)didCheckedMonkeyWithIndex:(NSUInteger)index Checked:(BOOL)checked
{
    if (self.isDeleteMode){
        _homeToolBar.enableDeleteBtn = (_monkeyScrollView.checkedMonkeyIndexList.count > 0);
    }
}

-(void)didFinishedAnimation:(BOOL)isFirstTime
{
    if (isFirstTime){
        GuideView *gv = [GuideView addGuideTarget:self.view Type:kGuideTypeHomeAddnickname checkFirstTime:YES];
        gv.completion = ^{
            //Nothing;
            
//            [ToastControluy ler showToastWithMessage:@"ABCDEFG\nEFZDFEFWE" duration:10];
        };
    }
}



- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark facebook sessionlogin sample

- (void) loginResult:(BOOL)isSucess withAcessToken:(NSString*)accessToken
{
    if (isSucess){
        
    }
    else{
        if (self.currentStatus < kHomeStatusDidLogIn){
            self.currentStatus = kHomeStatusDidLogOff;
        }
//        if (self.userConfig.useiCloud == NO)
//            [self reloadMonkeys];
    }
}

- (void) didReceiveUserID:(NSString*)userID Email:(NSString*)email Error:(id)error
{
    if (nil == error){
        [self requestLogInWithSNSID:userID];
    }
    else{ //Facebook Error
        if (self.currentStatus < kHomeStatusDidLogIn){
            self.currentStatus = kHomeStatusDidLogOff;
        }
    }
}

-(BOOL)checkAddHeightGuide
{
    if (FALSE == self.userConfig.isNeedGuideAddHeight ){

        if ([self monkeyCount] == 1){
            GuideView *gv = [GuideView addGuideTarget:self.view Type:kGuideTypeAddHeight checkFirstTime:YES];
            gv.completion = ^{
                
            };
            
            return TRUE;
        }
        
    }
    
    return FALSE;
}

-(void)becameActive
{
    NSLog(@"HomeViewController - DidBecameActive");

    [self reloadAnimation:nil];
    
    //이번에는 들어가지 않는다.
    
    /*
    if (self.currentStatus >= kHomeStatusDidLogIn){
        if (self.userConfig.accountStatus == kSignUpByEmail &&
            self.userConfig.dateGetAuthkey ){
            
            //NSInteger hours = [self.userConfig.dateGetAuthkey getHoursFromNow];
            NSInteger mins = [self.userConfig.dateGetAuthkey getMinsFromNow];
            NSInteger halfOfExpireMins = kExpireAuthkeyMins / 2;
            
            if (mins < -halfOfExpireMins){
                NSLog(@"HomeViewController - RenewAuthKey");
                [self requestRenewAuthKey];
            }
            else{
                NSLog(@"Authkey dont expire");
            }
        }
    }
     
     */
}


@end
