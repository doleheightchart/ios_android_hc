//
//  EventBannerViewController.h
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 4. 11..
//  Copyright (c) 2014년 Kim Sehyun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventBannerViewController : UIViewController<UIWebViewDelegate>

@property (nonatomic, retain) NSString *eventUrlString;

@property (nonatomic, copy) void (^closeCompletion)(BOOL);

@end
