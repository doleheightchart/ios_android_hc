//
//  PopupController.h
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 2. 11..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "OverlayViewController.h"
#import "UIFont+DoleHeightChart.h"
#import "UIColor+ColorUtil.h"

@interface PopupController : OverlayViewController

@property (nonatomic, weak) IBOutlet UIView *popupContainerView;
@property (nonatomic, weak) IBOutlet UIImageView *topBarView;
@property (nonatomic, weak) IBOutlet UIImageView *bottomBarView;
@property (nonatomic, weak) IBOutlet UIImageView *notitleBodyView;

@property (nonatomic, weak) IBOutlet UIButton *okButton;
@property (nonatomic, weak) IBOutlet UIButton *cancelButton;

@property (nonatomic, weak) IBOutlet UILabel *popupTitleLabel;

-(IBAction)cancelClose:(id)sender;

-(void)setOverlayStyle;
-(void)decorateUI;

@end
