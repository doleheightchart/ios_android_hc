//
//  OnOffSwitch.h
//  HeightChart
//
//  Created by mytwoface on 2014. 3. 29..
//  Copyright (c) 2014년 Kim Sehyun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OnOffSwitch : UIControl

@property (nonatomic) BOOL isCheckedOn;

@end
