//
//  CityPopupController.h
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 26..
//  Copyright (c) 2014년 Kim Sehyun. All rights reserved.
//

#import "PopupController.h"

@interface CityPopupController : PopupController<UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, retain) NSArray *citynames;
@property (nonatomic, retain) NSString *selectedCityname;
@property (nonatomic, copy) void (^completion)(CityPopupController*);

@end
