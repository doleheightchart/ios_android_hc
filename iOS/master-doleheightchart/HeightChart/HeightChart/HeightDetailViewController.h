//
//  HeightDetailViewController.h
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 3..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HeightDetailTree.h"
#import "MyChild.h"
#import "HeightChartProtocolList.h"
#import "HeightSaveContext.h"

@interface HeightDetailViewController : UIViewController<HeightDetailTreeDataSource,
                                                        HeightDetailTreeDelegate,
                                                        NSFetchedResultsControllerDelegate,
                                                        HeightViewerDataSource,
                                                        HeightViewerDelgate,
                                                        HeightSaveDelegate,
                                                        HeightDetailDataSource>
@property (nonatomic) NSUInteger monkeyNumber;
@property (nonatomic) BOOL startAddHeightMode;
@property (nonatomic, retain) MyChild *myChild;
@property (nonatomic, weak) NSFetchedResultsController *fetchResultController;
@property (nonatomic, copy) void (^completion)(MyChild *mychild);
@property (nonatomic) BOOL modifiedMonkey;

-(void)addHeight;
@end
