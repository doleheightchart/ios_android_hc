//
//  ForgotEmailViewController.m
//  HeightChart
//
//  Created by ne on 2014. 2. 11..
//  Copyright (c) 2014년 ne. All rights reserved.
//

#import "ForgotPasswordViewController.h"
#import "UIView+ImageUtil.h"
#import "TextViewDualPlaceHolder.h"
#import "UIViewController+DoleHeightChart.h"
#import "ToastController.h"
#import "WarningCoverView.h"
#import "UIFont+DoleHeightChart.h"
#import "UIColor+ColorUtil.h"
#import "UIViewController+PolyServer.h"
#import "MessageBoxController.h"
#import "InputChecker.h"

@interface ForgotPasswordViewController (){
    // for server
    NSMutableData   *_receivedData;
    InputChecker    *_inputChecker;
}
@property (weak, nonatomic) IBOutlet UILabel *txtDescript;
@property (weak, nonatomic) IBOutlet UIButton *btnSend;
@property (weak, nonatomic) IBOutlet TextViewDualPlaceHolder *emailTextView;
- (IBAction)clickSend:(id)sender;

@end

@implementation ForgotPasswordViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self.btnSend setResizableImageWithType:DoleButtonImageTypeOrange];
    [self.emailTextView setDualPlaceHolderTextView:DoleDualPlaceHolderTextTypeEmail];
    [self addCommonCloseButton];
    [self setupControlFlexiblePosition];
    
    self.txtDescript.font = [UIFont defaultRegularFontWithSize:26/2];
    self.txtDescript.textColor = [UIColor colorWithRGB:0x606060];
}

-(void)viewDidAppear:(BOOL)animated
{
    [self checkNetwork];
}


-(void)setupControlFlexiblePosition
{
    //한글 2줄 영문 4줄 이기때문에 아래로 밀린다.
    CGFloat lblDescriptHeight = self.txtDescript.frame.size.height *2;
    
    if ([UIScreen isFourInchiScreen]) {
        
        [self addTitleByBigSizeImage:YES Visible:YES];
        
        [self moveToPositionYPixel:(110 + 168 + 126 + 30 + 6 + 30) - lblDescriptHeight Control:self.txtDescript];
        
        [self moveToPositionYPixel:1136 - (454 + 74 + 34 + 72) Control:self.emailTextView];
        [self moveToPositionYPixel:1136 - (454 + 74) Control:self.btnSend];

        
    }
    else{
        
        [self addTitleByBigSizeImage:NO Visible:NO];
        
        [self moveToPositionYPixel:(238 + 30 + 6 + 30) - lblDescriptHeight Control:self.txtDescript];
        [self moveToPositionYPixel: 960 - (444+74+34+72) Control:self.emailTextView];
        [self moveToPositionYPixel: 960 - (444+74) Control:self.btnSend];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)checkNetwork
{
    if (NO == self.isEnableNetwork){

//        self.btnSend.enabled = NO;
//        [ToastController showToastWithMessage:
//         NSLocalizedString(@"The network is unstable. \nPlease try again after checking the network", @"") duration:3];
    }
    else{
        _inputChecker = [[InputChecker alloc]init];
        [_inputChecker addHostViewRecursive:self.view submitButton:self.btnSend checkBoxButtons:nil];
    }
}

-(NSMutableData*)receivedBufferData
{
    if (_receivedData == nil){
        _receivedData = [NSMutableData dataWithCapacity:0];
    }
    
    return _receivedData;
}

- (BOOL)isValidInput
{
    if ([self.emailTextView checkIsValidEmail] == NO) return NO;
    
    if (self.emailTextView.textField.text.length > 0){
        return YES;
    }
    
    //[WarningCoverView addWarningToTargetView:self.emailTextView warningmessage:@"입력하신 이메일에 오류가 있습니다"];
    
    return NO;
}

- (IBAction)clickSend:(id)sender {
    
    [self.emailTextView.textField resignFirstResponder];
    
    
    
    if ([self isValidInput]){
        [self requestFindPasswordWithEmail:self.emailTextView.textField.text];
    }
}

- (void)didConnectionFail:(NSError *)error
{
    [self notifyUserWithResult:NO];
}

- (void)didCompleteRecevedWithResultType:(DoleServerResultType)resultType ParsedData:(NSDictionary *)recevedData ParseError:(NSError *)error
{
    if (!error){
        NSNumber *returnValue = recevedData[@"Return"];
        if (returnValue && [returnValue boolValue]){
            [self notifyUserWithResult:YES];
            return;
        }
        else{
            NSInteger errorCode = [recevedData[@"ReturnCode"] integerValue];
            
            if ([self queryErrorCode:errorCode EmailView:self.emailTextView PasswordView:nil]) {
                
                [_inputChecker updateSubmitEnable];
                
                return;
            }
            
            [self toastNetworkErrorWithErrorCode:errorCode];
        }
    }
    
    [self notifyUserWithResult:NO];
}

- (void)notifyUserWithResult:(BOOL)result
{
    if (result){
        //Message Box 를 띄운다.
        
        //[ToastController showToastWithMessage:@"토스트가 아니라 메시지 박스를 띄워야 한다." duration:4];
        
        [self showOKConfirmMessage:
         NSLocalizedString(@"The temporary password\nwas sent.\nPlease check your spam folder\nif the email is not in your inbox.\n(Takes around 5 to 10 minutes.)", @"")
         completion:^{
            [self closeViewControllerAnimated:YES];
        }];
    }
    else {
        //[ToastController showToastWithMessage:@"오류가 발생하였습니다." duration:4];
        [self toastNetworkError];
    }
}

@end
