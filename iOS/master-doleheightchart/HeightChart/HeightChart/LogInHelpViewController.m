//
//  LogInHelpViewController.m
//  HeightChart
//
//  Created by ne on 2014. 3. 24..
//  Copyright (c) 2014년 Kim Sehyun. All rights reserved.
//

#import "LogInHelpViewController.h"
#import "UIView+ImageUtil.h"
#import "TextViewDualPlaceHolder.h"
#import "UIViewController+DoleHeightChart.h"
#import "UIButton+CheckAndRadio.h"
#import "UIFont+DoleHeightChart.h"
#import "UIColor+ColorUtil.h"
#import "ToastController.h"
#import "WarningCoverView.h"
#import "UIViewController+PolyServer.h"
#import "InputChecker.h"
#import "PaperConfirmPopupController.h"

#import "Mixpanel.h"

@interface LogInHelpViewController (){
    NSMutableData   *_receivedData;
    UILabel *_textViewPlaceHolder;
    InputChecker *_inputChecker;
}

//@property (weak, nonatomic) IBOutlet TextViewDualPlaceHolder *txtViewTopic;
@property (weak, nonatomic) IBOutlet TextViewDualPlaceHolder *txtViewEmail;

@property (weak, nonatomic) IBOutlet UILabel *lblDevice;
@property (weak, nonatomic) IBOutlet UILabel *lblDescript;
//@property (weak, nonatomic) IBOutlet UILabel *lblOK;
//@property (weak, nonatomic) IBOutlet UIButton *btnCheck;
@property (weak, nonatomic) IBOutlet UIButton *btnSend;
@property (weak, nonatomic) IBOutlet UITextView *txtQuestionView;
@property (weak, nonatomic) IBOutlet UIView *viewQuestion;
@property (weak, nonatomic) IBOutlet UIView *viewDevice;
@property (weak, nonatomic) IBOutlet UIButton *btnFAQ;

- (IBAction)clickSend:(id)sender;

@end

@implementation LogInHelpViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //    [self.txtViewTopic setDualPlaceHolderTextView:DoleDualPlaceHolderTextTypeTopic];
    [self.btnFAQ setResizableImageWithType:DoleButtonImageTypeOrange];
    
    [self.btnFAQ addTarget:self action:@selector(goFAQurl)  forControlEvents:UIControlEventTouchUpInside];
    
    [self.txtViewEmail setDualPlaceHolderTextView:DoleDualPlaceHolderTextTypeEmail];
    [self.txtViewEmail setSecondText:@""];
    [self.txtViewEmail setFirstText: NSLocalizedString(@"Your email", @"")];
    
    
    //    if ([self.currentLanguageCode isEqualToString:@"en"]) {
    //        [self.txtViewEmail.textField changeFontSize:27/2];
    //    }
    
    
    
    
    [self.btnSend setResizableImageWithType:DoleButtonImageTypeOrange];
    //    [self.btnCheck makeUICheckBox];
    [self addCommonCloseButton];
    
    [self setupControlFlexiblePosition];
    
    self.lblDescript.font = [UIFont defaultKoreanFontWithSize:24/2];
    self.lblDescript.textColor = [UIColor colorWithRGB:0x606060];
    
    //     self.lblOK.font = [UIFont defaultKoreanFontWithSize:24/2];
    //     self.lblOK.textColor =  [UIColor colorWithRGB:0x606060];
    
    self.txtQuestionView.textColor = [UIColor colorWithRGB:0x606060];
    self.txtQuestionView.delegate = self;
    self.txtQuestionView.font = [UIFont defaultBoldFontWithSize:30/2];
    
    //CGFloat top, CGFloat left, CGFloat bottom, CGFloat right
    //    self.txtQuestionView.contentInset = UIEdgeInsetsMake(28/2, 0, 28/2, 0);
    //    self.txtQuestionView.contentSize = CGSizeMake(self.txtQuestionView.frame.size.width - 28,
    //                                                  self.txtQuestionView.frame.size.height);
    self.txtQuestionView.textContainer.size = CGSizeMake(self.txtQuestionView.frame.size.width - 28,
                                                         self.txtQuestionView.frame.size.height);
    //    self.txtQuestionView.contentMode = UIViewContentModeScaleAspectFit;
    //    self.txtQuestionView.textContainerInset = UIEdgeInsetsMake(50, 50, 50, 50);
    self.txtQuestionView.textContainer.lineFragmentPadding = 28/2;
    
    self.txtQuestionView.showsHorizontalScrollIndicator = NO;
    //self.txtQuestionView.alwaysBounceVertical = YES;
    //self.txtQuestionView.alwaysBounceHorizontal = NO;
    
    
    if ([self.currentLanguageCode isEqualToString:@"en"]) {
        _textViewPlaceHolder = [[UILabel alloc]initWithFrame:CGRectMake(28/2, 24/2, 472/2, 36/2)];
        _textViewPlaceHolder.numberOfLines = 1;
    }
    else{
        _textViewPlaceHolder = [[UILabel alloc]initWithFrame:CGRectMake(28/2, 24/2, 472/2, 80/2)];
        _textViewPlaceHolder.numberOfLines = 2;
    }
    
    
    _textViewPlaceHolder.font = [UIFont defaultRegularFontWithSize:30/2];
    _textViewPlaceHolder.textColor = [UIColor colorWithRGB:0x9fa0a3];
    _textViewPlaceHolder.textAlignment = NSTextAlignmentLeft;
    _textViewPlaceHolder.adjustsFontSizeToFitWidth = NO;
    _textViewPlaceHolder.lineBreakMode =NSLineBreakByWordWrapping;
    
    
    _textViewPlaceHolder.text = NSLocalizedString(@"Type feedback here", @"");
    [self.viewQuestion addSubview:_textViewPlaceHolder];
    
    NSString *deviceType = [[UIDevice currentDevice] model];
    self.lblDevice.text = deviceType;
    self.lblDevice.font = [UIFont defaultRegularFontWithSize:28/2];
    self.lblDevice.textColor = [UIColor colorWithRGB:0x606060];
    
    if (self.userConfig.accountStatus != kUserLogInStatusOffLine){
        self.txtViewEmail.textField.text = self.userConfig.email;
    }
    
    _inputChecker = [[InputChecker alloc]init];
    [_inputChecker addHostViewRecursive:self.view submitButton:self.btnSend checkBoxButtons:nil];
    _inputChecker.bigTextView = self.txtQuestionView;
    
    self.txtQuestionView.backgroundColor = [UIColor clearColor];
    self.viewDevice.backgroundColor = [ UIColor clearColor];
    self.viewQuestion.layer.cornerRadius = 5;
    self.viewQuestion.layer.masksToBounds = YES;
    
    //    self.txtQuestionView.layer.cornerRadius = 15;
    //    self.txtQuestionView.layer.masksToBounds = YES;
}



- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if (textView == self.txtQuestionView) {
        [_textViewPlaceHolder setHidden:YES];
    }
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    if (textView == self.txtQuestionView) {
        if (self.txtQuestionView.text.length == 0) {
            [_textViewPlaceHolder setHidden:NO];
        }
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range
 replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"]) {
        if (textView == self.txtQuestionView) {
            [textView resignFirstResponder];
            return NO;
        }
    }
    // For any other character return TRUE so that the text gets added to the view
    return YES;
}
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if (self.view.frame.origin.y == 0 ){
        [UIView animateWithDuration:0.2 animations:^{
            self.view.frame = CGRectMake(0, -100, self.view.bounds.size.width, self.view.bounds.size.height);
        }];
    }
    return YES;
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    if (self.view.frame.origin.y < 0){
        [UIView animateWithDuration:0.2 animations:^{
            self.view.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height);
        }];
    }
    return YES;
}

- (void)viewDidAppear:(BOOL)animated
{
    if (NO == self.isEnableNetwork){
        
        //        self.txtViewTopic.textField.enabled = NO;
        self.txtViewEmail.textField.enabled = NO;
        self.txtQuestionView.editable = NO;
        //        self.btnCheck.enabled = NO;
        self.btnSend.enabled = NO;
        
        [self toastNetworkError];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)setupControlFlexiblePosition
{
    
    CGFloat inputFieldHeight = (384 - 72);
    CGFloat defalutMargin = 240;
    
    if ([UIScreen isFourInchiScreen]) {
        
        [self changeToHeightPixel:384 Control:self.viewQuestion];
        [self changeToHeightPixel:inputFieldHeight Control:self.txtQuestionView];
        
        [self addTitleByBigSizeImage:NO Visible:YES];
        
        [self moveToPositionYPixel:(defalutMargin) Control:self.btnFAQ];
        [self moveToPositionYPixel:(defalutMargin + 72 + 22) Control:self.txtViewEmail];
        [self moveToPositionYPixel:(defalutMargin + 72 + 22 + 72 +22) Control:self.viewQuestion];
        [self moveToPositionYPixel:(defalutMargin + 72 + 22 + 72 +22 + 384 +22 ) Control:self.lblDescript];
        //        [self moveToPositionYPixel:(defalutMargin + 72 + 22 + 72 +22 + 384 +22 + 28 + 5 + 28 + 22) Control:self.btnCheck];
        //        [self moveToPositionYPixel:(defalutMargin + 72 + 22 + 72 +22 + 384 +22 +28 + 5 + 28 + 22) Control:self.lblOK];
        [self moveToPositionYPixel:(defalutMargin + 72 + 22 + 72 +22 + 384 +22 +28 + 5 + 28 + 22 + 51 + 26) Control:self.btnSend];
        
    }
    else{
        
        inputFieldHeight = (358 - 72);
        
        [self changeToHeightPixel:358 Control:self.viewQuestion];
        [self changeToHeightPixel:inputFieldHeight Control:self.txtQuestionView];
        
        defalutMargin = 90;
        
        
        [self addTitleByBigSizeImage:NO Visible:NO];
        
        [self moveToPositionYPixel:(defalutMargin) Control:self.btnFAQ];
        [self moveToPositionYPixel:(defalutMargin + 72 + 22) Control:self.txtViewEmail];
        [self moveToPositionYPixel:(defalutMargin + 72 + 22 + 72 +22) Control:self.viewQuestion];
        [self moveToPositionYPixel:(defalutMargin + 72 + 22 + 72 +22 + 358 +22 ) Control:self.lblDescript];
        //        [self moveToPositionYPixel:(defalutMargin + 72 + 22 + 72 +22 + 358 +22 + 28 + 5 + 28 + 22) Control:self.btnCheck];
        //        [self moveToPositionYPixel:(defalutMargin + 72 + 22 + 72 +22 + 358 +22 +28 + 5 + 28 + 22) Control:self.lblOK];
        [self moveToPositionYPixel:(defalutMargin + 72 + 22 + 72 +22 + 358 +22 +28 + 5 + 28 + 22 + 51 + 26) Control:self.btnSend];
        
    }
    
    [self moveToPositionYPixel:inputFieldHeight Control:self.viewDevice];
}

- (IBAction)clickSend:(id)sender {
    
    
    [self.view.findFirstResponder resignFirstResponder];
    
    if ([self enableActionWithCurrentNetwork] == NO)return;
    
    if ([self isValidInput]){
        
        [self registerQuestion];
    }
    
}

-(void)goFAQurl
{
    [[Mixpanel sharedInstance] track:@"Opened FAQ"];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.doleapps.com/"]];
}

- (BOOL)isValidInput
{
    if ([self.txtViewEmail checkIsValidEmail] == NO) return NO;
    
    
    //    if (self.txtViewTopic.textField.text.length <= 0){
    //
    //        [WarningCoverView addWarningToTargetView:self.txtViewTopic warningtype:kInputFieldErrorTypeWrongContent];
    //
    //        return NO;
    //    }
    
    if (self.txtQuestionView.text.length <= 0){
        
        //[WarningCoverView addWarningToTargetView:self.txtQuestionView warningtype:kInputFieldErrorTypeWrongContent];
        
        return NO;
    }
    
    
    
    
    return YES;
}


#pragma mark Server

- (NSMutableData*)receivedBufferData
{
    if (_receivedData == nil){
        _receivedData = [NSMutableData dataWithCapacity:0];
    }
    
    return _receivedData;
}

- (void)registerQuestion
{
    NSString *email = self.txtViewEmail.textField.text;
    //    NSString *topic = self.txtViewTopic.textField.text;
    NSString *topic = @"HeightChart";
    NSString *question = self.txtQuestionView.text;
    
    [self requestRegisterQuestionWithTopic:topic ReceiveEmail:email Question:question];
}

- (void)didConnectionFail:(NSError *)error
{
    //[ToastController showToastWithMessage:error.description duration:5];
    [self toastNetworkError];
}

- (void)didCompleteRecevedWithResultType:(DoleServerResultType)resultType ParsedData:(NSDictionary *)recevedData ParseError:(NSError *)error
{
    if (recevedData && error==nil && [recevedData[@"Return"] boolValue] ){
        [self closeViewControllerAnimated:NO];
        [ToastController showToastWithMessage:NSLocalizedString(@"Your inquiry has been sent", @"")
                                     duration:kToastMessageDurationType_Auto];
        self.btnSend.enabled = NO;
        
        //longzhe cui added
        [[Mixpanel sharedInstance] track:@"Submitted Feedback"];
    }
    else{
        NSInteger errorCode = [recevedData[@"ReturnCode"] integerValue];
        [self toastNetworkErrorWithErrorCode:errorCode];
    }
}

@end
