//
//  PaperConfirmPopupController.m
//  HeightChart
//
//  Created by ne on 2014. 4. 7..
//  Copyright (c) 2014년 Kim Sehyun. All rights reserved.
//

#import "PaperConfirmPopupController.h"

@interface PaperConfirmPopupController ()
@property (weak, nonatomic) IBOutlet UIView *viewGrayBack;

@property (weak, nonatomic) IBOutlet UILabel *lblDescript;
@property (weak, nonatomic) IBOutlet UILabel *lblAdress;
- (IBAction)clickOK:(id)sender;

@end

@implementation PaperConfirmPopupController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    
    
    self.lblDescript.font = [UIFont defaultBoldFontWithSize:28/2];
    self.lblDescript.textColor = [UIColor colorWithRGB:0x787878];
    self.lblAdress.font = [UIFont defaultBoldFontWithSize:28/2];
    self.lblAdress.textColor = [UIColor colorWithRGB:0x606060];
    
    self.lblDescript.text = NSLocalizedString(@"Please check the entered data. You can't request after completion again.", @"");
    
    self.viewGrayBack.backgroundColor = [UIColor colorWithRGB:0xe6e6e6];
    self.viewGrayBack.layer.cornerRadius = 2;
    self.viewGrayBack.layer.masksToBounds = YES;
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)popupAddressText:(NSString*)adress
{
    self.lblAdress.text = adress;
    
}


- (IBAction)clickOK:(id)sender {
    self.completionBlock(YES);
}
@end
