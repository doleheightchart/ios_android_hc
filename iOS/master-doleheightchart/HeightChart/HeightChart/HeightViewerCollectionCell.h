//
//  HeightViewerCollectionCell.h
//  HeightChart
//
//  Created by ne on 2014. 3. 12..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HeightViewerCollectionCell : UICollectionViewCell


//@property (nonatomic, retain) UIImageView *heigtImageView;
//@property (nonatomic, retain) UILabel *kidNameLabel;
//-(void)updateCellWithImage:(UIImage*)image text:(NSString*)text;
-(void)updateCellWithImage:(UIImage*)image;


@end
