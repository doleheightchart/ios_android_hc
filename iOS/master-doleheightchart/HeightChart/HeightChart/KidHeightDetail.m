//
//  KidHeightDetail.m
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 4..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "KidHeightDetail.h"

@implementation HeightDetailInfo

- (id)initWithHeight:(CGFloat)kidHeight
                date:(NSDate*)takenDate
               image:(UIImage*)takenImage
{
    self = [super init];
    if (self){
        self.kidHeight = kidHeight;
        self.takenDate = takenDate;
        self.image = takenImage;
    }
    return self;
}

-(id)initWithChildHeight:(ChildHeight*)childHeight;
{
    self = [super init];
    if (self){
        self.kidHeight = [childHeight.height floatValue];
        self.takenDate = childHeight.takenDate;
        
        self.image = childHeight.thumbphoto;
//        self.image = childHeight.faceCropImage;
        
        self.imageFullSize = childHeight.photo;
        self.childHeight = childHeight;
    }
    return self;
}

@end

@implementation KidHeightDetail

- (id)init
{
    self = [super init];
    if (self){
        self.takenHeightHistory = [NSMutableArray array];
    }
    
    return self;
}

- (CGFloat)kidHeight
{
    HeightDetailInfo *first = self.takenHeightHistory.firstObject;
    return first.kidHeight;
}

-(HeightDetailInfo*)representDetail
{
    return self.takenHeightHistory.firstObject;
}

@end
