//
//  CloudBackgroundView.m
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 7..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "DetailCloudBackgroundView.h"
#import "UIView+Animation.h"
#import "MyAnimationTime.h"

//#define kCloud_INTERVAL 10
//#define kFOX_RACCON_INTERVAL 10
//#define kDELAY_RACCON 3

@interface DetailCloudBackgroundView (){
    UIImageView     *_skyBackgroundView;
    UIImageView     *_nightStarView;
    
    UIImageView     *_cloud1_1;
    UIImageView     *_cloud1_2;
    UIImageView     *_cloud2_1;
    UIImageView     *_cloud2_2;
    UIImageView     *_cloud3_1;
    UIImageView     *_cloud3_2;
    
    CGRect          _frameOffScreen_Cloud1_1;
    CGRect          _frameOffScreen_Cloud1_2;
    CGRect          _frameOffScreen_Cloud2_1;
    CGRect          _frameOffScreen_Cloud2_2;
    CGRect          _frameOffScreen_Cloud3_1;
    CGRect          _frameOffScreen_Cloud3_2;

    UIImageView     *_fox;
    UIImageView     *_raccon;
    
    CAKeyframeAnimation *_animationMoveFox;
    CAKeyframeAnimation *_animationMoveRaccon;
    
    NSMutableDictionary  *_animationDic;
}

@end

@implementation DetailCloudBackgroundView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        //[self initBackground];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self){
        //[self initBackground];
    }
    return self;
}

-(NSMutableDictionary*)animationDic
{
    if (_animationDic == nil){
        _animationDic = [NSMutableDictionary dictionary];
    }
    
    return _animationDic;
}

-(void)addSkyBackgroundImage
{
    _skyBackgroundView = [[UIImageView alloc]initWithFrame:self.bounds];
//    _skyBackgroundView.image = [UIImage imageNamed:@"sky_bg"];
    [self addSubview:_skyBackgroundView];
    
    _nightStarView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.bounds.size.width, (348/2))];
    [self addSubview:_nightStarView];
    
    self.isDeleteMode = NO;
}

-(void)addClouds
{
    _frameOffScreen_Cloud1_1 = CGRectMake(498/2, (358/2), 112/2, 58/2);
    _cloud1_1 = [[UIImageView alloc]initWithFrame:_frameOffScreen_Cloud1_1];
    _cloud1_1.image = [UIImage imageNamed:@"detail_cloud_01"];
    [self addSubview:_cloud1_1];
    
    _frameOffScreen_Cloud1_2 = CGRectMake(-142/2, (358/2), 112/2, 58/2);
    _cloud1_2 = [[UIImageView alloc]initWithFrame:_frameOffScreen_Cloud1_2];
    _cloud1_2.image = [UIImage imageNamed:@"detail_cloud_01"];
    [self addSubview:_cloud1_2];
    
    _frameOffScreen_Cloud2_1 = CGRectMake(30/2, (464/2), 158/2, 86/2);
    _cloud2_1 = [[UIImageView alloc]initWithFrame:_frameOffScreen_Cloud2_1];
    _cloud2_1.image = [UIImage imageNamed:@"detail_cloud_02"];
    [self addSubview:_cloud2_1];

    _frameOffScreen_Cloud2_2 = CGRectMake(-(640-30)/2, (464/2), 158/2, 86/2);
    _cloud2_2 = [[UIImageView alloc]initWithFrame:_frameOffScreen_Cloud2_2];
    _cloud2_2.image = [UIImage imageNamed:@"detail_cloud_02"];
    [self addSubview:_cloud2_2];
    
    CGFloat heightOfBackground = self.bounds.size.height * 2;

    _frameOffScreen_Cloud3_1 = CGRectMake(411/2, (heightOfBackground-166)/2, 146/2, 74/2);
    _cloud3_1 = [[UIImageView alloc]initWithFrame:CGRectMake(411/2, (heightOfBackground-166)/2, 146/2, 74/2)];
    _cloud3_1.image = [UIImage imageNamed:@"detail_cloud_03"];
    [self addSubview:_cloud3_1];
    
    _frameOffScreen_Cloud3_2 = CGRectMake(-229/2, (heightOfBackground-166)/2, 146/2, 74/2);
    _cloud3_2 = [[UIImageView alloc]initWithFrame:_frameOffScreen_Cloud3_2];
    _cloud3_2.image = [UIImage imageNamed:@"detail_cloud_03"];
    [self addSubview:_cloud3_2];
}

-(void)awakeFromNib
{
    //[self startScrolling];
    [super awakeFromNib];
    
    [self initBackground];
}

-(void)initBackground
{
    [self addSkyBackgroundImage];
    
    [self addClouds];
    
    //[self startScrolling];
}

-(CABasicAnimation*)animationMoveCloud:(UIView*)cloud Key:(NSString*)keyName;
{
    CABasicAnimation *moveAni = [CABasicAnimation animationWithKeyPath:@"position"];
    moveAni.fillMode = kCAFillModeForwards;
    moveAni.duration = 10;
    moveAni.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];;
    moveAni.repeatCount = HUGE_VALF;
    moveAni.delegate = self;
    [moveAni setValue:keyName forKey:@"AniName"];
    moveAni.toValue = [NSValue valueWithCGPoint:CGPointMake(cloud.layer.position.x + 320, cloud.layer.position.y)];
    [cloud.layer addAnimation:moveAni forKey:keyName];
    
    return moveAni;
}

-(void)startScrolling
{

    _cloud1_1.frame = _frameOffScreen_Cloud1_1;
    _cloud1_2.frame = _frameOffScreen_Cloud1_2;
    _cloud2_1.frame = _frameOffScreen_Cloud2_1;
    _cloud2_2.frame = _frameOffScreen_Cloud2_2;
    _cloud3_1.frame = _frameOffScreen_Cloud3_1;
    _cloud3_2.frame = _frameOffScreen_Cloud3_2;

    [UIView animateWithDuration:kDuration_Cloud delay:0 options:UIViewAnimationOptionCurveLinear|UIViewAnimationOptionRepeat animations:^{
  
        _cloud1_1.center = CGPointMake(_cloud1_1.center.x + 320, _cloud1_1.center.y);
        _cloud1_2.center = CGPointMake(_cloud1_2.center.x + 320, _cloud1_2.center.y);
        
        _cloud2_1.center = CGPointMake(_cloud2_1.center.x + 320, _cloud2_1.center.y);
        _cloud2_2.center = CGPointMake(_cloud2_2.center.x + 320, _cloud2_2.center.y);
        
        _cloud3_1.center = CGPointMake(_cloud3_1.center.x + 320, _cloud3_1.center.y);
        _cloud3_2.center = CGPointMake(_cloud3_2.center.x + 320, _cloud3_2.center.y);
      
    } completion:^(BOOL finished){
  
        if (finished){
          //[self switchCloudPositionToOffscreenPosition];
        }
  
    }];

    
}

-(void)stopAll
{
    [_fox.layer removeAllAnimations];
    [_raccon.layer removeAllAnimations];
}

-(void)loadAnimals
{
    if (_fox) return;
    
    CGRect frameFox = CGRectMake(-50, 600, 150/2, 222/2);
    
    _fox = [[UIImageView alloc]initWithFrame:frameFox];
//    [_fox addFrameImagesWithNameFormat:@"01_fox_detail_%02d" frameCount:10 duration:3 repeat:0];

    [self addSubview:_fox];
    
    _raccon = [[UIImageView alloc]initWithFrame:frameFox];
//    [_raccon addFrameImagesWithNameFormat:@"01_raccoon_detail_%02d" frameCount:10 duration:3 repeat:0];
    [self addSubview:_raccon];
    
    
}

-(void)initAnimals
{

    CGRect frameFox = CGRectMake(-50, 600, 150/2, 222/2);

    [_fox stopAnimating];
    [_raccon stopAnimating];

    _fox.frame = frameFox;
    [_fox addFrameImagesWithNameFormat:@"01_fox_detail_%02d" frameCount:10 duration:3 repeat:0];

    _raccon.frame = frameFox;
    [_raccon addFrameImagesWithNameFormat:@"01_raccoon_detail_%02d" frameCount:10 duration:3 repeat:0];
}

-(void)startMovingAnimation
{

    [self initAnimals];
    [self moveAnimateFox];
    [self moveAnimateRaccon];
}

-(void)moveAnimateFox
{
    NSValue *pos4 = [NSValue valueWithCGPoint:CGPointMake(-50, 600)];
    NSValue *pos1 = [NSValue valueWithCGPoint:CGPointMake(400, 300)];
    NSValue *pos2 = [NSValue valueWithCGPoint:CGPointMake(-100, -50)];
    NSValue *pos3 = [NSValue valueWithCGPoint:CGPointMake(-50, 600)];
    
    NSArray *positions = @[pos4, pos1, pos2, pos3, pos4];
    [_fox addMoveAnimationWithPosition:positions Delay:kDuration_Animal Repeat:HUGE_VALF FillMode:kCAFillModeForwards RepeatDuration:0 duration:kDuration_Animal completion:nil];
}

-(void)moveAnimateRaccon
{
    NSValue *pos4 = [NSValue valueWithCGPoint:CGPointMake(-50, 600)];
    NSValue *pos1 = [NSValue valueWithCGPoint:CGPointMake(400, 300)];
    NSValue *pos2 = [NSValue valueWithCGPoint:CGPointMake(-100, -50)];
    NSValue *pos3 = [NSValue valueWithCGPoint:CGPointMake(-50, 600)];
    
    NSArray *positions = @[pos4, pos1, pos2, pos3];
    
    [_raccon addMoveAnimationWithPosition:positions Delay:kDuration_Animal_Delay Repeat:HUGE_VALF FillMode:kCAFillModeForwards RepeatDuration:0 duration:kDuration_Animal completion:nil];
}

-(void)setIsDeleteMode:(BOOL)isDeleteMode
{
    _isDeleteMode = isDeleteMode;
    
//    if (_isDeleteMode){
//        _skyBackgroundView.image = [UIImage imageNamed:@"sky_delete_bg"];
//        _nightStarView.image = [UIImage imageNamed:@"sky_delete_stars"];
//    }
//    else{
//        _skyBackgroundView.image = [UIImage imageNamed:@"sky_bg"];
//        _nightStarView.image = nil;
//    }
//    
//    _cloud1_1.hidden = _isDeleteMode;
//    _cloud1_2.hidden = _isDeleteMode;
//    _cloud2_1.hidden = _isDeleteMode;
//    _cloud2_2.hidden = _isDeleteMode;
//    _cloud3_1.hidden = _isDeleteMode;
//    _cloud3_2.hidden = _isDeleteMode;

    if (_isDeleteMode){
        [_fox stopAnimating];
        [_raccon stopAnimating];
    }

        [UIView transitionWithView:self duration:0.5 options:(UIViewAnimationOptionTransitionCrossDissolve) animations:^{
            
            CGFloat toOpacity = 0;

        if (_isDeleteMode){
            _skyBackgroundView.image = [UIImage imageNamed:@"sky_delete_bg"];
            _nightStarView.image = [UIImage imageNamed:@"sky_delete_stars"];
            
            toOpacity = 0;
        }
        else{
            _skyBackgroundView.image = [UIImage imageNamed:@"sky_bg"];
            _nightStarView.image = nil;

            toOpacity = 1;

        }
        
//            CABasicAnimation *opa = [CABasicAnimation animationWithKeyPath:@"opacity"];
//            opa.toValue = [NSNumber numberWithFloat:toOpacity];
//            opa.duration = 0.5;
//            
//            [_fox.layer addAnimation:opa forKey:nil];
            
            _fox.layer.opacity = toOpacity;
            _raccon.layer.opacity = toOpacity;
        
        _cloud1_1.layer.opacity = toOpacity;
        _cloud1_2.layer.opacity = toOpacity;
        _cloud2_1.layer.opacity = toOpacity;
        _cloud2_2.layer.opacity = toOpacity;
        _cloud3_1.layer.opacity = toOpacity;
        _cloud3_2.layer.opacity = toOpacity;
        
    } completion:^(BOOL finished) {
        if (!_isDeleteMode){
            [_fox startAnimating];
            [_raccon startAnimating];
        }
        else{

        }
    }];
}

-(void)animationDidStart:(CAAnimation *)anim
{
    
}

-(void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    if (flag){
        
    }
    else{
        
    }
}

@end
