//
//  CameraCollectionCell.h
//  HeightChart
//
//  Created by ne on 2014. 3. 13..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CameraCollectionCell : UICollectionViewCell

-(void)updateFrameImage:(UIImage*)frameImage;

@end
