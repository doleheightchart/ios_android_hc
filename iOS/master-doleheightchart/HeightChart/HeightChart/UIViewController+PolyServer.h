//
//  UIViewController+PolyServer.h
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 4. 6..
//  Copyright (c) 2014년 Kim Sehyun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DoleInfo.h"

//#define kExpireAuthkeyMins 120
#define kExpireAuthkeyMins 4

typedef enum : NSUInteger {
    kDoleServerResultTypeLogIn,
    kDoleServerResultTypeSnsLogIn,
    kDoleServerResultTypeFindPassword,
    kDoleServerResultTypeSignUp,
    kDoleServerResultTypeUserInfo,
    kDoleServerResultTypeEditUserInfo,
    kDoleServerResultTypeEditSnsUserInfo,
    kDoleServerResultTypeGetCityNames,
    kDoleServerResultTypeEventInfo,
    kDoleServerResultTypeCoupon,
    kDoleServerResultTypeGetDoleCoin,
    kDoleServerResultTypeRegisterCS,
    kDoleServerResultTypeChargeFreeCash,
    kDoleServerResultTypeUseCoinCoupon,
    kDoleServerResultTypeCheckAppVersion,
    kDoleServerResultTypeWithdraw,
    kDoleServerResultTypeDissconnectSns,
    kDoleServerResultTypeRenewAuthkey,
}DoleServerResultType;

@interface UIViewController (PolyServer)

-(void)requestLogInWithUserID:(NSString*)userEmail Password:(NSString*)password;
-(void)requestLogInWithSNSID:(NSString*)snsID;
-(void)requestFindPasswordWithEmail:(NSString*)userEmail;
-(void)requestUserInfoWithUserNo:(NSInteger)userNo AuthKey:(NSString*)authKey;
-(void)requestEditUserInfoWithUserNo:(NSInteger)userNo CurPassword:(NSString*)curPassword NewPassword:(NSString*)password BirthYear:(NSUInteger)birthyear City:(NSString*)city Gender:(enum GenderType)gender;
-(void)requestSignUpWithEmail:(NSString*)userEmail Password:(NSString*)password BirthYear:(NSUInteger)birthyear City:(NSString*)city Gender:(enum GenderType)gender;
-(void)requestSignUpWithFacebookID:(NSString*)facebookID FacebookName:(NSString*)name FacebookEmail:(NSString*)email BirthYear:(NSUInteger)birthyear City:(NSString*)city Gender:(enum GenderType)gender;

-(void)requestCitynamesWithCountryCode:(NSString*)countryCode;
-(void)requestEventQuery;
-(void)requestCouponQueryWithQRCode:(NSString*)qrCode;
-(void)requestDoleCoinQuery;
-(void)requestRegisterQuestionWithTopic:(NSString*)topic ReceiveEmail:(NSString*)email Question:(NSString*)question;
-(void)requestRegisterHeightPaperWithAddress:(NSString*)address;
-(void)requestDoleCoinChargeFreeCash:(NSInteger)dolePoint UserNo:(NSInteger)userNo AuthKey:(NSString*)authKey OrderID:(NSString*)guid;
-(void)requestUseCouponWithCode:(NSString*)couponCode UserNo:(NSInteger)userNo AuthKey:(NSString*)authKey OrderID:(NSString*)guid;
//Use Facebook
-(void)requestEditFaceUserInfoWithBirthYear:(NSUInteger)birthyear City:(NSString*)city Gender:(enum GenderType)gender;
//CheckVersion
-(void)requestVersionCheck;
-(void)requestWithdrawWithEmail:(NSString*)email Password:(NSString*)password UserNo:(NSInteger)userNo AuthKey:(NSString*)authKey;
-(void)requestDissconnectWithSnsID:(NSString*)snsID AuthKey:(NSString*)authKey UserNo:(NSInteger)userNo;

//Renew AuthKey
-(void)requestRenewAuthKey;

-(NSMutableData*)receivedBufferData;

-(void)didConnectionFail:(NSError*)error;
-(void)didCompleteRecevedWithResultType:(DoleServerResultType)resultType ParsedData:(NSDictionary*)recevedData ParseError:(NSError*)error;

-(void)toastNetworkError;
-(void)toastNetworkErrorWithErrorCode:(NSInteger)errorCode;

//Ext
- (NSString *)UUID;
- (NSString *)IPAddress;
- (NSString *)DeviceUUID;

-(void)popToLonOnViewController;

@end
