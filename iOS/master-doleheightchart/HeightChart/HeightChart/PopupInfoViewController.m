//
//  PopupInfoViewController.m
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 16..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "PopupInfoViewController.h"
#import "UIViewController+DoleHeightChart.h"
#import "UIView+ImageUtil.h"
#import "UIButton+CheckAndRadio.h"
#import "UIFont+DoleHeightChart.h"

@interface PopupInfoViewController ()

@property (nonatomic,weak) IBOutlet UIImageView *infoImageView;
@property (nonatomic,weak) IBOutlet UIButton *checkButton;
@property (nonatomic,weak) IBOutlet UILabel *messageLabel;
@property (nonatomic,weak) IBOutlet UIButton *linkButton;
- (IBAction)clickLinkBtn:(id)sender;

@end

@implementation PopupInfoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self decorateUI];
    
    self.linkButton.titleLabel.text = self.linkMessage;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)decorateUI
{
    self.infoImageView.image = self.infoImage;
    
    [self.checkButton makeUICheckBox];
    
    self.messageLabel.backgroundColor = [UIColor clearColor];
    self.messageLabel.font = [UIFont defaultBoldFontWithSize:30/2];
    self.messageLabel.textAlignment = NSTextAlignmentLeft;
    self.messageLabel.textColor = [UIColor whiteColor];
    
    if (self.linkMessage){
        [self.linkButton setResizableImageWithType:DoleButtonImageTypeOrange];
//        self.linkButton.titleLabel.text = @"My Link Button!!!";
        [self.linkButton setTitle:self.linkMessage forState:UIControlStateNormal];
        self.linkButton.titleLabel.font = [UIFont defaultBoldFontWithSize:17];
        self.linkButton.titleLabel.textColor = [UIColor whiteColor];
        
    }
    else{
        self.linkButton.hidden = YES;
    }
    
    [self addCommonCloseButton];
}

- (IBAction)clickLinkBtn:(id)sender {
    NSURL *url = [NSURL URLWithString:self.linkURL];
    [[UIApplication sharedApplication] openURL:url];
}
@end
