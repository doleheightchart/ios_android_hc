//
//  UIImageToNSDataTransformer.m
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 3. 11..
//  Copyright (c) 2014년 Apportable. All rights reserved.
//

#import "UIImageToNSDataTransformer.h"

@implementation UIImageToNSDataTransformer
+ (Class)transformedValueClass
{
    return [NSData class];
}

+ (BOOL)allowsReverseTransformation
{
    return YES;
}

- (id)transformedValue:(id)value
{
    if (value == nil)
        return nil;
    
    // I pass in raw data when generating the image, save that directly to the database
    if ([value isKindOfClass:[NSData class]])
        return value;
    
//    NSData *imgData = UIImagePNGRepresentation((UIImage *)value);
//    NSData *imgData = UIImageJPEGRepresentation((UIImage *)value, 0.2); // 0.2로 하니 50KB 하지만 화질이 넘 떨어진다.
    NSData *imgData = UIImageJPEGRepresentation((UIImage *)value, 0.8);
    
    NSLog(@"ImageData size is %ld bytes", imgData.length);
    
    return imgData;
//return UIImagePNGRepresentation((UIImage *)value);
//    return UIImageJPEGRepresentation((UIImage *)value, 0.2);
}

- (id)reverseTransformedValue:(id)value
{
    return [UIImage imageWithData:(NSData *)value];
}
@end
