//
//  EventBannerViewController.m
//  HeightChart
//
//  Created by Kim Sehyun on 2014. 4. 11..
//  Copyright (c) 2014년 Kim Sehyun. All rights reserved.
//

#import "EventBannerViewController.h"
#import "UIButton+CheckAndRadio.h"
#import "UIView+ImageUtil.h"
#import "UIColor+ColorUtil.h"
#import "UIFont+DoleHeightChart.h"

@interface EventBannerViewController (){
    UIWebView   *_webView;
    UIButton    *_btnDontShowOneDayCheck;
    UIButton    *_btnClose;
    
    UIImageView *_bottomView;
}

@end

@implementation EventBannerViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _webView = [[UIWebView alloc]initWithFrame:self.view.bounds];
    [self.view addSubview:_webView];
    _webView.delegate = self;

//    UITapGestureRecognizer *taprecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapWebview:)];
//    [_webView addGestureRecognizer:taprecognizer];
    
    _bottomView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"banner_bottom"]];
    _bottomView.frame = CGRectMake(0, self.view.bounds.size.height - (110/2), self.view.bounds.size.width, (110/2));
    [self.view addSubview:_bottomView];
    
    _btnDontShowOneDayCheck = [UIButton buttonWithType:UIButtonTypeCustom];
    _btnDontShowOneDayCheck.frame = CGRectMake(28/2, self.view.bounds.size.height - (19+51)/2, 51/2, 51/2);
//    _btnDontShowOneDayCheck.frame = CGRectMake(28/2, 20, 51/2, 51/2);

    [_btnDontShowOneDayCheck makeUICheckBox];
    [self.view addSubview:_btnDontShowOneDayCheck];
//    [_bottomView addSubview:_btnDontShowOneDayCheck];
    
    _btnClose = [UIButton buttonWithType:UIButtonTypeCustom];
    _btnClose.frame = CGRectMake(self.view.bounds.size.width - (186+24)/2, self.view.bounds.size.height - (14+59)/2, 186/2, 59/2);
//    _btnClose.frame = CGRectMake(self.view.bounds.size.width - (186+24)/2, 20, 186/2, 59/2);

    [_btnClose setTitle:NSLocalizedString(@"Close",@"") forState:UIControlStateNormal];
    [_btnClose setBackgroundImage:[UIImage imageNamed:@"banner_close_btn"] forState:UIControlStateNormal];
    _btnClose.titleLabel.textColor = [UIColor whiteColor];
    _btnClose.titleLabel.font = [UIFont defaultBoldFontWithSize:30/2];
    [_btnClose addTarget:self action:@selector(closeThisWindow:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:_btnClose];
//    [_bottomView addSubview:_btnClose];
    
    UILabel *dontShowOneDayLabel = [[UILabel alloc]initWithFrame:CGRectMake((28+51+18)/2, self.view.bounds.size.height - (30+30)/2, 330/2, 30/2)];
//    UILabel *dontShowOneDayLabel = [[UILabel alloc]initWithFrame:CGRectMake((28+51+18)/2, 20, 330/2, 30/2)];

    dontShowOneDayLabel.textColor = [UIColor colorWithRGB:0x606060];
    dontShowOneDayLabel.backgroundColor = [UIColor clearColor];
    dontShowOneDayLabel.textAlignment = NSTextAlignmentLeft;
    dontShowOneDayLabel.font = [UIFont defaultRegularFontWithSize:26/2];
    dontShowOneDayLabel.text = NSLocalizedString(@"Do not display for a day", @"");// @"Don't show during 1Days";
    [self.view addSubview:dontShowOneDayLabel];
//    [_bottomView addSubview:dontShowOneDayLabel];
    
    if (self.eventUrlString){
        [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.eventUrlString]]];
    }
}

//-(void)tapTopArrowSticker:(UITapGestureRecognizer*)recognizer
//{
//    if (self.detailInfo == nil) return;
//    
//    if (recognizer.state == UIGestureRecognizerStateEnded) {
//        
//        if ([self.stickerDelegate isDeleteModeRequest]){
//            self.detailInfo.isChecked = !self.detailInfo.isChecked;
//            [self setStickerChecekd:self.detailInfo.isChecked];
//        }
//        
//        [self.stickerDelegate clickSticker:self HeightDetail:self.heightDetail clickedStickerInfo:self.detailInfo];
//        
//    }
//    else if (recognizer.state == UIGestureRecognizerStateBegan){
//    }
//}

-(void)tapWebview:(UITapGestureRecognizer*)recognizer
{
    if (recognizer.state == UIGestureRecognizerStateEnded) {
        
        [UIView animateWithDuration:0.3 animations:^{
            if (_bottomView.layer.opacity){
                _bottomView.layer.opacity = 0;
            }
            else{
                _bottomView.layer.opacity = 1;
            }
        }];
        
    }
    else if (recognizer.state == UIGestureRecognizerStateBegan){
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)closeThisWindow:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    self.closeCompletion(_btnDontShowOneDayCheck.selected);
}

- (void) scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (decelerate == YES) return;
    
//    CGFloat heightOfCell = scrollView.bounds.size.height / 3;
//    
//    NSInteger offSetIndex = roundf((scrollView.contentOffset.y) / heightOfCell);
//    CGFloat moveToY = (offSetIndex * heightOfCell);
//    
//    [UIView animateWithDuration:0.3
//                     animations:^{
//                         CGPoint point = CGPointMake(0, moveToY);
//                         scrollView.contentOffset = point;
//                     }
//                     completion:^(BOOL finished) {
//                         [self endScrollForScrollView:scrollView];
//                     }
//     ];
}

- (void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
//    CGFloat heightOfCell = scrollView.bounds.size.height / 3;
//    
//    NSInteger offSetIndex = roundf((scrollView.contentOffset.y) / heightOfCell);
//    CGFloat moveToY = (offSetIndex * heightOfCell);
//    
//    [UIView animateWithDuration:0.3
//                     animations:^{
//                         CGPoint point = CGPointMake(0, moveToY);
//                         scrollView.contentOffset = point;
//                     }
//                     completion:^(BOOL finished) {
//                         [self endScrollForScrollView:scrollView];
//                     }
//     ];
}

-(BOOL) webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType {
    if ( inType == UIWebViewNavigationTypeLinkClicked ) {
        [[UIApplication sharedApplication] openURL:[inRequest URL]];
        return NO;
    }
    
    return YES;
}


@end
