package com.dole.heightchart;

public class DLog {
	
	public static final String CUSTOM_TAG = null;
	
	public static String appendCustomTag(String tag) {
		if(CUSTOM_TAG != null)
			return tag + CUSTOM_TAG;
		return tag;
	}

	public static void d(String tag, String logMessage) {
		if(Constants.DEBUG)
			android.util.Log.d(appendCustomTag(tag), logMessage);
	}
	
	public static void d(String tag, String logMessage, Exception e) {
		if(Constants.DEBUG)
			android.util.Log.d(appendCustomTag(tag), logMessage, e);
	}
	
	public static void e(String tag, String logMessage) {
		if(Constants.DEBUG)
			android.util.Log.e(appendCustomTag(tag), logMessage);
	}
	
	public static void e(String tag, String logMessage, Exception e) {
		if(Constants.DEBUG)
			android.util.Log.e(appendCustomTag(tag), logMessage, e);
	}
	
	public static void i(String tag, String logMessage) {
		if(Constants.DEBUG)
			android.util.Log.i(appendCustomTag(tag), logMessage);
	}
	
	public static void i(String tag, String logMessage, Exception e) {
		if(Constants.DEBUG)
			android.util.Log.i(appendCustomTag(tag), logMessage, e);
	}
}
