package com.dole.heightchart;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.CheckBox;

import java.util.Date;

public class EventFragment extends Fragment {
	
	private static final String TAG = EventFragment.class.getName();
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View view =  inflater.inflate(R.layout.event_main, null);
		
		return view;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		WebView webView = (WebView)view.findViewById(R.id.event_webview);
		final CheckBox notShow = (CheckBox)view.findViewById(R.id.event_not_show);
		View close = view.findViewById(R.id.event_close);
		Bundle args = getArguments();
		if(args == null)
			return;
		String url = args.getString(Constants.EVENT_URL, "");
		DLog.e(TAG, "url:"+url);
		webView.loadUrl(url);
		
		webView.setWebViewClient(new WebViewClient() {
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				view.loadUrl(url);
				return true;
			}
		});
		
		close.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(notShow.isChecked()) {
					Date date = new Date();
					HeightChartPreference.putLong(Constants.PREF_NOT_SHOW_EVENT, date.getTime());
				}
				getFragmentManager().popBackStackImmediate();
			}
		});
	}
}
