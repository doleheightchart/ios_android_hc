package com.dole.heightchart;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class CityDialog extends DialogFragment {
	
	private String[] mCities;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		mCities = getResources().getStringArray(R.array.city);
		
		int titleHeight = getResources().getDimensionPixelSize(R.dimen.dialog_title_height);
		int buttonHeight = getResources().getDimensionPixelSize(R.dimen.dialog_button_height);
		int itemHeight = getResources().getDimensionPixelSize(R.dimen.dialog_item_height);
		
		final int totalHeight = titleHeight + buttonHeight + (itemHeight * Math.min(3, mCities.length));
		
		final Dialog d = getDialog();
		d.getWindow().setBackgroundDrawable(new ColorDrawable(0));
		
		int divierId = d.getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
		View divider = getDialog().findViewById(divierId);
		divider.setVisibility(View.GONE);
		
		int titleId = d.getContext().getResources().getIdentifier("android:id/title", null, null);
		View title = getDialog().findViewById(titleId);
		title.setVisibility(View.GONE);
		
		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
	    lp.copyFrom(d.getWindow().getAttributes());
	    lp.height = totalHeight;
	    d.getWindow().setAttributes(lp);
		
		return inflater.inflate(R.layout.city_chooser, null);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		final ListView listView = (ListView) view.findViewById(R.id.dialog_content);
		View button = view.findViewById(R.id.dialog_button);
		
		CityAdapter adapter = new CityAdapter();
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(mOnItemClickListener);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
			}
		});
	}

	private OnItemClickListener mOnItemClickListener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			dismiss();
			Object tag = view.getTag();
			if(tag instanceof String) {
				final String city = (String)tag;
				final Intent data = new Intent();
				data.putExtra(Constants.CITY, city);
				getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, data);
			}
		}
	};

	class CityAdapter extends BaseAdapter {
		LayoutInflater inflater;

		public CityAdapter() {
			inflater = LayoutInflater.from(getActivity());
		}

		@Override
		public int getCount() {
			if(mCities == null)
				return 0;
			return mCities.length;
		}
		
		@Override
		public Object getItem(int position) {
			if(mCities == null)
				return -1;
			if(position >= mCities.length)
				return -1;
			return mCities[position];
		}

		@Override
		public long getItemId(int position) {
			if(mCities == null)
				return -1;
			return position;
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if(convertView == null) {
				convertView = inflater.inflate(R.layout.city_list_item, null);
			}
			
			final TextView textView = (TextView)convertView;
			String city = mCities[position];
			textView.setText(city);
			convertView.setTag(city);
			
			return convertView;
		}
		
	}
}
