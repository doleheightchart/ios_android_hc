package com.dole.heightchart.ui;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.PointF;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Interpolator;
import android.view.animation.TranslateAnimation;
import android.widget.RelativeLayout;

import com.dole.heightchart.R;

public class ChartCloudBg extends RelativeLayout {
	private static final int CLOUD_FLOW_DURATION = 25000;
	
	private DashLine mHeightMinLine;
	private DashLine mHeightMaxLine;
	
	private Drawable mLine50;
	private Drawable mLine170;

	public ChartCloudBg(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		setFocusable(true);
	}

	public ChartCloudBg(Context context) {
		this(context, null);
	}
	
	private void init() {
		
		final Resources res = getResources();
		final int minBottomOffset = res.getDimensionPixelSize(R.dimen.chart_dash_start_bottom_offset);
		final int maxTopOffset = res.getDimensionPixelSize(R.dimen.chart_dash_end_top_offset);
		final int dashLineLength = res.getDimensionPixelSize(R.dimen.chart_dash_line_length);
		final int dashGap = res.getDimensionPixelSize(R.dimen.chart_dash_gap);
		final int dashSize = getResources().getDimensionPixelSize(R.dimen.chart_dash_size);
		Drawable dash = res.getDrawable(R.drawable.graph_line_02);
		
		mHeightMinLine = new DashLine(
				dash,
				dashSize, 
				dashSize, 
				dashGap, 
				new PointF(0f, getHeight() - minBottomOffset), 
				new PointF(dashLineLength, getHeight() - minBottomOffset));
		
		mHeightMaxLine = new DashLine(
				dash,
				dashSize, 
				dashSize, 
				dashGap, 
				new PointF(getWidth(), maxTopOffset), 
				new PointF(getWidth() - dashLineLength, maxTopOffset));
		
		final int line50Width = res.getDimensionPixelSize(R.dimen.chart_50_image_width);
		final int line170Width = res.getDimensionPixelSize(R.dimen.chart_170_image_width);
		final int lineImageHeight = res.getDimensionPixelSize(R.dimen.chart_height_text_image_height);
		final int lineImagePadding = res.getDimensionPixelSize(R.dimen.chart_height_text_image_padding);
		
		mLine50 = res.getDrawable(R.drawable.summary_line_50);
		mLine50.setBounds(
				lineImagePadding, 
				getHeight() - minBottomOffset + dashSize, 
				lineImagePadding + line50Width, 
				getHeight() - minBottomOffset + dashSize + lineImageHeight);
		
		mLine170 = res.getDrawable(R.drawable.summary_line_170);
		mLine170.setBounds(
				getWidth() - line170Width - lineImagePadding, 
				maxTopOffset + dashSize, 
				getWidth() - line170Width- lineImagePadding + line170Width, 
				maxTopOffset + dashSize + lineImageHeight);
	}
	
	public void animationStart() {
		int width = getResources().getDimensionPixelSize(R.dimen.chart_cloud_run_width);
		View cloudUpper1 = findViewById(R.id.upper_cloud1);
		View cloudUpper2 = findViewById(R.id.upper_cloud2);
		View cloudCenter1 = findViewById(R.id.center_cloud1);
		View cloudCenter2 = findViewById(R.id.center_cloud2);
		View cloudBelow1 = findViewById(R.id.lower_cloud1);
		View cloudBelow2 = findViewById(R.id.lower_cloud2);
		View cloudExtra1 = findViewById(R.id.extra_cloud1);
		View cloudExtra2 = findViewById(R.id.extra_cloud2);
		
		TranslateAnimation firstAnimation = new TranslateAnimation(0,width,0,0);
		TranslateAnimation secondAnimation = new TranslateAnimation(-width,0,0,0);

		firstAnimation = setupCloudAnimation(firstAnimation);
		secondAnimation = setupCloudAnimation(secondAnimation);
		
		if(cloudUpper1 != null && cloudUpper2 != null) {
			cloudUpper1.startAnimation(firstAnimation);
			cloudUpper2.startAnimation(secondAnimation);
		}
		if(cloudCenter1 != null && cloudCenter2 != null) {
			cloudCenter1.startAnimation(firstAnimation);
			cloudCenter2.startAnimation(secondAnimation);
		}
		if(cloudBelow1 != null && cloudBelow2 != null) {
			cloudBelow1.startAnimation(firstAnimation);
			cloudBelow2.startAnimation(secondAnimation);
		}
		if(cloudExtra1 != null && cloudExtra2 != null) {
			cloudExtra1.startAnimation(firstAnimation);
			cloudExtra2.startAnimation(secondAnimation);
		}
		
		init();
	}

	private TranslateAnimation setupCloudAnimation(TranslateAnimation anim) {
		anim.setInterpolator(new Interpolator() {
			@Override
			public float getInterpolation(float input) {
				return input;
			}
		});
		anim.setDuration(CLOUD_FLOW_DURATION);
		anim.setRepeatCount(Animation.INFINITE);
		anim.setRepeatMode(Animation.RESTART);
		return anim;
	}

	@Override
	protected void dispatchDraw(Canvas canvas) {
		super.dispatchDraw(canvas);
		
		if(mHeightMaxLine != null)
			mHeightMaxLine.drawLine(canvas);
		
		if(mHeightMinLine != null)
			mHeightMinLine.drawLine(canvas);
		
		if(mLine50 != null)
			mLine50.draw(canvas);

		if(mLine170 != null)
			mLine170.draw(canvas);
	}
}
