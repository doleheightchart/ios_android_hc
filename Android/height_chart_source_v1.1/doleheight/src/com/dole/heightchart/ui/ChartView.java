package com.dole.heightchart.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.text.format.DateFormat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;

import com.dole.heightchart.ApplicationImpl;
import com.dole.heightchart.Constants.Characters;
import com.dole.heightchart.DLog;
import com.dole.heightchart.HeightInfo;
import com.dole.heightchart.HeightInfo.FaceImageMode;
import com.dole.heightchart.ImageCache;
import com.dole.heightchart.Model;
import com.dole.heightchart.NickNameInfo;
import com.dole.heightchart.R;
import com.dole.heightchart.Util;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ChartView extends FrameLayout {
	
	private static final String TAG = ChartView.class.getName();
	
	private static final String DATE_FORMAT = "yy.MM.dd";

	private List<ChartInfo> mChartInfos;
	
	private int mItemWidth = 0;
	private int mItemSpace = 0;
	private int mSideBound = 0; //200; // Exist when need scrolls or child size is over 4
	private float mMaxHeightInChart = 170.0f;
	private float mMinHeightInChart = 50.0f;
	private float mBottomGrassHeight = 0f;
	
	private int mInfoTextSize;
	private int mBottomTextYOffset;
	private int mMinFaceBottomOffset;
	
	private int mBubbleTextSidePadding;
	private int mBubbleTextMinPadding;
	private int mBubbleTextMaxPadding;
	
	private Drawable mBubbleUp;
	private Drawable mBubbleDown;
	private Drawable mDropDownLineDot;  
	private Drawable mAvgDotImg;
	
	private Paint mChartAreaPaint = null;
	private Paint mBottomTextPaint = null;
	private Paint mBubbleTextPaint = null;
	private Paint mAvgAreaPaint = null;
	
	private String heightUnit = null;
	private Characters mCharacter;
	
	private boolean mIsShowAverage = false;
	
	class ChartInfo extends HeightInfo {
		
		public PointF mChartFacePoint;
		public Path mChartAreaPath;
		public DashLine mDashLine;
		
		public Path mAvgAreaPath;
		public PointF mAvgDotPoint;
		public float mAvgHeightOfInputDate;
		
		public ChartInfo(Context context, NickNameInfo nickInfo, HeightInfo heightInfo) {
			setHeightId(heightInfo.getHeightId());
			setNickId(heightInfo.getNickId());
			setHeight(heightInfo.getHeight());
			setInputDate(heightInfo.getInputDate());
			setImagePath(heightInfo.getImagePath());
			
			final CharSequence dateChar = android.text.format.DateFormat.format("yyyy-MM-dd", heightInfo.getInputDate());
			int age = Util.getAgeOfDate(nickInfo.getBirthday(), dateChar.toString());
			mAvgHeightOfInputDate = Util.getAverageHeightOfAge(context, age, nickInfo.getGender());
			DLog.d(TAG, " mAvgHeight = " + mAvgHeightOfInputDate);
			
			if(mAvgHeightOfInputDate != -1) {
				mAvgAreaPath = new Path();
				mAvgDotPoint = new PointF();
			}
		}
	}
	
	final int[] mBubbleColors = {
			R.color.chart_bubble_text_color_monkey1,
			R.color.chart_bubble_text_color_monkey2,
			R.color.chart_bubble_text_color_monkey3,
			R.color.chart_bubble_text_color_monkey4,
			R.color.chart_bubble_text_color_monkey5 
	};
	
	private ImageCache mImageCache = null;

	public ChartView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);

		setLayerType(View.LAYER_TYPE_NONE, null);
		final Resources res = getResources();
		mItemWidth = res.getDimensionPixelSize(R.dimen.chart_item_width);
		mItemSpace = res.getDimensionPixelSize(R.dimen.chart_item_space);
		mImageCache = ((ApplicationImpl)context.getApplicationContext()).getImageCache();
		mBottomGrassHeight = res.getDimensionPixelSize(R.dimen.chart_bottom_grass_height);
		
		mBubbleTextSidePadding = res.getDimensionPixelSize(R.dimen.chart_bubble_text_side_padding);
		mBubbleTextMinPadding = res.getDimensionPixelSize(R.dimen.chart_bubble_text_min_padding);
		mBubbleTextMaxPadding = res.getDimensionPixelSize(R.dimen.chart_bubble_text_max_padding);
		mSideBound = res.getDimensionPixelSize(R.dimen.chart_side_bound);
		mMinFaceBottomOffset = res.getDimensionPixelSize(R.dimen.chart_face_min_bottom_offset);
		
		mBubbleUp = res.getDrawable(R.drawable.summary_balloon_up);
		mBubbleDown = res.getDrawable(R.drawable.summary_balloon_down);
		
		heightUnit = res.getString(R.string.height_unit);
	}

	public ChartView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public ChartView(Context context) {
		this(context, null);
	}
	
	public void setShowAverageHeight(boolean isShowAverage) {
		mIsShowAverage = isShowAverage;
		invalidate();
	}
	
	private List<ChartInfo> getInfoFromHeights(NickNameInfo nickInfo, List<HeightInfo> infos) {
		List<ChartInfo> chartInfos = new ArrayList<ChartInfo>();
		
		for(HeightInfo height : infos) {
			chartInfos.add(new ChartInfo(getContext(), nickInfo, height));
		}
		return chartInfos;
	}
	
	public void initChart(List<HeightInfo> heights) {
		
		if(heights != null && heights.size() > 0) {
			
			final Model model = ((ApplicationImpl)getContext().getApplicationContext()).getModel();
			NickNameInfo nickInfo = model.getNickNameInfoById(heights.get(0).getNickId());
			mCharacter = nickInfo.getCharacter();
			
			mChartInfos = getInfoFromHeights(nickInfo, heights);

			for(ChartInfo charInfo : mChartInfos) {
				charInfo.mChartAreaPath = new Path();
			}
			
			mChartAreaPaint = new Paint();
			final int chartColor = getResources().getColor(R.color.chart_fill_color);
			mChartAreaPaint.setColor(chartColor);
			mChartAreaPaint.setStyle(Paint.Style.FILL_AND_STROKE);

			mDropDownLineDot = getResources().getDrawable(R.drawable.graph_line);
			
			mBottomTextYOffset = getResources().getDimensionPixelSize(R.dimen.chart_bottom_text_y_padding_top);
			mInfoTextSize = getResources().getDimensionPixelSize(R.dimen.chart_info_text_size);
			
			mBottomTextPaint = new Paint();
			mBottomTextPaint.setAntiAlias(true);
			mBottomTextPaint.setFakeBoldText(true);
			mBottomTextPaint.setColor(getResources().getColor(R.color.chart_bottom_text_color));
			mBottomTextPaint.setTextSize(mInfoTextSize);
			FontTextHelper.setFont(getContext(), mBottomTextPaint);
			
			mBubbleTextPaint = new Paint();
			mBubbleTextPaint.setTextSize(mInfoTextSize);
			mBubbleTextPaint.setAntiAlias(true);
			mBubbleTextPaint.setFakeBoldText(true);
			mBubbleTextPaint.setColor(mBubbleColors[mCharacter.getCharacterIndex()]);
			FontTextHelper.setFont(getContext(), mBubbleTextPaint);
			
			mAvgAreaPaint = new Paint();
			mAvgAreaPaint.setColor(getResources().getColor(R.color.chart_avg_height_fill_color));
			mAvgAreaPaint.setStyle(Paint.Style.FILL_AND_STROKE);
			
			mAvgDotImg = getResources().getDrawable(R.drawable.standard_dot);
			mAvgDotImg.setBounds(0, 0, mAvgDotImg.getIntrinsicWidth(), mAvgDotImg.getIntrinsicHeight());
		}
		
		requestLayout();
	}
	
	@SuppressLint("DrawAllocation")
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		int desiredHeight = ((View)getParent()).getHeight(); 
		int desiredWidth = 0;
		if(mChartInfos != null) {
			final int childCount = mChartInfos.size();
			if(mChartInfos.size() > 0) {
				int sideBoundToAffect = 0;
				if(mChartInfos.size() > 4)
					sideBoundToAffect = mSideBound * 2;
				desiredWidth = mItemWidth * childCount + mItemSpace * (childCount - 1) + sideBoundToAffect;
			}
			setMeasuredDimension(desiredWidth, desiredHeight);
			
			final Resources res = getResources();
			final int dashGap = res.getDimensionPixelSize(R.dimen.chart_dash_gap);
			final int dashSize = getResources().getDimensionPixelSize(R.dimen.chart_dash_size);

			if(desiredHeight > 0) {
				final int size = mChartInfos.size();
				for(int i = 0; i < size ; i++) {
					final ChartInfo chartInfo = mChartInfos.get(i);	
					final Bitmap faceImage = chartInfo.getFaceImage(getContext(), mImageCache, FaceImageMode.CHART);
					if(faceImage == null)
						continue;

					final float height = Math.max( mMinHeightInChart, Math.min(chartInfo.getHeight(), mMaxHeightInChart));
					chartInfo.mChartFacePoint = new PointF(
							(mItemSpace + mItemWidth) * i, 
							desiredHeight - mMinFaceBottomOffset - faceImage.getHeight() - 
							((height - mMinHeightInChart) * 
									(float)(desiredHeight - mMinFaceBottomOffset - faceImage.getHeight()) 
									/ (mMaxHeightInChart - mMinHeightInChart)) 
							);
					
					chartInfo.mDashLine = new DashLine(
							mDropDownLineDot, 
							dashSize, dashSize, dashGap,
							new PointF(chartInfo.mChartFacePoint.x + faceImage.getWidth() / 2, desiredHeight - mBottomGrassHeight), 
							new PointF(chartInfo.mChartFacePoint.x + faceImage.getWidth() / 2, 
									chartInfo.mChartFacePoint.y + faceImage.getHeight() / 2));
					
					if(chartInfo.mAvgHeightOfInputDate != -1) {
						int imgH = mAvgDotImg.getIntrinsicHeight();
						int heightPoint = (int) (desiredHeight - mMinFaceBottomOffset - imgH - 
								((chartInfo.mAvgHeightOfInputDate - mMinHeightInChart) * 
										(float)(desiredHeight - mMinFaceBottomOffset - imgH) 
										/ (mMaxHeightInChart - mMinHeightInChart)));

						chartInfo.mAvgDotPoint = new PointF(chartInfo.mChartFacePoint.x + faceImage.getWidth() / 2, heightPoint + imgH / 2);
					}
				}

				for(int i = 0; i < size ; i++) {
					final ChartInfo chartInfo = mChartInfos.get(i);	
					if(i < size - 1) {
						final ChartInfo nextChartInfo = mChartInfos.get(i + 1);
						final Bitmap faceImage = nextChartInfo.getFaceImage(getContext(), mImageCache, FaceImageMode.CHART);
						if(faceImage == null)
							continue;

						final Path path = chartInfo.mChartAreaPath;
						if(chartInfo.mChartFacePoint == null)
							continue;

						path.reset();
						path.moveTo(chartInfo.mChartFacePoint.x + faceImage.getWidth() / 2, desiredHeight - mBottomGrassHeight);
						path.lineTo(
								chartInfo.mChartFacePoint.x + faceImage.getWidth() / 2, 
								chartInfo.mChartFacePoint.y + faceImage.getHeight() / 2);
						path.lineTo(
								nextChartInfo.mChartFacePoint.x + faceImage.getWidth() / 2, 
								nextChartInfo.mChartFacePoint.y + faceImage.getHeight() / 2);
						path.lineTo(nextChartInfo.mChartFacePoint.x + faceImage.getWidth() / 2, desiredHeight - mBottomGrassHeight);
						path.lineTo(chartInfo.mChartFacePoint.x + faceImage.getWidth() / 2, desiredHeight - mBottomGrassHeight);
						
						if(chartInfo.mAvgAreaPath != null) {
							final Path avgPath = chartInfo.mAvgAreaPath;
							avgPath.reset();
							avgPath.moveTo(chartInfo.mAvgDotPoint.x, desiredHeight - mBottomGrassHeight);
							avgPath.lineTo(
									chartInfo.mAvgDotPoint.x, 
									chartInfo.mAvgDotPoint.y);
							avgPath.lineTo(
									nextChartInfo.mAvgDotPoint.x, 
									nextChartInfo.mAvgDotPoint.y);
							avgPath.lineTo(nextChartInfo.mAvgDotPoint.x, desiredHeight - mBottomGrassHeight);
							avgPath.lineTo(chartInfo.mAvgDotPoint.x + faceImage.getWidth() / 2, desiredHeight - mBottomGrassHeight);
						}
					}
				}
			}
		}
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		drawChart(canvas);
	}
	
	public void drawChart(Canvas canvas) {
		if(mChartInfos != null) {
			final int size = mChartInfos.size();
			for(ChartInfo chartInfo : mChartInfos) {
				canvas.save();
				if(size > 4)
						canvas.translate(mSideBound, 0);
				//Average
				if(mIsShowAverage) {
					if(chartInfo.mAvgAreaPath != null)
						canvas.drawPath(chartInfo.mAvgAreaPath, mAvgAreaPaint);
					
					if(chartInfo.mAvgDotPoint != null) {
						int tempSavePoint = canvas.save();
						canvas.translate(chartInfo.mAvgDotPoint.x - mAvgDotImg.getIntrinsicWidth() / 2, chartInfo.mAvgDotPoint.y - mAvgDotImg.getIntrinsicHeight() / 2);
						mAvgDotImg.draw(canvas);
						canvas.restoreToCount(tempSavePoint);
					}
				}
				
				//Area
				if(chartInfo.mChartAreaPath != null)
					canvas.drawPath(chartInfo.mChartAreaPath, mChartAreaPaint);
				
				//DropLine
				if(chartInfo.mDashLine != null)
					chartInfo.mDashLine.drawLine(canvas);
				
				//Face image
				if(chartInfo.mChartFacePoint != null) {
					canvas.drawBitmap(
							chartInfo.getFaceImage(getContext(), mImageCache, FaceImageMode.CHART),
							chartInfo.mChartFacePoint.x, chartInfo.mChartFacePoint.y,
							null);
				}
				
				//Input date
				final Date inputDate = chartInfo.getInputDate();
				if(inputDate != null) {
					final CharSequence dateChar = DateFormat.format(DATE_FORMAT, inputDate);
					final String dateStr = dateChar.toString();
					final float textWidth = mBottomTextPaint.measureText(dateStr);
					canvas.drawText(dateChar.toString(),
							chartInfo.mChartFacePoint.x + 
							chartInfo.getFaceImage(getContext(), mImageCache, FaceImageMode.CHART).getWidth() / 2 - textWidth / 2f,
							getHeight() - mBottomTextYOffset,
							mBottomTextPaint);
				}

				final float height = chartInfo.getHeight();
				if(height > 0) {
					final String hegihtStr = height + heightUnit;
					final float textWidth = mBubbleTextPaint.measureText(hegihtStr);
					final int bubbleWidth = (int)(mBubbleTextSidePadding * 2 + textWidth);
					final int bubbleHeight = mBubbleTextMinPadding + mBubbleTextMaxPadding + mInfoTextSize;
					
					boolean isBubbleUp = true;
					final Drawable bubble;
					if(bubbleHeight > chartInfo.mChartFacePoint.y) {
						bubble = mBubbleDown;
						isBubbleUp = false;
					} else {
						bubble = mBubbleUp;
					}
					
					bubble.setBounds(0, 0, bubbleWidth, bubbleHeight);
					Rect bound = bubble.getBounds();
					final int faceWidth = chartInfo.getFaceImage(getContext(), mImageCache, FaceImageMode.CHART).getWidth();
					final int faceHeight = chartInfo.getFaceImage(getContext(), mImageCache, FaceImageMode.CHART).getHeight();
					final int offsetX = (int)(chartInfo.mChartFacePoint.x + faceWidth / 2 - bubbleWidth / 2);
					final int offsetY = (int)(chartInfo.mChartFacePoint.y + faceHeight);
					if(isBubbleUp) {
						bound.offsetTo(offsetX, (int)(chartInfo.mChartFacePoint.y - bubbleHeight));
					} else {
						bound.offsetTo(offsetX, offsetY);
					}
					bubble.draw(canvas);
					
					final float textOffsetX = chartInfo.mChartFacePoint.x + faceWidth / 2 - textWidth / 2;
					if(isBubbleUp) {
						canvas.drawText(hegihtStr,
								textOffsetX,
								chartInfo.mChartFacePoint.y - mBubbleTextMaxPadding - mBubbleTextPaint.descent(),
								mBubbleTextPaint); 
					} else {
						canvas.drawText(hegihtStr,
								textOffsetX,
								chartInfo.mChartFacePoint.y + faceHeight + mBubbleTextMaxPadding - mBubbleTextPaint.ascent() - mBubbleTextPaint.descent(),
								mBubbleTextPaint);
					}
				}
				canvas.restore();
			}
		}
	}
	
	public int getChartWidth() {
		int imageWidth = 0;
		if(mChartInfos != null) {
			final int childCount = mChartInfos.size();
			if(mChartInfos.size() > 0) {
				int sideBoundToAffect = 0;
				if(mChartInfos.size() > 4)
					sideBoundToAffect = mSideBound * 2;
				imageWidth = mItemWidth * childCount + mItemSpace * (childCount - 1) + sideBoundToAffect;
			}
		}
		return imageWidth;
	}
}
