package com.dole.heightchart.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;

public class FontEditText extends EditText {
	
	public FontEditText(Context context) {
		super(context);
		FontTextHelper.setFont(getContext(), this);
	}
	
	public FontEditText(Context context, AttributeSet attrs) {
		super(context, attrs);
		FontTextHelper.setFont(getContext(), this);
	}

	public FontEditText(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		FontTextHelper.setFont(getContext(), this);
	}
}
