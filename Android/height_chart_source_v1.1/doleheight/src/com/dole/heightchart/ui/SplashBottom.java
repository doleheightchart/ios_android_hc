package com.dole.heightchart.ui;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.OvershootInterpolator;

import com.dole.heightchart.R;

import java.util.ArrayList;
import java.util.List;

public class SplashBottom extends View {
	private static final int ANIMATION_DURATION = 250;
	private static final int DESIRED_BG_WIDTH = 1080;
	private static final int DESIRED_BG_HEIGHT = 300;
	private static final int OFFSET_ANIMATE_START_BOTTOM = 219;
	
	enum AnimtaionMode {NONE, YET, RUNNING, FINISHED};
	
	private List<BottomItem> mItems;
	private float mWidthDiffRate = 1.0f;
	
	private ValueAnimator mPopUpAnimator;
	private int mAnimationGroupIdx;
	private boolean mIsDrawBeforeAnimation = false;

	public SplashBottom(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	public SplashBottom(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public SplashBottom(Context context) {
		this(context, null);
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		final int measuredWidth = getMeasuredWidth();
		final int desiredWidth = DESIRED_BG_WIDTH; 
		final int desiredHeight = DESIRED_BG_HEIGHT;
		mWidthDiffRate = ((float)measuredWidth * 1.0f / (float)desiredWidth);
		final int measuredHeight = (int)(mWidthDiffRate * desiredHeight);
		setMeasuredDimension(measuredWidth, measuredHeight);
		initItems();
	}

	private void initItems() {
		//Position of items is Hard-coded to bottom_bg image resource
		mItems = new ArrayList<BottomItem>();
		BottomItem bg = new BottomItem(R.drawable.bottom_bg, 0, 0, DESIRED_BG_WIDTH, DESIRED_BG_HEIGHT, mWidthDiffRate, 0);
		BottomItem tree1 = new BottomItem(R.drawable.bottom_tree_01_bg, 75, 53, 70, 247, mWidthDiffRate, 2);
		BottomItem tree2 = new BottomItem(R.drawable.bottom_tree_02_bg, 279, 57, 113, 243, mWidthDiffRate, 0);
		BottomItem tree3 = new BottomItem(R.drawable.bottom_tree_03_bg, 513, 101, 104, 199, mWidthDiffRate, 2);
		BottomItem tree4 = new BottomItem(R.drawable.bottom_tree_04_bg, 193, 133, 320, 167, mWidthDiffRate, 0);
		BottomItem mushroom1 = new BottomItem(R.drawable.bottom_mushroom_01_bg, 127, 168, 52, 51, mWidthDiffRate, 2);
		BottomItem mushroom2 = new BottomItem(R.drawable.bottom_mushroom_02_bg, 162, 141, 78, 78, mWidthDiffRate, 1);
		BottomItem rock = new BottomItem(R.drawable.bottom_rocks_bg, 569, 121, 361, 179, mWidthDiffRate, 0);
		
		//Draw order
		mItems.add(tree1);
		mItems.add(bg);
		mItems.add(tree4);
		mItems.add(tree3);
		mItems.add(tree2);
		mItems.add(mushroom2);
		mItems.add(mushroom1);
		mItems.add(rock);
	}
	
	public void startSplash(boolean animation) {
		mPopUpAnimator = ValueAnimator.ofFloat(0f, 1.0f);
		
		int duration = ANIMATION_DURATION;
		if(!animation)
			duration = 0;
			
		mPopUpAnimator.setDuration(duration);
		mPopUpAnimator.setRepeatCount(3);
		mPopUpAnimator.setInterpolator(new OvershootInterpolator(1.3f));
		mPopUpAnimator.addListener(new AnimatorListener() {
			
			@Override
			public void onAnimationStart(Animator animation) {
			}
			
			@Override
			public void onAnimationRepeat(Animator animation) {
				mAnimationGroupIdx++; //TODO What out for OutOfIndexBound
			}
			
			@Override
			public void onAnimationEnd(Animator animation) {
				mPopUpAnimator = null;
				mIsDrawBeforeAnimation = true;
			}
			
			@Override
			public void onAnimationCancel(Animator animation) {
			}
		});
		
		mPopUpAnimator.addUpdateListener(new AnimatorUpdateListener() {
			@Override
			public void onAnimationUpdate(ValueAnimator animation) {
				invalidate();
			}
		});
		mPopUpAnimator.start();
		
		mAnimationGroupIdx = 0;
		invalidate();
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		if(mPopUpAnimator != null && mPopUpAnimator.isRunning()) {
			drawItems(canvas, mItems, true);
		} else {
			if(mIsDrawBeforeAnimation)
				drawItems(canvas, mItems, false);
		}
	}
	
	private void drawItems(Canvas canvas, List<BottomItem> items, boolean animate) {
		if(items != null) {
			for(BottomItem item : items) {
				final Drawable d = item.drawable;
				final Rect r = item.bound;
				canvas.save();
				if(animate) {
					final int aniIdx = item.drawGroupIdx;
					final AnimtaionMode mode;
					if(aniIdx < mAnimationGroupIdx) {
						mode = AnimtaionMode.FINISHED;					
					} else if(aniIdx == mAnimationGroupIdx){
						mode = AnimtaionMode.RUNNING;
					} else {
						mode = AnimtaionMode.YET;
					}
					final float animateBottom =  (OFFSET_ANIMATE_START_BOTTOM) * mWidthDiffRate;
					switch (mode) {
					case YET:
						canvas.translate(0, animateBottom);
						break;
					case RUNNING:
						canvas.translate(0, animateBottom * (1.0f - mPopUpAnimator.getAnimatedFraction()));
						break;
					case FINISHED:
					default:
						break;
					}
				}
				d.setBounds(r);;
				d.draw(canvas);
				canvas.restore();
			}
		}
	}

	private class BottomItem {
		Drawable drawable;
		Rect bound;
		int drawGroupIdx = 0;
		
		public BottomItem(int resId, int left, int top, int width, int height, float rate, int drawGroupIdx) {
			this.drawable = getContext().getResources().getDrawable(resId);
			this.bound = new Rect(
					(int)(left * rate), 
					(int)(top * rate), 
					(int)((left + width) * rate), 
					(int)((top + height) * rate));
			this.drawGroupIdx = drawGroupIdx;
		}
	}
}
