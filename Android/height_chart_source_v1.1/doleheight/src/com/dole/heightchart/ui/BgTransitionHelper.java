package com.dole.heightchart.ui;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.view.View;
import android.view.animation.Interpolator;

public class BgTransitionHelper {
	private static final int DURATION = 300;
	
	private View mStartView;
	private View mEndView;
	
	private AnimatorSet mAnimator;
	private ValueAnimator mFadeInStart;
	private ValueAnimator mFadeOutEnd;
	
	boolean mIsNeedReverse = false;
	
	private OnTransitionListener mListener;
	
	public interface OnTransitionListener {
		public void onTransitionStarted(boolean isReverse);
		public void onTransitionFinished(boolean isReverse);
	}
	
	public class ReversibleInterpolator implements Interpolator {
	    @Override
	    public float getInterpolation(float paramFloat) {
	    	if(!mIsNeedReverse)
	    		return paramFloat;
	    	else
	    		return Math.abs(paramFloat - 1f);
	    }
	}
	
	public BgTransitionHelper(View startView, View endView, OnTransitionListener listener) {
		mStartView = startView;
		mEndView = endView;
		mListener = listener;

		updateViews(startView, endView);
	}
	
	public void updateViews(View startView, View endView) {
		mStartView = startView;
		mEndView = endView;
		
		mFadeInStart = ObjectAnimator.ofFloat(mStartView, "alpha", 1.0f, 0f);
		mFadeOutEnd = ObjectAnimator.ofFloat(mEndView, "alpha", 0f, 1.0f);
				
		mAnimator = new AnimatorSet();
		mAnimator.playTogether(mFadeInStart, mFadeOutEnd);
		mAnimator.setInterpolator(new ReversibleInterpolator());

		mAnimator.setDuration(DURATION);
		mAnimator.addListener(new AnimatorListener() {
			
			@Override
			public void onAnimationStart(Animator animation) {
				if(mListener != null)
					mListener.onTransitionStarted(!mIsNeedReverse);
			}
			
			@Override
			public void onAnimationRepeat(Animator animation) {
				
			}
			
			@Override
			public void onAnimationEnd(Animator animation) {
				if(mListener != null)
					mListener.onTransitionFinished(!mIsNeedReverse);
				mIsNeedReverse = !mIsNeedReverse;
			}
			
			@Override
			public void onAnimationCancel(Animator animation) {
				
			}
		});
	}
	
	public void start() {
		if(mAnimator == null)
			return;
		
		if(!mAnimator.isRunning())
			mAnimator.start();
	}
	
	public boolean isOnTransition() {
		return mAnimator.isRunning();
	}
}
