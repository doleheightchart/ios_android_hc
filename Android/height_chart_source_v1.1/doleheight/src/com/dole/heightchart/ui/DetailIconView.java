package com.dole.heightchart.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.dole.heightchart.Constants.Characters;
import com.dole.heightchart.R;

public class DetailIconView extends ImageView {
	
	private boolean mIsCheckForDelete = false;
	private boolean mIsLeft = false;
	private int mCharIndex = 0;
	private boolean mHasArrow = false;
	private boolean mIsInDelete = false;

	private int leftUncheckDrawables = R.drawable.left_arrow_transparent;

	private int leftUncheckOverDrawables = R.drawable.left_arrow_over_transparent;
	
	private int[] leftCheckDrawables = {
			R.drawable.left_arrow_check_monkey_01,
			R.drawable.left_arrow_check_monkey_02,
			R.drawable.left_arrow_check_monkey_03,
			R.drawable.left_arrow_check_monkey_04,
			R.drawable.left_arrow_check_monkey_05,
	};

	private int[] leftCheckOverDrawables = {
			R.drawable.left_arrow_check_over_monkey_01,
			R.drawable.left_arrow_check_over_monkey_02,
			R.drawable.left_arrow_check_over_monkey_03,
			R.drawable.left_arrow_check_over_monkey_04,
			R.drawable.left_arrow_check_over_monkey_05,
	};

	private int rightUncheckDrawables = R.drawable.right_arrow_transparent;

	private int rightUncheckOverDrawables = R.drawable.right_arrow_over_transparent;
	
	private int[] rightCheckDrawables = {
			R.drawable.right_arrow_check_monkey_01,
			R.drawable.right_arrow_check_monkey_02,
			R.drawable.right_arrow_check_monkey_03,
			R.drawable.right_arrow_check_monkey_04,
			R.drawable.right_arrow_check_monkey_05
	};

	private int[] rightCheckOverDrawables = {
			R.drawable.right_arrow_check_over_monkey_01,
			R.drawable.right_arrow_check_over_monkey_02,
			R.drawable.right_arrow_check_over_monkey_03,
			R.drawable.right_arrow_check_over_monkey_04,
			R.drawable.right_arrow_check_over_monkey_05
	};

//	private int[] leftLongClickDrawables = {
//			R.drawable.detail_left_monkey_01,
//			R.drawable.detail_left_monkey_02,
//			R.drawable.detail_left_monkey_03,
//			R.drawable.detail_left_monkey_04,
//			R.drawable.detail_left_monkey_05,
//	};
//	
//	private int[] rightLongClickDrawables = {
//			R.drawable.detail_right_monkey_01,
//			R.drawable.detail_right_monkey_02,
//			R.drawable.detail_right_monkey_03,
//			R.drawable.detail_right_monkey_04,
//			R.drawable.detail_right_monkey_05
//	};
	
	Drawable mCheckDrawable;
//	Drawable mLongClickDrawable;
	Drawable mUncheckDrawable;
	

	public DetailIconView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public DetailIconView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public DetailIconView(Context context) {
		super(context);
	}
	
	public void setCharacterByIsLeft(Characters character, boolean isLeft) {
		mCharIndex = character.getCharacterIndex();
		mIsLeft = isLeft;
	}
	
	public boolean isCheckedForDelete() {
		return mIsCheckForDelete;
	}
	
	public void setCheckForDelete(boolean checked) {
		mIsCheckForDelete = checked;

		invalidate();
	}

	public void setHasArrow(boolean hasArrow) {
		mHasArrow = hasArrow;
	}

	public void setDeleteMode(boolean isInDelete) {
		mIsInDelete = isInDelete;
	}

	public void determineCharacterDrawable() {
		int checkRes = 0;
		int uncheckRes = 0;
		if(mIsLeft && mHasArrow) {
			checkRes = leftCheckDrawables[mCharIndex];
			uncheckRes = leftUncheckDrawables;
		} else if(mIsLeft && !mHasArrow) {
			checkRes = leftCheckOverDrawables[mCharIndex];
			uncheckRes = leftUncheckOverDrawables;
		} else if(!mIsLeft && mHasArrow) {
			checkRes = rightCheckDrawables[mCharIndex];
			uncheckRes = rightUncheckDrawables;
		} else if(!mIsLeft && !mHasArrow) {
			checkRes = rightCheckOverDrawables[mCharIndex];
			uncheckRes = rightUncheckOverDrawables;
		}
		mCheckDrawable = getResources().getDrawable(checkRes);
		mUncheckDrawable = getResources().getDrawable(uncheckRes);
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
//		if(mIsLongClick && mLongClickDrawable != null) {
//			canvas.save();
//			canvas.translate(0, 6);
//			mLongClickDrawable.setBounds(0, 0, getWidth(), getHeight());
//			mLongClickDrawable.draw(canvas);
//			canvas.restore();
//		}
		
		if(mIsInDelete && mUncheckDrawable != null) {
			mUncheckDrawable.setBounds(0, 0, getWidth(), getHeight());
			mUncheckDrawable.draw(canvas);
		}
		
		super.onDraw(canvas);
		
		if(mIsInDelete && mIsCheckForDelete && mCheckDrawable != null) {
			mCheckDrawable.setBounds(0, 0, getWidth(), getHeight());
			mCheckDrawable.draw(canvas);
		}
	}
}
