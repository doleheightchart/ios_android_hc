package com.dole.heightchart.ui;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.widget.TextView;

import com.dole.heightchart.FontCache;

import java.util.Locale;

public class FontTextHelper {
	
	public static final String sEnglishFontPath = "Roboto-Regular.ttf";
	public static final String sEnglishBoldFontPath = "Roboto-Bold.ttf";
	public static final String sKorFontPath = "DroidSansFallback.ttf";
	
	public static final String sGuideDefaultFontPath = "NanumPen.ttf";
	
	public static final String sLangKr = Locale.KOREAN.getLanguage();
	public static final String sLangJa = Locale.JAPANESE.getLanguage();
	
	public static String sTypefaceFilename;

	public static void setFont(Context context, TextView textView) {
		final String currentLang = Locale.getDefault().getLanguage();
		if(sLangKr.equals(currentLang)) {
			sTypefaceFilename = sKorFontPath;
			//sTypefaceFilename = "NanumPen.ttf";
		} else {
			if(textView.getTypeface() != null && (textView.getTypeface().getStyle() & Typeface.BOLD) != 0) {
				//Bold
				sTypefaceFilename = sEnglishBoldFontPath;
			} else {
				sTypefaceFilename = sEnglishFontPath;
			}
		}
		FontCache.setCustomFont(context, textView, sTypefaceFilename);
	}
	
	public static void setGuideFont(Context context, TextView textView) {
		final String currentLang = Locale.getDefault().getLanguage();
		if(!sLangJa.equals(currentLang)) {
			sTypefaceFilename = sGuideDefaultFontPath;
		} else {
			sTypefaceFilename = sEnglishFontPath;
		}
		FontCache.setCustomFont(context, textView, sTypefaceFilename);
	}
	
	public static void setFont(Context context, Paint textPaint) {
		final String currentLang = Locale.getDefault().getLanguage();
		if(sLangKr.equals(currentLang)) {
			sTypefaceFilename = sKorFontPath;
			//sTypefaceFilename = "NanumPen.ttf";
		} else {
			if(textPaint.getTypeface() != null && (textPaint.getTypeface().getStyle() & Typeface.BOLD) != 0) {
				//Bold
				sTypefaceFilename = sEnglishBoldFontPath;
			} else {
				sTypefaceFilename = sEnglishFontPath;
			}
		}
		FontCache.setCustomFont(context, textPaint, sTypefaceFilename);
	}
}
