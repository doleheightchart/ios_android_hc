package com.dole.heightchart.ui;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dole.heightchart.AnimUtil;
import com.dole.heightchart.ApplicationImpl;
import com.dole.heightchart.Constants;
import com.dole.heightchart.Constants.Characters;
import com.dole.heightchart.DLog;
import com.dole.heightchart.Model;
import com.dole.heightchart.NickNameInfo;
import com.dole.heightchart.R;

import java.util.ArrayList;
import java.util.List;

public class CharacterContainer extends LinearLayout {
	
	private static final String TAG = CharacterContainer.class.getName();
	
	private Model mModel;
	private OnClickListener mCharacterClickListener;
	private OnLongClickListener mCharacterLongClickListener;
	private CharacterLoadListener mCharacterLoadListener;
	private boolean mIsDelete;
	
	public interface CharacterLoadListener {
		public void onCharacterLoadFinished(List<View> views);
		public void startCharacterDelete(List<View> views);
		public void startCharacterUpdate(List<View> views);
	}

	public CharacterContainer(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	public CharacterContainer(Context context) {
		this(context, null);
	}
	
	public void setCharacterLoadListener(CharacterLoadListener listener) {
		mCharacterLoadListener = listener;
	}
	
	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		super.onLayout(changed, l, t, r, b);
		int childCount = getChildCount();
		if(mIsDelete)
			childCount--;
		int w = childCount > 2 ? getContext().getResources().getDimensionPixelSize(R.dimen.main_character_between_size_multi)
				: getContext().getResources().getDimensionPixelSize(R.dimen.main_character_between_size_single);
		
		if(childCount > 0) {
			View prevView = getChildAt(0);
			int leftMargin = 0;
			ViewGroup.LayoutParams viewParams = prevView.getLayoutParams();
			if(viewParams != null && viewParams instanceof LinearLayout.LayoutParams) {
				leftMargin = ((LinearLayout.LayoutParams)viewParams).leftMargin;
			}
			int left = leftMargin;
			int right = 0 - prevView.getLeft() + prevView.getRight();
			prevView.layout(left, prevView.getTop(), right, prevView.getBottom());
			
			for(int i = 1; i < childCount; i++) {
				View v = getChildAt(i);
				if(v == null) return;
				left = right + w;
				right = left + v.getWidth();
				v.layout(left, v.getTop(), right, v.getBottom());
				prevView = v;
			}
		}
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		DLog.e(TAG, "onMeasure");
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		int childCount = getChildCount();
		if(mIsDelete)
			childCount--;
		int w = childCount > 2 ? getContext().getResources().getDimensionPixelSize(R.dimen.main_character_between_size_multi)
				: getContext().getResources().getDimensionPixelSize(R.dimen.main_character_between_size_single);

//		int leftMargin = 0;
//		ViewGroup.LayoutParams viewParams = prevView.getLayoutParams();
//		if(viewParams != null && viewParams instanceof LinearLayout.LayoutParams) {
//			leftMargin = ((LinearLayout.LayoutParams)viewParams).leftMargin;
//		}
		if(childCount != 0)
			childCount -= 1;
		
		setMeasuredDimension(getMeasuredWidth()+(childCount*w), getMeasuredHeight());
	}
	
	public void setDeleteState(boolean state) {
		mIsDelete = state;
	}
	
	public void initNicks(OnClickListener characterClickListener, OnLongClickListener characterLongClickListener) {
		mModel = ((ApplicationImpl)getContext().getApplicationContext()).getModel();
		
		mCharacterClickListener = characterClickListener;
		mCharacterLongClickListener = characterLongClickListener;
		
		final ArrayList<NickNameInfo> nickList = mModel.getNickList();
		int len = nickList.size();
		List<NickNameInfo> loadNickList = new ArrayList<NickNameInfo>();
		List<View> removeNickList = new ArrayList<View>();
		List<View> updateNickList = new ArrayList<View>();
		loadNickList.addAll(nickList);
		
		final int childCount = getChildCount();
		int treeMaxHeight = getResources().getDimensionPixelSize(R.dimen.main_character_tree_max_height);
		DLog.e(TAG, "treeMaxHeight:"+treeMaxHeight);
		if(childCount > 0) {
			for(int i = 0; i < childCount ; i++) {
				View child = getChildAt(i);
				Object tag = child.getTag();
				if(tag instanceof NickNameInfo) {
					NickNameInfo info = (NickNameInfo) tag;
					
					if(info.getNickNameId() != NickNameInfo.ADDNICK_OWL_ID && !nickList.contains(info)) {
						//Not exist
						DLog.d(TAG, "------------------- remove view = " + child);
						removeNickList.add(child);
					}

					int viewHeight = computeViewHeight(info.getLastHeight());
					LinearLayout.LayoutParams viewParams = (LinearLayout.LayoutParams)child.getLayoutParams();
					TextView nameView = (TextView)child.findViewById(R.id.character_text);
					ImageView characterView = (ImageView)child.findViewById(R.id.character_animal);
					
					int bottomMargin = viewParams == null ? viewHeight-treeMaxHeight : viewParams.bottomMargin;
					String name = nameView == null ? info.getNickName() : nameView.getText().toString();
					Characters character = characterView == null ? info.getCharacter() : (Characters)characterView.getTag();
					
					if(info.getNickNameId() != NickNameInfo.ADDNICK_OWL_ID && 
							(bottomMargin != viewHeight-treeMaxHeight
							|| !name.equals(info.getNickName())
							|| !character.equals(info.getCharacter()))) {
						updateNickList.add(child);
					}
					
					loadNickList.remove(info);
				}
			}
		} else {
			if(len > 0) {
				NickNameInfo info = new NickNameInfo();
				info.setNickNameId(NickNameInfo.ADDNICK_OWL_ID);
				info.setNickName("");
				info.setLastHeight(0);
				info.setCharacter(Characters.CHARACTER_OWL);
				loadNickList.add(info);
			}
			else {
				NickNameInfo info = new NickNameInfo();
				info.setNickNameId(NickNameInfo.ADDNICK_OWL_ID);
				info.setNickName("");
				info.setLastHeight(0);
				info.setCharacter(Characters.ADD_NICKNAME_OWL);
				loadNickList.add(info);
			}
		}
		
		DLog.e(TAG, "initNicks del size:"+removeNickList.size());
		DLog.e(TAG, "initNicks upd size:"+updateNickList.size());
		DLog.e(TAG, "initNicks load size:"+loadNickList.size());
		if(removeNickList.size() != 0) {
			if(mCharacterLoadListener != null) {
				mCharacterLoadListener.startCharacterDelete(removeNickList);
			}
		}
		else if(updateNickList.size() != 0) {
			if(mCharacterLoadListener != null) {
				mCharacterLoadListener.startCharacterUpdate(updateNickList);
			}
		}
		else if(loadNickList.size() != 0) {
			NickNameInfo[] params = new NickNameInfo[loadNickList.size()];
			loadNickList.toArray(params);

			InflateNickNameViewTask task = new InflateNickNameViewTask();
			task.execute(params);
		}
	}

	public void refreshHeight() {
		final int childCount = getChildCount();
		final Resources res = getResources();
		for(int i = 0 ; i < childCount ; i++) {
			View child = getChildAt(i);

			Object tag = child.getTag();
			if(tag instanceof NickNameInfo) {
				NickNameInfo info = (NickNameInfo) tag;
				if(info.getNickNameId() == NickNameInfo.ADDNICK_OWL_ID)
					continue;

				final float height = info.getLastHeight();
				final Characters character = info.getCharacter();

				if(character != Characters.ADD_NICKNAME_OWL && character != Characters.CHARACTER_OWL) {
					LinearLayout.LayoutParams viewParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
					int treeMaxHeight = res.getDimensionPixelSize(R.dimen.main_character_tree_max_height);
					int treeMinHeight = res.getDimensionPixelSize(R.dimen.main_character_tree_min_height);
					int viewHeight;

					if(height >= Constants.MAX_HEIGHT)
						viewHeight = treeMaxHeight;
					else if(height <= Constants.MIN_HEIGHT)
						viewHeight = treeMinHeight;
					else
						viewHeight = treeMinHeight+((treeMaxHeight-treeMinHeight)/(Constants.MAX_HEIGHT-Constants.MIN_HEIGHT))*((int)height-Constants.MIN_HEIGHT);

					float translateYBy = viewHeight-treeMaxHeight - viewParams.bottomMargin; 
					//child.setLayoutParams(viewParams);
					DLog.d(TAG, " tran Y = " + translateYBy);
					if(translateYBy > 0)
						AnimUtil.getTranslateYByAnimation(child, translateYBy);
					
				}
			}
		}
		
		AnimUtil.startAnimators(300);
	}
	
	public int computeViewHeight(float height) {
		Resources res = getResources();
		int treeMaxHeight = res.getDimensionPixelSize(R.dimen.main_character_tree_max_height);
		int treeMinHeight = res.getDimensionPixelSize(R.dimen.main_character_tree_min_height);

		if(height >= Constants.MAX_HEIGHT)
			return treeMaxHeight;
		else if(height <= Constants.MIN_HEIGHT)
			return treeMinHeight;
		else
			return treeMinHeight+((treeMaxHeight-treeMinHeight)/(Constants.MAX_HEIGHT-Constants.MIN_HEIGHT))*((int)height-Constants.MIN_HEIGHT);
	}
	
	public View configureCharacterView(NickNameInfo info, OnClickListener clickListener) { //String text, float height, Characters character, OnClickListener clickListener) {
		final LayoutInflater inflater = LayoutInflater.from(getContext());
		
		final View v = inflater.inflate(R.layout.character_view, null);
		FrameLayout addButton = (FrameLayout)v.findViewById(R.id.character_add_button_container);
		TextView textDialog = (TextView)v.findViewById(R.id.character_text);
		ImageView animalView = (ImageView)v.findViewById(R.id.character_animal);
		ImageView treeView = (ImageView)v.findViewById(R.id.character_tree);
		ImageView fruitView = (ImageView)v.findViewById(R.id.character_fruit);
		
		final Resources res = getResources();
		RelativeLayout.LayoutParams treeParams = (RelativeLayout.LayoutParams)treeView.getLayoutParams();
		RelativeLayout.LayoutParams animalParams = (RelativeLayout.LayoutParams)animalView.getLayoutParams();
		RelativeLayout.LayoutParams buttonParams = (RelativeLayout.LayoutParams)addButton.getLayoutParams();
		RelativeLayout.LayoutParams dialogParams = (RelativeLayout.LayoutParams)textDialog.getLayoutParams();
		RelativeLayout.LayoutParams fruitParams = (RelativeLayout.LayoutParams)fruitView.getLayoutParams();
		textDialog.setText(info.getNickName());
		
		final Characters character = info.getCharacter();
		
		if(character != Characters.ADD_NICKNAME_OWL && character != Characters.CHARACTER_OWL) {
			LinearLayout.LayoutParams viewParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			int treeMaxHeight = res.getDimensionPixelSize(R.dimen.main_character_tree_max_height);
			int viewHeight = computeViewHeight(info.getLastHeight());

			viewParams.bottomMargin = viewHeight-treeMaxHeight;
			v.setLayoutParams(viewParams);
		}
		
		switch(character) {
		case ADD_NICKNAME_OWL :
			treeView.setImageDrawable(res.getDrawable(R.drawable.add_nickname_tree));
			treeParams.leftMargin = res.getDimensionPixelSize(R.dimen.main_character_tree_margin_left);
			animalParams.leftMargin = 0;
			animalParams.bottomMargin = res.getDimensionPixelSize(R.dimen.main_character_add_owl_bottom_margin);
			buttonParams.bottomMargin = res.getDimensionPixelSize(R.dimen.main_character_add_button_add_bottom_margin);
			textDialog.setVisibility(View.GONE);
			fruitView.setVisibility(View.GONE);
			v.setPadding(getResources().getDimensionPixelSize(R.dimen.main_character_add_owl_left_padding), 0, 0, 0);
			break;
			
		case CHARACTER_OWL :
			treeView.setImageDrawable(res.getDrawable(R.drawable.add_nickname_tree));
			treeParams.leftMargin = 0;
			animalParams.leftMargin = res.getDimensionPixelSize(R.dimen.main_character_owl_margin_left);
			animalParams.bottomMargin = res.getDimensionPixelSize(R.dimen.main_character_owl_margin_bottom);
			buttonParams.bottomMargin = res.getDimensionPixelSize(R.dimen.main_character_add_button_main_bottom_margin);
			textDialog.setVisibility(View.GONE);
			fruitView.setVisibility(View.GONE);
			break;
			
		case CHARACTER_DOLEKEY :
		case CHARACTER_RILLA :
		case CHARACTER_TEENY :
		case CHARACTER_COCO :
		case CHARACTER_PANZEE :
			treeView.setImageDrawable(res.getDrawable(R.drawable.main_monkey_tree_01 + character.getCharacterIndex()));
			fruitView.setImageDrawable(res.getDrawable(R.drawable.main_monkey_fruit_01 + character.getCharacterIndex()));
			textDialog.setTextColor(res.getColorStateList(character.getTagNameColor()));
			treeParams.leftMargin = 0;
			animalParams.leftMargin = res.getDimensionPixelSize(R.dimen.main_character_owl_margin_left);
			animalParams.bottomMargin = res.getDimensionPixelSize(R.dimen.main_character_owl_margin_bottom);
			dialogParams.bottomMargin = res.getDimensionPixelSize(R.dimen.main_character_animal_bottom_margin);
			fruitParams.topMargin = res.getDimensionPixelSize(R.dimen.main_character_fruit_margin_bottom);
			addButton.setVisibility(View.GONE);
			break;
		}
		
		if(addButton.getVisibility() == View.VISIBLE) {
			addButton.setOnClickListener(clickListener);
		}
		
		return v;
	}
	
	private class InflateNickNameViewTask extends AsyncTask<NickNameInfo, Integer, List<View>> {
		
		@Override
		protected List<View> doInBackground(NickNameInfo... params) {
			
			if(params == null || params.length == 0)
				return null;
			List<View> inflatedViews = new ArrayList<View>();
			
			for(NickNameInfo info : params) {
				View v = configureCharacterView(info, mCharacterClickListener);
				DLog.d(TAG, " v = " + v + " info id = " + info.getNickNameId());
				v.setTag(info);
				inflatedViews.add(v);
				
				ImageView imgView = (ImageView)v.findViewById(R.id.character_animal);
				Drawable draw = getResources().getDrawable(getDrawableId(info.getCharacter(), false));
				imgView.setImageDrawable(draw);
				
				if(info.getNickNameId() != 0)
					imgView.setTag(info.getCharacter());
				
				imgView.setOnClickListener(mCharacterClickListener);
				imgView.setOnLongClickListener(mCharacterLongClickListener);
			}
			return inflatedViews;
		}

		@Override
		protected void onPostExecute(List<View> result) {
			super.onPostExecute(result);
			
			if(mCharacterLoadListener != null)
				mCharacterLoadListener.onCharacterLoadFinished(result);
		}
	}
	
	public static int getDrawableId(Characters character, boolean isDelete) {
		if(isDelete) {
			switch(character) {
			case CHARACTER_DOLEKEY :
				return R.drawable.delete_monkey_01;
			case CHARACTER_RILLA :
				return R.drawable.delete_monkey_02;
			case CHARACTER_TEENY :
				return R.drawable.delete_monkey_03;
			case CHARACTER_COCO :
				return R.drawable.delete_monkey_04;
			case CHARACTER_PANZEE :
				return R.drawable.delete_monkey_05;
			default :
				return -1;
			}
		}
		else {
			switch(character) {
			case ADD_NICKNAME_OWL :
				return R.drawable.main_add_nickname_ani;
			case CHARACTER_OWL :
				return R.drawable.main_owl_ani;
			case CHARACTER_DOLEKEY :
				return R.drawable.main_dolekey_ani;
			case CHARACTER_RILLA :
				return R.drawable.main_rilla_ani;
			case CHARACTER_TEENY :
				return R.drawable.main_smong_ani;
			case CHARACTER_COCO :
				return R.drawable.main_faka_ani;
			case CHARACTER_PANZEE :
				return R.drawable.main_panzee_ani;
			default :
				return -1;
			}
		}
	}
	
	public int getInitPos() {
		int childCount = getChildCount(); 
		DLog.e(TAG, "childCount:"+childCount);
		if(childCount > 1) {
			View lastChild = getChildAt(childCount-2);
			DLog.e(TAG, "lastChild.getRight():"+lastChild.getRight());
			if(lastChild.getRight() == 0) {
				return getWidth() - getResources().getDisplayMetrics().widthPixels;
			}
			else
				return lastChild.getRight() - getResources().getDisplayMetrics().widthPixels;
		}
		else
			return 0;
	}
}
