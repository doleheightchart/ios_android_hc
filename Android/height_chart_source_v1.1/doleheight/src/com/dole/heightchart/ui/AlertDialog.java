package com.dole.heightchart.ui;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.dole.heightchart.Constants;
import com.dole.heightchart.R;
import com.dole.heightchart.SplashFragment;
import com.dole.heightchart.Util;

public class AlertDialog extends DialogFragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		final Dialog d = getDialog();
		d.getWindow().setBackgroundDrawable(new ColorDrawable(0));
		
		int divierId = d.getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
		View divider = getDialog().findViewById(divierId);
		divider.setVisibility(View.GONE);
		
		int titleId = d.getContext().getResources().getIdentifier("android:id/title", null, null);
		View title = getDialog().findViewById(titleId);
		title.setVisibility(View.GONE);
		
		return inflater.inflate(R.layout.custom_dialog, null);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		Bundle args = getArguments();
		if(args == null)
			return;
		boolean isTitle = args.getBoolean(Constants.CUSTOM_DIALOG_IS_TITLE, false);
		int titleRes = args.getInt(Constants.CUSTOM_DIALOG_TITLE_RES, 0);
		int imgRes = args.getInt(Constants.CUSTOM_DIALOG_IMG_RES, 0);
		int textRes = args.getInt(Constants.CUSTOM_DIALOG_TEXT_RES, 0);
		int textResParam1 = args.getInt(Constants.CUSTOM_DIALOG_TEXT_RES_PARAM1, -1);
		String textString = args.getString(Constants.CUSTOM_DIALOG_TEXT_STRING, "");
		int leftRes = args.getInt(Constants.CUSTOM_DIALOG_LEFT_BUTTON_RES, 0);
		int rightRes = args.getInt(Constants.CUSTOM_DIALOG_RIGHT_BUTTON_RES, 0);
		boolean isYesNo = args.getBoolean(Constants.CUSTOM_DIALOG_CHANGE_TEXT_YES_NO, false);
		boolean allowCancel = args.getBoolean(Constants.CUSTOM_DIALOG_ALLOW_CANCEL, false);
//		mRequestCode = args.getInt(Constants.CUSTOM_DIALOG_REQUEST_CODE, 0);
		
		int leftTextRes = args.getInt(Constants.CUSTOM_DIALOG_LEFT_BUTTON_TEXT_RES, 0);
		int rightTextRes = args.getInt(Constants.CUSTOM_DIALOG_RIGHT_BUTTON_TEXT_RES, 0);

		TextView titleView = (TextView)view.findViewById(R.id.dialog_title);
		View noTitleHeader = view.findViewById(R.id.dialog_no_title_header);
		ImageView titleImage = (ImageView)view.findViewById(R.id.custom_dialog_image);
		TextView content = (TextView)view.findViewById(R.id.custom_dialog_content);
		TextView detail = (TextView)view.findViewById(R.id.custom_dialog_detail);
		Button leftBtn = (Button)view.findViewById(R.id.custom_dialog_button_left);
		Button rightBtn = (Button)view.findViewById(R.id.custom_dialog_button_right);
		
		if(isTitle) {
			titleView.setText(titleRes);
			noTitleHeader.setVisibility(View.GONE);
		}
		else {
			titleImage.setImageResource(imgRes);
			titleView.setVisibility(View.GONE);
		}
		
		if(textRes > 0 && textResParam1 > 0)
			content.setText(getResources().getString(textRes, textResParam1));
		else if(textRes > 0)
			content.setText(textRes);

		if("".equals(textString))
			detail.setVisibility(View.GONE);
		else
			detail.setText(textString);
		
		if(leftRes > 0)
			leftBtn.setBackgroundResource(leftRes);
		else 
			leftBtn.setVisibility(View.GONE);
		
		if(rightRes > 0)
			rightBtn.setBackgroundResource(rightRes);
		else 
			rightBtn.setVisibility(View.GONE);
		
		if(leftTextRes > 0) 
			leftBtn.setText(leftTextRes);
		
		if(rightTextRes > 0)
			rightBtn.setText(rightTextRes);
		
		if(isYesNo) {
			leftBtn.setText(R.string.no);
			rightBtn.setText(R.string.yes);
		}
		
		leftBtn.setOnClickListener(mOnClickListener);
		rightBtn.setOnClickListener(mOnClickListener);
		
//		if(allowCancel) {
//			getDialog().setOnKeyListener(new OnKeyListener() {
//				@Override
//				public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
//					if ((keyCode ==  android.view.KeyEvent.KEYCODE_BACK)) {
//						if(getTargetFragment() != null) {
//							getTargetFragment().onActivityResult(
//									getTargetRequestCode(),
//									Activity.RESULT_CANCELED, 
//									new Intent());
//						}
//					}
//					return false;
//				}
//			});
//		}
	}

	private OnClickListener mOnClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			dismiss();
			Util.replaceFragment(getFragmentManager(), new SplashFragment(), R.id.splash_container, Constants.TAG_FRAGMENT_SPLASH, null, false);
		}
	};
}
