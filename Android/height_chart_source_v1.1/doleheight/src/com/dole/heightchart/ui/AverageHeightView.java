package com.dole.heightchart.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;

import com.dole.heightchart.R;

public class AverageHeightView extends View {
	private Bitmap mLineBitmap;
	private int mInterval;

	public AverageHeightView(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		mLineBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.graph_line_02);
		mInterval = getResources().getDimensionPixelSize(R.dimen.detail_tree_avg_height_dot_interval);
	}

	public AverageHeightView(Context context) {
		this(context, null);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		int pos = 7, width = getWidth();
		while(pos < width) {
			canvas.drawBitmap(mLineBitmap, pos, 0, null);
			pos += mInterval;
		}
	}
}
