package com.dole.heightchart.ui;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.Interpolator;
import android.view.animation.ScaleAnimation;
import android.widget.ListView;

import com.dole.heightchart.DetailFragment;
import com.dole.heightchart.R;
import com.dole.heightchart.DetailFragment.ModeChangeListener;

public class FullHeightTree extends ListView {

	private ModeChangeListener mListener;

	public FullHeightTree(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public FullHeightTree(Context context) {
		this(context, null);
	}
	
	public void changeZoomMode() {
		animationImage();
	}
	
	private void animationImage() {
		int i, len = getChildCount();
		int leftPivot = getResources().getDimensionPixelSize(R.dimen.detail_tree_full_item_image_width)+
				(getResources().getDimensionPixelSize(R.dimen.detail_tree_full_item_width))/2;
		int rightPivot = -getResources().getDimensionPixelSize(R.dimen.detail_tree_full_item_width)/2;
		ScaleAnimation leftScaleAni = new ScaleAnimation(1, 0, 1, 0, Animation.ABSOLUTE, leftPivot, Animation.RELATIVE_TO_SELF, 0.5f);
		leftScaleAni.setDuration(DetailFragment.ZOOM_MODE_CHANGE_INTERVAL);
		leftScaleAni.setRepeatCount(0);
		ScaleAnimation rightScaleAni = new ScaleAnimation(1, 0, 1, 0, Animation.ABSOLUTE, rightPivot, Animation.RELATIVE_TO_SELF, 0.5f);
		rightScaleAni.setDuration(DetailFragment.ZOOM_MODE_CHANGE_INTERVAL);
		rightScaleAni.setRepeatCount(0);
		for(i = 0; i < len; i++) {
			ViewGroup child = (ViewGroup)getChildAt(i);
			if(child.getId() != R.id.detail_height_tree_item)
				return;
			ViewGroup leftItem = (ViewGroup)child.findViewById(R.id.detail_height_tree_left_item_container);
			ViewGroup rightItem = (ViewGroup)child.findViewById(R.id.detail_height_tree_right_item_container);
			
			if(leftItem != null) {
				animationChildrens(leftItem, leftScaleAni, true);
			}

			if(rightItem != null) {
				animationChildrens(rightItem, rightScaleAni, false);
			}
			View avgHeight = child.findViewById(R.id.detail_height_tree_item_avg_height_container);
			if(avgHeight != null && avgHeight.getVisibility() == View.VISIBLE)
				avgHeight.setVisibility(View.INVISIBLE);
		}
		animationTree();
	}
	
	private void animationChildrens(ViewGroup parent, Animation animation, boolean isLeft) {
		int i, len = parent.getChildCount();
		int imageViewId = isLeft == true ? R.id.detail_height_tree_left_image : R.id.detail_height_tree_right_image;
		int textHeightId = isLeft == true ? R.id.detail_height_tree_left_height_text : R.id.detail_height_tree_right_height_text;
		int textDateId = isLeft == true ? R.id.detail_height_tree_left_date_text : R.id.detail_height_tree_right_date_text;
		for(i = 0; i < len; i++) {
			View targetParent = parent.getChildAt(i);
			View image = targetParent.findViewById(imageViewId);
			if(image != null) {
				image.startAnimation(animation);
			}
			View textHeight = targetParent.findViewById(textHeightId);
			if(textHeight != null)
				textHeight.setVisibility(View.GONE);
			View textDate = targetParent.findViewById(textDateId);
			if(textDate != null)
				textDate.setVisibility(View.GONE);
		}
	}
	
	public void restoreVisibility() {
		int i, len = getChildCount();
		for(i = 0; i < len; i++) {
			ViewGroup child = (ViewGroup)getChildAt(i);
			if(child.getId() != R.id.detail_height_tree_item)
				return;
			View leftItem = child.findViewById(R.id.detail_height_tree_left_item_container);
			View rightItem = child.findViewById(R.id.detail_height_tree_right_item_container);
			
			if(leftItem != null) {
				View textHeight = leftItem.findViewById(R.id.detail_height_tree_left_height_text);
				if(textHeight != null)
					textHeight.setVisibility(View.VISIBLE);
				View textDate = leftItem.findViewById(R.id.detail_height_tree_left_date_text);
				if(textDate != null)
					textDate.setVisibility(View.VISIBLE);
			}

			if(rightItem != null) {
				View textHeight = leftItem.findViewById(R.id.detail_height_tree_right_height_text);
				if(textHeight != null)
					textHeight.setVisibility(View.VISIBLE);
				View textDate = leftItem.findViewById(R.id.detail_height_tree_right_date_text);
				if(textDate != null)
					textDate.setVisibility(View.VISIBLE);
			}
			View avgHeight = child.findViewById(R.id.detail_height_tree_item_avg_height_container);
			if(avgHeight != null && avgHeight.getVisibility() == View.INVISIBLE)
				avgHeight.setVisibility(View.VISIBLE);
		}
		setVisibility(View.VISIBLE);
	}
	
	private void animationTree() {
		final ZoomHeightTree trgView = (ZoomHeightTree)((Activity)getContext()).findViewById(R.id.detail_height_tree_scroll_view);
		Resources res = getResources();
		int srcWidth = res.getDimensionPixelSize(R.dimen.detail_tree_full_item_width);
		int trgWidth = res.getDimensionPixelSize(R.dimen.detail_tree_zoom_node_width);
		int srcHeight = getMeasuredHeight();
		int trgHeight = trgView.getMeasuredHeight();
//		DoleLog.Loge(TAG, "srcWidth:"+srcWidth+",trgWidth:"+trgWidth);
//		DoleLog.Loge(TAG, "srcHeight:"+srcHeight+",trgHeight:"+trgHeight);
		float widthRatio = (float)trgWidth / (float)srcWidth;
		float heightRatio = (float)trgHeight / (float)srcHeight;
//		DoleLog.Loge(TAG, "widthRatio:"+widthRatio+",heightRatio:"+heightRatio);
		ScaleAnimation scaleAnimation = new ScaleAnimation(1, widthRatio, 1, heightRatio, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 1f);
		scaleAnimation.setDuration(DetailFragment.ZOOM_MODE_CHANGE_INTERVAL);
		scaleAnimation.setRepeatCount(0);
		scaleAnimation.setInterpolator(new Interpolator() {
			@Override
			public float getInterpolation(float arg0) {
				return arg0;
			}
		});
		scaleAnimation.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
			}
			
			@Override
			public void onAnimationRepeat(Animation animation) {
			}
			
			@Override
			public void onAnimationEnd(Animation animation) {
				setVisibility(View.INVISIBLE);
				trgView.restoreVisibility();
				if(mListener != null) {
					mListener.onAnimationEnd();
				}
			}
		});
		startAnimation(scaleAnimation);
	}
	
	public void setModeChangeListener(ModeChangeListener listener) {
		mListener = listener;
	}
}
