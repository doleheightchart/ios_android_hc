package com.dole.heightchart;

import android.net.Uri;
import android.provider.BaseColumns;

public class TableColumns {
	
//	public static class User implements BaseColumns {
//		public static final String USER_NO = "user_no";
//		//TODO
//	}
	
	public static class Nickname implements BaseColumns {
		public static final Uri URI = Uri.parse("content://" + HeightChartProvider.AUTHORITY + "/" + HeightChartProvider.TABLE_NICKNAME);
		
		public static final String NICKNAME = "nickname";
		public static final String CHARACTER = "character";
		public static final String BIRTHDAY = "birthday";
		public static final String GENDER = "gender";
		//TODO
	}
	
	public static class Height implements BaseColumns {
		public static final Uri URI = Uri.parse("content://" + HeightChartProvider.AUTHORITY + "/" + HeightChartProvider.TABLE_HEIGHT);
		
		public static final String NICK_ID = "nick_id"; 
		public static final String HEIGHT = "height";
		public static final String INPUT_DATE = "input_date";
		public static final String IMAGE_PATH = "image_path";
		public static final String QRCODE = "qrcode";
		public static final String COUPON_TYPE = "coupon_type";
		//TODO
	}

}
