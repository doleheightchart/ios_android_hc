package com.dole.heightchart;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

public class ChooseNickNameDialog extends DialogFragment {
	
	private List<NickNameInfo> mNicks;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		final Model model = ((ApplicationImpl)getActivity().getApplication()).getModel();
		mNicks = model.getNickList();
		
		int titleHeight = getResources().getDimensionPixelSize(R.dimen.dialog_title_height);
		int buttonHeight = getResources().getDimensionPixelSize(R.dimen.dialog_button_height);
		int itemHeight = getResources().getDimensionPixelSize(R.dimen.dialog_item_height);
		
		final int totalHeight = titleHeight + buttonHeight + (itemHeight * Math.min(3, 1 + mNicks.size()));
		
		final Dialog d = getDialog();
		d.getWindow().setBackgroundDrawable(new ColorDrawable(0));
		
		int divierId = d.getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
		View divider = getDialog().findViewById(divierId);
		divider.setVisibility(View.GONE);
		
		int titleId = d.getContext().getResources().getIdentifier("android:id/title", null, null);
		View title = getDialog().findViewById(titleId);
		title.setVisibility(View.GONE);
		
		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
	    lp.copyFrom(d.getWindow().getAttributes());
	    lp.height = totalHeight;
	    d.getWindow().setAttributes(lp);
		
		return inflater.inflate(R.layout.nickname_chooser, null);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		final ListView listView = (ListView) view.findViewById(R.id.dialog_content);
		View button = view.findViewById(R.id.dialog_button);
		
		AddOrChooseNicknameAdapter adapter = new AddOrChooseNicknameAdapter(mNicks);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(mOnItemClickListener);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
			}
		});
	}

	private OnItemClickListener mOnItemClickListener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			dismiss();
			Object tag = view.getTag();
			if(tag instanceof NickNameInfo) {
				final NickNameInfo nickInfo = (NickNameInfo) tag;
				final Bundle args = new Bundle();
				args.putLong(Constants.NICKNAME_ID, nickInfo.getNickNameId());
				args.putBoolean(Constants.BYPASS_DETAIL_FRAG, true);
				Util.replaceFragment(getFragmentManager(), new DetailFragment(), 
						R.id.container, Constants.TAG_FRAGMENT_DETAIL, args, true);
			} else {
				Util.replaceFragment(getFragmentManager(), new AddNicknameFragment(), 
						R.id.container, Constants.TAG_FRAGMENT_ADD_NICK, null, true);
			}
		}
	};

	class AddOrChooseNicknameAdapter extends BaseAdapter {
		List<NickNameInfo> mNicks;
		LayoutInflater inflater;

		public AddOrChooseNicknameAdapter(List<NickNameInfo> nicks) {
			mNicks = nicks;
			inflater = LayoutInflater.from(getActivity());
		}

		@Override
		public int getCount() {
			if(mNicks == null)
				return 1;
			return mNicks.size() + 1;
		}
		
		@Override
		public Object getItem(int position) {
			if(mNicks == null)
				return -1;
			return mNicks.get(position);
		}

		@Override
		public long getItemId(int position) {
			if(mNicks == null)
				return -1;
			return position;
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if(convertView == null) {
				convertView = inflater.inflate(R.layout.nickname_list_item, null);
			}
			
			TextView itemView = (TextView)convertView.findViewById(R.id.nickname_item);
			final Resources res = getActivity().getResources();
			final int avatarDrawable;
			final String text;
			final NickNameInfo info;
			if(position == 0) {
				avatarDrawable = R.drawable.popup_add_btn;
				text = res.getString(R.string.add_nickname);
				convertView.setTag(null);
			} else {
				info = mNicks.get(position - 1);
				avatarDrawable = R.drawable.popup_monkey_01 + info.mCharacter.getCharacterIndex();
				text = info.mNickName;
				convertView.setTag(info);
			}
			
			itemView.setCompoundDrawablesWithIntrinsicBounds(res.getDrawable(avatarDrawable), null, null, null);
			itemView.setText(text);
			
			return convertView;
		}
		
	}
}
