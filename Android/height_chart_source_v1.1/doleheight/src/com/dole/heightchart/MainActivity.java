package com.dole.heightchart;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentManager.BackStackEntry;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.dole.heightchart.ui.AlertDialog;
import com.facebook.AppEventsLogger;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.mixpanel.android.mpmetrics.MixpanelAPI;

import jp.co.CAReward_Ack.CARController;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends Activity {

	private static final String	TAG									= "MainActivity";
	private static final int	CONTAINER_ID						= R.id.container;
	private final static int	PLAY_SERVICES_RESOLUTION_REQUEST	= 9000;

	private String				SENDER_ID							= "552152520824";

	private MediaPlayer			mMediaPlayer;

	GoogleCloudMessaging		mGcm;
	AtomicInteger				mMsgId								= new AtomicInteger();
	SharedPreferences			prefs;
	String						regid;

	boolean						mIsFirst;

	private boolean				mIsCARewardEnabled					= false;

	MixpanelAPI mMixpanel;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		DLog.d( TAG, "activity oncreate" );
		
		mMixpanel = MixpanelAPI.getInstance(this, Util.MIXPANEL_TOKEN);
		
		// Create Mixpanel distinct ID if not existent
		String distinctID = mMixpanel.getPeople().getDistinctId();
		if (distinctID == null) {
			String email = HeightChartPreference.getString(Constants.PREF_USER_ID, null);
			
			if (email == null){ // No email registered during launch
				
				JSONObject props = new JSONObject();
				try {
					props.put("User Type", "Anonymous");
					mMixpanel.registerSuperProperties(props);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();					
				}
				
				String uuid = UUID.randomUUID().toString();
				mMixpanel.identify(uuid);
				mMixpanel.getPeople().identify(mMixpanel.getDistinctId());
			} else {
				JSONObject props = new JSONObject();
				try {
					props.put("User Type", "Registered");
					props.put("Email", email);
					mMixpanel.registerSuperProperties(props);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();					
				}
				mMixpanel.identify(email);
				mMixpanel.getPeople().identify(mMixpanel.getDistinctId());
				mMixpanel.getPeople().set("$email", email);
			}
		} else {
			// Already has distinctID, now check if registered or not-registered
			String email = HeightChartPreference.getString(Constants.PREF_USER_ID, null);
			JSONObject props = new JSONObject();
			try {
				if (email == null) {
					props.put("User Type", "Anonymous");
				} else {
					props.put("User Type", "Registered");
					props.put("Email", email);
                    // No need for mixpanel.identify() since we have distinctId
					mMixpanel.getPeople().identify(mMixpanel.getDistinctId());
					mMixpanel.getPeople().set("$email", email);
				}
				mMixpanel.registerSuperProperties(props);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();					
			}
		}
		mMixpanel.getPeople().initPushHandling(Util.GCM_SENDERID);
		mMixpanel.track("App Launched", null);
		
		try {
			PackageInfo info = getPackageManager().getPackageInfo( "com.dole.heightchart", PackageManager.GET_SIGNATURES );
			for ( Signature signature : info.signatures ) {
				MessageDigest md = MessageDigest.getInstance( "SHA" );
				md.update( signature.toByteArray() );

				String strkeyHash = Base64.encodeToString( md.digest(), Base64.DEFAULT );
				Log.d( "KeyHash:", strkeyHash );
			}
		} catch ( NameNotFoundException e ) {

		} catch ( NoSuchAlgorithmException e ) {

		}

		Locale currentLocale = Locale.getDefault();
		
		mIsFirst = HeightChartPreference.getBoolean( Constants.PREF_IS_SIGN_UP, true );
		String email = HeightChartPreference.getString(Constants.PREF_USER_ID, null);
		if(email != null) {
			mIsFirst = false;
		}
		
		if ( Locale.JAPAN.equals( currentLocale ) && VERSION.SDK_INT >= 17 ) {
			//mIsCARewardEnabled = true;
			mIsCARewardEnabled = false;

			/* JPY 06202015 - Removed system migration popup
			if ( mIsFirst ) {

				Bundle args = new Bundle();
				args.putInt( Constants.CUSTOM_DIALOG_TEXT_RES, R.string.launch_agreement_jp );
				args.putInt( Constants.CUSTOM_DIALOG_RIGHT_BUTTON_RES, R.drawable.popup_ok_btn );
				args.putBoolean( Constants.CUSTOM_DIALOG_ALLOW_CANCEL, true );
				Util.showDialogFragment( getFragmentManager(), new AlertDialog(), Constants.TAG_DIALOG_CUSTOM, args );
			} */
		} 
		/* JPY 06202015 - Removed system migration popup
		else if ( Locale.KOREA.equals( currentLocale ) ) {
			if ( mIsFirst ) {

				Bundle args = new Bundle();
				args.putInt( Constants.CUSTOM_DIALOG_TEXT_RES, R.string.launch_agreement_ko );
				args.putInt( Constants.CUSTOM_DIALOG_RIGHT_BUTTON_RES, R.drawable.popup_ok_btn );
				args.putBoolean( Constants.CUSTOM_DIALOG_ALLOW_CANCEL, true );
				Util.showDialogFragment( getFragmentManager(), new AlertDialog(), Constants.TAG_DIALOG_CUSTOM, args );
			}
		} else {
			if ( mIsFirst ) {

				Bundle args = new Bundle();
				args.putInt( Constants.CUSTOM_DIALOG_TEXT_RES, R.string.launch_agreement_en );
				args.putInt( Constants.CUSTOM_DIALOG_RIGHT_BUTTON_RES, R.drawable.popup_ok_btn );
				args.putBoolean( Constants.CUSTOM_DIALOG_ALLOW_CANCEL, true );
				Util.showDialogFragment( getFragmentManager(), new AlertDialog(), Constants.TAG_DIALOG_CUSTOM, args );
			}
		}
		*/
		if ( Constants.USE_CA_REWARD && mIsCARewardEnabled )
			StrictMode.enableDefaults();

		super.onCreate( savedInstanceState );

		setContentView( R.layout.activity_main );
		LoginBeforeOptionFragment.mDoNotShowToast = false;

		if ( savedInstanceState == null ) {
			Util.replaceFragment( getFragmentManager(), new SplashFragment(), R.id.splash_container, Constants.TAG_FRAGMENT_SPLASH, null, false );
		}

		if ( checkPlayServices() ) {
			// TODO Toast error?

			mGcm = GoogleCloudMessaging.getInstance( this );
			regid = getRegistrationId( this );

			if ( regid.isEmpty() ) {
				registerInBackground();
			}
		}

		if ( Constants.USE_CA_REWARD && mIsCARewardEnabled )
			register_CA_Reward();
	}

	private void register_CA_Reward() {
		CARController.appkey = "ncIdX3la";
		CARController.cid = "11897";
		CARController.pid = "1";
		CARController.mcwait = false;
		CARController.nor = 1;
		CARController.cpi = "1";
		CARController.analytics = false;
		CARController.notifyAppLaunch( this.getApplicationContext(), this.getIntent() );
	}

	@Override
	protected void onResume() {
		super.onResume();
		mMixpanel.track("Landing", null);
		if ( HeightChartPreference.getBoolean( Constants.PREF_IS_BGM_ON, true ) )
			playBgm();
	}

	@Override
	protected void onPause() {
		super.onPause();

		stopBgm();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return false;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if ( id == R.id.action_settings ) {
			return true;
		}
		return super.onOptionsItemSelected( item );
	}

	public void playBgm() {
		if ( mMediaPlayer != null && mMediaPlayer.isPlaying() ) {
			return;
		}

		AssetFileDescriptor afd = null;
		try {
			afd = getAssets().openFd( "dole_bgm_main.ogg" );

			mMediaPlayer = new MediaPlayer();
			mMediaPlayer.setDataSource( afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength() );
			mMediaPlayer.setLooping( true );
			mMediaPlayer.prepare();
			mMediaPlayer.start();
		} catch ( IOException e ) {
			DLog.d( TAG, " IO error while playe bgm", e );
		} finally {
			if ( afd != null ) {
				try {
					afd.close();
				} catch ( IOException e ) {
					android.util.Log.d( TAG, " Closing Asset file descriptor failed", e );
				}
			}
		}
	}

	public void stopBgm() {
		if ( mMediaPlayer != null ) {
			mMediaPlayer.stop();
		}
	}

	/**
	 * Used in xml close button
	 * 
	 * @param view
	 *            Not used
	 */
	public void backPress(View view) {
		InputMethodManager imm = (InputMethodManager) getSystemService( Context.INPUT_METHOD_SERVICE );
		imm.hideSoftInputFromWindow( view.getWindowToken(), 0 );
		onBackPressed();
	}

	@Override
	public void onBackPressed() {
		int count = getFragmentManager().getBackStackEntryCount();
		if ( count > 0 ) {
			BackStackEntry entry = getFragmentManager().getBackStackEntryAt( count - 1 );
			if ( Constants.TAG_FRAGMENT_SPLASH.equals( entry.getName() ) ) {
				finish();
			} else if ( Constants.TAG_FRAGMENT_CHART.equals( entry.getName() ) ) {
				// setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
			} else if ( Constants.TAG_FRAGMENT_INPUT_HEIGHT.equals( entry.getName() ) ) {
				getFragmentManager().popBackStack( Constants.TAG_FRAGMENT_QRCODE, FragmentManager.POP_BACK_STACK_INCLUSIVE );
				return;
			} else if ( Constants.TAG_FRAGMENT_DETAIL.equals( entry.getName() ) ) {
				// SplashFragment splash = (SplashFragment) getFragmentManager().findFragmentByTag(Constants.TAG_FRAGMENT_SPLASH);
				// FragmentTransaction ft = getFragmentManager().beginTransaction();
				// ft.show(splash);
				// ft.commit();
				// SplashFragment splash = (SplashFragment) getFragmentManager().findFragmentByTag(Constants.TAG_FRAGMENT_SPLASH);
				// if(splash != null) {
				// final CharacterContainer container = (CharacterContainer) splash.getView().findViewById(R.id.character_container);
				// DoleLog.Logd(TAG, " refresh!!!!!!");
				// container.refreshHeight();
				// }
			}

			if ( isFinishing() )
				return;

			getFragmentManager().popBackStackImmediate();
			new Handler().post( new Runnable() {

				@Override
				public void run() {

					int count = getFragmentManager().getBackStackEntryCount();
					if ( count > 0 ) {
						final BackStackEntry entry = getFragmentManager().getBackStackEntryAt( count - 1 );
						if ( Constants.TAG_FRAGMENT_SPLASH.equals( entry.getName() ) ) {
							FragmentTransaction ft = getFragmentManager().beginTransaction();

							Fragment splash = getFragmentManager().findFragmentByTag( Constants.TAG_FRAGMENT_SPLASH );
							ft.show( splash );
							ft.commit();

							Locale currentLocale = Locale.getDefault();
							mIsFirst = HeightChartPreference.getBoolean( Constants.PREF_IS_SIGN_UP, true );
							String email = HeightChartPreference.getString(Constants.PREF_USER_ID, null);
							if(email != null) {
								mIsFirst = false;
							}
							//mIsFirst = HeightChartPreference.getBoolean( Constants.PREF_IS_SIGN_IN, true );
							if ( Locale.JAPAN.equals( currentLocale ) && VERSION.SDK_INT >= 17 ) {
								//mIsCARewardEnabled = true;
								mIsCARewardEnabled = false;
								
								/* JPY 06202015 - Removed system migration popup
								if ( mIsFirst ) {
									
									Bundle args = new Bundle();
									args.putInt( Constants.CUSTOM_DIALOG_TEXT_RES, R.string.launch_agreement_jp );
									args.putInt( Constants.CUSTOM_DIALOG_RIGHT_BUTTON_RES, R.drawable.popup_ok_btn );
									args.putBoolean( Constants.CUSTOM_DIALOG_ALLOW_CANCEL, true );
									Util.showDialogFragment( getFragmentManager(), new AlertDialog(), Constants.TAG_DIALOG_CUSTOM, args );
								} */
							} else if ( Locale.KOREA.equals( currentLocale ) ) {
								/* JPY 06202015 - Removed system migration popup
								if ( mIsFirst ) {

									Bundle args = new Bundle();
									args.putInt( Constants.CUSTOM_DIALOG_TEXT_RES, R.string.launch_agreement_ko );
									args.putInt( Constants.CUSTOM_DIALOG_RIGHT_BUTTON_RES, R.drawable.popup_ok_btn );
									args.putBoolean( Constants.CUSTOM_DIALOG_ALLOW_CANCEL, true );
									Util.showDialogFragment( getFragmentManager(), new AlertDialog(), Constants.TAG_DIALOG_CUSTOM, args );
								}
								*/
							} else {
								/* JPY 06202015 - Removed system migration popup
								if ( mIsFirst ) {

									Bundle args = new Bundle();
									args.putInt( Constants.CUSTOM_DIALOG_TEXT_RES, R.string.launch_agreement_en );
									args.putInt( Constants.CUSTOM_DIALOG_RIGHT_BUTTON_RES, R.drawable.popup_ok_btn );
									args.putBoolean( Constants.CUSTOM_DIALOG_ALLOW_CANCEL, true );
									Util.showDialogFragment( getFragmentManager(), new AlertDialog(), Constants.TAG_DIALOG_CUSTOM, args );
								}
								*/
							}
						}
					}
				}
			} );
		} else
			super.onBackPressed();
	}

	/**
	 * Gets the current registration ID for application on GCM service.
	 * <p>
	 * If result is empty, the app needs to register.
	 * 
	 * @return registration ID, or empty string if there is no existing
	 *         registration ID.
	 */
	private String getRegistrationId(Context context) {
		String registrationId = HeightChartPreference.getString( Constants.PROPERTY_REG_ID, "" );
		if ( registrationId.isEmpty() ) {
			DLog.i( TAG, "Registration not found." );
			return "";
		}
		// Check if app was updated; if so, it must clear the registration ID
		// since the existing regID is not guaranteed to work with the new
		// app version.
		int registeredVersion = HeightChartPreference.getInt( Constants.PROPERTY_APP_VERSION, Integer.MIN_VALUE );
		int currentVersion = getAppVersion( context );
		if ( registeredVersion != currentVersion ) {
			DLog.i( TAG, "App version changed." );
			return "";
		}

		Log.d( TAG, "Zaharang, getRegi :" + registrationId );
		return registrationId;
	}

	/**
	 * @return Application's version code from the {@code PackageManager}.
	 */
	private static int getAppVersion(Context context) {
		try {
			PackageInfo packageInfo = context.getPackageManager().getPackageInfo( context.getPackageName(), 0 );
			return packageInfo.versionCode;
		} catch ( NameNotFoundException e ) {
			// should never happen
			throw new RuntimeException( "Could not get package name: " + e );
		}
	}

	/**
	 * Registers the application with GCM servers asynchronously.
	 * <p>
	 * Stores the registration ID and app versionCode in the application's shared preferences.
	 */
	private void registerInBackground() {
		new AsyncTask<Void, Object, String>() {
			@Override
			protected String doInBackground(Void... params) {
				String msg = "";
				try {
					if ( mGcm == null ) {
						mGcm = GoogleCloudMessaging.getInstance( MainActivity.this );
					}
					regid = mGcm.register( SENDER_ID );
					msg = "Device registered, registration ID=" + regid;

					// You should send the registration ID to your server over HTTP,
					// so it can use GCM/HTTP or CCS to send messages to your app.
					// The request to your server should be authenticated if your app
					// is using accounts.
					sendRegistrationIdToBackend();

					// For this demo: we don't need to send it because the device
					// will send upstream messages to a server that echo back the
					// message using the 'from' address in the message.

					// Persist the regID - no need to register again.
					HeightChartPreference.putString( Constants.PROPERTY_REG_ID, regid );
					HeightChartPreference.putInt( Constants.PROPERTY_APP_VERSION, getAppVersion( MainActivity.this ) );
				} catch ( IOException ex ) {
					msg = "Error :" + ex.getMessage();
				}
				return msg;
			}

			@Override
			protected void onPostExecute(String msg) {
				DLog.d( TAG, "Zaharang , registertration id : " + msg );

				// mDisplay.append(msg + "\n");
			}
		}.execute( null, null, null );
	}

	/**
	 * Sends the registration ID to your server over HTTP, so it can use GCM/HTTP
	 * or CCS to send messages to your app. Not needed for this demo since the
	 * device sends upstream messages to a server that echoes back the message
	 * using the 'from' address in the message.
	 */
	private void sendRegistrationIdToBackend() {
		// TODO Your implementation here.
	}

	private boolean checkPlayServices() {
		int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable( this );
		if ( resultCode != ConnectionResult.SUCCESS ) {
			if ( GooglePlayServicesUtil.isUserRecoverableError( resultCode ) ) {
				GooglePlayServicesUtil.getErrorDialog( resultCode, this, PLAY_SERVICES_RESOLUTION_REQUEST ).show();
			} else {
				DLog.i( TAG, "This device is not supported." );
				finish();
			}
			return false;
		}
		return true;
	}

}
