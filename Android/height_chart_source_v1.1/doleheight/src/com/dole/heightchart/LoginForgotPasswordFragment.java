package com.dole.heightchart;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.InputMethodManager;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.dole.heightchart.server.FindPassword;
import com.dole.heightchart.server.ServerTask;
import com.dole.heightchart.server.ServerTask.IServerResponseCallback;
import com.dole.heightchart.ui.CustomDialog;

import java.util.HashMap;
import java.util.Map;

public class LoginForgotPasswordFragment extends Fragment implements IServerResponseCallback {

	private FindPassword mFindPassword;
	private String mEmailId;
	private ProgressDialog mProgressDialog;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.login_forgot_your_password, null);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {

		super.onViewCreated(view, savedInstanceState);
		
		final View sendBtn = view.findViewById(R.id.forgot_password_send_btn);
		final TextView email = (TextView)view.findViewById(R.id.forgot_password_edit_email);
		
		String userId = HeightChartPreference.getString(Constants.PREF_USER_ID, "");
		if(!userId.equals("")) {
			email.setText(userId);
		}
		
		sendBtn.setEnabled(false);
		sendBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Util.hideSoftInput(getActivity(), v);
			    
				mEmailId = email.getText().toString();
				HashMap<String, String> request = new HashMap<String, String>();
				request.put(Constants.TAG_USER_ID, mEmailId);
				request.put(Constants.TAG_EMAIL, mEmailId);
				request.put(Constants.TAG_CLIENT_IP, Util.getIp(getActivity()));
				
				mFindPassword = new FindPassword(getActivity(), request);
				mFindPassword.setCallback(LoginForgotPasswordFragment.this);
				Model.runOnWorkerThread(mFindPassword);

				mProgressDialog = Util.openProgressDialog(getActivity());
			}
		});

		email.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				String st = s.toString();
				if(!Util.checkEmail(st))
					sendBtn.setEnabled(false);
				else
					sendBtn.setEnabled(true);
			}
		});
		
		getView().setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent e) {
				Util.hideSoftInput(getActivity(), v);
			    email.clearFocus();
				return false;
			}
		});
	}

	@Override
	public void onSuccess(ServerTask parser, Map<String, String> result) {
		Bundle args = new Bundle();
		args.putInt(Constants.CUSTOM_DIALOG_IMG_RES, R.drawable.popup_warning);
		args.putInt(Constants.CUSTOM_DIALOG_TEXT_RES, R.string.sent_temp_password);
		args.putInt(Constants.CUSTOM_DIALOG_RIGHT_BUTTON_RES, R.drawable.popup_ok_btn);
		
		Util.showDialogFragment(
				getFragmentManager(), 
				new CustomDialog(), 
				Constants.TAG_DIALOG_CUSTOM, 
				args, 
				LoginForgotPasswordFragment.this, 
				Constants.REQUEST_USER_CONFIRM);
		mProgressDialog.dismiss();
	}

	@Override
	public void onFailed(ServerTask parser, Map<String, String> result, int returnCode) {
		Toast.makeText(getActivity(), Util.switchErrorCode(returnCode, getResources()), Toast.LENGTH_SHORT).show();
		mProgressDialog.dismiss();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		switch (requestCode) {
		case Constants.REQUEST_USER_CONFIRM:
			if(resultCode == Activity.RESULT_OK) {
				final Fragment target = getTargetFragment();
				if(target != null) {
					Intent retData = new Intent();
					retData.putExtra(Constants.EMAIL, mEmailId);
					target.onActivityResult(Constants.REQUEST_FORGOTTEN_PASS_EMAIL, resultCode, retData);
				}
				getFragmentManager().popBackStack();
			}
			break;

		default:
			break;
		}
	}
}
