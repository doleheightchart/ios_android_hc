package com.dole.heightchart;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.view.View;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class AnimUtil {
	
	static Map<Animator, View> sAnimators = new HashMap<Animator, View>();
	
	public static void startAnimators(int duration) {
		Set<Animator> keys = sAnimators.keySet();
		for(Animator anim : keys) {
			anim.setDuration(duration);
			anim.start();
		}
	}
	
	public static Animator getTranslateYByAnimation(View view, float to) {
		final Animator anim = ObjectAnimator.ofFloat(view, "translationYBy", 100f);
		sAnimators.put(anim, view);
		return anim;
	}

	public static Animator getFadeOutAnimation(View view) {
		final Animator anim = ObjectAnimator.ofFloat(view, "alpha", 1f, 0f);
		anim.addListener(new FadeOutListener());
		sAnimators.put(anim, view);
		
		return anim;
	}
	
	public static Animator getFadeInAnimation(View view) {
		final Animator anim = ObjectAnimator.ofFloat(view, "alpha", 0f, 1f);
		anim.addListener(new FadeInListener());
		sAnimators.put(anim, view);
		
		return anim;
	}
	
	static class DefaultAnimatorListener implements Animator.AnimatorListener {
		@Override
		public void onAnimationStart(Animator animation) {
			
		}
		
		@Override
		public void onAnimationEnd(Animator animation) {
			sAnimators.remove(animation);
		}

		@Override
		public void onAnimationRepeat(Animator animation) {
			
		}
		
		@Override
		public void onAnimationCancel(Animator animation) {
			sAnimators.remove(animation);
		}

	}
    
    private static class FadeOutListener extends DefaultAnimatorListener {
		
		@Override
		public void onAnimationEnd(Animator animation) {
			hideView(animation);
			super.onAnimationEnd(animation);
		}
	};
	
	private static class FadeInListener extends DefaultAnimatorListener {
		
		@Override
		public void onAnimationEnd(Animator animation) {
			showView(animation);
			super.onAnimationEnd(animation);
		}
	};
	
	private static void hideView(Animator animation) {
    	Object object = sAnimators.get(animation);
		if(object instanceof View) {
			View view = (View) object;
			view.setVisibility(View.INVISIBLE);
		}
    }
	
	private static void showView(Animator animation) {
    	Object object = sAnimators.get(animation);
		if(object instanceof View) {
			View view = (View) object;
			view.setVisibility(View.VISIBLE);
		}
    }
}
