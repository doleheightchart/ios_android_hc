package com.dole.heightchart;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.widget.Button;

import com.dole.heightchart.GuideDialog.GuideMode;
import com.dole.heightchart.ui.ImportImageView;

import java.io.FileNotFoundException;

public class ImportPhotoFragment extends Fragment {
	
	private static final String TAG = "ImportPhotoFragment";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.import_photo, null);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		Bundle args = getArguments();
		if(args == null)
			return;

		final ImportImageView importView = (ImportImageView) view.findViewById(R.id.import_photo_zoomview);
		final String uriStr = args.getString(Constants.URI);
		final Uri imageUri = Uri.parse(uriStr);
		Bitmap image;
		try {
			image = decodeUri(imageUri);
			importView.setImageBitmap(image);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		View cancelBtn = view.findViewById(R.id.import_btn_cancel);
		cancelBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				getFragmentManager().popBackStack();
			}
		});
		
		final Button doneBtn = (Button) view.findViewById(R.id.import_photo_done_btn);
		doneBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Bitmap bitmap = importView.getCurrentImageBitmap();
				//TODO Send to retake
				final Model model = ((ApplicationImpl)getActivity().getApplication()).getModel();
				model.setImportedImage(bitmap);
				
				final Fragment target = getTargetFragment();
				if(target != null) {
					Intent i = new Intent();
					i.putExtra(Constants.IS_IMPORTED, true);
					target.onActivityResult(Constants.REQUEST_IMPORT_IMAGE, Activity.RESULT_OK, i);
					getFragmentManager().popBackStack();
				}
			}
		});
		
		// Guide dialog
		final GuideMode mode = GuideMode.GUIDE_PHOTO_EDIT;
		boolean isGuideShown = HeightChartPreference.getBoolean(mode.name(), false);
		if(!isGuideShown) {
			final View v = getView();
			v.getViewTreeObserver().addOnPreDrawListener(new OnPreDrawListener() {

				@Override
				public boolean onPreDraw() {
					final Bundle args = new Bundle();
					args.putString(GuideDialog.MODE_GUIDE, mode.name());
					Util.showDialogFragment(getFragmentManager(), new GuideDialog(), Constants.TAG_DIALOG_GUIDE, args);
					v.getViewTreeObserver().removeOnPreDrawListener(this);
					return true;
				}
			});
		}
	}
	
	private Bitmap decodeUri(Uri selectedImage) throws FileNotFoundException {
		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inJustDecodeBounds = true;
		BitmapFactory.decodeStream(getActivity().getContentResolver().openInputStream(selectedImage), null, o);
		final int REQUIRED_SIZE = 140;
		int width_tmp = o.outWidth, height_tmp = o.outHeight;
		int scale = 1;
		while (true) {
			if (width_tmp / 2 < REQUIRED_SIZE
					|| height_tmp / 2 < REQUIRED_SIZE) {
				break;
			}
			width_tmp /= 2;
			height_tmp /= 2;
			scale *= 2;
		}
		BitmapFactory.Options o2 = new BitmapFactory.Options();
		o2.inSampleSize = scale;
		return BitmapFactory.decodeStream(getActivity().getContentResolver().openInputStream(selectedImage), null, o2);
	}

}
