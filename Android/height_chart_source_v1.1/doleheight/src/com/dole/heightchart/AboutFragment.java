package com.dole.heightchart;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

public class AboutFragment extends Fragment {
	
	boolean mIsInitialSelection = true;	

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View view =  inflater.inflate(R.layout.about_main, null);
		
		return view;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		TextView version = (TextView)view.findViewById(R.id.about_version);
		View service = view.findViewById(R.id.about_service_terms);
		Context context = getActivity();
		try {
			PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
			version.setText(context.getResources().getString(R.string.about_version, pInfo.versionName));
		} catch (Exception e) {
			e.printStackTrace();
		}
		service.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				final Bundle args = new Bundle();
				args.putBoolean(AgreePersonalInfoFragment.HIDE_CHECKBOX, true);
				Util.replaceFragment(getFragmentManager(), new AgreePersonalInfoFragment(),
						R.id.container, Constants.TAG_FRAGMENT_AGREE_PERSONAL_INFO, args, true);
			}
		});
	}
}
