package com.dole.heightchart;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;

import com.dole.heightchart.Constants.CouponType;
import com.dole.heightchart.camera.Storage;

import java.util.Date;

public class HeightInfo {

	long mHeightId;
	long mNickId;
	float mHeight;
	Date mInputDate;
	private String mImagePath;
	String mQrCode;
	CouponType mCouponType;
	
	public enum FaceImageMode {
		LEFT, LEFT_NO_ARROW,
		RIGHT, RIGHT_NO_ARROW,
		CHART;
	}
	
	public static HeightInfo loadFromCursor(NickNameInfo nickInfo, Cursor c) {
		final HeightInfo info = new HeightInfo();

		info.mHeightId =  c.getLong(c.getColumnIndex(TableColumns.Height._ID));
		info.mNickId = c.getLong(c.getColumnIndex(TableColumns.Height.NICK_ID));
		info.mHeight = c.getFloat(c.getColumnIndex(TableColumns.Height.HEIGHT));
		final long milSec = c.getLong(c.getColumnIndex(TableColumns.Height.INPUT_DATE));
		info.mInputDate = new Date(milSec);
		info.mImagePath = c.getString(c.getColumnIndex(TableColumns.Height.IMAGE_PATH));
		info.mQrCode = c.getString(c.getColumnIndex(TableColumns.Height.QRCODE));
		
		String couponTypeStr = c.getString(c.getColumnIndex(TableColumns.Height.COUPON_TYPE));
		if(couponTypeStr != null)
			info.mCouponType = CouponType.getType(couponTypeStr);
		
		return info;
	}
	
	public void onAddToContentValue(ContentValues values) {
		values.put(TableColumns.Height.NICK_ID, getNickId());
		values.put(TableColumns.Height.HEIGHT, getHeight());
		values.put(TableColumns.Height.INPUT_DATE, getInputDate().getTime());
		values.put(TableColumns.Height.IMAGE_PATH, getImagePath());
		values.put(TableColumns.Height.COUPON_TYPE, (getCouponType() == null)?null:getCouponType().type);
		values.put(TableColumns.Height.QRCODE, getQrCode());
	}
	
	public long getHeightId() {
		return mHeightId;
	}

	public void setHeightId(long heightId) {
		this.mHeightId = heightId;
	}

	public long getNickId() {
		return mNickId;
	}

	public void setNickId(long nickId) {
		this.mNickId = nickId;
	}

	public float getHeight() {
		return mHeight;
	}

	public void setHeight(float height) {
		this.mHeight = height;
	}

	public Date getInputDate() {
		return mInputDate;
	}

	public void setInputDate(Date inputDate) {
		this.mInputDate = inputDate;
	}
	
	public String getQrCode() {
		return mQrCode;
	}
	
	public void setQrCode(String qrCode) {
		mQrCode = qrCode;
	}
	
	public void setCouponType(CouponType type) {
		mCouponType = type;
	}
	
	public void setCouponType(String typeStr) {
		if(typeStr != null)
			mCouponType = CouponType.getType(typeStr);
	}
	
	public CouponType getCouponType() {
		return mCouponType;
	}

	public Bitmap getFaceImage(Context context, ImageCache cache, FaceImageMode direction) {
		if(cache == null)
			return null;
		
		String postFix = null;
		switch (direction) {
		case LEFT:
			postFix = Storage.POSTFIX_LEFT;
			break;
		case RIGHT:
			postFix = Storage.POSTFIX_RIGHT;
			break;
		case LEFT_NO_ARROW:
			postFix = Storage.POSTFIX_LEFT_NO_ARROW;
			break;
		case RIGHT_NO_ARROW:
			postFix = Storage.POSTFIX_RIGHT_NO_ARROW;
			break;
		case CHART:
			postFix = Storage.POSTFIX_CHART;
			break;
		default:
			postFix = null;
			break;
		}
		
		final String path = context.getFilesDir() + "/" + mImagePath + ((postFix != null)?postFix:"") + Storage.IMAGE_FACE_EXTENSION;
		return cache.getImage(path);
	}

	public String getImagePath() {
		return mImagePath;
	}
	
	public String getImageFullPath() {
		return Storage.IMAGE_DIRECTORY + "/" + mImagePath;
	}

	public void setImagePath(String imagePath) {
		this.mImagePath = imagePath;
	}

}
