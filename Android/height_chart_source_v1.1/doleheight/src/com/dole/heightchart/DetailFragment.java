package com.dole.heightchart;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationSet;
import android.view.animation.Interpolator;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.dole.heightchart.Constants.Characters;
import com.dole.heightchart.GuideDialog.GuideMode;
import com.dole.heightchart.HeightInfo.FaceImageMode;
import com.dole.heightchart.ui.BgTransitionHelper;
import com.dole.heightchart.ui.BgTransitionHelper.OnTransitionListener;
import com.dole.heightchart.ui.CloudBg;
import com.dole.heightchart.ui.CustomDialog;
import com.dole.heightchart.ui.DetailIconView;
import com.dole.heightchart.ui.FullHeightTree;
import com.dole.heightchart.ui.ZoomHeightTree;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class DetailFragment extends Fragment implements OnTransitionListener {
	
	private static final String TAG = DetailFragment.class.getName();
	
	public static final int ZOOM_MODE_CHANGE_INTERVAL = 500;
	private NickNameInfo mNickNameInfo;
	private boolean mIsIndelete = false;
	private boolean mIsInZoom = false;
	private boolean mIsAnimating = false;
	
	private List<HeightInfo> mDeleteList;
	private float mAvgHeight;
	private int mAge;
	
	private BgTransitionHelper mTransitionHelper;
	
	public interface ModeChangeListener {
		public void changeMode(int height);
		public void onAnimationEnd();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View view =  inflater.inflate(R.layout.detail_main, null);
		mDeleteList = new ArrayList<HeightInfo>();
		return view;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		Bundle args = getArguments();
		if(args == null) {
			return;
		}
		
		final FragmentManager fm = getFragmentManager();
		
		final long nickNameId = args.getLong(Constants.NICKNAME_ID);
		final Model model = ((ApplicationImpl)getActivity().getApplication()).getModel();
		mNickNameInfo = model.getNickNameInfoById(nickNameId);
		
		View nightBg = getView().findViewById(R.id.detail_night_bg);
		View dayBg = getView().findViewById(R.id.detail_day_bg);
		
		mTransitionHelper = new BgTransitionHelper(dayBg, nightBg, DetailFragment.this);
		
		final ImageButton btnDelete = (ImageButton) view.findViewById(R.id.detail_btn_delete);
		final ImageButton btnFullView = (ImageButton) view.findViewById(R.id.detail_btn_full_view);
		final ImageButton btnGraph = (ImageButton) view.findViewById(R.id.detail_btn_graph);
		final ImageButton btnDeleteReturn = (ImageButton) view.findViewById(R.id.detail_btn_return);
		final ImageButton btnConfirmDelete = (ImageButton) view.findViewById(R.id.detail_btn_confirm_delete);
		
		final FullHeightTree fullTree = (FullHeightTree) view.findViewById(R.id.detail_height_tree_list_view);
		final ZoomHeightTree zoomTree = (ZoomHeightTree) view.findViewById(R.id.detail_height_tree_scroll_view);
		final View addBtn = view.findViewById(R.id.detail_tree_zoom_add);
		zoomTree.init(mNickNameInfo, mModeChangeListener);
		fullTree.setModeChangeListener(mModeChangeListener);
		addBtn.setOnClickListener(mAddHeightClickListener);

		final View fullContainer = view.findViewById(R.id.detail_height_tree_full_container);
		fullContainer.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
				final Bundle args = new Bundle();
				args.putLong(Constants.NICKNAME_ID, nickNameId);
				Util.replaceFragment(fm, new AddNicknameFragment(), 
						R.id.container, Constants.TAG_FRAGMENT_ADD_NICK, args, 
						DetailFragment.this, Constants.REQUEST_UPDATE_NICK_INFO, true);
			}
		});
		
		TextView fullName = (TextView)view.findViewById(R.id.detail_height_tree_full_name);
		TextView zoomName = (TextView)view.findViewById(R.id.detail_height_tree_zoom_name);
		ImageView fullMonkey = (ImageView)view.findViewById(R.id.detail_height_tree_full_monkey);
		ImageView zoomMonkey = (ImageView)view.findViewById(R.id.detail_height_tree_zoom_monkey);
		fullName.setText(mNickNameInfo.mNickName);
		fullName.setTextColor(getResources().getColor(mNickNameInfo.mCharacter.getTagNameColor()));
		zoomName.setText(mNickNameInfo.mNickName);
		zoomName.setTextColor(getResources().getColor(mNickNameInfo.mCharacter.getTagNameColor()));
		fullMonkey.setImageResource(Util.getCharacterDrawable(mNickNameInfo.mCharacter, "detail_full"));
		zoomMonkey.setImageResource(Util.getCharacterDrawable(mNickNameInfo.mCharacter, "detail_zoom"));
		
		// Click Listeners
		btnDelete.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mTransitionHelper.start();
			}
		});
		
		btnFullView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				toggleMode();
			}
		});
		
		final int userNo = HeightChartPreference.getInt(Constants.PREF_USER_NO, 0);
		btnGraph.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(userNo > 0) {
					final Bundle args = new Bundle();
					args.putLong(Constants.NICKNAME_ID, mNickNameInfo.mNickNameId);
					//Util.replaceFragment(getFragmentManager(), new ChartFragment(), R.id.container, Constants.TAG_FRAGMENT_CHART, args, true);
					Util.startChartActivity(getActivity(), args);
				} else {
					Bundle args = new Bundle();
					args.putInt(Constants.CUSTOM_DIALOG_IMG_RES, R.drawable.popup_warning);
					args.putInt(Constants.CUSTOM_DIALOG_TEXT_RES, R.string.request_log_in);
					args.putInt(Constants.CUSTOM_DIALOG_RIGHT_BUTTON_TEXT_RES, R.string.login);
					args.putInt(Constants.CUSTOM_DIALOG_LEFT_BUTTON_RES, R.drawable.popup_cancel_btn);
					args.putInt(Constants.CUSTOM_DIALOG_RIGHT_BUTTON_RES, R.drawable.popup_ok_btn);
					Util.showDialogFragment(
							getFragmentManager(), 
							new CustomDialog(), 
							Constants.TAG_DIALOG_CUSTOM, 
							args, 
							DetailFragment.this, 
							Constants.REQUEST_USER_CONFIRM);
				}
			}
		});
		
		btnDeleteReturn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				mTransitionHelper.start();
			}
		});
		
		btnConfirmDelete.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Bundle args = new Bundle();
				args.putInt(Constants.CUSTOM_DIALOG_IMG_RES, R.drawable.popup_warning);
				args.putInt(Constants.CUSTOM_DIALOG_TEXT_RES, R.string.selected_record_deletion);
				args.putInt(Constants.CUSTOM_DIALOG_LEFT_BUTTON_RES, R.drawable.popup_cancel_btn);
				args.putInt(Constants.CUSTOM_DIALOG_RIGHT_BUTTON_RES, R.drawable.popup_ok_btn);
				
				Util.showDialogFragment(
						getFragmentManager(), 
						new CustomDialog(), 
						Constants.TAG_DIALOG_CUSTOM, 
						args, 
						DetailFragment.this, 
						Constants.REQUEST_DELETE_USER_INFO);
				
			}
		});
		
		int max_height = mNickNameInfo.getLastHeight() >  15 ? (int)mNickNameInfo.getLastHeight() : 15;
		HeightTreeAdapter adapter = new HeightTreeAdapter(createHeightTreeItems(mNickNameInfo), mNickNameInfo.mCharacter, 1, max_height);
		fullTree.setAdapter(adapter);
		
		// Guide dialog
		final GuideMode mode = GuideMode.GUIDE_DETAIL;
		boolean isGuideShown = HeightChartPreference.getBoolean(mode.name(), false);
		if(!isGuideShown) {
			final View v = getView();
			v.getViewTreeObserver().addOnPreDrawListener(new OnPreDrawListener() {

				@Override
				public boolean onPreDraw() {
					final int[] pos = new int[2];
					final ListView list = (ListView) v.findViewById(R.id.detail_height_tree_list_view);
					final ViewGroup item = (ViewGroup)list.getChildAt(0);
					if(item != null) {
						final View addButton = item.findViewById(R.id.detail_height_tree_right_image);
						addButton.getLocationOnScreen(pos);

						final Bundle args = new Bundle();
						args.putString(GuideDialog.MODE_GUIDE, mode.name());
						args.putInt(GuideDialog.ITEM_COUNT, 1);
						args.putIntArray(GuideDialog.ITEM_IDS, new int[]{R.id.guide_image});
						args.putIntArray(GuideDialog.ITEM_LEFTS, new int[]{pos[0]});
						args.putIntArray(GuideDialog.ITEM_TOPS, new int[]{pos[1]});

						Util.showDialogFragment(getFragmentManager(), new GuideDialog(), Constants.TAG_DIALOG_GUIDE, args);
					}
					v.getViewTreeObserver().removeOnPreDrawListener(this);
					return true;
				}
			});
		}
		setDoleTag();
		
		if(mNickNameInfo != null) {
			boolean isByPassDetail = args.getBoolean(Constants.BYPASS_DETAIL_FRAG, false);
			if(isByPassDetail) {
				args.remove(Constants.BYPASS_DETAIL_FRAG);
				DLog.d(TAG, " by passs!!!!!!!!!!!!!!!!");
				final Bundle byPassArgs = new Bundle();
				byPassArgs.putLong(Constants.NICKNAME_ID, mNickNameInfo.mNickNameId);
				Util.replaceFragment(getFragmentManager(), new QRCodeFragment(), R.id.container, Constants.TAG_FRAGMENT_QRCODE, byPassArgs, true);
			}
		}
	}

	private ModeChangeListener mModeChangeListener = new ModeChangeListener() {
		@Override
		public void changeMode(int height) {
			if(mIsInZoom) {
				toggleMode();
				FullHeightTree fullTree = (FullHeightTree)getView().findViewById(R.id.detail_height_tree_list_view);
				HeightTreeAdapter adapter = (HeightTreeAdapter)fullTree.getAdapter();
				int maxHeight = (Integer)adapter.getItem(1);
				if(maxHeight-height >= 0 && maxHeight-height < adapter.getCount()-1) {
					fullTree.setSelection(maxHeight-height);
				}
			}
		}
		
		@Override
		public void onAnimationEnd() {
			mIsAnimating = false;
		}
	};
	
	public void toggleMode() {
		if(mIsAnimating)
			return;
		mIsAnimating = true;
		
		final FullHeightTree fullTree = (FullHeightTree) getView().findViewById(R.id.detail_height_tree_list_view);
		final ZoomHeightTree zoomTree = (ZoomHeightTree) getView().findViewById(R.id.detail_height_tree_scroll_view);
		final ImageButton btnFullView = (ImageButton) getView().findViewById(R.id.detail_btn_full_view);
		if(mIsInZoom) {
			btnFullView.setImageResource(R.drawable.detail_btn_full_view);
			transitionViewMode();
			zoomTree.changeZoomMode();
			mIsInZoom = false;
		}
		else {
			btnFullView.setImageResource(R.drawable.detail_btn_zoom);
			transitionViewMode();
			fullTree.changeZoomMode();
			mIsInZoom = true;
		}
	}
	
	@Override
	public void onTransitionStarted(boolean isReverse) {
		mIsIndelete = isReverse;
		toggleDeleteState(isReverse);

		final FullHeightTree fullTree = (FullHeightTree) getView().findViewById(R.id.detail_height_tree_list_view);
		HeightTreeAdapter adapter = (HeightTreeAdapter) fullTree.getAdapter();
		adapter.clearCheck();
	}

	@Override
	public void onTransitionFinished(boolean isReverse) {
		mIsIndelete = isReverse;
		
		if(getView() == null)
			return;
	}

	private void toggleDeleteState(boolean isInDelete) {
		final View view = getView();
		final ImageButton btnDelete = (ImageButton) view.findViewById(R.id.detail_btn_delete);
		final ImageButton btnFullView = (ImageButton) view.findViewById(R.id.detail_btn_full_view);
		final ImageButton btnGraph = (ImageButton) view.findViewById(R.id.detail_btn_graph);
		final View btnCancel = getView().findViewById(R.id.common_btn_cancel);
		
		final ImageButton btnDeleteReturn = (ImageButton) view.findViewById(R.id.detail_btn_return);
		final ImageButton btnConfirmDelete = (ImageButton) view.findViewById(R.id.detail_btn_confirm_delete);
		final CloudBg cloud = (CloudBg)view.findViewById(R.id.detail_cloud_container);

		if(isInDelete) {
			AnimUtil.getFadeOutAnimation(btnDelete);
			AnimUtil.getFadeOutAnimation(btnFullView);
			AnimUtil.getFadeOutAnimation(btnGraph);
			AnimUtil.getFadeOutAnimation(btnCancel);
			
			AnimUtil.getFadeInAnimation(btnDeleteReturn);
			AnimUtil.getFadeInAnimation(btnConfirmDelete);
		} else {
			AnimUtil.getFadeInAnimation(btnDelete);
			AnimUtil.getFadeInAnimation(btnFullView);
			AnimUtil.getFadeInAnimation(btnGraph);
			AnimUtil.getFadeInAnimation(btnCancel);
			
			AnimUtil.getFadeOutAnimation(btnDeleteReturn);
			AnimUtil.getFadeOutAnimation(btnConfirmDelete);
		}
		
		AnimUtil.startAnimators(300);

		if(!isInDelete) {
			cloud.setNormalMode();
			mIsIndelete = false;
		}
		else {
			cloud.setDeleteMode();
			mIsIndelete = true;
		}

		btnConfirmDelete.setEnabled(false);
		if(mIsIndelete)
			mDeleteList.clear();
	}
	
	private List<HeightTreeItem> createHeightTreeItems(NickNameInfo nickNameInfo) {
		List<HeightInfo> heights = nickNameInfo.getHeightList();
		Collections.sort(heights, HeightComparator);
		Collections.reverse(heights);
		final List<HeightTreeItem> items = new ArrayList<HeightTreeItem>();
		final int len = heights.size();
		
		int lastHeight = -1;
		boolean pos = false;
		for(int i = 0; i < len; i++) {
			HeightInfo cur = heights.get(i);
			HeightTreeItem item = new HeightTreeItem();
			item.heightInfo = cur;
			if(lastHeight != (int)cur.mHeight) {
				pos = !pos;
				lastHeight = (int)cur.mHeight;
				item.isLeft = pos;
			}
			else {
				item.isLeft = pos;
			}
			items.add(item);
		}
		
		return items;
	}
	
	private void transitionViewMode() {
		View rootView = getView();
		final Resources res = getResources();

		final View fullContainer = rootView.findViewById(R.id.detail_height_tree_full_container);
		final View zoomContainer = rootView.findViewById(R.id.detail_height_tree_zoom_container);

		final View srcView, trgView;
		ScaleAnimation scaleAnimation;
		TranslateAnimation translateAnimation;
		AlphaAnimation alphaAnimation;
		float ratio;
		final ImageButton btnDelete = (ImageButton) rootView.findViewById(R.id.detail_btn_delete);
		final int isZoomVisible;
		if(mIsInZoom) {
			ratio = (float)res.getDimensionPixelSize(R.dimen.detail_tree_full_leaf_height)/
					(float)res.getDimensionPixelSize(R.dimen.detail_tree_zoom_leaf_height);
			scaleAnimation = createScaleAnimation(ratio);
			translateAnimation = createTranslateAnimation(res.getDimensionPixelSize(R.dimen.detail_tree_full_container_margin_top), 
					res.getDimensionPixelSize(R.dimen.detail_tree_zoom_container_margin_top), ratio);
			srcView = zoomContainer;
			trgView = fullContainer;
			alphaAnimation = new AlphaAnimation(0f, 1f);
			isZoomVisible = View.VISIBLE;
		}
		else {
			ratio = (float)res.getDimensionPixelSize(R.dimen.detail_tree_zoom_leaf_height)/
					(float)res.getDimensionPixelSize(R.dimen.detail_tree_full_leaf_height);
			scaleAnimation = createScaleAnimation(ratio);
			translateAnimation = createTranslateAnimation(res.getDimensionPixelSize(R.dimen.detail_tree_zoom_container_margin_top), 
					res.getDimensionPixelSize(R.dimen.detail_tree_full_container_margin_top), ratio);
			srcView = fullContainer;
			trgView = zoomContainer;
			alphaAnimation = new AlphaAnimation(1f, 0f);
			isZoomVisible = View.GONE;
		}
		AnimationSet animationSet = new AnimationSet(false);
		animationSet.addAnimation(scaleAnimation);
		animationSet.addAnimation(translateAnimation);
		srcView.startAnimation(animationSet);
		alphaAnimation.setDuration(ZOOM_MODE_CHANGE_INTERVAL);
		btnDelete.startAnimation(alphaAnimation);
		
		animationSet.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
			}
			
			@Override
			public void onAnimationRepeat(Animation animation) {
			}
			
			@Override
			public void onAnimationEnd(Animation animation) {
				srcView.setVisibility(View.GONE);
				trgView.setVisibility(View.VISIBLE);
				btnDelete.setVisibility(isZoomVisible);
			}
		});
	}
	
	private ScaleAnimation createScaleAnimation(float ratio) {
		ScaleAnimation ani = new ScaleAnimation(1, ratio, 1, ratio, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0f);
		ani.setDuration(ZOOM_MODE_CHANGE_INTERVAL);
		ani.setRepeatCount(0);
		ani.setInterpolator(new Interpolator() {
			@Override
			public float getInterpolation(float input) {
				return input;
			}
		});
		return ani;
	}
	
	private TranslateAnimation createTranslateAnimation(float from, float to, float ratio) {
		TranslateAnimation ani = new TranslateAnimation(0, 0, 0, from-to);
		ani.setDuration(ZOOM_MODE_CHANGE_INTERVAL);
		ani.setRepeatCount(0);
		ani.setInterpolator(new Interpolator() {
			@Override
			public float getInterpolation(float input) {
				return input;
			}
		});
		return ani;
	}
	
	public void backPress() {
		getFragmentManager().popBackStack();
	}
	
	private void setDoleTag() {
		ImageView viewTag = (ImageView) getView().findViewById(R.id.detail_tag);
		ZoomHeightTree zoomTree = (ZoomHeightTree) getView().findViewById(R.id.detail_height_tree_scroll_view);
		
		mAge = Util.getAgeOfDate(mNickNameInfo.mBirthday, null);
		mAvgHeight = Util.getAverageHeightOfAge(getActivity(), mAge, mNickNameInfo.mGender);

		zoomTree.setAvgHeightNAge(mAvgHeight, mAge);
		if(mNickNameInfo.mLastHeight != 0 && mAvgHeight != -1 && mNickNameInfo.mLastHeight > mAvgHeight+5)
			viewTag.setImageResource(R.drawable.healthy_tag);
		else if(mNickNameInfo.mLastHeight != 0 && mAvgHeight != -1 && mNickNameInfo.mLastHeight < mAvgHeight-5)
			viewTag.setImageResource(R.drawable.cheer_up_tag);
		else
			viewTag.setVisibility(View.GONE);
	}
	
	public static class HeightTreeItem {
		public HeightInfo heightInfo;
		public boolean isLeft;
		public boolean isCheckedForDelete;
//		public boolean isLongClicked;
//		public boolean isExpandable;
	}
	
	public static final Comparator<HeightInfo> HeightComparator= new Comparator<HeightInfo>() {
		@Override
		public int compare(HeightInfo object1, HeightInfo object2) {
			float object1Height = object1.mHeight;
			float object2Height = object2.mHeight;
			if(object1Height < object2Height)
				return -1;
			else
				return 1;
		}
	};

	
	private class HeightTreeAdapter extends BaseAdapter {
		private int[] mData;
		List<HeightTreeItem> mHeights;
		SimpleDateFormat mDateFormat;
		Characters mCharacters;
		
		int addItemUpperHeight;
		int addItemHeight;
		int addItemLowerHeight;
		int itemImageWidth;
		int itemImageHeight;
		int itemBetweenHeight;
		int itemHeight;
		Drawable addItemCover;
		Drawable itemCover;
		
		public HeightTreeAdapter(List<HeightTreeItem> items, Characters character, int min, int max) {
			mDateFormat = new SimpleDateFormat("yyyy.MM.dd");
			mHeights = items;
			mCharacters = character;
			if(min >= max)
				return;
			int len = max - min + 1;
			mData = new int[max-min+1];
			for(int i = 0; i < len; i++) {
				mData[i] = min + i;
			}
			Resources res = getResources();
			
			addItemUpperHeight = res.getDimensionPixelSize(R.dimen.detail_tree_full_add_item_upper_height);
			addItemHeight = res.getDimensionPixelSize(R.dimen.detail_tree_full_add_item_height);
			addItemLowerHeight = res.getDimensionPixelSize(R.dimen.detail_tree_full_add_item_lower_height);
			addItemCover = res.getDrawable(R.drawable.detail_tree_cover_add);
			itemImageWidth = res.getDimensionPixelSize(R.dimen.detail_tree_full_item_image_width);
			itemImageHeight = res.getDimensionPixelSize(R.dimen.detail_tree_full_item_image_height);
			
			itemBetweenHeight = res.getDimensionPixelSize(R.dimen.detail_tree_full_item_between_height);
			itemHeight = res.getDimensionPixelSize(R.dimen.detail_tree_full_item_height);
			itemCover = res.getDrawable(R.drawable.detail_tree_cover);

		}
		
		public void clearCheck() {
			for(HeightTreeItem item : mHeights) {
				item.isCheckedForDelete = false;
			}
			notifyDataSetChanged();
		}
		
//		public void clearLongClick() {
//			for(HeightTreeItem item : mHeights) {
//				item.isLongClicked = false;
//			}
//			notifyDataSetChanged();
//		}
		
		public void removeHeightInfo(HeightInfo heightInfo) {
			if(heightInfo == null)
				return;
			List<HeightTreeItem> removeList = new ArrayList<HeightTreeItem>(); 
			for(HeightTreeItem item : mHeights) {
				if(item.heightInfo == heightInfo) {
					removeList.add(item);
				}
			}
			
			mHeights.removeAll(removeList);
		}
		
		@Override
		public boolean isEnabled(int position) {
			return false;
		}

		@Override
		public int getCount() {
			return mData.length+1;
		}

		@Override
		public Object getItem(int position) {
			if(position == 0)
				return -1;
			else
				return mData[mData.length-position];
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return 0;
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if(convertView == null) {
				convertView = getActivity().getLayoutInflater().inflate(R.layout.detail_height_tree_item, null);
			}
			View v = convertView;
			LinearLayout avgHeightContainer = (LinearLayout)v.findViewById(R.id.detail_height_tree_item_avg_height_container);
			TextView avgHeightText = (TextView)v.findViewById(R.id.detail_height_tree_item_avg_height_text);
			LinearLayout leftContainer = (LinearLayout)v.findViewById(R.id.detail_height_tree_left_item_container);
			LinearLayout rightContainer = (LinearLayout)v.findViewById(R.id.detail_height_tree_right_item_container);
			View upper = v.findViewById(R.id.detail_height_tree_item_upper_view);
			TextView cover = (TextView)v.findViewById(R.id.detail_height_tree_item_cover_view);
			View lower = v.findViewById(R.id.detail_height_tree_item_lower_view);
			LayoutParams upperParam = upper.getLayoutParams();
			LayoutParams coverParam = cover.getLayoutParams();
			LayoutParams lowerParam = lower.getLayoutParams();
			leftContainer.removeAllViews();
			rightContainer.removeAllViews();
			Resources res = getResources();
			
			if(position == 0) {
				upperParam.height = addItemUpperHeight;
				coverParam.height = addItemHeight;
				lowerParam.height = addItemLowerHeight;
				cover.setBackgroundDrawable(addItemCover);
				cover.setText("");
				
				ImageView addButton = new ImageView(getActivity());
				LayoutParams param = new LayoutParams(itemImageWidth, itemImageHeight);
				addButton.setLayoutParams(param);
				addButton.setScaleType(ImageView.ScaleType.FIT_XY);
				addButton.setImageResource(R.drawable.right_arrow_add);
				addButton.setId(R.id.detail_height_tree_right_image);
				addButton.setOnClickListener(mAddHeightClickListener);
				if(mIsIndelete)
					addButton.setVisibility(View.INVISIBLE);
				else
					addButton.setVisibility(View.VISIBLE);
				rightContainer.addView(addButton);
				avgHeightContainer.setVisibility(View.GONE);
			}
			else {
				upperParam.height = itemBetweenHeight;
				coverParam.height = itemHeight;
				cover.setBackgroundDrawable(itemCover);
				cover.setText(""+mData[mData.length-position]);

				int height = itemBetweenHeight;
				
				final ImageCache imageCache = ((ApplicationImpl)getActivity().getApplication()).getImageCache();

				if(mAvgHeight < 0 || Math.floor(mAvgHeight) != mData[mData.length-position])
					avgHeightContainer.setVisibility(View.GONE);
				else {
					avgHeightText.setText(res.getString(R.string.average_height_of_n, mAge));
//					RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)avgHeightContainer.getLayoutParams();
//					int viewHeight = itemBetweenHeight+itemHeight+height;
//					DoleLog.Loge(TAG, "mAvgHeight:"+mAvgHeight);
//					DoleLog.Loge(TAG, "mData[mData.length-position]:"+mData[mData.length-position]);
//					float ratio = 0.5f-(mAvgHeight-(float)mData[mData.length-position]);
//					DoleLog.Loge(TAG, "ratio:"+ratio);
//					DoleLog.Loge(TAG, "viewHeight:"+viewHeight);
//					DoleLog.Loge(TAG, "itemBetweenHeight:"+itemBetweenHeight);
//					DoleLog.Loge(TAG, "itemHeight:"+itemHeight);
//					DoleLog.Loge(TAG, "height:"+height);
//					params.topMargin = (int)(ratio*viewHeight);
//					DoleLog.Loge(TAG, "params.topMargin:"+params.topMargin);
					
					avgHeightContainer.setVisibility(View.VISIBLE);
				}
				
				if(mHeights != null) {
					int len = mHeights.size();
					View firstView = null;
					for(int i = 0; i < len; i++) {
						HeightTreeItem cur = mHeights.get(i);
						if(Math.floor(cur.heightInfo.mHeight) == mData[mData.length-position]) {
							View item;
							DetailIconView image;
							TextView textHeight;
							TextView textDate;

							if(cur.isLeft) {
								item = getActivity().getLayoutInflater().inflate(R.layout.detail_height_tree_item_left, null);
								image = (DetailIconView)item.findViewById(R.id.detail_height_tree_left_image);
								textHeight = (TextView)item.findViewById(R.id.detail_height_tree_left_height_text);
								textDate = (TextView)item.findViewById(R.id.detail_height_tree_left_date_text);
								if(firstView == null)
									image.setImageBitmap(cur.heightInfo.getFaceImage(getActivity(), imageCache, FaceImageMode.LEFT));
								else
									image.setImageBitmap(cur.heightInfo.getFaceImage(getActivity(), imageCache, FaceImageMode.LEFT_NO_ARROW));
								image.setCharacterByIsLeft(mNickNameInfo.getCharacter(), true);
								leftContainer.addView(item);
							}
							else {
								item = getActivity().getLayoutInflater().inflate(R.layout.detail_height_tree_item_right, null);
								image = (DetailIconView)item.findViewById(R.id.detail_height_tree_right_image);
								textHeight = (TextView)item.findViewById(R.id.detail_height_tree_right_height_text);
								textDate = (TextView)item.findViewById(R.id.detail_height_tree_right_date_text);
								if(firstView == null)
									image.setImageBitmap(cur.heightInfo.getFaceImage(getActivity(), imageCache, FaceImageMode.RIGHT));
								else
									image.setImageBitmap(cur.heightInfo.getFaceImage(getActivity(), imageCache, FaceImageMode.RIGHT_NO_ARROW));
								image.setCharacterByIsLeft(mNickNameInfo.getCharacter(), false);
								rightContainer.addView(item);
							}
							image.setCheckForDelete(cur.isCheckedForDelete);
							image.setDeleteMode(mIsIndelete);
							image.setTag(cur);
							textHeight.setText(cur.heightInfo.mHeight+"cm");
							textHeight.setTextColor(res.getColor(mCharacters.getTagNameColor()));
							textDate.setText(mDateFormat.format(cur.heightInfo.mInputDate));
							textDate.setTextColor(res.getColor(mCharacters.getTagNameColor()));
							image.setOnClickListener(mHeightClickListener);
							image.setOnLongClickListener(mHeightLongClickListener);
							if(firstView == null) {
								firstView = item;
								image.setHasArrow(true);
							}
							else {
								image.setHasArrow(false);
							}
							image.determineCharacterDrawable();
//							if(cur.isLongClicked) {
//								HeightTreeItem firstTag;
//								DetailIconView firstImage;
//								if(cur.isLeft) {
//									leftContainer.removeAllViews();
//									leftContainer.addView(firstView);
//									firstTag = (HeightTreeItem)firstView.findViewById(R.id.detail_height_tree_left_image).getTag();
//									firstImage = (DetailIconView)firstView.findViewById(R.id.detail_height_tree_left_image);
//								}
//								else {
//									rightContainer.removeAllViews();
//									rightContainer.addView(firstView);
//									firstTag = (HeightTreeItem)firstView.findViewById(R.id.detail_height_tree_right_image).getTag();
//									firstImage = (DetailIconView)firstView.findViewById(R.id.detail_height_tree_right_image);
//								}
//								if(firstTag.isExpandable) {
//									if(!cur.equals(firstTag)) {
//										firstTag.isLongClicked = true;
//										cur.isLongClicked = false;
//									}
//									firstImage.setLongClicked(true);
//									
//								height = itemBetweenHeight;
//								}
//								break;
//							}
							height += itemImageHeight;
						}
					}
//					if(firstView != null) {
//						if(leftContainer.getChildCount() > 1) {
//							HeightTreeItem firstTag = (HeightTreeItem)firstView.findViewById(R.id.detail_height_tree_left_image).getTag();
//							firstTag.isExpandable = true;
//						}
//						else if(rightContainer.getChildCount() > 1) {
//							HeightTreeItem firstTag = (HeightTreeItem)firstView.findViewById(R.id.detail_height_tree_right_image).getTag();
//							firstTag.isExpandable = true;
//						}
//					}
				}
				if(height > itemImageHeight)
					height -= itemImageHeight;
				lowerParam.height = height;
			}
			return v;
		}
		
	}
	
	private OnClickListener mAddHeightClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			final Bundle args = new Bundle();
			args.putLong(Constants.NICKNAME_ID, mNickNameInfo.mNickNameId);
			Util.replaceFragment(getFragmentManager(), new QRCodeFragment(), R.id.container, Constants.TAG_FRAGMENT_QRCODE, args, false);
			
			
		}
	};
	
	private OnClickListener mHeightClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			final Object tag = v.getTag();
			if(tag instanceof HeightTreeItem) {
				final HeightTreeItem heightTreeInfo = (HeightTreeItem) tag;
				final HeightInfo heightInfo = heightTreeInfo.heightInfo;
				View btnConfirmDelete = (ImageButton)getView().findViewById(R.id.detail_btn_confirm_delete);
				
				if(mIsIndelete) {
					DetailIconView icon = (DetailIconView)v;
					if(icon.isCheckedForDelete()) {
						heightTreeInfo.isCheckedForDelete = false;
						icon.setCheckForDelete(false);
						mDeleteList.remove(heightInfo);
					} else {
						heightTreeInfo.isCheckedForDelete = true;
						icon.setCheckForDelete(true);
						mDeleteList.add(heightInfo);
					}
					if(mDeleteList.size() > 0)
						btnConfirmDelete.setEnabled(true);
					else
						btnConfirmDelete.setEnabled(false);
				} else {
					Bundle args = new Bundle();
					args.putLong(Constants.HEIGHT_ID, heightInfo.mHeightId);
					Util.replaceFragment(getFragmentManager(), new HeightViewerFragment(), 
							R.id.container, Constants.TAG_FRAGMENT_HEIGHT_VIEWER, args, true);
				}
			}
		}
	};
	
	private OnLongClickListener mHeightLongClickListener = new OnLongClickListener() {
		@Override
		public boolean onLongClick(View v) {
			if(mIsIndelete)
				return true;
			
			mTransitionHelper.start();
			
			v.performClick();
			return true;
		}
	};

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		
		switch (requestCode) {
		case Constants.REQUEST_USER_CONFIRM:
			if(resultCode == Activity.RESULT_OK)
				Util.replaceFragment(
						getFragmentManager(), 
						new LoginFragment(), 
						R.id.container, 
						Constants.TAG_FRAGMENT_LOGIN, 
						null, 
						true);

			break;
		case Constants.REQUEST_UPDATE_NICK_INFO:
			//TODO ?
			break;
		case Constants.REQUEST_DELETE_USER_INFO:
			
			if(resultCode != Activity.RESULT_OK)
				return;

			Model.runOnWorkerThread(new Runnable() {
				public void run() {
					Model.deleteHeights(getActivity(), mNickNameInfo, mDeleteList);
					
					Toast.makeText(getActivity(), getResources().getString(R.string.deleted), Toast.LENGTH_LONG).show();
				}
			});
			
			if(mDeleteList != null && mDeleteList.size() > 0) {
				final CloudBg cloud = (CloudBg)getView().findViewById(R.id.detail_cloud_container);
				final View mainContainer = getView().findViewById(R.id.detail_main);
				final FullHeightTree fullTree = (FullHeightTree) getView().findViewById(R.id.detail_height_tree_list_view);
				final int cnt = fullTree.getChildCount();
				final int[] ids = new int[]{
						R.id.detail_height_tree_right_image, 
						R.id.detail_height_tree_left_image
				}; 
				for(int i = 0; i < cnt ; i++) {
					final View child = fullTree.getChildAt(i);
					if(child != null) {
						for(int j = 0; j < ids.length; j++) {
							View view = child.findViewById(ids[j]);
							if(view instanceof DetailIconView) {
								DetailIconView icon = (DetailIconView) view;
								if(icon != null) {
									Object tag = icon.getTag();
									if(tag instanceof HeightTreeItem) {
										HeightTreeItem item = (HeightTreeItem)tag;
										if(mDeleteList.contains(item.heightInfo)) {
											AnimUtil.getFadeOutAnimation(icon);
											continue;
										}
									}
								}
							}
						}
					}
				}
				AnimUtil.startAnimators(300);
				
				fullTree.postDelayed(new Runnable() {
					
					@Override
					public void run() {
						HeightTreeAdapter adapter = ((HeightTreeAdapter)fullTree.getAdapter());
						for(HeightInfo info :mDeleteList)
							adapter.removeHeightInfo(info);
						adapter.notifyDataSetChanged();
						setDoleTag();
						mDeleteList.clear();
					}
				}, 300);
				mTransitionHelper.start();
			}
			
			break;

		default:
			break;
		}
	}
	
	
}
