package com.dole.heightchart;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.widget.TextView;

import java.util.Hashtable;

public class FontCache {

    private static Hashtable<String, Typeface> fontCache = new Hashtable<String, Typeface>();

    public static Typeface get(Context context, String name) {
        Typeface tf = fontCache.get(name);
        if(tf == null) {
            try {
                tf = Typeface.createFromAsset(context.getAssets(), name);
            }
            catch (Exception e) {
                return null;
            }
            fontCache.put(name, tf);
        }
        return tf;
    }

    public static void setCustomFont(Context context, TextView textview, String font) {
        if(font == null) {
            return;
        }
        Typeface tf = FontCache.get(context, font);
        if(tf != null) {
            textview.setTypeface(tf);
        }
    }

    public static void setCustomFont(Context context, Paint textPaint, String font) {
        if(font == null) {
            return;
        }
        Typeface tf = FontCache.get(context, font);
        if(tf != null) {
        	textPaint.setTypeface(tf);
        }
    }
}
