package com.dole.heightchart;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.dole.heightchart.server.RegisterCS;
import com.dole.heightchart.server.ServerTask;
import com.dole.heightchart.server.ServerTask.IServerResponseCallback;
import com.dole.heightchart.ui.CustomDialog;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class RequestPaperFragment extends Fragment implements IServerResponseCallback {
	
	private static final String TAG = RequestPaperFragment.class.getName();
	
	private boolean mIsEmailFill = false;
	private boolean mIsRecipientFill = false;
	private boolean mIsAddressFill = false;
	private boolean mIsCityFill = false;
	private boolean mIsPostalFill = false;
	private ProgressDialog mProgressDialog;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.request_paper_main, null);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		//Change visibility and Change text
		final TextView emailText = (TextView) view.findViewById(R.id.request_edit_email);
		final TextView recipientText = (TextView)view.findViewById(R.id.request_edit_recipient);
		final TextView phoneText = (TextView)view.findViewById(R.id.request_edit_phone);
		final TextView addressText = (TextView)view.findViewById(R.id.request_edit_address);
		final TextView cityText = (TextView) view.findViewById(R.id.request_edit_city);
		final TextView postcodeText = (TextView)view.findViewById(R.id.request_edit_postcode);
		final Button confirmRequestButton = (Button) view.findViewById(R.id.request_btn_send);
		confirmRequestButton.setEnabled(false);
		
		final String[] cities = getResources().getStringArray(R.array.city);		
		if(cities != null && cities.length > 0) {
			cityText.setFocusable(false);
			cityText.setClickable(true);
			cityText.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View view) {
					Util.showDialogFragment(getFragmentManager(), new CityDialog(), Constants.CITY, null, RequestPaperFragment.this, Constants.REQUEST_CITY);
				}
			});
		}
		
		if(HeightChartPreference.getInt(Constants.PREF_USER_NO, 0) > 0) {
			mIsEmailFill = true;
			emailText.setText(HeightChartPreference.getString(Constants.PREF_USER_ID, ""));
		}
		
		emailText.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				String st = s.toString();
				if(st.trim().equals(""))
					mIsEmailFill = false;
				else
					mIsEmailFill = true;
				
				if(mIsEmailFill && mIsRecipientFill && mIsAddressFill && mIsCityFill && mIsPostalFill)
					confirmRequestButton.setEnabled(true);
				else
					confirmRequestButton.setEnabled(false);
			}
		});
		
		recipientText.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				String st = s.toString();
				if(st.trim().equals(""))
					mIsRecipientFill = false;
				else
					mIsRecipientFill = true;
				
				if(mIsEmailFill && mIsRecipientFill && mIsAddressFill && mIsCityFill && mIsPostalFill)
					confirmRequestButton.setEnabled(true);
				else
					confirmRequestButton.setEnabled(false);
			}
		});
		
		addressText.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				String st = s.toString();
				if(st.trim().equals(""))
					mIsAddressFill = false;
				else
					mIsAddressFill = true;
				
				if(mIsEmailFill && mIsRecipientFill && mIsAddressFill && mIsCityFill && mIsPostalFill)
					confirmRequestButton.setEnabled(true);
				else
					confirmRequestButton.setEnabled(false);
			}
		});
		
		cityText.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				String st = s.toString();
				if(st.trim().equals(""))
					mIsCityFill = false;
				else
					mIsCityFill = true;

				if(mIsEmailFill && mIsRecipientFill && mIsAddressFill && mIsCityFill && mIsPostalFill)
					confirmRequestButton.setEnabled(true);
				else
					confirmRequestButton.setEnabled(false);
			}
		});
		
		postcodeText.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				String st = s.toString();
				if(st.trim().equals(""))
					mIsPostalFill = false;
				else
					mIsPostalFill = true;

				if(mIsEmailFill && mIsRecipientFill && mIsAddressFill && mIsCityFill && mIsPostalFill)
					confirmRequestButton.setEnabled(true);
				else
					confirmRequestButton.setEnabled(false);
			}
		});
		
		confirmRequestButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String text = recipientText.getText().toString() + "," + phoneText.getText().toString() + "\n"
						+ addressText.getText().toString() + "\n" + postcodeText.getText().toString();
				
				Bundle args = new Bundle();
				args.putInt(Constants.CUSTOM_DIALOG_IMG_RES, R.drawable.popup_warning);
				args.putInt(Constants.CUSTOM_DIALOG_TEXT_RES, R.string.check_written_info);
				args.putString(Constants.CUSTOM_DIALOG_TEXT_STRING, text);
				args.putInt(Constants.CUSTOM_DIALOG_LEFT_BUTTON_RES, R.drawable.popup_cancel_btn);
				args.putInt(Constants.CUSTOM_DIALOG_RIGHT_BUTTON_RES, R.drawable.popup_ok_btn);
				args.putBoolean(Constants.CUSTOM_DIALOG_CHANGE_TEXT_YES_NO, true);
				
				Util.showDialogFragment(getFragmentManager(), new CustomDialog(), Constants.TAG_DIALOG_CUSTOM, 
						args, RequestPaperFragment.this, Constants.REQUEST_USER_CONFIRM);
			}
		});
		
		getView().setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent e) {
				Util.hideSoftInput(getActivity(), v);
				emailText.clearFocus();
				recipientText.clearFocus();
				phoneText.clearFocus();
				addressText.clearFocus();
				cityText.clearFocus();
				postcodeText.clearFocus();
				return false;
			}
		});
	}
	
	private void sendRequest() {
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd kk:mm");
		
		View view = getView();
		TextView emailText = (TextView) view.findViewById(R.id.request_edit_email);
		TextView recipientText = (TextView)view.findViewById(R.id.request_edit_recipient);
		TextView addressText = (TextView)view.findViewById(R.id.request_edit_address);
		TextView cityText = (TextView) view.findViewById(R.id.request_edit_city);
		TextView postcodeText = (TextView)view.findViewById(R.id.request_edit_postcode);
		
		String emailAdd = emailText.getText().toString();
		if(!Util.checkEmail(emailAdd)) {
			Toast.makeText(getActivity(), getResources().getString(R.string.enter_full_email), Toast.LENGTH_SHORT).show();
			return;
		}
		String content = recipientText.getText().toString()+","+
				addressText.getText().toString()+","+
				cityText.getText().toString()+","+
				postcodeText.getText().toString();
		
		HashMap<String, String> request = new HashMap<String, String>();
		request.put(Constants.TAG_USER_NO, String.valueOf(HeightChartPreference.getInt(Constants.PREF_USER_NO, 0)));
		request.put(Constants.TAG_USER_ID, HeightChartPreference.getString(Constants.PREF_USER_ID, emailAdd));
		request.put(Constants.TAG_EMAIL, emailAdd);
		request.put(Constants.TAG_IS_RECEIVE_EMAIL, String.valueOf(true));
		request.put(Constants.TAG_CATEGORY_NO, String.valueOf(1));
		request.put(Constants.TAG_SUB_CATEGORY_NO, String.valueOf(151));
		request.put(Constants.TAG_INQUIRY_TITLE, "paper event");
		request.put(Constants.TAG_INQUIRY_CONTENT, content);
		request.put(Constants.TAG_SERVICE_CODE, Constants.SERVICE_CODE);
		request.put(Constants.TAG_OCCURRENCE_DATE_TIME, format.format(date));
		request.put(Constants.TAG_DESCRIPTION, "");
		request.put(Constants.TAG_CLIENT_IP, Util.getIp(getActivity()));

		RegisterCS registerCS = new RegisterCS(getActivity(), request);
		registerCS.setCallback(this);
		Model.runOnWorkerThread(registerCS);
		
		mProgressDialog = Util.openProgressDialog(getActivity());
	}
	
	@Override
	public void onSuccess(ServerTask parser, Map<String, String> result) {
		DLog.d(TAG, " callback success = " + result);
		HeightChartPreference.putBoolean(Constants.PREF_REQUEST_PAPER, true);
		Toast.makeText(getActivity(), getResources().getString(R.string.request_received), Toast.LENGTH_SHORT).show();
		mProgressDialog.dismiss();
		getFragmentManager().popBackStack();
	}
	
	@Override
	public void onFailed(ServerTask parser, Map<String, String> result, int returnCode) {
		DLog.d(TAG, " callback fail = " + result);
		Toast.makeText(getActivity(), Util.switchErrorCode(returnCode, getResources()), Toast.LENGTH_SHORT).show();
		mProgressDialog.dismiss();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if(resultCode != Activity.RESULT_OK) 
			return;
		
		switch (requestCode) {
		case Constants.REQUEST_USER_CONFIRM:
			if(resultCode == Activity.RESULT_OK)
				sendRequest();
			break;
		case Constants.REQUEST_CITY:
			if(resultCode == Activity.RESULT_OK) {
				String city = data.getStringExtra(Constants.CITY);
				TextView cityText = (TextView) getView().findViewById(R.id.request_edit_city);
				cityText.setText(city);
			}
		default:
			break;
		}
	}
}
