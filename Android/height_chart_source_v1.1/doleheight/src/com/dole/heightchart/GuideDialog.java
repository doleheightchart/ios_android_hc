package com.dole.heightchart;

import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dole.heightchart.ui.FontTextHelper;

public class GuideDialog extends DialogFragment {
	private static final String TAG = GuideDialog.class.getName();
	
	public static final String MODE_GUIDE = "mode_guide";
	public static final String ITEM_COUNT = "item_count";
	public static final String ITEM_IDS = "item_ids";
	public static final String ITEM_LEFTS = "item_lefts";
	public static final String ITEM_TOPS = "item_tops";
	
	public static final String COUNTRY_NEW_ZEALAND = "NZ";
	
	private GuideMode mMode = GuideMode.GUIDE_SPLASH;
	
	enum GuideMode {
		GUIDE_SPLASH(R.layout.guide_splash),
		GUIDE_SPLASH_NEW_NICK(R.layout.guide_splash_new_nick),
		GUIDE_SPLASH_PAPER(R.layout.guide_splash_paper),
		GUIDE_DETAIL(R.layout.guide_detail),
		GUIDE_INPUT_HEIGHT(R.layout.guide_input_height),
		GUIDE_CAMERA(R.layout.guide_camera),
		GUIDE_PHOTO_EDIT(R.layout.guide_photo_edit),
		GUIDE_LOGIN(R.layout.guide_login); 
		
		int layoutId;
		private GuideMode(int layoutId) {
			this.layoutId = layoutId;
		}
	}
	
	private int mArgsCount = 0;
	private int[] mPosIDAry;
	private int[] mPosXAry;
	private int[] mPosYAry;

	private int mFragmentHeight = 0;

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
        final RelativeLayout root = new RelativeLayout(getActivity());
        root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
 
        final Dialog dialog = new Dialog(getActivity()) {
        	@Override
        	public boolean onTouchEvent(MotionEvent event) {
        		this.dismiss();
        		return true;
        	}
        };
        
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        dialog.setContentView(root);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.YELLOW));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
		
		return dialog;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		final Dialog d = getDialog();
		d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		d.getWindow().setLayout(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		Bundle args = getArguments();
		if(args != null) {
			DLog.d(TAG, " args.getString(MODE_GUIDE) = " + args.getString(MODE_GUIDE));
			mMode = GuideMode.valueOf(args.getString(MODE_GUIDE));
			
			HeightChartPreference.putBoolean(mMode.name(), true);
		}
		DLog.d(TAG, " mMode.layoutId = " + mMode.layoutId);
		return inflater.inflate(mMode.layoutId, null);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		Bundle args = getArguments();
		if(args != null) {
			DLog.d(TAG, " args.getString(MODE_GUIDE) = " + args.getString(MODE_GUIDE));
			mMode = GuideMode.valueOf(args.getString(MODE_GUIDE));
			
			mArgsCount = args.getInt(GuideDialog.ITEM_COUNT);
			mPosIDAry = args.getIntArray(GuideDialog.ITEM_IDS);
			mPosXAry = args.getIntArray(GuideDialog.ITEM_LEFTS);
			mPosYAry = args.getIntArray(GuideDialog.ITEM_TOPS);
			
			for(int i = 0; i< mArgsCount; i++) {
				final int id = mPosIDAry[i];
				final View child = ((ViewGroup)getView()).findViewById(id);
				if(child != null) {
					final RelativeLayout.LayoutParams param = (RelativeLayout.LayoutParams) child.getLayoutParams();
					if(mPosXAry[i] != -1)
						param.leftMargin = mPosXAry[i];
					if(mPosYAry[i] != -1)
						param.topMargin = mPosYAry[i];
					child.setLayoutParams(param);
					child.requestLayout();
					DLog.d(TAG, "guide id = " + id + " x = " + mPosXAry[i] + " y = " + mPosYAry[i]);
				}
			}
		}
		
		initSpecificViews();
	}
	
	private void initSpecificViews() {
		final View textView = getView().findViewById(R.id.guide_text);
		if(textView instanceof TextView) {
			final TextView guideText = (TextView) textView;
			FontTextHelper.setGuideFont(guideText.getContext(), guideText);
		}
		
		switch (mMode) {
		case GUIDE_SPLASH:
			break;
		case GUIDE_SPLASH_PAPER:
			final View textView1 = getView().findViewById(R.id.guide_text1);
			if(textView1 instanceof TextView) {
				final TextView guideText = (TextView) textView1;
				FontCache.setCustomFont(getActivity(), guideText, "NanumPen.ttf");
			}
			break;
		case GUIDE_INPUT_HEIGHT:
			Activity activity = getActivity();
			if(activity == null)
				break;
			
			final GuideHeightTreeAdapter adapter = new GuideHeightTreeAdapter(0, 200);
			final ListView listView = (ListView) getView().findViewById(R.id.guide_input_height_ruler);
			listView.setAdapter(adapter);
			listView.setScrollContainer(false);
			listView.setClickable(false); 
			listView.setEnabled(false); 
			listView.setOnTouchListener(new OnTouchListener() {
			    public boolean onTouch(View v, MotionEvent event) {
			        return true;
			    }
			});
			
			listView.getViewTreeObserver().addOnPreDrawListener(new OnPreDrawListener() {
				
				@Override
				public boolean onPreDraw() {
					final View v = listView.getChildAt(0);
					int listHalf = listView.getHeight() / 2;
					int itemHalf = v.getHeight() / 2;
					listView.setSelectionFromTop(10, listHalf - itemHalf);
					listView.getViewTreeObserver().removeOnPreDrawListener(this);
					
					return true;

				}
			});
			break;

		default:
			break;
		}
	}
	
	class GuideHeightTreeAdapter extends BaseAdapter {
		private int[] mData;
		
		public GuideHeightTreeAdapter(int min, int max) {
			if(min >= max)
				return;
			int len = (max - min + 1)/10;
			mData = new int[len];
			for(int i = 0; i < len; i++) {
				mData[i] = min + 9 + (i*10);
			}
		}
		
		@Override
		public boolean isEnabled(int position) {
			return false;
		}

		@Override
		public int getCount() {
			return mData.length+2;
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return 0;
		}
		
		@Override
		public int getItemViewType(int position) {
			if(position == 0 || position == getCount()-1)
				return 1;
			else
				return 2;
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			int viewType = getItemViewType(position);
			if(convertView == null || (Integer)convertView.getTag() != viewType) {
				if(viewType == 1) {
					convertView = getActivity().getLayoutInflater().inflate(R.layout.input_height_empty_item, null);
					convertView.setTag(viewType);
				}
				else {
					convertView = getActivity().getLayoutInflater().inflate(R.layout.input_height_item, null);
					convertView.setTag(viewType);
				}
			}
			View v = convertView;
			if(viewType == 1) {
				if(mFragmentHeight == 0) {
					mFragmentHeight = GuideDialog.this.getView().getMeasuredHeight();
				}
				View view = v.findViewById(R.id.input_height_item_container);
				ViewGroup.LayoutParams params = view.getLayoutParams();
				params.height = mFragmentHeight/2;
				view.setLayoutParams(params);
				convertView.requestLayout();
			}
			else {
				TextView tv = (TextView)v.findViewById(R.id.input_height_item_cover);
				tv.setText(mData[mData.length-position]+"");
			}
			
			return v;
		}
		
	}
}
