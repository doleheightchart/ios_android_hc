/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dole.heightchart;

import android.app.Application;

import com.dole.heightchart.server.ServerIF;
import com.facebook.Settings;

public class ApplicationImpl extends Application {
	
	private Model mModel;
	private ImageCache mImageCache;

    @Override
    public void onCreate() {
        super.onCreate();
        mImageCache = new ImageCache();
        mModel = new Model(this);
        mModel.loadDB();
        
        Settings.setPlatformCompatibilityEnabled(true);
        
        //Util.initialize(this);
        HeightChartPreference.initSharedPreferences(getApplicationContext());
        
        //TODO Needs before any server-if posts
        ServerIF.getUserAgent(this);
    }
    
    public Model getModel() {
    	return mModel;
    }
    
    public ImageCache getImageCache() {
    	return mImageCache;
    }
    
    public void reloadDB() {
    	mModel = new Model(this);
        mModel.loadDB();
    }
}


