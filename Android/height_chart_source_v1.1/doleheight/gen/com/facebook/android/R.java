/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */
package com.facebook.android;

public final class R {
	public static final class attr {
		public static final int confirm_logout = 0x7f01001d;
		public static final int done_button_background = 0x7f010017;
		public static final int done_button_text = 0x7f010015;
		public static final int extra_fields = 0x7f010012;
		public static final int fetch_user_info = 0x7f01001e;
		public static final int is_cropped = 0x7f010022;
		public static final int login_text = 0x7f01001f;
		public static final int logout_text = 0x7f010020;
		public static final int multi_select = 0x7f010018;
		public static final int preset_size = 0x7f010021;
		public static final int radius_in_meters = 0x7f010019;
		public static final int results_limit = 0x7f01001a;
		public static final int search_text = 0x7f01001b;
		public static final int show_pictures = 0x7f010011;
		public static final int show_search_box = 0x7f01001c;
		public static final int show_title_bar = 0x7f010013;
		public static final int title_bar_background = 0x7f010016;
		public static final int title_text = 0x7f010014;
	}
	public static final class color {
		public static final int com_facebook_blue = 0x7f06000d;
		public static final int com_facebook_loginview_text_color = 0x7f060011;
		public static final int com_facebook_picker_search_bar_background = 0x7f06000b;
		public static final int com_facebook_picker_search_bar_text = 0x7f06000c;
		public static final int com_facebook_usersettingsfragment_connected_shadow_color = 0x7f06000f;
		public static final int com_facebook_usersettingsfragment_connected_text_color = 0x7f06000e;
		public static final int com_facebook_usersettingsfragment_not_connected_text_color = 0x7f060010;
	}
	public static final class dimen {
		public static final int com_facebook_loginview_compound_drawable_padding = 0x7f0a0008;
		public static final int com_facebook_loginview_padding_bottom = 0x7f0a0007;
		public static final int com_facebook_loginview_padding_left = 0x7f0a0004;
		public static final int com_facebook_loginview_padding_right = 0x7f0a0005;
		public static final int com_facebook_loginview_padding_top = 0x7f0a0006;
		public static final int com_facebook_loginview_text_size = 0x7f0a0009;
		public static final int com_facebook_picker_divider_width = 0x7f0a0001;
		public static final int com_facebook_picker_place_image_size = 0x7f0a0000;
		public static final int com_facebook_profilepictureview_preset_size_large = 0x7f0a000c;
		public static final int com_facebook_profilepictureview_preset_size_normal = 0x7f0a000b;
		public static final int com_facebook_profilepictureview_preset_size_small = 0x7f0a000a;
		public static final int com_facebook_tooltip_horizontal_padding = 0x7f0a000d;
		public static final int com_facebook_usersettingsfragment_profile_picture_height = 0x7f0a0003;
		public static final int com_facebook_usersettingsfragment_profile_picture_width = 0x7f0a0002;
	}
	public static final class drawable {
		public static final int com_facebook_button_blue = 0x7f02015d;
		public static final int com_facebook_button_blue_focused = 0x7f02015e;
		public static final int com_facebook_button_blue_normal = 0x7f02015f;
		public static final int com_facebook_button_blue_pressed = 0x7f020160;
		public static final int com_facebook_button_check = 0x7f020161;
		public static final int com_facebook_button_check_off = 0x7f020162;
		public static final int com_facebook_button_check_on = 0x7f020163;
		public static final int com_facebook_button_grey_focused = 0x7f020164;
		public static final int com_facebook_button_grey_normal = 0x7f020165;
		public static final int com_facebook_button_grey_pressed = 0x7f020166;
		public static final int com_facebook_close = 0x7f020167;
		public static final int com_facebook_inverse_icon = 0x7f020168;
		public static final int com_facebook_list_divider = 0x7f020169;
		public static final int com_facebook_list_section_header_background = 0x7f02016a;
		public static final int com_facebook_loginbutton_silver = 0x7f02016b;
		public static final int com_facebook_logo = 0x7f02016c;
		public static final int com_facebook_picker_default_separator_color = 0x7f020316;
		public static final int com_facebook_picker_item_background = 0x7f02016d;
		public static final int com_facebook_picker_list_focused = 0x7f02016e;
		public static final int com_facebook_picker_list_longpressed = 0x7f02016f;
		public static final int com_facebook_picker_list_pressed = 0x7f020170;
		public static final int com_facebook_picker_list_selector = 0x7f020171;
		public static final int com_facebook_picker_list_selector_background_transition = 0x7f020172;
		public static final int com_facebook_picker_list_selector_disabled = 0x7f020173;
		public static final int com_facebook_picker_magnifier = 0x7f020174;
		public static final int com_facebook_picker_top_button = 0x7f020175;
		public static final int com_facebook_place_default_icon = 0x7f020176;
		public static final int com_facebook_profile_default_icon = 0x7f020177;
		public static final int com_facebook_profile_picture_blank_portrait = 0x7f020178;
		public static final int com_facebook_profile_picture_blank_square = 0x7f020179;
		public static final int com_facebook_tooltip_black_background = 0x7f02017a;
		public static final int com_facebook_tooltip_black_bottomnub = 0x7f02017b;
		public static final int com_facebook_tooltip_black_topnub = 0x7f02017c;
		public static final int com_facebook_tooltip_black_xout = 0x7f02017d;
		public static final int com_facebook_tooltip_blue_background = 0x7f02017e;
		public static final int com_facebook_tooltip_blue_bottomnub = 0x7f02017f;
		public static final int com_facebook_tooltip_blue_topnub = 0x7f020180;
		public static final int com_facebook_tooltip_blue_xout = 0x7f020181;
		public static final int com_facebook_top_background = 0x7f020182;
		public static final int com_facebook_top_button = 0x7f020183;
		public static final int com_facebook_usersettingsfragment_background_gradient = 0x7f020184;
	}
	public static final class id {
		public static final int com_facebook_body_frame = 0x7f0d007c;
		public static final int com_facebook_button_xout = 0x7f0d007e;
		public static final int com_facebook_login_activity_progress_bar = 0x7f0d006c;
		public static final int com_facebook_picker_activity_circle = 0x7f0d006b;
		public static final int com_facebook_picker_checkbox = 0x7f0d006e;
		public static final int com_facebook_picker_checkbox_stub = 0x7f0d0072;
		public static final int com_facebook_picker_divider = 0x7f0d0076;
		public static final int com_facebook_picker_done_button = 0x7f0d0075;
		public static final int com_facebook_picker_image = 0x7f0d006f;
		public static final int com_facebook_picker_list_section_header = 0x7f0d0073;
		public static final int com_facebook_picker_list_view = 0x7f0d006a;
		public static final int com_facebook_picker_profile_pic_stub = 0x7f0d0070;
		public static final int com_facebook_picker_row_activity_circle = 0x7f0d006d;
		public static final int com_facebook_picker_search_text = 0x7f0d007b;
		public static final int com_facebook_picker_title = 0x7f0d0071;
		public static final int com_facebook_picker_title_bar = 0x7f0d0078;
		public static final int com_facebook_picker_title_bar_stub = 0x7f0d0077;
		public static final int com_facebook_picker_top_bar = 0x7f0d0074;
		public static final int com_facebook_search_bar_view = 0x7f0d007a;
		public static final int com_facebook_tooltip_bubble_view_bottom_pointer = 0x7f0d0080;
		public static final int com_facebook_tooltip_bubble_view_text_body = 0x7f0d007f;
		public static final int com_facebook_tooltip_bubble_view_top_pointer = 0x7f0d007d;
		public static final int com_facebook_usersettingsfragment_login_button = 0x7f0d0083;
		public static final int com_facebook_usersettingsfragment_logo_image = 0x7f0d0081;
		public static final int com_facebook_usersettingsfragment_profile_name = 0x7f0d0082;
		public static final int large = 0x7f0d0005;
		public static final int normal = 0x7f0d0002;
		public static final int picker_subtitle = 0x7f0d0079;
		public static final int small = 0x7f0d0006;
	}
	public static final class layout {
		public static final int com_facebook_friendpickerfragment = 0x7f030013;
		public static final int com_facebook_login_activity_layout = 0x7f030014;
		public static final int com_facebook_picker_activity_circle_row = 0x7f030015;
		public static final int com_facebook_picker_checkbox = 0x7f030016;
		public static final int com_facebook_picker_image = 0x7f030017;
		public static final int com_facebook_picker_list_row = 0x7f030018;
		public static final int com_facebook_picker_list_section_header = 0x7f030019;
		public static final int com_facebook_picker_search_box = 0x7f03001a;
		public static final int com_facebook_picker_title_bar = 0x7f03001b;
		public static final int com_facebook_picker_title_bar_stub = 0x7f03001c;
		public static final int com_facebook_placepickerfragment = 0x7f03001d;
		public static final int com_facebook_placepickerfragment_list_row = 0x7f03001e;
		public static final int com_facebook_search_bar_layout = 0x7f03001f;
		public static final int com_facebook_tooltip_bubble = 0x7f030020;
		public static final int com_facebook_usersettingsfragment = 0x7f030021;
	}
	public static final class string {
		public static final int com_facebook_choose_friends = 0x7f070031;
		public static final int com_facebook_dialogloginactivity_ok_button = 0x7f070022;
		public static final int com_facebook_internet_permission_error_message = 0x7f070035;
		public static final int com_facebook_internet_permission_error_title = 0x7f070034;
		public static final int com_facebook_loading = 0x7f070033;
		public static final int com_facebook_loginview_cancel_action = 0x7f070028;
		public static final int com_facebook_loginview_log_in_button = 0x7f070024;
		public static final int com_facebook_loginview_log_out_action = 0x7f070027;
		public static final int com_facebook_loginview_log_out_button = 0x7f070023;
		public static final int com_facebook_loginview_logged_in_as = 0x7f070025;
		public static final int com_facebook_loginview_logged_in_using_facebook = 0x7f070026;
		public static final int com_facebook_logo_content_description = 0x7f070029;
		public static final int com_facebook_nearby = 0x7f070032;
		public static final int com_facebook_picker_done_button_text = 0x7f070030;
		public static final int com_facebook_placepicker_subtitle_catetory_only_format = 0x7f07002e;
		public static final int com_facebook_placepicker_subtitle_format = 0x7f07002d;
		public static final int com_facebook_placepicker_subtitle_were_here_only_format = 0x7f07002f;
		public static final int com_facebook_requesterror_password_changed = 0x7f070038;
		public static final int com_facebook_requesterror_permissions = 0x7f07003a;
		public static final int com_facebook_requesterror_reconnect = 0x7f070039;
		public static final int com_facebook_requesterror_relogin = 0x7f070037;
		public static final int com_facebook_requesterror_web_login = 0x7f070036;
		public static final int com_facebook_tooltip_default = 0x7f07003b;
		public static final int com_facebook_usersettingsfragment_log_in_button = 0x7f07002a;
		public static final int com_facebook_usersettingsfragment_logged_in = 0x7f07002b;
		public static final int com_facebook_usersettingsfragment_not_logged_in = 0x7f07002c;
	}
	public static final class style {
		public static final int com_facebook_loginview_default_style = 0x7f090002;
		public static final int com_facebook_loginview_silver_style = 0x7f090003;
		public static final int tooltip_bubble_text = 0x7f090004;
	}
	public static final class styleable {
		public static final int[] com_facebook_friend_picker_fragment = { 0x7f010018 };
		public static final int com_facebook_friend_picker_fragment_multi_select = 0;
		public static final int[] com_facebook_login_view = { 0x7f01001d, 0x7f01001e, 0x7f01001f, 0x7f010020 };
		public static final int com_facebook_login_view_confirm_logout = 0;
		public static final int com_facebook_login_view_fetch_user_info = 1;
		public static final int com_facebook_login_view_login_text = 2;
		public static final int com_facebook_login_view_logout_text = 3;
		public static final int[] com_facebook_picker_fragment = { 0x7f010011, 0x7f010012, 0x7f010013, 0x7f010014, 0x7f010015, 0x7f010016, 0x7f010017 };
		public static final int com_facebook_picker_fragment_done_button_background = 6;
		public static final int com_facebook_picker_fragment_done_button_text = 4;
		public static final int com_facebook_picker_fragment_extra_fields = 1;
		public static final int com_facebook_picker_fragment_show_pictures = 0;
		public static final int com_facebook_picker_fragment_show_title_bar = 2;
		public static final int com_facebook_picker_fragment_title_bar_background = 5;
		public static final int com_facebook_picker_fragment_title_text = 3;
		public static final int[] com_facebook_place_picker_fragment = { 0x7f010019, 0x7f01001a, 0x7f01001b, 0x7f01001c };
		public static final int com_facebook_place_picker_fragment_radius_in_meters = 0;
		public static final int com_facebook_place_picker_fragment_results_limit = 1;
		public static final int com_facebook_place_picker_fragment_search_text = 2;
		public static final int com_facebook_place_picker_fragment_show_search_box = 3;
		public static final int[] com_facebook_profile_picture_view = { 0x7f010021, 0x7f010022 };
		public static final int com_facebook_profile_picture_view_is_cropped = 1;
		public static final int com_facebook_profile_picture_view_preset_size = 0;
	}
}
