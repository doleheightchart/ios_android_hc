var searchData=
[
  ['select_5fbackup',['SELECT_BACKUP',['../classcom_1_1dole_1_1heightchart_1_1_backup_dialog.html#a5f269496425ac09391bf033a37469680',1,'com::dole::heightchart::BackupDialog']]],
  ['select_5frestore',['SELECT_RESTORE',['../classcom_1_1dole_1_1heightchart_1_1_backup_dialog.html#ab1df8bca646f3e4d29201dabe66c06de',1,'com::dole::heightchart::BackupDialog']]],
  ['senglishboldfontpath',['sEnglishBoldFontPath',['../classcom_1_1dole_1_1heightchart_1_1ui_1_1_font_text_helper.html#a8d74925985f6c6f7a0d5171c6fbede0e',1,'com::dole::heightchart::ui::FontTextHelper']]],
  ['senglishfontpath',['sEnglishFontPath',['../classcom_1_1dole_1_1heightchart_1_1ui_1_1_font_text_helper.html#a768f5df4ce0bf29472203fa582be7e1e',1,'com::dole::heightchart::ui::FontTextHelper']]],
  ['shared_5fpreferences_5fname',['SHARED_PREFERENCES_NAME',['../classcom_1_1dole_1_1heightchart_1_1_height_chart_preference.html#abcd8a1ce56dabc339f2a9a8f4ff8e0c1',1,'com::dole::heightchart::HeightChartPreference']]],
  ['sign_5fup_5femail',['SIGN_UP_EMAIL',['../classcom_1_1dole_1_1heightchart_1_1_user_detail_fragment.html#abf3f9e7a4228e41bb949711e40ad6844',1,'com::dole::heightchart::UserDetailFragment']]],
  ['sign_5fup_5ffacebook',['SIGN_UP_FACEBOOK',['../classcom_1_1dole_1_1heightchart_1_1_user_detail_fragment.html#a83bab1258a83090daa4c1a0661b5aa64',1,'com::dole::heightchart::UserDetailFragment']]],
  ['skorfontpath',['sKorFontPath',['../classcom_1_1dole_1_1heightchart_1_1ui_1_1_font_text_helper.html#a54b307f13ca2503a25817eae0f7574f5',1,'com::dole::heightchart::ui::FontTextHelper']]],
  ['slangkr',['sLangKr',['../classcom_1_1dole_1_1heightchart_1_1ui_1_1_font_text_helper.html#aaa9f3dce4ddbe48d4505ce754820c2f9',1,'com::dole::heightchart::ui::FontTextHelper']]],
  ['snap_5fanimation_5fduration',['SNAP_ANIMATION_DURATION',['../classcom_1_1dole_1_1heightchart_1_1ui_1_1_scroll_helper.html#a95ac67573a4d98c588b7f5ebad81c5cd',1,'com::dole::heightchart::ui::ScrollHelper']]],
  ['snap_5fanimation_5fmax_5fduration',['SNAP_ANIMATION_MAX_DURATION',['../classcom_1_1dole_1_1heightchart_1_1ui_1_1_scroll_helper.html#a1a5bf8634953dd05c2ffaa780a660c9d',1,'com::dole::heightchart::ui::ScrollHelper']]],
  ['snap_5fvelocity',['SNAP_VELOCITY',['../classcom_1_1dole_1_1heightchart_1_1ui_1_1_scroll_helper.html#af7e6fd3eb1a026a3760a93959758e88f',1,'com::dole::heightchart::ui::ScrollHelper']]],
  ['stypefacefilename',['sTypefaceFilename',['../classcom_1_1dole_1_1heightchart_1_1ui_1_1_font_text_helper.html#ab4a25195e57fa9eae401227d119f20bf',1,'com::dole::heightchart::ui::FontTextHelper']]]
];
