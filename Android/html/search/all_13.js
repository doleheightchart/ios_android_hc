var searchData=
[
  ['unavailable',['UNAVAILABLE',['../classcom_1_1dole_1_1heightchart_1_1camera_1_1_storage.html#af97e787f6e89877bd27a9bbc592f7f9d',1,'com::dole::heightchart::camera::Storage']]],
  ['unknown_5fsize',['UNKNOWN_SIZE',['../classcom_1_1dole_1_1heightchart_1_1camera_1_1_storage.html#ab880b6bc788ab904b2f8679f8dd205c7',1,'com::dole::heightchart::camera::Storage']]],
  ['update',['update',['../classcom_1_1dole_1_1heightchart_1_1_height_chart_provider.html#aa9cb3b611961f7e4ae43225f9d1ef496',1,'com.dole.heightchart.HeightChartProvider.update()'],['../classcom_1_1dole_1_1heightchart_1_1_nick_name_info.html#a258bd4f1860d6a843325b46210b765cf',1,'com.dole.heightchart.NickNameInfo.update()']]],
  ['updateviews',['updateViews',['../classcom_1_1dole_1_1heightchart_1_1ui_1_1_bg_transition_helper.html#a0b8f3a39fc30ea2717c6c4af129a1c10',1,'com::dole::heightchart::ui::BgTransitionHelper']]],
  ['uri',['URI',['../classcom_1_1dole_1_1heightchart_1_1_constants.html#aeb8442225139c5407db1b83ec6aefd18',1,'com::dole::heightchart::Constants']]],
  ['use_5fstaging_5fserver',['USE_STAGING_SERVER',['../classcom_1_1dole_1_1heightchart_1_1_constants.html#a97d40ba2f71715e3c729f572f2ec3160',1,'com::dole::heightchart::Constants']]],
  ['used_5fcode_5fwithin_5f24h',['USED_CODE_WITHIN_24H',['../classcom_1_1dole_1_1heightchart_1_1_constants.html#a31924f070d566d65c6869cad732f2daf',1,'com::dole::heightchart::Constants']]],
  ['userdetailfragment',['UserDetailFragment',['../classcom_1_1dole_1_1heightchart_1_1_user_detail_fragment.html',1,'com::dole::heightchart']]],
  ['userdetailfragment_2ejava',['UserDetailFragment.java',['../_user_detail_fragment_8java.html',1,'']]],
  ['util',['Util',['../classcom_1_1dole_1_1heightchart_1_1_util.html',1,'com::dole::heightchart']]],
  ['util_2ejava',['Util.java',['../_util_8java.html',1,'']]]
];
